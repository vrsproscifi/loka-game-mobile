// VRSPRO

#include "LokaGame.h"
#include "PlayerController/LobbyPlayerController.h"
#include "LobbyCamera.h"


ALobbyCamera::ALobbyCamera(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	TPCameraSpring = ObjectInitializer.CreateDefaultSubobject<USpringArmComponent>(this, TEXT("ThirdPersonCameraArm"));
	TPCameraSpring->SetupAttachment(GetRootComponent());
	TPCameraSpring->bAutoActivate = true;
	TPCameraSpring->bUsePawnControlRotation = true;

	TPCamera = ObjectInitializer.CreateDefaultSubobject<UCameraComponent>(this, TEXT("ThirdPersonCamera"));
	TPCamera->SetupAttachment(TPCameraSpring);
	TPCamera->bAutoActivate = true;
	TPCamera->bUsePawnControlRotation = false;
	TPCamera->bConstrainAspectRatio = false;

	TPCamera->PostProcessSettings.bOverride_DepthOfFieldMethod = true;
	TPCamera->PostProcessSettings.DepthOfFieldMethod = EDepthOfFieldMethod::DOFM_Gaussian;
	TPCamera->PostProcessSettings.bOverride_DepthOfFieldFocalDistance = true;
	TPCamera->PostProcessSettings.DepthOfFieldFocalDistance = 50.0f;
	TPCamera->PostProcessSettings.bOverride_DepthOfFieldFocalRegion = true;
	TPCamera->PostProcessSettings.DepthOfFieldFocalRegion = 40.0f;
	TPCamera->PostProcessSettings.bOverride_DepthOfFieldNearTransitionRegion = true;
	TPCamera->PostProcessSettings.DepthOfFieldNearTransitionRegion = 0.0f;
	TPCamera->PostProcessSettings.bOverride_DepthOfFieldFarTransitionRegion = true;
	TPCamera->PostProcessSettings.DepthOfFieldFarTransitionRegion = 100.0f;
	TPCamera->PostProcessSettings.bOverride_DepthOfFieldNearBlurSize = true;
	TPCamera->PostProcessSettings.DepthOfFieldNearBlurSize = 0.0f;
	TPCamera->PostProcessSettings.bOverride_DepthOfFieldFarBlurSize = true;
	TPCamera->PostProcessSettings.DepthOfFieldFarBlurSize = 0.0f;
	//TPCamera->PostProcessSettings.bOverride_AntiAliasingMethod = true;
	//TPCamera->PostProcessSettings.AntiAliasingMethod = EAntiAliasingMethod::AAM_TemporalAA;

	PrimaryActorTick.bCanEverTick = true;

	bEnableAutoRotate = true;
	bRotationDirection = true;
	bEnableDOF = false;
	CurrentMode = ELobbyCameraMode::Character;

	OldScroolDistance[ELobbyCameraMode::Character] = .0f;
	OldScroolDistance[ELobbyCameraMode::Weapon] = .0f;

	StartDistance = .35f;
}

void ALobbyCamera::BeginPlay()
{
	Super::BeginPlay();
	
	TargetPosition = GetActorLocation();
	SourcePosition = TargetPosition;
	TargetRotation = TPCameraSpring->RelativeRotation;
	TargetScroolDistance = FMath::Lerp(SpringDistanceLimits[CurrentMode].X, SpringDistanceLimits[CurrentMode].Y, StartDistance);

	if (auto MyController = Cast<ALobbyPlayerController>(GetWorld()->GetFirstPlayerController()))
	{
		MyController->SetCameraRef(this);
		MyController->SetViewTarget(this);
	}
}

void ALobbyCamera::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	
	SetActorLocation(FMath::VInterpTo(GetActorLocation(), TargetPosition, DeltaTime, 3.5f));

	TPCameraSpring->TargetArmLength = FMath::FInterpTo(TPCameraSpring->TargetArmLength, TargetScroolDistance, DeltaTime, SmoothScroolSpeed);

	if (bEnableAutoRotate && RotationLastTime < GetWorld()->GetTimeSeconds())
	{
		FRotator CurrentRotation = TPCameraSpring->RelativeRotation;
		CurrentRotation.Yaw = FMath::FInterpTo(CurrentRotation.Yaw, CurrentRotation.Yaw + (bRotationDirection ? RotationIterator : -RotationIterator), DeltaTime, RotationSmoothSpeed);
		TPCameraSpring->SetRelativeRotation(CurrentRotation);
		TargetRotation = CurrentRotation;
	}
	else
	{
		TPCameraSpring->SetRelativeRotation(FMath::RInterpTo(TPCameraSpring->RelativeRotation, TargetRotation, DeltaTime, RotationSmoothSpeed));
	}

	if (bEnableDOF)
	{
		TPCamera->PostProcessSettings.DepthOfFieldFarBlurSize = FMath::FInterpTo(TPCamera->PostProcessSettings.DepthOfFieldFarBlurSize, BlurRadius[ELobbyCameraMode::Weapon], DeltaTime, 1.0f);
		TPCamera->PostProcessSettings.DepthOfFieldFocalDistance = FMath::FInterpTo(TPCamera->PostProcessSettings.DepthOfFieldFocalDistance, TPCameraSpring->TargetArmLength, DeltaTime, 5.2f);
	}
	else
	{
		TPCamera->PostProcessSettings.DepthOfFieldFarBlurSize = FMath::FInterpTo(TPCamera->PostProcessSettings.DepthOfFieldFarBlurSize, BlurRadius[ELobbyCameraMode::Character], DeltaTime, 1.0f);
		TPCamera->PostProcessSettings.DepthOfFieldFocalDistance = FMath::FInterpTo(TPCamera->PostProcessSettings.DepthOfFieldFocalDistance, TPCameraSpring->TargetArmLength, DeltaTime, 4.2f);
	}
}

void ALobbyCamera::AddScroolDistance(const float target)
{
	TargetScroolDistance = FMath::Clamp<float>(TargetScroolDistance + target, SpringDistanceLimits[CurrentMode].X, SpringDistanceLimits[CurrentMode].Y);
}

void ALobbyCamera::SetScroolDistance(const float target)
{
	TargetScroolDistance = target;
	TargetScroolDistance = FMath::Clamp<float>(TargetScroolDistance, SpringDistanceLimits[CurrentMode].X, SpringDistanceLimits[CurrentMode].Y);
}

float ALobbyCamera::GetScroolDistance() const
{
	return TPCameraSpring->TargetArmLength;
}

void ALobbyCamera::AddRotation(const FRotator Val)
{
	SetRotation(TargetRotation + Val);
}

void ALobbyCamera::SetRotation(const FRotator Val)
{
	bRotationDirection = (TargetRotation.Yaw < Val.Yaw);

	TargetRotation = Val;
	TargetRotation.Pitch = FMath::Clamp(TargetRotation.Pitch, -50.0f, 50.0f);
	
	RotationLastTime = GetWorld()->GetTimeSeconds() + 1;
}

FRotator ALobbyCamera::GetRotation() const
{
	return TPCameraSpring->RelativeRotation;
}

void ALobbyCamera::SetIsAutoRotation(const bool toggle)
{
	bEnableAutoRotate = toggle;
}

bool ALobbyCamera::IsAutoRotation() const
{
	return bEnableAutoRotate;
}

void ALobbyCamera::SetIsEnabledDOF(const bool toggle)
{
	bEnableDOF = toggle;
}

bool ALobbyCamera::IsEnabledDOF() const
{
	return bEnableDOF;
}

FVector ALobbyCamera::GetCameraLocation() const
{
	return TPCamera->GetComponentLocation();
}

void ALobbyCamera::SetMode(const ELobbyCameraMode::Type Target)
{
	OldScroolDistance[CurrentMode] = TargetScroolDistance;
	CurrentMode = Target;

	if (FMath::IsNearlyEqual(OldScroolDistance[CurrentMode], .0f, 1.0f))
	{
		auto ClampedDistance = FMath::Clamp<float>(TargetScroolDistance, SpringDistanceLimits[CurrentMode].X, SpringDistanceLimits[CurrentMode].Y);
		TargetScroolDistance = FMath::Lerp(SpringDistanceLimits[CurrentMode].X, ClampedDistance, (CurrentMode == ELobbyCameraMode::Weapon) ? .3f : .65f);
	}
	else
	{
		TargetScroolDistance = OldScroolDistance[CurrentMode];
	}
}

ELobbyCameraMode::Type ALobbyCamera::GetMode() const
{
	return CurrentMode;
}

void ALobbyCamera::BeginActivateWM(const FVector Position)
{
	TargetPosition = Position;
	bEnableAutoRotate = false;
	SetIsEnabledDOF(true);
	SetMode(ELobbyCameraMode::Weapon);
}

void ALobbyCamera::BeginDeactivateWM(const FVector Position)
{
	TargetPosition = SourcePosition;
	bEnableAutoRotate = true;
	SetIsEnabledDOF(false);
	SetMode(ELobbyCameraMode::Character);
}