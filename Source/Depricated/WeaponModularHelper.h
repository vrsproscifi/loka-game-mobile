// VRSPRO

#pragma once
#include "Item/ItemSlotTypeId.h"
#include "Module/ItemModuleModelId.h"
#include "Material/ItemMaterialModelId.h"
#include "WeaponModularHelper.generated.h"

class UItemModuleEntity;
class UItemWeaponEntity;

USTRUCT()
struct FWeaponModularHelper// : public FGCObject
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY()
	TMap<FName, USkeletalMeshComponent*> InstalledModulesMeshMap;

	UPROPERTY()
	TArray<USkeletalMeshComponent*> InstalledModulesMesh;

	UPROPERTY()
	USkeletalMeshComponent* TargetModulesComponent;

	UPROPERTY()
	AActor* Owner;

	FWeaponModularHelper();

	void ClearModules();
	void InitializeModules(const UItemWeaponEntity* Source, const bool IsUseSlot = false);
	void InitializeModules(const UItemWeaponEntity* Source, const TArray<TEnumAsByte<ItemModuleModelId::Type>>& Modules, const TEnumAsByte<ItemMaterialModelId::Type>& Skin = ItemMaterialModelId::Default);

	bool InstallModule(const UItemModuleEntity* InModule, const ItemSlotTypeId::Type& Slot = ItemSlotTypeId::None);
	bool UninstallModule(const UItemModuleEntity* InModule, const ItemSlotTypeId::Type& Slot = ItemSlotTypeId::None);

	//virtual void AddReferencedObjects(FReferenceCollector& Collector) override;
};

#define ConstructModulesHelper(InComponent)\
WeaponModularHelper.Owner = this;\
WeaponModularHelper.ClearModules();\
WeaponModularHelper.TargetModulesComponent = InComponent;\
for (int32 i = 0; i < 10; ++i)\
{\
	WeaponModularHelper.InstalledModulesMesh.Add(CreateDefaultSubobject<USkeletalMeshComponent>(*FString::Printf(TEXT("Module_%d"), i)));\
	WeaponModularHelper.InstalledModulesMesh.Last()->SetupAttachment(RootComponent);\
	WeaponModularHelper.InstalledModulesMesh.Last()->bReceivesDecals = false;\
	WeaponModularHelper.InstalledModulesMesh.Last()->bComponentUseFixedSkelBounds = true;\
	WeaponModularHelper.InstalledModulesMesh.Last()->SetVisibility(false, true);\
}

#define DECLARE_FWeaponModularHelper() \
UPROPERTY()\
FWeaponModularHelper WeaponModularHelper;\
void ClearModules() { WeaponModularHelper.ClearModules(); }\
void InitializeModules(const UItemWeaponEntity* Source, const bool IsUseSlot = false) { WeaponModularHelper.InitializeModules(Source, IsUseSlot); }\
void InitializeModules(const UItemWeaponEntity* Source, const TArray<TEnumAsByte<ItemModuleModelId::Type>>& Modules, const TEnumAsByte<ItemMaterialModelId::Type>& Skin = ItemMaterialModelId::Default) { WeaponModularHelper.InitializeModules(Source, Modules, Skin); }\
bool InstallModule(const UItemModuleEntity* InModule, const ItemSlotTypeId::Type& Slot = ItemSlotTypeId::None) { return WeaponModularHelper.InstallModule(InModule, Slot); }\
bool UninstallModule(const UItemModuleEntity* InModule, const ItemSlotTypeId::Type& Slot = ItemSlotTypeId::None) { return WeaponModularHelper.UninstallModule(InModule, Slot); }