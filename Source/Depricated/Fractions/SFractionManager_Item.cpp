// VRSPRO

#include "LokaGame.h"
#include "SFractionManager_Item.h"
#include "SlateOptMacros.h"
#include "Components/SLokaToolTip.h"
#include "Layout/SScaleBox.h"
#include "Components/SItemProperty.h"
#include "Character/CharacterBaseEntity.h"

#include "Item/ItemBaseEntity.h"

#include "Styles/FractionManagerWidgetStyle.h"

#define LOCTEXT_NAMESPACE "SFractionManager"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SFractionManager_Item::Construct(const FArguments& InArgs, const UItemBaseEntity* InInstance)
{
	Instance = InInstance;
	Style = InArgs._Style;
	OnClickedItem = InArgs._OnClickedItem;
	Character = InArgs._Character;

	SButton::Construct(
		SButton::FArguments()
		.ButtonStyle(&Style->Item.Button)
		.IsEnabled(this, &SFractionManager_Item::GetIsEnabled)
		.ContentPadding(FMargin(0))
		[
			SNew(SBox)
			.WidthOverride(220)
			.HeightOverride(100)
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot()
				[
					SNew(SOverlay)
					+ SOverlay::Slot()
					[
						SNew(SScaleBox)
						.VAlign(VAlign_Center)
						.HAlign(HAlign_Center)
						.Stretch(EStretch::ScaleToFit)
						[
							SNew(SImage)
							.Image(this, &SFractionManager_Item::GetImage)
						]
					]
					+ SOverlay::Slot()
					.HAlign(HAlign_Left)
					.VAlign(VAlign_Top)
					.Padding(0)
					[
						SNew(SBorder)
						.BorderImage(&Style->Item.NameBorder)
						.Padding(FMargin(8, 2))
						[
							SNew(STextBlock)
							.Text(this, &SFractionManager_Item::GetTextName)
							.TextStyle(&Style->Item.NameText)
						]
					]
				]
				+ SVerticalBox::Slot().AutoHeight()
				//+ SOverlay::Slot()
				//.HAlign(HAlign_Fill)
				//.VAlign(VAlign_Bottom)
				[
					SAssignNew(Border_Buy, SBorder)
					.BorderImage(&Style->Item.BuyBorder)
					//.Visibility(this, &SFractionManager_Item::GetBorderBuyVisibility)
					.Visibility_Lambda([&]() { return (Instance && Instance->GetDescription().CategoryType != CategoryTypeId::Addon) ? EVisibility::Visible : EVisibility::Hidden; })
					.Padding(FMargin(8, 2))
					.HAlign(HAlign_Fill)
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot()
						[
							SNew(STextBlock)
							.Text(this, &SFractionManager_Item::GetTextBuy)
							.Justification(ETextJustify::Center)
							.Font(Style->Item.BuyText)
							.ColorAndOpacity(this, &SFractionManager_Item::GetTextBuyColor)
						]
						+ SVerticalBox::Slot().HAlign(HAlign_Center)
						[
							SNew(SResourceTextBox).CustomSize(FVector(16, 16, 12))
							.Type(this, &SFractionManager_Item::GetCurrencyType)
							.Value(this, &SFractionManager_Item::GetCurrencyValue)
							.Visibility(this, &SFractionManager_Item::GetCurrencyVisibility)
						]						
					]
				]
			]
		]
	);

	if (Instance == nullptr || (Instance && Instance->GetSkeletalMesh() == nullptr))
	{
		SetVisibility(EVisibility::Hidden);
	}
	else
	{
		auto ToolTipContent = SNew(SVerticalBox)
		+ SVerticalBox::Slot().AutoHeight()
		[
			SNew(STextBlock).Text(Instance->GetDescription().Description)
		]
		+ SVerticalBox::Slot().AutoHeight()
		[
			SNew(SItemProperty, Instance) // Params list
		];

		if (Instance && Instance->GetDescription().CategoryType != CategoryTypeId::Addon)
		{
			this->SetOnClicked(FOnClicked::CreateSP(this, &SFractionManager_Item::OnClickedEvent));
			this->SetToolTip(SNew(SLokaToolTip).Content()[ToolTipContent]);
		}
		else
		{
			this->SetToolTip(SNew(SLokaToolTip).Text(NSLOCTEXT("SFractionManager_Item", "SFractionManager_Item.Addon.Tip", "For buy or install, available only on modification screen from hangar.")));
		}

		SetVisibility(TAttribute<EVisibility>(this, &SFractionManager_Item::GetVisibilityByCharacter));
	}	

	IsHovered = false;
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SFractionManager_Item::OnMouseEnter(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
	SButton::OnMouseEnter(MyGeometry, MouseEvent);
	IsHovered = true;
}

void SFractionManager_Item::OnMouseLeave(const FPointerEvent& MouseEvent)
{
	SButton::OnMouseLeave(MouseEvent);
	IsHovered = false;
}

FReply SFractionManager_Item::OnClickedEvent()
{
	//TODO: SeNTIke
	//if (Instance && Instance->IsLocked())
	//{
	//	if (Instance->GetDescription().CategoryType == CategoryTypeId::Addon)
	//	{
	//
	//	}
	//	else
	//	{
	//		OnClickedItem.ExecuteIfBound(FUnlockItemRequestContainer(Instance->GetModelId(), Instance->GetDescription().CategoryType));
	//	}
	//	return FReply::Handled();
	//}

	return FReply::Unhandled();
}

//==========================================================================================] Attributes

bool SFractionManager_Item::GetIsEnabled() const
{
	if (Instance)
	{
		return true;
	}

	return false;
}

const FSlateBrush* SFractionManager_Item::GetImage() const
{
	if (Instance)
	{
		return Instance->GetImage();
	}

	return new FSlateNoResource();
}

FText SFractionManager_Item::GetTextName() const
{
	//TODO: SeNTIke
//	if (Instance)
//	{
//#if WITH_EDITOR
//		return FText::Format(FText::FromString("M:{0} C:{1}"), FText::AsNumber(Instance->GetModelId()), FText::AsNumber(Instance->GetDescription().CategoryType));
//#else
//		return Instance->GetDescription().Name;
//#endif
//	}

	return FText::GetEmpty();
}

FText SFractionManager_Item::GetTextBuy() const
{
	//TODO: SeNTIke
	//if (Instance)
	//{
	//	return Instance->IsLocked() ? LOCTEXT("Item.Buy", "BUY") : LOCTEXT("Item.Purchased", "PURCHASED");
	//}

	return FText::GetEmpty();
}

FSlateColor SFractionManager_Item::GetTextBuyColor() const
{
	//TODO: SeNTIke
	//if (Instance && Instance->IsLocked())
	//{
	//	return Style->Item.BuyTextColor;
	//}

	return Style->Item.BuyedTextColor;
}

EResourceTextBoxType::Type SFractionManager_Item::GetCurrencyType() const
{
	//TODO: SeNTIke
	//if (Instance)
	//{
	//	return Instance->GetCost().IsDonate ? EResourceTextBoxType::Donate : EResourceTextBoxType::Money;
	//}

	return EResourceTextBoxType::Custom;
}

int32 SFractionManager_Item::GetCurrencyValue() const
{
	//TODO: SeNTIke
	//if (Instance)
	//{
	//	return Instance->GetCost().Amount;
	//}

	return 0;
}

EVisibility SFractionManager_Item::GetCurrencyVisibility() const
{
	return EVisibility::Visible;

	//if (Instance && Instance->IsLocked())
	//{
	//	return EVisibility::Visible;
	//}

	//return EVisibility::Hidden;
}

EVisibility SFractionManager_Item::GetBorderBuyVisibility() const
{
	//TODO: SeNTIke
	//if (Instance && IsEnabled())
	//{
	//	if (IsHovered || !Instance->IsLocked())
	//	{
	//		return EVisibility::HitTestInvisible;
	//	}
	//}

	return EVisibility::Hidden;
}

EVisibility SFractionManager_Item::GetVisibilityByCharacter() const
{
	//TODO: SeNTIke
	//if (Instance && Character.IsSet() && Character.Get())
	//{
	//	return Instance->IsAllowCharacter(Character.Get()->GetInstanceModelId()) ? EVisibility::Visible : EVisibility::Collapsed;
	//}

	return EVisibility::Visible;
}

#undef LOCTEXT_NAMESPACE
