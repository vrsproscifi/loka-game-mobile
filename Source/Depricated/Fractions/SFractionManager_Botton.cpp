// VRSPRO

#include "LokaGame.h"
#include "SFractionManager_Botton.h"
#include "SlateOptMacros.h"

#include "Layout/SScaleBox.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SFractionManager_Botton::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	SAssignNew(MenuContent, SBorder)
	.BorderImage(&Style->Button.Background)
	.Padding(0)
	[
		SNew(SBorder)
		.BorderImage(&Style->Button.Border)
		.Padding(4)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Fill).VAlign(VAlign_Center).Padding(FMargin(0, 2))
			[
				SNew(STextBlock).Text(InArgs._Name)
				.TextStyle(&Style->Button.TextName)
				.Justification(ETextJustify::Center)
			]
			+ SVerticalBox::Slot().AutoHeight()
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().AutoWidth().Padding(FMargin(2, 0))
				[
					SNew(SButton).Text(NSLOCTEXT("SFractionManager_Botton", "Information", "Information"))
					.TextStyle(&Style->Button.TextInformation)
					.ButtonStyle(&Style->Button.ButtonInformation)
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					.ContentPadding(FMargin(10, 2))
					.OnClicked(InArgs._OnClickedInformation)
				]
				+ SHorizontalBox::Slot().AutoWidth().Padding(FMargin(2, 0))
				[
					SNew(SButton).Text(NSLOCTEXT("SFractionManager_Botton", "Join", "Join"))
					.TextStyle(&Style->Button.TextJoin)
					.ButtonStyle(&Style->Button.ButtonJoin)
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					.ContentPadding(FMargin(10, 2))
					.OnClicked(InArgs._OnClickedJoin)
				]
			]
		]
	];

	ChildSlot
	[
		SAssignNew(Menu, SMenuAnchor)
		.Method(EPopupMethod::UseCurrentWindow)
		.Placement(EMenuPlacement::MenuPlacement_CenteredBelowAnchor)
		.Visibility(EVisibility::SelfHitTestInvisible)
		.MenuContent(MenuContent)
		[
			SNew(SCheckBox)
			.Type(ESlateCheckBoxType::ToggleButton)
			.Style(&Style->Button.Button)
			.IsChecked(InArgs._IsChecked)
			.OnCheckStateChanged(InArgs._OnCheckStateChanged)
			.Padding(FMargin(10, 4))
			[
				SNew(SScaleBox)
				.HAlign(HAlign_Center)
				.VAlign(VAlign_Center)
				.Stretch(EStretch::ScaleToFit)
				[
					SNew(SImage)
					.Image(InArgs._Image)
				]
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SFractionManager_Botton::OnMouseEnter(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
	SCompoundWidget::OnMouseEnter(MyGeometry, MouseEvent);
	Menu->SetIsOpen(true);
}

void SFractionManager_Botton::OnMouseLeave(const FPointerEvent& MouseEvent)
{
	SCompoundWidget::OnMouseLeave(MouseEvent);
	Menu->SetIsOpen(false);
}