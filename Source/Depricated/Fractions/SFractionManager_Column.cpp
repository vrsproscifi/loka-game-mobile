// VRSPRO

#include "LokaGame.h"
#include "SFractionManager_Column.h"
#include "SlateOptMacros.h"

#include "Components/SLokaToolTip.h"
#include "Fraction/FractionEntity.h"

#include "SFractionManager_Item.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SFractionManager_Column::Construct(const FArguments& InArgs, const UFractionEntity* InInstance)
{
	SAssignNew(NullItem, SFractionManager_Item, nullptr);

	Style				= InArgs._Style;	
	OnClickedItem		= InArgs._OnClickedItem;
	Instance			= InInstance;
	TargetLevel			= InArgs._TargetLevel;

	SAssignNew(ItemsList, STileView<UItemBaseEntity*>)
		.SelectionMode(ESelectionMode::None)
		.ListItemsSource(&ItemsList_Source)
		.ItemWidth(220)
		.ItemHeight(160)
		.ItemAlignment(EListItemAlignment::Fill)
		.OnGenerateTile_Lambda([&](UItemBaseEntity* Item, const TSharedRef<STableViewBase>& Table) {
		//TODO: SeNTike	
		//if (Item)
			//{
			//	auto TargetLambda = [&, i = Item]() { return (Instance->Progress.CurrentRank >= TargetLevel || !i->IsLocked()); };
			//	return SNew(STableRow<UItemBaseEntity*>, Table).Padding(FMargin(4, 6))
			//	[
			//		SNew(SFractionManager_Item, Item).OnClickedItem(OnClickedItem).IsEnabled_Lambda(TargetLambda)
			//	];
			//}

			return SNew(STableRow<UItemBaseEntity*>, Table).Padding(FMargin(4, 6))[SNew(SFractionManager_Item, Item)];
		});

	ChildSlot
	[
		SNew(SOverlay)
		+ SOverlay::Slot()
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight().VAlign(VAlign_Center).Padding(FMargin(0, 2))
			[
				SNew(STextBlock).Text(Instance->GetRank(TargetLevel - 1)->Name)
				.Justification(ETextJustify::Center)
				.TextStyle(&Style->Column.Name)
			]
			+ SVerticalBox::Slot().AutoHeight().VAlign(VAlign_Center).Padding(FMargin(0, 2))
			[
				SNew(SBox).MaxDesiredHeight(20)
				[
					SNew(SOverlay)
					+ SOverlay::Slot()
					[
						SNew(SProgressBar).ToolTip(SNew(SLokaToolTip).Text(this, &SFractionManager_Column::GetProgressText))
						.BarFillType(EProgressBarFillType::LeftToRight)
						.Percent(this, &SFractionManager_Column::GetProgressPercent)
						.Style(&Style->Column.ProgressBar)
					]
					+ SOverlay::Slot().VAlign(VAlign_Center)
					[
						SNew(STextBlock)
						.Text(NSLOCTEXT("SFractionManager_Column", "SFractionManager_Column.Locked", "Up this rank to unlock items for buy."))
						.TextStyle(&Style->Column.LockedText)
						.Justification(ETextJustify::Center)
						.AutoWrapText(true)
						//.Visibility_Lambda([&]() { return (Instance->Progress.CurrentRank >= TargetLevel) ? EVisibility::Hidden : EVisibility::HitTestInvisible; })
					]
				]
			]
			+ SVerticalBox::Slot().FillHeight(1)
			[
				ItemsList.ToSharedRef()
			]
			+ SVerticalBox::Slot().AutoHeight()
			[
				SNew(SGridPanel).FillColumn(0, 1.0f).FillColumn(1, 1.0f)
				+ SGridPanel::Slot(0, 0).Expose(Slot_ItemsDonate[0]) // Any item
				+ SGridPanel::Slot(1, 0).Expose(Slot_ItemsDonate[1]) // Any item
				+ SGridPanel::Slot(0, 1).Expose(Slot_ItemsDonate[2]) // Weapon one
				+ SGridPanel::Slot(1, 1).Expose(Slot_ItemsDonate[3]) // Weapon two
			]
		]
		//+ SOverlay::Slot().HAlign(HAlign_Center).VAlign(VAlign_Center)
		//[

		//]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

TOptional<float> SFractionManager_Column::GetProgressPercent() const
{
	//if (Instance->Progress.CurrentRank == TargetLevel - 1)
	//{
	//	return float(Instance->Progress.Reputation) / float(Instance->Progress.NextReputation);
	//}
	//else if (Instance->Progress.CurrentRank < TargetLevel)
	//{
	//	return .0f;
	//}
	
	return 1.0f;
}

FText SFractionManager_Column::GetProgressText() const
{
	//if (Instance->Progress.CurrentRank == TargetLevel - 1)
	//{
	//	return FText::Format(NSLOCTEXT("SLobbyContainer", "Lobby.Reputation", "Reputation: {0}/{1}"), FText::AsNumber(Instance->Progress.Reputation), FText::AsNumber(Instance->Progress.NextReputation));
	//}
	//else if (Instance->Progress.CurrentRank < TargetLevel)
	//{
	//	return NSLOCTEXT("SFractionManager_Column", "SFractionManager_Column.NotStart", "Not started");
	//}

	return NSLOCTEXT("SFractionManager_Column", "SFractionManager_Column.Complete", "Complete");
}

void SFractionManager_Column::FillList(const TArray<UItemBaseEntity*> Items)
{
	ItemsList_Source = Items;
	ItemsList_Source.Sort([](const UItemBaseEntity& A, const UItemBaseEntity& B) { return A.GetDescription().CategoryType < B.GetDescription().CategoryType; });

	if (ItemsList_Source.Num() == 1)
	{
		ItemsList_Source.Add(nullptr);
	}
	
	ItemsList->RequestListRefresh();
}
