// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/FractionManagerWidgetStyle.h"
#include "StoreController/Operation/UnlockItem.h"
#include "Item/ItemBaseEntity.h"

namespace ItemSlotTypeId{
	enum Type;
}

class UFractionEntity;
/**
 * 
 */
class LOKAGAME_API SFractionManager_Column : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SFractionManager_Column)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FFractionManagerStyle>(TEXT("SFractionManagerStyle")))
		, _TargetLevel(1)
	{}
	SLATE_ARGUMENT(int32, TargetLevel)
	SLATE_EVENT(Operation::FOnUnlockItem, OnClickedItem)
	SLATE_STYLE_ARGUMENT(FFractionManagerStyle, Style)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, const UFractionEntity* InInstance);

	void FillList(const TArray<UItemBaseEntity*>);

protected:

	const FFractionManagerStyle* Style;
	const UFractionEntity* Instance;
	int32 TargetLevel;

	Operation::FOnUnlockItem OnClickedItem;
	SGridPanel::FSlot* Slot_ItemsDonate[4];

	TAttribute<float> CurrentReputation, MaxReputation;

	TOptional<float> GetProgressPercent() const;
	FText GetProgressText() const;

	TSharedPtr<class SFractionManager_Item> NullItem;
	TSharedPtr<STileView<UItemBaseEntity*>> ItemsList;
	TArray<UItemBaseEntity*> ItemsList_Source;
};
