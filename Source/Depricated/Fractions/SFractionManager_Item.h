// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Components/SResourceTextBox.h"
#include "StoreController/Operation/UnlockItem.h"

class UItemBaseEntity;
class UCharacterBaseEntity;
/**
 * 
 */
class LOKAGAME_API SFractionManager_Item : public SButton
{
public:
	SLATE_BEGIN_ARGS(SFractionManager_Item)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<struct FFractionManagerStyle>(TEXT("SFractionManagerStyle")))
	{}
	SLATE_STYLE_ARGUMENT(struct FFractionManagerStyle, Style)
	SLATE_EVENT(Operation::FOnUnlockItem, OnClickedItem)
	SLATE_ATTRIBUTE(const UCharacterBaseEntity*, Character)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs, const UItemBaseEntity* InInstance);

protected:

	const struct FFractionManagerStyle* Style;

	const UItemBaseEntity* Instance;
	TAttribute<const UCharacterBaseEntity*> Character;

	TSharedPtr<SBorder> Border_Buy;
	bool IsHovered;

	virtual void OnMouseEnter(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;
	virtual void OnMouseLeave(const FPointerEvent& MouseEvent) override;

	FReply OnClickedEvent();

	Operation::FOnUnlockItem OnClickedItem;

	//==========================================================================================] Attributes

	bool GetIsEnabled() const;
	const FSlateBrush* GetImage() const;

	FText GetTextName() const;
	FText GetTextBuy() const;

	FSlateColor GetTextBuyColor() const;

	EResourceTextBoxType::Type GetCurrencyType() const;
	int32 GetCurrencyValue() const;
	EVisibility GetCurrencyVisibility() const;
	EVisibility GetBorderBuyVisibility() const;
	EVisibility GetVisibilityByCharacter() const;
};
