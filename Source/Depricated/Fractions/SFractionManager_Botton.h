// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/FractionManagerWidgetStyle.h"
/**
 * 
 */
class LOKAGAME_API SFractionManager_Botton : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SFractionManager_Botton)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FFractionManagerStyle>(TEXT("SFractionManagerStyle")))
		, _Image(new FSlateNoResource())
	{}
	SLATE_STYLE_ARGUMENT(FFractionManagerStyle, Style)
	SLATE_EVENT(FOnCheckStateChanged, OnCheckStateChanged)
	SLATE_ATTRIBUTE(ECheckBoxState, IsChecked)
	SLATE_ATTRIBUTE(const FSlateBrush*, Image)
	SLATE_ATTRIBUTE(FText, Name)
	SLATE_EVENT(FOnClicked, OnClickedInformation)
	SLATE_EVENT(FOnClicked, OnClickedJoin)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

protected:

	const FFractionManagerStyle* Style;

	TSharedPtr<SBorder> MenuContent;
	TSharedPtr<SMenuAnchor> Menu;

	virtual void OnMouseEnter(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;
	virtual void OnMouseLeave(const FPointerEvent& MouseEvent) override;
};
