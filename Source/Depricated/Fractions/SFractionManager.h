// VRSPRO

#pragma once

#include <Character/CharacterBaseEntity.h>
#include "Widgets/SCompoundWidget.h"
#include "Styles/FractionManagerWidgetStyle.h"
#include "StoreController/Operation/UnlockItem.h"
#include "IdentityController/Operation/OnSwitchFraction.h"

class UFractionEntity;

class LOKAGAME_API SFractionManager : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SFractionManager)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FFractionManagerStyle>(TEXT("SFractionManagerStyle")))
	{}
	SLATE_STYLE_ARGUMENT(FFractionManagerStyle, Style)
	SLATE_EVENT(Operation::FOnUnlockItem, OnClickedItem)
	SLATE_EVENT(Operation::OnSwitchFraction::FOnSwitchFraction, OnFractionSelect)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

	void AddFraction(const UFractionEntity*);
	bool SetActiveFraction(const UFractionEntity*);
	bool RemoveFraction(const UFractionEntity*);

	void AddCharacter(UCharacterBaseEntity*);
	Operation::FOnUnlockItem OnClickedItem;

protected:

	const FFractionManagerStyle* Style;

	Operation::OnSwitchFraction::FOnSwitchFraction OnFractionSelect;

	TSharedPtr<SWidgetSwitcher> Switcher;
	TSharedPtr<SHorizontalBox> ButtonsContainer;	

	TSharedPtr<SListView<UCharacterBaseEntity*>> Sort_Characters;
	TArray<UCharacterBaseEntity*> List_Characters;

	void OnSetCurrentCharacter(UCharacterBaseEntity* Character, ESelectInfo::Type Info);

	struct FractionContainer
	{
		const UFractionEntity* Instance;
		TSharedPtr<class SFractionManager_Botton> ToggleButton;
		TSharedPtr<SScrollBox> ScrollBox;
		TArray<TSharedPtr<class SFractionManager_Column>> Columns;
		float CachedScrollOffset;
		int32 CachedScrollIndex;
	};

	TArray<FractionContainer> Fractions;

	void OnScrolledFraction(float, const int32);

	int32 GetActiveFractionIndex() const;
	ECheckBoxState IsActiveFractionIndex(const int32) const;
	void OnActiveFractionIndex(ECheckBoxState, const int32);

	int32 CurrentFraction, CurrentCharacter;
};
