#pragma once

#include "SearchSessionParams.generated.h"
USTRUCT()
struct FSearchSessionParams
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	int32 GameModeTypeId;

	UPROPERTY()
	int32 GameModeAdditionalFlag;

	UPROPERTY()
	int32 RegionTypeId;

	FSearchSessionParams() : GameModeTypeId(0), GameModeAdditionalFlag(0), RegionTypeId(0) {}
};