#pragma once
#include "ServiceOperation.h"
#include "RequestOneParam.h"
#include "../Models/LobbyMatchResultPreview.h"
#include "PoolingOperation.h"

namespace Operation
{
	namespace GetMatchResults
	{
		class Operation final 
			:// public COperation<void, ObjectSerializer::NativeObject, TArray<FLobbyMatchResultPreview>, ObjectSerializer::JsonArrayObject>
			// , public FPoolingOperation
		{
		public:
			Operation() 
				: Super(FServerHost::MasterClient, "Session/GetMatchResults", FVerb::Get) 
				, FPoolingOperation(15.0f, 2.0f)
			{
				this->TimerDelegate.BindRaw(this, &Operation::Request);
			}

		};
	}
}