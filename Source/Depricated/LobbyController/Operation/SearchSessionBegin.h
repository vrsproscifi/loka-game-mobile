#pragma once
#include "ServiceHandler.h"
#include "ServiceOperation.h"
#include "SearchSessionParams.h"

namespace Operation
{
	namespace SearchSessionBegin
	{
		namespace SessionSearchBeginExceptionCode
		{
			enum SessionSearchBeginExceptionCode
			{
				NotHavePrimaryWeapon = 900,
				NotHaveEnabledProfiles,
				GameModeIsNotAvailable,
				MembersNotReady,
				SmallPvEMatchLevel,
				SmallPlayerLevel,
				ErrorOnDataBase
			};
		}

		class Operation final :// public COperation<FSearchSessionParams, ObjectSerializer::JsonObject, void, ObjectSerializer::NativeObject>
		{
		public:
			IResponseHandler<void, OperationResponseType::None> OnGameModeIsNotAvailable;

			//  if even one set does not have a primary weapon
			IResponseHandler<void, OperationResponseType::None> OnWeaponEquipRequired;

			//  if the player does not have enabled set
			IResponseHandler<void, OperationResponseType::None> OnNoAvalibleProfiles;

			IResponseHandler<void, OperationResponseType::None> OnMembersNotReady;

			IResponseHandler<void, OperationResponseType::None> OnSmallPvEMatchLevel;

			IResponseHandler<void, OperationResponseType::None> OnSmallPlayerLevel;


			Operation() 
				: Super(FServerHost::MasterClient, "Session/SearchBegin") 
				, OnGameModeIsNotAvailable(SessionSearchBeginExceptionCode::GameModeIsNotAvailable, this)
				, OnWeaponEquipRequired(SessionSearchBeginExceptionCode::NotHavePrimaryWeapon, this)
				, OnNoAvalibleProfiles(SessionSearchBeginExceptionCode::NotHaveEnabledProfiles, this)

				, OnMembersNotReady(SessionSearchBeginExceptionCode::MembersNotReady, this)
				, OnSmallPvEMatchLevel(SessionSearchBeginExceptionCode::SmallPvEMatchLevel, this)
				, OnSmallPlayerLevel(SessionSearchBeginExceptionCode::SmallPlayerLevel, this)

			{

			}

		};
	}
}
