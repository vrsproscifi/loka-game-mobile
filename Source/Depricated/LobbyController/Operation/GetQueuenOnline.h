#pragma once
#include "ServiceHandler.h"
#include "ServiceOperation.h"
#include "../Models/QueuenOnlineContainer.h"
#include "PoolingOperation.h"

namespace Operation
{
	namespace GetQueuenOnline
	{
		class Operation final 
			:// public COperation<void, ObjectSerializer::JsonObject, TArray<FQueuenOnlineContainer>, ObjectSerializer::JsonArrayObject, EOperationLogVerbosity::Error_Warning, false>
			// , public FPoolingOperation
		{
		public:			
			Operation() 
				: Super(FServerHost::MasterClient, "Session/GetQueuenOnline", FVerb::Get)
				, FPoolingOperation(10.0f, 2.5f)
			{
				TimerDelegate.BindRaw(this, &Operation::Request);
			}

		};
	}
}
