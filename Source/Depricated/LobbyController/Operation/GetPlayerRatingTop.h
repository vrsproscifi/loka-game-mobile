#pragma once
#include "ServiceHandler.h"
#include "ServiceOperation.h"
#include "../Models/PlayerRatingTopContainer.h"
#include "PoolingOperation.h"

namespace Operation
{
	namespace GetPlayerRatingTop
	{
		class Operation final 
			:// public COperation<void, ObjectSerializer::JsonObject, FPlayerRatingTopView, ObjectSerializer::JsonObject>
			// , public FPoolingOperation
		{
		public:			
			Operation() 
				: Super(FServerHost::MasterClient, "Session/GetPlayerRatingTop", FVerb::Get)
				, FPoolingOperation(30.0f, 2.5f)
			{
				TimerDelegate.BindRaw(this, &Operation::Request);
			}

		};
	}
}
