#pragma once
#include "ServiceOperation.h"
#include "RequestOneParam.h"
#include "../Models/LobbyMatchResultDetailed.h"

namespace Operation
{
	namespace GetMatchResult
	{
		class Operation final :// public COperation<FRequestOneParam<FString>, ObjectSerializer::NativeObject, FLobbyMatchResultDetailed, ObjectSerializer::JsonObject>
		{
		public:
			Operation() : Super(FServerHost::MasterClient, "Session/GetMatchResult") { }

		};
	}
}