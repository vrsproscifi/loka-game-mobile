#pragma once
#include "ServiceHandler.h"
#include "ServiceOperation.h"
#include "RequestOneParam.h"

namespace Operation
{
	namespace StartMission
	{
		class Operation final 
			: public COperation<FRequestOneParam<int32>, ObjectSerializer::NativeObject, void, ObjectSerializer::NativeObject>
		{
		public:			
			Operation() 
				: Super(FServerHost::MasterClient, "Company/StartMission")
			{
			}

		};
	}
}
