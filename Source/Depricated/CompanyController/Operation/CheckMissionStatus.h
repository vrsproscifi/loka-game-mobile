#pragma once
#include "ServiceHandler.h"
#include "ServiceOperation.h"


namespace Operation
{
	namespace CheckMissionStatus
	{
		class Operation final
			: public COperation<void, ObjectSerializer::NativeObject, void, ObjectSerializer::NativeObject>
		{
		public:
			Operation()
				: Super(FServerHost::MasterClient, "Company/CheckMissionStatus", FVerb::Get)
			{
			}

		};
	}
}
