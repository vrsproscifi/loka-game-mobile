#pragma once

#include "Operation/CancelMission.h"
#include "Operation/CheckMissionStatus.h"
#include "Operation/StartMission.h"

class CompanyController
{
public:
	CompanyController();
	
	Operation::StartMission::Operation				StartMission;
	Operation::CheckMissionStatus::Operation			CheckMissionStatus;
	Operation::CancelMission::Operation				CancelMission;
};
