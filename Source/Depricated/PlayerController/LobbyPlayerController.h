#pragma once

#include "BasePlayerController.h"
#include "Models/PlayerEntityInfo.h"
#include "HangarController/Models/PreSetEquip.h"
#include "HangarController/Models/AddonEquip.h"
#include "LobbyPlayerController.generated.h"

namespace EItemSetSlotId { enum Type; }

class ALobbyPlayerCharacter;
class UCharacterBaseEntity;
class UItemWeaponEntity;
class UItemArmourEntity;
class UItemMaterialEntity;
class UItemModuleEntity;
enum class ELobbyActionButton : uint8;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChangeSetCharacter, uint8, Clicked);

DECLARE_DELEGATE_OneParam(FOnPreSetItemEquip, const FPreSetEquipRequest&);

UCLASS()
class LOKAGAME_API ALobbyPlayerController 
	: public ABasePlayerController
{
	GENERATED_BODY()

	UFUNCTION()
	void OnSwitchCharacter(const UCharacterBaseEntity* characterEntity) const;

	UPROPERTY()
	TEnumAsByte<EPlayerPreSetId::Type> CurrentPrEItemSetSlotIdId;

	virtual void OnSwitchProfile(const EPlayerPreSetId::Type& preSetId) override;


	UFUNCTION()
	void OnConfirmCharacterSelect(const UCharacterBaseEntity* characterEntity);

	UFUNCTION()
	void OnCancelCharacterSelect() const;

	UFUNCTION()
	void OnItemModify(const class UItemBaseEntity* Item);

	void OnProxyItemEquip(const UItemBaseEntity* item, const EItemSetSlotId::Type& itemSetSlot, const EPlayerPreSetId::Type& setIndex) const;
	void OnProxyItemUnEquip(const UItemBaseEntity* item, const EItemSetSlotId::Type& itemSetSlot, const EPlayerPreSetId::Type& setIndex) const;
	void OnProxyItemUnEquip(const EItemSetSlotId::Type itemSetSlot, const EPlayerPreSetId::Type setIndex) const;

	UFUNCTION()
	void OnItemChangeMaterial(const UItemBaseEntity* item, const UItemMaterialEntity* mat); 
	void OnItemBuyMaterial(const UItemBaseEntity* item, const UItemMaterialEntity* mat);

	void OnItemChangeModule(UItemModuleEntity* InModule);
	

public:

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	void OnItemInstallFromList(const class UItemBaseEntity* Item);
	void OnEquipAddonResponse(const Operation::AddonEquip::Response& response);


	void OnConfirmedItemUpgrade(const struct FInvertoryItemWrapper& Item);

	FOnPreSetItemEquip OnPreSetItemEquipDelegate;
	FOnPreSetItemEquip OnPreSetItemUnEquipDelegate;

	void OnPreSetItemEquip(const FPreSetEquipResponse& response) const;
	void OnPreSetItemUnEquip(const FPreSetEquipResponse& response) const;

	UPROPERTY()
		FTransform SpawnCharacterLocation;

	FActorSpawnParameters SpawnCharacterParameters;

	FOnChangeSetCharacter OnChangeSetCharacter;

	void InitializUi() override;

	void InitializePreSet(const EPlayerPreSetId::Type& profileId, const FClientPreSetData& property);

	void InitializePreSetList(const TArray<FClientPreSetData>& preSetList) override;

	void InitializePreSet() override;

	FORCEINLINE void SetEntityInfo(const FPlayerEntityInfo& entityInfo)
	{
		FGuid playerId;
		auto isValid = FGuid::Parse(entityInfo.PlayerId, playerId);
		ensureAlwaysMsgf(isValid, ANSI_TO_TCHAR("Invalid playerId"));
		if(isValid)
		{
			//SetEntityId(playerId);
			FPlayerEntityInfo::Instance = entityInfo;

			//// Test info
			//EntityInfo.Cash.Money = 56000;
			//EntityInfo.Cash.Donate = 500;
			//EntityInfo.Experience.Experience = 500;
			//EntityInfo.Experience.NextExperience = 1400;
			//EntityInfo.Experience.Level = 1;
		}
	}

public:
	TSharedPtr<class SCharacterManager> Slate_CharacterManager;
	TSharedPtr<class SWeaponModularManager> Slate_WeaponModularManager;

	UFUNCTION()
	ALobbyPlayerCharacter* GetCurrentPreSet() const;

	UFUNCTION()
	ALobbyPlayerCharacter* GetPreSet(const TEnumAsByte<EPlayerPreSetId::Type>& setId) const;

	UFUNCTION()
	ALobbyPlayerCharacter* GetPreSetById(const FGuid& setId) const;

	UFUNCTION()
	ALobbyPlayerCharacter* GetPreSetByStrId(const FString& setId) const;


	void SelectSet(const TEnumAsByte<EPlayerPreSetId::Type>& setId);

	UItemBaseEntity* AddItem(const FGuid& Index, UItemBaseEntity* Instance);

	ALobbyPlayerController();


public:
	UPROPERTY()
	class ALobbyCamera* LobbyCameraRef;

	UPROPERTY()
	class AWeaponModular* WeaponModularRef;

	bool bIsWeaponMode;

public:
	UFUNCTION()
	void SetCameraRef(class ALobbyCamera* camera);
	void SetModularRef(class AWeaponModular*);

protected:
	UPROPERTY()
	ALobbyPlayerCharacter* LobbyPlayerCharacterRef;

public:
	UFUNCTION()
	void SetLobbyPlayerCharacter(ALobbyPlayerCharacter* lobbyCharacter)
	{
		LobbyPlayerCharacterRef = lobbyCharacter;
	}
	
	UFUNCTION()
	ALobbyPlayerCharacter* GetLobbyPlayerCharacter() const
	{
		return LobbyPlayerCharacterRef;
	}


public:
	virtual void SetupInputComponent() override;
	virtual void AddPitchInput(float Val) override;
	virtual void AddYawInput(float Val) override;
	void OnNextScroll();
	void OnPrevScroll();	

	void OnSwitchedLobbySlide(const ELobbyActionButton&, const ELobbyActionButton&);
	virtual	void InitializeInventory(const struct FLobbyClientLootData& response) override;

	const FSlateBrush* GetProfileIcon(const EPlayerPreSetId::Type) const;

protected:	

	FReply OnRevertCharacterMode();
	FVector TargetLocation;
};
