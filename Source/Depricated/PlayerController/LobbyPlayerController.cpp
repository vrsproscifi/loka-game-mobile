// VRSPRO

#include "LokaGame.h"
#include "Character/CharacterBaseEntity.h"
#include "PlayerCharacter/LobbyPlayerCharacter.h"
#include "LobbyCamera.h"
#include "Weapon/WeaponModular.h"
#include "LobbyPlayerController.h"
#include "Slate/Character/SCharacterManager.h"
#include "GameMode/LobbyGameMode.h"
#include "Slate/Lobby/SLobbyContainer.h"
#include "Weapon/SWeaponModularManager.h"
#include <LokaGame/PlayerCharacter/PlayerCharacterInstance.h>
#include <Weapon/ItemWeaponEntity.h>
#include "Module/ItemModuleEntity.h"
#include "Material/ItemMaterialEntity.h"
#include <Armour/ItemArmourEntity.h>
#include "Module/ItemModuleEntity.h"
#include "Grenade/ItemGrenadeEntity.h"
#include "Service/ItemServiceEntity.h"
#include "Marketing/Lobby/LobbyActionView.h"

#include <Item/ItemSetSlotId.h>
#include "CharacterAbility.h"

ALobbyPlayerController::ALobbyPlayerController()
	: Super()
	, LobbyCameraRef(nullptr)
	, WeaponModularRef(nullptr)
	, LobbyPlayerCharacterRef(nullptr)
	, CurrentPrEItemSetSlotIdId(EPlayerPreSetId::One)
{
	SpawnCharacterParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableMouseOverEvents = true;

	bIsWeaponMode = false;
}

void ALobbyPlayerController::OnPreSetItemUnEquip(const FPreSetEquipResponse& response) const
{
	if (ALobbyPlayerCharacter *targetPreSet = GetPreSetByStrId(response.PreSetId))
	{
		if (response.CategoryId == CategoryTypeId::Armour)
		{
			if (auto TargetItem = InventoryRepository.FindArmourById(response.ItemId))
			{
				targetPreSet->UnSetArmourEntity(TargetItem);
			}
		}
		else if (response.CategoryId == CategoryTypeId::Weapon)
		{
			if (auto TargetItem = InventoryRepository.FindWeaponById(response.ItemId))
			{
				targetPreSet->UnsetWeaponEntity(TargetItem);
			}
		}
		else if (response.CategoryId == CategoryTypeId::Grenade)
		{
			if (auto Grenade = InventoryRepository.FindGrenadeById(response.ItemId))
			{
				Slate_CharacterManager->GetSetManager()->UninstallSlot(static_cast<EItemSetSlotId::Type>(response.SetSlotId), CurrentPrEItemSetSlotIdId);
			}
		}
	}
}

void ALobbyPlayerController::OnPreSetItemEquip(const FPreSetEquipResponse& response) const
{
	if (ALobbyPlayerCharacter *targetPreSet = GetPreSetByStrId(response.PreSetId))
	{
		if (response.CategoryId == CategoryTypeId::Armour)
		{
			if (auto TargetItem = InventoryRepository.FindArmourById(response.ItemId))
			{
				targetPreSet->SetArmourEntity(TargetItem);
			}
		}
		else if (response.CategoryId == CategoryTypeId::Weapon)
		{
			if (auto TargetItem = InventoryRepository.FindWeaponById(response.ItemId))
			{
				targetPreSet->SetWeaponEntity(TargetItem);
			}
		}
		else if (response.CategoryId == CategoryTypeId::Grenade)
		{
			if (auto Grenade = InventoryRepository.FindGrenadeById(response.ItemId))
			{
				Slate_CharacterManager->GetSetManager()->InstallAsset(Grenade, CurrentPrEItemSetSlotIdId, static_cast<EItemSetSlotId::Type>(response.SetSlotId));
			}
		}
		else if (response.CategoryId == CategoryTypeId::Character)
		{
			if (auto Char = InventoryRepository.FindCharacterById(response.ItemId))
			{
				if (GetCurrentPreSet()->GetEntityInstance() != Char)
				{
					for (int i = 0; i < ItemSlotTypeId::End; i++)
					{
						GetCurrentPreSet()->UnSetArmourEntitySlot(static_cast<ItemSlotTypeId::Type>(i));
					}
				}

				GetCurrentPreSet()->ApplyEntity(Char);
			}
		}
		else if (response.CategoryId == CategoryTypeId::Service)
		{
			if (auto Service = InventoryRepository.FindServiceById(response.ItemId))
			{
				Service->SetCount(response.Count);

				if (response.Count <= 0 && Slate_CharacterManager.IsValid() && Slate_CharacterManager->GetInventoryManager() != SNullWidget::NullWidget)
				{
					Slate_CharacterManager->GetInventoryManager()->RemoveItem(Service);
				}
			}
		}
	}
}


void ALobbyPlayerController::OnProxyItemEquip(const UItemBaseEntity* item, const EItemSetSlotId::Type& itemSetSlot, const EPlayerPreSetId::Type& setIndex) const
{
	OnPreSetItemEquipDelegate.ExecuteIfBound(FPreSetEquipRequest(item->GetEntityId(), item->GetDescription().CategoryType, itemSetSlot, GetPreSet(setIndex)->GetEntityId(), true));
}

void ALobbyPlayerController::OnProxyItemUnEquip(const UItemBaseEntity* item, const EItemSetSlotId::Type& itemSetSlot, const EPlayerPreSetId::Type& setIndex) const
{
	OnPreSetItemUnEquipDelegate.ExecuteIfBound(FPreSetEquipRequest(item->GetEntityId(), item->GetDescription().CategoryType, itemSetSlot, GetPreSet(setIndex)->GetEntityId(), false));
}

void ALobbyPlayerController::OnProxyItemUnEquip(const EItemSetSlotId::Type itemSetSlot, const EPlayerPreSetId::Type setIndex) const
{
	CategoryTypeId::Type categoryType;
	if (itemSetSlot == ItemSlotTypeId::PrimaryWeapon || itemSetSlot == ItemSlotTypeId::SecondaryWeapon) categoryType = CategoryTypeId::Weapon;
	else categoryType = CategoryTypeId::Armour;
	OnPreSetItemUnEquipDelegate.ExecuteIfBound(FPreSetEquipRequest(FGuid(), categoryType, static_cast<EItemSetSlotId::Type>(itemSetSlot), GetPreSet(setIndex)->GetEntityId(), false));
}

ALobbyPlayerCharacter* ALobbyPlayerController::GetCurrentPreSet() const
{
	return GetPreSet(CurrentPrEItemSetSlotIdId);
}

ALobbyPlayerCharacter* ALobbyPlayerController::GetPreSet(const TEnumAsByte<EPlayerPreSetId::Type>& setId) const
{
	return CastChecked<ALobbyPlayerCharacter>(PreSetArray[setId]);
}

ALobbyPlayerCharacter* ALobbyPlayerController::GetPreSetByStrId(const FString& setId) const
{
	FGuid preSetId;
	if (FGuid::Parse(setId, preSetId))
		return GetPreSetById(preSetId);
	return nullptr;
}

ALobbyPlayerCharacter* ALobbyPlayerController::GetPreSetById(const FGuid& setId) const
{
	for(auto set : PreSetArray)
	{
		if (set && set->GetEntityId() == setId)
			return CastChecked<ALobbyPlayerCharacter>(set);
	}
	return nullptr;
}

void ALobbyPlayerController::SelectSet(const TEnumAsByte<EPlayerPreSetId::Type> & setId)
{
	Slate_CharacterManager->GetSetManager()->SetCurrentSet(setId);

	GetCurrentPreSet()->GetMesh()->SetVisibility(false);
	GetCurrentPreSet()->ToggleClickable(false);
	GetCurrentPreSet()->OnClickedCharacter.Unbind();
	CurrentPrEItemSetSlotIdId = setId;
	GetCurrentPreSet()->GetMesh()->SetVisibility(true);

	if (auto gm = GetWorld()->GetAuthGameMode<ALobbyGameMode>())
	{
		GetCurrentPreSet()->ToggleClickable(true);
		GetCurrentPreSet()->OnClickedCharacter.BindLambda([g = gm]() {
			g->Slate_LobbyContainer->SetActiveScrollablyWidget(ELobbyActionButton::Hangar);
		});
	}

	
	//Slate_CharacterManager->SetCharacter(GetCurrentPreSet()->player);
}

void ALobbyPlayerController::OnSwitchedLobbySlide(const ELobbyActionButton& Index, const ELobbyActionButton& OldIndex)
{
	if (auto gm = GetWorld()->GetAuthGameMode<ALobbyGameMode>())
	{
		if (gm->InstanceRepository->Character[0])
		{
			Slate_CharacterManager->SetCharacter(gm->InstanceRepository->Character[0]);
		}
	}	

	if (bIsWeaponMode && Index != ELobbyActionButton::Hangar)
	{
		OnRevertCharacterMode();
	}

	if (Index == ELobbyActionButton::Welcome)
	{
		GetCurrentPreSet()->ToggleClickable(true);
	}
	else
	{
		GetCurrentPreSet()->ToggleClickable(false);

		if (Index == ELobbyActionButton::Hangar)
		{
			for (auto &i : InventoryRepository.Weapon)
			{
				if (i && i->IsValidLowLevel())
				{
					i->CaptureThumbnail(GetWorld());

					for (auto &m : i->Modules)
					{
						for (auto &mk : m.Value.Array)
						{
							if (mk && mk->IsValidLowLevel())
							{
								mk->CaptureThumbnail(GetWorld());
							}
						}						
					}					
				}
			}

			for (auto &i : InventoryRepository.Armour)
			{
				if (i && i->IsValidLowLevel())
				{
					i->CaptureThumbnail(GetWorld());
				}
			}

			for (auto &i : InventoryRepository.Grenade)
			{
				if (i && i->IsValidLowLevel())
				{
					i->CaptureThumbnail(GetWorld());
				}
			}
		}
	}

	//if (InventoryRepository.Weapon[ItemWeaponModelId::MilitarySilver_AssaultRifle])
	//	InventoryRepository.Weapon[ItemWeaponModelId::MilitarySilver_AssaultRifle]->CaptureThumbnail(GetWorld());
}

void ALobbyPlayerController::OnSwitchProfile(const EPlayerPreSetId::Type & preSetId)
{
	OnCancelCharacterSelect();

	if (auto currentProfileInstance = PreSetArray[CurrentPrEItemSetSlotIdId])
	{
		currentProfileInstance->SetActorHiddenInGame(true);

		if (auto mesh = currentProfileInstance->GetMesh())
		{
			mesh->SetVisibility(false, true);
		}
	}

	if (auto targetProfileInstance = PreSetArray[preSetId])
	{
		targetProfileInstance->SetActorHiddenInGame(false);

		Slate_CharacterManager->SetCharacter(targetProfileInstance->GetEntityInstance());
		if (auto mesh = targetProfileInstance->GetMesh())
		{
			mesh->SetVisibility(true, true);
		}
	}

	CurrentPrEItemSetSlotIdId = preSetId;
}

void ALobbyPlayerController::OnSwitchCharacter(const UCharacterBaseEntity* characterEntity) const
{
	GetCurrentPreSet()->SwapEnity(characterEntity);
}

void ALobbyPlayerController::OnConfirmCharacterSelect(const UCharacterBaseEntity * characterEntity)
{
	OnProxyItemEquip(characterEntity, static_cast<EItemSetSlotId::Type>(characterEntity->GetDescription().SlotType.GetValue()), CurrentPrEItemSetSlotIdId);	
}

void ALobbyPlayerController::OnCancelCharacterSelect() const
{
	GetCurrentPreSet()->RestoreEnity();
}

void ALobbyPlayerController::OnItemModify(const UItemBaseEntity* Item)
{
	if (WeaponModularRef && LobbyCameraRef)
	{
		auto Weapon = Cast<UItemWeaponEntity>(Item);

		WeaponModularRef->SetInstance(Weapon);

		if (auto gm = GetWorld()->GetAuthGameMode<ALobbyGameMode>())
		{
			Slate_WeaponModularManager->ModularActor = WeaponModularRef;
			Slate_WeaponModularManager->SetInstance(Weapon);
			Slate_WeaponModularManager->OnGetSocketScreen.BindUObject(WeaponModularRef, &AWeaponModular::GetSocketToScreen);
			Slate_WeaponModularManager->OnSelectedModule.BindUObject(this, &ALobbyPlayerController::OnItemChangeModule);

			Slate_CharacterManager->GetModifyManager()->SetInstance(Weapon);
			Slate_CharacterManager->SwitchMode(true);
		}

		auto TestLocation = LobbyCameraRef->GetActorLocation() + LobbyCameraRef->GetSpringComponent()->GetForwardVector() * -160.0f;
		
		bIsWeaponMode = true;
		LobbyCameraRef->BeginActivateWM(TestLocation);
		WeaponModularRef->BeginActivate(TestLocation, FRotationMatrix::MakeFromX(LobbyCameraRef->GetCameraLocation() - TestLocation).Rotator());
	}
}

void ALobbyPlayerController::OnItemChangeMaterial(const UItemBaseEntity* item, const UItemMaterialEntity* mat)
{
	//TODO: SeNTIKE
	//if (item && mat)
	//{
	//	WeaponModularRef->BaseMesh->SetMaterial(0, item->GetMaterialByModel(mat->GetInstanceModelId()));
	//
	//	if (!mat->IsLocked())
	//	{
	//		if (auto gm = GetWorld()->GetAuthGameMode<ALobbyGameMode>())
	//		{
	//			gm->hangarController.AddonEquip.Request(Operation::AddonEquip::Request(item->GetEntityId(), mat->GetInstanceModelId(), AddonSlotSkin, CategoryTypeId::Material));
	//		}
	//	}
	//}
}

void ALobbyPlayerController::OnItemBuyMaterial(const UItemBaseEntity* item, const UItemMaterialEntity* mat)
{
	//TODO: SeNTIKE
	//if (item && mat)
	//{
	//	if (mat->IsLocked())
	//	{
	//		if (auto gm = GetWorld()->GetAuthGameMode<ALobbyGameMode>())
	//		{
	//			gm->hangarController.AddonEquip.Request(Operation::AddonEquip::Request(item->GetEntityId(), mat->GetInstanceModelId(), AddonSlotSkin));
	//		}
	//	}
	//}
}

void ALobbyPlayerController::OnItemChangeModule(UItemModuleEntity* InModule)
{
	if (InModule && WeaponModularRef->Instance)
	{
		if (auto gm = GetWorld()->GetAuthGameMode<ALobbyGameMode>())
		{
			const int32 SocketNum = WeaponModularRef->Instance->SupportedSockets.Find(InModule->TargetSocket);
			gm->hangarController.AddonEquip.Request(Operation::AddonEquip::Request(WeaponModularRef->Instance->GetEntityId(), InModule->GetInstanceModelId(), SocketNum, CategoryTypeId::Addon));
		}

		if (!InModule->IsLocked())
		{
			WeaponModularRef->InstallModule(InModule);
		}
	}
}

FReply ALobbyPlayerController::OnRevertCharacterMode()
{
	Slate_CharacterManager->SwitchMode(false);

	WeaponModularRef->BeginDeactivate();

	bIsWeaponMode = false;
	LobbyCameraRef->BeginDeactivateWM(GetCurrentPreSet()->GetActorLocation());

	return FReply::Handled();
}

void ALobbyPlayerController::OnEquipAddonResponse(const Operation::AddonEquip::Response & response)
{
	auto itemInstance = InventoryRepository.FindByStrId(response.ItemId, CategoryTypeId::Weapon);
	if(itemInstance == nullptr) itemInstance = InventoryRepository.FindByStrId(response.ItemId, CategoryTypeId::Armour);
	if (itemInstance)
	{
		if (response.AddonSlot == AddonSlotSkin)
		{
			for (auto skinInstance : itemInstance->SkinList)
			{
				if (skinInstance->GetModelId() == response.AddonModelId)
				{
					skinInstance->SetEntityId(response.AddonItemId);
					itemInstance->ApplyMaterial(skinInstance);
					itemInstance->CaptureThumbnail(GetWorld());

					if (GetCurrentPreSet()->PrimaryWeaponEntity->SkeletalMesh == itemInstance->GetSkeletalMesh())
						GetCurrentPreSet()->PrimaryWeaponEntity->SetMaterial(0, itemInstance->GetMaterialByModel(skinInstance->GetInstanceModelId()));
					else if (GetCurrentPreSet()->SecondaryWeaponEntity->SkeletalMesh == itemInstance->GetSkeletalMesh())
						GetCurrentPreSet()->SecondaryWeaponEntity->SetMaterial(0, itemInstance->GetMaterialByModel(skinInstance->GetInstanceModelId()));
				}
			}
		}
		else
		{
			if (WeaponModularRef && WeaponModularRef->Instance)
			{
				if (WeaponModularRef->Instance->SupportedModules.Contains(response.AddonModelId))
				{
					for (auto &km : WeaponModularRef->Instance->Modules)
					{
						for (auto &m : km.Value.Array)
						{
							if (m->GetModelId() == response.AddonModelId)
							{
								if (auto TargetWeapon = Cast<UItemWeaponEntity>(itemInstance))
								{
									m->SetEntityId(response.AddonItemId);
									TargetWeapon->InstalledModules.Add(m);

									WeaponModularRef->InstallModule(m);
								}
							}
						}
					}

					GetCurrentPreSet()->SetWeaponEntity(WeaponModularRef->Instance);
				}				
			}
		}
	}
}

void ALobbyPlayerController::OnConfirmedItemUpgrade(const FInvertoryItemWrapper& Item)
{
	auto InstItem = InventoryRepository.FindWeaponById(Item.EntityId);
	if (InstItem)
	{
		InstItem->ApplyModifers(Item.Modifications);
		Slate_CharacterManager->GetModifyManager()->SetInstance(InstItem);
		Slate_CharacterManager->GetModifyManager()->OnConfirmedUpdate();
	}
}

void ALobbyPlayerController::InitializUi()
{
	SAssignNew(Slate_WeaponModularManager, SWeaponModularManager);		

	SAssignNew(Slate_CharacterManager, SCharacterManager)
		.OnSwitchCharacter_UObject(this, &ALobbyPlayerController::OnSwitchCharacter)
		.OnConfirmCharacterSelect_UObject(this, &ALobbyPlayerController::OnConfirmCharacterSelect)
		.OnCancelCharacterSelect_UObject(this, &ALobbyPlayerController::OnCancelCharacterSelect)
		.OnItemModify_UObject(this, &ALobbyPlayerController::OnItemModify)
		.OnItemEquip_UObject(this, &ALobbyPlayerController::OnProxyItemEquip)
		.OnItemUnEquip_UObject(this, &ALobbyPlayerController::OnProxyItemUnEquip)
		.OnChangeAsset_UObject(this, &ALobbyPlayerController::OnSwitchProfile)
		.OtherManager(Slate_WeaponModularManager.ToSharedRef())
		.OnClickedBack_UObject(this, &ALobbyPlayerController::OnRevertCharacterMode)
		.OnItemInstall_UObject(this, &ALobbyPlayerController::OnItemInstallFromList);

	Slate_CharacterManager->SetPlayerControllerRef(this);
	Slate_CharacterManager->GetModifyManager()->OnChangeMaterial.BindUObject(this, &ALobbyPlayerController::OnItemChangeMaterial);
	Slate_CharacterManager->GetModifyManager()->OnBuyMaterial.BindUObject(this, &ALobbyPlayerController::OnItemBuyMaterial);
}

void ALobbyPlayerController::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (Slate_WeaponModularManager.IsValid())
	{
		Slate_WeaponModularManager.Reset();
	}

	if (Slate_CharacterManager.IsValid())
	{
		Slate_CharacterManager.Reset();
	}
}

void ALobbyPlayerController::OnItemInstallFromList(const class UItemBaseEntity* Item)
{
	OnProxyItemEquip(Item, static_cast<EItemSetSlotId::Type>(Item->GetDescription().SlotType.GetValue()), CurrentPrEItemSetSlotIdId);
}

void ALobbyPlayerController::InitializeInventory(const struct FLobbyClientLootData& response)
{
	Super::InitializeInventory(response);

	if (auto InstamceRep = GetInstanceRepository())
	{
		for (auto item : response.CharacterList)
		{
			if (auto character = InventoryRepository.FindCharacterById(item.EntityId))
			{
				Slate_CharacterManager->ReplaceCharacter(InstamceRep->Character[character->GetInstanceModelId()], character);
			}
		}
	}
}

void ALobbyPlayerController::InitializePreSet(const EPlayerPreSetId::Type& profileId, const FClientPreSetData& property)
{
	checkf(profileId >= EPlayerPreSetId::One || profileId <= EPlayerPreSetId::Five, TEXT("Invalid profileId %d of %d"), static_cast<int32>(profileId), static_cast<int32>(EPlayerPreSetId::Five));
	
	auto targetPreSetInstance = PreSetArray[profileId];
	checkf(targetPreSetInstance, TEXT("Target profile  %d of %d was nullptr"), static_cast<int32>(profileId), static_cast<int32>(EPlayerPreSetId::Five));

	auto character = InventoryRepository.FindCharacterById(property.CharacterId);
	checkf(character, TEXT("Character [%s] for profile %d was nullptr"), *property.CharacterId, static_cast<int32>(profileId));

	targetPreSetInstance->InitializeEntity(character);
	targetPreSetInstance->SetEntityId(property.GetEntityId());
	targetPreSetInstance->GetMesh()->SetVisibility(false);
	targetPreSetInstance->SetActorHiddenInGame(true);

	for (const auto item : property.Items)
	{
		targetPreSetInstance->SetArmourEntity(InventoryRepository.FindArmourById(item.ItemId));
		targetPreSetInstance->SetWeaponEntity(InventoryRepository.FindWeaponById(item.ItemId));

		if (auto Grenade = InventoryRepository.FindGrenadeById(item.ItemId))
		{
			Slate_CharacterManager->GetSetManager()->InstallAsset(Grenade, profileId, item.AsSlotId());
		}
	}	

	Slate_CharacterManager->SetCharacterByModel(character->GetInstanceModelId());

	if (property.GetEntityId().IsValid())
	{
		Slate_CharacterManager->GetSetManager()->SetPresetIsAvaliable(profileId, true);
		Slate_CharacterManager->GetSetManager()->SetPresetIsEnable(profileId, property.IsEnabled);
	}
}

void ALobbyPlayerController::InitializePreSetList(const TArray<FClientPreSetData>& preSetList)
{
	for (SSIZE_T i = 0; i < preSetList.Num(); i++)
	{
		InitializePreSet(static_cast<EPlayerPreSetId::Type>(i), preSetList[i]);
	}
}

void ALobbyPlayerController::InitializePreSet()
{	
	for (size_t i = 0; i < EPlayerPreSetId::End; i++)
	{
		auto CharacterSet = GetWorld()->SpawnActor<ALobbyPlayerCharacter>(ALobbyPlayerCharacter::StaticClass(), SpawnCharacterLocation, SpawnCharacterParameters);
		checkf(CharacterSet, TEXT("Failed spawn %d profile"), static_cast<int32>(i));
		
		ensureAlwaysMsgf(InventoryRepository.Character[CharacterModelId::PeopleMan], TEXT("PeopleMan Character Instance was nullptr"));
		CharacterSet->InitializeEntity(InventoryRepository.Character[CharacterModelId::PeopleMan]);
		CharacterSet->GetMesh()->SetVisibility(false);
		CharacterSet->SetActorHiddenInGame(true);

		CharacterSet->OnArmourInstalled.AddSP(Slate_CharacterManager->GetSetManager(), &SCharacterManager_Sets::InstallAsset, static_cast<EPlayerPreSetId::Type>(i));
		CharacterSet->OnArmourUnInstalled.AddSP(Slate_CharacterManager->GetSetManager(), &SCharacterManager_Sets::UninstallAsset, static_cast<EPlayerPreSetId::Type>(i));

		CharacterSet->OnWeaponInstalled.AddSP(Slate_CharacterManager->GetSetManager(), &SCharacterManager_Sets::InstallAsset, static_cast<EPlayerPreSetId::Type>(i));
		CharacterSet->OnWeaponUnInstalled.AddSP(Slate_CharacterManager->GetSetManager(), &SCharacterManager_Sets::UninstallAsset, static_cast<EPlayerPreSetId::Type>(i));

		CharacterSet->OnSlotUnInstalled.AddSP(Slate_CharacterManager->GetSetManager(), &SCharacterManager_Sets::UninstallSlot, static_cast<EPlayerPreSetId::Type>(i));
		CharacterSet->OnSlotUnInstalled.AddUObject(this, &ALobbyPlayerController::OnProxyItemUnEquip, static_cast<EPlayerPreSetId::Type>(i));
		PreSetArray[i] = CharacterSet;

		Slate_CharacterManager->GetSetManager()->SetPresetIcon(static_cast<EPlayerPreSetId::Type>(i), TAttribute<const FSlateBrush*>::Create(TAttribute<const FSlateBrush*>::FGetter::CreateUObject(this, &ALobbyPlayerController::GetProfileIcon, static_cast<EPlayerPreSetId::Type>(i))));
	}
}

UItemBaseEntity* ALobbyPlayerController::AddItem(const FGuid& Index, UItemBaseEntity* Instance)
{
	if (Instance)
	{
		TGuardValue<bool> GuardTemplateNameFlag(GIsReconstructingBlueprintInstances, true);

		//if (Instance->GetDescription().CategoryType == ECategoryTypeId::Armour)
		//{
		//	Instance->SetEntityId(FGuid::NewGuid()); // �������� ��� ��������
		//	auto InventoryItem = DuplicateObject(Cast<UItemArmourEntity>(Instance), this, *FString::Printf(TEXT("Inventory_Armour_%d"), Instance->GetModelId()));
		//	InventoryItem->SetFlags(EObjectFlags::RF_WasLoaded);
		//	InventoryItem->SetEntityId(Index);
		//	InventoryRepository.Armour[Instance->GetModelId()] = InventoryItem;
		//	return InventoryItem;
		//}
		//else if (Instance->GetDescription().CategoryType == ECategoryTypeId::Weapon)
		//{
		//	Instance->SetEntityId(FGuid::NewGuid()); // �������� ��� ��������
		//	auto InventoryItem = DuplicateObject(Cast<UItemWeaponEntity>(Instance), this, *FString::Printf(TEXT("Inventory_Weapon_%d"), Instance->GetModelId()));
		//	InventoryItem->SetFlags(EObjectFlags::RF_WasLoaded);
		//	InventoryItem->SetEntityId(Index);
		//
		//	InventoryRepository.Weapon[Instance->GetModelId()] = InventoryItem;
		//	return InventoryItem;
		//}
		//else if (Instance->GetDescription().CategoryType == ECategoryTypeId::Character)
		//{
		//	Instance->SetEntityId(FGuid::NewGuid()); // �������� ��� ��������
		//	auto InventoryItem = DuplicateObject(Cast<UCharacterBaseEntity>(Instance), this, *FString::Printf(TEXT("Inventory_Character_%d"), Instance->GetModelId()));
		//	InventoryItem->SetFlags(EObjectFlags::RF_WasLoaded);
		//	InventoryItem->SetEntityId(Index);
		//	InventoryRepository.Character[Instance->GetModelId()] = InventoryItem;
		//	Slate_CharacterManager->ReplaceCharacter(Cast<UCharacterBaseEntity>(Instance), InventoryItem);
		//	return InventoryItem;
		//}
		//else if (Instance->GetDescription().CategoryType == ECategoryTypeId::Grenade)
		//{
		//	Instance->SetEntityId(FGuid::NewGuid()); // �������� ��� ��������
		//	auto InventoryItem = DuplicateObject(Cast<UItemGrenadeEntity>(Instance), this, *FString::Printf(TEXT("Inventory_Grenade_%d"), Instance->GetModelId()));
		//	InventoryItem->SetFlags(EObjectFlags::RF_WasLoaded);
		//	InventoryItem->SetEntityId(Index);
		//	InventoryRepository.Grenade[Instance->GetModelId()] = InventoryItem;
		//	return InventoryItem;
		//}
		//else if (Instance->GetDescription().CategoryType == ECategoryTypeId::Service)
		//{
		//	Instance->SetEntityId(Index);
		//	return Instance;
		//}
	}

	return nullptr;
}

void ALobbyPlayerController::SetupInputComponent() 
{
	Super::SetupInputComponent();

	if (InputComponent)
	{
		InputComponent->BindAxis("Turn", this, &ALobbyPlayerController::AddYawInput);
		InputComponent->BindAxis("LookUp", this, &ALobbyPlayerController::AddPitchInput);

		InputComponent->BindAction("WeaponSwitchDown", IE_Pressed, this, &ALobbyPlayerController::OnNextScroll);
		InputComponent->BindAction("WeaponSwitchUp", IE_Pressed, this, &ALobbyPlayerController::OnPrevScroll);
	}
}

void ALobbyPlayerController::AddPitchInput(float Val)
{
	if (FMath::IsNearlyEqual(Val, 0.0f) == false)// && this->IsInputKeyDown(EKeys::RightMouseButton))
	{
		Super::AddPitchInput(Val);

		if (bIsWeaponMode)
		{
			if (WeaponModularRef)
			{
				WeaponModularRef->AddRotation(FRotator(-Val, .0f, .0f));
			}
		}
		else
		{
			if (LobbyCameraRef)
			{
				LobbyCameraRef->AddRotation(FRotator(-Val, .0f, .0f));
			}
		}
	}
}

void ALobbyPlayerController::AddYawInput(float Val)
{
	if (FMath::IsNearlyEqual(Val, 0.0f) == false)// && this->IsInputKeyDown(EKeys::RightMouseButton))
	{
		Super::AddYawInput(Val);

		if (bIsWeaponMode)
		{
			if (WeaponModularRef)
			{
				WeaponModularRef->AddRotation(FRotator(.0f, Val, .0f));
			}
		}
		else
		{
			if (LobbyCameraRef)
			{
				LobbyCameraRef->AddRotation(FRotator(.0f, Val, .0f));
			}
		}
	}
}

void ALobbyPlayerController::OnNextScroll()
{
	if (LobbyCameraRef)
	{
		LobbyCameraRef->AddScroolDistance(8.0f);
	}
}

void ALobbyPlayerController::OnPrevScroll()
{
	if (LobbyCameraRef)
	{
		LobbyCameraRef->AddScroolDistance(-8.0f);
	}
}

void ALobbyPlayerController::SetCameraRef(ALobbyCamera* Target)
{
	LobbyCameraRef = Target;
}

void ALobbyPlayerController::SetModularRef(AWeaponModular* Target)
{
	WeaponModularRef = Target;
}

const FSlateBrush* ALobbyPlayerController::GetProfileIcon(const EPlayerPreSetId::Type InProfileId) const
{
	auto MyProfile = GetPreSet(InProfileId);
	if (MyProfile && MyProfile->GetEntityInstance() && MyProfile->GetEntityInstance()->CharacterAbility)
	{
		return MyProfile->GetEntityInstance()->CharacterAbility.GetDefaultObject()->GetIcon();
	}

	return nullptr;
}