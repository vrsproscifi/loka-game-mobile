// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "BuildPlayerController.h"

#include "BuildSystem/BuilderCharacter.h"
#include "BuildSystem/BuildHelperComponent.h"

#include "Build/ItemBuildEntity.h"
#include "Fraction/FractionEntity.h"
#include "Build/SBuildSystem_Item.h"
#include "Build/SBuildSystem_ToolBar.h"
#include "Build/SBuildSystem_Inventory.h"
#include "Components/SVerticalMenuBuilder.h"
#include "Components/SWindowBuyBuilding.h"
#include "Settings/SSettingsManager.h"
#include "Notify/SMessageBox.h"
#include "Notify/SNotifyList.h"

#include "UTGameViewportClient.h"
#include "UTGameUserSettings.h"

#include "EntityRepository.h"
#include "ShooterGameInstance.h"
#include "TutorialBuilding.h"
#include "GameMode/GameMode_Build.h"
#include "GameState/GameState_Build.h"
#include "ConstructionGameMode/Components/BasicGameComponent.h"

ABuildPlayerController::ABuildPlayerController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	IsInitAfterTimer = false;
	IsMatchMakingPrepareBeginNotified = false;
}

void ABuildPlayerController::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABuildPlayerController, SavedSlots);
}

void ABuildPlayerController::OnRep_SavedSlots()
{
	SaveConfig();
}


void ABuildPlayerController::InitializUi()
{
	if (GetNetMode() != NM_DedicatedServer)
	{
		if (MyCharacterBuilder && MyCharacterBuilder->BuildHelperComponent && IsInitAfterTimer == false)
		{
			IsInitAfterTimer = true;

			//MyCharacterBuilder->BuildHelperComponent->OnBuildItemSelected.AddUniqueDynamic(this, &ABuildPlayerController::OnBuildItemSelected);
			//MyCharacterBuilder->BuildHelperComponent->OnBuildItemBuyRequest.AddUniqueDynamic(this, &ABuildPlayerController::OnBuildItemBuyRequest);
#if !UE_SERVER
			if (auto GameInst = Cast<UShooterGameInstance>(GetGameInstance()))
			{
				for (auto Tutorial : GameInst->GetTutorials())
				{
					if (auto TargetTutorial = Cast<UTutorialBuilding>(Tutorial.Value))
					{
						//MyCharacterBuilder->BuildHelperComponent->OnBuildingSelected.AddDynamic(TargetTutorial, &UTutorialBuilding::OnBuildingSelected);
						//MyCharacterBuilder->BuildHelperComponent->OnBuildingPlaced.AddDynamic(TargetTutorial, &UTutorialBuilding::OnBuildingPlaced);
						//MyCharacterBuilder->BuildHelperComponent->OnBuildingPreRemove.AddDynamic(TargetTutorial, &UTutorialBuilding::OnBuildingPreRemove);
					}
				}
			}

			if (GetWorld() && GetWorld()->GetAuthGameMode<AGameMode_Build>())
			{
				GetWorld()->GetAuthGameMode<AGameMode_Build>()->RequestStartTutorial();
			}
#endif
			LoadConfig();

			if (SavedSlots.Num() < 10)
			{
				for (SIZE_T i = 0; i < 12; ++i)
				{
					SavedSlots.Add(-1);
				}

				SaveConfig();
			}

			if (Slate_ToolBar.IsValid() == false)
			{
				//PLACE_WIDGET(SAssignNew(Slate_ToolBar, SBuildSystem_ToolBar)
				//			 .Cash_UObject(this, &ABuildPlayerController::GetCurrentOwnerCash)
				//			 .SelectedSlot_Lambda([&]() {
				//	return (MyCharacterBuilder && MyCharacterBuilder->BuildHelperComponent) ? MyCharacterBuilder->BuildHelperComponent->GetSelectedNum() : 0;
				//})
				//			 .OnDropBuildInSlot_Lambda([&](const int32& InSlot, UItemBuildEntity* InItem) {
				//	if (MyCharacterBuilder && MyCharacterBuilder->BuildHelperComponent)
				//	{
				//		//TODO: SeNTIke
				//		//MyCharacterBuilder->BuildHelperComponent->SetSlotFromItem(InSlot, InItem);
				//		//SavedSlots[InSlot] = InItem ? InItem->GetInstanceModelId() : INDEX_NONE;
				//		//SaveConfig();
				//	}
				//})
				//	.CurrentMode_Lambda([&]() { return (MyCharacterBuilder && MyCharacterBuilder->BuildHelperComponent) ? MyCharacterBuilder->BuildHelperComponent->GetCurrentMode() : 0; })
				//	, 2);
			}

			if (Slate_Inventory.IsValid() == false)
			{
//				SAssignNew(Slate_Inventory, SBuildSystem_Inventory).OnClickedBuyItem_UObject(this, &ABuildPlayerController::OnBuildItemBuyRequest);
				if (GEngine && GEngine->GameViewport && !Slate_SettingsManager.IsValid())
				{
					if (auto UTViewport = Cast<UUTGameViewportClient>(GEngine->GameViewport))
					{
						UTViewport->AddUsableViewportWidgetContent(Slate_Inventory.ToSharedRef(), 1);
						UTViewport->ToggleSlateControll(false);
					}
				}
			}

			if (auto MyGameState = GetWorld()->GetGameState<AGameState_Build>())
			{
				TArray<UItemBuildEntity*> List;

				SIZE_T _count = 0;
				for (auto mValue : SavedSlots)
				{
					if (mValue != -1 && MyGameState->OwnerInventory.IsValidIndex(mValue) && MyGameState->OwnerInventory[mValue])
					{
//						Slate_ToolBar->SetSlotItem(_count, MyGameState->OwnerInventory[mValue]);

						if (MyCharacterBuilder && MyCharacterBuilder->BuildHelperComponent)
						{
//							MyCharacterBuilder->BuildHelperComponent->SetSlotFromItem(_count, MyGameState->OwnerInventory[mValue]);
						}
					}
					++_count;
				}

//				Slate_Inventory->OnFillList(MyGameState->OwnerInventory);
			}

			SAssignNew(Slate_WindowBuyBuilding, SWindowBuyBuilding)
				.Cash_UObject(this, &ABuildPlayerController::GetCurrentOwnerCash);
		}
	}

	//if (MyCharacterBuilder && MyCharacterBuilder->BuildHelperComponent)
	//{
	//	MyCharacterBuilder->BuildHelperComponent->OnBuildingSelected.AddDynamic(this, &ABuildPlayerController::OnBuildingSelected);
	//	MyCharacterBuilder->BuildHelperComponent->OnBuildingPlaced.AddDynamic(this, &ABuildPlayerController::OnBuildingPlaced);
	//	MyCharacterBuilder->BuildHelperComponent->OnBuildingPreRemove.AddDynamic(this, &ABuildPlayerController::OnBuildingPreRemove);
	//}
}

void ABuildPlayerController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);

	MyCharacterBuilder = Cast<ABuilderCharacter>(InPawn);
}

void ABuildPlayerController::OnBuildItemSelected_Implementation(const int32 InSlotNumTarget, UItemBuildEntity* InItem)
{
	if (Slate_ToolBar.IsValid())
	{
//		Slate_ToolBar->SetSlotItem(InSlotNumTarget, InItem);
	}
}

void ABuildPlayerController::OnBuildItemBuyRequest_Implementation(UItemBuildEntity* InItem)
{
	//TODO: SeNTIke

	//if (InItem && Slate_WindowBuyBuilding.IsValid())
	//{
	//	ClientShowBuyRequest(InItem->GetModelId(), Slate_WindowBuyBuilding->GetCurrentItemAmount(), FString());
	//}
}

bool ABuildPlayerController::ServerRequestBuyBuilding_Validate(const int32 InModelId, const int32 InCount) { return true; }
void ABuildPlayerController::ServerRequestBuyBuilding_Implementation(const int32 InModelId, const int32 InCount)
{
	auto MyGameMode = GetWorld()->GetAuthGameMode<AGameMode_Build>();
	auto MyGameState = GetWorld()->GetGameState<AGameState_Build>();

	if (MyGameMode && MyGameState)
	{
		if (MyGameState->OwnerInventory.IsValidIndex(InModelId) && MyGameState->OwnerInventory[InModelId])
		{
			MyGameMode->RequestBuyBuildItem(this, MyGameState->OwnerInventory[InModelId], InCount);
		}		
	}
}

void ABuildPlayerController::ClientToggleBuyWaiting_Implementation(const int32 InModelId, const int32 InCount, const bool InToggle)
{
	if (InToggle)
	{
		QueueMessageBegin("BuyWaitingDialog")
			SMessageBox::Get()->SetHeaderText(NSLOCTEXT("BuildMode", "BuildMode.BuyRequestWaiting.Title", "Waiting"));
			SMessageBox::Get()->SetContent(NSLOCTEXT("BuildMode", "BuildMode.BuyRequestWaiting.Desc", "Please wait, we send request to buy your item to owner."));
			SMessageBox::Get()->SetButtonsText();
			SMessageBox::Get()->SetEnableClose(true);
			SMessageBox::Get()->ToggleWidget(true);
		QueueMessageEnd
	}
	else
	{
		SMessageBox::RemoveQueueMessage(TEXT("BuyWaitingDialog"));
	}
}

void ABuildPlayerController::ClientShowBuyRequest_Implementation(const int32 InModelId, const int32 InCount, const FString& InFromName)
{
	auto MyGameState = GetWorld()->GetGameState<AGameState_Build>();
	//TODO: SeNTIKE
	//if (MyGameState && MyGameState->OwnerInventory.IsValidIndex(InModelId))
	//{
	//	auto TargetItem = MyGameState->OwnerInventory[InModelId];
	//	if (TargetItem && TargetItem->GetCost().IsEnough(&MyGameState->Cash) && Slate_WindowBuyBuilding.IsValid())
	//	{
	//		Slate_WindowBuyBuilding->SetCurrentItem(TargetItem);
	//		Slate_WindowBuyBuilding->SetCurrentItemAmount(InCount);
	//
	//		SMessageBox::AddQueueMessage(TEXT("OnBuildItemBuyRequest"), FOnClickedOutside::CreateLambda([&, ii = TargetItem, fromn = InFromName]() {
	//			SMessageBox::Get()->SetHeaderText((fromn.Len() > 1) ? FText::Format(NSLOCTEXT("BuildMode", "BuildMode.BuyRequestFrom.Title", "Request buy from {0}"), TextHelper_NotifyValue.SetValue(fromn).ToText()) : NSLOCTEXT("BuildMode", "BuildMode.BuyRequest.Title", "Buy"));
	//			SMessageBox::Get()->SetContent(Slate_WindowBuyBuilding.ToSharedRef());
	//			SMessageBox::Get()->SetButtonsText(FButtonNameLocalization::Yes, FButtonNameLocalization::No);
	//			SMessageBox::Get()->OnMessageBoxButton.AddLambda([&, i = ii](const EMessageBoxButton& InButton) {
	//				if (InButton == EMessageBoxButton::Left)
	//				{
	//					ServerRequestBuyBuilding(i->GetModelId(), Slate_WindowBuyBuilding->GetCurrentItemAmount());
	//				}
	//				else
	//				{
	//					ServerRequestBuyBuilding(i->GetModelId(), 0);
	//				}
	//				SMessageBox::Get()->ToggleWidget(false);
	//			});
	//			SMessageBox::Get()->SetEnableClose(true);
	//			SMessageBox::Get()->ToggleWidget(true);
	//		}));
	//
	//	}
	//	else if (auto MyGameMode = GetWorld()->GetAuthGameMode<AGameMode_Build>())
	//	{
	//		MyGameMode->OnBuildPurchaseNoMoney();
	//	}
	//}
}

void ABuildPlayerController::Destroyed()
{
#if !WITH_EDITOR
	if (GEngine && GEngine->GameViewport)
	{
		if (auto UTViewport = Cast<UUTGameViewportClient>(GEngine->GameViewport))
		{
			UTViewport->RemoveAllViewportWidgetsAlt();
		}
	}
#endif
	Super::Destroyed();
}

void ABuildPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("InGameESC", IE_Pressed, this, &ABuildPlayerController::OnToggleInGameMenu);

	InputComponent->BindAction(TEXT("BuildActionPlace"), IE_Pressed, this, &ABuildPlayerController::OnBuildAction_Helper<true>);
	InputComponent->BindAction(TEXT("BuildActionCancel"), IE_Pressed, this, &ABuildPlayerController::OnBuildAction_Helper<false>);

	InputComponent->BindAction(TEXT("BuildRotateLeft"), IE_Pressed, this, &ABuildPlayerController::OnRotateAction_Helper<false>);
	InputComponent->BindAction(TEXT("BuildRotateRight"), IE_Pressed, this, &ABuildPlayerController::OnRotateAction_Helper<true>);

	InputComponent->BindAction(TEXT("BuildSelection_1"), IE_Pressed, this, &ABuildPlayerController::OnSelectionAction_Helper<1>);
	InputComponent->BindAction(TEXT("BuildSelection_2"), IE_Pressed, this, &ABuildPlayerController::OnSelectionAction_Helper<2>);
	InputComponent->BindAction(TEXT("BuildSelection_3"), IE_Pressed, this, &ABuildPlayerController::OnSelectionAction_Helper<3>);
	InputComponent->BindAction(TEXT("BuildSelection_4"), IE_Pressed, this, &ABuildPlayerController::OnSelectionAction_Helper<4>);
	InputComponent->BindAction(TEXT("BuildSelection_5"), IE_Pressed, this, &ABuildPlayerController::OnSelectionAction_Helper<5>);
	InputComponent->BindAction(TEXT("BuildSelection_6"), IE_Pressed, this, &ABuildPlayerController::OnSelectionAction_Helper<6>);
	InputComponent->BindAction(TEXT("BuildSelection_7"), IE_Pressed, this, &ABuildPlayerController::OnSelectionAction_Helper<7>);
	InputComponent->BindAction(TEXT("BuildSelection_8"), IE_Pressed, this, &ABuildPlayerController::OnSelectionAction_Helper<8>);
	InputComponent->BindAction(TEXT("BuildSelection_9"), IE_Pressed, this, &ABuildPlayerController::OnSelectionAction_Helper<9>);

	InputComponent->BindAction(TEXT("BuildSelectionLeft"), IE_Pressed, this, &ABuildPlayerController::OnSelectionActionList_Helper<false>);
	InputComponent->BindAction(TEXT("BuildSelectionRight"), IE_Pressed, this, &ABuildPlayerController::OnSelectionActionList_Helper<true>);

	InputComponent->BindAction(TEXT("BuildMode_0"), IE_Pressed, this, &ABuildPlayerController::OnModeAction_Helper<EBuildHelperMode::None>);
	InputComponent->BindAction(TEXT("BuildMode_1"), IE_Pressed, this, &ABuildPlayerController::OnModeAction_Helper<EBuildHelperMode::Building>);
	InputComponent->BindAction(TEXT("BuildMode_2"), IE_Pressed, this, &ABuildPlayerController::OnModeAction_Helper<EBuildHelperMode::Selecting>);

	InputComponent->BindAction(TEXT("BuildInventory"), IE_Pressed, this, &ABuildPlayerController::OnToggleInventory);
	InputComponent->BindAction(TEXT("BuildMode"), IE_Pressed, this, &ABuildPlayerController::OnToggleMode);
}

void ABuildPlayerController::OnBuildAction(const bool IsPlace)
{
	if (MyCharacterBuilder && MyCharacterBuilder->BuildHelperComponent)
	{
		MyCharacterBuilder->BuildHelperComponent->ProcessActionPlace(IsPlace);
	}
}

void ABuildPlayerController::OnRotateAction(const bool IsRight)
{
	if (MyCharacterBuilder && MyCharacterBuilder->BuildHelperComponent)
	{
		MyCharacterBuilder->BuildHelperComponent->ProcessActionRotate(IsRight);
	}
}

void ABuildPlayerController::OnSelectionAction(const int32 InNumber)
{
	if (MyCharacterBuilder && MyCharacterBuilder->BuildHelperComponent)
	{
		MyCharacterBuilder->BuildHelperComponent->ProcessActionSelecting(InNumber);
	}
}

void ABuildPlayerController::OnSelectionActionList(const bool IsRight)
{
	if (MyCharacterBuilder && MyCharacterBuilder->BuildHelperComponent)
	{
		int32 TargetSelectionNum = MyCharacterBuilder->BuildHelperComponent->GetSelectedNum(); 
		TargetSelectionNum = IsRight ? TargetSelectionNum + 1 : TargetSelectionNum - 1;
		if (TargetSelectionNum > 9) TargetSelectionNum = 1;
		if (TargetSelectionNum < 1) TargetSelectionNum = 9;

		MyCharacterBuilder->BuildHelperComponent->ProcessActionSelecting(TargetSelectionNum);
	}
}

void ABuildPlayerController::OnModeAction(const int32 InNumber)
{
	if (MyCharacterBuilder && MyCharacterBuilder->BuildHelperComponent)
	{
		MyCharacterBuilder->BuildHelperComponent->SetCurrentMode(static_cast<EBuildHelperMode::Type>(InNumber));
	}
}

void ABuildPlayerController::OnToggleInventory()
{
	const bool IsNowVisible = (Slate_Inventory->GetVisibility() == EVisibility::Hidden);
	Slate_Inventory->ToggleWidget(IsNowVisible);
}

void ABuildPlayerController::OnToggleMode()
{
	if (MyCharacterBuilder && MyCharacterBuilder->BuildHelperComponent)
	{
		const int32 CurrentSlot = MyCharacterBuilder->BuildHelperComponent->GetCurrentMode().GetValue();
		MyCharacterBuilder->BuildHelperComponent->SetCurrentMode(static_cast<EBuildHelperMode::Type>((CurrentSlot + 1) % EBuildHelperMode::End));
	}
}

bool ABuildPlayerController::TryCreateSettingsManager()
{
	if (GEngine && GEngine->GameViewport && !Slate_SettingsManager.IsValid())
	{
		if (auto UTViewport = Cast<UUTGameViewportClient>(GEngine->GameViewport))
		{
			SAssignNew(Slate_SettingsManager, SSettingsManager)
				.GameUserSettings(Cast<UUTGameUserSettings>(GEngine->GameUserSettings))
				.IsCentered(true);

			UTViewport->AddUsableViewportWidgetContent_AlwaysVisible(Slate_SettingsManager.ToSharedRef(), 10);

			Slate_SettingsManager->ToggleWidget(false);
			Slate_SettingsManager->OnClickedX.BindLambda([&]() { Slate_SettingsManager->ToggleWidget(false); });
			return true;
		}
	}

	return false;
}

bool ABuildPlayerController::TryCreateMenu()
{
	if (GEngine && GEngine->GameViewport && !Slate_EscapeMenu.IsValid())
	{
		if (auto UTViewport = Cast<UUTGameViewportClient>(GEngine->GameViewport))
		{
			TArray<FText> _TDMMenu;
			_TDMMenu.Add(NSLOCTEXT("TDMMenu", "ButtonContinue", "Continue"));			// EPauseMenuButtons::Continue
			_TDMMenu.Add(NSLOCTEXT("TDMMenu", "ButtonSettings", "Settings"));			// EPauseMenuButtons::SwitchTeam
			_TDMMenu.Add(NSLOCTEXT("TDMMenu", "ButtonReturnLobby", "Return to Lobby"));	// EPauseMenuButtons::ReturnToLobby

			UTViewport->AddUsableViewportWidgetContent_AlwaysVisible(SAssignNew(Slate_EscapeMenu, SVerticalMenuBuilder).ButtonsText(_TDMMenu).OnClickAnyButton_Lambda([&](uint8 _Buttom) {
				OnToggleInGameMenu();

				if (_Buttom == 1) // Settings
				{
					if (Slate_SettingsManager.IsValid() || TryCreateSettingsManager())
					{
						Slate_SettingsManager->LoadSettings();
						Slate_SettingsManager->ToggleWidget(true);
					}
				}
				else if (_Buttom == 2) // EPauseMenuButtons::ReturnToLobby
				{
					ServerRequestExitMe();
				}			
			}), 3);

			return true;
		}
	}

	return false;
}

bool ABuildPlayerController::ServerRequestExitMe_Validate() { return true; }
void ABuildPlayerController::ServerRequestExitMe_Implementation()
{
	AGameMode_Build* MyGameMode = GetWorld() ? GetWorld()->GetAuthGameMode<AGameMode_Build>() : nullptr;
	if (MyGameMode)
	{
		MyGameMode->RequestPlayerExiting(this);
	}
}

void ABuildPlayerController::OnToggleInGameMenu()
{
	if (Slate_SettingsManager.IsValid() && Slate_SettingsManager->GetVisibility() != EVisibility::Hidden)
	{
		Slate_SettingsManager->ToggleWidget(false);
	}
	else if (Slate_Inventory.IsValid() && Slate_Inventory->IsInInteractiveMode())
	{
		Slate_Inventory->ToggleWidget(false);
	}
	else if(Slate_EscapeMenu.IsValid() || TryCreateMenu())
	{
		static bool IsVisible;
		IsVisible = !IsVisible;

		Slate_EscapeMenu->ToggleWidget(IsVisible);
	}
}

void ABuildPlayerController::OnBuildingSelected(ABuildPlacedComponent* InComponent)
{
	if (InComponent && Slate_ToolBar.IsValid())
	{
#if WITH_EDITOR
		FNotifyInfo NotifyInfo(FText::FromString(*InComponent->GetName()), FText::FromString("OnBuildingSelected"));
		UBasicGameComponent::AddNotify(NotifyInfo);
#endif
	}
}

void ABuildPlayerController::OnBuildingPlaced(ABuildPlacedComponent* InComponent)
{
	if (InComponent && Slate_ToolBar.IsValid())
	{
#if WITH_EDITOR
		FNotifyInfo NotifyInfo(FText::FromString(*InComponent->GetName()), FText::FromString("OnBuildingPlaced"));
		UBasicGameComponent::AddNotify(NotifyInfo);
#endif
	}
}

void ABuildPlayerController::OnBuildingPreRemove(ABuildPlacedComponent* InComponent)
{
	if (InComponent && Slate_ToolBar.IsValid())
	{
#if WITH_EDITOR
		FNotifyInfo NotifyInfo(FText::FromString(*InComponent->GetName()), FText::FromString("OnBuildingRemove"));
		UBasicGameComponent::AddNotify(NotifyInfo);
#endif
	}
}

bool ABuildPlayerController::ServerRequestChangeSlot_Validate(const int32 InSlot, const int32 InModelId) { return true; }
void ABuildPlayerController::ServerRequestChangeSlot_Implementation(const int32 InSlot, const int32 InModelId)
{
	if (SavedSlots.IsValidIndex(InSlot))
	{
		SavedSlots[InSlot] = InModelId;
	}
}

FTypeCash ABuildPlayerController::GetCurrentOwnerCash() const
{
	if (auto MyGameState = GetWorld()->GetGameState<AGameState_Build>())
	{
		return MyGameState->Cash;
	}

	return FTypeCash();
}

// Error messages

void ABuildPlayerController::ClientPlaceObjectOnNotFoundInstance_Implementation()
{
	QueueMessageBegin("PlaceObject.OnNotFoundInstance")
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("All", "PlaceObject.OnNotFoundInstance.Title", "Instance not found"));
		SMessageBox::Get()->SetContent(NSLOCTEXT("All", "PlaceObject.OnNotFoundInstance.Desc", "Instance not found."));

		SMessageBox::Get()->SetButtonsText(FButtonNameLocalization::Ok);
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton&) { SMessageBox::Get()->ToggleWidget(false); });
		SMessageBox::Get()->SetEnableClose(true);
		SMessageBox::Get()->ToggleWidget(true);
	QueueMessageEnd
}

void ABuildPlayerController::ClientPlaceObjectOnNotEnouthItem_Implementation()
{
	QueueMessageBegin("PlaceObject.OnNotEnouthItem")
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("All", "PlaceObject.OnNotEnouthItem.Title", "No details"));
		SMessageBox::Get()->SetContent(NSLOCTEXT("All", "PlaceObject.OnNotEnouthItem.Desc", "Insufficient facilities for construction."));

		SMessageBox::Get()->SetButtonsText(FButtonNameLocalization::Ok);
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton&) { SMessageBox::Get()->ToggleWidget(false); });
		SMessageBox::Get()->SetEnableClose(true);
		SMessageBox::Get()->ToggleWidget(true);
	QueueMessageEnd
}

void ABuildPlayerController::ClientPlaceObjectOnFailed_Implementation()
{
	QueueMessageBegin("PlaceObject.OnFailed")
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("All", "PlaceObject.OnFailed.Title", "Unknown error while building"));
		SMessageBox::Get()->SetContent(NSLOCTEXT("All", "PlaceObject.OnFailed.Desc", "Unknown error while building."));

		SMessageBox::Get()->SetButtonsText(FButtonNameLocalization::Ok);
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton&) { SMessageBox::Get()->ToggleWidget(false); });
		SMessageBox::Get()->SetEnableClose(true);
		SMessageBox::Get()->ToggleWidget(true);
	QueueMessageEnd
}

void ABuildPlayerController::ClientRemoveObjectOnFailed_Implementation()
{
	QueueMessageBegin("RemoveObject.OnFailed")
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("All", "RemoveObject.OnFailed.Title", "Error while destroy the build"));
		SMessageBox::Get()->SetContent(NSLOCTEXT("All", "RemoveObject.OnFailed.Desc", "Error while destroy the build"));

		SMessageBox::Get()->SetButtonsText(FButtonNameLocalization::Ok);
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton&) { SMessageBox::Get()->ToggleWidget(false); });
		SMessageBox::Get()->SetEnableClose(true);
		SMessageBox::Get()->ToggleWidget(true);
	QueueMessageEnd
}

void ABuildPlayerController::ClientOnStartTutorialOnFailed_Implementation()
{
	QueueMessageBegin("OnStartTutorial.OnFailed")
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("All", "OnStartTutorial.OnFailed.Title", "OnStartTutorial"));
		SMessageBox::Get()->SetContent(NSLOCTEXT("All", "OnStartTutorial.OnFailed.Desc", "OnFailed"));

		SMessageBox::Get()->SetButtonsText(FButtonNameLocalization::Ok);
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton&) { SMessageBox::Get()->ToggleWidget(false); });
		SMessageBox::Get()->SetEnableClose(true);
		SMessageBox::Get()->ToggleWidget(true);
	QueueMessageEnd
}

void ABuildPlayerController::ClientOnStartTutorialOnNotFoundInstance_Implementation()
{
	QueueMessageBegin("OnStartTutorial.OnNotFoundInstance")
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("All", "OnStartTutorial.OnNotFoundInstance.Title", "OnStartTutorial"));
		SMessageBox::Get()->SetContent(NSLOCTEXT("All", "OnStartTutorial.OnNotFoundInstance.Desc", "OnNotFoundInstance"));

		SMessageBox::Get()->SetButtonsText(FButtonNameLocalization::Ok);
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton&) { SMessageBox::Get()->ToggleWidget(false); });
		SMessageBox::Get()->SetEnableClose(true);
		SMessageBox::Get()->ToggleWidget(true);
	QueueMessageEnd
}

void ABuildPlayerController::ClientOnStartTutorialOnNotFoundSteps_Implementation()
{
	QueueMessageBegin("OnStartTutorial.OnNotFoundSteps")
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("All", "OnStartTutorial.OnNotFoundSteps.Title", "OnStartTutorial"));
		SMessageBox::Get()->SetContent(NSLOCTEXT("All", "OnStartTutorial.OnNotFoundSteps.Desc", "OnNotFoundSteps"));

		SMessageBox::Get()->SetButtonsText(FButtonNameLocalization::Ok);
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton&) { SMessageBox::Get()->ToggleWidget(false); });
		SMessageBox::Get()->SetEnableClose(true);
		SMessageBox::Get()->ToggleWidget(true);
	QueueMessageEnd
}

void ABuildPlayerController::ClientRespondSqaudInvite_Implementation(const FString& InPlayerName)
{
	if (Slate_ToolBar.IsValid())
	{
		FNotifyInfo NotifyInfo(
			FText::Format(NSLOCTEXT("Notify", "Notify.OnRespondSqaudInviteResponse.Desc", "You have accepted the invitation in the squad from {0}"), FRichHelpers::TextHelper_NotifyValue.SetValue(InPlayerName).ToText()),
			NSLOCTEXT("Notify", "Notify.OnRespondSqaudInviteResponse.Title", "Incoming invitation squad")
		);

		UBasicGameComponent::AddNotify(NotifyInfo);
	}
}

void ABuildPlayerController::ClientRespondSqaudInviteNotFound_Implementation()
{
	if (Slate_ToolBar.IsValid())
	{
		FNotifyInfo NotifyInfo(
			NSLOCTEXT("Notify", "Notify.OnRespondSqaudInviteInviteNotFound.Desc", "Unable to find such an invitation"),
			NSLOCTEXT("Notify", "Notify.OnRespondSqaudInviteInviteNotFound.Title", "Squad management")
		);

		UBasicGameComponent::AddNotify(NotifyInfo);
	}
}

void ABuildPlayerController::ClientRespondSqauInviteAreRecived_Implementation(const FString& InPlayerName)
{
	if (Slate_ToolBar.IsValid())
	{
		FNotifyInfo NotifyInfo(
			NSLOCTEXT("Notify", "Notify.OnRespondSqaudInviteResponse.Desc", "You have already responded to the invitation to squad from {0}"), FRichHelpers::TextHelper_NotifyValue.SetValue(InPlayerName).ToText(),
			NSLOCTEXT("Notify", "Notify.OnRespondSqaudInviteResponse.Title", "Incoming invitation squad")
		);

		UBasicGameComponent::AddNotify(NotifyInfo);
	}
}

void ABuildPlayerController::ClientRespondSenderInSquad_Implementation(const FString& InPlayerName)
{
	if (Slate_ToolBar.IsValid())
	{
		FNotifyInfo NotifyInfo(
			NSLOCTEXT("Notify", "Notify.OnRespondSqaudInviteSenderInSquad.Desc", "You can not accept an invitation to a duel, because {0} is already a member of the squad."), FRichHelpers::TextHelper_NotifyValue.SetValue(InPlayerName).ToText(),
			NSLOCTEXT("Notify", "Notify.OnRespondSqaudInviteSenderInSquad.Title", "Incoming invitation duel")
		);

		UBasicGameComponent::AddNotify(NotifyInfo);
	}
}

void ABuildPlayerController::ClientSquadDuelInvitesResponse_Implementation(const FSquadInviteListContainer& InInvites)
{
	for (const auto invite : InInvites.Duels)
	{
		if (invite.State == ESquadInviteState::Submitted)
		{
			const auto id = FName(*FString::Printf(TEXT("DuelInvite_%s"), *invite.InviteId));

			if (Slate_ToolBar.IsValid())
			{
				FNotifyInfo NotifyInfo(
					FText::Format(NSLOCTEXT("Notify", "Notify.DuelInvite.Desc", "{0} challenge to a duel on weapons: {1}, number of lives: {2}. Bet: {3}"),
					FRichHelpers::TextHelper_NotifyValue.SetValue(invite.Name).ToText(),
					FRichHelpers::TextHelper_NotifyValue.SetValue(FDuelWeapons::GetText(invite.WeaponType)).ToText(),
					FRichHelpers::TextHelper_NotifyValue.SetValue(FText::AsNumber(invite.NumberOfLives)).ToText(),
					invite.Bet.ToResourse()),
					NSLOCTEXT("Notify", "Notify.DuelInvite.Title", "Challenge to a duel!")
				);

				UBasicGameComponent::AddNotify(NotifyInfo);
			}

			QueueMessageBegin(id, i = invite)
				SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "MessageBox.DuelInvite.Title", "Invite into Duel"));
				SMessageBox::Get()->SetContent(FText::Format(NSLOCTEXT("MessageBox", "Notify.DuelInvite.Desc", "Do you want to accept the invitation from {0}\n into Duel on weapon: {1}? Number of lives: {2}.\n Bet: {3}"),
											   FRichHelpers::TextHelper_NotifyValue.SetValue(i.Name).ToText(),
											   FRichHelpers::TextHelper_NotifyValue.SetValue(FDuelWeapons::GetText(i.WeaponType)).ToText(),
											   FRichHelpers::TextHelper_NotifyValue.SetValue(FText::AsNumber(i.NumberOfLives)).ToText(),
											   i.Bet.ToResourse()));


				SMessageBox::Get()->SetButtonsText(FButtonNameLocalization::Yes, FText::GetEmpty(), FButtonNameLocalization::No);
				SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton button) {
					if (button == EMessageBoxButton::Left)
					{
						ServerDuelAnswer(i.InviteId, true);
					}
					else
					{
						ServerDuelAnswer(i.InviteId, false);
					}
					SMessageBox::Get()->ToggleWidget(false);
				});
				SMessageBox::Get()->SetEnableClose(false);
				SMessageBox::Get()->ToggleWidget(true);
			QueueMessageEnd
		}
		else if (Slate_ToolBar.IsValid())
		{
			if (invite.State & ESquadInviteState::Approved)
			{
				FNotifyInfo NotifyInfo(
					FText::Format(NSLOCTEXT("Notify", "Notify.DuelInviteApproved.Desc", "{0} approved challenge to a duel on weapons: {1}, number of lives: {2}. Bet: {3}"),
					FRichHelpers::TextHelper_NotifyValue.SetValue(invite.Name).ToText(),
					FRichHelpers::TextHelper_NotifyValue.SetValue(FDuelWeapons::GetText(invite.WeaponType)).ToText(),
					FRichHelpers::TextHelper_NotifyValue.SetValue(FText::AsNumber(invite.NumberOfLives)).ToText(),
					invite.Bet.ToResourse()),
					NSLOCTEXT("Notify", "Notify.DuelInvite.Title", "Challenge to a duel!")
				);

				UBasicGameComponent::AddNotify(NotifyInfo);
			}
			else
			{
				FNotifyInfo NotifyInfo(
					FText::Format(NSLOCTEXT("Notify", "Notify.DuelInviteRejected.Desc", "{0} rejected challenge to a duel on weapons: {1}, number of lives: {2}. Bet: {3}"),
					FRichHelpers::TextHelper_NotifyValue.SetValue(invite.Name).ToText(),
					FRichHelpers::TextHelper_NotifyValue.SetValue(FDuelWeapons::GetText(invite.WeaponType)).ToText(),
					FRichHelpers::TextHelper_NotifyValue.SetValue(FText::AsNumber(invite.NumberOfLives)).ToText(),
					invite.Bet.ToResourse()),
					NSLOCTEXT("Notify", "Notify.DuelInvite.Title", "Challenge to a duel!")
				);

				UBasicGameComponent::AddNotify(NotifyInfo);
			}
		}
	}
}

bool ABuildPlayerController::ServerDuelAnswer_Validate(const FString& InInviteId, const bool InAnswer) { return true; }
void ABuildPlayerController::ServerDuelAnswer_Implementation(const FString& InInviteId, const bool InAnswer)
{
	auto MyGameMode = GetWorld() ? GetWorld()->GetAuthGameMode<AGameMode_Build>() : nullptr;
	if (MyGameMode)
	{
		MyGameMode->ProxySendDuelAnswer(InInviteId, InAnswer);
	}	
}

void ABuildPlayerController::ClientRespondDuelInviteNoMoney_Implementation(const FString& InPlayerName)
{
	if (Slate_ToolBar.IsValid())
	{
		FNotifyInfo NotifyInfo(
			FText::Format(NSLOCTEXT("Notify", "Notify.OnRespondDuelInviteNotEnouthMoney.Desc", "You do not have the money to accept an invitation to a duel from {0}"), FRichHelpers::TextHelper_NotifyValue.SetValue(InPlayerName).ToText()),
			NSLOCTEXT("Notify", "Notify.OnRespondDuelInviteNotEnouthMoney.Title", "Incoming invitation duel")
		);

		UBasicGameComponent::AddNotify(NotifyInfo);
	}

	QueueMessageBegin("DuelInviteNotEnouthMoney", name = InPlayerName)
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "MessageBox.DuelInviteNotEnouthMoney.Title", "Incoming invitation duel"));
		SMessageBox::Get()->SetContent(FText::Format(NSLOCTEXT("Notify", "MessageBox.DuelInviteNotEnouthMoney.Desc", "You do not have the money that would invite {0} to a duel. \nDo you want to replenish your account through the site?"), FRichHelpers::TextHelper_NotifyValue.SetValue(name).ToText()));

		SMessageBox::Get()->SetButtonsText(FButtonNameLocalization::Yes, FText::GetEmpty(), NSLOCTEXT("All", "All.No", "No"));
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton button) {
			if (button == EMessageBoxButton::Left)
			{
				FPlatformProcess::LaunchURL(TEXT("http://lokagame.com/"), NULL, NULL);
			}
			SMessageBox::Get()->ToggleWidget(false);
		});
		SMessageBox::Get()->SetEnableClose(false);
		SMessageBox::Get()->ToggleWidget(true);
	QueueMessageEnd
}

void ABuildPlayerController::ClientRespondDuelSenderNoMoney_Implementation(const FString& InPlayerName)
{
	if (Slate_ToolBar.IsValid())
	{
		FNotifyInfo NotifyInfo(
			FText::Format(NSLOCTEXT("Notify", "Notify.OnRespondDuelInviteSenderNotEnouthMoney.Desc", "You can not accept an invitation to a duel, since {0} (the organizer of a duel) insufficient funds for its implementation."), FRichHelpers::TextHelper_NotifyValue.SetValue(InPlayerName).ToText()),
			NSLOCTEXT("Notify", "Notify.OnRespondDuelInviteSenderNotEnouthMoney.Title", "Incoming invitation duel")
		);

		UBasicGameComponent::AddNotify(NotifyInfo);
	}
}

void ABuildPlayerController::ClientRespondDuelInvite_Implementation(const FString& InPlayerName)
{
	if (Slate_ToolBar.IsValid())
	{
		FNotifyInfo NotifyInfo(
			FText::Format(NSLOCTEXT("Notify", "Notify.OnRespondDuelInviteResponse.Desc", "You have accepted the invitation in the squad from {0}"), FRichHelpers::TextHelper_NotifyValue.SetValue(InPlayerName).ToText()),
			NSLOCTEXT("Notify", "Notify.OnRespondDuelInviteResponse.Title", "Incoming invitation squad")
		);

		UBasicGameComponent::AddNotify(NotifyInfo);
	}
}

//void ABuildPlayerController::ClientSessionMatchFounded_Implementation(const FSearchSessionStatus& InResponse)
//{
//	if (InResponse.Status == ESearchSessionStatus::Prepare)
//	{
//		if (IsMatchMakingPrepareBeginNotified == false)
//		{
//			const auto mapName = FGameMapTypeId::GetString(InResponse.GameMapTypeId);
//
//			if (mapName.Len() > 0 && mapName.IsEmpty() == false)
//			{
//				LoadPackageAsync(FString::Printf(TEXT("Game/Maps/%s.umap"), *mapName), FLoadPackageAsyncDelegate(), 0, EPackageFlags::PKG_Need);
//			}
//
//			QueueMessageBegin("MatchMaking.TravelMessage", res = InResponse)
//				if (res.GameModeTypeId == EGameModeTypeId::Attack)
//				{
//					if (res.IsDefence)
//					{
//						//	������ {0} �������� �� ���� ����! ���� ���������� � ������� ����. ����������, ���������, ��� ����� ������ �� ������ ����� ������
//						//	Players {0} attack your base! Preparations are in progress for the defense of the base. Please wait, this may take no more than one minute.
//						SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "Notify.TravelMessage.Defence2.Title", "Your base is under attack!"));
//						SMessageBox::Get()->SetContent(FText::Format(NSLOCTEXT("Notify", "Notify.TravelMessage.Defenc2e.Desc", "Players {0} attack your base! Preparations are in progress for the defense of the base. Please wait, this may take no more than one minute"), FText::FromString(res.UniversalName)));
//					}
//					else
//					{
//						//	���� ������� �������� ����� �� ������ {0}. ���������� �� ����� � ���, ��������� ��� ����� ������ �� ������ ����� ������.
//						//	Your team is attacking the player {0}. Preparing to enter the battle, wait it can take no more than one minute.
//						SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "Notify.TravelMessage.Attack2.Title", "Outgoing attack on the player"));
//						SMessageBox::Get()->SetContent(FText::Format(NSLOCTEXT("Notify", "Notify.TravelMessage.Attack2.Desc", "Your team is attacking the player {0}. Preparing to enter the battle, wait it can take no more than one minute."), FText::FromString(res.UniversalName)));
//					}
//				}
//				else if (res.GameModeTypeId == EGameModeTypeId::DuelMatch)
//				{
//					//	���� ���������� � ����� � ������� {0}. ����������, ��������� ��� ����� ������ �� ����� ����� ������.
//					//	Preparations are in progress for a duel with player {0}. Please, wait it may take no more than one minute.
//					SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "Notify.TravelMessage.Duel.Title", "Preparing for a duel"));
//					SMessageBox::Get()->SetContent(FText::Format(NSLOCTEXT("Notify", "Notify.TravelMessage.Duel2.Desc", "Preparations are in progress for a duel with player {0}. Please, wait it may take no more than one minute."), FText::FromString(res.UniversalName)));
//				}
//				else if(res.GameModeTypeId == EGameModeTypeId::Lobby)
//				{
//					//	������ {0} �������� �� ���� ����! ���� ���������� � ������� ����. ����������, ���������, ��� ����� ������ �� ������ ����� ������
//					//	Players {0} attack your base! Preparations are in progress for the defense of the base. Please wait, this may take no more than one minute.
//					SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "Notify.TravelMessage.LobbyCOOP.Title", "Enter cooperative mode"));
//					SMessageBox::Get()->SetContent(NSLOCTEXT("Notify", "Notify.TravelMessage.LobbyCOOP.Desc", "Preparing to enter cooperative mode. Please wait, this may take no more than one minute"));
//				}
//				else
//				{
//					const auto GameMapName = FGameMapTypeId::GetText(res.GameMapTypeId);
//					const auto GameModeName = FGameModeTypeId::GetText(res.GameModeTypeId);
//
//					//	���������� �� ����� � ������������� ���, {0} - {1}. ���������� ���������, ��� ����� ������ �� ������ ����� ������.
//					//	Preparing to enter the unfinished battle, {0} - {1}. Please wait, this may take no more than one minute.
//					SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "Notify.TravelMessage.Title", "Unfinished game"));
//					SMessageBox::Get()->SetContent(FText::Format(NSLOCTEXT("Notify", "Notify.TravelMessage2.Desc", "Preparing to enter the unfinished battle, {0} - {1}. Please wait, this may take no more than one minute."), GameModeName, GameMapName));
//				}
//
//
//			//SMessageBox::Get()->SetButtonsText(NSLOCTEXT("All", "All.Yes", "Yes"), NSLOCTEXT("All", "All.No", "No"));
//			//SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton& button)
//			//{
//			//	if (button == EMessageBoxButton::Left)
//			//	{
//			//		JoinGameState = EJoinGameState::Continue;
//			//		lobbyController.GameSessionContinue.Request();
//			//	}
//			//	else
//			//	{
//			//		lobbyController.GameSessionBreak.Request();
//			//	}
//			//
//			//	SMessageBox::RemoveQueueMessage("MatchMaking.TravelMessage");
//			//	SMessageBox::Get()->ToggleWidget(false);
//			//});
//
//			SMessageBox::Get()->SetButtonsText();
//			SMessageBox::Get()->SetEnableClose(false);
//			SMessageBox::Get()->ToggleWidget(true);
//			QueueMessageEnd
//
//			if (Slate_ToolBar.IsValid())
//			{
//				FNotifyInfo NotifyInfo(
//					FText::Format(NSLOCTEXT("Lobby", "Notify.MatchMaking.MatchPrepareBegin", "Search game ended, the preparation of the entrance to the game. \r\nGamemode: {0} | Location: {1}"),
//					FGameModeTypeId::GetText(InResponse.GameModeTypeId),
//					FGameMapTypeId::GetText(InResponse.GameMapTypeId)),
//					FText(NSLOCTEXT("Lobby", "Notify.MatchMaking.Title", "Match making information"))
//				);
//
//				UBasicGameComponent::AddNotify(NotifyInfo);
//			}
//		}
//
//		IsMatchMakingPrepareBeginNotified = true;
//	}
//}

void ABuildPlayerController::ClientMatchResults_Implementation(const TArray<FLobbyMatchResultPreview>& InResults)
{
	for (auto result : InResults)
	{
		const auto GameMapName = FGameMapTypeId::GetText(result.GameMapTypeId);
		const auto GameModeName =
			result.GameModeTypeId == EGameModeTypeId::Attack
			? FText::FromString(result.UniversalName)
			: FGameModeTypeId::GetText(result.GameModeTypeId);

		const auto Victory = NSLOCTEXT("Notify", "Notify.Victory", "Victory");
		const auto Defeat = NSLOCTEXT("Notify", "Notify.Defeat", "Defeat");

		const auto WinState = result.IsWin ? Victory : Defeat;

		if (Slate_ToolBar.IsValid())
		{
			FNotifyInfo NotifyInfo(
				FText::Format(NSLOCTEXT("Notify", "Notify.OnGetMatchResults.Desc", "\t\tReward\nMoney: {0} Hexagon: {3}\nExperience: {1} \nReputation: {2}"),
				FRichHelpers::ResourceHelper_Money.SetValue(result.Award.Money).ToText(),
				FRichHelpers::TextHelper_NotifyValue.SetValue(FText::AsNumber(result.Award.Experience)).ToText(),
				FRichHelpers::TextHelper_NotifyValue.SetValue(FText::AsNumber(result.Award.Reputation)).ToText(),
				FRichHelpers::ResourceHelper_Donate.SetValue(result.Award.Donate).ToText()),
				FText::Format(NSLOCTEXT("Notify", "Notify.OnGetMatchResults.Title", "{0} {1} {2}"),	WinState, GameModeName, GameMapName)
			);

			UBasicGameComponent::AddNotify(NotifyInfo);
		}
	}

	//TODO: SeNTIke

	//if (InResults.Num())
	//{
	//	if (const auto gi = GetGameInstance())
	//	{
	//		if (const auto cgi = Cast<UShooterGameInstance>(gi))
	//		{
	//			const auto fraction = cgi->EntityRepository->InstanceRepository.Fraction[FPlayerEntityInfo::Instance.FractionTypeId];
	//
	//			if (Slate_ToolBar.IsValid() && fraction)
	//			{
	//				FNotifyInfo NotifyInfo(
	//					FText::Format(NSLOCTEXT("Notify", "Notify.OnGetMatchResults.Desc", "For every battle you gain respect in the fraction. Each new level rank provides access to new items( weapons, armor and kits). You needed to get another {0} reputation in the fraction {1} to the new rank. \n\rYou can change the faction at any time, go to the store and choose another faction."),
	//					fraction->Progress.NextReputation - fraction->Progress.Reputation,
	//					fraction->GetDescription().Name),
	//					NSLOCTEXT("Notify", "Notify.GameGuide.Fraction.Title", "Fraction guide in game")
	//				);
	//
	//				UBasicGameComponent::AddNotify(NotifyInfo);
	//			}
	//		}
	//	}
	//}
}