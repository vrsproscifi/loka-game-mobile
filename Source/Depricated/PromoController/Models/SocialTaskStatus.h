#pragma once

#include "Types/TypeCost.h"
#include "SocialGroupId.h"
#include "SocialTaskStatus.generated.h"
//

USTRUCT()
struct FSocialTaskStatusContainer
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	TEnumAsByte<SocialGroupId::Type> SocialGroupId;

	UPROPERTY()
	FTypeCost Award;
};

USTRUCT()
struct FSocialTaskStatusResponse
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	int32 ActiveTaskCount;

	UPROPERTY()
	TArray<FSocialTaskStatusContainer> CompletedTasks;

};