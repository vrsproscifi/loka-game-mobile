#pragma once

#include "SocialGroupId.h"
#include "SocialTaskWrapper.generated.h"

USTRUCT()
struct FSocialTaskWrapper
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
		FString TaskId;

	UPROPERTY()
		TEnumAsByte<SocialGroupId::Type> SocialGroupId;

	UPROPERTY()
		FString UrlAddress;

	UPROPERTY()
		int32 EndDate;
};