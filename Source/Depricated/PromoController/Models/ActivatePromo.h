#pragma once

#include <Types/TypeCash.h>
#include "ActivatePromo.generated.h"

namespace CategoryTypeId
{
	enum Type;
}

USTRUCT()
struct FActivatePromoResponse
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
		FString ItemId;

	UPROPERTY()
		uint8 ModelId;

	UPROPERTY()
	TEnumAsByte<CategoryTypeId::Type> CategoryTypeId;

	UPROPERTY()
		FTypeCash Cash;
};