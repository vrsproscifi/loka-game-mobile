#pragma once

#include "Operation/ActivatePromo.h"
#include "Operation/ListOfSocialTask.h"
#include "Operation/StatusOfSocialTask.h"
#include "Operation/TakeSocialTask.h"


class PromoController
{
public:
	PromoController();

	
	Operation::ActivatePromo::Operation	ActivatePromo;
	Operation::ListOfSocialTask::Operation	ListOfSocialTask;
	Operation::StatusOfSocialTask::Operation	StatusOfSocialTask;
	Operation::TakeSocialTask::Operation	TakeSocialTask;

};