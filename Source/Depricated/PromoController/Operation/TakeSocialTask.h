#pragma once

#include "ServiceHandler.h"
#include "ServiceOperation.h"
#include "RequestOneParam.h"

namespace Operation
{
	namespace TakeSocialTask
	{
		class Operation final 
			: public COperation<FRequestOneParam<FString>, ObjectSerializer::NativeObject, FRequestOneParam<FString>, ObjectSerializer::NativeObject>
		{
		public:
			IResponseHandler<void, OperationResponseType::None> OnCodeNotFound;
			IResponseHandler<void, OperationResponseType::None> OnCodeActivated;


			Operation() 
				: Super(FServerHost::MasterClient, "Promo/TakeSocialTask") 
				, OnCodeNotFound(400, this)
				, OnCodeActivated(409, this)
			{

			}

		};
	}
}
