#pragma once

#include "ServiceHandler.h"
#include "ServiceOperation.h"
#include "../Models/SocialTaskStatus.h"
#include "PoolingOperation.h"

namespace Operation
{
	namespace StatusOfSocialTask
	{
		class Operation final 
			: public COperation<void, ObjectSerializer::JsonObject, FSocialTaskStatusResponse, ObjectSerializer::JsonObject>
			, public FPoolingOperation
		{
		public:
			Operation() 
				: Super(FServerHost::MasterClient, "Promo/StatusOfSocialTask", FVerb::Get)
				, FPoolingOperation(30.0f, 2.5f)
			{ 
				TimerDelegate.BindRaw(this, &Operation::Request);
			}

		};
	}
}
