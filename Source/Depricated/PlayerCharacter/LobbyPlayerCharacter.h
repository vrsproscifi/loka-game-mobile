// VRSPRO

#pragma once

#include "BasePlayerCharacter.h"

#include "Item/ItemSlotTypeId.h"
#include "LobbyPlayerCharacter.generated.h"

namespace EItemSetSlotId { enum Type; }

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FComponentChanged, FGuid, setId, FGuid, entityId);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnArmourSlotUnInstalled, const EItemSetSlotId::Type);
DECLARE_DELEGATE(FOnClickedOnCharacter);

UCLASS(MinimalAPI)
class ALobbyPlayerCharacter
	: public ABasePlayerCharacter
{
	GENERATED_BODY()

public:
	ALobbyPlayerCharacter();

	UFUNCTION(BlueprintCallable, Category = Socket)
	FName GetHardWeaponSocket() const;

	UFUNCTION(BlueprintCallable, Category = Socket)
	FName GetLiteWeaponSocket() const;

	FVector GetSelectionLocation() const;
	void SetSelectedComponent(const int32&);
	
	virtual void ApplyEntity(const UCharacterBaseEntity* entity) override;
	virtual void SwapEnity(const UCharacterBaseEntity* entity) override;
	virtual void RestoreEnity() override;


	FComponentChanged OnChangedArmour;
	FComponentChanged OnChangedWeapon;
	FOnArmourSlotUnInstalled OnSlotUnInstalled;
	FOnClickedOutside OnClickedCharacter;

	void ToggleClickable(const bool);

	virtual void SetArmourEntity(const UItemArmourEntity* armourEntity) override;

	UFUNCTION()
	virtual void UnSetArmourEntitySlot(const ItemSlotTypeId::Type& slot);

protected:
	bool bClickableCharacter;
	bool bIsSwaped;
	virtual void PostInitializeComponents() override;


	UPROPERTY(transient)
	const UCharacterBaseEntity* TemporaryInstance;

	UFUNCTION()
	void OnClickedComponentCursorOver(UPrimitiveComponent* Component, FKey ButtonPressed);

	UFUNCTION()
	void OnBeginComponentCursorOver(UPrimitiveComponent* Component);

	UFUNCTION()
	void OnEndComponentCursorOver(UPrimitiveComponent* Component);

	UPROPERTY(EditDefaultsOnly, Category = Socket)
		FName HardWeaponSocket;

	UPROPERTY(EditDefaultsOnly, Category = Socket)
		FName LiteWeaponSocket;

public:
	//UPROPERTY()
	//	TArray<class UInvertoryItemBase*> AssetInvertory;

	//UCharacterInvertoryItem* SelectedComponent;
};
