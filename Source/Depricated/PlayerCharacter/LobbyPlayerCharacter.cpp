// VRSPRO

#include "LokaGame.h"
#include "LobbyPlayerCharacter.h"
#include "Character/SCharacterManager.h"
#include "Entity/Item/CategoryTypeId.h"
#include <Armour/ItemArmourEntity.h>
#include <Weapon/ItemWeaponEntity.h>

ALobbyPlayerCharacter::ALobbyPlayerCharacter()
	: Super()
	, bIsSwaped(false)
	, TemporaryInstance(nullptr)
{
	auto _mesh = GetMesh();
	
	_mesh->SetupAttachment(GetCapsuleComponent());
	_mesh->bReceivesDecals = false;
	_mesh->SetCollisionObjectType(ECC_Pawn);
	_mesh->SetCollisionResponseToAllChannels(ECR_Block);
	_mesh->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);

	if (!HasAnyFlags(RF_ClassDefaultObject | RF_ArchetypeObject))
	{
		_mesh->OnBeginCursorOver.AddDynamic(this, &ALobbyPlayerCharacter::OnBeginComponentCursorOver);
		_mesh->OnEndCursorOver.AddDynamic(this, &ALobbyPlayerCharacter::OnEndComponentCursorOver);
		_mesh->OnClicked.AddDynamic(this, &ALobbyPlayerCharacter::OnClickedComponentCursorOver);
	}

	GetCapsuleComponent()->SetCollisionObjectType(ECC_Pawn);
	GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECR_Block);
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Visibility, ECR_Ignore);

	bClickableCharacter = false;
}

void ALobbyPlayerCharacter::SetArmourEntity(const UItemArmourEntity* armourEntity)
{
	Super::SetArmourEntity(armourEntity);
}

void ALobbyPlayerCharacter::UnSetArmourEntitySlot(const ItemSlotTypeId::Type& slot)
{
	if (ArmourSlotMap.Contains(slot))
	{
		auto armourSlotInstance = ArmourSlotMap.FindChecked(slot);
		armourSlotInstance->SetVisibility(false);
		armourSlotInstance->SetSkeletalMesh(nullptr);
		OnSlotUnInstalled.Broadcast(static_cast<EItemSetSlotId::Type>(slot));
	}
}


void ALobbyPlayerCharacter::OnClickedComponentCursorOver(UPrimitiveComponent* Component, FKey ButtonPressed)
{
	if (bClickableCharacter && GetMesh()->IsVisible())
	{
		OnClickedCharacter.ExecuteIfBound();
	}
}

void ALobbyPlayerCharacter::OnBeginComponentCursorOver(UPrimitiveComponent* Component)
{
	if (bClickableCharacter && GetMesh()->IsVisible())
	{
		GetMesh()->SetRenderCustomDepth(true);
	}
}

void ALobbyPlayerCharacter::OnEndComponentCursorOver(UPrimitiveComponent* Component)
{
	if (bClickableCharacter && GetMesh()->IsVisible())
	{
		GetMesh()->SetRenderCustomDepth(false);
	}
}

void ALobbyPlayerCharacter::ToggleClickable(const bool Toggle)
{
	bClickableCharacter = Toggle;

	if (!bClickableCharacter)
	{
		GetMesh()->SetRenderCustomDepth(false);
	}
}

void ALobbyPlayerCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	//if (!HasAnyFlags(RF_ClassDefaultObject | RF_ArchetypeObject))
	//{
	//	if (InvertoryInstance.Num() >= InvertorySlot::hardweapon && InvertoryInstance[InvertorySlot::hardweapon])
	//	{
	//		InvertoryInstance[InvertorySlot::hardweapon]->AttachTo(GetMesh(), HardWeaponSocket);
	//	}
	//
	//	if (InvertoryInstance.Num() >= InvertorySlot::lightweapon && InvertoryInstance[InvertorySlot::lightweapon])
	//	{
	//		InvertoryInstance[InvertorySlot::lightweapon]->AttachTo(GetMesh(), LiteWeaponSocket);
	//	}
	//}
}

void ALobbyPlayerCharacter::SwapEnity(const UCharacterBaseEntity * entity)
{
	if (bIsSwaped == false)
	{
		TemporaryInstance = EntityInstance;
	}
	InitializeEntity(entity);
}

void ALobbyPlayerCharacter::RestoreEnity()
{
	if(bIsSwaped)
	{
		ensureMsgf(TemporaryInstance, ANSI_TO_TCHAR("EntityInstance was nullptr"));
		InitializeEntity(TemporaryInstance);
		bIsSwaped = false;
	}
//	else ensureMsgf(bIsSwaped, ANSI_TO_TCHAR("attemp RestoreEnity, but entity is not swap"));
}


void ALobbyPlayerCharacter::ApplyEntity(const UCharacterBaseEntity* entity)
{
	ensureMsgf(entity, ANSI_TO_TCHAR("entity was nullptr"));
	InitializeEntity(entity);
	bIsSwaped = false;
}


//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FName ALobbyPlayerCharacter::GetHardWeaponSocket() const
{
	return HardWeaponSocket;
}

FName ALobbyPlayerCharacter::GetLiteWeaponSocket() const
{
	return LiteWeaponSocket;
}

FVector ALobbyPlayerCharacter::GetSelectionLocation() const
{
	//if (SelectedComponent)
	//{
	//	FVector CSL = SelectedComponent->GetCustomLocation();
	//	if (CSL == FVector::ZeroVector && SelectedComponent->Slot >= 0 && SelectedComponent->Slot < EInvertorySlot::throwing1)
	//	{
	//		if (GetMesh()->DoesSocketExist(SelectionSockets[SelectedComponent->Slot]))
	//			return GetMesh()->GetSocketLocation(SelectionSockets[SelectedComponent->Slot]);
	//	}
	//
	//	return CSL;
	//}

	return FVector::ZeroVector;
}

//UCharacterInvertoryItem* ALobbyPlayerCharacter::GetSelectedComponent() const
//{
//	return SelectedComponent;
//}

void ALobbyPlayerCharacter::SetSelectedComponent(const int32& Slot)
{
	//if (InvertoryInstance.IsValidIndex(Slot))
	//{
	//	if (SelectedComponent) 
	//		SelectedComponent->SetRenderCustomDepth(false);
	//
	//	SelectedComponent = InvertoryInstance[Slot];
	//	SelectedComponent->SetRenderCustomDepth(true);
	//
	//	if (auto gm = GetWorld()->GetAuthGameMode<ALobbyGameMode>())
	//	{
	//		if (gm->Slate_CharacterManager.IsValid() && SelectedComponent->GetShopInstance())
	//		{
	//			auto SelectedInfo = SelectedComponent->GetShopInstance()->getInformation();
	//			TArray<UInvertoryItemBase*> Result;
	//
	//			SIZE_T _count = 0, _bNeedCount = 1;
	//			for (auto item : AssetInvertory)
	//			{
	//				auto ItemInfo = item->GetInstance()->getInformation();
	//				if (ItemInfo->category == SelectedInfo->category && ItemInfo->slot == Slot)
	//				{
	//					Result.Add(item);
	//
	//					if (_bNeedCount == 1)
	//					{
	//						if (SelectedComponent->GetInstance()->GetProperty().Index == item->GetProperty().Index)
	//						{
	//							_bNeedCount = 0;
	//						}
	//						else
	//						{
	//							++_count;
	//						}
	//					}
	//				}
	//			}
	//
	//			gm->Slate_CharacterManager->FillSlotAllowedList(Result, _count);
	//		}
	//	}
	//}
}
