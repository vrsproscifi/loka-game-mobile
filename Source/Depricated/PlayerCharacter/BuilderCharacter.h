// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "BuilderCharacter.generated.h"

class ABuildHelperComponent;
class SBuildSystem_ToolBar;
class SBuildSystem_Inventory;
class IUsableActorInterface;

UCLASS()
class LOKAGAME_API ABuilderCharacter : public ACharacter
{
	friend class ABuildPlayerController;

	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABuilderCharacter();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void MoveForward(float Value);
	void MoveRight(float Value);
	void MoveUp(float Value);
	void TurnAtRate(float Value);
	void LookUpAtRate(float Value);

	template<bool Value>
	void OnUseAny_Helper() { OnUseAny(Value); }

	UFUNCTION(Reliable, Server, WithValidation)
	void OnUseAny(const bool IsStart);

	AActor* GetUsableObject();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = System)
	FORCEINLINE bool IsAllowAnyBuildMode() const { return bIsAllowAnyBuildMode; }

	UFUNCTION(BlueprintCallable, Category = System)
	void SetAllowAnyBuildMode(const bool InAllow);

protected:

	UPROPERTY(EditDefaultsOnly, Category = System)
	TSubclassOf<ABuildHelperComponent> BuildHelperComponentClass;

	UPROPERTY(BlueprintReadOnly, Replicated, Category = System)
	ABuildHelperComponent* BuildHelperComponent;	

	UPROPERTY()
	TScriptInterface<IUsableActorInterface> UsableTargetObject;

	UPROPERTY(BlueprintReadWrite, Category = System)
	bool bIsAllowAnyBuildMode;

};
