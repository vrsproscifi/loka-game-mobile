// VRSPRO

#pragma once


#include "Character/CharacterBaseEntity.h"
#include "GameFramework/Character.h"
#include "BasePlayerCharacter.generated.h"

class UItemArmourEntity;
class UItemWeaponEntity;
class UItemModuleEntity;

DECLARE_MULTICAST_DELEGATE_OneParam(FOnArmourInstalled, const class UItemBaseEntity*);

UCLASS(MinimalAPI)
class ABasePlayerCharacter
	: public ACharacter
	
{
	GENERATED_BODY()

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		const UCharacterBaseEntity* EntityInstance;

	//==================================================================
	//							[ Property ]
	//==================================================================
public:
	const UCharacterBaseEntity* GetEntityInstance() const
	{
		return EntityInstance;
	}


	virtual void PostInitializeComponents() override;

	FString EntityName;
	FOnArmourInstalled OnArmourInstalled;
	FOnArmourInstalled OnArmourUnInstalled;

	FOnArmourInstalled OnWeaponInstalled;
	FOnArmourInstalled OnWeaponUnInstalled;

	//==================================================================
	//							[ Invertory Property ]
	//==================================================================
	UFUNCTION()
	virtual void SetArmourEntity(const UItemArmourEntity* armourEntity);
	
	UFUNCTION()
	virtual void SetArmourEntitySlot(const UItemArmourEntity* armourEntity, const ItemSlotTypeId::Type& slot);

	UFUNCTION()
	virtual void UnSetArmourEntity(const UItemArmourEntity* armourEntity);


	UFUNCTION()
	virtual void SetWeaponEntity(const UItemWeaponEntity* WeaponEntity);

	UFUNCTION()
	virtual void SetWeaponEntitySlot(const UItemWeaponEntity* WeaponEntity, const ItemSlotTypeId::Type& Slot);

	UFUNCTION()
	virtual void UnsetWeaponEntity(const UItemWeaponEntity* WeaponEntity);


	UPROPERTY()
	TMap<TEnumAsByte<ItemSlotTypeId::Type>, USkeletalMeshComponent*> ArmourSlotMap;

	UPROPERTY()
	TMap<TEnumAsByte<ItemSlotTypeId::Type>, USkeletalMeshComponent*> WeaponSlotMap;


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Invertory Property")
		USkeletalMeshComponent*		HelmetEntity;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Invertory Property")
		USkeletalMeshComponent*		MaskEntity;



	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Invertory Property")
		USkeletalMeshComponent*		GlovesEntity;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Invertory Property")
		USkeletalMeshComponent*	BackpackEntity;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Invertory Property")
		USkeletalMeshComponent*		BodyEntity;



	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Invertory Property")
		USkeletalMeshComponent*		PantsEntity;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Invertory Property")
		USkeletalMeshComponent*		BootsEntity;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Invertory Property")
		USkeletalMeshComponent*		PrimaryWeaponEntity;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Invertory Property")
		USkeletalMeshComponent*		SecondaryWeaponEntity;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "InDev")
		AActor* VisualActor;

	//==================================================================
	//							[ Methods ]
	//==================================================================

public:
	ABasePlayerCharacter();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InInputComponent) override;

	UFUNCTION()
	virtual void InitializeEntity(const UCharacterBaseEntity* entity);

	UFUNCTION()
	virtual void ApplyEntity(const UCharacterBaseEntity* entity);

	UFUNCTION()
	virtual void SwapEnity(const UCharacterBaseEntity* entity);

	UFUNCTION()
	virtual void RestoreEnity();

	UPROPERTY()
	TArray<USkeletalMeshComponent*> InstalledModulesMesh;

	UPROPERTY()
	TMap<FName, USkeletalMeshComponent*> InstalledModulesMeshMap;

	void InstallModule(UItemModuleEntity* InModule, const ItemSlotTypeId::Type& Slot);
	void UninstallModule(UItemModuleEntity* InModule, const ItemSlotTypeId::Type& Slot);
};
