#pragma once
#include "ServiceHandler.h"
#include "ServiceOperation.h"
#include "Marketing/Improvements/ImprovementCreateView.h"

namespace Operation
{
	namespace PushSentence
	{
		class Operation final 
			: public COperation<FImprovementCreateView, ObjectSerializer::JsonObject, void, ObjectSerializer::JsonObject>
		{
		public:			
			IResponseHandler<void, OperationResponseType::None> OnTimeOut;
			IResponseHandler<void, OperationResponseType::None> OnNotEnoughMoney;

			Operation() 
				: Super(FServerHost::MasterClient, "Marketing/PushSentence")
				, OnTimeOut(408, this)
				, OnNotEnoughMoney(402, this)
			{

			}
		};
	}
}
