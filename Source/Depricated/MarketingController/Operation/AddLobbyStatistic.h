#pragma once
#include "ServiceHandler.h"
#include "ServiceOperation.h"
#include "Marketing/Lobby/LobbyActionView.h"
#include "RequestOneParam.h"

namespace Operation
{
	namespace AddLobbyStatistic
	{
		class Operation final 
			: public COperation<FLobbyActionContainer, ObjectSerializer::JsonObject, FRequestOneParam<FString>, ObjectSerializer::NativeObject>
		{
		public:			
			Operation() 
				: Super(FServerHost::MasterClient, "Marketing/AddLobbyStatistic")
			{
			}

		};
	}
}
