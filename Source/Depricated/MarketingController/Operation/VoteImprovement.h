#pragma once
#include "ServiceHandler.h"
#include "ServiceOperation.h"
#include "Marketing/Improvements/ImprovementVoteCreate.h"

namespace Operation
{
	namespace VoteImprovement
	{
		class Operation final 
			: public COperation<FImprovementVoteCreate, ObjectSerializer::JsonObject, FImprovementVoteCreate, ObjectSerializer::JsonObject>
		{
		public:			
			IResponseHandler<void, OperationResponseType::None> OnNotFound;
			IResponseHandler<void, OperationResponseType::None> OnVoteIsClosed;
			IResponseHandler<void, OperationResponseType::None> OnAlreadyVoted;
			IResponseHandler<void, OperationResponseType::None> OnNotEnoughMoney;

			Operation() 
				: Super(FServerHost::MasterClient, "Marketing/VoteImprovement")
				, OnNotFound(404, this)
				, OnVoteIsClosed(406, this)
				, OnAlreadyVoted(405, this)
				, OnNotEnoughMoney(402, this)
			{

			}
		};	
	}
}
