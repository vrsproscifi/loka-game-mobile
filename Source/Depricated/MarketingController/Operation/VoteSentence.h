#pragma once
#include "ServiceHandler.h"
#include "ServiceOperation.h"
#include "Marketing/Improvements/ImprovementVoteCreate.h"

namespace Operation
{
	namespace VoteSentence
	{
		class Operation final 
			: public COperation<FImprovementVoteCreate, ObjectSerializer::JsonObject, void, ObjectSerializer::JsonObject>
		{
		public:			
			IResponseHandler<void, OperationResponseType::None> OnNotFound;
			IResponseHandler<void, OperationResponseType::None> OnVoteIsClosed;
			IResponseHandler<void, OperationResponseType::None> OnAlreadyVoted;

			Operation() 
				: Super(FServerHost::MasterClient, "Marketing/VoteSentence")
				, OnNotFound(404, this)
				, OnVoteIsClosed(406, this)
				, OnAlreadyVoted(405, this)
			{

			}
		};
	}
}
