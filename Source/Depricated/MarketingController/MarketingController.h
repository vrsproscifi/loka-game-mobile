#pragma once

#include "Operation/AddLobbyStatistic.h"
#include "Operation/RequestImprovementsList.h"
#include "Operation/VoteImprovement.h"
#include "Operation/PushSentence.h"
#include "Operation/VoteSentence.h"

DECLARE_DELEGATE_OneParam(FOnVoteAction, TSharedPtr<FImprovementView>&);

class MarketingController
{
public:
	MarketingController();
	
	Operation::AddLobbyStatistic::Operation				AddLobbyStatistic;
	Operation::RequestImprovementsList::Operation		RequestImprovementsList;
	Operation::VoteImprovement::Operation				VoteImprovement;

	Operation::VoteSentence::Operation					VoteSentence;
	Operation::PushSentence::Operation					PushSentence;
};
