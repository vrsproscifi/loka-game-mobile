// VRSPRO

#pragma once

#include "GameFramework/Actor.h"
#include "WeaponModular.generated.h"

struct EModularSockets
{
	static FName Center;
	static FName Aim;
	static FName Muzzle;
	static FName Pointer;
};

class UItemModuleEntity;
class UItemWeaponEntity;

UCLASS()
class LOKAGAME_API AWeaponModular : public AActor
{
	GENERATED_BODY()
	
public:	
	AWeaponModular(const FObjectInitializer&);

	virtual void BeginPlay() override;
	virtual void Tick( float DeltaSeconds ) override;

	FVector2D GetSocketToScreen(const FName&) const;

	UFUNCTION(BlueprintCallable, Category = Test)
	void AddRotation(const FRotator Val);
	void SetRotation(const FRotator Val);
	FRotator GetRotation() const;

	UFUNCTION(BlueprintCallable, Category = Default)
	void SetInstance(const UItemWeaponEntity* InInstance);

	UFUNCTION(BlueprintCallable, Category = Default)
	void InstallModule(UItemModuleEntity* InModule);

	UFUNCTION(BlueprintCallable, Category = Default)
	void UninstallModule(UItemModuleEntity* InModule);

	void BeginActivate(const FVector, const FRotator);
	void BeginDeactivate();

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USkeletalMeshComponent* BaseMesh;

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	UCapsuleComponent* Capsule;	

	UPROPERTY()
	TMap<FName, UItemModuleEntity*> InstalledModules;

	UPROPERTY()
	const UItemWeaponEntity* Instance;

	UPROPERTY()
	TArray<USkeletalMeshComponent*> InstalledModulesMesh;

	UPROPERTY()
	TMap<FName, USkeletalMeshComponent*> InstalledModulesMeshMap;

	FTransform TargetTransform;
	FRotator TargetRotation;
	FVector TargetPosition;
};
