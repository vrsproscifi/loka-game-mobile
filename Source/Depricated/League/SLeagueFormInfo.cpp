// VRSPRO

#include "LokaGame.h"
#include "SLeagueFormInfo.h"
#include "SlateOptMacros.h"

#include "SResourceTextBox.h"
#include "STextBlockButton.h"
#include "SLokaPopUpError.h"
#include "SWindowBackground.h"

#include "CorporationDetails.h"
#include "CorporationService.h"

#define LOCTEXT_NAMESPACE "SLeagueFormInfo"

FName FLeaguePlayerRow::ColumnNumber("Number");
FName FLeaguePlayerRow::ColumnName("Name");
FName FLeaguePlayerRow::ColumnRole("Role");
FName FLeaguePlayerRow::ColumnActivity("Activity");
FName FLeaguePlayerRow::ColumnKillsAvg("KillsAvg");

class SLeaguePlayerCommands : public TCommands<SLeaguePlayerCommands>
{
public:

	SLeaguePlayerCommands() : TCommands<SLeaguePlayerCommands>
		(
			"SLeaguePlayerCommands", // Context name for fast lookup
			NSLOCTEXT("SLeaguePlayerCommands", "SLeaguePlayerCommands", "SLeaguePlayerCommands"), // Localized context name for displaying
			NAME_None, FCoreStyle::Get().GetStyleSetName()
		)
	{}

	TSharedPtr<FUICommandInfo> SetRoleModerator;
	TSharedPtr<FUICommandInfo> SetRoleCommander;
	TSharedPtr<FUICommandInfo> SetRolePlayer;
	TSharedPtr<FUICommandInfo> Delete;

	virtual void RegisterCommands() override
	{
		FUICommandInfo::MakeCommandInfo(
			this->AsShared(),
			SetRolePlayer,
			TEXT("SetRolePlayer"),
			ELeaguePlayerRole::Localization[ELeaguePlayerRole::Player],
			FText::GetEmpty(),
			FSlateIcon(),
			EUserInterfaceActionType::Button,
			FInputChord()
		);

		FUICommandInfo::MakeCommandInfo(
			this->AsShared(),
			SetRoleCommander,
			TEXT("SetRoleCommander"),
			ELeaguePlayerRole::Localization[ELeaguePlayerRole::Commander],
			FText::GetEmpty(),
			FSlateIcon(),
			EUserInterfaceActionType::Button,
			FInputChord()
		);

		FUICommandInfo::MakeCommandInfo(
			this->AsShared(),
			SetRoleModerator,
			TEXT("SetRoleModerator"),
			ELeaguePlayerRole::Localization[ELeaguePlayerRole::Moderator],
			FText::GetEmpty(),
			FSlateIcon(),
			EUserInterfaceActionType::Button,
			FInputChord()
		);

		FUICommandInfo::MakeCommandInfo(
			this->AsShared(),
			Delete,
			TEXT("Delete"),
			NSLOCTEXT("SLeaguePlayerCommands", "Command.Delete", "Kick From League"),
			FText::GetEmpty(),
			FSlateIcon(),
			EUserInterfaceActionType::Button,
			FInputChord()
		);
	}
};

class SLeaguePlayerRow : public SMultiColumnTableRow< TSharedPtr<FLeaguePlayerRow> >
{
public:

	SLATE_BEGIN_ARGS(SLeaguePlayerRow)
		: _Item()
		, _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FLeagueFormInfoStyle>("SLeagueFormInfoStyle"))
	{
		_Visibility = EVisibility::Visible;
	}
	SLATE_STYLE_ARGUMENT(FLeagueFormInfoStyle, Style)
	SLATE_EVENT(FOnCanAcceptDrop, OnCanAcceptDrop)
	SLATE_EVENT(FOnAcceptDrop, OnAcceptDrop)
	SLATE_EVENT(FOnDragDetected, OnDragDetected)
	SLATE_ARGUMENT(TSharedPtr<FLeaguePlayerRow>, Item)
	SLATE_END_ARGS()


	virtual TSharedRef<SWidget> GenerateWidgetForColumn(const FName& ColumnName) override
	{
		if (ColumnName == FLeaguePlayerRow::ColumnNumber)
		{
			return SNew(STextBlock).Visibility(EVisibility::HitTestInvisible).TextStyle(&Style->TableRowText).Text(FText::AsNumber(Item->Number, &FNumberFormattingOptions::DefaultNoGrouping()));
		}
		else if (ColumnName == FLeaguePlayerRow::ColumnName)
		{
			return SNew(STextBlock).Visibility(EVisibility::HitTestInvisible).TextStyle(&Style->TableRowText).Text(FText::FromString(Item->Name));
		}
		else if (ColumnName == FLeaguePlayerRow::ColumnRole)
		{
			return SNew(STextBlock).Visibility(EVisibility::HitTestInvisible).TextStyle(&Style->TableRowText).Text(ELeaguePlayerRole::Localization[Item->Role]);
		}
		else if (ColumnName == FLeaguePlayerRow::ColumnActivity)
		{
			FTimespan Timespan = FTimespan::FromSeconds(Item->LastActivity);

			FText LastTime = NSLOCTEXT("SLeaguePlayerRow", "LastActivity.Online", "Online");			

			if (Timespan.GetTotalDays() > 1)
			{
				LastTime = FText::Format(NSLOCTEXT("SLeaguePlayerRow", "LastActivity.Days", "{0} Days later"), FText::AsNumber(static_cast<int32>(Timespan.GetTotalDays())));
			}
			else if (Timespan.GetTotalHours() > 1)
			{
				LastTime = FText::Format(NSLOCTEXT("SLeaguePlayerRow", "LastActivity.Hours", "{0} Hours later"), FText::AsNumber(static_cast<int32>(Timespan.GetTotalHours())));
			}
			else if (Timespan.GetTotalMinutes() > 1)
			{
				LastTime = FText::Format(NSLOCTEXT("SLeaguePlayerRow", "LastActivity.Minutes", "{0} Minutes later"), FText::AsNumber(static_cast<int32>(Timespan.GetTotalMinutes())));
			}

			return SNew(STextBlock).Visibility(EVisibility::HitTestInvisible).TextStyle(&Style->TableRowText).Text(LastTime);
		}
		else
		{
			return SNew(STextBlock).Visibility(EVisibility::HitTestInvisible).TextStyle(&Style->TableRowText).Text(FText::Format(NSLOCTEXT("SLeaguePlayerRow", "UnsupprtedColumnText", "Unsupported Column: {0}"), FText::FromName(ColumnName)));
		}
	}

	void Construct(const FArguments& InArgs, const TSharedRef<STableViewBase>& InOwnerTableView)
	{
		Item = InArgs._Item;
		Style = InArgs._Style;

		FSuperRowType::Construct(FSuperRowType::FArguments()
			.OnCanAcceptDrop(InArgs._OnCanAcceptDrop)
			.OnAcceptDrop(InArgs._OnAcceptDrop)
			.OnDragDetected(InArgs._OnDragDetected)
			.ShowSelection(true)
			.Padding(1)
			.Style(&Style->TableRow)
			, InOwnerTableView);
	}

private:
	
	TSharedPtr<FLeaguePlayerRow> Item;
	const FLeagueFormInfoStyle *Style;

	virtual FReply OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override
	{
		FReply Reply = FSuperRowType::OnMouseButtonDown(MyGeometry, MouseEvent);
		TSharedRef< STableViewBase > OwnerTableViewBase = StaticCastSharedPtr< SListView<TSharedPtr<FLeaguePlayerRow>> >(OwnerTablePtr.Pin()).ToSharedRef();
		if (MouseEvent.GetEffectingButton() == EKeys::RightMouseButton && !OwnerTableViewBase->IsRightClickScrolling())
		{
			return FReply::Handled();
		}

		return Reply;
	}
};

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SLeagueFormInfo::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	OnLeagueEdit = InArgs._OnLeagueEdit;
	OnLeagueKick = InArgs._OnLeagueKick;
	OnLeagueDonate = InArgs._OnLeagueDonate;
	OnLeagueSetRole = InArgs._OnLeagueSetRole;

	BackupEditableData = new FCorporationEdit();

	MAKE_UTF8_SYMBOL(strAccept, 0xf00c);
	MAKE_UTF8_SYMBOL(strCancel, 0xf00d);

	bToDonateMoneyState = false;
	bToDonateDonateState = false;

	ChildSlot
	[
		SNew(SWindowBackground)
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
			.FillWidth(Style->InfoBlockWidth)
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot().FillHeight(Style->InfoBlockHeights.IconAndBaseInfo)
				[
					SNew(SBorder)
					.BorderImage(&Style->InfoBackgrounds.IconAndBaseInfo)
					[
						SNew(SHorizontalBox)
						+ SHorizontalBox::Slot()
						.AutoWidth()
						.Padding(45)
						.HAlign(EHorizontalAlignment::HAlign_Center)
						.VAlign(EVerticalAlignment::VAlign_Center)
						[
							SNew(SImage)
							.Image(&Style->DefaultImage)
						]
						+ SHorizontalBox::Slot().VAlign(EVerticalAlignment::VAlign_Center)
						[
							SNew(SVerticalBox)
							+ SVerticalBox::Slot().AutoHeight()
							[
								SNew(SHorizontalBox)
								+ SHorizontalBox::Slot().FillWidth(.4f)
								[
									SAssignNew(TextAbbr, SEditableTextBox) //Abbr
									.ErrorReporting(SNew(SLokaPopUpError))
									.Style(&FLokaSlateStyle::Get().GetWidgetStyle<FEditableTextBoxStyle>("Default_EditableTextBox"))
									.OnTextChanged_Lambda([&](const FText &) { TextAbbr->SetError(""); EditCtrlContainer->SetVisibility(EVisibility::Visible); TextAbbr->SetText(FText::FromString(TextAbbr->GetText().ToString().ToUpper())); })
								]
								+ SHorizontalBox::Slot().FillWidth(1).Padding(FMargin(0,0,40,0))
								[
									SAssignNew(TextName, SEditableTextBox) //Name
									.ErrorReporting(SNew(SLokaPopUpError))
									.Style(&FLokaSlateStyle::Get().GetWidgetStyle<FEditableTextBoxStyle>("Default_EditableTextBox"))
									.OnTextChanged_Lambda([&](const FText &) { TextName->SetError(""); EditCtrlContainer->SetVisibility(EVisibility::Visible); })
								]
							]
							+ SVerticalBox::Slot().AutoHeight()
							[
								SNew(SHorizontalBox)
								+ SHorizontalBox::Slot()
								[
									SNew(STextBlock).Text(NSLOCTEXT("SLeagueFormInfo","League.Founded","Founded:")).TextStyle(&Style->InfoHeadText)
								]
								+ SHorizontalBox::Slot().Padding(FMargin(0, 0, 40, 0))
								[
									SAssignNew(TextFounded, STextBlock) //Founded
									.TextStyle(&Style->InfoRowText)
								]
							]
							+ SVerticalBox::Slot().AutoHeight()
							[
								SNew(SHorizontalBox)
								+ SHorizontalBox::Slot()
								[
									SNew(STextBlock).Text(NSLOCTEXT("SLeagueFormInfo", "League.Rating", "Rating:")).TextStyle(&Style->InfoHeadText)
								]
								+ SHorizontalBox::Slot().Padding(FMargin(0, 0, 40, 0))
								[
									SAssignNew(TextRating, STextBlock) //Rating
									.TextStyle(&Style->InfoRowText)
								]
							]
							+ SVerticalBox::Slot().AutoHeight()
							[
								SNew(SHorizontalBox) //JoinPrice
								+ SHorizontalBox::Slot()
								[
									SNew(STextBlock).Text(NSLOCTEXT("SLeagueFormInfo", "League.JoinPrice", "Join price:")).TextStyle(&Style->InfoHeadText)
								]
								+ SHorizontalBox::Slot().Padding(FMargin(0, 0, 40, 0))
								[
									SAssignNew(JoinPriceSwitcher, SWidgetSwitcher)
									+ SWidgetSwitcher::Slot()
									[
										SAssignNew(TextJoinPrice, STextBlock)
										.TextStyle(&Style->InfoRowText)
									]
									+ SWidgetSwitcher::Slot()
									[
										SAssignNew(SpinJoinPrice, SSpinBox<int32>)
										.OnValueCommitted_Lambda([&](int32, ETextCommit::Type) { EditCtrlContainer->SetVisibility(EVisibility::Visible); })
										.Delta(1)
										.MinSliderValue(0)
										.MinValue(0)
										.MaxSliderValue(100000)
										.MaxValue(1000000)
									]
								]
							]
							+ SVerticalBox::Slot().AutoHeight()
							[
								SNew(SHorizontalBox)
								+ SHorizontalBox::Slot()
								.FillWidth(1)
								[
									SNew(STextBlock).Text(NSLOCTEXT("SLeagueFormInfo", "League.Type", "League type:")).TextStyle(&Style->InfoHeadText)
								]
								+ SHorizontalBox::Slot()
								.AutoWidth()
								[
									SAssignNew(CheckBox_Open, SCheckBox)
									.Style(&Style->CheckBoxes)
									.Type(ESlateCheckBoxType::ToggleButton)
									.IsChecked(ECheckBoxState::Checked)
									.OnCheckStateChanged(this, &SLeagueFormInfo::OnTypeChanged, true)
									[
										SNew(STextBlock)
										.TextStyle(&Style->InfoRowText)
										.Justification(ETextJustify::Center)
										.Text(NSLOCTEXT("SLeagueFormInfo", "League.Public", "Public"))
									]
								]
								+ SHorizontalBox::Slot().Padding(FMargin(0, 0, 40, 0))
								.AutoWidth()
								[
									SAssignNew(CheckBox_Close, SCheckBox)
									.Style(&Style->CheckBoxes)
									.Type(ESlateCheckBoxType::ToggleButton)
									.OnCheckStateChanged(this, &SLeagueFormInfo::OnTypeChanged, false)
									[
										SNew(STextBlock)
										.TextStyle(&Style->InfoRowText)
										.Justification(ETextJustify::Center)
										.Text(NSLOCTEXT("SLeagueFormInfo", "League.Private", "Private"))
									]
								]
							]
							+ SVerticalBox::Slot().AutoHeight()
							[
								SAssignNew(EditCtrlContainer, SHorizontalBox)
								.Visibility(EVisibility::Hidden)
								+ SHorizontalBox::Slot()
								[
									SNew(SButton)
									.HAlign(EHorizontalAlignment::HAlign_Center)
									.VAlign(EVerticalAlignment::VAlign_Center)
									.Text(NSLOCTEXT("SLeagueFormInfo", "League.Save", "Save"))
									.TextStyle(&Style->InfoHeadText)
									.OnClicked(this, &SLeagueFormInfo::onClickedEditCtrl, true)
								]
								+ SHorizontalBox::Slot()
								[
									SNew(SButton)
									.HAlign(EHorizontalAlignment::HAlign_Center)
									.VAlign(EVerticalAlignment::VAlign_Center)
									.Text(NSLOCTEXT("SLeagueFormInfo", "League.Cancel", "Cancel"))
									.TextStyle(&Style->InfoHeadText)
									.OnClicked(this, &SLeagueFormInfo::onClickedEditCtrl, false)
								]
							]
						]
					]
				]
				+ SVerticalBox::Slot().FillHeight(Style->InfoBlockHeights.MoneyAndButtons)
				[
					SNew(SBorder)
					.BorderImage(&Style->InfoBackgrounds.MoneyAndButtons)
					.VAlign(EVerticalAlignment::VAlign_Center)
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot().Padding(FMargin(20, 4, 4, 4))
						[
							SNew(SHorizontalBox)
							+ SHorizontalBox::Slot()
							[
								SAssignNew(Resource_Money, SResourceTextBox).Type(EResourceTextBoxType::Money)
							]
							+ SHorizontalBox::Slot()
							[
								SNew(SWidgetSwitcher)
								.WidgetIndex(this, &SLeagueFormInfo::ToDonateMoneyWidget)
								+ SWidgetSwitcher::Slot()
								[
									SNew(STextBlockButton)
									.Style(&Style->ToDonate)
									.Text(NSLOCTEXT("SLeagueFormInfo", "League.ToDonate", "To Donate"))
									.OnClicked_Lambda([&] { bToDonateMoneyState = true; return FReply::Handled(); })
								]
								+ SWidgetSwitcher::Slot()
								[
									SNew(SHorizontalBox)
									+ SHorizontalBox::Slot().FillWidth(1)
									[
										SAssignNew(SpinBoxMoney, SSpinBox<int32>)
										.MinSliderValue(1)
										.MinValue(1)
										.MaxSliderValue(InArgs._PlayerMoney)
										.MaxValue(InArgs._PlayerMoney)
									]
									+ SHorizontalBox::Slot().AutoWidth().VAlign(VAlign_Center).HAlign(HAlign_Center)
									[
										SNew(STextBlockButton)
										.Style(&Style->ToDonateAccept)
										.Text(FText::FromString(strAccept))
										.OnClicked_Lambda([&] { OnLeagueDonate.ExecuteIfBound(false, SpinBoxMoney->GetValue()); bToDonateMoneyState = false; return FReply::Handled(); })
									]
									+ SHorizontalBox::Slot().AutoWidth().VAlign(VAlign_Center).HAlign(HAlign_Center)
									[
										SNew(STextBlockButton)
										.Style(&Style->ToDonateCancel)
										.Text(FText::FromString(strCancel))
										.OnClicked_Lambda([&] { bToDonateMoneyState = false; return FReply::Handled(); })
									]
								]
							]
						]
						+ SVerticalBox::Slot().Padding(FMargin(20, 4, 4, 4))
						[
							SNew(SHorizontalBox)
							+ SHorizontalBox::Slot()
							[
								SAssignNew(Resource_Donate, SResourceTextBox).Type(EResourceTextBoxType::Donate)
							]
							+ SHorizontalBox::Slot()
							[
								SNew(SWidgetSwitcher)
								.WidgetIndex(this, &SLeagueFormInfo::ToDonateDonateWidget)
								+ SWidgetSwitcher::Slot()
								[
									SNew(STextBlockButton)
									.Style(&Style->ToDonate)
									.Text(NSLOCTEXT("SLeagueFormInfo", "League.ToDonate", "To Donate"))
									.OnClicked_Lambda([&] { bToDonateDonateState = true; return FReply::Handled(); })
								]
								+ SWidgetSwitcher::Slot()
								[
									SNew(SHorizontalBox)
									+ SHorizontalBox::Slot().FillWidth(1)
									[
										SAssignNew(SpinBoxDonate, SSpinBox<int32>)
										.MinSliderValue(1)
										.MinValue(1)
										.MaxSliderValue(InArgs._PlayerDonate)
										.MaxValue(InArgs._PlayerDonate)
									]
									+ SHorizontalBox::Slot().AutoWidth().VAlign(VAlign_Center).HAlign(HAlign_Center)
									[
										SNew(STextBlockButton)
										.Style(&Style->ToDonateAccept)
										.Text(FText::FromString(strAccept))
										.OnClicked_Lambda([&] { OnLeagueDonate.ExecuteIfBound(true, SpinBoxDonate->GetValue()); bToDonateDonateState = false; return FReply::Handled(); })
									]
									+ SHorizontalBox::Slot().AutoWidth().VAlign(VAlign_Center).HAlign(HAlign_Center)
									[
										SNew(STextBlockButton)
										.Style(&Style->ToDonateCancel)
										.Text(FText::FromString(strCancel))
										.OnClicked_Lambda([&] { bToDonateDonateState = false; return FReply::Handled(); })
									]
								]
							]
						]
					]
				]
				+ SVerticalBox::Slot().FillHeight(Style->InfoBlockHeights.Events)
				[
					SNew(SBorder)
					.BorderImage(&Style->InfoBackgrounds.Events)
					//.HAlign(EHorizontalAlignment::HAlign_Center)
					//.VAlign(EVerticalAlignment::VAlign_Center)
					//[
					//	SNew(SDPIScaler).DPIScale(4)
					//	[
					//		SNew(STextBlock).TextStyle(&Style->InfoRowText).Text(FText::FromString("Coming soon..."))
					//	]
					//]
				]
			]
			+ SHorizontalBox::Slot()
			.AutoWidth()
			.VAlign(VAlign_Fill)
			.HAlign(HAlign_Center)
			[
				SNew(SSeparator)
				.Orientation(EOrientation::Orient_Vertical)
				.Thickness(4)
			]
			+ SHorizontalBox::Slot()
			.FillWidth(1)
			.Padding(FMargin(4))
			[
				SAssignNew(PlayersListWidget, SListView<TSharedPtr<FLeaguePlayerRow>>)
				.ConsumeMouseWheel(EConsumeMouseWheel::WhenScrollingPossible)
				.SelectionMode(ESelectionMode::Single)
				.OnGenerateRow(this, &SLeagueFormInfo::OnGeneratePlayerRow)
				.OnContextMenuOpening(this, &SLeagueFormInfo::GetListContextMenu)
				.ListItemsSource(&PlayersList)
				.Visibility(EVisibility::Visible)
				.HeaderRow
				(
					SNew(SHeaderRow).Style(&Style->HeaderRow)
					+ SHeaderRow::Column(FLeaguePlayerRow::ColumnNumber).FillWidth(0.2f).OnSort(this, &SLeagueFormInfo::OnPlayersListSort).SortMode(this, &SLeagueFormInfo::GetColumnSortMode, FLeaguePlayerRow::ColumnNumber)
					[
						SNew(STextBlock).TextStyle(&Style->TableHeadText).Text(NSLOCTEXT("SLeagueFormInfo", "TableHeader.Number", "#"))
					]
					+ SHeaderRow::Column(FLeaguePlayerRow::ColumnName).FillWidth(1).OnSort(this, &SLeagueFormInfo::OnPlayersListSort).SortMode(this, &SLeagueFormInfo::GetColumnSortMode, FLeaguePlayerRow::ColumnName)
					[
						SNew(STextBlock).TextStyle(&Style->TableHeadText).Text(NSLOCTEXT("SLeagueFormInfo", "TableHeader.Name", "Player Name"))
					]
					+ SHeaderRow::Column(FLeaguePlayerRow::ColumnRole).FillWidth(1).OnSort(this, &SLeagueFormInfo::OnPlayersListSort).SortMode(this, &SLeagueFormInfo::GetColumnSortMode, FLeaguePlayerRow::ColumnRole)
					[
						SNew(STextBlock).TextStyle(&Style->TableHeadText).Text(NSLOCTEXT("SLeagueFormInfo", "TableHeader.Role", "Player Role"))
					]
					+ SHeaderRow::Column(FLeaguePlayerRow::ColumnActivity).FillWidth(1).OnSort(this, &SLeagueFormInfo::OnPlayersListSort).SortMode(this, &SLeagueFormInfo::GetColumnSortMode, FLeaguePlayerRow::ColumnActivity)
					[
						SNew(STextBlock).TextStyle(&Style->TableHeadText).Text(NSLOCTEXT("SLeagueFormInfo", "TableHeader.Activity", "Last Activity"))
					]
				)
			]
		]
	];

	SLeaguePlayerCommands::Register();

	UICommandList = MakeShareable(new FUICommandList());

	UICommandList->MapAction(SLeaguePlayerCommands::Get().Delete,
		FExecuteAction::CreateSP(this, &SLeagueFormInfo::DeleteMembers),
		FCanExecuteAction::CreateSP(this, &SLeagueFormInfo::CanDeleteMembers));

	UICommandList->MapAction(SLeaguePlayerCommands::Get().SetRolePlayer,
		FExecuteAction::CreateSP(this, &SLeagueFormInfo::SetMemberRole, ELeaguePlayerRole::Player),
		FCanExecuteAction::CreateSP(this, &SLeagueFormInfo::CanSetMemberRole, ELeaguePlayerRole::Player));

	UICommandList->MapAction(SLeaguePlayerCommands::Get().SetRoleCommander,
		FExecuteAction::CreateSP(this, &SLeagueFormInfo::SetMemberRole, ELeaguePlayerRole::Commander),
		FCanExecuteAction::CreateSP(this, &SLeagueFormInfo::CanSetMemberRole, ELeaguePlayerRole::Commander));

	UICommandList->MapAction(SLeaguePlayerCommands::Get().SetRoleModerator,
		FExecuteAction::CreateSP(this, &SLeagueFormInfo::SetMemberRole, ELeaguePlayerRole::Moderator),
		FCanExecuteAction::CreateSP(this, &SLeagueFormInfo::CanSetMemberRole, ELeaguePlayerRole::Moderator));

	SwitchView(ELeaguePlayerRole::Player);	
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

TSharedRef<ITableRow> SLeagueFormInfo::OnGeneratePlayerRow(TSharedPtr<FLeaguePlayerRow> InItem, const TSharedRef<STableViewBase>& OwnerTable)
{
	return SNew(SLeaguePlayerRow, OwnerTable).Item(InItem);
}

EColumnSortMode::Type SLeagueFormInfo::GetColumnSortMode(const FName ColumnName) const
{
	if (SortByColumn != ColumnName)
	{
		return EColumnSortMode::None;
	}

	return SortMode;
}

void SLeagueFormInfo::OnPlayersListSort(EColumnSortPriority::Type SortPriority, const FName& ColumnName, EColumnSortMode::Type InSortMode)
{
	SortByColumn = ColumnName;
	SortMode = InSortMode;

	StartSort();
}

void SLeagueFormInfo::StartSort()
{
	if (SortByColumn == FLeaguePlayerRow::ColumnNumber)
	{
		if (SortMode == EColumnSortMode::Ascending)
			PlayersList.Sort([](const TSharedPtr<FLeaguePlayerRow>& A, const TSharedPtr<FLeaguePlayerRow>& B) { return A->Number <= B->Number; });
		else
			PlayersList.Sort([](const TSharedPtr<FLeaguePlayerRow>& A, const TSharedPtr<FLeaguePlayerRow>& B) { return A->Number >= B->Number; });
	}
	else if (SortByColumn == FLeaguePlayerRow::ColumnName)
	{
		if (SortMode == EColumnSortMode::Ascending)
			PlayersList.Sort([](const TSharedPtr<FLeaguePlayerRow>& A, const TSharedPtr<FLeaguePlayerRow>& B) { return A->Name <= B->Name; });
		else
			PlayersList.Sort([](const TSharedPtr<FLeaguePlayerRow>& A, const TSharedPtr<FLeaguePlayerRow>& B) { return A->Name >= B->Name; });
	}
	else if (SortByColumn == FLeaguePlayerRow::ColumnRole)
	{
		if (SortMode == EColumnSortMode::Ascending)
			PlayersList.Sort([](const TSharedPtr<FLeaguePlayerRow>& A, const TSharedPtr<FLeaguePlayerRow>& B) { return A->Role >= B->Role; });
		else
			PlayersList.Sort([](const TSharedPtr<FLeaguePlayerRow>& A, const TSharedPtr<FLeaguePlayerRow>& B) { return A->Role <= B->Role; });
	}
	else if (SortByColumn == FLeaguePlayerRow::ColumnActivity)
	{
		if (SortMode == EColumnSortMode::Ascending)
			PlayersList.Sort([](const TSharedPtr<FLeaguePlayerRow>& A, const TSharedPtr<FLeaguePlayerRow>& B) { return A->LastActivity <= B->LastActivity; });
		else
			PlayersList.Sort([](const TSharedPtr<FLeaguePlayerRow>& A, const TSharedPtr<FLeaguePlayerRow>& B) { return A->LastActivity >= B->LastActivity; });
	}

	PlayersListWidget->RequestListRefresh();
}

void SLeagueFormInfo::SetMemberRole(const ELeaguePlayerRole::Type _Type)
{
	auto SelectedItems = PlayersListWidget->GetSelectedItems();
	OnLeagueSetRole.ExecuteIfBound(SelectedItems, _Type);
}

bool SLeagueFormInfo::CanSetMemberRole(const ELeaguePlayerRole::Type _Type) const
{
	return true;
}

void SLeagueFormInfo::DeleteMembers()
{
	auto SelectedItems = PlayersListWidget->GetSelectedItems();

	if (GEngine)
	{
		for (auto item : SelectedItems)
		{
			GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::White, item->Name);
		}
	}

	OnLeagueKick.ExecuteIfBound(SelectedItems);
}

bool SLeagueFormInfo::CanDeleteMembers() const
{
	return (PlayersListWidget->GetSelectedItems().Num() > 0);
}

TSharedPtr<SWidget> SLeagueFormInfo::GetListContextMenu()
{
	auto SelectedItems = PlayersListWidget->GetSelectedItems();	
	if(SelectedItems.Num() == 0) return SNullWidget::NullWidget;

	FMenuBuilder MenuBuilder(true, UICommandList);
	{
		MenuBuilder.BeginSection("LeaderMenu", FText::FromString(SelectedItems[0]->Name));
		{
			MenuBuilder.AddWidget(SNullWidget::NullWidget, FText::GetEmpty());
		}
		MenuBuilder.EndSection();

		MenuBuilder.BeginSection("LeaderMenuSelectRole", LOCTEXT("ContextMenu.SelectRole", "Set Member Role"));
		{
			MenuBuilder.AddMenuEntry(SLeaguePlayerCommands::Get().SetRolePlayer);
			MenuBuilder.AddMenuEntry(SLeaguePlayerCommands::Get().SetRoleCommander);
			MenuBuilder.AddMenuEntry(SLeaguePlayerCommands::Get().SetRoleModerator);
		}
		MenuBuilder.EndSection();

		MenuBuilder.BeginSection("LeaderMenuDelete");
		{
			MenuBuilder.AddMenuEntry(SLeaguePlayerCommands::Get().Delete);
		}
		MenuBuilder.EndSection();
	}
	
	FLokaSlateStyle::ReGenerateContextMenuStyle();
	MenuBuilder.SetStyle(&FLokaSlateStyle::Get(), "Menu");

	if (CheckBox_Open->GetVisibility() == EVisibility::Visible)
	{
		return MenuBuilder.MakeWidget();
	}

	return SNullWidget::NullWidget;
}

void SLeagueFormInfo::OnTypeChanged(ECheckBoxState State, const bool IsOpen)
{
	if (IsOpen)
	{
		CheckBox_Open->SetIsChecked(ECheckBoxState::Checked);
		CheckBox_Close->SetIsChecked(ECheckBoxState::Unchecked);
		EditCtrlContainer->SetVisibility(EVisibility::Visible);
	}
	else
	{
		CheckBox_Close->SetIsChecked(ECheckBoxState::Checked);
		CheckBox_Open->SetIsChecked(ECheckBoxState::Unchecked);
		EditCtrlContainer->SetVisibility(EVisibility::Visible);
	}	
}

// IOnlineSlateError Interface Begin

void SLeagueFormInfo::OnErrorReportingCode(const int32 &ErrorCode)
{
	if (ErrorCode == CCorporationSerivce::CorporationNameWasExist) //CorporationNameWasExist
	{
		TextName->SetError(NSLOCTEXT("SLeagueFormInfo", "Error.Name.Exist", "This name already exist!"));
	}
	else if (ErrorCode == CCorporationSerivce::CorporationNameWasInvalid) //CorporationNameWasInvalid
	{
		TextName->SetError(NSLOCTEXT("SLeagueFormInfo", "Error.Name.Invalid", "This name invalid!"));
	}
	else if (ErrorCode == CCorporationSerivce::CorporationAbbrWasExist) //CorporationAbbrWasExist
	{
		TextAbbr->SetError(NSLOCTEXT("SLeagueFormInfo", "Error.Abbr.Exist", "This abbreviation already exist!"));
	}
	else if (ErrorCode == CCorporationSerivce::CorporationAbbrWasInvalid) //CorporationAbbrWasInvalid
	{
		TextAbbr->SetError(NSLOCTEXT("SLeagueFormInfo", "Error.Abbr.Invalid", "This abbreviation invalid!"));
	}
}

bool SLeagueFormInfo::IsExistCode(const int32 &ErrorCode) const
{
	return (ErrorCode >= CCorporationSerivce::CorporationNameWasExist && ErrorCode <= CCorporationSerivce::CorporationAbbrWasInvalid);
}

// IOnlineSlateError Interface End

FReply SLeagueFormInfo::onClickedEditCtrl(const bool IsSave)
{
	if (IsSave)
	{
		FCorporationEdit ToSave;

		ToSave.Access = !(CheckBox_Open->GetCheckedState() == ECheckBoxState::Checked);
		ToSave.Name = TextName->GetText().ToString();
		ToSave.Abbr = TextAbbr->GetText().ToString();
		ToSave.JoinPrice = SpinJoinPrice->GetValue();

		OnLeagueEdit.ExecuteIfBound(ToSave);
	}
	else
	{
		SetIsAcceptEditing(false);
	}

	EditCtrlContainer->SetVisibility(EVisibility::Hidden);

	return FReply::Handled();
}

//UI_COMMAND( SubmitToSourceControl, "Submit to Source Control...", "Opens a dialog with check in options for content and levels.", EUserInterfaceActionType::Button, FInputChord());

//========================================================================================================================================================================] Public Functions

void SLeagueFormInfo::SortBy(const FName &ColumnId, const EColumnSortMode::Type SortDirection)
{
	SortByColumn = ColumnId;
	SortMode = SortDirection;

	StartSort();
}

void SLeagueFormInfo::AppendRow(const FLeaguePlayerRow &Row)
{
	PlayersMap.Add(Row.Index, PlayersList.Add(MakeShareable(new FLeaguePlayerRow(Row))));

	StartSort();
}

void SLeagueFormInfo::AppendRows(const TArray<FLeaguePlayerRow> &Rows)
{
	for (auto Row : Rows)
	{
		PlayersMap.Add(Row.Index, PlayersList.Add(MakeShareable(new FLeaguePlayerRow(Row))));
	}

	StartSort();
}

void SLeagueFormInfo::ClearRows()
{
	PlayersMap.Empty();
	PlayersList.Empty();
	PlayersListWidget->RequestListRefresh();
}

void SLeagueFormInfo::SetInfo(const ELeagueInfoRow::Type Row, const FText& Info)
{
	if (Row == ELeagueInfoRow::Name)
	{
		TextName->SetText(Info);
		BackupEditableData->Name = Info.ToString();
	}
	else if (Row == ELeagueInfoRow::Founded)
	{
		TextFounded->SetText(Info);
	}		
	else if (Row == ELeagueInfoRow::IsPrivate)
	{
		const auto value = !Info.IsEmpty();
		BackupEditableData->Access = value;

		if (value)
		{
			CheckBox_Open->SetIsChecked(ECheckBoxState::Checked);
			CheckBox_Close->SetIsChecked(ECheckBoxState::Unchecked);
		}
		else
		{
			CheckBox_Open->SetIsChecked(ECheckBoxState::Unchecked);
			CheckBox_Close->SetIsChecked(ECheckBoxState::Checked);
		}
	}	
	else if (Row == ELeagueInfoRow::Abbr)
	{
		TextAbbr->SetText(Info);
		BackupEditableData->Abbr = Info.ToString();
	}

	EditCtrlContainer->SetVisibility(EVisibility::Hidden);
}

void SLeagueFormInfo::SetInfo(const ELeagueInfoRow::Type Row, const int32& Info)
{
	if (Row == ELeagueInfoRow::JoinPrice)
	{
		TextJoinPrice->SetText(FText::AsNumber(Info));
		SpinJoinPrice->SetValue(Info);
		BackupEditableData->JoinPrice = Info;
	}
	else if (Row == ELeagueInfoRow::Money)
	{
		Resource_Money->SetValue(Info);
	}
	else if (Row == ELeagueInfoRow::Donate)
	{
		Resource_Donate->SetValue(Info);
	}
	else if (Row == ELeagueInfoRow::Rating)
	{
		TextRating->SetText(FText::AsNumber(Info));
	}
}

void SLeagueFormInfo::SwitchView(const ELeaguePlayerRole::Type View)
{
	if (View >= ELeaguePlayerRole::Moderator)
	{
		JoinPriceSwitcher->SetActiveWidgetIndex(1);
		TextName->SetIsReadOnly(false);
		TextAbbr->SetIsReadOnly(false);
		CheckBox_Open->SetVisibility(EVisibility::Visible);
		CheckBox_Close->SetVisibility(EVisibility::Visible);
	}
	else
	{
		JoinPriceSwitcher->SetActiveWidgetIndex(0);
		TextName->SetIsReadOnly(true);
		TextAbbr->SetIsReadOnly(true);
		CheckBox_Open->SetVisibility(EVisibility::HitTestInvisible);
		CheckBox_Close->SetVisibility(EVisibility::HitTestInvisible);
	}
}

void SLeagueFormInfo::SetIsAcceptEditing(const bool IsAccept)
{
	if (IsAccept)
	{
		BackupEditableData->Access = !(CheckBox_Open->GetCheckedState() == ECheckBoxState::Checked);
		BackupEditableData->Name = TextName->GetText().ToString();
		BackupEditableData->Abbr = TextAbbr->GetText().ToString();
		BackupEditableData->JoinPrice = SpinJoinPrice->GetValue();
	}
	else
	{
		SetInfo(ELeagueInfoRow::IsPrivate, BackupEditableData->Access ? FText::AsNumber(1) : FText::GetEmpty());
		SetInfo(ELeagueInfoRow::Name, FText::FromString(BackupEditableData->Name));
		SetInfo(ELeagueInfoRow::Abbr, FText::FromString(BackupEditableData->Abbr));
		SetInfo(ELeagueInfoRow::JoinPrice, FText::AsNumber(BackupEditableData->JoinPrice));
	}
}

#undef LOCTEXT_NAMESPACE