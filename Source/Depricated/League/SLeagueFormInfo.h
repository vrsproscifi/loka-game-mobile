// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Slate/Styles/LeagueFormInfoWidgetStyle.h"

#include "Slate/Components/IOnlineSlateError.h"

namespace ELeaguePlayerRole
{
	enum Type
	{
		Player,
		Commander,
		Moderator,
		Leader,
		End
	};

	static FText Localization[] = {
		NSLOCTEXT("ELeguePlayerRole", "Role.Player", "Member"),
		NSLOCTEXT("ELeguePlayerRole", "Role.Commander", "Commander"),
		NSLOCTEXT("ELeguePlayerRole", "Role.Moderator", "Moderator"),
		NSLOCTEXT("ELeguePlayerRole", "Role.Leader", "Leader")
	};
}

namespace ELeagueInfoRow
{
	enum Type
	{
		Name,
		Founded,
		Rating,
		JoinPrice,
		IsPrivate,
		Money,
		Donate,
		Abbr,
		End
	};
}

struct FLeaguePlayerRow
{
	int32 Index;
	int32 Number;
	FString Name;
	int32 Role;
	int32 LastActivity;
	float KillsAvg;

	static FName ColumnNumber;
	static FName ColumnName;
	static FName ColumnRole;
	static FName ColumnActivity;
	static FName ColumnKillsAvg;
};

DECLARE_DELEGATE_OneParam(FOnLeagueEdit, const struct FCorporationEdit&)
DECLARE_DELEGATE_OneParam(FOnLeagueKick, const TArray<TSharedPtr<FLeaguePlayerRow>>)
DECLARE_DELEGATE_TwoParams(FOnLeagueDonate, const bool, const int32)
DECLARE_DELEGATE_TwoParams(FOnLeagueSetRole, const TArray<TSharedPtr<FLeaguePlayerRow>>, const ELeaguePlayerRole::Type)

class LOKAGAME_API SLeagueFormInfo : public SCompoundWidget, public IOnlineSlateError
{
public:
	SLATE_BEGIN_ARGS(SLeagueFormInfo)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FLeagueFormInfoStyle>("SLeagueFormInfoStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FLeagueFormInfoStyle, Style)
	SLATE_ATTRIBUTE(TOptional<int32>, PlayerMoney)
	SLATE_ATTRIBUTE(TOptional<int32>, PlayerDonate)
	SLATE_EVENT(FOnLeagueEdit, OnLeagueEdit)
	SLATE_EVENT(FOnLeagueKick, OnLeagueKick)
	SLATE_EVENT(FOnLeagueDonate, OnLeagueDonate)
	SLATE_EVENT(FOnLeagueSetRole, OnLeagueSetRole)
	SLATE_END_ARGS()


	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

	void StartSort();
	void SortBy(const FName &ColumnId, const EColumnSortMode::Type SortDirection);
	void AppendRow(const FLeaguePlayerRow &Row);
	void AppendRows(const TArray<FLeaguePlayerRow> &Rows);
	void ClearRows();
	void SetInfo(const ELeagueInfoRow::Type Row, const FText& Info);
	void SetInfo(const ELeagueInfoRow::Type Row, const int32& Info);
	void SwitchView(const ELeaguePlayerRole::Type);
	void SetIsAcceptEditing(const bool);
	
	bool IsEditing() const
	{
		return (EditCtrlContainer->GetVisibility() == EVisibility::Visible);
	}

	TArray<TSharedPtr<FLeaguePlayerRow>> PlayersList;
	TMap<int32, int32> PlayersMap;
protected:
	
	// IOnlineSlateError Interface Begin

	virtual void OnErrorReportingCode(const int32 &ErrorCode) override;
	virtual bool IsExistCode(const int32 &ErrorCode) const override;

	// IOnlineSlateError Interface End

	TSharedPtr<SWidgetSwitcher> JoinPriceSwitcher;

	TSharedPtr<STextBlock> TextJoinPrice;
	TSharedPtr<SSpinBox<int32>> SpinJoinPrice;
	TSharedPtr<STextBlock> TextFounded;
	TSharedPtr<STextBlock> TextRating;

	TSharedPtr<SCheckBox> CheckBox_Open;
	TSharedPtr<SCheckBox> CheckBox_Close;

	TSharedPtr<SHorizontalBox> EditCtrlContainer;

	TSharedPtr<class SResourceTextBox> Resource_Money;
	TSharedPtr<class SResourceTextBox> Resource_Donate;

	TSharedPtr<class SEditableTextBox> TextName;
	TSharedPtr<class SEditableTextBox> TextAbbr;

	TSharedPtr< FUICommandList > UICommandList;

	FName SortByColumn;
	EColumnSortMode::Type SortMode;

	TSharedPtr<SListView<TSharedPtr<FLeaguePlayerRow>>> PlayersListWidget;

	TSharedRef<ITableRow> OnGeneratePlayerRow(TSharedPtr<FLeaguePlayerRow> InItem, const TSharedRef<STableViewBase>& OwnerTable);
	void OnPlayersListSort(EColumnSortPriority::Type, const FName&, EColumnSortMode::Type);
	TSharedPtr<SWidget> GetListContextMenu();	

	EColumnSortMode::Type GetColumnSortMode(const FName ColumnName) const;

	void OnTypeChanged(ECheckBoxState, const bool);

	const FLeagueFormInfoStyle *Style;

	FOnLeagueEdit OnLeagueEdit;
	FOnLeagueKick OnLeagueKick;
	FOnLeagueDonate OnLeagueDonate;
	FOnLeagueSetRole OnLeagueSetRole;

	struct FCorporationEdit *BackupEditableData;

	FReply onClickedEditCtrl(const bool IsSave);	

	void DeleteMembers();
	bool CanDeleteMembers() const;

	void SetMemberRole(const ELeaguePlayerRole::Type);
	bool CanSetMemberRole(const ELeaguePlayerRole::Type) const;

	uint32 bToDonateMoneyState : 1;
	uint32 bToDonateDonateState : 1;

	int32 ToDonateMoneyWidget() const { return bToDonateMoneyState;	}
	int32 ToDonateDonateWidget() const { return bToDonateDonateState; }

	TSharedPtr<SSpinBox<int32>> SpinBoxMoney;
	TSharedPtr<SSpinBox<int32>> SpinBoxDonate;
};
