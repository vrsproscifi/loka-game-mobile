// VRSPRO

#include "LokaGame.h"
#include "SLeagueForm.h"
#include "SlateOptMacros.h"

#include "SLeagueFormInfo.h"

#include "SItemListRow.h"
#include "SSliderSpin.h"
#include "SLokaPopUpError.h"
#include "SResourceTextBox.h"
#include "SWindowBackground.h"

#include "CorporationDetails.h"
#include "CorporationCreate.h"

#include "CorporationService.h"

#include "Internationalization/Regex.h"
#include "IdentityController/Models/PlayerEntityInfo.h"


BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SLeagueForm::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	OnClickedLeague = InArgs._OnClickedLeague;
	OnMakeLeague = InArgs._OnMakeLeague;
	
	IsLeagueTypeOpen = true;

	TSharedPtr<SScrollBar> ScrollBar;

	ChildSlot
	[
		SNew(SWindowBackground)
		.IsEnabledBlur(true)
		[
			SNew(SOverlay)
			+ SOverlay::Slot()
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot()
				.FillWidth(Style->WidthList)
				[
					SNew(SVerticalBox)
					+ SVerticalBox::Slot()
					.AutoHeight()
					[
						SNew(SBorder)
						.Padding(0)
						[
							SNew(SHorizontalBox)
							+ SHorizontalBox::Slot()
							.FillWidth(1.0f)
							+ SHorizontalBox::Slot()
							.FillWidth(0.5f)
							[
								SNew(SSearchBox).OnTextChanged(InArgs._OnSearch)
							]
						]
					]
					+ SVerticalBox::Slot()
					.FillHeight(1.0f)
					[
						SNew(SOverlay)
						+ SOverlay::Slot()
						[
							SAssignNew(ListContainer, SScrollBox)
							.ExternalScrollbar(ScrollBar)
						]
						+ SOverlay::Slot()
						.HAlign(EHorizontalAlignment::HAlign_Right)
						.VAlign(EVerticalAlignment::VAlign_Fill)
						[
							SAssignNew(ScrollBar, SScrollBar)
							.Style(&FLokaSlateStyle::Get().GetWidgetStyle<FScrollBarStyle>("Default_ScrollBar"))
						]
					]
				]
				+ SHorizontalBox::Slot()
				.FillWidth(Style->WidthMake)
				[
					SNew(SBorder)
					.Padding(0)
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot()
						.AutoHeight()
						[
							SNew(SHorizontalBox)
							+ SHorizontalBox::Slot()
							.FillWidth(Style->WidthMakeInputName)
							[
								SAssignNew(Text_Name, SEditableTextBox)
								.Style(&FLokaSlateStyle::Get().GetWidgetStyle<FEditableTextBoxStyle>("Default_EditableTextBox"))
								.ErrorReporting(SNew(SLokaPopUpError))
								.OnTextChanged_Lambda([&](const FText&) { Text_Name->SetError(""); })
								.HintText(NSLOCTEXT("SLeagueForm", "MakeLeague.HintName", "League Name"))
							]
							+ SHorizontalBox::Slot()
							.FillWidth(Style->WidthMakeInputAbbr)
							[
								SAssignNew(Text_Abbr, SEditableTextBox)
								.Style(&FLokaSlateStyle::Get().GetWidgetStyle<FEditableTextBoxStyle>("Default_EditableTextBox"))
								.ErrorReporting(SNew(SLokaPopUpError))
								.OnTextChanged_Lambda([&](const FText&) { Text_Abbr->SetError(""); Text_Abbr->SetText(FText::FromString(Text_Abbr->GetText().ToString().ToUpper())); })
								.HintText(NSLOCTEXT("SLeagueForm", "MakeLeague.HintAbbr", "Abbreviation"))
							]
						]
						+ SVerticalBox::Slot()
						.AutoHeight()
						[
							SNew(SHorizontalBox)
							+ SHorizontalBox::Slot()
							.FillWidth(1.0f)
							+ SHorizontalBox::Slot()
							.AutoWidth()
							[
								SAssignNew(CheckBox_Open, SCheckBox)
								.Style(&Style->LeftToggleButton)
								.Type(ESlateCheckBoxType::ToggleButton)
								.OnCheckStateChanged(this, &SLeagueForm::onCheckBoxLeagueType, true)
								.IsChecked(ECheckBoxState::Checked)
								[
									SNew(STextBlock)
									.TextStyle(&Style->CheckBoxText)
									.Justification(ETextJustify::Center)
									.Text(NSLOCTEXT("SLeagueForm", "MakeLeague.Public", "Public"))
								]
							]
							+ SHorizontalBox::Slot()
							.AutoWidth()
							[
								SAssignNew(CheckBox_Close, SCheckBox)
								.Style(&Style->RightToggleButton)
								.Type(ESlateCheckBoxType::ToggleButton)
								.OnCheckStateChanged(this, &SLeagueForm::onCheckBoxLeagueType, false)
								[
									SNew(STextBlock)
									.TextStyle(&Style->CheckBoxText)
									.Justification(ETextJustify::Center)
									.Text(NSLOCTEXT("SLeagueForm", "MakeLeague.Private", "Private"))
								]
							]
						]				
						+ SVerticalBox::Slot()
						.AutoHeight()
						.Padding(FMargin(4, 0))
						[
							SNew(SHorizontalBox)
							+ SHorizontalBox::Slot()
							.FillWidth(1)
							[
								SNew(STextBlock)
								.TextStyle(&Style->NeedBoxText)
								.Text(NSLOCTEXT("SLeagueForm", "MakeLeague.Cost", "Need for create:"))
							]
							+ SHorizontalBox::Slot()
							.AutoWidth()
							[
								SNew(SResourceTextBox)
								.Type(EResourceTextBoxType::Money)
								.Value(25000)
							]
						]
						+ SVerticalBox::Slot()
						.AutoHeight()
						.Padding(FMargin(4, 4))
						[
							SNew(SButton)
							.HAlign(EHorizontalAlignment::HAlign_Center)
							.Text(NSLOCTEXT("SLeagueForm", "MakeLeague.Make", "Make League"))
							.ButtonStyle(&Style->MakeButton)
							.TextStyle(&Style->MakeText)
							.OnClicked(this, &SLeagueForm::onClickedMakeLeague)
						]
					]
				]
			]
			+ SOverlay::Slot()
			[
				SNew(SWidgetSwitcher)
				.WidgetIndex_Lambda([&]() {
				return FPlayerEntityInfo::Instance.Experience.Level >= 10;
				})
				+ SWidgetSwitcher::Slot()
				[
					SNew(SBorder)
					.BorderImage(new FSlateColorBrush(FColor::Black.WithAlpha(128)))
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					[
						SNew(SHorizontalBox)
						+ SHorizontalBox::Slot().AutoWidth().VAlign(VAlign_Center)
						[
							SNew(STextBlock)
							.TextStyle(&FLokaSlateStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_TextBlock_InDev"))
							.Text(NSLOCTEXT("All", "All.AllowFrom", "Available at ")) //
						]
						+ SHorizontalBox::Slot().AutoWidth().VAlign(VAlign_Center)
						[
							SNew(SResourceTextBox)
							.Type(EResourceTextBoxType::Level)
							.Value(10)
						]
					]
				]
				+ SWidgetSwitcher::Slot()
				[
					SNew(SBorder)
					.BorderImage(new FSlateColorBrush(FColor::Black.WithAlpha(128)))
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					[
						SNew(STextBlock)
						.TextStyle(&FLokaSlateStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_TextBlock_InDev"))
						.Text(NSLOCTEXT("All", "All.InDev", "In Development, coming soon.")) //
					]
				]
			]
		]
	];

	Regex_Abbr = MakeShareable(new FRegexPattern("^([A-Z�-�0-9]{3,5})$"));
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SLeagueForm::onCheckBoxLeagueType(ECheckBoxState State, const bool IsOpen)
{
	if (IsOpen)
	{
		CheckBox_Open->SetIsChecked(ECheckBoxState::Checked);
		CheckBox_Close->SetIsChecked(ECheckBoxState::Unchecked);
	}
	else
	{
		CheckBox_Close->SetIsChecked(ECheckBoxState::Checked);
		CheckBox_Open->SetIsChecked(ECheckBoxState::Unchecked);
	}

	IsLeagueTypeOpen = IsOpen;
}

FReply SLeagueForm::onClickedMakeLeague()
{
	const auto SAbbr = Text_Abbr->GetText().ToString();
	const auto SName = Text_Name->GetText().ToString();

	if (SName.Len() < 5)
	{
		Text_Name->SetError(NSLOCTEXT("SLeagueForm", "Error.Name.Small", "Name is small, minimum symbols 5"));
	}
	else if (SName.Len() > 16)
	{
		Text_Name->SetError(NSLOCTEXT("SLeagueForm", "Error.Name.Large", "Name is large, maximum symbols 16"));
	}
	else if (SAbbr.Len() < 3)
	{
		Text_Abbr->SetError(NSLOCTEXT("SLeagueForm", "Error.Abbr.Small", "Abbreviation is small, minimum symbols 3"));
	}
	else if (SAbbr.Len() > 5)
	{
		Text_Abbr->SetError(NSLOCTEXT("SLeagueForm", "Error.Abbr.Large", "Abbreviation is large, maximum symbols 5"));
	}
	else if (!FRegexMatcher(*Regex_Abbr, SAbbr).FindNext())
	{
		Text_Abbr->SetError(NSLOCTEXT("SLeagueForm", "Error.Abbr.Incorrect", "Abbreviation is incorrect, please use only A-Z, �-�, 0-9 symbols"));
	}
	else 
	{
		OnMakeLeague.ExecuteIfBound(Operation::CreateLeague::Request(SName, SAbbr, 0, !IsLeagueTypeOpen));
	}

	return FReply::Handled();
}

void SLeagueForm::AddItem(const FCorporationPreviewResponse &Details)
{
	ListWidgets.Add(
		SNew(SItemListRow)
		.Name(FText::FromString(FString::Printf(TEXT("[%s]: %s"), *Details.Abbr, *Details.Name)))
		.SubName(FText::Format(NSLOCTEXT("SLeagueForm", "Leagule.SubName", "Enter price: {Price}"), FText::AsNumber(Details.JoinPrice)))
		.AmountText(FText::AsNumber(Details.Members))
		.ButtonName((Details.Access == ECorporationAccess::Public) ? NSLOCTEXT("SLeagueForm", "Leagule.Public", "Join") : NSLOCTEXT("SLeagueForm", "Leagule.Private", "Request join"))
		.OnClicked(this, &SLeagueForm::onClickedItem, Details.Index)
	);

	ListContainer->AddSlot()
	[
		ListWidgets.Last().ToSharedRef()
	];
}

void SLeagueForm::ClearItems()
{
	ListContainer->ClearChildren();
	ListWidgets.Empty();
}

FReply SLeagueForm::onClickedItem(const int32 Index)
{
	OnClickedLeague.ExecuteIfBound(Index);
	return FReply::Handled();
}

// IOnlineSlateError Interface Begin

void SLeagueForm::OnErrorReportingCode(const int32 &ErrorCode)
{
	if (ErrorCode == CCorporationSerivce::CorporationNameWasExist) //CorporationNameWasExist
	{
		Text_Name->SetError(NSLOCTEXT("SLeagueForm", "Error.Name.Exist", "This name already exist!"));
	}
	else if (ErrorCode == CCorporationSerivce::CorporationNameWasInvalid) //CorporationNameWasInvalid
	{
		Text_Name->SetError(NSLOCTEXT("SLeagueForm", "Error.Name.Invalid", "This name invalid!"));
	}
	else if (ErrorCode == CCorporationSerivce::CorporationAbbrWasExist) //CorporationAbbrWasExist
	{
		Text_Abbr->SetError(NSLOCTEXT("SLeagueForm", "Error.Abbr.Exist", "This abbreviation already exist!"));
	}
	else if (ErrorCode == CCorporationSerivce::CorporationAbbrWasInvalid) //CorporationAbbrWasInvalid
	{
		Text_Abbr->SetError(NSLOCTEXT("SLeagueForm", "Error.Abbr.Invalid", "This abbreviation invalid!"));
	}
}

bool SLeagueForm::IsExistCode(const int32 &ErrorCode) const
{
	return (ErrorCode >= CCorporationSerivce::CorporationNameWasExist && ErrorCode <= CCorporationSerivce::CorporationAbbrWasInvalid);
}

// IOnlineSlateError Interface End