#pragma once
#include "ServiceOperation.h"
#include "../Models/WorldBuildingItemView.h"

namespace Operation
{
	namespace GetListOfBuildings
	{
		class Operation final
			:// public COperation<void, ObjectSerializer::NativeObject, TArray<FWorldBuildingItemView>, ObjectSerializer::JsonArrayObject>
		{
		public:
			uint64 Attemp;
			Operation() 
				: Super(FServerHost::MasterClient, "Building/GetListOfBuildings", FVerb::Get)
				, Attemp(1)
			{

			}
		};
	}
}