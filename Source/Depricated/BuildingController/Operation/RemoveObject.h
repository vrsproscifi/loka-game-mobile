#pragma once
#include "ServiceOperation.h"
#include "../Models/WorldBuildingItemView.h"
#include "RequestOneParam.h"

namespace Operation
{
	namespace RemoveObject
	{
		class Operation final
			:// public COperation<FRequestOneParam<FString>, ObjectSerializer::NativeObject, FWorldBuildingItemView, ObjectSerializer::JsonObject>
		{
		public:			
			IResponseHandler<FRequestOneParam<FString>, OperationResponseType::NativeObject> OnBadRequest;

			Operation() 
				: Super(FServerHost::MasterClient, "Building/RemoveObject")
				, OnBadRequest(400, this)
			{

			}
		};
	}
}