#pragma once
#include "ServiceOperation.h"
#include "../Models/WorldBuildingItemView.h"
#include "RequestOneParam.h"

namespace Operation
{
	namespace OnHarvest
	{
		class Operation final
			:// public COperation<FRequestOneParam<FString>, ObjectSerializer::NativeObject, FWorldBuildingStorageView, ObjectSerializer::JsonObject>
		{
		public:
			IResponseHandler<void, OperationResponseType::None> OnNotEnouthSpace;

			Operation() 
				: Super(FServerHost::MasterClient, "Building/OnHarvest")
				, OnNotEnouthSpace(400, this)
			{

			}
		};
	}
}