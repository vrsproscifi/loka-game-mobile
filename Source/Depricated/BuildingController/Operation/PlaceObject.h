#pragma once
#include "ServiceHandler.h"
#include "ServiceOperation.h"
#include "../Models/WorldBuildingItemView.h"
#include "RequestOneParam.h"

namespace Operation
{
	namespace PlaceObject
	{
		class Operation final
			:// public COperation<FWorldBuildingItemView, ObjectSerializer::JsonObject, FWorldBuildingItemView, ObjectSerializer::JsonObject>
		{
		public:
			IResponseHandler<void, OperationResponseType::None> OnNotFoundInstance;
			IResponseHandler<void, OperationResponseType::None> OnNotEnouthItem;
			IResponseHandler<FRequestOneParam<FString>, OperationResponseType::NativeObject> OnBadRequest;

			Operation() 
				: Super(FServerHost::MasterClient, "Building/PlaceObject")
				, OnBadRequest(400, this)
				, OnNotFoundInstance(404, this)
				, OnNotEnouthItem(409, this)
			{

			}
		};
	}
}