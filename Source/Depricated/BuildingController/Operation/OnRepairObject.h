#pragma once
#include "ServiceOperation.h"
#include "../Models/WorldBuildingRepairView.h"
#include "RequestOneParam.h"

namespace Operation
{
	namespace OnRepairObject
	{
		class Operation final
			:// public COperation<FWorldBuildingRepairContainer, ObjectSerializer::JsonObject, TArray<FWorldBuildingRepairView>, ObjectSerializer::JsonArrayObject>
		{
		public:			
			Operation() 
				: Super(FServerHost::MasterClient, "Building/OnRepairObject")
			{

			}
		};
	}
}