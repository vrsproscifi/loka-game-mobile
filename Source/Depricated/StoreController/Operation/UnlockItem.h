#pragma once
#include "ServiceHandler.h"
#include "ServiceOperation.h"
#include "Item/CategoryTypeId.h"
#include "../Models/UnlockItemContainer.h"
#include "../Models/UnlockProfile.h"

namespace Operation
{
	namespace UnlockItem
	{
		class Operation final :// public COperation<FUnlockItemRequestContainer, ObjectSerializer::JsonObject, FUnlockItemResponseContainer, ObjectSerializer::JsonObject, EOperationLogVerbosity::All>
		{
		public:
			Operation() 
				: Super(FServerHost::MasterClient, "Store/BuyItem") 
				, OnPaymentRequired(402, this)
			{ 

			}
			IResponseHandler<void, OperationResponseType::None> OnPaymentRequired;
		};
	}


	namespace UnlockProfile
	{
		class Operation final :// public COperation<FUnlockItemRequestContainer, ObjectSerializer::JsonObject, FUnlockProfileResponse, ObjectSerializer::JsonObject, EOperationLogVerbosity::All>
		{
		public:
			Operation()
				: Super(FServerHost::MasterClient, "Store/BuyProfile")
				, OnPaymentRequired(402, this)
			{

			}
			IResponseHandler<void, OperationResponseType::None> OnPaymentRequired;
		};
	}
	DECLARE_DELEGATE_OneParam(FOnUnlockItem, const FUnlockItemRequestContainer&);
}