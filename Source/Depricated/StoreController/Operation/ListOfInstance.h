#pragma once
#include "ServiceOperation.h"
#include "../Models/ListOfInstance.h"
#include "PoolingOperation.h"

namespace Operation
{
	namespace ListOfInstance
	{
		class Operation final 
			// : public COperation<void, ObjectSerializer::NativeObject, FListOfInstance, ObjectSerializer::JsonObject, EOperationLogVerbosity::Error_Warning, false>
			// , public FPoolingOperation
		{
		public:
			int32 AttempRequestNumber;

			Operation() : Super(
#if UE_SERVER
				FServerHost::MasterServer
#else
				FServerHost::MasterClient
#endif
				, "Store/ListOfInstance", FVerb::Get) 
				, FPoolingOperation(4.5f, 1.5f)
				, AttempRequestNumber(0)
			{
				TimerDelegate.BindRaw(this, &Operation::Request);
			}

		};
	}
}