// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Types/TypeCash.h"
#include "GameFramework/GameState.h"
#include "GameState_Build.generated.h"


class UItemBuildEntity;

UCLASS()
class LOKAGAME_API AGameState_Build : public AGameState
{
	GENERATED_BODY()

public:

	AGameState_Build();

	UFUNCTION()
	void OnRep_OwnerInventory();

	UPROPERTY(ReplicatedUsing = OnRep_OwnerInventory)
	TArray<UItemBuildEntity*> OwnerInventory;

	UPROPERTY(Replicated)
	FTypeCash Cash;

	UPROPERTY(Replicated)
	APlayerState* OwnerState;

	virtual bool ReplicateSubobjects(class UActorChannel *Channel, class FOutBunch *Bunch, FReplicationFlags *RepFlags) override;
};
