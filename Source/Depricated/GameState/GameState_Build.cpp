// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "GameState_Build.h"

#include "EntityRepository.h"
#include "Build/ItemBuildEntity.h"
#include "ShooterGameInstance.h"
#include "BuildPlayerController.h"
#include "BuildSystem/BuilderCharacter.h"
#include "Engine/ActorChannel.h"


AGameState_Build::AGameState_Build()
	: Super()
{

}

void AGameState_Build::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME(AGameState_Build, OwnerState);
	DOREPLIFETIME(AGameState_Build, OwnerInventory);
	DOREPLIFETIME(AGameState_Build, Cash);
}

bool AGameState_Build::ReplicateSubobjects(UActorChannel *Channel, class FOutBunch *Bunch, FReplicationFlags *RepFlags)
{
	bool WroteSomething = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (auto &i : OwnerInventory)
	{
		if (i && i->IsValidLowLevel())
		{
			WroteSomething |= Channel->ReplicateSubobject(i, *Bunch, *RepFlags);
		}
	}

	return WroteSomething;
}

void AGameState_Build::OnRep_OwnerInventory()
{
	if (GetWorld() && OwnerInventory.Num())
	{
		SIZE_T _count = 0, _maxnull = 10;
		bool IsOwnerInventoryNull = false;
		for (auto Item : OwnerInventory)
		{
			if (Item == nullptr)
			{
				if (_count >= _maxnull)
				{
					IsOwnerInventoryNull = true;
					break;
				}

				++_count;
			}
		}

		auto MyCtrl = Cast<ABuildPlayerController>(GetWorld()->GetFirstPlayerController());
		if (MyCtrl && !IsOwnerInventoryNull)
		{
			MyCtrl->InitializUi();
		}
	}
}