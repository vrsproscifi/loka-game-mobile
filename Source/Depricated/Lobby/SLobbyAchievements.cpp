// VRSPRO

#include "LokaGame.h"
#include "SLobbyAchievements.h"
#include "SlateOptMacros.h"

#include "SAnimatedBackground.h"
#include "SWindowBackground.h"
#include "SLokaToolTip.h"
#include "SResourceTextBox.h"

#include "Layout/SScaleBox.h"

class LOKAGAME_API SLobbyAchievements_Item : public STableRow<UItemAchievementEntity*>
{
	typedef STableRow<UItemAchievementEntity*> SSuper;
public:
	SLATE_BEGIN_ARGS(SLobbyAchievements_Item)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FLobbyContainerStyle>("SLobbyContainerStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FLobbyContainerStyle, Style)
	SLATE_END_ARGS()

	BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
	void Construct(const FArguments& InArgs, const TSharedRef<STableViewBase>& OwnerTable, UItemAchievementEntity* InInstance)
	{
		Instance = InInstance;
		Style = InArgs._Style;

		SSuper::Construct(SSuper::FArguments().Padding(FMargin(24))
			[
				SNew(SOverlay).ToolTip(SNew(SLokaToolTip)
				[
					SNew(SBox).WidthOverride(400)
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot().AutoHeight().VAlign(VAlign_Center)
						[
							SNew(SHorizontalBox)
							+ SHorizontalBox::Slot().FillWidth(1).VAlign(VAlign_Center)
							[
								SNew(STextBlock) // Name
								.TextStyle(&Style->Achievements.Name)
								.Text(Instance->GetDescription().Name)
							]
							+ SHorizontalBox::Slot().AutoWidth().VAlign(VAlign_Center)
							[
								SNew(SResourceTextBox) // Reward
								.CustomSize(FVector(16, 16, 10))
								.Type(Instance->GetAchievementProperty().Reward.IsDonate ? EResourceTextBoxType::Donate : EResourceTextBoxType::Money)
								.Value(Instance->GetAchievementProperty().Reward.Amount)
							]
						]
						+ SVerticalBox::Slot().Padding(FMargin(0, 14, 0, 6))
						[
							SNew(SVerticalBox) // Progress
							+ SVerticalBox::Slot().HAlign(HAlign_Right)
							[
								SNew(STextBlock)
								.TextStyle(&Style->Achievements.Progress)
								.Text(this, &SLobbyAchievements_Item::GetProgress_Text)
							]
							+ SVerticalBox::Slot()
							[
								SNew(SProgressBar)
								.Percent(this, &SLobbyAchievements_Item::GetProgress, false)
								.Style(&Style->Achievements.ProgressBar)
							]
						]
						+ SVerticalBox::Slot().Padding(FMargin(4))
						[
							SNew(STextBlock) // Description
							.AutoWrapText(true)
							.TextStyle(&Style->Achievements.Description)
							.Text(Instance->GetDescription().Description)
						]
					]
				])
				+ SOverlay::Slot()
				[
					SNew(SScaleBox)
					.Stretch(EStretch::ScaleToFit)
					[
						SNew(SImage).Image(Instance->GetIcon())
					]
				]
				+ SOverlay::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
				[
					SNew(STextBlock)
					.TextStyle(&Style->Achievements.Level)
					.Text(this, &SLobbyAchievements_Item::GetProgressLevel_Text)
				]
				//+ SOverlay::Slot().HAlign(HAlign_Right).VAlign(VAlign_Bottom)
				//[
				//	SNew(SResourceTextBox) // Reward
				//	.CustomSize(FVector(16, 16, 10))
				//	.Type(Instance->GetAchievementProperty().Reward.IsDonate ? EResourceTextBoxType::Donate : EResourceTextBoxType::Money)
				//	.Value(Instance->GetAchievementProperty().Reward.Amount)
				//]
			], OwnerTable);

		RegisterActiveTimer(1.0f, FWidgetActiveTimerDelegate::CreateSP(this, &SLobbyAchievements_Item::OnUpdateLevel));

		ASSIGN_UTF8_SYMBOL(sEmptyStar, 0xf006);
		ASSIGN_UTF8_SYMBOL(sFullStar, 0xf005);
	}
	END_SLATE_FUNCTION_BUILD_OPTIMIZATION

protected:

	const FLobbyContainerStyle* Style;
	FString sEmptyStar, sFullStar;
	FText CachedLevelText;

	TOptional<float> GetProgress(const bool IsLevel) const
	{
		if (Instance)
		{
			if (IsLevel)
			{
				return float(Instance->GetAchievementProperty().Level) / float(Instance->GetAchievementProperty().MaxLevel);
			}
			else
			{
				return float(Instance->GetAchievementProperty().CurrentProgress) / float(Instance->GetAchievementProperty().NeedProgress);
			}
		}

		return .0f;
	}

	EActiveTimerReturnType OnUpdateLevel(double InCurrentTime, float DeltaTime)
	{
		if (Instance)
		{
			FString Stars;

			for (SSIZE_T i = 0; i < Instance->GetAchievementProperty().MaxLevel; ++i)
			{
				if (i < Instance->GetAchievementProperty().Level)
				{
					Stars.Append(sFullStar);
				}
				else
				{
					Stars.Append(sEmptyStar);
				}
			}

			CachedLevelText = FText::FromString(Stars);
		}

		return EActiveTimerReturnType::Continue;
	}

	FText GetProgressLevel_Text() const
	{
		return CachedLevelText;
	}

	FText GetProgress_Text() const
	{
		if (Instance)
		{
			return FText::Format(FText::FromString("{0}/{1}"), FText::AsNumber(Instance->GetAchievementProperty().CurrentProgress), FText::AsNumber(Instance->GetAchievementProperty().NeedProgress));
		}

		return FText::FromString("0/0");
	}

	UItemAchievementEntity* Instance;
};


BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SLobbyAchievements::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	auto ScrollBar = SNew(SScrollBar).Style(&FLokaSlateStyle::Get().GetWidgetStyle<FScrollBarStyle>("Default_ScrollBar")).Thickness(FVector2D(4, 4));

	SAssignNew(List, STileView<UItemAchievementEntity*>)
		.ListItemsSource(&Achievements)
		.ItemHeight(256)
		.ItemWidth(256)
		.SelectionMode(ESelectionMode::None)
		.ExternalScrollbar(ScrollBar)
		.OnGenerateTile_Lambda([&](UItemAchievementEntity* Item, const TSharedRef<STableViewBase>& OwnerTable) {
		return SNew(SLobbyAchievements_Item, OwnerTable, Item);
	});

	ChildSlot.HAlign(HAlign_Center)
	[
		SNew(SWindowBackground)
		.Header(NSLOCTEXT("SLobbyAchievements", "SLobbyAchievements.Header", "Achievements"))
		.Padding(FMargin(20, 10))
		.IsEnabledBlur(true)
		[
			SNew(SBox).MinDesiredWidth(256*4)
			[
				SNew(SOverlay)
				+ SOverlay::Slot()
				[
					List.ToSharedRef()
				]
				+ SOverlay::Slot().HAlign(HAlign_Right)
				[
					ScrollBar
				]
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SLobbyAchievements::SetList(TArray<UItemAchievementEntity*>& InAchievements)
{
	Achievements = InAchievements.FilterByPredicate([](UItemAchievementEntity* A) { return A->IsVisible; });
	List->RequestListRefresh();
}