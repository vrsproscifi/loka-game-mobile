// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/LobbyContainerWidgetStyle.h"
#include "Entity/Achievement/ItemAchievementEntity.h"

class LOKAGAME_API SLobbyAchievements : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SLobbyAchievements)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FLobbyContainerStyle>("SLobbyContainerStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FLobbyContainerStyle, Style)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);
	void SetList(TArray<UItemAchievementEntity*>& InAchievements);

protected:

	const FLobbyContainerStyle* Style;

	TSharedPtr<STileView<UItemAchievementEntity*>> List;
	TArray<UItemAchievementEntity*> Achievements;
};
