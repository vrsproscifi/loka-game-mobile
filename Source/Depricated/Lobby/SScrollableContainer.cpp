// VRSPRO

#include "LokaGame.h"
#include "SScrollableContainer.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SScrollableContainer::Construct(const FArguments& InArgs)
{
	Widget_Array = InArgs._WidgetsSource;

	ChildSlot
	[
		SNew(SOverlay)
		+ SOverlay::Slot().HAlign(HAlign_Fill).VAlign(VAlign_Fill)
		[
			SAssignNew(Widget_Current, SAnimatedBackground) // Current
			.ShowAnimation(EAnimBackAnimation::ZoomIn)
			.HideAnimation(EAnimBackAnimation::ZoomOut)
			.Duration(.8f)
		]
		+ SOverlay::Slot().HAlign(HAlign_Fill).VAlign(VAlign_Fill)
		[
			SAssignNew(Widget_Next, SAnimatedBackground) // Next
			.ShowAnimation(EAnimBackAnimation::ZoomIn)
			.HideAnimation(EAnimBackAnimation::ZoomOut)
			.Duration(.8f)
		]
	];

	IsNextAsCurrent = false;
	ActiveIndex = 0;

	if (Widget_Array->Num())
	{
		SetActiveIndex(0);
	}
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

bool SScrollableContainer::SetActiveIndex(const int32& Index)
{
	const TArray<TSharedRef<SWidget>>& Widget_ArrayRef = (*Widget_Array);

	if (Widget_ArrayRef.IsValidIndex(ActiveIndex) && Widget_ArrayRef.IsValidIndex(Index) && !Widget_Current->IsInAnimated())
	{
		(IsNextAsCurrent ? Widget_Next : Widget_Current)->SetContent(Widget_ArrayRef[Index]);

		Widget_Current->ToggleWidget(IsNextAsCurrent ? false : true);
		Widget_Next->ToggleWidget(IsNextAsCurrent ? true : false);

		IsNextAsCurrent = !IsNextAsCurrent;
		ActiveIndex = Index;

		return true;
	}

	return false;
}