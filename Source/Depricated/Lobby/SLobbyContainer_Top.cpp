// VRSPRO

#include "LokaGame.h"
#include "SLobbyContainer_Top.h"
#include "SlateOptMacros.h"

#include "Components/SResourceTextBox.h"
#include "Components/SLokaToolTip.h"

#include "IdentityController/Models/PlayerEntityInfo.h"
#include "LobbyController/Operation/SearchSessionParams.h"
#include "FriendController/Models/PlayerFriendStatus.h"
#include "GameModeTypeId.h"

#include "EntityRepository.h"
#include "Fraction/FractionEntity.h"

#include "SWindowBackground.h"
#include "SAnimatedBackground.h"
#include "Layout/SScaleBox.h"
#include "Models/SearchSessionStatus.h"

#include "LokaTextDecorator.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SLobbyContainer_Top::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	

	OnSearchSessionBegin = InArgs._OnSearchSessionBegin;
	OnSearchSessionCancel = InArgs._OnSearchSessionCancel;
	OnClickedBooster = InArgs._OnClickedBooster;

	auto Default_CheckBoxStyle = &FLokaSlateStyle::Get().GetWidgetStyle<FCheckBoxStyle>("Default_CheckBox");

	MAKE_UTF8_SYMBOL(sAchievements, 0xf091);
	MAKE_UTF8_SYMBOL(sIntoFightAlt, 0xf078);
	MAKE_UTF8_SYMBOL(sBuilding, 0xf1b3);

	auto Widget_FractionToolTip = SNew(SVerticalBox)
		+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(10, 4))
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot().AutoWidth().Padding(FMargin(0, 0, 40, 0))
			[
				SNew(STextBlock) // Fraction:
				.TextStyle(&Style->ToolTipText)
				.Text(NSLOCTEXT("SLobbyContainer_Top", "SLobbyContainer_Top.Fraction", "Fraction:"))
			]
			+ SHorizontalBox::Slot().AutoWidth().HAlign(HAlign_Right)
			[	
				SNew(STextBlock) // Fraction name
				.TextStyle(&Style->ToolTipText)
				.Text(this, &SLobbyContainer_Top::GetFractionName)
			]
		]
		+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(10, 4))
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot().AutoWidth().Padding(FMargin(0, 0, 40, 0))
			[
				SNew(STextBlock) // Rank:
				.TextStyle(&Style->ToolTipText)
				.Text(NSLOCTEXT("SLobbyContainer_Top", "SLobbyContainer_Top.Rank", "Rank:"))
			]
			+ SHorizontalBox::Slot().AutoWidth().HAlign(HAlign_Right)
			[
				SNew(STextBlock) // Rank name
				.TextStyle(&Style->ToolTipText)
				.Text(this, &SLobbyContainer_Top::GetFractionRankName)
			]
		];

	ChildSlot
	[
		SNew(SOverlay)
		+ SOverlay::Slot().VAlign(VAlign_Top).HAlign(HAlign_Left).Padding(FMargin(0, Style->TopBottomOffset + 8))
		[
			SNew(SBox).WidthOverride(600).HeightOverride(50)
			[
				SNew(SBorder)
				.BorderImage(&Style->TopLines)
				.RenderTransformPivot(FVector2D(.5f, .5f))
			]
		]
		+ SOverlay::Slot().VAlign(VAlign_Top).HAlign(HAlign_Right).Padding(FMargin(0, Style->TopBottomOffset + 8))
		[
			SNew(SBox).WidthOverride(600).HeightOverride(50)
			[
				SNew(SBorder)
				.BorderImage(&Style->TopLines)
				.RenderTransformPivot(FVector2D(.5f, .5f))
				.RenderTransform(FSlateRenderTransform(FScale2D(-1, 1)))
			]
		]
		+ SOverlay::Slot().VAlign(VAlign_Top).Padding(FMargin(0, Style->TopBottomOffset))
		[
			SNew(SBorder)
			.Padding(0)
			.BorderImage(&Style->BottomBackground)
			[
				SNew(SBorder)
				.BorderImage(&Style->BottomBorder)
				[
					SNew(SBox).HeightOverride(64).VAlign(VAlign_Center).Padding(FMargin(20, 0))
					[
						SNew(SOverlay)
						+ SOverlay::Slot().HAlign(HAlign_Fill).VAlign(VAlign_Fill)
						[
							SNew(SHorizontalBox)
							+ SHorizontalBox::Slot().AutoWidth().VAlign(VAlign_Center)
							[
								SNew(SResourceTextBox)
								.Type(EResourceTextBoxType::Level)
								.Value(this, &SLobbyContainer_Top::GetPlayerLevel)
								.CustomSize(FVector(20, 20, 14))
							]
							+ SHorizontalBox::Slot().AutoWidth().Padding(FMargin(10, 0)).VAlign(VAlign_Center)
							[
								SNew(SBox).WidthOverride(200)
								[
									SNew(SVerticalBox)
									+ SVerticalBox::Slot().AutoHeight()
									[
										SNew(STextBlock)
										.Text(this, &SLobbyContainer_Top::GetPlayerName)
										.TextStyle(&Style->ProgressBarText)
									]
									+ SVerticalBox::Slot().AutoHeight()
									[
										SNew(SBox).HeightOverride(16)
										[
											SNew(SProgressBar)
											.BarFillType(EProgressBarFillType::LeftToRight)
											.Percent(this, &SLobbyContainer_Top::GetPlayerProgress_Exp)
											.Style(&Style->ProgressBar)
											.ToolTip(SNew(SLokaToolTip).Text(this, &SLobbyContainer_Top::GetPlayerProgressText_Exp))
										]
									]
								]
							]
							+ SHorizontalBox::Slot().AutoWidth().Padding(FMargin(10, 0)).VAlign(VAlign_Center)
							[
								SNew(SResourceTextBox)
								.Type(EResourceTextBoxType::Donate)
								.Value(this, &SLobbyContainer_Top::GetPlayerDonate)
								.CustomSize(FVector(20, 20, 14))
							]
							+ SHorizontalBox::Slot().AutoWidth().Padding(FMargin(10, 0)).VAlign(VAlign_Center)
							[
								SNew(SResourceTextBox)
								.Type(EResourceTextBoxType::Money)
								.Value(this, &SLobbyContainer_Top::GetPlayerMoney)
								.CustomSize(FVector(20, 20, 14))
							]
							+ SHorizontalBox::Slot().FillWidth(1)
							+ SHorizontalBox::Slot().AutoWidth().Padding(FMargin(10, 0)).VAlign(VAlign_Center) // Premium
							[
								SNew(SResourceTextBox).Type(EResourceTextBoxType::Premium)
								.BoosterTime(this, &SLobbyContainer_Top::GetBoosterTime, EResourceTextBoxType::Premium)
								.OnClicked_Lambda([&]() { OnClickedBooster.ExecuteIfBound(EResourceTextBoxType::Premium); return FReply::Handled(); })
							]
							+ SHorizontalBox::Slot().AutoWidth().Padding(FMargin(10, 0)).VAlign(VAlign_Center) // Boost money
							[
								SNew(SResourceTextBox).Type(EResourceTextBoxType::BoostMoney)
								.BoosterTime(this, &SLobbyContainer_Top::GetBoosterTime, EResourceTextBoxType::BoostMoney)
								.OnClicked_Lambda([&]() { OnClickedBooster.ExecuteIfBound(EResourceTextBoxType::BoostMoney); return FReply::Handled(); })
							]
							+ SHorizontalBox::Slot().AutoWidth().Padding(FMargin(10, 0)).VAlign(VAlign_Center) // Boost exp
							[
								SNew(SResourceTextBox).Type(EResourceTextBoxType::BoostExperience)
								.BoosterTime(this, &SLobbyContainer_Top::GetBoosterTime, EResourceTextBoxType::BoostExperience)
								.OnClicked_Lambda([&]() { OnClickedBooster.ExecuteIfBound(EResourceTextBoxType::BoostExperience); return FReply::Handled(); })
							]
							+ SHorizontalBox::Slot().AutoWidth().Padding(FMargin(10, 0, 40, 0)).VAlign(VAlign_Center) // Boost rep
							[
								SNew(SResourceTextBox).Type(EResourceTextBoxType::BoostReputation)
								.BoosterTime(this, &SLobbyContainer_Top::GetBoosterTime, EResourceTextBoxType::BoostReputation)
								.OnClicked_Lambda([&]() { OnClickedBooster.ExecuteIfBound(EResourceTextBoxType::BoostReputation); return FReply::Handled(); })
							]
							+ SHorizontalBox::Slot().AutoWidth().Padding(FMargin(10, 0)).VAlign(VAlign_Center)
							[
								SNew(SBox).WidthOverride(200)
								.ToolTip(SNew(SLokaToolTip)[Widget_FractionToolTip])
								[
									SNew(SVerticalBox)
									+ SVerticalBox::Slot().AutoHeight()
									[
										SNew(STextBlock)
										.Text(this, &SLobbyContainer_Top::GetFractionRankName)
										.Justification(ETextJustify::Center)
										.TextStyle(&Style->ProgressBarText)
									]
									+ SVerticalBox::Slot().AutoHeight()
									[
										SNew(SBox).HeightOverride(16)
										[
											SNew(SProgressBar)
											.BarFillType(EProgressBarFillType::LeftToRight)
											.Percent(this, &SLobbyContainer_Top::GetFractionProgress_Exp)
											.Style(&Style->ProgressBar)
											.ToolTip(SNew(SLokaToolTip).Text(this, &SLobbyContainer_Top::GetFractionProgressText_Exp))
										]
									]
								]
							]
							+ SHorizontalBox::Slot().AutoWidth().Padding(FMargin(10, 0)).VAlign(VAlign_Center)
							[
								SNew(SScaleBox)
								.Stretch(EStretch::ScaleToFit)
								.ToolTip(SNew(SLokaToolTip)[Widget_FractionToolTip])
								[
									SNew(SImage).Image(this, &SLobbyContainer_Top::GetFractionIcon)
								]
							]
						]
					]
				]
			]
		]
		+ SOverlay::Slot().VAlign(VAlign_Top).HAlign(HAlign_Center).Padding(FMargin(0, Style->TopBottomOffset + 8))
		[
			SNew(SBox).MaxDesiredHeight(70).MinDesiredWidth(420).MinDesiredHeight(50)
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot()
				[
					SNew(SCheckBox)
					.Style(&Style->IntoBattleButton)
					.Type(ESlateCheckBoxType::ToggleButton)
					.IsChecked_Lambda([&]() { return GetIsEnableModeChange() ? ECheckBoxState::Unchecked : ECheckBoxState::Checked; })
					.OnCheckStateChanged(this, &SLobbyContainer_Top::OnClickedIntoFight, false, 0)
					.Padding(0)
					[
						SNew(SBox)
						.HAlign(HAlign_Center)
						.VAlign(VAlign_Center)
						.Padding(FMargin(10, 8))
						[
							SNew(STextBlock)
							.TextStyle(&Style->IntoBattleText)
							.Text(this, &SLobbyContainer_Top::GetIntoFightButtonText)
						]
					]
				]
				+ SHorizontalBox::Slot().FillWidth(.2f)
				[
					SNew(SCheckBox)
					.Style(&Style->IntoBattleButton)
					.Type(ESlateCheckBoxType::ToggleButton)
					.IsChecked_Lambda([&]() { return GetIsEnableModeChange() ? ECheckBoxState::Unchecked : ECheckBoxState::Checked; })
					.OnCheckStateChanged(this, &SLobbyContainer_Top::OnClickedIntoFight, true, 0)
					.Padding(0)
					[
						SNew(SBox)
						.HAlign(HAlign_Center)
						.VAlign(VAlign_Center)
						[
							SNew(SRichTextBlock)
							.Text(FText::FromString(FString("<text font=\"Awesome\" size=\"18\">" + FString(sIntoFightAlt) + "</>")))
							.AutoWrapText(true)
							.DecoratorStyleSet(&FLokaSlateStyle::Get())
							+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(TMap<FName, FSlateFontInfo>(), Style->IntoBattleText.Font, Style->IntoBattleText.ColorAndOpacity.GetSpecifiedColor()))
						]
					]
				]
				+ SHorizontalBox::Slot().AutoWidth()
				[
					SNew(SBox).MaxDesiredHeight(70).MinDesiredWidth(100).MinDesiredHeight(50)
					[
						SNew(SButton)
						.IsEnabled(this, &SLobbyContainer_Top::GetIsEnableModeChange)
						.ToolTip(SNew(SLokaToolTip).Text(NSLOCTEXT("SLobbyContainer", "Lobby.Build", "Go to build mode")))
						.ButtonStyle(&Style->IntoBuildingButton)
						.HAlign(HAlign_Center)
						.VAlign(VAlign_Center)
						[
							SNew(SRichTextBlock)
							.Text(FText::FromString(FString("<text font=\"Awesome\" size=\"18\">" + FString(sBuilding) + "</>")))
							.AutoWrapText(true)
							.DecoratorStyleSet(&FLokaSlateStyle::Get())
							+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(Style->IntoBattleText))
						]
						.OnClicked_Lambda([&, bm = InArgs._OnClickedBuildMode]() {
							bm.ExecuteIfBound();
							return FReply::Handled();
						})
					]
				]
			]
		]
		+ SOverlay::Slot().VAlign(VAlign_Fill).HAlign(HAlign_Fill)
		[
			SAssignNew(Widget_BigModeAnimated[0], SAnimatedBackground)
			.ShowAnimation(EAnimBackAnimation::Color)
			.HideAnimation(EAnimBackAnimation::Color)
			.Duration(1.0f)
			.InitAsHide(true)
			[
				SNew(SBorder).Padding(FMargin(60, 100)).HAlign(HAlign_Center).VAlign(VAlign_Center)
				.BorderImage(new FSlateColorBrush(FColor::Black.WithAlpha(210)))
				.OnMouseButtonDown_Lambda([&](const FGeometry&, const FPointerEvent&) {
					Widget_BigModeAnimated[0]->ToggleWidget(false);
					Widget_BigModeAnimated[1]->ToggleWidget(false);
					return FReply::Handled();
				})
				[
					SAssignNew(Widget_BigModeAnimated[1], SAnimatedBackground)
					.ShowAnimation(EAnimBackAnimation::UpFade)
					.HideAnimation(EAnimBackAnimation::DownFade)
					.Duration(1.0f)
					.InitAsHide(true)
					[
						SNew(SWindowBackground)
						[
							SNew(SGridPanel)
							+ SGridPanel::Slot(0, 0)
							[
								BuildBigModeButton(&Style->Helper.ImageLastHero, EGameModeTypeId::LostDeadMatch)
							]
							+ SGridPanel::Slot(1, 0)
							[
								BuildBigModeButton(&Style->Helper.ImageTeamFight, EGameModeTypeId::TeamDeadMatch)
							]
							+ SGridPanel::Slot(2, 0)
							[
								BuildBigModeButton(&Style->Helper.ImageResearch, EGameModeTypeId::ResearchMatch)
							]
							+ SGridPanel::Slot(0, 1)
							[
								BuildBigModeButton(&Style->Helper.ImageDuel, EGameModeTypeId::DuelMatch)
							]
							+ SGridPanel::Slot(1, 1)
							[
								BuildBigModeButton(&Style->Helper.ImageResearch2, EGameModeTypeId::Attack)
							]
							+ SGridPanel::Slot(2, 1)
							[
								BuildBigModeButton(&Style->Helper.ImageNone, EGameModeTypeId::None, false)
							]
						]
					]
				]
			]
		]
		+ SOverlay::Slot().VAlign(VAlign_Top).HAlign(HAlign_Center)
		[
			SNew(SWindowBackground)
			.Header(NSLOCTEXT("SLobbyContainer", "Lobby.SearchInProgress", "Search"))
			.IsEnabledHeaderBackground(false)
			.Opacity(.9f)
			.RenderTransformPivot(FVector2D(.5f, .0f))
			.RenderTransform(FSlateRenderTransform(FVector2D(.0f, 70.0f)))
			.Padding(FMargin(10, 2))
			.Visibility_Lambda([&]() { return (SearchSessionStatus & ESearchSessionState::Search) ? EVisibility::Visible : EVisibility::Collapsed; })
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot().AutoHeight()
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot()
					[
						SNew(STextBlock) // Time desc
						.TextStyle(&Style->IntoBattleCheckText)
						.Text(FText::FromString("Time:\t\t\t"))
					]
					+ SHorizontalBox::Slot().AutoWidth()
					[
						SNew(STextBlock) // Time value
						.TextStyle(&Style->IntoBattleCheckText)
						.Text_Lambda([&]() {
							FTimespan TimeSpan = FTimespan::FromSeconds(FDateTime::UtcNow().ToUnixTimestamp() - SearchTime);

							FNumberFormattingOptions NumberFormatting;
							NumberFormatting.SetMaximumIntegralDigits(2);
							NumberFormatting.SetMinimumIntegralDigits(2);

							return FText::Format(FText::FromString("{0}:{1}"), FText::AsNumber(TimeSpan.GetMinutes(), &NumberFormatting), FText::AsNumber(TimeSpan.GetSeconds(), &NumberFormatting));
						})
					]
				]
				+ SVerticalBox::Slot().AutoHeight()
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot()
					[
						SNew(STextBlock) // Level desc
						.TextStyle(&Style->IntoBattleCheckText)
						.Text(FText::FromString("Level:\t\t\t"))
					]
					+ SHorizontalBox::Slot().AutoWidth()
					[
						SNew(STextBlock) // Level value
						.TextStyle(&Style->IntoBattleCheckText)
						.Text(FText::FromString("1-2"))
					]
				]
				+ SVerticalBox::Slot().AutoHeight()
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot()
					[
						SNew(STextBlock) // Mode desc
						.TextStyle(&Style->IntoBattleCheckText)
						.Text(FText::FromString("Mode:\t\t\t"))
					]
					+ SHorizontalBox::Slot().AutoWidth()
					[
						SNew(STextBlock) // Mode value
						.TextStyle(&Style->IntoBattleCheckText)
						.Text_Lambda([&]() {
							return FGameModeTypeId::GetText(CurrentGameModeTypeId);
						})
					]
				]
				+ SVerticalBox::Slot().AutoHeight()
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot()
					[
						SNew(STextBlock) // Mode desc
						.TextStyle(&Style->IntoBattleCheckText)
						.Text(FText::FromString("Avg. Time:\t\t\t"))
					]
					+ SHorizontalBox::Slot().AutoWidth()
					[
						SNew(STextBlock) // Mode value
						.TextStyle(&Style->IntoBattleCheckText)
						.Text(NSLOCTEXT("SLobbyContainer_Top", "SLobbyContainer_Top.OptionalDesc2.TeamDeadMatch", "1-5 minutes"))
					]
				]
			]
		]
		+ SOverlay::Slot().HAlign(HAlign_Fill).VAlign(VAlign_Fill)
		[
			SAssignNew(Widget_PveWindow, SLobbyContainer_PVE)
			.OnClickedPVP_Lambda([&]() { 
				FFlagsHelper::SetFlags(SelectedGameModesFlags, 
									   /*EGameModeTypeId::Attack |*/
									   EGameModeTypeId::DuelMatch | 
									   EGameModeTypeId::LostDeadMatch |
									   EGameModeTypeId::ResearchMatch |
									   EGameModeTypeId::TeamDeadMatch);

				OnClickedIntoFight(ECheckBoxState::Checked);
				Widget_PveWindow->ToggleWidget(false);
				return FReply::Handled();
			})
			.OnClickedPVPA_Lambda([&](){
				OnClickedIntoFight(ECheckBoxState::Checked, true);
				Widget_PveWindow->ToggleWidget(false);
				return FReply::Handled();
			})
			.OnClickedPVE_Lambda([&](int32 InLevel) {
				FFlagsHelper::SetFlags(SelectedGameModesFlags, EGameModeTypeId::PVE_Waves);
				OnClickedIntoFight(ECheckBoxState::Checked, false, InLevel);
				Widget_PveWindow->ToggleWidget(false);
			})
		]
	];

	SearchTime = FDateTime::UtcNow().ToUnixTimestamp();
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

TSharedRef<SWidget> SLobbyContainer_Top::BuildBigModeButton(const FSlateBrush* Image, const EGameModeTypeId::Type& GameMode, const bool IsEnabled)
{
	FText OptionalDesc;

	switch (GameMode)
	{
	case EGameModeTypeId::TeamDeadMatch: OptionalDesc = NSLOCTEXT("SLobbyContainer_Top", "SLobbyContainer_Top.OptionalDesc.TeamDeadMatch", "\r\n\r\nMaximum time: 15 minutes\r\nMaximum players: 64\r\nSearch time: 1-5 minutes"); break;
	case EGameModeTypeId::LostDeadMatch: OptionalDesc = NSLOCTEXT("SLobbyContainer_Top", "SLobbyContainer_Top.OptionalDesc.LostDeadMatch", "\r\n\r\nMaximum time: 15 minutes\r\nMaximum players: 32\r\nSearch time: 1-5 minutes"); break;
	case EGameModeTypeId::ResearchMatch: OptionalDesc = NSLOCTEXT("SLobbyContainer_Top", "SLobbyContainer_Top.OptionalDesc.ResearchMatch", "\r\n\r\nMaximum time: 15 minutes\r\nMaximum players: 64\r\nSearch time: 1-5 minutes"); break;
	case EGameModeTypeId::Building: OptionalDesc = NSLOCTEXT("SLobbyContainer_Top", "SLobbyContainer_Top.OptionalDesc.ResearchMatch2", "\r\n\r\nMaximum time: 60 minutes\r\nMaximum players: 120\r\nSearch time: 2-6 minutes"); break;
	default:
		break;
	}


	return
		SNew(SButton)
		.IsEnabled(IsEnabled)
		.OnClicked_Lambda([&, g = GameMode]() {
			FFlagsHelper::SetFlags(SelectedGameModesFlags, g);

			Widget_BigModeAnimated[0]->ToggleWidget(false);
			Widget_BigModeAnimated[1]->ToggleWidget(false);

			OnClickedIntoFight(ECheckBoxState::Checked);

			return FReply::Handled();
		})
		.ButtonStyle(&Style->Helper.ImageButton)
		.ContentPadding(10)
		[
			SNew(SBox).WidthOverride(390).HeightOverride(390)
			[
				SNew(SOverlay)
				+ SOverlay::Slot()
				[
					SNew(SScaleBox)
					.Stretch(EStretch::ScaleToFit)
					[
						SNew(SImage).Image(Image)
					]
				]
				+ SOverlay::Slot().VAlign(VAlign_Center)
				[
					SNew(SBorder)
					.BorderImage(new FSlateColorBrush(FColor::Black.WithAlpha(210))).HAlign(HAlign_Center)
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot().AutoHeight()
						[
							SNew(STextBlock).Text(FGameModeTypeId::GetText(GameMode)).TextStyle(&Style->Helper.ImageTitle)
						]
						+ SVerticalBox::Slot().AutoHeight().Padding(0, 4)
						[
							SNew(STextBlock).Text(FText::Format(FText::FromString("{0}{1}"), FGameModeTypeId::GetTextDesc(GameMode), OptionalDesc)).TextStyle(&Style->Helper.ImageDesc).AutoWrapText(true)
						]
					]
				]
			]
		];
}

FText SLobbyContainer_Top::GetPlayerName() const
{
	return FText::FromString(FPlayerEntityInfo::Instance.Login);
}


int32 SLobbyContainer_Top::GetPlayerLevel() const
{
	return FPlayerEntityInfo::Instance.Experience.Level;

}

int32 SLobbyContainer_Top::GetPlayerMoney() const
{
	return FPlayerEntityInfo::Instance.Cash.Money;
}

int32 SLobbyContainer_Top::GetPlayerDonate() const
{
	return FPlayerEntityInfo::Instance.Cash.Donate;

}

TOptional<float> SLobbyContainer_Top::GetPlayerProgress_Exp() const
{
	return float(FPlayerEntityInfo::Instance.Experience.Experience) / float(FPlayerEntityInfo::Instance.Experience.NextExperience);
}

FText SLobbyContainer_Top::GetPlayerProgressText_Exp() const
{
	return FText::Format(NSLOCTEXT("SLobbyContainer", "Lobby.Experience", "Experience: {0}/{1}"), FText::AsNumber(FPlayerEntityInfo::Instance.Experience.Experience), FText::AsNumber(FPlayerEntityInfo::Instance.Experience.NextExperience));
}

FText SLobbyContainer_Top::GetFractionRankName() const
{
	//TODO: SeNTIke
	//if (Repository)
	//{
	//	if (FPlayerEntityInfo::Instance.FractionTypeId >= 0 && FPlayerEntityInfo::Instance.FractionTypeId < FractionTypeId::End)
	//	{
	//		if (auto const TargetFraction = Repository->Fraction[FPlayerEntityInfo::Instance.FractionTypeId])
	//		{
	//			return TargetFraction->RankList[FMath::Clamp(TargetFraction->Progress.CurrentRank - 1, 0, TargetFraction->RankList.Num() - 1)].Name;
	//		}
	//		else return FText::FromString("Invalid fraction");
	//	}
	//	else return FText::FromString("Out of fraction range");
	//}

	return GetPlayerName();
}

FText SLobbyContainer_Top::GetFractionName() const
{
	//TODO: SeNTIke
	//if (Repository)
	//{
	//	
	//	if (FPlayerEntityInfo::Instance.FractionTypeId >= 0 && FPlayerEntityInfo::Instance.FractionTypeId < FractionTypeId::End)
	//	{
	//		if (auto const TargetFraction = Repository->Fraction[FPlayerEntityInfo::Instance.FractionTypeId])
	//		{
	//			return TargetFraction->GetDescription().Name;
	//		}
	//		else return FText::FromString("Invalid fraction");
	//	}
	//	else return FText::FromString("Out of fraction range");
	//}

	return GetPlayerName();
}

TOptional<float> SLobbyContainer_Top::GetFractionProgress_Exp() const
{
	//TODO: SeNTIke
	//if (Repository)
	//{
	//	
	//	if (FPlayerEntityInfo::Instance.FractionTypeId >= 0 && FPlayerEntityInfo::Instance.FractionTypeId < FractionTypeId::End)
	//	{
	//		if (auto const TargetFraction = Repository->Fraction[FPlayerEntityInfo::Instance.FractionTypeId])
	//		{
	//			return float(TargetFraction->Progress.Reputation) / float(TargetFraction->Progress.NextReputation);
	//		}
	//		return .0f;
	//	}
	//	return .0f;
	//}

	return GetPlayerProgress_Exp();
}

FText SLobbyContainer_Top::GetFractionProgressText_Exp() const
{
	//TODO: SeNTIke
	//if (Repository)
	//{
	//	
	//	if (FPlayerEntityInfo::Instance.FractionTypeId >= 0 && FPlayerEntityInfo::Instance.FractionTypeId < FractionTypeId::End)
	//	{
	//		if (auto const TargetFraction = Repository->Fraction[FPlayerEntityInfo::Instance.FractionTypeId])
	//		{
	//			return FText::Format(NSLOCTEXT("SLobbyContainer", "Lobby.Reputation", "Reputation: {0}/{1}"), FText::AsNumber(TargetFraction->Progress.Reputation), FText::AsNumber(TargetFraction->Progress.NextReputation));
	//		}
	//		return FText::FromString("0/0");
	//	}
	//	return FText::FromString("0/0");
	//}

	return GetPlayerProgressText_Exp();
}

const FSlateBrush* SLobbyContainer_Top::GetFractionIcon() const
{
	static const FSlateBrush* NoResource;

	if (!NoResource)
	{
		NoResource = new FSlateNoResource();
	}

	//TODO: SeNTIke
	//if (Repository)
	//{
	//	
	//	if (FPlayerEntityInfo::Instance.FractionTypeId >= 0 && FPlayerEntityInfo::Instance.FractionTypeId < FractionTypeId::End)
	//	{
	//		if (auto const TargetFraction = Repository->Fraction[FPlayerEntityInfo::Instance.FractionTypeId])
	//		{
	//			return TargetFraction->GetIcon();
	//		}
	//	}
	//}

	return NoResource;
}

// Into Fight

void SLobbyContainer_Top::OnClickedIntoFight(ECheckBoxState State, const bool IsAlternative, const int32 InAddonFlag)
{
	if (IsAlternative)
	{
		if (SearchSessionStatus == ESearchSessionState::None && !IsSquadMemeber)
		{
			Widget_BigModeAnimated[0]->ToggleWidget(true);
			Widget_BigModeAnimated[1]->ToggleWidget(true);
		}
	}
	else
	{
		if (IsSquadMemeber)
		{
			if (SearchSessionStatus == ESearchSessionState::None)
			{
				if (IsSquadMemeberReady)
				{
					OnSearchSessionCancel.ExecuteIfBound();
				}
				else
				{
					OnSearchSessionBegin.ExecuteIfBound(FSearchSessionParams());
				}
			}
			else
			{
				OnSearchSessionCancel.ExecuteIfBound();
			}
		}
		else
		{
			if (SearchSessionStatus == ESearchSessionState::None)
			{
				FSearchSessionParams searchParams;
				searchParams.GameModeTypeId = SelectedGameModesFlags;
				searchParams.GameModeAdditionalFlag = InAddonFlag;

				if (searchParams.GameModeTypeId == EGameModeTypeId::None)
				{
					//Widget_PveWindow->ToggleWidget(true);
					FFlagsHelper::SetFlags(searchParams.GameModeTypeId, EGameModeTypeId::TeamDeadMatch | EGameModeTypeId::LostDeadMatch | EGameModeTypeId::ResearchMatch | EGameModeTypeId::DuelMatch);
				}
				//else
				//{
					if (OnSearchSessionBegin.ExecuteIfBound(searchParams))
					{
						SearchSessionStatus = static_cast<ESearchSessionState>(static_cast<uint8>(ESearchSessionState::Search) | static_cast<uint8>(ESearchSessionState::Waiting));
					}

					CurrentGameModeTypeId = static_cast<EGameModeTypeId::Type>(searchParams.GameModeTypeId);
				//}
			}
			else if (SearchSessionStatus == ESearchSessionState::Search)
			{
				if (OnSearchSessionCancel.ExecuteIfBound())
				{
					SearchSessionStatus = static_cast<ESearchSessionState>(static_cast<uint8>(ESearchSessionState::None) | static_cast<uint8>(ESearchSessionState::Waiting));

					SelectedGameModesFlags = EGameModeTypeId::None;
					CurrentGameModeTypeId = EGameModeTypeId::None;
				}
			}
		}
	}
}

FText SLobbyContainer_Top::GetIntoFightButtonText() const
{
	if (SearchSessionStatus == ESearchSessionState::None)
	{
		if (IsSquadMemeber == true)
		{
			if (IsSquadMemeberReady)
				return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SquadNotReady", "Not ready");
			return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SquadReady", "Ready");
		}
		return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SessionSearchBegin", "Into Battle");
	}

	if (SearchSessionStatus == ESearchSessionState::Search)
	{
		return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SessionSearchCancel", "Cancel");
	}

	if (SearchSessionStatus == ESearchSessionState::Complete)
	{
		return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SessionSearchComplete", "Complete");
	}

	return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SessionSearchWait", "Waiting");
}

void SLobbyContainer_Top::OnSearchSessionStatusRecived()
{
	const auto OldStatus = SearchSessionStatus;
	SearchSessionStatus = static_cast<ESearchSessionState>(static_cast<uint8>(SearchSessionStatus) ^ static_cast<uint8>(ESearchSessionState::Waiting));

	if (SearchSessionStatus != OldStatus)
	{
		SearchTime = FDateTime::UtcNow().ToUnixTimestamp();
	}
}

void SLobbyContainer_Top::OnSearchSessionStatusFailed()
{
	SearchSessionStatus = ESearchSessionState::None;

	SelectedGameModesFlags = EGameModeTypeId::None;
}

FText SLobbyContainer_Top::GetLevelText() const
{
	return FText::Format(NSLOCTEXT("SLobbyContainer", "SLobbyContainer.GetLevelText", "Level: {0}"), FText::AsNumber(1));
}

FText SLobbyContainer_Top::GetTimeText() const
{
	return FText::AsTime(FDateTime::FromUnixTimestamp(120), EDateTimeStyle::Short);
}

bool SLobbyContainer_Top::GetIsEnableModeChange() const
{
	if (!IsSquadMemeber)
	{
		return SearchSessionStatus == ESearchSessionState::None;
	}

	return SearchSessionStatus == ESearchSessionState::None && !IsSquadMemeberReady;
}

void SLobbyContainer_Top::SetRepository(const FInstanceRepository* InRepository)
{
	Repository = InRepository;
}

void SLobbyContainer_Top::OnSquadInformationUpdate(const FSqaudInformation& information)
{
	IsSquadMemeber = false;
	IsSquadMemeberReady = false;
	CurrentGameModeTypeId = information.GameModeId;
	if (information.Status == ESearchSessionStatus::Search)
		SearchSessionStatus = ESearchSessionState::Search;
	else if (information.Status == ESearchSessionStatus::Prepare)
		SearchSessionStatus = ESearchSessionState::Waiting;
	else if (information.Status == ESearchSessionStatus::Complete)
		SearchSessionStatus = ESearchSessionState::Complete;
	else SearchSessionStatus = ESearchSessionState::None;
	SearchTime = information.ElapsedSeconds;


		for (auto& m : information.Members)
		{
			int32 Value = m.Status.GetValue();
			if (m.PlayerId == FPlayerEntityInfo::Instance.PlayerId)
			{
				if (FFlagsHelper::HasAnyFlags(Value, PlayerFriendStatus::Leader))
				{
					IsSquadMemeberReady = true;
				}
				else
				{
					IsSquadMemeber = true;
				}

				if (FFlagsHelper::HasAnyFlags(Value, PlayerFriendStatus::Ready))
				{
					IsSquadMemeberReady = true;
				}
			}
		}
	
}

void SLobbyContainer_Top::OnFillRaiting(const FPlayerRatingTopView& InRating)
{
	Widget_PveWindow->OnFillRaiting(InRating);
}

int64 SLobbyContainer_Top::GetBoosterTime(const EResourceTextBoxType::Type InType) const
{
	for (auto Service : FPlayerEntityInfo::Instance.UsingService)
	{
		if (SResourceTextBox::ServiceModelToType(Service.ServiceTypeId) == InType)
		{
			return Service.ExpirationDate;
		}
	}
	

	return 0;
}