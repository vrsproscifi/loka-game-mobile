// VRSPRO

#pragma once


#include "SLobbyContainer_Top.h"
#include "Marketing/Lobby/LobbyActionView.h"
#include "Analytics/AnalyticsInterface.h"

class SNotifyList;
class SLobbyChat;
class SFriendsForm;
class SSettingsManager;
class SLobbyAchievements;

DECLARE_MULTICAST_DELEGATE_TwoParams(FOnScrollablyWidget, const ELobbyActionButton&, const ELobbyActionButton&);

class LOKAGAME_API SLobbyContainer : public SCompoundWidget, public FLokaAnalyticsHelper
{
public:
	SLobbyContainer();

	SLATE_BEGIN_ARGS(SLobbyContainer)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FLobbyContainerStyle>("SLobbyContainerStyle"))
		, _RefNotifyList(SNullWidget::NullWidget)
		, _RefLobbyChat(SNullWidget::NullWidget)
		, _RefFriendsForm(SNullWidget::NullWidget)
		, _RefSettingsManager(SNullWidget::NullWidget)
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FLobbyContainerStyle, Style)
	SLATE_ARGUMENT(TSharedRef<SWidget>, RefNotifyList)
	SLATE_ARGUMENT(TSharedRef<SWidget>, RefLobbyChat)
	SLATE_ARGUMENT(TSharedRef<SWidget>, RefFriendsForm)
	SLATE_ARGUMENT(TSharedRef<SWidget>, RefSettingsManager)
	SLATE_EVENT(FOnClickedOutside, OnQuit)
	SLATE_EVENT(FOnClickedOutside, OnSearchSessionCancel)
	SLATE_EVENT(FOnStartSearchGame, OnSearchSessionBegin)
	SLATE_EVENT(FOnClcikedBooster, OnClickedBooster)
	SLATE_EVENT(FOnClickedOutside, OnClickedBuildMode)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void AddScrollablyWidget(const ELobbyActionButton, TSharedRef<SWidget>, const FText&);
	bool SetScrollablyWidget(const ELobbyActionButton, TSharedRef<SWidget>);
	void SetActiveScrollablyWidget(const ELobbyActionButton);

	void ToggleWidget(const bool);

	void OnSquadInformationUpdate(const FSqaudInformation& information);

	TSharedRef<SLobbyContainer_Top> GetContainerTopWidget()
	{
		return Widget_ContainerTop.ToSharedRef();
	}

	FOnScrollablyWidget OnSwitchScrollablyWidget;

protected:
	const FLobbyContainerStyle* Style;

	FOnClickedOutside OnQuit;	
	FOnClickedOutside OnClickedBuildMode;

	TMap<ELobbyActionButton, int32> ScrollablyWidgetsMap;

	TSharedPtr<class SScrollableContainer> ScrollablyContainer;
	TArray<TSharedRef<SWidget>> ScrollablyWidgets;
	TSharedPtr<SHorizontalBox> ButtonsContainer;
	TArray<TSharedPtr<SCheckBox>> ButtonsWidgets;

	TSharedPtr<class SAnimatedBackground> 
		  Widget_FxTop
		, Widget_FxBottom
		, Widget_FxCenter;

	void OnSetScrollablyWidget(ECheckBoxState, const int32);
	ECheckBoxState IsCheckedWidget(const int32) const;

	// Specific widgets
	TSharedRef<SNotifyList> Widget_NotifyList;
	TSharedRef<SLobbyChat> Widget_LobbyChat;
	TSharedRef<SFriendsForm> Widget_FriendsForm;
	TSharedRef<SSettingsManager> Widget_SettingsManager;
	TSharedPtr<SCheckBox> CheckBox_Quit;
	TSharedPtr<SLobbyContainer_Top> Widget_ContainerTop;

	TSharedPtr<SCheckBox> Button_NotifyList, Button_LobbyChat, Button_FriendsForm, Button_SettingsManager;

	EVisibility GetVisibilityByCheck_LobbyChat() const;
	EVisibility GetVisibilityByCheck_FriendsForm() const;
	EVisibility GetVisibilityByCheck_SettingsManager() const;
};