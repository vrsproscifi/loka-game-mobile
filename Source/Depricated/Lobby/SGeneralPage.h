// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/GeneralPageWidgetStyle.h"
#include "Operation/GetPlayerRatingTop.h"
#include "RequestOneParam.h"

#include "SFriendsForm.h"
#include "MarketingController/MarketingController.h"

struct FPlayerEntityInfo;
struct FSocialTaskWrapper;
struct FImprovementView;

typedef TSharedPtr<FImprovementView> FImprovementViewPtr;

DECLARE_DELEGATE_OneParam(FOnActivatePromoCode, const FRequestOneParam<FString>&);

class LOKAGAME_API SGeneralPage : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SGeneralPage)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FGeneralPageStyle>("SGeneralPageStyle"))
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FGeneralPageStyle, Style)
	SLATE_EVENT(FOnActivatePromoCode, OnClickedSuggestion)
	SLATE_EVENT(Operation::VoteImprovement::Operation::FOnRequest, OnVoteChange)
	SLATE_EVENT(Operation::VoteSentence::Operation::FOnRequest, OnVoteProposal)
	SLATE_EVENT(Operation::PushSentence::Operation::FOnRequest, OnCreateProposal)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);
	void OnFillRaiting(const FPlayerRatingTopView&);
	void SetBuildTime_MasterServer(const int32&);

	void AddSocialTask(const FSocialTaskWrapper&);
	void ClearCodeInput();

	void SetSpecificWidget(TSharedRef<SWidget>, const bool /*IsFriends*/);
	void OnFillIssues(const FImprovementViewModel&);

	FOnActivatePromoCode OnReedemCode;

	int64 TargetTimeStart, TargetTimeEnd;

protected:

	FOnActivatePromoCode OnClickedSuggestion;

	TArray<TSharedPtr<FPlayerRatingTopContainer>> TopTablePlayers;
	TSharedPtr<SListView<TSharedPtr<FPlayerRatingTopContainer>>> Widget_TopTable;
	TSharedPtr<SEditableTextBox> Input_ReedemCode;
	TSharedPtr<SHorizontalBox> SocialTaskContainer;
	TSharedPtr<SVerticalBox> SideContainer_Left, SideContainer_Right;
	TSharedPtr<class SWindowVotingForChange> Widget_VotingForChanges;
	TSharedPtr<class SWindowVotingForProposals> Widget_VotingForProposals;
	TSharedPtr<class SWindowDetailsForProposals> Widget_DetailsForProposals;


	const FGeneralPageStyle* Style;

	int32 BuildTime_MasterServer;

	void OpenVotingForChanges();
	void OpenVotingForProposals();
	void OpenDetailsForProposals(TSharedRef<FImprovementView> InData, const bool IsReadOnly = true);

	TSharedPtr<SListView<FImprovementViewPtr>>
		  Widget_ListInDev
		, Widget_ListTopProposals;

	TArray<FImprovementViewPtr>
		  ItemsInDev
		, ItemsTopProposals;

	Operation::PushSentence::Operation::FOnRequest OnCreateProposal;
};
