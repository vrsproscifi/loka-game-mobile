// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/LobbyContainerWidgetStyle.h"
#include "Operation/GetPlayerRatingTop.h"

struct FPlayerEntityInfo;

class LOKAGAME_API SLobbyContainer_PVE : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SLobbyContainer_PVE)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FLobbyContainerStyle>("SLobbyContainerStyle"))
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FLobbyContainerStyle, Style)
	SLATE_EVENT(FOnClicked, OnClickedPVP)
	SLATE_EVENT(FOnClicked, OnClickedPVPA)
	SLATE_EVENT(FOnInt32ValueChanged, OnClickedPVE)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);
	void OnFillRaiting(const FPlayerRatingTopView&);
	void ToggleWidget(const bool);

protected:

	TSharedRef<SWidget> BuildLevelButton(const int32 InLevel, const int32 maximumLevel, const TArray<FPlayerPvEMatchStatisticView>& InRaiting = TArray<FPlayerPvEMatchStatisticView>());
	TSharedRef<SWidget> BuildPlacePlayer(const int32 InPlace, const FPlayerPvEMatchStatisticView& InPlayer = FPlayerPvEMatchStatisticView());

	const FLobbyContainerStyle* Style;

	TSharedPtr<class SAnimatedBackground> Widget_AnimatedBackground[3];
	TSharedPtr<class SScrollBox> Widget_ScrollBox;

	FOnClicked OnClickedPVP;
	FOnInt32ValueChanged OnClickedPVE;

	bool IsThisIsPlayer(const FPlayerPvEMatchStatisticView& InPlayer) const;
};
