// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SLobbyContainer_PVE.h"
#include "SlateOptMacros.h"

#include "SAnimatedBackground.h"
#include "SWindowBackground.h"
#include "Layout/SScaleBox.h"
#include "IdentityController/Models/PlayerEntityInfo.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SLobbyContainer_PVE::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	OnClickedPVP = InArgs._OnClickedPVP;
	OnClickedPVE = InArgs._OnClickedPVE;
	

	const float SidesWidth = .6f;

	ChildSlot
	[
		SAssignNew(Widget_AnimatedBackground[0], SAnimatedBackground)
		.InitAsHide(true)
		.ShowAnimation(EAnimBackAnimation::Color)
		.HideAnimation(EAnimBackAnimation::Color)
		.IsRelative(false)
		.Duration(0.8f)
		.IsEnabledBlur(true)
		[
			SNew(SBorder).Padding(FMargin(0, 120))
			.BorderImage(new FSlateColorBrush(FColor::Black.WithAlpha(210)))
			.OnMouseButtonDown_Lambda([&](const FGeometry&, const FPointerEvent&) {
				ToggleWidget(false);
				return FReply::Handled();
			})
			[
				SNew(SOverlay)
				+ SOverlay::Slot() // PVE - PVP Selector
				.HAlign(HAlign_Center)
				.VAlign(VAlign_Center)
				[
					SAssignNew(Widget_AnimatedBackground[2], SAnimatedBackground)
					.InitAsHide(true)
					.ShowAnimation(EAnimBackAnimation::UpFade)
					.HideAnimation(EAnimBackAnimation::DownFade)
					.IsRelative(false)
					.Duration(0.5f)
					[
						SNew(SWindowBackground)
						.Padding(FMargin(10, 6))
						[
							SNew(SHorizontalBox)
							+ SHorizontalBox::Slot() // PVE
							[
								SNew(SButton)
								.ButtonStyle(&Style->Helper.ImageButton)
								.ContentPadding(10)
								.OnClicked_Lambda([&]() {
									Widget_AnimatedBackground[1]->ToggleWidget(true);
									Widget_AnimatedBackground[2]->ToggleWidget(false);
									return FReply::Handled();
								})
								[
									SNew(SBox).WidthOverride(390).HeightOverride(390)
									[
										SNew(SOverlay)
										+ SOverlay::Slot()
										[
											SNew(SScaleBox)
											.Stretch(EStretch::ScaleToFit)
											[
												SNew(SImage).Image(&Style->Pve.PVE_Image)
											]
										]
										+ SOverlay::Slot().VAlign(VAlign_Center)
										[
											SNew(SBorder)
											.BorderImage(new FSlateColorBrush(FColor::Black.WithAlpha(210))).HAlign(HAlign_Center)
											[
												SNew(SVerticalBox)
												+ SVerticalBox::Slot().AutoHeight()
												[
													SNew(STextBlock).Text(NSLOCTEXT("SLobbyContainer_PVE", "PVE.Name", "Player vs Environment")).TextStyle(&Style->Helper.ImageTitle)
												]
												+ SVerticalBox::Slot().AutoHeight().Padding(0, 4)
												[
													SNew(STextBlock).Text(NSLOCTEXT("SLobbyContainer_PVE", "PVE.Desc", "The series of levels is designed to pass either alone or with friends in a squad.")).TextStyle(&Style->Helper.ImageDesc).AutoWrapText(true)
												]
											]
										]
									]
								]
							]
							+ SHorizontalBox::Slot() // PVP + Advanced
							[
								SNew(SButton)
								.ButtonStyle(&Style->Helper.ImageButton)
								.ContentPadding(10)
								.OnClicked(OnClickedPVP)
								[
									SNew(SBox).WidthOverride(390).HeightOverride(390)
									[
										SNew(SOverlay)
										+ SOverlay::Slot()
										[
											SNew(SScaleBox)
											.Stretch(EStretch::ScaleToFit)
											[
												SNew(SImage).Image(&Style->Pve.PVP_Image)
											]
										]
										+ SOverlay::Slot().VAlign(VAlign_Center)
										[
											SNew(SBorder)
											.BorderImage(new FSlateColorBrush(FColor::Black.WithAlpha(210))).HAlign(HAlign_Center)
											[
												SNew(SVerticalBox)
												+ SVerticalBox::Slot().AutoHeight()
												[
													SNew(STextBlock).Text(NSLOCTEXT("SLobbyContainer_PVE", "PVP.Name", "Player vs Player")).TextStyle(&Style->Helper.ImageTitle)
												]
												+ SVerticalBox::Slot().AutoHeight().Padding(0, 4)
												[
													SNew(STextBlock).Text(NSLOCTEXT("SLobbyContainer_PVE", "PVP.Desc", "Random map, random game mode and random players.")).TextStyle(&Style->Helper.ImageDesc).AutoWrapText(true)
												]
											]
										]
										+ SOverlay::Slot().VAlign(VAlign_Bottom)
										[
											SNew(SBox).HeightOverride(64)
											[
												SNew(SButton)
												.ButtonStyle(&Style->Pve.Button_PVP)
												.TextStyle(&Style->Pve.Button_PVP_Font)
												.HAlign(HAlign_Center)
												.VAlign(VAlign_Center)
												.ContentPadding(FMargin(20, 6))
												.Text(NSLOCTEXT("SLobbyContainer_PVE", "PVP.AdvBtn", "Select Mode"))
												.OnClicked(InArgs._OnClickedPVPA)
											]
										]
									]
								]
							]
						]
					]
				]
				+ SOverlay::Slot() // PVP Only Selector
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot().FillWidth(SidesWidth)
					+ SHorizontalBox::Slot().FillWidth(1.0f)
					[
						SAssignNew(Widget_AnimatedBackground[1], SAnimatedBackground)
						.InitAsHide(true)
						.ShowAnimation(EAnimBackAnimation::UpFade)
						.HideAnimation(EAnimBackAnimation::DownFade)
						.IsRelative(false)
						.Duration(0.5f)
						[
							SNew(SWindowBackground)
							.Padding(FMargin(10, 6))
							[
								SAssignNew(Widget_ScrollBox, SScrollBox)
								.ScrollBarStyle(&FLokaSlateStyle::Get().GetWidgetStyle<FScrollBarStyle>("Default_ScrollBar")).ScrollBarThickness(FVector2D(4, 4))
							]
						]
					]
					+ SHorizontalBox::Slot().FillWidth(SidesWidth)
				]
			]
		]		
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SLobbyContainer_PVE::OnFillRaiting(const FPlayerRatingTopView& InRating)
{
	int32 maxLevel = -1;
	for (const auto level : FPlayerEntityInfo::Instance.PvEPlayerRating)
	{
		if (level.Level > maxLevel) maxLevel = level.Level;
	}
	maxLevel++;

	TArray<FPlayerPvEMatchStatisticView> PvEPlayerRating;
	PvEPlayerRating.AddZeroed(15);

	for (const auto level : FPlayerEntityInfo::Instance.PvEPlayerRating)
	{
		PvEPlayerRating[level.Level] = level;
	}

	Widget_ScrollBox->ClearChildren();
	for (int32 i = 0; i < 100; ++i)
	{
		auto TargetPlayers = InRating.PvERating.FilterByPredicate([ThisLevel = i](const FPlayerPvEMatchStatisticView& Source) { return Source.Level == ThisLevel; });
		TargetPlayers.Sort(TGreater<FPlayerPvEMatchStatisticView>());

		if (TargetPlayers.Num() >= 3)
		{
			TargetPlayers.RemoveAt(3, TargetPlayers.Num() - 3);
		}

		if (PvEPlayerRating.IsValidIndex(i) && TargetPlayers.FindByPredicate([&](const FPlayerPvEMatchStatisticView& Source) { return IsThisIsPlayer(Source); }) == nullptr)
		{
			TargetPlayers.Add(PvEPlayerRating[i]);
		}

		Widget_ScrollBox->AddSlot().Padding(0, 2)
			[
				BuildLevelButton(i, maxLevel, TargetPlayers)
			];
	}
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SLobbyContainer_PVE::ToggleWidget(const bool InToggle)
{
	if (InToggle)
	{
		Widget_AnimatedBackground[0]->ToggleWidget(true);
		Widget_AnimatedBackground[2]->ToggleWidget(true);
	}
	else
	{
		Widget_AnimatedBackground[0]->ToggleWidget(false);
		Widget_AnimatedBackground[1]->ToggleWidget(false);
		Widget_AnimatedBackground[2]->ToggleWidget(false);
	}
}

TSharedRef<SWidget> SLobbyContainer_PVE::BuildLevelButton(const int32 InLevel, const int32 maximumLevel, const TArray<FPlayerPvEMatchStatisticView>& InRaiting)
{
	return SNew(SButton)
	.IsEnabled_Lambda([&, l = InLevel, m = maximumLevel]() { return m >= l || l == 0; })
	.ButtonStyle(&Style->Pve.Button_PVE)
	.ContentPadding(0)
	.OnClicked_Lambda([&, l = InLevel]() { OnClickedPVE.ExecuteIfBound(l); return FReply::Handled(); })
	[
		SNew(SHorizontalBox)
		+ SHorizontalBox::Slot().AutoWidth()
		[
			SNew(SBox).WidthOverride(100).HeightOverride(100)
			[
				SNew(SOverlay)
				+ SOverlay::Slot()
				[
					SNew(SBorder)
					.BorderImage(&Style->Pve.Button_PVE_Number_Background)
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					[
						SNew(STextBlock)
						.TextStyle(&Style->Pve.Button_PVE_Number_Font)
						.Text(FText::AsNumber(InLevel + 1))
					]
				]
				+ SOverlay::Slot().VAlign(VAlign_Bottom).Padding(0, 2)
				[
					SNew(STextBlock)
					.TextStyle(&Style->Pve.Button_PVE_Number_Desc_Font)
					.Text(NSLOCTEXT("SLobbyContainer_PVE", "SLobbyContainer_PVE.PVELVL", "Level PVE"))
					.Justification(ETextJustify::Center)
				]
			]
		]
		+ SHorizontalBox::Slot()
		[
			SNew(SBorder)
			.BorderImage(new FSlateColorBrush(FColor::Black.WithAlpha(210)))
			.HAlign(HAlign_Fill)
			.VAlign(VAlign_Center)
			.Padding(FMargin(80, 0))
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot().AutoHeight()
				[
					BuildPlacePlayer(1, InRaiting.IsValidIndex(0) ? InRaiting[0] : FPlayerPvEMatchStatisticView())
				]
				+ SVerticalBox::Slot().AutoHeight()
				[
					BuildPlacePlayer(2, InRaiting.IsValidIndex(1) ? InRaiting[1] : FPlayerPvEMatchStatisticView())
				]
				+ SVerticalBox::Slot().AutoHeight()
				[
					BuildPlacePlayer(3, InRaiting.IsValidIndex(2) ? InRaiting[2] : FPlayerPvEMatchStatisticView())
				]
				+ SVerticalBox::Slot().AutoHeight()
				[
					BuildPlacePlayer(4, InRaiting.IsValidIndex(3) ? InRaiting[3] : FPlayerPvEMatchStatisticView())
				]
			]
		]
	];
}

TSharedRef<SWidget> SLobbyContainer_PVE::BuildPlacePlayer(const int32 InPlace, const FPlayerPvEMatchStatisticView& InPlayer)
{
	if (InPlace == 4)
	{
		return (InPlayer.PlayerId.IsEmpty() == false) ?	SNew(SHorizontalBox)
			+ SHorizontalBox::Slot().FillWidth(.2f)
			[
				SNew(STextBlock)
				.TextStyle(&Style->Pve.Font_Places_Local)
				.Text(FText::FromString("..."))
			]
			+ SHorizontalBox::Slot()
			[
				SNew(STextBlock)
				.TextStyle(&Style->Pve.Font_Places_Local)
				.Text(FText::FromString(InPlayer.PlayerName))
			]
			+ SHorizontalBox::Slot().FillWidth(.6f)
			[
				SNew(STextBlock)
				.TextStyle(&Style->Pve.Font_Places_Local)
				.Text(FText::AsNumber(InPlayer.Score))
				.Justification(ETextJustify::Right)
			] : SNullWidget::NullWidget;
	}

	return	SNew(SHorizontalBox)
			+ SHorizontalBox::Slot().FillWidth(.2f)
			[
				SNew(STextBlock)
				.TextStyle(IsThisIsPlayer(InPlayer) ? &Style->Pve.Font_Places_Local : &Style->Pve.Font_Places)
				.Text(FText::AsNumber(InPlace))
			]
			+ SHorizontalBox::Slot()
			[
				SNew(STextBlock)
				.TextStyle(IsThisIsPlayer(InPlayer) ? &Style->Pve.Font_Places_Local : &Style->Pve.Font_Places)
				.Text(FText::FromString(InPlayer.PlayerName))
			]
			+ SHorizontalBox::Slot().FillWidth(.6f)
			[
				SNew(STextBlock)
				.TextStyle(IsThisIsPlayer(InPlayer) ? &Style->Pve.Font_Places_Local : &Style->Pve.Font_Places)
				.Text(FText::AsNumber(InPlayer.Score))
				.Justification(ETextJustify::Right)
			];
}

bool SLobbyContainer_PVE::IsThisIsPlayer(const FPlayerPvEMatchStatisticView& InPlayer) const
{
	if (FPlayerEntityInfo::Instance.PvEPlayerRating.IsValidIndex(0))
	{
		auto MyPlayer = FPlayerEntityInfo::Instance.PvEPlayerRating[0];
		return MyPlayer.PlayerId == InPlayer.PlayerId;
	}

	return false;
}