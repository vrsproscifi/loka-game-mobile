// VRSPRO

#include "LokaGame.h"
#include "WeaponModular.h"

#include "WidgetLayoutLibrary.h"
#include "Weapon/SWeaponModularManager.h"
#include "PlayerController/LobbyPlayerController.h"
#include "Kismet/KismetMathLibrary.h"
#include "LobbyCamera.h"

#include "ShooterGameInstance.h"
#include "Weapon/ItemWeaponEntity.h"
#include "Module/ItemModuleEntity.h"
#include "EntityRepository.h"

FName EModularSockets::Center = TEXT("CenterOfWeapon");
FName EModularSockets::Aim = TEXT("AimModule");
FName EModularSockets::Muzzle = TEXT("MuzzleModule");
FName EModularSockets::Pointer = TEXT("PointerModule");

AWeaponModular::AWeaponModular(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	//auto ForFixCapsule = ObjectInitializer.CreateDefaultSubobject<UVectorFieldComponent>(this, TEXT("ForFixCapsule"));
	//ForFixCapsule->AttachParent = RootComponent;

	auto ArrowComp = ObjectInitializer.CreateDefaultSubobject<UArrowComponent>(this, TEXT("Arrow"));
	ArrowComp->SetupAttachment(RootComponent);

	Capsule = ObjectInitializer.CreateDefaultSubobject<UCapsuleComponent>(this, TEXT("Capsule")); 
	Capsule->SetRelativeRotation(FRotator(-90.0f, 180.0f, 90.0f));
	Capsule->SetupAttachment(ArrowComp);
	Capsule->bAbsoluteLocation = false;
	Capsule->bAbsoluteRotation = false;
	Capsule->bAbsoluteScale = false;

	BaseMesh = ObjectInitializer.CreateDefaultSubobject<USkeletalMeshComponent>(this, TEXT("BaseMesh"));
	BaseMesh->SetupAttachment(Capsule);

	PrimaryActorTick.bCanEverTick = true;

	for (int32 i = 0; i < 10; ++i)
	{
		InstalledModulesMesh.Add(ObjectInitializer.CreateDefaultSubobject<USkeletalMeshComponent>(this, *FString::Printf(TEXT("Module_%d"), i)));
		InstalledModulesMesh.Last()->SetupAttachment(RootComponent);
		InstalledModulesMesh.Last()->bReceivesDecals = false;
		InstalledModulesMesh.Last()->bComponentUseFixedSkelBounds = true;
		InstalledModulesMesh.Last()->SetVisibility(false, true);
	}
}

void AWeaponModular::BeginPlay()
{
	Super::BeginPlay();

	if (auto MyController = Cast<ALobbyPlayerController>(GetWorld()->GetFirstPlayerController()))
	{
		TargetTransform = this->GetTransform();// Capsule->GetRelativeTransform();
		MyController->SetModularRef(this);
	}
}

void AWeaponModular::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );		
	
	SetActorLocation(FMath::VInterpTo(GetActorLocation(), TargetPosition, DeltaTime, 2.5f));

	TargetRotation = FMath::RInterpTo(TargetRotation, FRotator::ZeroRotator, DeltaTime, 10.0f);
	Capsule->AddWorldRotation(TargetRotation);
}

void AWeaponModular::AddRotation(const FRotator Val)
{
	FRotator Target = Val;

	float Alpha = GetActorForwardVector().Y; //for find pitch and roll
	if (Alpha < .0f) Alpha *= -1.0f;
	Alpha = FMath::Clamp(Alpha, .0f, 1.f);

	Target.Roll = FMath::Lerp(Target.Roll, Target.Pitch, Alpha);
	Target.Pitch -= Target.Roll;

	TargetRotation += Target;

	GEngine->AddOnScreenDebugMessage(1, 5.0f, FColor::White, GetActorForwardVector().ToString());
}

void AWeaponModular::SetRotation(const FRotator Val)
{
	Capsule->SetRelativeRotation(Val);
}

FRotator AWeaponModular::GetRotation() const
{
	return Capsule->RelativeRotation;
}

FVector2D AWeaponModular::GetSocketToScreen(const FName& Name) const
{
	FVector2D Result;
	FVector SocketLocation = BaseMesh->GetSocketLocation(Name);
	UWidgetLayoutLibrary::ProjectWorldLocationToWidgetPosition(GetWorld()->GetFirstPlayerController(), SocketLocation, Result);

	return Result;
}

void AWeaponModular::BeginActivate(const FVector Location, const FRotator Rotation)
{
	TargetPosition = Location;

	Capsule->SetRelativeRotation(FRotator(90.0f, 0.0f, 90.0f));
	SetActorLocation(TargetPosition + FVector(.0f, .0f, 200.0f));

	SetActorRotation(Rotation);
}

void AWeaponModular::BeginDeactivate()
{
	TargetPosition -= FVector(.0f, .0f, 200.0f);
}


void AWeaponModular::SetInstance(const UItemWeaponEntity* InInstance)
{
	Instance = InInstance;

	for (auto &c : InstalledModulesMesh)
	{
		c->SetVisibility(false, true);
	}

	InstalledModules.Empty();

	if (Instance)
	{
		BaseMesh->SetSkeletalMesh(Instance->GetSkeletalMesh());
		BaseMesh->SetMaterial(0, Instance->GetCurrentMaterial());

		FAttachmentTransformRules Rules(EAttachmentRule::SnapToTarget, false);		

		for (auto s : Instance->SupportedSockets)
		{
			for (auto &c : InstalledModulesMesh)
			{
				if (!c->IsVisible())
				{
					InstalledModulesMeshMap.Add(s, c);
					c->SetSkeletalMesh(nullptr);
					c->AttachToComponent(BaseMesh, FAttachmentTransformRules::SnapToTargetIncludingScale, s);
					c->bComponentUseFixedSkelBounds = true;
					c->SetVisibility(true, true);
					break;
				}
			}
		}
	}

	auto GameInst = Cast<UShooterGameInstance>(GetWorld()->GetGameInstance());
	if (GameInst)
	{
		auto Repository = GameInst->EntityRepository->InstanceRepository;

		if (Instance->DefaultModules.Num())
		{
			TArray<uint8> NoInstall;
			for (auto i : Instance->DefaultModules)
			{
				if (i >= 0 && i < ItemModuleModelId::End && Repository.Module[i])
				{
					for (auto &m : Instance->InstalledModules)
					{
						InstallModule(m);

						if (m->TargetSocket == Repository.Module[i]->TargetSocket)
						{
							NoInstall.Add(i);
						}
					}
				}
			}

			for (auto i : Instance->DefaultModules)
			{
				if (NoInstall.Contains(i) == false)
				{
					if (i >= 0 && i < ItemModuleModelId::End && Repository.Module[i])
					{
						InstallModule(Repository.Module[i]);
					}
				}
			}
		}
		else
		{
			for (auto i : Instance->SupportedModules)
			{
				if (i >= 0 && i < ItemModuleModelId::End && Repository.Module[i])
				{
					for (auto &m : Instance->InstalledModules)
					{
						InstallModule(m);
					}
				}
			}
		}
	}
}

void AWeaponModular::InstallModule(UItemModuleEntity* InModule)
{
	if (InstalledModulesMeshMap.Contains(InModule->TargetSocket))
	{
		InstalledModulesMeshMap[InModule->TargetSocket]->SetSkeletalMesh(InModule->GetSkeletalMesh());
		auto &module = InstalledModules.FindOrAdd(InModule->TargetSocket);
		module = InModule;
	}
}

void AWeaponModular::UninstallModule(UItemModuleEntity* InModule)
{
	if (InstalledModulesMeshMap.Contains(InModule->TargetSocket))
	{
		InstalledModulesMeshMap[InModule->TargetSocket]->SetSkeletalMesh(nullptr);
		InstalledModules.Remove(InModule->TargetSocket);
	}
}