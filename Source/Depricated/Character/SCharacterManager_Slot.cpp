// VRSPRO

#include "LokaGame.h"
#include "SCharacterManager_Slot.h"
#include "SCharacterManager_Item.h"
#include "LokaDragDrop.h"
#include "SlateOptMacros.h"

#include "Components/SLokaToolTip.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SCharacterManager_Slot::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	AllowContent = InArgs._AllowContent;

	OnItemEquip = InArgs._OnItemEquip;
	OnItemUnEquip = InArgs._OnItemUnEquip;

	ChildSlot
	[
		SNew(SBox).HeightOverride(96)
		[
			SNew(SOverlay)
			+ SOverlay::Slot()
			[
				SAssignNew(Button, SButton)
				.ButtonStyle(&Style->Slot.Button.ButtonStyle)
			]
			+ SOverlay::Slot().HAlign(HAlign_Center).VAlign(VAlign_Center)
			[
				SNew(STextBlock)
				.Text(InArgs._Description)
				.TextStyle(&Style->Slot.Text)
				.Visibility_Lambda([&]() { return Button->GetContent() == SNullWidget::NullWidget ? EVisibility::HitTestInvisible : EVisibility::Hidden; })
			]
		]
	];

	Content = SNullWidget::NullWidget;

	AllowDropStyle = FButtonStyle(Style->Slot.Button.ButtonStyle)
		.SetNormal(Style->Slot.AllowDrop)
		.SetHovered(Style->Slot.AllowDrop)
		.SetPressed(Style->Slot.AllowDrop)
		;

	DisallowDropStyle = FButtonStyle(Style->Slot.Button.ButtonStyle)
		.SetNormal(Style->Slot.DisallowDrop)
		.SetHovered(Style->Slot.DisallowDrop)
		.SetPressed(Style->Slot.DisallowDrop)
		;
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SCharacterManager_Slot::OnDragEnter(const FGeometry& MyGeometry, const FDragDropEvent& DragDropEvent)
{
	SCompoundWidget::OnDragEnter(MyGeometry, DragDropEvent);

	auto DragDropOpeartion = DragDropEvent.GetOperationAs<FLokaDragDropOp>();

	if (DragDropOpeartion.IsValid())
	{
		auto OriginWidget = DragDropOpeartion->GetSourceAs<SCharacterManager_Item>();

		if(OriginWidget.GetSharedReferenceCount() > 0 && OriginWidget != SNullWidget::NullWidget && IsAllowDrop(OriginWidget->GetInventorySlot()))
			Button->SetButtonStyle(&AllowDropStyle);
		else
			Button->SetButtonStyle(&DisallowDropStyle);
	}
}

void SCharacterManager_Slot::OnDragLeave(const FDragDropEvent& DragDropEvent)
{
	SCompoundWidget::OnDragLeave(DragDropEvent);
	RevertButtonBorder();

	auto DragDropOpeartion = DragDropEvent.GetOperationAs<FLokaDragDropOp>();

	if (DragDropOpeartion.IsValid())
	{
		auto OriginWidget = DragDropOpeartion->GetSourceAs<SCharacterManager_Item>();
		if (Button->GetContent() == OriginWidget)
		{
			SetContent(SNullWidget::NullWidget);
			OnItemUnEquip.ExecuteIfBound(OriginWidget->GetInstance());
		}
	}
}

FReply SCharacterManager_Slot::OnDrop(const FGeometry& MyGeometry, const FDragDropEvent& DragDropEvent)
{
	auto DragDropOpeartion = DragDropEvent.GetOperationAs<FLokaDragDropOp>();

	if (DragDropOpeartion.IsValid())
	{
		auto OriginWidget = DragDropOpeartion->GetSourceAs<SCharacterManager_Item>();
		
		if (IsAllowDrop(OriginWidget->GetInventorySlot()))
		{
			//auto NewWidget = SNew(SCharacterManager_Item, nullptr).Source(OriginWidget).State(SCharacterManager_Item::EItemState::InSlot);

			//SetContent(NewWidget);
			OnItemEquip.ExecuteIfBound(OriginWidget->GetInstance());
		}

		return FReply::Handled().EndDragDrop();
	}

	return SCompoundWidget::OnDrop(MyGeometry, DragDropEvent);
}

void SCharacterManager_Slot::RevertButtonBorder()
{
	Button->SetButtonStyle(&Style->Slot.Button.ButtonStyle);
}

bool SCharacterManager_Slot::IsAllowDrop(const uint8 _slot) const
{
	return AllowContent == _slot;
}

void SCharacterManager_Slot::SetContent(TSharedRef<SWidget> _Content)
{
	Content = _Content;
	Button->SetContent(Content.ToSharedRef());
}

TSharedRef<SWidget> SCharacterManager_Slot::GetContent() const
{
	return Content.ToSharedRef();
}