// VRSPRO

#include "LokaGame.h"
#include "SCharacterManager.h"
#include "SlateOptMacros.h"

#include "Components/SMovableWidget.h"
#include "SCharacterManager_Item.h"
#include "SCharacterManager_Lock.h"
#include "Layout/SScaleBox.h"
#include "SAnimatedBackground.h"

#include "WidgetLayoutLibrary.h"
#include "Character/CharacterModelId.h"

#include "SMessageBox.h"

#define LOCTEXT_NAMESPACE "SCharacterManager"


BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SCharacterManager::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	OnChangeAsset = InArgs._OnChangeAsset;
	OnSwitchCharacter = InArgs._OnSwitchCharacter;
	OnConfirmCharacterSelect = InArgs._OnConfirmCharacterSelect;
	OnCancelCharacterSelect = InArgs._OnCancelCharacterSelect;
	OnUnlockCharacter = InArgs._OnUnlockCharacter;
	
	//AnimatedBackgrounds

	MAKE_UTF8_SYMBOL(sSlideLeft, 0xf053);
	MAKE_UTF8_SYMBOL(sSlideRight, 0xf054);
	MAKE_UTF8_SYMBOL(sInformation, 0xf129);

	ChildSlot
	[
		SNew(SOverlay)
		+ SOverlay::Slot()
		[
			SAssignNew(AnimatedBackgrounds[0], SAnimatedBackground)
			.ShowAnimation(EAnimBackAnimation::UpFade)
			.HideAnimation(EAnimBackAnimation::DownFade)
			.Duration(1.0f)
			.InitAsHide(true)
			[
				InArgs._OtherManager
			]
		]
		+ SOverlay::Slot()
		.HAlign(HAlign_Fill)
		.VAlign(VAlign_Fill)
		[			
			SNew(SOverlay)
			+ SOverlay::Slot().VAlign(VAlign_Center).HAlign(HAlign_Left)
			[
				SNew(SBox).WidthOverride(480).HeightOverride(720)
				[
					SAssignNew(AnimatedBackgrounds[2], SAnimatedBackground)
					.ShowAnimation(EAnimBackAnimation::LeftFade)
					.HideAnimation(EAnimBackAnimation::LeftFade)
					.Duration(1.0f)
					.InitAsHide(false)
					[
						SAssignNew(SetManager, SCharacterManager_Sets)
						//TODO:SeNTike
						//.Visibility_Lambda([&c = CurrentCharacter]() { if (c && c->IsLocked()) return EVisibility::Hidden; return EVisibility::Visible; })
						.OnChangeSet(this, &SCharacterManager::OnChangeSetEvent)
						.OnChangeSetName(InArgs._OnChangeSetName)
						.OnItemEquip(InArgs._OnItemEquip)
						.OnItemUnEquip(InArgs._OnItemUnEquip)
						.OnItemModify(InArgs._OnItemModify)
						.OnItemSell(InArgs._OnItemSell)
						.OnAmmoAmountChange(InArgs._OnAmmoAmountChange)
			//TODO:SeNTike
						//.IsEnabled_Lambda([&]() { return (CurrentCharacter && !CurrentCharacter->IsLocked() && IsEnableInventory.IsSet() && IsEnableInventory.Get()); })
						.CharacterInstance_Lambda([&]() { return CurrentCharacter; })
					]
				]
			]
			+ SOverlay::Slot().Padding(FMargin(0, 10)).HAlign(HAlign_Fill)
			[
				SAssignNew(AnimatedBackgrounds[1], SAnimatedBackground)
				.ShowAnimation(EAnimBackAnimation::LeftFade)
				.HideAnimation(EAnimBackAnimation::LeftFade)
				.Duration(1.0f)
				.InitAsHide(true)
				[
					SAssignNew(ModifyManager, SCharacterManager_Modify)
					.OnClickedBack(InArgs._OnClickedBack)
				]
			]
		]
		+ SOverlay::Slot()
		.HAlign(HAlign_Right)
		.VAlign(VAlign_Center)
		[
			SNew(SBox).WidthOverride(540).HeightOverride(720)
			[
				SAssignNew(AnimatedBackgrounds[3], SAnimatedBackground)
				.ShowAnimation(EAnimBackAnimation::RightFade)
				.HideAnimation(EAnimBackAnimation::RightFade)
				.Duration(1.0f)
				.InitAsHide(false)
				[
					SAssignNew(InventoryManager, SCharacterManager_List)
					.OnItemModify(InArgs._OnItemModify)
					.OnItemSell(InArgs._OnItemSell)
			//TODO:SeNTike
					//.IsEnabled_Lambda([&]() { return (CurrentCharacter && !CurrentCharacter->IsLocked() && IsEnableInventory.IsSet() && IsEnableInventory.Get()); })
					.OnItemInstall(InArgs._OnItemInstall)
				]
			]
		]
		+ SOverlay::Slot()
		.HAlign(HAlign_Fill)
		.VAlign(VAlign_Center)
		.Padding(FMargin(580, 0))
		[
			SAssignNew(AnimatedBackgrounds[5], SAnimatedBackground)
			.ShowAnimation(EAnimBackAnimation::UpFade)
			.HideAnimation(EAnimBackAnimation::DownFade)
			.Duration(1.0f)
			.InitAsHide(false)
			.IsEnabled_Lambda([&]() { return (IsEnableInventory.IsSet() && IsEnableInventory.Get()); })
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().AutoWidth()
				[
					SNew(SButton)
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					.ButtonStyle(&Style->Select.BigButtons)
					.TextStyle(&Style->Select.BigButtonsText)
					.ContentPadding(20)
					.Text(FText::FromString(sSlideLeft))
					.OnClicked_Lambda([&]() {
						CharactersSlider->SetActiveValueIndex(CharactersSlider->GetActiveValueIndex() - 1, false);
						return FReply::Handled();
					})
				]
				+ SHorizontalBox::Slot().FillWidth(1)
				+ SHorizontalBox::Slot().AutoWidth()
				[
					SNew(SButton)
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					.ButtonStyle(&Style->Select.BigButtons)
					.TextStyle(&Style->Select.BigButtonsText)
					.ContentPadding(20)
					.Text(FText::FromString(sSlideRight))
					.OnClicked_Lambda([&]() {
						CharactersSlider->SetActiveValueIndex(CharactersSlider->GetActiveValueIndex() + 1, false);
						return FReply::Handled();
					})
				]
			]
		]
		+ SOverlay::Slot()
		.HAlign(HAlign_Center)
		.VAlign(VAlign_Bottom)
		[
			SNew(SBox).MinDesiredWidth(320)
			[
				SAssignNew(AnimatedBackgrounds[4], SAnimatedBackground)
				.ShowAnimation(EAnimBackAnimation::UpFade)
				.HideAnimation(EAnimBackAnimation::DownFade)
				.Duration(1.0f)
				.InitAsHide(false)
				.IsEnabled_Lambda([&]() { return (IsEnableInventory.IsSet() && IsEnableInventory.Get()); })
				[
					SNew(SVerticalBox)
					+ SVerticalBox::Slot().AutoHeight()
					.VAlign(VAlign_Bottom)
					[
						SAssignNew(CharactersSlider, SSliderSpin)
						.OnListValue(this, &SCharacterManager::OnCharacterList)
					]
					+ SVerticalBox::Slot().AutoHeight()
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Top)
					[
						SNew(SButton)
						.ButtonStyle(&Style->Select.Button)
						.TextStyle(&Style->Select.ButtonText)
						.HAlign(HAlign_Center)
						.VAlign(VAlign_Center)
						.ContentPadding(FMargin(20, 2))
						.Text(LOCTEXT("Character.Select", "SELECT"))
						.OnClicked(this, &SCharacterManager::OnClickedSelectCharacter)
						//TODO:SeNTike
						//.Visibility_Lambda([&]() { if (CurrentCharacter && CurrentCharacter->IsLocked()) return EVisibility::Hidden; return EVisibility::Visible; })
					]
				]
			]
		]
		+ SOverlay::Slot()
		.HAlign(HAlign_Left)
		.VAlign(VAlign_Center)
		.Padding(FMargin(10, 0))
		[
			SNew(SBox)
			.WidthOverride(460)
			.HeightOverride(380)
			[
				SAssignNew(LockWindow, SCharacterManager_Lock)
				//TODO:SeNTike
				//.Visibility_Lambda([&c = CurrentCharacter]() { if (c && c->IsLocked()) return EVisibility::Visible; return EVisibility::Hidden; })
			//TODO:SeNTike
				//.OnClicked_Lambda([&]() { OnUnlockCharacter.ExecuteIfBound(FUnlockItemRequestContainer(CurrentCharacter->GetInstanceModelId(), CategoryTypeId::Character)); return FReply::Handled(); })
				.Character_Lambda([&]() { return CurrentCharacter; })
			]
		]
		+ SOverlay::Slot()
		.HAlign(HAlign_Center)
		.VAlign(VAlign_Top)
		.Padding(FMargin(320, 120, 0, 0))
		[
			SAssignNew(AnimatedBackgrounds[6], SAnimatedBackground)
			.ShowAnimation(EAnimBackAnimation::Color)
			.HideAnimation(EAnimBackAnimation::Color)
			.Duration(1.0f)
			.InitAsHide(false)
			[
				SNew(SButton)
				//TODO:SeNTike
				//.Visibility_Lambda([&c = CurrentCharacter]() { if (c && c->IsLocked()) return EVisibility::Hidden; return EVisibility::Visible; })
				.HAlign(HAlign_Center)
				.VAlign(VAlign_Center)
				.ButtonStyle(&Style->Select.BigButtons)
				.TextStyle(&Style->Select.BigButtonsText)
				.ContentPadding(20)
				.Text(FText::FromString(sInformation))
				.OnClicked_Lambda([&c = CurrentCharacter]() {
			//TODO:SeNTike
					//SMessageBox::Get()->SetHeaderText(c->GetDescription().Name);
					//SMessageBox::Get()->SetContent(c->GetDescription().Description);
					//SMessageBox::Get()->SetEnableClose(false);
					//SMessageBox::Get()->SetButtonsText(FButtonNameLocalization::Ok);
					//SMessageBox::Get()->OnMessageBoxButton.AddLambda([](const EMessageBoxButton& Button) {
					//	SMessageBox::Get()->ToggleWidget(false);
					//});
					//
					//SMessageBox::Get()->ToggleWidget(true);
					return FReply::Handled();
				})
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SCharacterManager::SetPlayerControllerRef(class ALobbyPlayerController *Ctrl)
{
	ControllerRef = Ctrl;
}

void SCharacterManager::OnChangeSetEvent(const EPlayerPreSetId::Type& setId)
{
	OnChangeAsset.ExecuteIfBound(setId);
}

void SCharacterManager::OnCharacterList(const int32& Value)
{
	if (auto Character = CharacterList.FindChecked(Value))
	{
		CurrentCharacter = Character;
		OnSwitchCharacter.ExecuteIfBound(CurrentCharacter);
	}
}

bool SCharacterManager::AddCharacter(const UCharacterBaseEntity* Character)
{
	//TODO:SeNTike
	//if (CharacterList.FindKey(Character) == nullptr)
	//{
	//	CharacterList.Add(CharactersSlider->AddValue(Character->GetDescription().Name), Character);
	//	return true;
	//}
	return false;
}

bool SCharacterManager::ReplaceCharacter(const UCharacterBaseEntity* InFrom, const UCharacterBaseEntity* InTo)
{
	if (auto Key = CharacterList.FindKey(InFrom))
	{
		CharacterList[*Key] = InTo;
		return true;
	}
	return false;
}

bool SCharacterManager::SetCharacter(const UCharacterBaseEntity* Character)
{
	if (auto Key = CharacterList.FindKey(Character))
	{
		CurrentCharacter = Character;
		CharactersSlider->SetActiveValueIndex(*Key);
		return true;
	}
	return false;
}

bool SCharacterManager::SetCharacterByModel(const CharacterModelId::Type ModelId)
{
	//TODO:SeNTike
	//for (auto &c : CharacterList)
	//{
	//	if (c.Value && c.Value->IsValidLowLevel() && c.Value->GetModelId() == ModelId)
	//	{
	//		return SetCharacter(c.Value);
	//	}
	//}

	return false;
}

FReply SCharacterManager::OnClickedSelectCharacter() const
{
	if (CurrentCharacter)
	{
		OnConfirmCharacterSelect.ExecuteIfBound(CurrentCharacter);
	}
	return FReply::Handled();
}

void SCharacterManager::SwitchMode(const bool Mode)
{
	FSlateApplication::Get().PlaySound(Mode ? Style->Modify.SoundOpen : Style->Modify.SoundClose);

	AnimatedBackgrounds[0]->ToggleWidget(Mode); // Mode = true
	AnimatedBackgrounds[1]->ToggleWidget(Mode); // Mode = true	

	AnimatedBackgrounds[2]->ToggleWidget(!Mode); // Mode = false
	AnimatedBackgrounds[3]->ToggleWidget(!Mode); // Mode = false
	AnimatedBackgrounds[4]->ToggleWidget(!Mode); // Mode = false
	AnimatedBackgrounds[5]->ToggleWidget(!Mode); // Mode = false
	AnimatedBackgrounds[6]->ToggleWidget(!Mode); // Mode = false
}

#undef LOCTEXT_NAMESPACE