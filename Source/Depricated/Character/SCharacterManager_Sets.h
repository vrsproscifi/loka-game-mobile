// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/CharacterManagerWidgetStyle.h"
#include "SCharacterManager_Item.h"
#include "Item/CategoryTypeId.h"
#include <Item/ItemSetSlotId.h>
#include <PlayerPreSetId.h>
#include "StoreController/Operation/UnlockItem.h"


class UItemBaseEntity;
class UItemAmmoEntity;
class UCharacterBaseEntity;

DECLARE_DELEGATE_OneParam(FOnChangeSet, const EPlayerPreSetId::Type&);
DECLARE_DELEGATE_TwoParams(FOnChangeSetName, const int32&, const FText&);



//namespace EPlayerPreSetId {enum Type;}

DECLARE_DELEGATE_ThreeParams(FOnItemActionSetSlot, const UItemBaseEntity*, const EItemSetSlotId::Type&, const EPlayerPreSetId::Type&);
DECLARE_DELEGATE_ThreeParams(FOnAmmoAmountChange, const UItemAmmoEntity*, const EPlayerPreSetId::Type&, const int32&);

class LOKAGAME_API SCharacterManager_Sets : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SCharacterManager_Sets)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FCharacterManagerStyle>("SCharacterManagerStyle"))
	{}
		SLATE_STYLE_ARGUMENT(FCharacterManagerStyle, Style)
		SLATE_ATTRIBUTE(const UCharacterBaseEntity*, CharacterInstance)
		SLATE_EVENT(FOnChangeSet, OnChangeSet)
		SLATE_EVENT(FOnChangeSetName, OnChangeSetName)
		SLATE_EVENT(FOnItemActionSetSlot, OnItemEquip)
		SLATE_EVENT(FOnItemActionSetSlot, OnItemUnEquip)
		SLATE_EVENT(FOnItemAction, OnItemModify)
		SLATE_EVENT(FOnItemAction, OnItemSell)
		SLATE_EVENT(FOnAmmoAmountChange, OnAmmoAmountChange)
	SLATE_END_ARGS()



	void Construct(const FArguments& InArgs);

	void SetCurrentSet(const EPlayerPreSetId::Type&);
	int32 GetGetCurrentPreSet() const;

	void InstallAsset(const UItemBaseEntity*, const EPlayerPreSetId::Type);
	void InstallAsset(const UItemBaseEntity*, const EPlayerPreSetId::Type, const EItemSetSlotId::Type);
	void UninstallAsset(const UItemBaseEntity*, const EPlayerPreSetId::Type);
	void UninstallSlot(const EItemSetSlotId::Type, const EPlayerPreSetId::Type);

	void SetAmmoAmount(const UItemAmmoEntity*, const EPlayerPreSetId::Type&, const EItemSlotTypeId& TargetWeapon, const int32&);

	void SetSetName(const EPlayerPreSetId::Type&, const FText&);
	FText GetSetName(const EPlayerPreSetId::Type&) const;

	void SetPresetIsAvaliable(const EPlayerPreSetId::Type&, const TAttribute<bool>&);
	void SetPresetIsEnable(const EPlayerPreSetId::Type&, const TAttribute<bool>&);
	void SetPresetIcon(const EPlayerPreSetId::Type&, const TAttribute<const FSlateBrush*>&);

	FOnItemActionSetSlot OnItemEquip;
	FOnItemActionSetSlot OnItemUnEquip;

	Operation::FOnUnlockItem OnUnlockPreset;

protected:

	float GetAssetWidth(const EPlayerPreSetId::Type) const;

	FCurveSequence AnimInstance[5];
	FCurveHandle AnimHandle[5];

	FOnChangeSet OnChangeSet;
	FOnChangeSetName OnChangeSetName;
	FOnAmmoAmountChange OnAmmoAmountChange;

	const FCharacterManagerStyle* Style;

	EPlayerPreSetId::Type CurrentSet;
	TAttribute<const UCharacterBaseEntity*> CharacterInstance;
	TAttribute<const FSlateBrush*> ProfileIcon[EPlayerPreSetId::End];

	void ChangeCurrentSet(ECheckBoxState, const EPlayerPreSetId::Type);
	void OnChangeSetNameEvent(const FText&, const EPlayerPreSetId::Type);
	ECheckBoxState GetSetIsSelected(const EPlayerPreSetId::Type) const;
	
	TSharedPtr<class SCharacterManager_Asset> ProfileContainer[5];

	void OnItemEquipEvent(const UItemBaseEntity*, const EItemSetSlotId::Type&, const EPlayerPreSetId::Type) const;
	void OnItemUnEquipEvent(const UItemBaseEntity*, const EItemSetSlotId::Type&, const EPlayerPreSetId::Type) const;

	void OnAmmoAmountChangeEvent(const UItemAmmoEntity*, const int32&, const EPlayerPreSetId::Type) const;
};
