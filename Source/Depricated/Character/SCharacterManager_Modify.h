// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/CharacterManagerWidgetStyle.h"
#include "HangarController/Operation/ItemUpgrade.h"
#include "HangarController/Operation/ConfirmItemUpgrade.h"
#include "Material/ItemMaterialEntity.h"

struct FUpgradeParameter
{
	FUpgradeParameter(const int32& i) : Index(i), Name(), Value() {}
	FUpgradeParameter(const FText& n, const float v) : Index(INDEX_NONE), Name(n), Value(v) {}
	FUpgradeParameter(const int32& i, const FText& n, const float v) : Index(i), Name(n), Value(v) {}

	int32 Index;
	FText Name;
	float Value;
};

struct FInstanceRepository;

DECLARE_DELEGATE_TwoParams(FOnChangeMaterial, const UItemBaseEntity*, const UItemMaterialEntity*);

class LOKAGAME_API SCharacterManager_Modify : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SCharacterManager_Modify)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FCharacterManagerStyle>("SCharacterManagerStyle"))
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FCharacterManagerStyle, Style)
	SLATE_EVENT(FOnClicked, OnClickedBack)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void SetInstance(const UItemBaseEntity* InInstance);
	void SetInstanceRepository(const FInstanceRepository* InInstance);

	void OnUpdateList(const TArray<FItemModification>&);
	void OnConfirmedUpdate();
	Operation::ItemUpgrade::FOnGetUpdateList OnGetUpdateList;
	Operation::ConfirmItemUpgrade::FOnConfirmUpdate OnConfirmUpdate;

	FOnChangeMaterial OnChangeMaterial;
	FOnChangeMaterial OnBuyMaterial;

protected:

	const FCharacterManagerStyle* Style;
	const UItemBaseEntity* Instance;	
	const FInstanceRepository* InstanceRepository;

	TSharedPtr<SCheckBox> Widget_CheckBoxTextures;
	TArray<TSharedPtr<FUpgradeParameter>> UpgradeParameters, CurrentParameters;
	TSharedPtr<SListView<TSharedPtr<FUpgradeParameter>>> UpgradeParameters_List, CurrentParameters_List;
	TSharedPtr<class SItemProperty> Widget_ItemProperty;
	TSharedPtr<class SAnimatedBackground> UpgradeBorder, TexturesBorder;

	//FItemMaterial
	TArray<UItemMaterialEntity*> TextureParameters;
	TSharedPtr<STileView<UItemMaterialEntity*>> TextureParameters_List;

	FText GetItemName() const;
	const FSlateBrush* GetItemImage() const;

	TSharedRef<SWidget> BuildParameter(const int32&, TSharedPtr<FUpgradeParameter>);

	enum EModiferSlot
	{
		// Base
		Modifed = 1,
		Ready = 2,
		Locked = 4,
		// Helpers
		Cancel = 8,
		Accept = 16,
		End = 0
	};

	FString sBack, sAccept,	sCancel, sRefresh, sLocked;
};			