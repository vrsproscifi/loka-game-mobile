// VRSPRO

#include "LokaGame.h"
#include "SCharacterManager_Modify.h"
#include "SlateOptMacros.h"

#include "Layout/SScaleBox.h"
#include "SItemProperty.h"
#include "SResourceTextBox.h"
#include "SAnimatedBackground.h"
#include "SLokaToolTip.h"

#include "InstanceRepository.h"

class LOKAGAME_API SCharacterManager_Material : public STableRow<UItemMaterialEntity*>
{
	typedef STableRow<UItemMaterialEntity*> Super;

public:
	SLATE_BEGIN_ARGS(SCharacterManager_Material)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FCharacterManagerStyle>("SCharacterManagerStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FCharacterManagerStyle, Style)
	SLATE_END_ARGS()

	BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
	void Construct(const FArguments& InArgs, const TSharedRef<STableViewBase>& InOwnerTableView, UItemMaterialEntity* Item)
	{
		Style = InArgs._Style;
		Instance = Item;

		auto Widget_Content = 
		SNew(SScaleBox)
		.Stretch(EStretch::ScaleToFit)
		[
			SNew(SImage).Image(Instance->GetIcon())
		];

		Super::Construct(Super::FArguments().Style(&Style->Modify.TileStyle).Padding(2)[Widget_Content], InOwnerTableView);

		this->SetToolTip(SNew(SLokaToolTip).Text(Item->GetDescription().Name));
	}
	END_SLATE_FUNCTION_BUILD_OPTIMIZATION

protected:

	const FCharacterManagerStyle* Style;
	UItemMaterialEntity* Instance;

	virtual void OnMouseEnter(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override
	{
		Super::OnMouseEnter(MyGeometry, MouseEvent);
	}

	virtual void OnMouseLeave(const FPointerEvent& MouseEvent) override
	{
		Super::OnMouseLeave(MouseEvent);
	}
};

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SCharacterManager_Modify::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	ASSIGN_UTF8_SYMBOL(sBack, 0xf053);
	ASSIGN_UTF8_SYMBOL(sAccept, 0xf00c);
	ASSIGN_UTF8_SYMBOL(sCancel, 0xf05e);
	ASSIGN_UTF8_SYMBOL(sRefresh, 0xf021); 
	ASSIGN_UTF8_SYMBOL(sLocked, 0xf023);

	SAssignNew(TextureParameters_List, STileView<UItemMaterialEntity* >)
		.ScrollbarVisibility(EVisibility::Collapsed)
		.ItemHeight(80)
		.ItemWidth(80)
		.ItemAlignment(EListItemAlignment::CenterAligned)
		.SelectionMode(ESelectionMode::Single)
		.ListItemsSource(&TextureParameters)
		.OnGenerateTile_Lambda([&](UItemMaterialEntity* Item, const TSharedRef<STableViewBase>& OwnerTable)
		{
			return SNew(SCharacterManager_Material, OwnerTable, Item);
		})
		.OnSelectionChanged_Lambda([&](UItemMaterialEntity* Item, ESelectInfo::Type Direction)
		{		
			if (Instance && Direction != ESelectInfo::Direct)
			{
				OnChangeMaterial.ExecuteIfBound(Instance, Item);
			}
		}
	);

	SAssignNew(CurrentParameters_List, SListView<TSharedPtr<FUpgradeParameter>>)
		.SelectionMode(ESelectionMode::None)
		.ListItemsSource(&CurrentParameters)
		.OnGenerateRow_Lambda([&](TSharedPtr<FUpgradeParameter> Item, const TSharedRef<STableViewBase>& OwnerTable) 
		{
			return SNew(STableRow<TSharedPtr<FUpgradeParameter>>, OwnerTable).Padding(FMargin(0, 2))
			[
				SNew(SBox).HeightOverride(64)
				[
					SNew(SButton)
					.ButtonStyle(&Style->Modify.ModiferBack)
					.ContentPadding(0)
					[
						BuildParameter(Item->Index, Item)
					]
				]
			];
	});

	SAssignNew(UpgradeParameters_List, SListView<TSharedPtr<FUpgradeParameter>>)
		.SelectionMode(ESelectionMode::None)
		.ListItemsSource(&UpgradeParameters)
		.OnGenerateRow_Lambda([&](TSharedPtr<FUpgradeParameter> Item, const TSharedRef<STableViewBase>& OwnerTable)
		{
			return SNew(STableRow<TSharedPtr<FUpgradeParameter>>, OwnerTable).Padding(FMargin(0, 2))
			[
				SNew(SBox).HeightOverride(64)
				[
					SNew(SButton)
					.ButtonStyle(&Style->Modify.ModiferBack)
					.ContentPadding(0)
					[
						BuildParameter(EModiferSlot::Accept, Item)
					]
				]
			];
	});	

	ChildSlot
	[
		SNew(SOverlay)
		+ SOverlay::Slot().HAlign(HAlign_Left)
		[
			SNew(SBox).WidthOverride(400)
			[
				SNew(SBorder)
				.Padding(0)
				.BorderImage(&Style->Modify.Background)
				[
					SNew(SBorder)
					.Padding(8)
					.BorderImage(&Style->Modify.Border)
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot().AutoHeight() // Name
						[
							SNew(SHorizontalBox)
							+ SHorizontalBox::Slot().AutoWidth()
							[
								SNew(SButton)
								.ButtonStyle(&Style->Modify.ButtonBack)
								.TextStyle(&Style->Modify.ButtonBackFont)
								.Text(FText::FromString(sBack))
								.HAlign(HAlign_Center)
								.VAlign(VAlign_Center)
								.ContentPadding(FMargin(10, 4))
								.OnClicked(InArgs._OnClickedBack)
							]
							+ SHorizontalBox::Slot().FillWidth(1).Padding(FMargin(4, 2))
							[
								SNew(STextBlock)
								.TextStyle(&Style->Modify.Header)
								.Text(this, &SCharacterManager_Modify::GetItemName)
								.AutoWrapText(true)
							]
						]
						+ SVerticalBox::Slot().AutoHeight() // Preview
						[
							SNew(SBox).HeightOverride(240)
							[
								SNew(SOverlay)
								+ SOverlay::Slot()
								[
									SNew(SScaleBox)
									.Stretch(EStretch::ScaleToFit)
									[
										SNew(SImage).Image(this, &SCharacterManager_Modify::GetItemImage)
									]
								]
								+ SOverlay::Slot().HAlign(HAlign_Right).VAlign(VAlign_Top)
								[
									SAssignNew(Widget_CheckBoxTextures, SCheckBox).IsChecked(ECheckBoxState::Checked).Visibility(EVisibility::Hidden)
								]
							]
						]
						+ SVerticalBox::Slot().FillHeight(1).Padding(FMargin(10, 0)) // Characteristics
						[
							SAssignNew(Widget_ItemProperty, SItemProperty, nullptr).IsShowedModifers(false)
						]
						+ SVerticalBox::Slot().AutoHeight() // Modifers list
						[
							CurrentParameters_List.ToSharedRef()
						]
						+ SVerticalBox::Slot().AutoHeight()
						[
							SNew(SBox).HeightOverride(8)
						]
					]
				]
			]
		]
		+ SOverlay::Slot().VAlign(VAlign_Top).HAlign(HAlign_Left).Padding(FMargin(410, 0, 10, 0))
		[
			SNew(SBox).HeightOverride(100).WidthOverride(658)
			[
				SNew(SBorder)
				.Padding(0)
				.BorderImage(&Style->Modify.Background)
				.Visibility_Lambda([&]() { return Widget_CheckBoxTextures->IsChecked() ? EVisibility::Visible : EVisibility::Collapsed; })
				[
					SNew(SBorder).VAlign(VAlign_Center)
					.Padding(4)
					.BorderImage(&Style->Modify.Border)
					[
						TextureParameters_List.ToSharedRef()
					]
				]
			]
		]
		+ SOverlay::Slot().VAlign(VAlign_Bottom).HAlign(HAlign_Left).Padding(FMargin(410, 0, 0, 0))
		[
			SNew(SBox).WidthOverride(400)
			[
				SAssignNew(UpgradeBorder, SAnimatedBackground)
				.ShowAnimation(EAnimBackAnimation::ZoomOut)
				.HideAnimation(EAnimBackAnimation::ZoomIn)
				.Duration(1.0f)
				.InitAsHide(true)
				[
					SNew(SBorder)
					.Padding(0)
					.BorderImage(&Style->Modify.Background)
					[
						SNew(SBorder)
						.Padding(8)
						.BorderImage(&Style->Modify.Border)
						[
							SNew(SVerticalBox)
							+ SVerticalBox::Slot().AutoHeight()
							[
								SNew(STextBlock).Text(NSLOCTEXT("SCharacterManager", "SCharacterManager.ImprovementDesc", "Select improvement from the list below")).Justification(ETextJustify::Center)
							]
							+ SVerticalBox::Slot().AutoHeight()
							[
								UpgradeParameters_List.ToSharedRef()
							]
							+ SVerticalBox::Slot().AutoHeight()
							[
								SNew(SBox).HeightOverride(8)
							]
						]
					]
				]
			]
		]
		+ SOverlay::Slot().VAlign(VAlign_Top).HAlign(HAlign_Right).Padding(FMargin(220, 180))
		[
			SNew(SBox).MinDesiredWidth(400)
			.Visibility_Lambda([&]() {
			//TODO: SeNTIke
				//if (TextureParameters_List->GetSelectedItems().Num() && TextureParameters_List->GetSelectedItems()[0])
				//{
				//	return TextureParameters_List->GetSelectedItems()[0]->IsLocked() ? EVisibility::Visible : EVisibility::Hidden;
				//}
				return EVisibility::Hidden;
			})
			[
				SNew(SBorder)
				.Padding(0)
				.BorderImage(&Style->Modify.Background)
				[
					SNew(SBorder)
					.Padding(8)
					.BorderImage(&Style->Modify.Border)
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(8, 6))
						[
							SNew(SHorizontalBox)
							+ SHorizontalBox::Slot().HAlign(HAlign_Center).VAlign(VAlign_Center)
							[
								SNew(STextBlock)
								.TextStyle(&Style->Modify.SkinTextName)
								.Text_Lambda([&]() {
									if (TextureParameters_List->GetSelectedItems().Num())
									{
										return TextureParameters_List->GetSelectedItems()[0]->GetDescription().Name;
									}
									return FText::GetEmpty();
								})
							]
							+ SHorizontalBox::Slot().HAlign(HAlign_Right).VAlign(VAlign_Center).AutoWidth()
							[
								SNew(SResourceTextBox)
								.CustomSize(FVector(16, 16, 12))
								.Type_Lambda([&]() {
								//TODO: SeNTIke

									//if (TextureParameters_List->GetSelectedItems().Num())
									//{
									//	return TextureParameters_List->GetSelectedItems()[0]->GetCost().IsDonate ? EResourceTextBoxType::Donate : EResourceTextBoxType::Money;
									//}
									return EResourceTextBoxType::Money;
								})
								.Value_Lambda([&]() {
									//TODO: SeNTIke
									//if (TextureParameters_List->GetSelectedItems().Num())
									//{
									//	return TextureParameters_List->GetSelectedItems()[0]->GetCost().Amount;
									//}
									return int64(0);
								})
							]
						]
						+ SVerticalBox::Slot().AutoHeight()
						[
							SNew(SHorizontalBox)
							+ SHorizontalBox::Slot()
							[
								SNew(SButton)
								.HAlign(HAlign_Center)
								.VAlign(VAlign_Center)
								.ContentPadding(FMargin(10, 4))
								.ButtonStyle(&Style->Modify.SkinButtonBuy)
								.TextStyle(&Style->Modify.SkinTextBuy)
								.Text(NSLOCTEXT("SCharacterManager_Modify", "Skin.Buy", "BUY"))
								.OnClicked_Lambda([&]() {
									if (TextureParameters_List->GetSelectedItems().Num())
									{
										OnBuyMaterial.ExecuteIfBound(Instance, TextureParameters_List->GetSelectedItems()[0]);
									}

									return FReply::Handled();
								})
							]
							+ SHorizontalBox::Slot()
							[
								SNew(SButton)
								.HAlign(HAlign_Center)
								.VAlign(VAlign_Center)
								.ContentPadding(FMargin(10, 4))
								.ButtonStyle(&Style->Modify.SkinButtonCancel)
								.TextStyle(&Style->Modify.SkinTextCancel)
								.Text(NSLOCTEXT("SCharacterManager_Modify", "Skin.Cancel", "CANCEL"))
								.OnClicked_Lambda([&]() {
									TextureParameters_List->SetSelection(TextureParameters[0]);
									return FReply::Handled();
								})
							]
						]
					]
				]
			]
		]
	];

	CurrentParameters_List->RequestListRefresh();
	TextureParameters_List->RequestListRefresh();
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

TSharedRef<SWidget> SCharacterManager_Modify::BuildParameter(const int32& i, TSharedPtr<FUpgradeParameter> Item)
{
	if (0 != (i & (EModiferSlot::Modifed | EModiferSlot::Accept | EModiferSlot::Cancel)))
	{
		// Content if improved
		return SNew(SHorizontalBox)
		+ SHorizontalBox::Slot().AutoWidth().Padding(FMargin(6))
		[
			SNew(SScaleBox)
			.Stretch(EStretch::ScaleToFit)
			[
				SNew(SImage).ColorAndOpacity(FColor::Transparent)
			]
		]
		+ SHorizontalBox::Slot().FillWidth(1).Padding(FMargin(6, 2)).VAlign(VAlign_Center)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
			[
				SNew(STextBlock).TextStyle(&Style->Modify.ModiferValue).Text(FText::AsPercent(Item->Value / 100.0f))
			]
			+ SVerticalBox::Slot()
			[
				SNew(STextBlock).TextStyle(&Style->Modify.ModiferName).Text(Item->Name)
			]
		]
		+ SHorizontalBox::Slot().AutoWidth().Padding(FMargin(6))
		[
			SNew(SButton)
			.IsEnabled(i == EModiferSlot::Accept)
			.ToolTip(i != EModiferSlot::Accept ? SNew(SLokaToolTip).Text(NSLOCTEXT("InDev", "InDevelopment", "In Development, coming soon.")) : TSharedPtr<IToolTip>())
			.ButtonStyle(&Style->Modify.ModiferButtonChange)
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Center)
			.ContentPadding(6)
			.TextStyle(&Style->Modify.ButtonBackFont)
			.Text(FText::FromString((i == EModiferSlot::Accept) ? sAccept : sRefresh))
			.OnClicked_Lambda([&, n = i, id = Item->Index]()
			{ 
				if (n == EModiferSlot::Accept)
				{
					if (OnConfirmUpdate.IsBound())
					{
						//TODO: SeNTIke
						//Operation::ConfirmItemUpgrade::Request Req;
						//Req.CategoryId = Instance->GetDescription().CategoryType;
						//Req.ModificationId = id;
						//Req.ItemId = Instance->GetEntityId().ToString();
						//OnConfirmUpdate.Execute(Req);
					}
				}
				else
				{
					// Request to server
				}

				return FReply::Handled();
			})
		];

		//ASSIGN_UTF8_SYMBOL(sAccept, 0xf00c);
		//ASSIGN_UTF8_SYMBOL(sCancel, 0xf05e);
		//ASSIGN_UTF8_SYMBOL(sRefresh, 0xf021);
	}
	else if (i == EModiferSlot::Ready)
	{
		// Content if ready to improved
		return SNew(SHorizontalBox)
		+ SHorizontalBox::Slot().AutoWidth().Padding(FMargin(6))
		[
			SNew(SScaleBox)
			.Stretch(EStretch::ScaleToFit)
			[
				SNew(SImage).ColorAndOpacity(FColor::Transparent)
			]
		]
		+ SHorizontalBox::Slot().FillWidth(1).Padding(FMargin(6, 2))
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().HAlign(HAlign_Right).VAlign(VAlign_Center).Padding(FMargin(8, 0))
				[
					SNew(STextBlock).Text(NSLOCTEXT("SCharacterManager_Modify", "Modifer.Chance50", "Chance of luck 50%")).TextStyle(&Style->Modify.LuckText)
				]
				+ SHorizontalBox::Slot().VAlign(VAlign_Center)
				[
					SNew(SButton)
					.ButtonStyle(&Style->Modify.ModiferButtonMoney)
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					.ContentPadding(4)
					[
						SNew(SResourceTextBox)
						.CustomSize(FVector(16, 16, 10))
						.Type(EResourceTextBoxType::Money)
						.Value(5000)
					]
					.OnClicked_Lambda([&]() 
					{ 
						//TODO: SeNTIke
						//if (OnGetUpdateList.IsBound())
						//{
						//	Operation::ItemUpgrade::Request Req;
						//	Req.CategoryId = Instance->GetDescription().CategoryType;
						//	Req.IsDonate = false;
						//	Req.ItemId = Instance->GetEntityId().ToString();
						//	OnGetUpdateList.Execute(Req);
						//}

						return FReply::Handled();
					})
				]
			]
			+ SVerticalBox::Slot()
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().HAlign(HAlign_Right).VAlign(VAlign_Center).Padding(FMargin(8, 0))
				[
					SNew(STextBlock).Text(NSLOCTEXT("SCharacterManager_Modify", "Modifer.Chance100", "Chance of luck 100%")).TextStyle(&Style->Modify.LuckText)
				]
				+ SHorizontalBox::Slot().VAlign(VAlign_Center)
				[
					SNew(SButton)
					.ButtonStyle(&Style->Modify.ModiferButtonDonate)
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					.ContentPadding(4)
					[
						SNew(SResourceTextBox)
						.CustomSize(FVector(16, 16, 10))
						.Type(EResourceTextBoxType::Donate)
						.Value(5)
					]
					.OnClicked_Lambda([&]()
					{
						//TODO: SeNTIke
						//if (OnGetUpdateList.IsBound())
						//{
						//	Operation::ItemUpgrade::Request Req;
						//	Req.CategoryId = Instance->GetDescription().CategoryType;
						//	Req.IsDonate = true;
						//	Req.ItemId = Instance->GetEntityId().ToString();
						//	OnGetUpdateList.Execute(Req);
						//}

						return FReply::Handled();
					})
				]
			]
		];
	}
	else // EModiferSlot::Locked
	{
		// Content if locked
		return SNew(SHorizontalBox)
		+ SHorizontalBox::Slot().AutoWidth().Padding(FMargin(6)).HAlign(HAlign_Center).VAlign(VAlign_Center)
		[
			SNew(STextBlock)
			.Text(FText::FromString(sLocked))
			.TextStyle(&Style->Modify.LockedSymbol)
		]
		+ SHorizontalBox::Slot().FillWidth(1).Padding(FMargin(6, 2))
		.HAlign(HAlign_Center)
		.VAlign(VAlign_Center)
		[
			SNew(STextBlock)
			.Text(NSLOCTEXT("SCharacterManager_Modify", "Modifer.Locked", "LOCKED"))
			.TextStyle(&Style->Modify.LockedText)
		];
	}
}

void SCharacterManager_Modify::SetInstance(const UItemBaseEntity* InInstance)
{
	Instance = InInstance;
	Widget_ItemProperty->BuildWidget(Instance);

	auto Modifers = Instance->GetDisplayModifers();

	CurrentParameters.Empty();

	bool IsFoundEmpty = false;
	for (SIZE_T i = 0; i < 5; ++i)
	{
		if (IsFoundEmpty)
		{
			CurrentParameters.Add(MakeShareable(new FUpgradeParameter(EModiferSlot::Locked)));
		}
		else
		{
			if (i == Modifers.Num())
			{
				IsFoundEmpty = true;
				CurrentParameters.Add(MakeShareable(new FUpgradeParameter(EModiferSlot::Ready)));
			}
			else
			{
				CurrentParameters.Add(MakeShareable(new FUpgradeParameter(EModiferSlot::Modifed, Modifers[i].Name, Modifers[i].Value)));
			}
		}
	}

	CurrentParameters_List->RequestListRefresh();

	TextureParameters.Empty();

	for (auto m : Instance->SkinList)
	{
		TextureParameters.Add(m);
	}

	TextureParameters_List->RequestListRefresh();
	TextureParameters_List->SetSelection(Instance->GetCurrentItemMaterial());
}

void SCharacterManager_Modify::SetInstanceRepository(const FInstanceRepository* InInstance)
{
	InstanceRepository = InInstance;

	CurrentParameters_List->RequestListRefresh();
	TextureParameters_List->RequestListRefresh();
}

void SCharacterManager_Modify::OnUpdateList(const TArray<FItemModification>& Mods)
{
	UpgradeParameters.Empty();

	for (auto &m : Mods)
	{		
		UpgradeParameters.Add(MakeShareable(new FUpgradeParameter(m.Type, FText::FromString(GetEnumValueToString("EWeaponModificationTypeId", static_cast<EWeaponModificationTypeId>(m.Type))), m.value)));
	}

	UpgradeParameters_List->RequestListRefresh();
	UpgradeBorder->ToggleWidget(true);
}

void SCharacterManager_Modify::OnConfirmedUpdate()
{
	UpgradeBorder->ToggleWidget(false);
}

FText SCharacterManager_Modify::GetItemName() const
{
	if (Instance)
	{
		return Instance->GetDescription().Name;
	}

	return FText::GetEmpty();
}

const FSlateBrush* SCharacterManager_Modify::GetItemImage() const
{
	if (Instance)
	{
		return Instance->GetImage();
	}

	return new FSlateNoResource();
}