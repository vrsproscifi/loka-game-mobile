// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/CharacterManagerWidgetStyle.h"

class UItemBaseEntity;
typedef TBaseDelegate<void, const UItemBaseEntity*> FOnItemAction;

/**
 * 
 */
class LOKAGAME_API SCharacterManager_Slot : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SCharacterManager_Slot)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FCharacterManagerStyle>("SCharacterManagerStyle"))
	{}
	SLATE_ATTRIBUTE(FText, Description)
	SLATE_STYLE_ARGUMENT(FCharacterManagerStyle, Style)
	SLATE_ARGUMENT(uint8, AllowContent)
	SLATE_EVENT(FOnItemAction, OnItemEquip)
	SLATE_EVENT(FOnItemAction, OnItemUnEquip)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void SetContent(TSharedRef<SWidget>);
	TSharedRef<SWidget> GetContent() const;

protected:

	FOnItemAction OnItemEquip;
	FOnItemAction OnItemUnEquip;

	uint8 AllowContent;

	const FCharacterManagerStyle* Style;

	TSharedPtr<SButton> Button;
	TSharedPtr<SWidget> Content;

	FButtonStyle AllowDropStyle, DisallowDropStyle;

	virtual void OnDragEnter(const FGeometry& MyGeometry, const FDragDropEvent& DragDropEvent) override;
	virtual void OnDragLeave(const FDragDropEvent& DragDropEvent) override;
	virtual FReply OnDrop(const FGeometry& MyGeometry, const FDragDropEvent& DragDropEvent) override;

	bool IsAllowDrop(const uint8 _slot) const;
	void RevertButtonBorder();
};
