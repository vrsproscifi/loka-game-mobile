// VRSPRO

#include "LokaGame.h"
#include "SCharacterManager_Sets.h"
#include "SCharacterManager_Ammo.h"
#include "SlateOptMacros.h"

#include "Layout/SResponsiveGridPanel.h"
#include "Layout/SScaleBox.h"
#include "Components/SCharacteristicParameter.h"
#include "Components/SLokaToolTip.h"
#include "Components/SAdvanceTextBox.h"
#include "SCharacterManager_Slot.h"

#include "Ammo/ItemAmmoEntity.h"
#include "Weapon/ItemWeaponEntity.h"

#define LOCTEXT_NAMESPACE "SCharacterManager"

DECLARE_DELEGATE_TwoParams(FOnItemActionSlot, const UItemBaseEntity*, const EItemSetSlotId::Type&);
DECLARE_DELEGATE_TwoParams(FOnAmmoAmount, const UItemAmmoEntity*, const int32&);

class LOKAGAME_API SCharacterManager_Asset : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SCharacterManager_Asset)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FCharacterManagerStyle>("SCharacterManagerStyle"))
		, _IsAllow(false)
	{}
		SLATE_STYLE_ARGUMENT(FCharacterManagerStyle, Style)
		SLATE_ATTRIBUTE(FText, Name)
		SLATE_ATTRIBUTE(bool, IsAllow)
		SLATE_ATTRIBUTE(const UCharacterBaseEntity*, CharacterInstance)
		SLATE_EVENT(FOnTextChanged, OnNameChanged)
		SLATE_EVENT(FOnItemActionSlot, OnItemEquip)
		SLATE_EVENT(FOnItemActionSlot, OnItemUnEquip)
		SLATE_EVENT(FOnItemAction, OnItemUpgrade)
		SLATE_EVENT(FOnItemAction, OnItemModify)
		SLATE_EVENT(FOnItemAction, OnItemSell)
		SLATE_EVENT(FOnAmmoAmount, OnAmmoAmountChange)
		SLATE_EVENT(FOnClicked, OnUnlock)
	SLATE_END_ARGS()

	BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
	void Construct(const FArguments& InArgs)
	{
		Style = InArgs._Style;

		IsAllow = InArgs._IsAllow;

		OnItemEquip = InArgs._OnItemEquip;
		OnItemUnEquip = InArgs._OnItemUnEquip;

		OnItemUpgrade = InArgs._OnItemUpgrade;
		OnItemModify = InArgs._OnItemModify;
		OnItemSell = InArgs._OnItemSell;

		OnAmmoAmountChange = InArgs._OnAmmoAmountChange;
		CharacterInstance = InArgs._CharacterInstance;

		ChildSlot
		[
			SNew(SWidgetSwitcher)
			.WidgetIndex_Lambda([&]() { return IsAllow.Get(); })
			+ SWidgetSwitcher::Slot()
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Center)
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot().AutoHeight()
				[
					SNew(STextBlock).Text(LOCTEXT("DontAllowPreset", "This preset is locked!")).Justification(ETextJustify::Center)
				]
				+ SVerticalBox::Slot().AutoHeight()
				[
					SNew(SButton)
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					.Text(LOCTEXT("DontAllowPresetButton", "Unlock preset"))
					.OnClicked(InArgs._OnUnlock)
				]
			]
			+ SWidgetSwitcher::Slot()
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot().AutoHeight()
				[
					SNew(SBorder)
					.BorderImage(&Style->Assets.Header)
					.Padding(FMargin(30, 6))
					[
						//SNew(SHorizontalBox)
						//+ SHorizontalBox::Slot()
						//[
							SAssignNew(Input_Widget, SAdvanceTextBox)
							.Style(&Style->Assets.Input)
							.InitialText(InArgs._Name)
							.OnAcceptText(InArgs._OnNameChanged)
						//]
						//+ SHorizontalBox::Slot().AutoWidth().Padding(FMargin(8, 0, 0, 0))
						//[
						//	SNew(SCheckBox)
						//	.Style(&FLokaSlateStyle::Get().GetWidgetStyle<FCheckBoxStyle>("Default_CheckBox"))
						//	.ToolTip(SNew(SLokaToolTip).Text(LOCTEXT("EnableSwitchDesc", "Toggle on/off this profile.")))
						//	.IsChecked_Lambda([&]() { return IsEnable.Get() ? ECheckBoxState::Checked : ECheckBoxState::Unchecked; })
						//]
					]
				]
				+ SVerticalBox::Slot().AutoHeight()
				[
					SNew(SVerticalBox)
					+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(0, 4))
					[
						SNew(SSeparator).Orientation(EOrientation::Orient_Horizontal)
					]
					+ SVerticalBox::Slot().AutoHeight()
					[
						SNew(SBox).HeightOverride(96)
						[
							SNew(SHorizontalBox)
							+ SHorizontalBox::Slot()
							[
								SAssignNew(Slot_Widgets[EItemSetSlotId::PrimaryWeapon], SCharacterManager_Slot).Description(LOCTEXT("Slot.PrimaryWeapon", "Primary Weapon")).AllowContent(EItemSlotTypeId::PrimaryWeapon)
								.OnItemEquip(this, &SCharacterManager_Asset::OnItemEquipEvent, EItemSetSlotId::PrimaryWeapon)
								.OnItemUnEquip(this, &SCharacterManager_Asset::OnItemUnEquipEvent, EItemSetSlotId::PrimaryWeapon)
							]
							+ SHorizontalBox::Slot()
							[
								SAssignNew(Slot_Widgets[EItemSetSlotId::SecondaryWeapon], SCharacterManager_Slot).Description(LOCTEXT("Slot.SecondaryWeapon", "Secondary Weapon")).AllowContent(EItemSlotTypeId::SecondaryWeapon)
								.OnItemEquip(this, &SCharacterManager_Asset::OnItemEquipEvent, EItemSetSlotId::SecondaryWeapon)
								.OnItemUnEquip(this, &SCharacterManager_Asset::OnItemUnEquipEvent, EItemSetSlotId::SecondaryWeapon)
							]
						]
					]
					+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Center).Padding(4)
					[
						SNew(SCheckBox) // Ammo
						.IsChecked(this, &SCharacterManager_Asset::GetIsExpanded_CheckBox)
						.OnCheckStateChanged(this, &SCharacterManager_Asset::OnClicked_CheckBox)
						.Style(&Style->Asset.CheckBox)
						.Type(ESlateCheckBoxType::ToggleButton)
						.HAlign(HAlign_Center)
						.Padding(FMargin(8, 2))
						[
							SNew(STextBlock)
							.TextStyle(&Style->Asset.CheckBox_Text)
							.Text(LOCTEXT("AmmoSelector", "Ammo for weapons"))
						]
					]
					+ SVerticalBox::Slot().AutoHeight()
					[
						SNew(SSeparator).Orientation(EOrientation::Orient_Horizontal)
					]
				]
				+ SVerticalBox::Slot().FillHeight(1)
				[
					SNew(SOverlay)
					+ SOverlay::Slot()
					[
						SNew(SFxWidget)
						.ColorAndOpacity(this, &SCharacterManager_Asset::GetIsExpanded_Back)
						[
							SNew(SResponsiveGridPanel, 4)
							.ColumnGutter(2)
							.RowGutter(4)
							+ SResponsiveGridPanel::Slot(1).ColumnSpan(SResponsiveGridSize::Mobile, 1, 1)
							[
								SAssignNew(Slot_Widgets[EItemSetSlotId::Mask], SCharacterManager_Slot).Description(LOCTEXT("Slot.Mask", "Mask")).AllowContent(EItemSlotTypeId::Mask)
								.OnItemEquip(this, &SCharacterManager_Asset::OnItemEquipEvent, EItemSetSlotId::Mask)
								.OnItemUnEquip(this, &SCharacterManager_Asset::OnItemUnEquipEvent, EItemSetSlotId::Mask)
							]
							+ SResponsiveGridPanel::Slot(1).ColumnSpan(SResponsiveGridSize::Mobile, 1)
							[
								SAssignNew(Slot_Widgets[EItemSetSlotId::Head], SCharacterManager_Slot).Description(LOCTEXT("Slot.Helmet", "Helmet")).AllowContent(EItemSlotTypeId::Helmet)
								.OnItemEquip(this, &SCharacterManager_Asset::OnItemEquipEvent, EItemSetSlotId::Head)
								.OnItemUnEquip(this, &SCharacterManager_Asset::OnItemUnEquipEvent, EItemSetSlotId::Head)
							]
							+ SResponsiveGridPanel::Slot(2).ColumnSpan(SResponsiveGridSize::Mobile, 1)
							[
								SAssignNew(Slot_Widgets[EItemSetSlotId::Gloves], SCharacterManager_Slot).Description(LOCTEXT("Slot.Gloves", "Gloves")).AllowContent(EItemSlotTypeId::Gloves)
								.OnItemEquip(this, &SCharacterManager_Asset::OnItemEquipEvent, EItemSetSlotId::Gloves)
								.OnItemUnEquip(this, &SCharacterManager_Asset::OnItemUnEquipEvent, EItemSetSlotId::Gloves)
							]
							+ SResponsiveGridPanel::Slot(2).ColumnSpan(SResponsiveGridSize::Mobile, 2)
							[
								SAssignNew(Slot_Widgets[EItemSetSlotId::Body], SCharacterManager_Slot).Description(LOCTEXT("Slot.Body", "Body")).AllowContent(EItemSlotTypeId::Body)
								.OnItemEquip(this, &SCharacterManager_Asset::OnItemEquipEvent, EItemSetSlotId::Body)
								.OnItemUnEquip(this, &SCharacterManager_Asset::OnItemUnEquipEvent, EItemSetSlotId::Body)
							]
							+ SResponsiveGridPanel::Slot(2).ColumnSpan(SResponsiveGridSize::Mobile, 1)
							[
								SAssignNew(Slot_Widgets[EItemSetSlotId::Backpack], SCharacterManager_Slot).Description(LOCTEXT("Slot.Backpack", "Backpack")).AllowContent(EItemSlotTypeId::Backpack)
								.OnItemEquip(this, &SCharacterManager_Asset::OnItemEquipEvent, EItemSetSlotId::Backpack)
								.OnItemUnEquip(this, &SCharacterManager_Asset::OnItemUnEquipEvent, EItemSetSlotId::Backpack)
							]
							+ SResponsiveGridPanel::Slot(3).ColumnSpan(SResponsiveGridSize::Mobile, 1)
							[
								SAssignNew(Slot_Widgets[EItemSetSlotId::Explosive_1], SCharacterManager_Slot).Description(LOCTEXT("Slot.Explosive", "Explosive")).AllowContent(EItemSlotTypeId::Grenade)
								.OnItemEquip(this, &SCharacterManager_Asset::OnItemEquipEvent, EItemSetSlotId::Explosive_1)
								.OnItemUnEquip(this, &SCharacterManager_Asset::OnItemUnEquipEvent, EItemSetSlotId::Explosive_1)
							]
							+ SResponsiveGridPanel::Slot(3).ColumnSpan(SResponsiveGridSize::Mobile, 1)
							[
								SAssignNew(Slot_Widgets[EItemSetSlotId::Pants], SCharacterManager_Slot).Description(LOCTEXT("Slot.Pants", "Pants")).AllowContent(EItemSlotTypeId::Pants)
								.OnItemEquip(this, &SCharacterManager_Asset::OnItemEquipEvent, EItemSetSlotId::Pants)
								.OnItemUnEquip(this, &SCharacterManager_Asset::OnItemUnEquipEvent, EItemSetSlotId::Pants)
							]
							+ SResponsiveGridPanel::Slot(3).ColumnSpan(SResponsiveGridSize::Mobile, 1)
							[
								SAssignNew(Slot_Widgets[EItemSetSlotId::Bots], SCharacterManager_Slot).Description(LOCTEXT("Slot.Boots", "Boots")).AllowContent(EItemSlotTypeId::Boots)
								.OnItemEquip(this, &SCharacterManager_Asset::OnItemEquipEvent, EItemSetSlotId::Bots)
								.OnItemUnEquip(this, &SCharacterManager_Asset::OnItemUnEquipEvent, EItemSetSlotId::Bots)
							]
							+ SResponsiveGridPanel::Slot(3).ColumnSpan(SResponsiveGridSize::Mobile, 1)
							[
								SAssignNew(Slot_Widgets[EItemSetSlotId::Other_3], SCharacterManager_Slot).Description(LOCTEXT("Slot.Other", "Other")).AllowContent(EItemSlotTypeId::Kit)
								.OnItemEquip(this, &SCharacterManager_Asset::OnItemEquipEvent, EItemSetSlotId::Other_3)
								.OnItemUnEquip(this, &SCharacterManager_Asset::OnItemUnEquipEvent, EItemSetSlotId::Other_3)
							]					
							+ SResponsiveGridPanel::Slot(4).ColumnSpan(SResponsiveGridSize::Mobile, 1)
							[
								SAssignNew(Slot_Widgets[EItemSetSlotId::Explosive_2], SCharacterManager_Slot).Description(LOCTEXT("Slot.Explosive", "Explosive")).AllowContent(EItemSlotTypeId::Grenade)
								.OnItemEquip(this, &SCharacterManager_Asset::OnItemEquipEvent, EItemSetSlotId::Explosive_2)
								.OnItemUnEquip(this, &SCharacterManager_Asset::OnItemUnEquipEvent, EItemSetSlotId::Explosive_2)
							]
							+ SResponsiveGridPanel::Slot(4).ColumnSpan(SResponsiveGridSize::Mobile, 1)
							[
								SAssignNew(Slot_Widgets[EItemSetSlotId::Explosive_3], SCharacterManager_Slot).Description(LOCTEXT("Slot.Explosive", "Explosive")).AllowContent(EItemSlotTypeId::Grenade)
								.OnItemEquip(this, &SCharacterManager_Asset::OnItemEquipEvent, EItemSetSlotId::Explosive_3)
								.OnItemUnEquip(this, &SCharacterManager_Asset::OnItemUnEquipEvent, EItemSetSlotId::Explosive_3)
							]					
							+ SResponsiveGridPanel::Slot(4).ColumnSpan(SResponsiveGridSize::Mobile, 1)
							[
								SAssignNew(Slot_Widgets[EItemSetSlotId::Other_1], SCharacterManager_Slot).Description(LOCTEXT("Slot.Other", "Other")).AllowContent(EItemSlotTypeId::Kit)
								.OnItemEquip(this, &SCharacterManager_Asset::OnItemEquipEvent, EItemSetSlotId::Other_1)
								.OnItemUnEquip(this, &SCharacterManager_Asset::OnItemUnEquipEvent, EItemSetSlotId::Other_1)
							]
							+ SResponsiveGridPanel::Slot(4).ColumnSpan(SResponsiveGridSize::Mobile, 1)
							[
								SAssignNew(Slot_Widgets[EItemSetSlotId::Other_2], SCharacterManager_Slot).Description(LOCTEXT("Slot.Other", "Other")).AllowContent(EItemSlotTypeId::Kit)
								.OnItemEquip(this, &SCharacterManager_Asset::OnItemEquipEvent, EItemSetSlotId::Other_2)
								.OnItemUnEquip(this, &SCharacterManager_Asset::OnItemUnEquipEvent, EItemSetSlotId::Other_2)
							]
						]
					]
					+ SOverlay::Slot()
					[
						SNew(SBorder)
						.BorderImage(new FSlateColorBrush(FColor::Transparent))
						.Visibility(this, &SCharacterManager_Asset::GetIsExpanded_Border)
					]
					+ SOverlay::Slot().VAlign(VAlign_Top)
					[
						SNew(SFxWidget)
						.IgnoreClipping(false)
						.VisualOffset(this, &SCharacterManager_Asset::GetContentSizeScale)
						.Visibility(this, &SCharacterManager_Asset::GetIsExpanded_Border)
						[
							SNew(SBox).HeightOverride(128)
							[
								SNew(SOverlay)
								+ SOverlay::Slot()
								[
									SNew(SGridPanel)
									.FillColumn(0, 1)
									.FillColumn(1, 1)
									.FillRow(0, 1)
									.FillRow(1, 1)
									+ SGridPanel::Slot(0, 0)[SAssignNew(SlotAmmo_Widgets[0], SCharacterManager_Ammo).Limit(10)]
									+ SGridPanel::Slot(0, 1)[SAssignNew(SlotAmmo_Widgets[1], SCharacterManager_Ammo).Limit(10)]
									+ SGridPanel::Slot(1, 0)[SAssignNew(SlotAmmo_Widgets[2], SCharacterManager_Ammo).Limit(10)]
									+ SGridPanel::Slot(1, 1)[SAssignNew(SlotAmmo_Widgets[3], SCharacterManager_Ammo).Limit(10)]
								]
								+ SOverlay::Slot()
								[
									SNew(SBorder)
									.BorderImage(new FSlateColorBrush(FColor::Black.WithAlpha(196)))
									.HAlign(HAlign_Center)
									.VAlign(VAlign_Center)
									[
										SNew(STextBlock)
										.TextStyle(&FLokaSlateStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_TextBlock_InDev"))
										.Text(NSLOCTEXT("All", "All.InDev", "In Development, coming soon."))
									]
								]
							]
						]
					]
				]
				+ SVerticalBox::Slot().AutoHeight()
				[
					SNew(SSeparator).Orientation(EOrientation::Orient_Horizontal)
				]
				+ SVerticalBox::Slot()
				.AutoHeight()
				.HAlign(HAlign_Center)
				.VAlign(VAlign_Center)
				.Padding(FMargin(10, 8))
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot().Padding(FMargin(10))
					[
						SNew(SCharacteristicParameter)
						.Icon(&Style->Asset.IconHealth)
						.TextStyleName(&Style->Asset.ParameterName)
						.TextStyleValue(&Style->Asset.ParameterValue)
						.ToolTip(SNew(SLokaToolTip).Text(LOCTEXT("Health", "Health")))
					//TODO: SeNTIke
						//.Value_Lambda([&]() { 
						//
						//	int32 _Value = 0;
						//	if (CharacterInstance.Get() != nullptr && !CharacterInstance.Get()->IsLocked())
						//	{								
						//		for (SIZE_T i = 0; i < EItemSetSlotId::End; ++i)
						//		{
						//			if (Slot_Widgets[i].IsValid() && Slot_Widgets[i]->GetContent() != SNullWidget::NullWidget)
						//			{
						//				auto TargetWidget = StaticCastSharedRef<SCharacterManager_Item>(Slot_Widgets[i]->GetContent());
						//				if (TargetWidget != SNullWidget::NullWidget && TargetWidget->GetInstance())
						//				{
						//					_Value += TargetWidget->GetInstance()->Property.Health.MaxAmount;
						//				}
						//
						//			}
						//		}
						//	}
						//
						//	return (CharacterInstance.Get() != nullptr) ? CharacterInstance.Get()->Property.Health.MaxAmount + _Value : 0;
						//})
					]
					+ SHorizontalBox::Slot().Padding(FMargin(10))
					[
						SNew(SCharacteristicParameter)
						.Icon(&Style->Asset.IconArmour)
						.TextStyleName(&Style->Asset.ParameterName)
						.TextStyleValue(&Style->Asset.ParameterValue)
						.ToolTip(SNew(SLokaToolTip).Text(LOCTEXT("Armour", "Armour")))
						//TODO: SeNTIke
						//.Value_Lambda([&]() { 
						//
						//	int32 _Value = 0;
						//	if (CharacterInstance.Get() != nullptr && !CharacterInstance.Get()->IsLocked())
						//	{								
						//		for (SIZE_T i = 0; i < EItemSetSlotId::End; ++i)
						//		{
						//			if (Slot_Widgets[i].IsValid() && Slot_Widgets[i]->GetContent() != SNullWidget::NullWidget)
						//			{
						//				auto TargetWidget = StaticCastSharedRef<SCharacterManager_Item>(Slot_Widgets[i]->GetContent());
						//				if (TargetWidget != SNullWidget::NullWidget && TargetWidget->GetInstance())
						//				{
						//					_Value += TargetWidget->GetInstance()->Property.Armour.MaxAmount;
						//				}
						//
						//			}
						//		}
						//	}
						//
						//	return (CharacterInstance.Get() != nullptr) ? CharacterInstance.Get()->Property.Armour.MaxAmount + _Value : 0;
						//})
					]
					+ SHorizontalBox::Slot().Padding(FMargin(10))
					[
						SNew(SCharacteristicParameter)
						.Icon(&Style->Asset.IconSpeed)
						.TextStyleName(&Style->Asset.ParameterName)
						.TextStyleValue(&Style->Asset.ParameterValue)
						.ToolTip(SNew(SLokaToolTip).Text(LOCTEXT("Speed", "Speed")))
						//TODO: SeNTIke
						//.Value_Lambda([&]() { 
						//
						//	int32 _Value = 0;
						//	if (CharacterInstance.Get() != nullptr && !CharacterInstance.Get()->IsLocked())
						//	{								
						//		for (SIZE_T i = 0; i < EItemSetSlotId::End; ++i)
						//		{
						//			if (Slot_Widgets[i].IsValid() && Slot_Widgets[i]->GetContent() != SNullWidget::NullWidget)
						//			{
						//				auto TargetWidget = StaticCastSharedRef<SCharacterManager_Item>(Slot_Widgets[i]->GetContent());
						//				if (TargetWidget != SNullWidget::NullWidget && TargetWidget->GetInstance())
						//				{
						//					_Value += TargetWidget->GetInstance()->Property.Speed.MaxAmount;
						//				}
						//
						//			}
						//		}
						//	}
						//
						//	return (CharacterInstance.Get() != nullptr) ? CharacterInstance.Get()->Property.Speed.MaxAmount + _Value : 0;
						//})
					]
				]
			]
		];

		AnimInstance = FCurveSequence();
		AnimHandle = AnimInstance.AddCurve(.0f, 0.2f, ECurveEaseFunction::QuadIn);

	}
	END_SLATE_FUNCTION_BUILD_OPTIMIZATION
		
	void SetIsAvaliable(const TAttribute<bool>& InIsAllow)
	{
		IsAllow = InIsAllow;
	}

	void SetIsEnable(const TAttribute<bool>& InIsAllow)
	{
		IsEnable = InIsAllow;
	}

	FORCEINLINE void SetExpandedAmmo(const bool Toggle)
	{
		if (Toggle && !AnimInstance.IsAtEnd())
			AnimInstance.Play(AsShared());
		else if(!AnimInstance.IsAtStart())
			AnimInstance.PlayReverse(AsShared());		
	}

	void InstallAsset(const UItemBaseEntity* Item)
	{
		if (Item)
		{
			InstallAsset(Item, static_cast<EItemSetSlotId::Type>(Item->GetDescription().SlotType.GetValue()));
		}
	}

	void InstallAsset(const UItemBaseEntity* Item, const EItemSetSlotId::Type Slot)
	{
		if (Item)
		{
			ECategoryTypeId Category = Item->GetDescription().CategoryType;

			auto Widget_New = SNew(SCharacterManager_Item, const_cast<UItemBaseEntity*>(Item))
				.State(SCharacterManager_Item::EItemState::InSlot)
				.OnClickedModify(OnItemModify)
				.OnClickedSell(OnItemSell);

			if (Category == ECategoryTypeId::Weapon || Category == ECategoryTypeId::Armour || (Category == ECategoryTypeId::Grenade && Slot < EItemSetSlotId::End && Slot_Widgets[Slot].IsValid()))
			{
				Slot_Widgets[Slot]->SetContent(Widget_New);

				if (Slot == EItemSetSlotId::PrimaryWeapon || Slot == EItemSetSlotId::SecondaryWeapon)
				{
					auto WeaponItem = Cast<UItemWeaponEntity>(Item);
					if (WeaponItem)
					{
						SlotAmmo_Widgets[(Slot == EItemSetSlotId::PrimaryWeapon) ? 0 : 2]->SetInstance(WeaponItem->GetPrimaryAmmo());
						SlotAmmo_Widgets[(Slot == EItemSetSlotId::PrimaryWeapon) ? 0 : 2]->SetDelegate(FOnInt32ValueChanged::CreateSP(this, &SCharacterManager_Asset::OnAmmoAmountChangeEvent, WeaponItem->GetPrimaryAmmo()));
						SlotAmmo_Widgets[(Slot == EItemSetSlotId::PrimaryWeapon) ? 1 : 3]->SetInstance(WeaponItem->GetSecondaryAmmo());
						SlotAmmo_Widgets[(Slot == EItemSetSlotId::PrimaryWeapon) ? 1 : 3]->SetDelegate(FOnInt32ValueChanged::CreateSP(this, &SCharacterManager_Asset::OnAmmoAmountChangeEvent, WeaponItem->GetSecondaryAmmo()));
					}
				}
			}
		}
	}

#pragma optimize("", off)
	void UninstallSlot(const EItemSetSlotId::Type& slotId)
	{
		if (slotId >= EItemSetSlotId::PrimaryWeapon && slotId < EItemSetSlotId::End && Slot_Widgets[slotId].IsValid())
		{
			Slot_Widgets[slotId]->SetContent(SNullWidget::NullWidget);
		}
	}

	void UninstallAsset(const UItemBaseEntity* Item)
	{
		for (SSIZE_T i = 0; i < Slot_Widgets.Num(); ++i)
		{
			auto widget = Slot_Widgets[i];
			if (widget.IsValid() && widget != SNullWidget::NullWidget)
			{
				auto SlotContent = widget->GetContent();
				auto s = StaticCastSharedRef<SCharacterManager_Item>(SlotContent);
				if (s->GetInstance() == Item)
				{
					Slot_Widgets[i]->SetContent(SNullWidget::NullWidget);

					EItemSlotTypeId Slot = Item->GetDescription().SlotType;
					if (Slot == EItemSetSlotId::PrimaryWeapon || Slot == EItemSetSlotId::SecondaryWeapon)
					{
						auto WeaponItem = Cast<UItemWeaponEntity>(Item);
						if (WeaponItem)
						{
							SlotAmmo_Widgets[(Slot == EItemSetSlotId::PrimaryWeapon) ? 0 : 2]->SetInstance(nullptr);
							SlotAmmo_Widgets[(Slot == EItemSetSlotId::PrimaryWeapon) ? 1 : 3]->SetInstance(nullptr);
						}
					}
					break;
				}
			}
		}
	}

	void SetAmmoAmount(const UItemAmmoEntity* Target, const EItemSlotTypeId& TargetWeapon, const int32& Amount)
	{
		if (SlotAmmo_Widgets[(TargetWeapon == EItemSetSlotId::PrimaryWeapon) ? 0 : 2]->GetInstance() == Target)
		{
			SlotAmmo_Widgets[(TargetWeapon == EItemSetSlotId::PrimaryWeapon) ? 0 : 2]->SetAmount(Amount);
		}
		else if (SlotAmmo_Widgets[(TargetWeapon == EItemSetSlotId::PrimaryWeapon) ? 1 : 3]->GetInstance() == Target)
		{
			SlotAmmo_Widgets[(TargetWeapon == EItemSetSlotId::PrimaryWeapon) ? 1 : 3]->SetAmount(Amount);
		}
	}

	FORCEINLINE void SetName(const FText& Name)
	{
		Input_Widget->SetText(Name);
	}

	FORCEINLINE FText GetName() const
	{
		return Input_Widget->GetText();
	}

protected:

	const FCharacterManagerStyle* Style;
	TAttribute<const UCharacterBaseEntity*> CharacterInstance;

	FCurveSequence AnimInstance;
	FCurveHandle AnimHandle;

	TStaticArray<TSharedPtr<SCharacterManager_Ammo>, 4> SlotAmmo_Widgets;
	TStaticArray<TSharedPtr<SCharacterManager_Slot>, EItemSetSlotId::End> Slot_Widgets;
	TSharedPtr<SAdvanceTextBox> Input_Widget;

	TAttribute<bool> IsAllow, IsEnable;

	FOnItemActionSlot OnItemEquip;
	FOnItemActionSlot OnItemUnEquip;

	FOnItemAction OnItemUpgrade;
	FOnItemAction OnItemModify;
	FOnItemAction OnItemSell;

	FOnAmmoAmount OnAmmoAmountChange;

	FVector2D GetContentSizeScale() const
	{
		return FVector2D(0.0f, FMath::Lerp(-1.0f, 0.0f, AnimHandle.GetLerp()));
	}

	ECheckBoxState GetIsExpanded_CheckBox() const
	{
		return (AnimHandle.GetLerp() > .0f) ? ECheckBoxState::Checked : ECheckBoxState::Unchecked;
	}

	EVisibility GetIsExpanded_Border() const
	{
		return (AnimHandle.GetLerp() > .0f) ? EVisibility::Visible : EVisibility::Collapsed;
	}

	FLinearColor GetIsExpanded_Back() const
	{
		return FLinearColor(1, 1, 1, FMath::Lerp<float>(1, 0, AnimHandle.GetLerp()));
	}

	void OnClicked_CheckBox(ECheckBoxState State)
	{
		SetExpandedAmmo(State == ECheckBoxState::Checked);
	}

	void OnItemEquipEvent(const UItemBaseEntity* Item, const EItemSetSlotId::Type Slot)
	{
		OnItemEquip.ExecuteIfBound(Item, Slot);
	}

	void OnItemUnEquipEvent(const UItemBaseEntity* Item, const EItemSetSlotId::Type Slot)
	{
		OnItemUnEquip.ExecuteIfBound(Item, Slot);
	}

	void OnAmmoAmountChangeEvent(int32 Amount, UItemAmmoEntity* Item) const
	{
		OnAmmoAmountChange.ExecuteIfBound(Item, Amount);
	}
};


BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SCharacterManager_Sets::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	OnChangeSet = InArgs._OnChangeSet;
	OnChangeSetName = InArgs._OnChangeSetName;

	OnItemEquip = InArgs._OnItemEquip;
	OnItemUnEquip = InArgs._OnItemUnEquip;

	OnAmmoAmountChange = InArgs._OnAmmoAmountChange;
	CharacterInstance = InArgs._CharacterInstance;

	TSharedRef<SHorizontalBox> HorizontalBox_Buttons = SNew(SHorizontalBox);
	TSharedRef<SHorizontalBox> HorizontalBox_Content = SNew(SHorizontalBox);

	TSharedRef<SVerticalBox> VerticalBox =
		SNew(SVerticalBox)
		+ SVerticalBox::Slot().AutoHeight()
		[
			HorizontalBox_Buttons
		]
		+ SVerticalBox::Slot().FillHeight(1)
		[
			HorizontalBox_Content
		];

	ChildSlot
	[
		SNew(SOverlay)
		+ SOverlay::Slot()
		[
			VerticalBox
		]
		+ SOverlay::Slot().HAlign(HAlign_Center).VAlign(VAlign_Top)
		[
			SNew(STextBlock)
			.RenderTransformPivot(FVector2D(.5f, 1.f))
			.RenderTransform(FSlateRenderTransform(FVector2D(0, -60)))
			.TextStyle(&Style->Headers)
			.Text(NSLOCTEXT("SCharacterManager", "Headers.Character", "Character"))
		]
	];

	const FSlateBrush* EmptyIcon = new FSlateNoResource();

	for (int32 i = 0; i < 5; ++i)
	{
		HorizontalBox_Buttons->AddSlot().FillWidth(1)
		[
			SNew(SCheckBox)
			.Type(ESlateCheckBoxType::ToggleButton)
			.OnCheckStateChanged(this, &SCharacterManager_Sets::ChangeCurrentSet, static_cast<EPlayerPreSetId::Type>(i))
			.IsChecked(this, &SCharacterManager_Sets::GetSetIsSelected, static_cast<EPlayerPreSetId::Type>(i))
			.Style(&Style->Assets.Button)
			.HAlign(HAlign_Center)
			[
				SNew(SScaleBox)
				.Stretch(EStretch::ScaleToFit)
				[
					SNew(SImage)
					.Image_Lambda([&, ei = EmptyIcon, n = i]() {
						if (ProfileIcon[n].IsSet() && ProfileIcon[n].Get() != nullptr)
						{
							return ProfileIcon[n].Get();
						}

						return ei;
					})
				]

				//SNew(STextBlock)
				//.Text(FText::Format(NSLOCTEXT("SCharacterManager_Sets", "Sets", "Set {0}"), FText::AsNumber(i+1)))
				//.TextStyle(&Style->Assets.Number)
				//.Justification(ETextJustify::Center)
			]
		];

		HorizontalBox_Content->AddSlot().FillWidth(TAttribute<float>::Create(TAttribute<float>::FGetter::CreateSP(this, &SCharacterManager_Sets::GetAssetWidth, static_cast<EPlayerPreSetId::Type>(i))))
		[
			SNew(SFxWidget)
			.IgnoreClipping(false)
			.RenderScale(this, &SCharacterManager_Sets::GetAssetWidth, static_cast<EPlayerPreSetId::Type>(i))
			[
				SNew(SBorder)
				.BorderImage(&Style->List.Background)
				.Padding(0)
				[
					SNew(SBorder)
					.BorderImage(&Style->List.Border)
					.Padding(2)
					[
						SAssignNew(ProfileContainer[i], SCharacterManager_Asset)
						.Name(FText::Format(FText::FromString("Equipment set {0}"), FText::AsNumber(i + 1)))
						.OnNameChanged(this, &SCharacterManager_Sets::OnChangeSetNameEvent, static_cast<EPlayerPreSetId::Type>(i))
						.OnItemEquip(this, &SCharacterManager_Sets::OnItemEquipEvent, static_cast<EPlayerPreSetId::Type>(i))
						.OnItemUnEquip(this, &SCharacterManager_Sets::OnItemUnEquipEvent, static_cast<EPlayerPreSetId::Type>(i))
						.OnItemModify(InArgs._OnItemModify)
						.OnItemSell(InArgs._OnItemSell)
						.OnAmmoAmountChange(this, &SCharacterManager_Sets::OnAmmoAmountChangeEvent, static_cast<EPlayerPreSetId::Type>(i))
						.OnUnlock_Lambda([&, n = i]() { OnUnlockPreset.ExecuteIfBound(FUnlockItemRequestContainer(n, CategoryTypeId::Profile)); return FReply::Handled(); })
						.CharacterInstance(CharacterInstance)
					]
				]
			]
		];

		AnimInstance[i] = FCurveSequence();
		AnimHandle[i] = AnimInstance[i].AddCurve(.0f, 0.3f, ECurveEaseFunction::QuadInOut);
	}

	CurrentSet = EPlayerPreSetId::Two;
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SCharacterManager_Sets::OnChangeSetNameEvent(const FText& Name, const EPlayerPreSetId::Type Set)
{
	OnChangeSetName.ExecuteIfBound(Set, Name);
}

float SCharacterManager_Sets::GetAssetWidth(const EPlayerPreSetId::Type s) const
{
	return AnimHandle[s].GetLerp();
}

void SCharacterManager_Sets::ChangeCurrentSet(ECheckBoxState, const EPlayerPreSetId::Type Set)
{
	if (Set != CurrentSet)
	{
		AnimInstance[CurrentSet].PlayReverse(this->AsShared());
		CurrentSet = Set;
		AnimInstance[CurrentSet].Play(this->AsShared());
		OnChangeSet.ExecuteIfBound(Set);
	}
}

ECheckBoxState SCharacterManager_Sets::GetSetIsSelected(const EPlayerPreSetId::Type Set) const
{
	return (CurrentSet == Set) ? ECheckBoxState::Checked : ECheckBoxState::Unchecked;
}

void SCharacterManager_Sets::SetCurrentSet(const EPlayerPreSetId::Type& Set)
{
	if (Set >= 0 && Set < 5)
	{
		ChangeCurrentSet(ECheckBoxState::Checked, Set);
	}
}

int32 SCharacterManager_Sets::GetGetCurrentPreSet() const
{
	return CurrentSet;
}

void SCharacterManager_Sets::InstallAsset(const UItemBaseEntity* Item, const EPlayerPreSetId::Type Set)
{
	if (Set >= 0 && Set < EPlayerPreSetId::End) ProfileContainer[Set]->InstallAsset(Item);
}

void SCharacterManager_Sets::InstallAsset(const UItemBaseEntity* Item, const EPlayerPreSetId::Type Set, const EItemSetSlotId::Type Slot)
{
	if (Set >= 0 && Set < EPlayerPreSetId::End) ProfileContainer[Set]->InstallAsset(Item, Slot);
}

void SCharacterManager_Sets::UninstallAsset(const UItemBaseEntity* Item, const EPlayerPreSetId::Type Set)
{
	if (Set >= 0 && Set < EPlayerPreSetId::End) ProfileContainer[Set]->UninstallAsset(Item);
}

void SCharacterManager_Sets::UninstallSlot(const EItemSetSlotId::Type slotId, const EPlayerPreSetId::Type setId)
{
	if (setId >= 0 && setId < EPlayerPreSetId::End) ProfileContainer[setId]->UninstallSlot(slotId);
}

void SCharacterManager_Sets::SetAmmoAmount(const UItemAmmoEntity* Target, const EPlayerPreSetId::Type& PresetId, const EItemSlotTypeId& TargetWeapon, const int32& Amount)
{	
	if (PresetId < EPlayerPreSetId::End) ProfileContainer[PresetId]->SetAmmoAmount(Target, TargetWeapon, Amount);
}

void SCharacterManager_Sets::SetSetName(const EPlayerPreSetId::Type& Set, const FText& Name)
{
	if (Set >= 0 && Set < EPlayerPreSetId::End) ProfileContainer[Set]->SetName(Name);
}

FText SCharacterManager_Sets::GetSetName(const EPlayerPreSetId::Type& Set) const
{
	if (Set >= 0 && Set < EPlayerPreSetId::End) return ProfileContainer[Set]->GetName();
	return FText::GetEmpty();
}

void SCharacterManager_Sets::SetPresetIsAvaliable(const EPlayerPreSetId::Type& PresetId, const TAttribute<bool>& IsAllow)
{
	if (PresetId < EPlayerPreSetId::End) return ProfileContainer[PresetId]->SetIsAvaliable(IsAllow);
}

void SCharacterManager_Sets::SetPresetIsEnable(const EPlayerPreSetId::Type& PresetId, const TAttribute<bool>& IsAllow)
{
	if (PresetId < EPlayerPreSetId::End) return ProfileContainer[PresetId]->SetIsEnable(IsAllow);
}

void SCharacterManager_Sets::OnItemEquipEvent(const UItemBaseEntity* Item, const EItemSetSlotId::Type& Slot, const EPlayerPreSetId::Type Set) const
{
	OnItemEquip.ExecuteIfBound(Item, Slot, Set);
}

void SCharacterManager_Sets::OnItemUnEquipEvent(const UItemBaseEntity* Item, const EItemSetSlotId::Type& Slot, const EPlayerPreSetId::Type Set) const
{
	OnItemUnEquip.ExecuteIfBound(Item, Slot, Set);
}

void SCharacterManager_Sets::OnAmmoAmountChangeEvent(const UItemAmmoEntity* Item, const int32& Amount, const EPlayerPreSetId::Type PresetId) const
{
	OnAmmoAmountChange.ExecuteIfBound(Item, PresetId, Amount);
}

void SCharacterManager_Sets::SetPresetIcon(const EPlayerPreSetId::Type& InProfileId, const TAttribute<const FSlateBrush*>& InAttr)
{
	ProfileIcon[InProfileId] = InAttr;
}

#undef LOCTEXT_NAMESPACE