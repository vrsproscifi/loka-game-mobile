// VRSPRO

#include "LokaGame.h"
#include "SCharacterManager_Ammo.h"
#include "SlateOptMacros.h"

#include "Ammo/ItemAmmoEntity.h"
#include "Layout/SScaleBox.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SCharacterManager_Ammo::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	OnValueChanged = InArgs._OnValueChanged;
	Limit = InArgs._Limit;

	ChildSlot
	[
		SNew(SButton)
		.ButtonStyle(&Style->Asset.Button)
		.ContentPadding(FMargin(8))
		[
			SAssignNew(Widget_HorizontalBox, SHorizontalBox)
			+ SHorizontalBox::Slot().AutoWidth()
			[
				SNew(SBox).WidthOverride(64)
				[
					SNew(SScaleBox)
					.Stretch(EStretch::ScaleToFit)
					[
						SAssignNew(Widget_Image, SImage)
						.Image(new FSlateNoResource())
					]
				]
			]
			+ SHorizontalBox::Slot().FillWidth(1)
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot()
				[
					SAssignNew(Widget_TextBlock, STextBlock)
				]
				+ SVerticalBox::Slot().AutoHeight()
				[
					SAssignNew(Widget_SpinBox, SSpinBox<int32>)
					.Style(&Style->Asset.SpinBox)
					.MinValue(this, &SCharacterManager_Ammo::GetSpinBoxLimits, true)
					.MinSliderValue(this, &SCharacterManager_Ammo::GetSpinBoxLimits, true)
					.MaxValue(this, &SCharacterManager_Ammo::GetSpinBoxLimits, false)
					.MaxSliderValue(this, &SCharacterManager_Ammo::GetSpinBoxLimits, false)
					.OnValueChanged(this, &SCharacterManager_Ammo::OnAmountChanged)
				]
			]
		]
	];

	SetInstance(nullptr);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

TOptional<int32> SCharacterManager_Ammo::GetSpinBoxLimits(const bool IsMin) const
{
	return IsMin ? 0 : Limit.Get(); 
}

void SCharacterManager_Ammo::SetInstance(UItemAmmoEntity* InInstance)
{
	Instance = InInstance;	
	if (Instance != nullptr)
	{
		Widget_Image->SetImage(Instance->GetIcon());
		Widget_TextBlock->SetText(Instance->GetDescription().Name);
	}
	Widget_HorizontalBox->SetVisibility((Instance != nullptr) ? EVisibility::SelfHitTestInvisible : EVisibility::Hidden);
}

UItemAmmoEntity* SCharacterManager_Ammo::GetInstance() const
{
	return Instance;
}

void SCharacterManager_Ammo::SetDelegate(FOnInt32ValueChanged InOnValueChanged)
{
	OnValueChanged = InOnValueChanged;
}

void SCharacterManager_Ammo::SetAmount(const int32& Amount) 
{
	Widget_SpinBox->SetValue(Amount);
}

void SCharacterManager_Ammo::OnAmountChanged(int32 Value)
{
	OnValueChanged.ExecuteIfBound(Value);
}