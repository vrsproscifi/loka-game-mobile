// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/CharacterManagerWidgetStyle.h"

#include "Components/SResourceTextBox.h"

class UCharacterBaseEntity;

class LOKAGAME_API SCharacterManager_Lock : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SCharacterManager_Lock)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FCharacterManagerStyle>("SCharacterManagerStyle"))
	{}
	SLATE_EVENT(FOnClicked, OnClicked)
	SLATE_ATTRIBUTE(const UCharacterBaseEntity*, Character)
	SLATE_STYLE_ARGUMENT(FCharacterManagerStyle, Style)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);
	
protected:

	const FCharacterManagerStyle* Style;

	TAttribute<const UCharacterBaseEntity*> Character;

	FText GetCharacterName() const;
	FText GetCharacterDesc() const;

	int32 GetCharacterLevel() const;

	EResourceTextBoxType::Type GetCharacterCostType() const;
	int32 GetCharacterCostValue() const;
};
