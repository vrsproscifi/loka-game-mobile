#pragma once
#include "LobbyActionView.generated.h"

UENUM()
enum class ELobbyActionButton : uint8
{
	Welcome,
	Store,
	Hangar,
	League,
	Tournament,
	Achievements,
	ViewItemDetails,
	//============================
	Notifications,
	Contacts,
	Settings,
	Chat,
	Exit,
	
	//================================
	PvEBattle,
	PvPBattle,
	QuickBattle,
	
	//================================
	SwitchCharacter,	//	The buttons change the character (previous, next)
	SelectCharacter,	//	Character selection button
	SwitchProfile,		//	Profile buttons
	
	//================================
	SwitchFraction,		//	Store Fraction Page
	SelectFraction,		//	Store Fraction Select Button
	ScrollFraction,		//	Store scroll fraction page
	
	//================================
	InforamtionPremiumAccount,				// Top: PremiumAccount
    InforamtionBoosterOfReputation,			// Top: BoosterOfReputation
    InforamtionBoosterOfExperience,			// Top: BoosterOfExperience
    InforamtionBoosterOfMoney,				// Top: BoosterOfMoney
	InforamtionFraction,
};

USTRUCT()
struct FLobbyActionView
{
	GENERATED_USTRUCT_BODY()
	
	UPROPERTY(VisibleInstanceOnly) ELobbyActionButton OldAction;
	UPROPERTY(VisibleInstanceOnly) ELobbyActionButton NewAction;
	UPROPERTY(VisibleInstanceOnly) int64 Duration;
	UPROPERTY(VisibleInstanceOnly) int16 Direction;
};

USTRUCT()
struct FLobbyActionContainer
{
	GENERATED_USTRUCT_BODY()
	
	FLobbyActionContainer(){}
	FLobbyActionContainer(const TArray<FLobbyActionView>& views)
		: RequestId(FGuid::NewGuid().ToString())
		, Views(views)
	{

	}

	UPROPERTY(VisibleInstanceOnly) FString RequestId;
	UPROPERTY(VisibleInstanceOnly) TArray<FLobbyActionView> Views;
};