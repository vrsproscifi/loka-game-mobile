#pragma once
#include "GameSessionKillActionView.h"
#include "GameSessionItemView.generated.h"

namespace CategoryTypeId
{
	enum Type;
}

USTRUCT()
struct FGameSessionItemView
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(VisibleInstanceOnly) uint8 ModelId;
	UPROPERTY(VisibleInstanceOnly) TEnumAsByte<CategoryTypeId::Type> CategoryTypeId;
	UPROPERTY(VisibleInstanceOnly) FGameDamageStatistic Statistic;
	UPROPERTY(VisibleInstanceOnly) FString ProfileId;
};