#pragma once
#include "GameSessionKillActionView.h"
#include "GameSessionSwitchProfileView.generated.h"

USTRUCT()
struct FGameSessionSwitchProfileView
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(VisibleInstanceOnly) FString ProfileId;
	UPROPERTY(VisibleInstanceOnly) FString LastProfileId;
	UPROPERTY(VisibleInstanceOnly) int64 EquipTimeAtStartMatch;
	UPROPERTY(VisibleInstanceOnly) int64 SwitchTimeAtStartMatch;
	UPROPERTY(VisibleInstanceOnly) FGameDamageStatistic Statistic;
};