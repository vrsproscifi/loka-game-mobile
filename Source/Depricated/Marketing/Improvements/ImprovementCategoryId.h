#pragma once
#include "ImprovementCategoryId.generated.h"

UENUM()
enum class EImprovementCategoryId : uint8
{
	None,
	GamePlay,
	Lobby,
	Store,
};