#pragma once
#include "ImprovementProgressState.generated.h"

UENUM()
enum class EImprovementProgressState : uint8
{
	Voting,
	Development,
	Release,
};