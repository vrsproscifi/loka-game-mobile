#pragma once
#include "ImprovementVoteCreate.generated.h"



USTRUCT()
struct FImprovementVoteCreate
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(VisibleInstanceOnly) FString ImprovementId;
	UPROPERTY(VisibleInstanceOnly) bool IsApprove;

	FImprovementVoteCreate() : ImprovementId(), IsApprove(false) {}
	FImprovementVoteCreate(const FString& InString, const bool InIsApprove = true) : ImprovementId(InString), IsApprove(InIsApprove) {}
};