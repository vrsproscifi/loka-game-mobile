#pragma once
#include "ImprovementVoteView.generated.h"

enum class EImprovementProgressState : uint8;


USTRUCT()
struct FImprovementVoteView
{
	GENERATED_USTRUCT_BODY()
	
	UPROPERTY(VisibleInstanceOnly) FString Title;
	UPROPERTY(VisibleInstanceOnly) EImprovementProgressState CurrentProgressState;
	UPROPERTY(VisibleInstanceOnly) EImprovementProgressState OldProgressState;
};