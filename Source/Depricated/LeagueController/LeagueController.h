#pragma once
#include "Operation/Create.h"
#include "Operation/Donate.h"
#include "Operation/EditInfo.h"
#include "Operation/EditMember.h"
#include "Operation/List.h"
#include "Operation/View.h"



class LeagueController
{
public:
	Operation::CreateLeague::Operation	  Create;
	Operation::Donate::Operation		  Donate;
	Operation::EditInfo::Operation		  EditInfo;
	Operation::EditMember::Operation	  EditMember;
	Operation::List::Operation			  List;
	Operation::ViewDetails::Operation	  ViewDetails;
};