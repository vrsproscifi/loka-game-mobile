#pragma once
#include "LeagueMemberAccess.generated.h"

UENUM()
enum class ELeagueMemberAccess : uint8
{
	Member,
	Organizer,
	Assistant,
	Leader,
};