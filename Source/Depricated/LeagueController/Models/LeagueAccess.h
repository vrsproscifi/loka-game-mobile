#pragma once
#include "LeagueAccess.generated.h"


UENUM()
enum class ELeagueAccess : uint8
{
	Public,
	Private
};