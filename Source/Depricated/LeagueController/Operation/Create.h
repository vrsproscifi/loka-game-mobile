#pragma once
#include "ServiceOperation.h"
#include "RequestOneParam.h"


namespace Operation
{
	namespace CreateLeague
	{
		struct Request final : FOnlineJsonSerializable
		{
			Request(const FString &name, const FString &abbr, int32 price, int32 access)
			{
				Name = name;
				Abbr = abbr;
				JoinPrice = price;
				Access = access;
			}

			FString Name;

			FString Abbr;

			int32 JoinPrice;

			int32 Access;


			BEGIN_ONLINE_JSON_SERIALIZER
				ONLINE_JSON_SERIALIZE("Name", Name);
				ONLINE_JSON_SERIALIZE("Abbr", Abbr);
				ONLINE_JSON_SERIALIZE("JoinPrice", JoinPrice);
				ONLINE_JSON_SERIALIZE("Access", Access);
			END_ONLINE_JSON_SERIALIZER
		};

		class Operation final : public COperation<Request, ObjectSerializer::NativeObject, FRequestOneParam<bool>, ObjectSerializer::NativeObject>
		{
		public:
			Operation() : Super(FServerHost::MasterClient, "League/Create") { }

		};
	}
}