#pragma once
#include "ServiceOperation.h"


namespace Operation
{
	namespace Donate
	{
		struct Request final : FOnlineJsonSerializable
		{
			Request(const FString& login, const FString& password, bool isAuthorization)
				: Login(login)
				, Password(password)
				, IsAuthorization(isAuthorization)
			{

			}

			virtual ~Request() { }

			FString Login;

			FString Password;

			bool IsAuthorization;

			BEGIN_ONLINE_JSON_SERIALIZER
				ONLINE_JSON_SERIALIZE("Login", Login);
			ONLINE_JSON_SERIALIZE("Password", Password);
			ONLINE_JSON_SERIALIZE("IsAuthorization", IsAuthorization);
			END_ONLINE_JSON_SERIALIZER
		};

		struct Response final : FOnlineJsonSerializable
		{
			virtual ~Response() { }

			TArray<FString> Error;

			FString	Token;

			BEGIN_ONLINE_JSON_SERIALIZER
				ONLINE_JSON_SERIALIZE_ARRAY("Error", Error);
			ONLINE_JSON_SERIALIZE("Token", Token);
			END_ONLINE_JSON_SERIALIZER
		};

		class Operation final : public COperation<Request, ObjectSerializer::NativeObject, Response, ObjectSerializer::NativeObject>
		{
		public:
			Operation() : Super(FServerHost::MasterClient, "League/OnAuthentication") { }

		};
	}
}