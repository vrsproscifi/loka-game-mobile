#include "LokaGame.h"
#include "GameMode/LobbyGameMode.h"

#include "Slate/Notify/SNotifyList.h"
#include "Slate/Notify/SMessageBox.h"
#include "Localization/ButtonNameLocalization.h"

void ALobbyGameMode::BindSquadHandlers()
{
	squadController.GetSquadInvites.OnSuccess.AddUObject(this, &ALobbyGameMode::OnGetSquadInvitesResponse);
	squadController.LeaveFromSquad.OnSuccess.AddUObject(this, &ALobbyGameMode::OnLeaveMemberFromSquad);


	//==============================================================
	//						SendSquadInvite 
	squadController.SendSquadInvite.OnSuccess.AddUObject(this, &ALobbyGameMode::OnSendSquadInviteResponse);
	squadController.SendSquadInvite.OnInviteAlreadySent.Handler.AddUObject(this, &ALobbyGameMode::OnSendSquadInviteAlreadySent);
	squadController.SendSquadInvite.OnPlayerNotFound.Handler.AddUObject(this, &ALobbyGameMode::OnSendSquadInvitePlayerNotFound);
	squadController.SendSquadInvite.OnPlayerInSquad.Handler.AddUObject(this, &ALobbyGameMode::OnSendSquadInvitePlayerInSquad);
	squadController.SendSquadInvite.OnInviteYourself.Handler.AddUObject(this, &ALobbyGameMode::OnSendSquadInviteYourself);

	squadController.SendSquadInvite.OnPlayerNotOnline.Handler.AddUObject(this, &ALobbyGameMode::OnSendSquadInvitePlayerNotOnline);
	squadController.SendSquadInvite.OnYouAreNotLeader.Handler.AddUObject(this, &ALobbyGameMode::OnSendSquadInviteYouAreNotLeader);
	squadController.SendSquadInvite.OnInviteTimeoutSent.Handler.AddUObject(this, &ALobbyGameMode::OnSendSquadInviteInviteTimeoutSent);

	//==============================================================
	lobbyController.OnAttackTo.OnSuccess.AddUObject(this, &ALobbyGameMode::OnAttackToResponse);
	lobbyController.OnAttackTo.OnFailed.AddUObject(this, &ALobbyGameMode::OnAttack_ServerError);
	lobbyController.OnAttackTo.OnAttackOnYourself.Handler.AddUObject(this, &ALobbyGameMode::OnAttack_AttackOnYourself);
	lobbyController.OnAttackTo.OnPlayerUnderDefence.Handler.AddUObject(this, &ALobbyGameMode::OnAttack_PlayerUnderDefence);
	lobbyController.OnAttackTo.OnPlayerNotFound.Handler.AddUObject(this, &ALobbyGameMode::OnAttack_PlayerNotFound);

	//==============================================================
	//						SendDuelInvite 

	squadController.SendDuelInvite.OnSuccess.AddUObject(this, &ALobbyGameMode::OnSendDuelInviteResponse);
	squadController.SendDuelInvite.OnInviteAlreadySent.Handler.AddUObject(this, &ALobbyGameMode::OnSendSquadInviteAlreadySent);
	squadController.SendDuelInvite.OnPlayerNotFound.Handler.AddUObject(this, &ALobbyGameMode::OnSendSquadInvitePlayerNotFound);
	squadController.SendDuelInvite.OnPlayerInSquad.Handler.AddUObject(this, &ALobbyGameMode::OnSendSquadInvitePlayerInSquad);
	squadController.SendDuelInvite.OnInviteYourself.Handler.AddUObject(this, &ALobbyGameMode::OnSendDuelInviteYourself);
	
	squadController.SendDuelInvite.OnPlayerNotEnouthMoney.Handler.AddUObject(this, &ALobbyGameMode::OnSendDuelInvitePlayerNotEnouthMoney);
	squadController.SendDuelInvite.OnPlayerNotOnline.Handler.AddUObject(this, &ALobbyGameMode::OnSendSquadInvitePlayerNotOnline);
	squadController.SendDuelInvite.OnYouAreNotLeader.Handler.AddUObject(this, &ALobbyGameMode::OnSendSquadInviteYouAreNotLeader);
	squadController.SendDuelInvite.OnInviteTimeoutSent.Handler.AddUObject(this, &ALobbyGameMode::OnSendSquadInviteInviteTimeoutSent);



	//==============================================================
	//						RemoveFromSquad
	squadController.RemoveFromSquad.OnSuccess.AddUObject(this, &ALobbyGameMode::OnRemoveMemberFromSquad);
	squadController.RemoveFromSquad.OnPlayerNotFound.Handler.AddUObject(this, &ALobbyGameMode::OnRemoveFromSquadPlayerNotFound);
	squadController.RemoveFromSquad.OnPlayerNotFound.Handler.AddUObject(this, &ALobbyGameMode::OnRemoveFromSquadPlayerNotSquad);
	squadController.RemoveFromSquad.OnYouAreNotLeader.Handler.AddUObject(this, &ALobbyGameMode::OnRemoveFromSquadYouAreNotLeader);


	//==============================================================
	//						RespondSqaudInvite

	squadController.RespondSqaudInvite.OnSuccess.AddUObject(this, &ALobbyGameMode::OnRespondSqaudInviteResponse);
	squadController.RespondSqaudInvite.OnInviteNotFound.Handler.AddUObject(this, &ALobbyGameMode::OnRespondSqaudInviteInviteNotFound);
	squadController.RespondSqaudInvite.OnInviteAreRecived.Handler.AddUObject(this, &ALobbyGameMode::OnRespondSqaudInviteInviteAreRecived);
	squadController.RespondSqaudInvite.OnNotEnouthMoney.Handler.AddUObject(this, &ALobbyGameMode::OnRespondDuelInviteNotEnouthMoney);
	squadController.RespondSqaudInvite.OnSenderInSquad.Handler.AddUObject(this, &ALobbyGameMode::OnRespondSqaudInviteSenderInSquad);
	squadController.RespondSqaudInvite.OnSenderNotEnouthMoney.Handler.AddUObject(this, &ALobbyGameMode::OnRespondDuelInviteSenderNotEnouthMoney);



	//==============================================================
	//						RespondDuelInvite
	//
	squadController.RespondDuelInvite.OnSuccess.AddUObject(this, &ALobbyGameMode::OnRespondDuelInviteResponse);
	squadController.RespondDuelInvite.OnInviteNotFound.Handler.AddUObject(this, &ALobbyGameMode::OnRespondSqaudInviteInviteNotFound);
	squadController.RespondDuelInvite.OnInviteAreRecived.Handler.AddUObject(this, &ALobbyGameMode::OnRespondSqaudInviteInviteAreRecived);
	squadController.RespondDuelInvite.OnNotEnouthMoney.Handler.AddUObject(this, &ALobbyGameMode::OnRespondDuelInviteNotEnouthMoney);
	squadController.RespondDuelInvite.OnSenderInSquad.Handler.AddUObject(this, &ALobbyGameMode::OnRespondSqaudInviteSenderInSquad);
	squadController.RespondDuelInvite.OnSenderNotEnouthMoney.Handler.AddUObject(this, &ALobbyGameMode::OnRespondDuelInviteSenderNotEnouthMoney);
}

void ALobbyGameMode::OnGetSquadInvitesResponse(const FSquadInviteListContainer& invites)
{
	for (const auto invite : invites.Squads)
	{
		if (invite.State == ESquadInviteState::Submitted)
		{
			const auto id = FName(*FString::Printf(TEXT("SquadInvite_%s"), *invite.InviteId));

			Slate_NotifyList->AddNotify(FNotifyInfo(
				FText::Format(NSLOCTEXT("Notify", "Notify.SquadInvite.Desc", "{0} has invited you in the squad"), 
				TextHelper_NotifyValue.SetValue(invite.Name).ToText()),
				NSLOCTEXT("Notify", "Notify.SquadInvite.Title", "Invite into Squad")
			));

			SMessageBox::AddQueueMessage(id, FOnClickedOutside::CreateLambda([&, qeueueId = id, i = invite, action = squadController.RespondSqaudInvite]()
			{
				SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "MessageBox.SquadInvite.Title", "Invite into Squad"));
				SMessageBox::Get()->SetContent(FText::Format(NSLOCTEXT("Notify", "MessageBox.SquadInvite.Desc", "Do you want to accept the invitation from {0} into Squad?"), TextHelper_NotifyValue.SetValue(i.Name).ToText()));

				SMessageBox::Get()->SetButtonsText(FButtonNameLocalization::Yes, FText::GetEmpty(), FButtonNameLocalization::No);
				SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton button)
				{
					if (button == EMessageBoxButton::Left)
					{
						action.Request(FSquadRespond(i.InviteId, true));
					}
					else
					{
						action.Request(FSquadRespond(i.InviteId, false));
					}
					SMessageBox::Get()->ToggleWidget(false);
					SMessageBox::RemoveQueueMessage(qeueueId);
				});
				SMessageBox::Get()->SetEnableClose(false);
				SMessageBox::Get()->ToggleWidget(true);
			}));
		}
		else
		{
			FText ValueName = TextHelper_NotifyValue.SetValue(invite.Name).ToText();

			if (invite.State & ESquadInviteState::Approved)
			{
				Slate_NotifyList->AddNotify(FNotifyInfo(
					FText::Format(NSLOCTEXT("Notify", "Notify.SquadInviteApproved.Desc", "{0} has approved invited you in the squad"), ValueName),
					NSLOCTEXT("Notify", "Notify.SquadInvite.Title", "Invite into Squad")
				));
			}
			else
			{
				Slate_NotifyList->AddNotify(FNotifyInfo(
					FText::Format(NSLOCTEXT("Notify", "Notify.SquadInviteRejected.Desc", "{0} has rejected invited you in the squad"), ValueName),
					NSLOCTEXT("Notify", "Notify.SquadInvite.Title", "Invite into Squad")
				));
			}
		}
	}

	OnGetSquadDuelInvitesResponse(invites);
}


void ALobbyGameMode::OnLeaveMemberFromSquad()
{
	Slate_NotifyList->AddNotify(FNotifyInfo(
		NSLOCTEXT("Notify", "Notify.LeaveFromSquad.Desc", "You have left the squad"),
		NSLOCTEXT("Notify", "Notify.LeaveFromSquad.Title", "You have left the squad")
	));
}


//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//						SendSquadInvite 

void ALobbyGameMode::OnSendSquadInviteResponse(const FRequestOneParam<FString>& playerName)
{
	Slate_NotifyList->AddNotify(FNotifyInfo(
		FText::Format(NSLOCTEXT("Notify", "Notify.OnSendSquadInviteResponse.Desc", "You have sent an invitation to {0} in the squad."), TextHelper_NotifyValue.SetValue(playerName).ToText()),
		NSLOCTEXT("Notify", "Notify.OnSendSquadInviteResponse.Title", "Outgoing invitation squad"))
	);
}


void ALobbyGameMode::OnSendSquadInviteYourself()
{
	Slate_NotifyList->AddNotify(FNotifyInfo(NSLOCTEXT("Notify", "Notify.OnSendSquadInviteYourself.Desc", "You can not invite yourself in the squad"),
		NSLOCTEXT("Notify", "Notify.OnSendSquadInviteYourself.Title", "Outgoing invitation squad"))
	);
}


//==============================================================

void ALobbyGameMode::OnSendSquadInviteAlreadySent(const FRequestOneParam<FString>& playerName)
{
	Slate_NotifyList->AddNotify(FNotifyInfo(
		FText::Format(NSLOCTEXT("Notify", "Notify.OnSendSquadInviteAlreadySent.Desc", "You have already sent {0} an invitation to squad."), TextHelper_NotifyValue.SetValue(playerName).ToText()),
		NSLOCTEXT("Notify", "Notify.OnSendSquadInviteAlreadySent.Title", "Outgoing invitation squad"))
	);
}

//==============================================================

void ALobbyGameMode::OnSendSquadInvitePlayerNotFound()
{
	Slate_NotifyList->AddNotify(FNotifyInfo(NSLOCTEXT("Notify", "Notify.OnSendSquadInvitePlayerNotFound.Desc", "Could not find a player with the same name"),
		NSLOCTEXT("Notify", "Notify.OnSendSquadInvitePlayerNotFound.Title", "Outgoing invitation squad"))
	);
}

//==============================================================

void ALobbyGameMode::OnSendSquadInvitePlayerInSquad(const FRequestOneParam<FString>& playerName)
{
	Slate_NotifyList->AddNotify(FNotifyInfo(
		FText::Format(NSLOCTEXT("Notify", "Notify.OnSendSquadInvitePlayerInSquad.Desc", "{0} is in the squad."), TextHelper_NotifyValue.SetValue(playerName).ToText()),
		NSLOCTEXT("Notify", "Notify.OnSendSquadInvitePlayerInSquad.Title", "Outgoing invitation squad"))
	);
}


void ALobbyGameMode::OnSendSquadInvitePlayerNotOnline(const FRequestOneParam<FString>& playerName)
{
	Slate_NotifyList->AddNotify(FNotifyInfo(
		FText::Format(NSLOCTEXT("Notify", "Notify.OnSendSquadInvitePlayerNotOnline.Desc", "{0} is offline."), TextHelper_NotifyValue.SetValue(playerName).ToText()),
		NSLOCTEXT("Notify", "Notify.OnSendSquadInvitePlayerNotOnline.Title", "Outgoing invitation squad"))
	);
}

void ALobbyGameMode::OnSendSquadInviteYouAreNotLeader()
{
	Slate_NotifyList->AddNotify(FNotifyInfo(
		NSLOCTEXT("Notify", "Notify.OnSendSquadInviteYouAreNotLeader.Desc", "You are not a leader for invite player to squad!"),
		NSLOCTEXT("Notify", "Notify.OnSendSquadInviteYouAreNotLeader.Title", "Outgoing invitation squad"))
	);
}

void ALobbyGameMode::OnSendSquadInviteInviteTimeoutSent()
{
	Slate_NotifyList->AddNotify(FNotifyInfo(
		NSLOCTEXT("Notify", "Notify.OnSendSquadInviteInviteTimeoutSent.Desc", "You can not send invitations more than once every five seconds!"),
		NSLOCTEXT("Notify", "Notify.OnSendSquadInviteInviteTimeoutSent.Title", "Outgoing invitation squad"))
	);
}


//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//						RemoveFromSquad


void ALobbyGameMode::OnRemoveMemberFromSquad(const FRequestOneParam<FString>& playerName)
{
	const auto id = FName(*FString::Printf(TEXT("RemoveFromSquad_%s"), *playerName.Value));

	SMessageBox::AddQueueMessage(id, FOnClickedOutside::CreateLambda([&, qeueueId = id, name = playerName.Value]() {
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "Notify.RemoveFromSquad.Title", "Member removed from squad"));
		SMessageBox::Get()->SetContent(FText::Format(NSLOCTEXT("Notify", "Notify.RemoveFromSquad.Desc", "You kicked out {0} from group"), TextHelper_NotifyValue.SetValue(name).ToText()));
		SMessageBox::Get()->SetButtonsText(FText::GetEmpty(), NSLOCTEXT("All", "All.Close", "Close"));
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton)
		{
			SMessageBox::Get()->ToggleWidget(false);
			SMessageBox::RemoveQueueMessage(qeueueId);
		});
		SMessageBox::Get()->SetEnableClose(false);
		SMessageBox::Get()->ToggleWidget(true);
	}));
}

//==============================================================

void ALobbyGameMode::OnRemoveFromSquadPlayerNotFound()
{
	Slate_NotifyList->AddNotify(FNotifyInfo(NSLOCTEXT("Notify", "Notify.OnRemoveFromSquadPlayerNotFound.Desc", "Could not find a player with the same name"),
		NSLOCTEXT("Notify", "Notify.OnRemoveFromSquadPlayerNotFound.Title", "Squad management"))
	);
}

//==============================================================

void ALobbyGameMode::OnRemoveFromSquadPlayerNotSquad()
{
	Slate_NotifyList->AddNotify(FNotifyInfo(NSLOCTEXT("Notify", "Notify.OnRemoveFromSquadPlayerNotFound.Desc", "The player is not in the squad"),
		NSLOCTEXT("Notify", "Notify.OnRemoveFromSquadPlayerNotFound.Title", "Squad management"))
	);
}

//==============================================================

void ALobbyGameMode::OnRemoveFromSquadYouAreNotLeader()
{
	Slate_NotifyList->AddNotify(FNotifyInfo(NSLOCTEXT("Notify", "Notify.OnRemoveFromSquadYouAreNotLeader.Desc", "You are not a leader for kick player from squad"),
		NSLOCTEXT("Notify", "Notify.OnRemoveFromSquadYouAreNotLeader.Title", "Squad management"))
	);
}



//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//						RespondSqaudInvite
void ALobbyGameMode::OnRespondSqaudInviteResponse(const FRequestOneParam<FString>& playerName)
{
	Slate_NotifyList->AddNotify(FNotifyInfo(
		FText::Format(NSLOCTEXT("Notify", "Notify.OnRespondSqaudInviteResponse.Desc", "You have accepted the invitation in the squad from {0}"), TextHelper_NotifyValue.SetValue(playerName).ToText()),
		NSLOCTEXT("Notify", "Notify.OnRespondSqaudInviteResponse.Title", "Incoming invitation squad"))
	);
}

//==============================================================

void ALobbyGameMode::OnRespondSqaudInviteInviteNotFound()
{
	Slate_NotifyList->AddNotify(FNotifyInfo(NSLOCTEXT("Notify", "Notify.OnRespondSqaudInviteInviteNotFound.Desc", "Unable to find such an invitation"),
		NSLOCTEXT("Notify", "Notify.OnRespondSqaudInviteInviteNotFound.Title", "Squad management"))
	);
}

//==============================================================

void ALobbyGameMode::OnRespondSqaudInviteInviteAreRecived(const FRequestOneParam<FString>& playerName)
{
	Slate_NotifyList->AddNotify(FNotifyInfo(
		FText::Format(NSLOCTEXT("Notify", "Notify.OnRespondSqaudInviteResponse.Desc", "You have already responded to the invitation to squad from {0}"), TextHelper_NotifyValue.SetValue(playerName).ToText()),
		NSLOCTEXT("Notify", "Notify.OnRespondSqaudInviteResponse.Title", "Incoming invitation squad"))
	);
}



void ALobbyGameMode::OnRespondSqaudInviteSenderInSquad(const FRequestOneParam<FString>& playerName)
{
	Slate_NotifyList->AddNotify(FNotifyInfo(
		FText::Format(NSLOCTEXT("Notify", "Notify.OnRespondSqaudInviteSenderInSquad.Desc", "You can not accept an invitation to a duel, because {0} is already a member of the squad."), TextHelper_NotifyValue.SetValue(playerName).ToText()),
		NSLOCTEXT("Notify", "Notify.OnRespondSqaudInviteSenderInSquad.Title", "Incoming invitation duel"))
	);
}

