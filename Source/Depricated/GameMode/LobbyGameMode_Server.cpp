#include "LokaGame.h"
#include "GameMode/LobbyGameMode.h"

#include "Slate/Notify/SNotifyList.h"
#include "Slate/Notify/SMessageBox.h"
#include "Localization/ButtonNameLocalization.h"

namespace EServerStatus
{
	enum Type
	{
		None,
		Working,
		Service
	};
}

void ALobbyGameMode::OnCheckServerStatusResponse(const FRequestOneParam<int32>& response)
{
	auto status = static_cast<EServerStatus::Type>(response.Value);
	if (status == EServerStatus::Working)
	{
		GetWorldTimerManager().SetTimer(lobbyController.SearchStatus.TimerHandle, lobbyController.SearchStatus.TimerDelegate, 4.0f, true, 4.0f);
		storeController.ListOfInstance.TimerStart(GetWorldTimerManager());


		if (GetWorld()->IsPlayInEditor() == false && bUseSteamAuthentication)
		{
			//TODO: Message Box: ����������� � STEAM, �������� ������
			SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Authentication", "Authentication.Steam.Title", "Steam Authentication"));
			SMessageBox::Get()->SetContent(FText::Format(
				NSLOCTEXT("Authentication", "Authentication.Steam.Waiting", "Waiting authentication from steam, please wait. \n{0}"),
				FText::GetEmpty())
			);
			SMessageBox::Get()->SetButtonsText();
			SMessageBox::Get()->SetEnableClose(false);
			SMessageBox::Get()->ToggleWidget(true);
		}
	}
	else
	{
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Authentication", "Authentication.Service.Title", "Engineering works"));
		SMessageBox::Get()->SetContent(NSLOCTEXT("Authentication", "Authentication.Service.Desc", "On the server, the technical work carried out. \nThe work will be impeccablyrenovated \nin the nearest announcement time. \nIt can take anywhere from 5 to 20 minutes. \nThank you for understanding."));
		SMessageBox::Get()->SetButtonsText(FText::GetEmpty(), FButtonNameLocalization::Ok);
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton&) { GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit"); });
		SMessageBox::Get()->SetEnableClose(false);
		SMessageBox::Get()->ToggleWidget(true);
	}
}

void ALobbyGameMode::OnCheckServerStatusNotResponsed()
{
	SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Authentication", "Authentication.ServiceUnavailable.Title", "Service is unavailable"));
	SMessageBox::Get()->SetContent(NSLOCTEXT("Authentication", "Authentication.ServiceUnavailable.Desc", "On the server, the technical work carried out. \nThe work will be impeccablyrenovated \nin the nearest announcement time. \nIt can take anywhere from 5 to 20 minutes. \nThank you for understanding."));
	SMessageBox::Get()->SetButtonsText(FText::GetEmpty(), FButtonNameLocalization::Ok);
	SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton&) { GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit"); });
	SMessageBox::Get()->SetEnableClose(false);
	SMessageBox::Get()->ToggleWidget(true);
}

void ALobbyGameMode::OnCheckServerStatusNotWorking()
{
	SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Authentication", "Authentication.ServiceNotWorking.Title", "Service is unavailable"));
	SMessageBox::Get()->SetContent(NSLOCTEXT("Authentication", "Authentication.ServiceNotWorking.Desc", "Could not connect to the server.\nPerhaps the problem is local.\n\nCheck the connection to the Internet and\nthe right of access in the firewall."));
	SMessageBox::Get()->SetButtonsText(FText::GetEmpty(), FButtonNameLocalization::Ok);
	SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton&) { GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit"); });
	SMessageBox::Get()->SetEnableClose(false);
	SMessageBox::Get()->ToggleWidget(true);
}