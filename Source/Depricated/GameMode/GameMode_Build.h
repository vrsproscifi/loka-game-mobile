// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "RichTextHelpers.h"
#include "OnlineGameMode.h"
#include <SquadController/SquadController.h>
#include "BuildingController/BuildingController.h"
#include "IdentityController/IdentityController.h"
#include "StoreController/StoreController.h"
#include "LobbyController.h"
#include "GameMode_Build.generated.h"


class UItemBuildEntity;
class ABuildPlacedComponent;
class ABuildPlayerController;
class AGameState_Build;

UCLASS()
class LOKAGAME_API AGameMode_Build : public AOnlineGameMode
{
	friend class ABuildHelperComponent;

	GENERATED_BODY()
	
public:
	virtual void PostLogin(APlayerController* NewPlayer);
	virtual void Logout(AController* Exiting) override;
	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;
	virtual void InitGameState() override;
	virtual void SendStartMatchRequest() override;
	virtual void EndMatch() override;

	UPROPERTY(Transient) FString OwnerId;
	UPROPERTY(Transient) FString OwnerTokenId;
	UPROPERTY(Transient) uint64 ContextValidator;
	UPROPERTY(Transient) FTimerHandle PlayerLogoutTimeoutHandler;
	UPROPERTY(Transient) FTimerHandle ShutDownTimerHandler;

	FRichHelpers::FTextHelper TextHelper_NotifyValue;

	void OnServerShutDown() const;
	void OnPlayerLogoutTimeout();
	void ValidatePlayerLogoutTimeout();
	void ProxySendDuelAnswer(const FString& InInviteId, const bool InAnswer);

	AGameMode_Build(const class FObjectInitializer& PCIP);
	void OnRequestClientData(const FPlayerEntityInfo& response) const;

	virtual void StartPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	void RequestBuyBuildItem(APlayerController* InRequestFrom, UItemBuildEntity* InItem, const int32& InCount = 1);
	void OnBuildPurchaseNoMoney();
	void RequestStartTutorial() const;

	UFUNCTION(BlueprintCallable, Category = Runtime)
	void RequestPlayerExiting(APlayerController* InRequestFrom);

	UFUNCTION(BlueprintCallable, Category = Runtime)
	ABuildPlayerController* GetSessionOwner() const;

protected:

	//void TravelToGame(const FSearchSessionStatus& response);
	//void OnSessionMatchFounded(const FSearchSessionStatus& response);
	void OnGetMatchResults(const TArray<FLobbyMatchResultPreview>& results);


	//========================================================================================
	//									[ DUEL ]
	SquadController		squadController;
	void BindSquadHandlers();
	void OnGetSquadDuelInvitesResponse(const struct FSquadInviteListContainer& invites);

	//==============================================================
	//						RespondSqaudInvite
	void OnRespondSqaudInviteResponse(const FRequestOneParam<FString>& playerName);
	void OnRespondSqaudInviteInviteNotFound();
	void OnRespondSqaudInviteInviteAreRecived(const FRequestOneParam<FString>& playerName);
	void OnRespondDuelInviteNotEnouthMoney(const FRequestOneParam<FString>& playerName);
	void OnRespondSqaudInviteSenderInSquad(const FRequestOneParam<FString>& playerName);
	void OnRespondDuelInviteSenderNotEnouthMoney(const FRequestOneParam<FString>& playerName);
	void OnRespondDuelInviteResponse(const FRequestOneParam<FString>& playerName);

	//========================================================================================


	virtual void HandleMatchIsWaitingToStart() override;
	virtual void HandleMatchHasStarted() override;
	virtual void HandleMatchHasEnded() override;
	virtual AActor* ChoosePlayerStart_Implementation(AController* Player) override;

	void OnLootDataList(const FLobbyClientLootData& InList);
	void OnBuildingsList(const TArray<FWorldBuildingItemView>& InList);
	void OnBuildingsRepairList(const TArray<FWorldBuildingRepairView>& InList);
	void OnBuildPlaced(const FWorldBuildingItemView& InAnswer);
	void OnBuildRemoved(const FWorldBuildingItemView& InAnswer);
	void OnCheckAccountStatusResponse(const FAccountStatusContainer& response);
	void OnBuildPurchased(const FUnlockItemResponseContainer&);	
	void OnStartTutorial(const FStartTutorialView&);	

	virtual void DefaultTimer() override;

	UPROPERTY()
	TMap<FGuid, ABuildPlacedComponent*> StorageComponents;

	UPROPERTY()
	TMap<FGuid, ABuildPlacedComponent*> BuildingsList;

	UPROPERTY()
	TMap<FGuid, ABuildPlacedComponent*> DamagedBuildingsList;

	UPROPERTY()
	AGameState_Build* BuildGameState;

	UPROPERTY()
	int32 NonePlayersTime;

public:

	BuildingController	buildingController;
	IdentityController	identityController;
	StoreController		storeController;
	LobbyController		lobbyController;
};
