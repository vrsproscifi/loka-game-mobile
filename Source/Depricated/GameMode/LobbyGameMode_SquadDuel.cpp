#include "LokaGame.h"
#include "GameMode/LobbyGameMode.h"

#include "Slate/Notify/SNotifyList.h"
#include "Slate/Notify/SMessageBox.h"

#include "Slate/Components/SWindowDuelRules.h"
#include "Entity/DuelStructures.h"
#include "Localization/ButtonNameLocalization.h"
#include "RichTextHelpers.h"


void ALobbyGameMode::OnGetSquadDuelInvitesResponse(const FSquadInviteListContainer& invites)
{
	for (const auto invite : invites.Duels)
	{
		if (invite.State == ESquadInviteState::Submitted)
		{
			const auto id = FName(*FString::Printf(TEXT("DuelInvite_%s"), *invite.InviteId));
			Slate_NotifyList->AddNotify(FNotifyInfo(
				FText::Format(NSLOCTEXT("Notify", "Notify.DuelInvite.Desc", "{0} challenge to a duel on weapons: {1}, number of lives: {2}. Bet: {3}"), 
				TextHelper_NotifyValue.SetValue(invite.Name).ToText(),
				TextHelper_NotifyValue.SetValue(FDuelWeapons::GetText(invite.WeaponType)).ToText(),
				TextHelper_NotifyValue.SetValue(FText::AsNumber(invite.NumberOfLives)).ToText(),
				invite.Bet.ToResourse()),
				NSLOCTEXT("Notify", "Notify.DuelInvite.Title", "Challenge to a duel!")
			));

			SMessageBox::AddQueueMessage(id, FOnClickedOutside::CreateLambda([&, qeueueId = id, i = invite, action = squadController.RespondDuelInvite]()
			{
				SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "MessageBox.DuelInvite.Title", "Invite into Duel"));
				SMessageBox::Get()->SetContent(FText::Format(NSLOCTEXT("MessageBox", "Notify.DuelInvite.Desc", "Do you want to accept the invitation from {0}\n into Duel on weapon: {1}? Number of lives: {2}.\n Bet: {3}"), 
											   TextHelper_NotifyValue.SetValue(i.Name).ToText(),
											   TextHelper_NotifyValue.SetValue(FDuelWeapons::GetText(i.WeaponType)).ToText(),
											   TextHelper_NotifyValue.SetValue(FText::AsNumber(i.NumberOfLives)).ToText(),
											   i.Bet.ToResourse()));


				SMessageBox::Get()->SetButtonsText(FButtonNameLocalization::Yes, FText::GetEmpty(), FButtonNameLocalization::No);
				SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton button)
				{
					if (button == EMessageBoxButton::Left)
					{
						action.Request(FSquadRespond(i.InviteId, true));
					}
					else
					{
						action.Request(FSquadRespond(i.InviteId, false));
					}
					SMessageBox::Get()->ToggleWidget(false);
					SMessageBox::RemoveQueueMessage(qeueueId);
				});
				SMessageBox::Get()->SetEnableClose(false);
				SMessageBox::Get()->ToggleWidget(true);
			}));
		}
		else
		{
			if (invite.State & ESquadInviteState::Approved)
			{
				Slate_NotifyList->AddNotify(FNotifyInfo(
					FText::Format(NSLOCTEXT("Notify", "Notify.DuelInviteApproved.Desc", "{0} approved challenge to a duel on weapons: {1}, number of lives: {2}. Bet: {3}"), 
					TextHelper_NotifyValue.SetValue(invite.Name).ToText(),
					TextHelper_NotifyValue.SetValue(FDuelWeapons::GetText(invite.WeaponType)).ToText(),
					TextHelper_NotifyValue.SetValue(FText::AsNumber(invite.NumberOfLives)).ToText(),
					invite.Bet.ToResourse()),
					NSLOCTEXT("Notify", "Notify.DuelInvite.Title", "Challenge to a duel!")
				));
			}
			else
			{
				Slate_NotifyList->AddNotify(FNotifyInfo(
					FText::Format(NSLOCTEXT("Notify", "Notify.DuelInviteRejected.Desc", "{0} rejected challenge to a duel on weapons: {1}, number of lives: {2}. Bet: {3}"), 
					TextHelper_NotifyValue.SetValue(invite.Name).ToText(),
					TextHelper_NotifyValue.SetValue(FDuelWeapons::GetText(invite.WeaponType)).ToText(),
					TextHelper_NotifyValue.SetValue(FText::AsNumber(invite.NumberOfLives)).ToText(),
					invite.Bet.ToResourse()),
					NSLOCTEXT("Notify", "Notify.DuelInvite.Title", "Challenge to a duel!")
				));
			}
		}
	}
}

void ALobbyGameMode::OnSendDuelInviteProxy(const FRequestOneParam<FString>& playerId)
{
	if (Slate_MessageBoxDuel.IsValid())
	{
		squadController.SendDuelInvite.Request(Slate_MessageBoxDuel->FillAndGetRequestData());
		Slate_MessageBoxDuel.Reset();
	}
	else
	{
		SAssignNew(Slate_MessageBoxDuel, SWindowDuelRules);
		Slate_MessageBoxDuel->RequestData.TargetId = playerId;

		SMessageBox::AddQueueMessage("DuelInviteProxy", FOnClickedOutside::CreateLambda([&]()
		{
			SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "MessageBox.SendDuelInvite.Title", "Make Duel"));
			SMessageBox::Get()->SetContent(Slate_MessageBoxDuel);

			SMessageBox::Get()->SetButtonsText(FButtonNameLocalization::Yes, FText::GetEmpty(), FButtonNameLocalization::No);
			SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton button) {
				if (button == EMessageBoxButton::Left)
				{
					OnSendDuelInviteProxy(FRequestOneParam<FString>(""));
				}
				else
				{
					Slate_MessageBoxDuel.Reset();
				}

				SMessageBox::Get()->ToggleWidget(false);
				SMessageBox::RemoveQueueMessage("DuelInviteProxy");
			});
			SMessageBox::Get()->SetEnableClose(false);
			SMessageBox::Get()->ToggleWidget(true);
		}));
	}
}

//==============================================================

void ALobbyGameMode::OnAttackToResponse() const
{
	JoinGameState = EJoinGameState::Search;

	const FText content = NSLOCTEXT("Notify", "Notify.OnAttackToResponse.Desc", "Preparation for an attack can take from a few seconds to one minute.");
	Slate_NotifyList->AddNotify(FNotifyInfo(content, NSLOCTEXT("Notify", "Notify.OnAttackToResponse.Title", "Preparations for the attack")));
}

void ALobbyGameMode::OnAttack_AttackOnYourself() const
{
	Slate_NotifyList->AddNotify(FNotifyInfo(
		NSLOCTEXT("Notify", "Notify.OnAttack_AttackOnYourself.Desc", "You can not attack yourself"),
		NSLOCTEXT("Notify", "Notify.OnAttack_AttackOnYourself.Title", "Error while attacking the player"))
	);
}

void ALobbyGameMode::OnAttack_PlayerNotFound() const
{
	Slate_NotifyList->AddNotify(FNotifyInfo(
		NSLOCTEXT("Notify", "Notify.OnAttack_PlayerNotFound.Desc", "Player not found"),
		NSLOCTEXT("Notify", "Notify.OnAttack_PlayerNotFound.Title", "Error while attacking the player"))
	);
}

void ALobbyGameMode::OnAttack_PlayerUnderDefence() const
{
	//	����� ��������� ��� ������� �� ���������, ���������� ������� �����.
	Slate_NotifyList->AddNotify(FNotifyInfo(
		NSLOCTEXT("Notify", "Notify.OnAttack_PlayerUnderDefence.Desc", "The player is protected from attacks, try to attack later."),
		NSLOCTEXT("Notify", "Notify.OnAttack_PlayerUnderDefence.Title", "Error while attacking the player"))
	);
}

void ALobbyGameMode::OnAttack_ServerError() const
{
	Slate_NotifyList->AddNotify(FNotifyInfo(
		NSLOCTEXT("Notify", "Notify.OnAttack_ServerError.Desc", "During the preparation for the attack, an unexpected error occurred!"),
		NSLOCTEXT("Notify", "Notify.OnAttack_ServerError.Title", "Error while attacking the player"))
	);
}


void ALobbyGameMode::OnSendDuelInviteResponse(const FSendSquadDuelInviteResponse& response)
{
	JoinGameState = EJoinGameState::Search;

	const FText content = FText::Format(NSLOCTEXT("Notify", "Notify.OnSendDuelInviteResponse.Desc", "You have sent an invitation to {0} in the duel on weapons: {1}, number of lives: {2}. Bet: {3}"), 
										TextHelper_NotifyValue.SetValue(response.PlayerName).ToText(),
										TextHelper_NotifyValue.SetValue(FDuelWeapons::GetText(response.WeaponType)).ToText(),
										TextHelper_NotifyValue.SetValue(FText::AsNumber(response.NumberOfLives)).ToText(),
										response.Bet.ToResourse());

	Slate_NotifyList->AddNotify(FNotifyInfo(content, NSLOCTEXT("Notify", "Notify.OnSendDuelInviteResponse.Title", "Outgoing invitation duel")));
}

//==============================================================

void ALobbyGameMode::OnSendDuelInviteYourself()
{
	Slate_NotifyList->AddNotify(FNotifyInfo(NSLOCTEXT("Notify", "Notify.OnSendDuelInviteYourself.Desc", "You can not invite yourself in the duel"),
		NSLOCTEXT("Notify", "Notify.OnSendDuelInviteYourself.Title", "Outgoing invitation squad"))
	);
}

//==============================================================

void ALobbyGameMode::OnSendDuelInvitePlayerNotEnouthMoney(const FRequestOneParam<FString>& playerName)
{
	Slate_NotifyList->AddNotify(FNotifyInfo(
		FText::Format(NSLOCTEXT("Notify", "Notify.OnSendDuelInvitePlayerNotEnouthMoney.Desc", "{0} does not have enough money to participate in a duel."), TextHelper_NotifyValue.SetValue(playerName).ToText()),
		NSLOCTEXT("Notify", "Notify.OnSendDuelInvitePlayerNotEnouthMoney.Title", "Outgoing invitation squad"))
	);
}

//==============================================================

void ALobbyGameMode::OnRespondDuelInviteNotEnouthMoney(const FRequestOneParam<FString>& playerName)
{
	Slate_NotifyList->AddNotify(FNotifyInfo(
		FText::Format(NSLOCTEXT("Notify", "Notify.OnRespondDuelInviteNotEnouthMoney.Desc", "You do not have the money to accept an invitation to a duel from {0}"), TextHelper_NotifyValue.SetValue(playerName).ToText()),
		NSLOCTEXT("Notify", "Notify.OnRespondDuelInviteNotEnouthMoney.Title", "Incoming invitation duel"))
	);

	SMessageBox::AddQueueMessage("DuelInviteNotEnouthMoney", FOnClickedOutside::CreateLambda([&, name = playerName.Value]()
	{
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "MessageBox.DuelInviteNotEnouthMoney.Title", "Incoming invitation duel"));
		SMessageBox::Get()->SetContent(FText::Format(NSLOCTEXT("Notify", "MessageBox.DuelInviteNotEnouthMoney.Desc", "You do not have the money that would invite {0} to a duel. \nDo you want to replenish your account through the site?"), TextHelper_NotifyValue.SetValue(name).ToText()));

		SMessageBox::Get()->SetButtonsText(FButtonNameLocalization::Yes, FText::GetEmpty(), NSLOCTEXT("All", "All.No", "No"));
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton button)
		{
			if (button == EMessageBoxButton::Left)
			{
				FPlatformProcess::LaunchURL(TEXT("http://new.lokagame.com/"), NULL, NULL);
			}
			SMessageBox::Get()->ToggleWidget(false);
			SMessageBox::RemoveQueueMessage("DuelInviteNotEnouthMoney");
		});
		SMessageBox::Get()->SetEnableClose(false);
		SMessageBox::Get()->ToggleWidget(true);
	}));
}

//==============================================================

void ALobbyGameMode::OnRespondDuelInviteSenderNotEnouthMoney(const FRequestOneParam<FString>& playerName)
{
	Slate_NotifyList->AddNotify(FNotifyInfo(
		FText::Format(NSLOCTEXT("Notify", "Notify.OnRespondDuelInviteSenderNotEnouthMoney.Desc", "You can not accept an invitation to a duel, since {0} (the organizer of a duel) insufficient funds for its implementation."), TextHelper_NotifyValue.SetValue(playerName).ToText()),
		NSLOCTEXT("Notify", "Notify.OnRespondDuelInviteSenderNotEnouthMoney.Title", "Incoming invitation duel"))
	);
}

//==============================================================

void ALobbyGameMode::OnRespondDuelInviteResponse(const FRequestOneParam<FString>& playerName)
{
	JoinGameState = EJoinGameState::Search;

	Slate_NotifyList->AddNotify(FNotifyInfo(
		FText::Format(NSLOCTEXT("Notify", "Notify.OnRespondDuelInviteResponse.Desc", "You have accepted the invitation in the squad from {0}"), TextHelper_NotifyValue.SetValue(playerName).ToText()),
		NSLOCTEXT("Notify", "Notify.OnRespondDuelInviteResponse.Title", "Incoming invitation squad"))
	);
}