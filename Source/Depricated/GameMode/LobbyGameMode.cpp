	// VRSPRO

#include "LokaGame.h"
#include "GameInstance/ShooterGameInstance.h"

#include "GameMode/LobbyGameMode.h"
#include "PlayerController/LobbyPlayerController.h"

#include "Slate/Tournament/STournamentForm.h"
#include "Slate/Tournament/STournamentTeam.h"
#include "Slate/Tournament/STournamentTree.h"
#include "Slate/League/SLeagueForm.h"
#include "Slate/League/SLeagueFormInfo.h"
#include "Slate/Notify/SNotifyList.h"
#include "Slate/Notify/SLobbyChat.h"
#include "Slate/Notify/SMessageBox.h"
#include "Slate/Components/SInputWindow.h"
#include "Slate/Character/SCharacterManager.h"
#include "Slate/Fractions/SFractionManager.h"
#include "Slate/Fractions/SFractionManager_Item.h"
#include "Slate/Fractions/SFractionSelector.h"
#include "Slate/Lobby/SLobbyContainer.h"
#include "Slate/Lobby/SGeneralPage.h"
#include "Slate/Lobby/SLobbyAchievements.h"
#include "Slate/Auth/SAuthForm.h"
#include "Slate/Settings/SSettingsManager.h"
#include "Slate/Components/SWindowEverydayBonus.h"
#include "Slate/Components/SWindowTournamentRules.h"
#include "SWindowVotingForChange.h"
#include "SWindowVotingForProposals.h"
#include "SWindowDetailsForProposals.h"
#include "SFriendsForm.h"
#include "SFriends_Commands.h"
#include "OnlineStats.h"
#include "OnlineAchievementsInterface.h"
#include "Slate/Styles/WelcomeWindowsWidgetStyle.h"

#include <EntityRepository.h>
#include "Entity/Weapon/ItemWeaponEntity.h"
#include "PlayerCharacter/PlayerCharacterInstance.h"
#include "UTGameUserSettings.h"
#include "Armour/ItemArmourEntity.h"
#include "Module/ItemModuleEntity.h"
#include "Grenade/ItemGrenadeEntity.h"

#include "Fraction/FractionEntity.h"

#include "AssetCaptureScene.h"
#include "Localization/ButtonNameLocalization.h"
#include "RichTextHelpers.h"
#include "WindowsNotify.h"
#include "SUTMenuBase.h"
//#include "Weapon/SWeaponModularManager.h"
//#include "Weapon/WeaponModular.h"
#include "Internationalization/Regex.h"
#include "TutorialBase.h"

#include "BuildSystem/BuildPlacedComponent.h"
#include "Build/ItemBuildEntity.h"
#if (UE_GAME || UE_BUILD_SHIPPING || UE_BUILD_TEST || IS_MONOLITHIC)
#include "HttpManager.h"
#endif
using namespace Operation;

ALobbyGameMode::ALobbyGameMode(const class FObjectInitializer& PCIP)
	: Super(PCIP)
	, IsAuthenticationComplete(false)
	, IsMatchMakingPrepareBeginNotified(false)
	, IsMatchMakingPrepareCompleteNotified(false)
	, bUseSteamAuthentication(false)
{
	AGameMode::bPauseable = false;
	PlayerControllerClass = ALobbyPlayerController::StaticClass();

	TextHelper_NotifyValue.SetColor(FColorList::SkyBlue);

	if (!HasAnyFlags(RF_ClassDefaultObject | RF_ArchetypeObject))
	{
		auto TargetLambdaFooterBox = [&]() {
			SMessageBox::Get()->SetButtonsText(FText::GetEmpty(), FButtonNameLocalization::Ok);
			SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton&) { SMessageBox::Get()->ToggleWidget(false); });
			SMessageBox::Get()->SetEnableClose(true);
			SMessageBox::Get()->ToggleWidget(true);
		};

		auto TargetLambdaNoMoney = [&]() {		
			SMessageBox::Get()->SetHeaderText(NSLOCTEXT("All", "All.NoMoney.Title", "No Money"));
			SMessageBox::Get()->SetContent(NSLOCTEXT("All", "All.NoMoney.Desc", "Not enough money for this action."));				
			TargetLambdaFooterBox();
		};


		auto TargetLambdaPlayerWasExist = [&]() {
			SMessageBox::Get()->SetHeaderText(NSLOCTEXT("All", "All.PlayerWasExist.Title", "Player Was Exist"));
			SMessageBox::Get()->SetContent(NSLOCTEXT("All", "All.PlayerWasExist.Desc", "This player is already in one of your contact lists."));
			TargetLambdaFooterBox();
		};

		auto TargetLambdaPlayerNotFound = [&]() {
			SMessageBox::Get()->SetHeaderText(NSLOCTEXT("All", "All.PlayerNotFound.Title", "Player Not Found"));
			SMessageBox::Get()->SetContent(NSLOCTEXT("All", "All.PlayerNotFound.Desc", "Player Not Found."));
			TargetLambdaFooterBox();
		};


		// Errors
		storeController.UnlockItem.OnPaymentRequired.Handler.AddLambda(TargetLambdaNoMoney);
		hangarController.ConfirmItemUpgrade.OnPaymentRequired.Handler.AddLambda(TargetLambdaNoMoney);

		lobbyController.SearchBegin.OnWeaponEquipRequired.Handler.AddLambda([&]() {
			SMessageBox::Get()->SetHeaderText(NSLOCTEXT("All", "All.NoWeapon.Title", "No Weapon"));
			SMessageBox::Get()->SetContent(NSLOCTEXT("All", "All.NoWeapon.Desc", "Not one of the profiles is not equipped weapons."));
			TargetLambdaFooterBox();
		});
		//
		lobbyController.SearchBegin.OnNoAvalibleProfiles.Handler.AddLambda([&]() {
			SMessageBox::Get()->SetHeaderText(NSLOCTEXT("All", "All.NoProfiles.Title", "No Profiles"));
			SMessageBox::Get()->SetContent(NSLOCTEXT("All", "All.NoProfiles.Desc", "Not one of the profiles is not available."));
			TargetLambdaFooterBox();
		});


		lobbyController.SearchBegin.OnMembersNotReady.Handler.AddLambda([&]() {
			SMessageBox::Get()->SetHeaderText(NSLOCTEXT("All", "Squad.MembersNotReady.Title", "Squad not ready"));
			SMessageBox::Get()->SetContent(NSLOCTEXT("All", "Squad.MembersNotReady.Desc", "One of the party members are not ready"));
			TargetLambdaFooterBox();
		});

		lobbyController.SearchBegin.OnSmallPvEMatchLevel.Handler.AddLambda([&]() {
			SMessageBox::Get()->SetHeaderText(NSLOCTEXT("All", "MatchMaking.PvELevelNotOppened.Title", "PvE was locked"));
			SMessageBox::Get()->SetContent(NSLOCTEXT("All", "MatchMaking.PvELevelNotOppened.Desc", "Selected level PvE still not unlocked!"));
			TargetLambdaFooterBox();
		});

		lobbyController.SearchBegin.OnFailed.AddLambda([&]() {
			SMessageBox::Get()->SetHeaderText(NSLOCTEXT("All", "MatchMaking.Failed.Title", "MatchMaking Failed"));
			SMessageBox::Get()->SetContent(NSLOCTEXT("All", "MatchMaking.Failed.Desc", "An unexpected error occurred when searching for a match!"));
			TargetLambdaFooterBox();
		});

		friendController.Add.OnPlayerWasExist.Handler.AddLambda(TargetLambdaPlayerWasExist);
		friendController.Add.OnPlayerNotFound.Handler.AddLambda(TargetLambdaPlayerNotFound);

		identityController.Authentication.OnSuccess.AddUObject(this, &ALobbyGameMode::OnAuthenticationComplete);
		identityController.SteamAuthentication.OnSuccess.AddUObject(this, &ALobbyGameMode::OnAuthenticationComplete);
		identityController.SteamAuthentication.OnSuccess.AddUObject(this, &ALobbyGameMode::OnSteamAuthenticationComplete);
		identityController.SteamAuthentication.OnPaymentRequired.Handler.AddUObject(this, &ALobbyGameMode::OnSteamPaymentRequired);
		
		identityController.SteamAuthentication.OnFailed.AddUObject(this, &ALobbyGameMode::OnCheckServerStatusNotResponsed);

		identityController.RequestClientData.OnSuccess.AddUObject(this, &ALobbyGameMode::OnRequestClientData);
		identityController.RequestClientData.OnFailed.AddUObject(this, &ALobbyGameMode::OnRequestClientDataAttemp);




		identityController.RequestLootData.OnSuccess.AddUObject(this, &ALobbyGameMode::OnRequestRequestLootData);
		identityController.RequestLootData.OnFailed.AddLambda([&]()
		{
			if (identityController.RequestLootData.AttempRequestNumber < 5)
			{
				identityController.RequestLootData.AttempRequestNumber++;
				identityController.RequestLootData.Request();
			}

			SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Authentication", "Authentication.RequestLootData.Title", "Steam Authentication"));
			SMessageBox::Get()->SetContent(NSLOCTEXT("Authentication", "Authentication.RequestLootData.OnFailed", "Failed to load the player's inventory"));
			SMessageBox::Get()->SetButtonsText(FText::GetEmpty(), FButtonNameLocalization::Ok);
			SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton&) { LobbyPlayerController()->ConsoleCommand("quit"); });
			SMessageBox::Get()->SetEnableClose(false);
			SMessageBox::Get()->ToggleWidget(true);
		});

		identityController.RequestPreSetData.OnSuccess.AddUObject(this, &ALobbyGameMode::OnRequestRequestPreSetData);
		identityController.RequestPreSetData.OnFailed.AddLambda([&]()
		{
			if (identityController.RequestPreSetData.AttempRequestNumber < 5)
			{
				identityController.RequestPreSetData.AttempRequestNumber++;
				identityController.RequestPreSetData.Request();
			}

			SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Authentication", "Authentication.RequestPreSetData.Title", "Steam Authentication"));
			SMessageBox::Get()->SetContent(NSLOCTEXT("Authentication", "Authentication.RequestPreSetData.OnFailed", "Failed to load player profiles"));
			SMessageBox::Get()->SetButtonsText(FText::GetEmpty(), FButtonNameLocalization::Ok);
			SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton&) { LobbyPlayerController()->ConsoleCommand("quit"); });
			SMessageBox::Get()->SetEnableClose(false);
			SMessageBox::Get()->ToggleWidget(true);
		});


		identityController.SwitchFraction.OnSuccess.AddUObject(this, &ALobbyGameMode::OnSwitchFractionResponse);

		identityController.SelectFraction.OnSuccess.AddUObject(this, &ALobbyGameMode::OnSelectFractionResponse);
		identityController.SelectFraction.OnFailed.AddLambda([&]()
		{
			SMessageBox::AddQueueMessage("FractionSelector", FOnClickedOutside::CreateLambda([&]() {
				Slate_FractionSelector->ToggleWidget(true);
			}));
		});


		identityController.CheckExistAccount.OnSuccess.AddUObject(this, &ALobbyGameMode::OnCheckExistAccount);
		identityController.CheckExistAccount.OnFailed.AddLambda([&]() 
		{
			if(identityController.CheckExistAccount.AttempRequestNumber < 5)
			{
				identityController.CheckExistAccount.AttempRequestNumber++;
				identityController.CheckExistAccount.Request();
			}

			SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Authentication", "Authentication.CheckExistAccount.Title", "Steam Authentication"));
			SMessageBox::Get()->SetContent(NSLOCTEXT("Authentication", "Authentication.CheckExistAccount.OnFailed", "Unable to check game account attached to Steam"));
			SMessageBox::Get()->SetButtonsText(FText::GetEmpty(), FButtonNameLocalization::Ok);
			SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton&) { LobbyPlayerController()->ConsoleCommand("quit"); });
			SMessageBox::Get()->SetEnableClose(false);
			SMessageBox::Get()->ToggleWidget(true);
		});

		identityController.CreateGameAccount.OnSuccess.AddUObject(this, &ALobbyGameMode::OnCreateGameAccount);
		identityController.CreateGameAccount.OnBadNameRequest.Handler.AddUObject(this, &ALobbyGameMode::OnCreateGameAccountBadResponse);
		identityController.CreateGameAccount.OnFailed.AddUObject(this, &ALobbyGameMode::OnCreateGameAccountBadResponse);

		
		identityController.CheckAccountStatus.OnSuccess.AddUObject(this, &ALobbyGameMode::OnCheckAccountStatusResponse);
		
		identityController.CheckServerStatus.OnSuccess.AddUObject(this, &ALobbyGameMode::OnCheckServerStatusResponse);
		identityController.CheckServerStatus.OnServerNotWorking.Handler.AddUObject(this, &ALobbyGameMode::OnCheckServerStatusNotWorking);
		identityController.CheckServerStatus.OnFailed.AddUObject(this, &ALobbyGameMode::OnCheckServerStatusNotResponsed);

		identityController.OnChangeAccountEmail.OnSuccess.AddUObject(this, &ALobbyGameMode::OnChangeAccountEmailSuccess);
		identityController.OnChangeAccountEmail.OnBadEmailAddresFormat.Handler.AddUObject(this, &ALobbyGameMode::OnBadEmailAddresFormat);
		
		//==========================================================================================================
		identityController.OnCheckEmailConfirmStatus.OnSuccess.AddLambda([&](const FRequestOneParam<bool>& status) 
		{
			if (status.Value)
			{
				OnSuccessEmailConfirmStatus();
			}
		});

		//==========================================================================================================
		identityController.OnConfirmEmailByCode.OnSuccess.AddLambda([&](const FRequestOneParam<bool>& status)
		{
			if (status.Value)
			{
				OnSuccessEmailConfirmStatus();
			}
		});

		identityController.OnConfirmEmailByCode.OnBadEmailAddresFormat.Handler.AddLambda([&]() {
			Slate_MessageBoxInput->SetInputError(NSLOCTEXT("Notify", "Notify.ConfirmEmailBox.Error", "The incorrect code"));
		});
		

		identityController.OnConfirmEmailByCode.OnFailed.AddLambda([&]() {
			SMessageBox::Get()->ToggleWidget(false);
			SMessageBox::Get()->OnMessageBoxHidden.AddLambda([&]() { SMessageBox::RemoveQueueMessage("ConfirmEmailBox"); });
			Slate_NotifyList->AddNotify(FNotifyInfo(NSLOCTEXT("Notify", "Notify.OnConfirmEmailByCode.OnFailed", "Failed to confirm Email address. An error on a server, try again later or report a problem in one of the steam discussions")));
		});

		//==========================================================================================================
		identityController.OnCheckEmailConfirmStatus.OnFailed.AddLambda([&]() {
			Slate_NotifyList->AddNotify(FNotifyInfo(NSLOCTEXT("Notify", "Notify.OnChangeAccountEmail.OnFailed", "An error occurred while attempting to change the Email address. Please try again later or write about an error in one of the discussions.")));
		});

		identityController.OnChangeAccountEmail.OnFailed.AddLambda([&]() {
			Slate_NotifyList->AddNotify(FNotifyInfo(NSLOCTEXT("Notify", "Notify.OnChangeAccountEmail.OnFailed", "An error occurred while attempting to change the Email address. Please try again later or write about an error in one of the discussions.")));
		});

		identityController.OnChangeAccountEmail.OnPlayerEmailWasNotConfirmed.Handler.AddLambda([&](const FRequestOneParam<FString>& email) {
			Slate_NotifyList->AddNotify(FNotifyInfo(FText::Format(NSLOCTEXT("Notify", "Notify.OnChangeAccountEmail.OnPlayerEmailWasNotConfirmed", "Email {0} is already attached to your account. On your mail sent the code to confirm."), FText::FromString(email.Value))));
			OnChangeAccountEmailSuccess(email);
		});

		identityController.OnChangeAccountEmail.OnPlayerEmailWasAlreadyConfirmed.Handler.AddLambda([&](const FRequestOneParam<FString>& email) {
			Slate_NotifyList->AddNotify(FNotifyInfo(FText::Format(NSLOCTEXT("Notify", "Notify.OnChangeAccountEmail.OnPlayerEmailWasNotConfirmed", "Email {0} is already attached to your account."), FText::FromString(email.Value))));
			OnChangeAccountEmailSuccess(email);
		});

		identityController.OnChangeAccountEmail.OnExistEmailAndAlreadyConfirmed.Handler.AddLambda([&](const FRequestOneParam<FString>& email) {
			Slate_MessageBoxInput->SetInputError(NSLOCTEXT("Notify", "Notify.EmailBox.Error", "Email is already in use by another user. Attaching address confirmed."));
		});

		identityController.OnChangeAccountEmail.OnExistEmailAndNotConfirmed.Handler.AddLambda([&](const FRequestOneParam<FString>& email) {
			Slate_MessageBoxInput->SetInputError(NSLOCTEXT("Notify", "Notify.EmailBox.Error", "Email is already in use by another user. Attaching address in process."));
		});
		identityController.OnStartTutorial.OnSuccess.AddUObject(this, &ALobbyGameMode::OnStartTutorial);


		storeController.UnlockItem.OnSuccess.AddUObject(this, &ALobbyGameMode::OnUnlockItem);
		storeController.UnlockProfile.OnSuccess.AddUObject(this, &ALobbyGameMode::OnUnlockProfile);



		lobbyController.SearchStatus.OnSuccess.AddUObject(this, &ALobbyGameMode::OnSessionMatchFounded);
		lobbyController.SearchStatus.OnFailed.AddUObject(this, &ALobbyGameMode::OnSessionNotAuthorized);
		lobbyController.GameSessionContinue.OnSuccess.AddUObject(this, &ALobbyGameMode::TravelToGame);


		BindSquadHandlers();
		BindPromoHandlers();
		BindSocialTaskHandlers();


		SteamLoginAttempTimerDelegate.BindUObject(this, &ALobbyGameMode::OnSteamLoginAttemp);
		OnSteamLoginCompleteDelegate.BindUObject(this, &ALobbyGameMode::OnSteamLoginComplete);





		lobbyController.GetMatchResults.OnSuccess.AddUObject(this, &ALobbyGameMode::OnGetMatchResults);

		{
			marketingController.AddLobbyStatistic.OnSuccess.AddUObject(this, &ALobbyGameMode::OnSuccessAddLobbyStatistic);
			marketingController.RequestImprovementsList.OnSuccess.AddUObject(this, &ALobbyGameMode::OnRequestImprovementsListResponse);

			//=========================================================================================================
			marketingController.VoteImprovement.OnSuccess.AddUObject(this, &ALobbyGameMode::OnVoteImprovementResponse);
			marketingController.VoteImprovement.OnNotFound.Handler.AddLambda([&]()
			{
				// �� ������� ����� ��������� ���������..
				Slate_NotifyList->AddNotify(FNotifyInfo(NSLOCTEXT("Notify", "Notify.VoteImprovement.OnNotFound", "Could not find the selected improvement.")));
			});	

			marketingController.VoteImprovement.OnVoteIsClosed.Handler.AddLambda([&]()
			{
				//������ ��������� �� ��������� � �����������, �������� ��� ��� � ���������� ��� ����� ������������.
				Slate_NotifyList->AddNotify(FNotifyInfo(NSLOCTEXT("Notify", "Notify.VoteImprovement.OnVoteIsClosed", "This improvement does not participate in the vote, it is already possible to develop or for a long time to implement.")));
			}); 

			marketingController.VoteImprovement.OnAlreadyVoted.Handler.AddLambda([&]()
			{
				//�� ������ ���������� �� ���� ��� ���� ��� � ������..
				Slate_NotifyList->AddNotify(FNotifyInfo(NSLOCTEXT("Notify", "Notify.VoteImprovement.OnAlreadyVoted", "You can not vote more than once a week.")));
			});	

			marketingController.VoteImprovement.OnNotEnoughMoney.Handler.AddLambda([&]()
			{
				//��� ����������� �� ��������� ���������� ����� 10 ����������..
				Slate_NotifyList->AddNotify(FNotifyInfo(NSLOCTEXT("Notify", "Notify.VoteImprovement.OnNotEnoughMoney", "To vote for an improvement, you must have 10 crystals.")));
			}); 

			//=========================================================================================================
			marketingController.VoteSentence.OnSuccess.AddLambda([&]()
			{
				// ��� ����� ������.
				Slate_NotifyList->AddNotify(FNotifyInfo(NSLOCTEXT("Notify", "Notify.VoteSentence.OnSuccess", "Your vote has been accepted.")));
			});	

			marketingController.VoteSentence.OnNotFound.Handler.AddLambda([&]()
			{
				// �� ������� ����� ��������� ���������..
				Slate_NotifyList->AddNotify(FNotifyInfo(NSLOCTEXT("Notify", "Notify.VoteSentence.OnNotFound", "Could not find the selected idea.")));
			});	

			marketingController.VoteSentence.OnVoteIsClosed.Handler.AddLambda([&]()
			{
				//������ ��������� �� ��������� � �����������, �������� ��� ��� � ���������� ��� ����� ������������.
				Slate_NotifyList->AddNotify(FNotifyInfo(NSLOCTEXT("Notify", "Notify.VoteSentence.OnVoteIsClosed", "This improvement does not participate in the vote, it is already possible to develop or for a long time to implement.")));
			}); 

			marketingController.VoteSentence.OnAlreadyVoted.Handler.AddLambda([&]()
			{
				//�� ��� ������������� �� ������ ����
				Slate_NotifyList->AddNotify(FNotifyInfo(NSLOCTEXT("Notify", "Notify.VoteSentence.OnAlreadyVoted", "You have already voted for this idea")));
			});

			//=========================================================================================================
			marketingController.PushSentence.OnSuccess.AddLambda([&]()
			{
				//	���� ���� ��������� � ������ ��� �����������.
				Slate_NotifyList->AddNotify(FNotifyInfo(NSLOCTEXT("Notify", "Notify.PushSentence.OnSuccess", "Your idea is added to the voting list.")));
			});
			
			marketingController.PushSentence.OnTimeOut.Handler.AddLambda([&]()
			{
				//�� ������ ���������� ���� �� ���� ��� ��� � ���.
				Slate_NotifyList->AddNotify(FNotifyInfo(NSLOCTEXT("Notify", "Notify.PushSentence.OnTimeOut", "You can send ideas are not more often than once per hour.")));
			});

			marketingController.PushSentence.OnNotEnoughMoney.Handler.AddLambda([&]()
			{
				//��� ����������� �� ��������� ���������� ����� 10 ����������..
				Slate_NotifyList->AddNotify(FNotifyInfo(NSLOCTEXT("Notify", "Notify.PushSentence.OnNotEnoughMoney", "To vote for an improvement, you must have 10 crystals.")));
			}); 

			//=========================================================================================================
			AddLobbyStatisticTimerDelegate.BindUObject(this, &ALobbyGameMode::AddLobbyStatisticProxy);
		}

		buildingController.GetListOfBuildings.OnSuccess.AddUObject(this, &ALobbyGameMode::OnBuildingsList);

		InitializeSlateWidgets();
	}
}

void ALobbyGameMode::OnSuccessEmailConfirmStatus()
{
	SMessageBox::Get()->ToggleWidget(false);
	SMessageBox::Get()->OnMessageBoxHidden.AddLambda([&]() { SMessageBox::RemoveQueueMessage("ConfirmEmailBox"); });
	Slate_NotifyList->AddNotify(FNotifyInfo(NSLOCTEXT("Notify", "Notify.OnCheckEmailConfirmStatus.OnSuccess", "Your Email address successfully confirmed. Have a good game!")));
	identityController.OnCheckEmailConfirmStatus.TimerStop(GetWorldTimerManager());
}

void ALobbyGameMode::AddLobbyStatisticProxy()
{
	if (LobbyActionStatistic.Num())
	{
		const auto container = FLobbyActionContainer(LobbyActionStatistic);
		marketingController.AddLobbyStatistic.Request(container);
		LobbyActionStatisticInProcess.Add(container.RequestId, container);
		LobbyActionStatistic.Empty();
	}
}

void ALobbyGameMode::OnSuccessAddLobbyStatistic(const FRequestOneParam<FString>& requestId)
{
	LobbyActionStatisticInProcess.Remove(requestId);
}


//void ALobbyGameMode::StartToLeaveMap()
//{
//	//GEngine->GameViewport->RemoveViewportWidgetContent(Slate_LobbyContainer->AsShared());
//
//#if WITH_EDITOR == 0
//	GEngine->GameViewport->RemoveAllViewportWidgets();
//#endif
//	Super::StartToLeaveMap();
//}

void ALobbyGameMode::OnSteamPaymentRequired()
{
	SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Authentication", "Authentication.Steam.Title", "Steam Authentication"));
	SMessageBox::Get()->SetContent(NSLOCTEXT("Authentication", "Authentication.Steam.NotPayment", "Authentication error, this game not purchased from steam."));
	SMessageBox::Get()->SetButtonsText(FText::GetEmpty(), FButtonNameLocalization::Ok);
	SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton&) { LobbyPlayerController()->ConsoleCommand("quit"); });
	SMessageBox::Get()->SetEnableClose(false);
	SMessageBox::Get()->ToggleWidget(true);
}

void ALobbyGameMode::OnSteamAuthenticationComplete(const Operation::Authentication::Response & response)
{
	if (response.Error.Num() || response.Token.IsEmpty())
	{
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Authentication", "Authentication.Steam.Title", "Steam Authentication"));
		SMessageBox::Get()->SetContent(FText::FromString(response.Error.Top()));
		SMessageBox::Get()->SetButtonsText(FText::GetEmpty(), NSLOCTEXT("All", "All.Quit", "Quit from game"));
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton&) { LobbyPlayerController()->ConsoleCommand("quit"); });
		SMessageBox::Get()->SetEnableClose(false);
		SMessageBox::Get()->ToggleWidget(true);
	}
	else
	{
		identityController.SteamAuthentication.TimerStop(GetWorldTimerManager());
		SMessageBox::Get()->ToggleWidget(false);
	}
}

void ALobbyGameMode::OnAuthenticationComplete(const Authentication::Response& response) const
{
	if(response.Error.Num() || response.Token.IsEmpty())
	{
		for(auto error : response.Error)
		{
			Slate_AuthForm->OnErrorMessage(FText::FromString(error));
		}
	}
	else
	{
		IOperationContainer::UserToken = response.Token;
		identityController.CheckExistAccount.Request();
	}
}
void ALobbyGameMode::OnSelectFractionResponse(const FFractionReputation& fraction)
{
	OnRequestClientDataComplete();
	UpdateFractionProgress(fraction);
	OnSwitchFractionResponse(FRequestOneParam<int32>(fraction.Id));
}

//===================================================================================================


//===================================================================================================

void ALobbyGameMode::OnStatusOfSocialTaskResponse(const FSocialTaskStatusResponse& response)
{
	if (response.ActiveTaskCount == 0) promoController.StatusOfSocialTask.TimerStop(GetWorldTimerManager());
	if (response.ActiveTaskCount== response.CompletedTasks.Num()) promoController.StatusOfSocialTask.TimerStop(GetWorldTimerManager());
	/*for (auto task : response.CompletedTasks)
	{
		auto queueId = FName(*FString::Printf(TEXT("SocialTask.Confirmed_%d"), static_cast<int32>(task.SocialGroupId)));
		auto desc = FText::Format(
			NSLOCTEXT("Notify", "Notify.OnStatusOfSocialTaskResponse.Desc", "Thank you for your review in the {0}. Your reward: {1} money"),
			FText::FromString(GetEnumValueToString<SocialGroupId::Type>("SocialGroupId", static_cast<SocialGroupId::Type>(task.SocialGroupId))),
			FText::AsNumber(task.Award.Amount));

		auto title = FText::Format(
			NSLOCTEXT("Notify", "Notify.OnStatusOfSocialTaskResponse.Title", "Reviewed in {0}"),
			FText::FromString(GetEnumValueToString<SocialGroupId::Type>("SocialGroupId", static_cast<SocialGroupId::Type>(task.SocialGroupId)))
		);
		  
		SMessageBox::AddQueueMessage(queueId, FOnClickedOutside::CreateLambda([&, HeaderText = title, Content = desc, Id = queueId]() {
			SMessageBox::Get()->SetHeaderText(HeaderText);
			SMessageBox::Get()->SetContent(Content);


			SMessageBox::Get()->SetButtonsText(FText::GetEmpty(), FButtonNameLocalization::Continue);
			SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton) {
				SMessageBox::Get()->ToggleWidget(false);
				SMessageBox::Get()->OnMessageBoxHidden.AddLambda([&]() { SMessageBox::RemoveQueueMessage(Id); });
			});
			SMessageBox::Get()->SetEnableClose(false);
			SMessageBox::Get()->ToggleWidget(true);
		}));

		Slate_NotifyList->AddNotify(FNotifyInfo(desc, title));
	}*/
}

void ALobbyGameMode::OnListOfSocialTaskResponse(const TArray<FSocialTaskWrapper>& tasks)
{
	if (Slate_GeneralPage.IsValid())
	{
		promoController.ListOfSocialTask.TimerStop(GetWorldTimerManager());

		for (auto t : tasks)
		{
			Slate_GeneralPage->AddSocialTask(t);
		}
	}
}

void ALobbyGameMode::OnTakeSocialTaskResponse(const FRequestOneParam<FString>& response)
{
	FPlatformProcess::LaunchURL(*response.Value, nullptr, nullptr);

	//SMessageBox::AddQueueMessage("SocialTask.Succes", FOnClickedOutside::CreateLambda([&]() {
	//	SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "Notify.SocialTask.Title", "Social Task"));
	//	SMessageBox::Get()->SetContent(NSLOCTEXT("Notify", "Notify.SocialTask.Succes.Desc", "Hand Held approval, within 10 minutes you will get your reward. After 10 minutes, the result will come with a bonus in the form of money"));
	//	SMessageBox::Get()->SetButtonsText(FText::GetEmpty(), FButtonNameLocalization::Continue);
	//	SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton) {
	//		SMessageBox::Get()->ToggleWidget(false);
	//		SMessageBox::Get()->OnMessageBoxHidden.AddLambda([&]() { SMessageBox::RemoveQueueMessage("SocialTask.Succes"); });
	//	});
	//	SMessageBox::Get()->SetEnableClose(false);
	//	SMessageBox::Get()->ToggleWidget(true);
	//}));
}

void ALobbyGameMode::OnTakeSocialTaskNotFound()
{
	SMessageBox::AddQueueMessage("SocialTask.NotFound", FOnClickedOutside::CreateLambda([&]() {
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "Notify.SocialTask.Title", "Social Task"));
		SMessageBox::Get()->SetContent(NSLOCTEXT("Notify", "Notify.SocialTask.NotFound.Desc", "Task was not found"));
		SMessageBox::Get()->SetButtonsText(FText::GetEmpty(), FButtonNameLocalization::Continue);
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton) {
			SMessageBox::Get()->ToggleWidget(false);
			SMessageBox::Get()->OnMessageBoxHidden.AddLambda([&]() { SMessageBox::RemoveQueueMessage("SocialTask.NotFound"); });
		});
		SMessageBox::Get()->SetEnableClose(false);
		SMessageBox::Get()->ToggleWidget(true);
	}));
}

void ALobbyGameMode::OnTakeSocialTaskActivated()
{
	SMessageBox::AddQueueMessage("SocialTask.Activated", FOnClickedOutside::CreateLambda([&]() {
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "Notify.SocialTask.Title", "Social Task"));
		SMessageBox::Get()->SetContent(NSLOCTEXT("Notify", "Notify.SocialTask.Activated.Desc", "Task was are completed"));
		SMessageBox::Get()->SetButtonsText(FText::GetEmpty(), FButtonNameLocalization::Continue);
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton) {
			SMessageBox::Get()->ToggleWidget(false);
			SMessageBox::Get()->OnMessageBoxHidden.AddLambda([&]() { SMessageBox::RemoveQueueMessage("SocialTask.Activated"); });
		});
		SMessageBox::Get()->SetEnableClose(false);
		SMessageBox::Get()->ToggleWidget(true);
	}));
}

//===================================================================================================

void ALobbyGameMode::OnGetMatchResults(const TArray<FLobbyMatchResultPreview>& results)
{
	for (auto result : results)
	{
		const auto GameMapName = FGameMapTypeId::GetText(result.GameMapTypeId);
		const auto GameModeName =
			result.GameModeTypeId == EGameModeTypeId::Attack
			? FText::FromString(result.UniversalName)
			: FGameModeTypeId::GetText(result.GameModeTypeId);

		const auto Victory = NSLOCTEXT("Notify", "Notify.Victory", "Victory");
		const auto Defeat = NSLOCTEXT("Notify", "Notify.Defeat", "Defeat");
			
		const auto WinState = result.IsWin ? Victory : Defeat;


		Slate_NotifyList->AddNotify(FNotifyInfo(FText::Format(
			NSLOCTEXT("Notify", "Notify.OnGetMatchResults.Desc", "\t\tReward\nMoney: {0} Hexagon: {3}\nExperience: {1} \nReputation: {2}"), 
			FRichTextHelpers::GetResourse(FRichTextHelpers::Money, result.Award.Money), 
			TextHelper_NotifyValue.SetValue(FText::AsNumber(result.Award.Experience)).ToText(), 
			TextHelper_NotifyValue.SetValue(FText::AsNumber(result.Award.Reputation)).ToText(),
			FRichTextHelpers::GetResourse(FRichTextHelpers::Donate, result.Award.Donate)
		),
			
			FText::Format(
				NSLOCTEXT("Notify", "Notify.OnGetMatchResults.Title", "{0} {1} {2}"),
				WinState, GameModeName, GameMapName)		
		));
	}

	if (results.Num())
	{
		const auto fraction = InstanceRepository->Fraction[FPlayerEntityInfo::Instance.FractionTypeId];

		Slate_NotifyList->AddNotify(
			FNotifyInfo(FText::Format
			(
				NSLOCTEXT("Notify", "Notify.OnGetMatchResults.Desc", "For every battle you gain respect in the fraction. Each new level rank provides access to new items( weapons, armor and kits). You needed to get another {0} reputation in the fraction {1} to the new rank. \n\rYou can change the faction at any time, go to the store and choose another faction."),
				fraction->Progress.NextReputation - fraction->Progress.Reputation,
				fraction->GetDescription().Name
			),
			NSLOCTEXT("Notify", "Notify.GameGuide.Fraction.Title", "Fraction guide in game")
		));
	}
}

//===================================================================================================

void ALobbyGameMode::OnSwitchFractionResponse(const FRequestOneParam<int32>& fractionId)
{
	auto FractionId = static_cast<FractionTypeId::Type>(fractionId.Value);

	FPlayerEntityInfo::Instance.FractionTypeId = FractionId;
	Slate_FractionSelector->ToggleWidget(false);
	SMessageBox::RemoveQueueMessage("FractionSelector");

	FText Name = FText::GetEmpty();

	if (FractionId >= 0 && FractionId < FractionTypeId::End)
	{
		Name = InstanceRepository->Fraction[FPlayerEntityInfo::Instance.FractionTypeId]->GetDescription().Name;
	}	

	Slate_NotifyList->AddNotify(FNotifyInfo(FText::Format(NSLOCTEXT("Notify", "Notify.OnSwitchFraction.Desc", "You join to fraction: {0}"), TextHelper_NotifyValue.SetValue(Name).ToText()), NSLOCTEXT("Notify", "Notify.OnSwitchFraction.Title", "Switch Fraction")));
}

void ALobbyGameMode::OnCheckAccountStatusResponse(const FAccountStatusContainer& response)
{

	const auto currentDate = FDateTime::UtcNow().ToUnixTimestamp();
	for (const auto service : response.UsingService)
	{
		if (service.ExpirationDate <= currentDate && service.NotifyDate < service.ExpirationDate)
		{
			const auto ExpirationDate = FDateTime::FromUnixTimestamp(service.ExpirationDate);

			if (service.ServiceTypeId == ServiceTypeId::PremiumAccount)
			{
				Slate_NotifyList->AddNotify(FNotifyInfo(FText::Format(NSLOCTEXT("Notify", "Notify.EndDurationOfPremium", "Duration of Premium account is over {0}"), FText::AsDateTime(ExpirationDate)), NSLOCTEXT("Notify", "Notify.PremiumServices.Title", "Premium services")));
			}
			else if (service.ServiceTypeId == ServiceTypeId::BoosterOfExperience)
			{
				Slate_NotifyList->AddNotify(FNotifyInfo(FText::Format(NSLOCTEXT("Notify", "Notify.EndDurationOfBoosterOfExperience", "Duration of Booster Of Experience is over {0}"), FText::AsDateTime(ExpirationDate)), NSLOCTEXT("Notify", "Notify.PremiumServices.Title", "Premium services")));
			}
			else if (service.ServiceTypeId == ServiceTypeId::BoosterOfMoney)
			{
				Slate_NotifyList->AddNotify(FNotifyInfo(FText::Format(NSLOCTEXT("Notify", "Notify.EndDurationOfBoosterOfMoney", "Duration of Booster Of Money is over {0}"), FText::AsDateTime(ExpirationDate)), NSLOCTEXT("Notify", "Notify.PremiumServices.Title", "Premium services")));
			}
			else if (service.ServiceTypeId == ServiceTypeId::BoosterOfReputation)
			{
				Slate_NotifyList->AddNotify(FNotifyInfo(FText::Format(NSLOCTEXT("Notify", "Notify.EndDurationOfBoosterOfReputation", "Duration of Booster Of Reputation is over {0}"), FText::AsDateTime(ExpirationDate)), NSLOCTEXT("Notify", "Notify.PremiumServices.Title", "Premium services")));
			}
		}
	}


	if (FPlayerEntityInfo::Instance.Raiting.Position != response.Raiting.Position)
	{
		Slate_NotifyList->AddNotify(FNotifyInfo(FText::Format(NSLOCTEXT("Notify", "Notify.OnChangeRatinPosition.Desc", "The moved from {0} place in the tournament rating by {1}, you just {2} points"),
			TextHelper_NotifyValue.SetValue(FText::AsNumber(FPlayerEntityInfo::Instance.Raiting.Position)).ToText(),
			TextHelper_NotifyValue.SetValue(FText::AsNumber(response.Raiting.Position)).ToText(),
			TextHelper_NotifyValue.SetValue(FText::AsNumber(response.Raiting.Experience)).ToText()),
			NSLOCTEXT("Notify", "Notify.OnChangeRatinPosition.Title", "Tournament News")));
	}

	if (FPlayerEntityInfo::Instance.Experience.Level != response.Experience.Level)
	{
		Slate_NotifyList->AddNotify(FNotifyInfo(FText::Format(NSLOCTEXT("Notify", "Notify.OnChangeAccountLevel.Desc", "You got {0} account level."),
			TextHelper_NotifyValue.SetValue(FText::AsNumber(response.Experience.Level)).ToText()),
			NSLOCTEXT("Notify", "Notify.OnChangeAccountLevel.Title", "Level up!")));
	}

	FPlayerEntityInfo::Instance.IsSquadLeader = response.IsSquadLeader;
	FPlayerEntityInfo::Instance.Cash = response.Cash;
	FPlayerEntityInfo::Instance.Experience = response.Experience;
	FPlayerEntityInfo::Instance.FractionExperience = response.FractionExperience;
	FPlayerEntityInfo::Instance.Raiting = response.Raiting;
	FPlayerEntityInfo::Instance.UsingService = response.UsingService;
	FPlayerEntityInfo::Instance.IsAllowVoteImprovement = response.IsAllowVoteImprovement;
	FPlayerEntityInfo::Instance.DateOfNextAvailableProposal = response.DateOfNextAvailableProposal;
	FPlayerEntityInfo::Instance.Phone = response.Phone;
	FPlayerEntityInfo::Instance.Email = response.Email;
	FPlayerEntityInfo::Instance.TutorialId = response.TutorialId;
	FPlayerEntityInfo::Instance.Tutorials = response.Tutorials;	
}

void ALobbyGameMode::ShowEnterEmailConfirmCodeBox(const FRequestOneParam<FString>& response)
{
	SMessageBox::Get()->ToggleWidget(false);
	SMessageBox::Get()->OnMessageBoxHidden.AddLambda([&]() { SMessageBox::RemoveQueueMessage("EmailBox"); });
	identityController.OnCheckEmailConfirmStatus.TimerStart(GetWorldTimerManager());

	SMessageBox::AddQueueMessage("ConfirmEmailBox", FOnClickedOutside::CreateLambda([&, email = response.Value]()
	{
		//	�� ���� ����� ({0}) ��� ��������� ��� �������������.
		Slate_MessageBoxInput->SetContent(FText::Format(NSLOCTEXT("Notify", "Notify.ConfirmEmailBox.Desc", "On your email {0} has been sent to the confirmation code. Click the received link or enter the code below. If you make a mistake in the E-mail address, go back and try again."), FText::FromString(email)));
		Slate_MessageBoxInput->SetInputText(FText::GetEmpty());

		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "Notify.ConfirmEmailBox.Title", "Confirm Email"));
		SMessageBox::Get()->SetContent(Slate_MessageBoxInput.ToSharedRef());
		SMessageBox::Get()->SetButtonsText(FButtonNameLocalization::Next, FButtonNameLocalization::Back);
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton InButton) {
			if (InButton == EMessageBoxButton::Left)
			{
				FString InputString = Slate_MessageBoxInput->GetInputText().ToString();

				FRegexPattern RegexPattern("^([0-9]{4,8})$");
				if (FRegexMatcher(RegexPattern, InputString).FindNext())
				{
					identityController.OnConfirmEmailByCode.Request(InputString);
				}
				else
				{
					Slate_MessageBoxInput->SetInputError(NSLOCTEXT("Notify", "Notify.ConfirmEmailBox.Error", "Is incorrect, code consists of 6 digits"));
				}
			}
			else
			{
				SMessageBox::Get()->ToggleWidget(false);
				SMessageBox::Get()->OnMessageBoxHidden.AddLambda([&]() { SMessageBox::RemoveQueueMessage("ConfirmEmailBox"); });
				ShowEnterEmailBox();
			}
		});
		SMessageBox::Get()->SetEnableClose(false);
		SMessageBox::Get()->ToggleWidget(true);
	}));
}


void ALobbyGameMode::OnChangeAccountEmailSuccess(const FRequestOneParam<FString>& response)
{
	ShowEnterEmailConfirmCodeBox(response);
}

void ALobbyGameMode::OnBadEmailAddresFormat() const
{
	Slate_MessageBoxInput->SetInputError(NSLOCTEXT("Notify", "Notify.EmailBox.Error", "E-Mail address is incorrect, try again."));
}

void ALobbyGameMode::ShowEnterEmailBox()
{

	SMessageBox::AddQueueMessage("EmailBox", FOnClickedOutside::CreateLambda([&]()
	{
		Slate_MessageBoxInput->SetContent(NSLOCTEXT("Notify", "Notify.EmailBox.Desc", "Thank you for your interest in this game, please enter your E-Mail address to get access to all game features and additional rewards."));
		Slate_MessageBoxInput->SetInputText(FText::GetEmpty());

		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "Notify.EmailBox.Title", "E-Mail"));
		SMessageBox::Get()->SetContent(Slate_MessageBoxInput.ToSharedRef());
		SMessageBox::Get()->SetButtonsText(FButtonNameLocalization::Next);
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton InButton) 
		{
			FString InputString = Slate_MessageBoxInput->GetInputText().ToString();

			FRegexPattern RegexPattern("^([A-Za-z0-9\\-\\.\\_]{3,20})@([A-Za-z0-9]{3,10})\\.([A-Za-z]{2,3})$");
			if (FRegexMatcher(RegexPattern, InputString).FindNext())
			{
				identityController.OnChangeAccountEmail.Request(InputString);
			}
			else
			{
				OnBadEmailAddresFormat();
			}
		});
		SMessageBox::Get()->SetEnableClose(false);
		SMessageBox::Get()->ToggleWidget(true);
	}));
}

void ALobbyGameMode::OnRequestClientDataComplete()
{
	identityController.RequestLootData.Request();
	buildingController.GetListOfBuildings.Request();
	identityController.CheckAccountStatus.TimerStart(GetWorldTimerManager());

	lobbyController.GetMatchResults.TimerStart(GetWorldTimerManager());
	promoController.ListOfSocialTask.TimerStart(GetWorldTimerManager());
	promoController.StatusOfSocialTask.TimerStart(GetWorldTimerManager());

	squadController.GetSqaudInformation.TimerStart(GetWorldTimerManager());
	squadController.GetSquadInvites.TimerStart(GetWorldTimerManager());
	marketingController.RequestImprovementsList.TimerStart(GetWorldTimerManager());

	//if (FPlayerEntityInfo::Instance.Email.Value.IsEmpty() || FPlayerEntityInfo::Instance.Email.IsConfirmed == false)
	//{
	//	ShowEnterEmailBox();
	//}

	Slate_AuthForm->ToggleWidget(false);
	//SMessageBox::Get()->ToggleWidget(false);

#if !WITH_EDITOR
	auto GameInst = Cast<UShooterGameInstance>(GetGameInstance());
	auto SettingsInstance = Cast<UUTGameUserSettings>(UGameUserSettings::GetGameUserSettings());
	if ((GameInst && SettingsInstance && SettingsInstance->IsLoadingBuildingMode() && GameInst->StartMessages.bIsTournamentRulesShowed == false) == false)
	{
		Slate_LobbyContainer->ToggleWidget(true);
	}
#else
	Slate_LobbyContainer->ToggleWidget(true);
#endif

	identityController.ChangeRegionAccount.Request(FChangeRegionAccountView(UUTGameUserSettings::GetCurrentLanguageTwoISO()));	
}
/////
void ALobbyGameMode::UpdateFractionProgress(const FFractionReputation& fraction)
{
	if (fraction.Id >= 0 && fraction.Id < FractionTypeId::End)
	{
		auto instance = InstanceRepository->Fraction[fraction.Id];
		if (instance)
		{

			instance->Progress = fraction;
			if (fraction.Id == FractionTypeId::RIFT_Friend)
			{
				instance->Progress.CurrentRank = 10;
			}

			if (fraction.CurrentRank > fraction.LastRank)
			{
				SMessageBox::AddQueueMessage("FractionLevelUp", FOnClickedOutside::CreateLambda([&, f = fraction, inst = instance]() {
					const auto AllowItems = inst->GetItemsByLevel(FMath::Clamp<int32>(f.CurrentRank - 1, 0, inst->RankList.Num()), false);
					auto RankName = inst->RankList[FMath::Clamp(f.CurrentRank - 1, 0, inst->RankList.Num() - 1)].Name;

					auto ScrollBox = SNew(SScrollBox)
						.Orientation(Orient_Horizontal)
						.ScrollBarStyle(&FLokaSlateStyle::Get().GetWidgetStyle<FScrollBarStyle>("Default_ScrollBar")).ScrollBarThickness(FVector2D(4, 4));

					for (auto &i : AllowItems)
					{
						if (i->GetSkeletalMesh())
						{
							ScrollBox->AddSlot()
								[
									SNew(SFractionManager_Item, i)
									.OnClickedItem_Raw(&storeController.UnlockItem, &UnlockItem::Operation::Request)
									.IsEnabled(true)
								];
						}
					}

					SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "Notify.OnFractionLevelUp.Title", "Fraction Level Up"));
					SMessageBox::Get()->SetContent(
						SNew(SVerticalBox)
						+ SVerticalBox::Slot()
						.AutoHeight()
						.Padding(FMargin(20, 18))
						[
							SNew(STextBlock)
							.Text(FText::Format(NSLOCTEXT("Notify", "Notify.OnFractionLevelUp.GoShopDesc", "Your rank is now \"{0}\", you have become available for purchase the following items"), TextHelper_NotifyValue.SetValue(RankName).ToText()))
							.TextStyle(&FLokaSlateStyle::Get().GetWidgetStyle<FGeneralPageStyle>("SGeneralPageStyle").YourPlaceText)
							.Justification(ETextJustify::Center)
						]
						+ SVerticalBox::Slot()
						.AutoHeight()
						[
							ScrollBox
						]
					);
					SMessageBox::Get()->SetButtonsText(NSLOCTEXT("Notify", "Notify.OnFractionLevelUp.GoShop", "INTO SHOP"), FText::GetEmpty(), FButtonNameLocalization::Continue);
					SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton b) { 
						if (b == EMessageBoxButton::Left)
						{
							Slate_LobbyContainer->SetActiveScrollablyWidget(ELobbyActionButton::Store);
						}					
						SMessageBox::Get()->ToggleWidget(false);
						SMessageBox::Get()->OnMessageBoxHidden.AddLambda([&]() { SMessageBox::RemoveQueueMessage("FractionLevelUp"); });						
					});
					SMessageBox::Get()->SetEnableClose(false);
					SMessageBox::Get()->ToggleWidget(true);

					Slate_NotifyList->AddNotify(FNotifyInfo(FText::Format(NSLOCTEXT("Notify", "Notify.OnFractionLevelUp.Desc", "Your current rank now: {0}"), TextHelper_NotifyValue.SetValue(RankName).ToText()), NSLOCTEXT("Notify", "Notify.OnFractionLevelUp.Title", "Fraction Level Up")));
				}));				
			}
		}
		else ensureAlwaysMsgf(instance, TEXT("Fraction was nullptr at %d [%s]"), static_cast<int32>(fraction.Id), *GetEnumValueToString("FractionTypeId", fraction.Id));
	}
	else ensureAlwaysMsgf(false, TEXT("Invalid FractionTypeId: %d of %d"), static_cast<int32>(fraction.Id), static_cast<int32>(FractionTypeId::End));
}

void ALobbyGameMode::OnQueryAchievementsComplete(const FUniqueNetId& playerId, const bool isSu)
{
	if (GetSteamSubsystem())
	{
		//	Writer container
		FOnlineAchievementsWrite* achievementWriter = new FOnlineAchievementsWrite();

		//	Write player achievements
		for (auto a : FPlayerEntityInfo::Instance.Archievements)
		{
			achievementWriter->SetIntStat(*GetEnumValueToString<ItemAchievementTypeId::Type>("ItemAchievementTypeId", a.Id), a.Amount);
		}

		//	Share Writer container
		auto sharedAchievementWriter = FOnlineAchievementsWritePtr(achievementWriter).ToSharedRef();
		GetSteamSubsystem()->GetAchievementsInterface()->WriteAchievements(*GetSteamSubsystem()->GetIdentityInterface()->GetUniquePlayerId(0), sharedAchievementWriter);
	}
}

void ALobbyGameMode::OnRequestClientData(const FPlayerEntityInfo& response)
{
	IsAuthenticationComplete = true;
	lobbyController.GetPlayerRatingTop.TimerStart(GetWorldTimerManager());
	LobbyPlayerController()->SetEntityInfo(response);
	FGenericPlatformMisc::SetEpicAccountId(response.PlayerId);
	GetWorld()->GetFirstPlayerController()->PlayerState->SetPlayerName(response.Login);

	for (auto a : response.Archievements)
	{
		auto inst = InstanceRepository->Achievement[a.Id];
		if (inst)
		{
			inst->CurrentProgress = a.Amount;
		}
	}

	if (auto steam = GetSteamSubsystem())
	{
		FOnQueryAchievementsCompleteDelegate QueryAchievementsCompleteDelegate;
		QueryAchievementsCompleteDelegate.BindUObject(this, &ALobbyGameMode::OnQueryAchievementsComplete);
		steam->GetAchievementsInterface()->QueryAchievements(*GetSteamSubsystem()->GetIdentityInterface()->GetUniquePlayerId(0), QueryAchievementsCompleteDelegate);
	}


	if (response.Experience.Level > response.Experience.LastLevel)
	{		
		Slate_NotifyList->AddNotify(FNotifyInfo(FText::Format(NSLOCTEXT("Notify", "Notify.OnAccountLevelUp.Desc", "Your current level now: {0}"), TextHelper_NotifyValue.SetValue(FText::AsNumber(response.Experience.Level)).ToText()), NSLOCTEXT("Notify", "Notify.OnAccountLevelUp.Title", "Level Up")));
	}

	//=============================================================================
	//
	for (auto fraction : InstanceRepository->Fraction)
	{
		if (fraction && (fraction->GetModelId() == FractionTypeId::Keepers || fraction->GetModelId() == FractionTypeId::RIFT || fraction->GetModelId() == FractionTypeId::RIFT_Friend))
		{
			if(fraction->GetModelId() == FractionTypeId::RIFT_Friend)
			{
				fraction->Progress.CurrentRank = 10;
				fraction->Progress.Reputation = 10000;
			}
			Slate_FractionManager->AddFraction(fraction);

			if (fraction->IsAllowToDisplay)
			{
				Slate_FractionSelector->AddFraction(fraction);
			}
		}
	}

	/////////////////////////////// ach

	auto GameInst = Cast<UShooterGameInstance>(GetGameInstance());
	if (GameInst/* && !GEngine->IsEditor()*/)
	{
		/*auto WelcomeStyle = &FLokaSlateStyle::Get().GetWidgetStyle<FWelcomeWindowsStyle>("SWelcomeWindowsStyle");

		if (GameInst->StartMessages.bIsTournamentRulesShowed == false)
		{
			SMessageBox::AddQueueMessage("TournamentRules", FOnClickedOutside::CreateLambda([&, gi = GameInst]() {
				SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "Notify.Welcome.Title", "Welcome"));
				SMessageBox::Get()->SetContent(SNew(SWindowTournamentRules));
				SMessageBox::Get()->SetButtonsText(FText::GetEmpty(), FButtonNameLocalization::Continue);
				SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton) {
					SMessageBox::Get()->ToggleWidget(false);
					SMessageBox::Get()->OnMessageBoxHidden.AddLambda([&]() { SMessageBox::RemoveQueueMessage("TournamentRules"); });
				});
				SMessageBox::Get()->SetEnableClose(false);
				SMessageBox::Get()->ToggleWidget(true);

				gi->StartMessages.bIsTournamentRulesShowed = true;
				gi->StartMessages.Save();
			}));
		}*/
	}

	if (response.WeekBonus.Notify && response.WeekBonus.Stage > 0)
	{
		SMessageBox::AddQueueMessage("WeekBonus", FOnClickedOutside::CreateLambda([&, s = response.WeekBonus.Stage]() {
			SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "Notify.WeekBonus.Title", "Everyday Bonus"));
			SMessageBox::Get()->SetContent(SNew(SWindowEverydayBonus).DayNumber(s));
			SMessageBox::Get()->SetButtonsText(FText::GetEmpty(), FButtonNameLocalization::Continue);
			SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton) {
				SMessageBox::Get()->ToggleWidget(false);
				SMessageBox::Get()->OnMessageBoxHidden.AddLambda([&]() { SMessageBox::RemoveQueueMessage("WeekBonus"); });
			});
			SMessageBox::Get()->SetEnableClose(false);
			SMessageBox::Get()->ToggleWidget(true);
		}));

		Slate_NotifyList->AddNotify(FNotifyInfo(FText::Format(NSLOCTEXT("Notify", "Notify.WeekBonus.Desc", "Everyday bonus stage: {0}\r\nReward: {1}"),
									TextHelper_NotifyValue.SetValue(FText::AsNumber(response.WeekBonus.Stage)).ToText(),
									FRichTextHelpers::GetResourse("money", FString::FromInt(response.WeekBonus.Stage * 100))),
									NSLOCTEXT("Notify", "Notify.WeekBonus.Title", "Everyday Bonus")));
	}

	if (response.FractionTypeId == FractionTypeId::End)
	{
		SMessageBox::AddQueueMessage("FractionSelector", FOnClickedOutside::CreateLambda([&]() {
			Slate_FractionSelector->ToggleWidget(true);
		}));
	}
	else
	{
		OnRequestClientDataComplete();
	}

	ensureAlwaysMsgf(response.FractionExperience.Num() == FractionTypeId::End, TEXT("Invalid FractionExperience size: %d of %d"), response.FractionExperience.Num(), static_cast<int32>(FractionTypeId::End));
	for (auto f : response.FractionExperience)
	{
		UpdateFractionProgress(f);
	}
}


void SetEntityId(UItemBaseEntity* entity, const FString& itemId)
{
	if (entity)
	{
		entity->SetEntityId(itemId);
	}
}

void SetEntityId(UItemBaseEntity* entity, const FGuid& itemId)
{
	if (entity)
	{
		entity->SetEntityId(itemId);
	}
}

void ALobbyGameMode::OnRequestRequestLootData(const FLobbyClientLootData& response)
{
	auto currentPartyMember = LobbyPlayerController();
	//=============================================================================
	//	Init Character List at onec
	for (size_t i = 0; i < CharacterModelId::End; i++)
	{
		auto targetCharacterInstance = InstanceRepository->Character[i];
		if (targetCharacterInstance == nullptr)
		{
			targetCharacterInstance = UCharacterBaseEntity::StaticClass()->GetDefaultObject<UCharacterBaseEntity>();
		}

		Slate_CharacterManager->AddCharacter(targetCharacterInstance);
		Slate_FractionManager->AddCharacter(targetCharacterInstance);
	}

	//=============================================================================
	//
	currentPartyMember->InitializeInventory(response);


	currentPartyMember->Slate_CharacterManager->GetInventoryManager()->FillList(currentPartyMember->GetInventory());
	
	//=============================================================================
	//
	identityController.RequestPreSetData.Request();
}

void ALobbyGameMode::OnUnlockProfile(const FUnlockProfileResponse& response)
{
	LobbyPlayerController()->InitializePreSet(response.ProfileSlotId, response);
	Slate_NotifyList->AddNotify(FNotifyInfo(FText::Format(NSLOCTEXT("Lobby", "Notify.ProfileUnlocked", "You have opened a new profile slot for ammunition. Now available {0} profiles for you."), FText::AsNumber(static_cast<int32>(response.ProfileSlotId) + 1))));
}

void ALobbyGameMode::OnUnlockItem(const FUnlockItemResponseContainer& response)
{
	FGuid id;
	const auto categoryTypeId = static_cast<CategoryTypeId::Type>(response.ItemType);
	if (FGuid::Parse(response.ItemId, id))
	{
		if (auto item = InstanceRepository->FindById(response.ModelId, categoryTypeId))
		{		
			if (response.Cost.IsDonate)
			{
				FPlayerEntityInfo::Instance.Cash.Donate -= response.Cost.Amount;
			}
			else
			{
				FPlayerEntityInfo::Instance.Cash.Money -= response.Cost.Amount;
			}

			if (item->GetCount() == 0)
			{
				Slate_CharacterManager->GetInventoryManager()->FillList(LobbyPlayerController()->AddItem(id, item));
			}

			if (categoryTypeId == CategoryTypeId::Service)
			{
				item->SetCount(item->GetCount() + 1);
			}

			//	TODO:: Add item image & price
			Slate_NotifyList->AddNotify(FNotifyInfo(FText::Format(NSLOCTEXT("Lobby", "Notify.ItemUnlocked", "You bought {0}, {1} level"), item->GetDescription().Name, FText::AsNumber(static_cast<int32>(item->GetDisplayLevel())))));
			UE_LOG(LogHttp, Display, TEXT("Item unlocked %d of cat[%d] [%s]"), response.ModelId, response.ItemType, *response.ItemId);
		}
		else ensureAlwaysMsgf(id.IsValid(), TEXT("Invalid unlocked item instance: %s | model[%d] | type[%d]"), *response.ItemId, response.ModelId, categoryTypeId);
	}
	else ensureAlwaysMsgf(id.IsValid(), TEXT("Invalid unlocked item id: %s | model[%d] | type[%d]"), *response.ItemId, response.ModelId, categoryTypeId);
}


void ALobbyGameMode::OnRequestRequestPreSetData(const TArray<FClientPreSetData>& response)
{
	ALobbyPlayerController *currentPartyMember = LobbyPlayerController();
	//---------------------------------------------------------------------
	currentPartyMember->InitializePreSet();
	currentPartyMember->InitializePreSetList(response);

	friendController.List.TimerStart(GetWorldTimerManager());
	chatController.GetGlobalChatMessages.TimerStart(GetWorldTimerManager());
	
	//---------------------------------------------------------------------
	currentPartyMember->SelectSet(EPlayerPreSetId::One);
}


void ALobbyGameMode::OnRequestClientDataAttemp()
{
	if (identityController.RequestClientData.AttempRequestNumber < 5)
	{
		identityController.RequestClientData.AttempRequestNumber++;
		identityController.RequestClientData.Request();
	}

	SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Authentication", "Authentication.RequestClientData.Title", "Steam Authentication"));
	SMessageBox::Get()->SetContent(NSLOCTEXT("Authentication", "Authentication.RequestClientData.OnFailed", "Failed to load the player data"));
	SMessageBox::Get()->SetButtonsText(FText::GetEmpty(), FButtonNameLocalization::Ok);
	SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton&) { LobbyPlayerController()->ConsoleCommand("quit"); });
	SMessageBox::Get()->SetEnableClose(false);
	SMessageBox::Get()->ToggleWidget(true);
}

void ALobbyGameMode::OnCheckExistAccount(const FRequestOneParam<bool>& response)
{
	if (response)
	{
		SMessageBox::Get()->ToggleWidget(false);

		if (identityController.RequestClientData.SafeRequest("") == false)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Begin RequestClientData recived was false"));
			OnRequestClientDataAttemp();
		}
		else
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Begin RequestClientData recived was true"));
		}
	}
	else
	{
		Slate_AuthForm->ToggleWidget(false);		

		SMessageBox::Get()->SetContent(Slate_MessageBoxInput.ToSharedRef());
		SMessageBox::Get()->SetHeaderText(CreateGameAccount::DialogTitle());
		Slate_MessageBoxInput->SetContent(CreateGameAccount::EnterName());
		auto sub = IOnlineSubsystem::Get("Steam");
		if (sub)
		{
			auto Identity = sub->GetIdentityInterface();
			Slate_MessageBoxInput->SetInputText(FText::FromString(Identity->GetPlayerNickname(0)));
		}
		SMessageBox::Get()->SetButtonText(EMessageBoxButton::Left);
		SMessageBox::Get()->SetButtonText(EMessageBoxButton::Middle, FButtonNameLocalization::Ok);
		SMessageBox::Get()->SetButtonText(EMessageBoxButton::Right);
		SMessageBox::Get()->OnMessageBoxButton.AddUObject(this, &ALobbyGameMode::OnClickCreateGameAccount);
		SMessageBox::Get()->SetEnableClose(false);
		SMessageBox::Get()->ToggleWidget(true);
	}
}

void ALobbyGameMode::OnCreateGameAccountBadResponse() const
{
	SMessageBox::Get()->SetContent(Slate_MessageBoxInput.ToSharedRef());
	SMessageBox::Get()->SetHeaderText(CreateGameAccount::DialogTitle());
	Slate_MessageBoxInput->SetContent(CreateGameAccount::InvalidName());
	SMessageBox::Get()->SetButtonText(EMessageBoxButton::Left);
	SMessageBox::Get()->SetButtonText(EMessageBoxButton::Middle, FButtonNameLocalization::Ok);
	SMessageBox::Get()->SetButtonText(EMessageBoxButton::Right);
	SMessageBox::Get()->OnMessageBoxButton.AddUObject(this, &ALobbyGameMode::OnClickCreateGameAccount);
	SMessageBox::Get()->SetEnableClose(false);
	SMessageBox::Get()->ToggleWidget(true);
}

void ALobbyGameMode::OnCreateGameAccount(const FRequestOneParam<bool>& response) const
{
	if (response)
	{
		//	Double check account exist
		identityController.CheckExistAccount.Request();
	}
	else
	{
		Slate_MessageBoxInput->SetContent(CreateGameAccount::ExistName());
	}
}

void ALobbyGameMode::OnProxySearchSessionBegin(const FSearchSessionParams& params)
{
	lobbyController.SearchBegin.Request(params);
	JoinGameState = EJoinGameState::Search;

	Slate_NotifyList->AddNotify(FNotifyInfo(
		NSLOCTEXT("Lobby", "Notify.MatchMaking.MatchSearchBegin", "Match search began. Search time the game is dependent on the selected mode, it usually takes less than a minute."),
		 NSLOCTEXT("Lobby", "Notify.MatchMaking.Title", "Match making information")
	));
}

void ALobbyGameMode::OnProxySearchSessionCancel() const
{
	lobbyController.SearchCancel.Request();

	Slate_NotifyList->AddNotify(FNotifyInfo(
		NSLOCTEXT("Lobby", "Notify.MatchMaking.MatchSearchBegin", "Match search canceled"),
		NSLOCTEXT("Lobby", "Notify.MatchMaking.Title", "Match making information")
	));
}

void ALobbyGameMode::OnProcessTravelToGame()
{
	auto SharedToken = MakeShareable(new FUniqueNetIdString(SearchSessionStatus.Token));
	auto world = GetWorld();
	auto Player = world->GetFirstLocalPlayerFromController();
	Player->SetCachedUniqueNetId(SharedToken);

	LobbyPlayerController()->ClientTravel(SearchSessionStatus.Host, TRAVEL_Absolute);
}

void ALobbyGameMode::TravelToGame(const FSearchSessionStatus& response)
{
	FGuid token;
	if (response.Host.IsEmpty() == false && FGuid::Parse(response.Token, token))
	{
		{
			AddLobbyStatisticProxy();
			IOperationContainer::AllowHttpRequest = false;

			for (auto request : *IOperationContainer::HttpRequestList)
			{
				FScopeLock ScopeLock(&RequestArraysLock);

				request->CancelRequest();
#if (UE_GAME || UE_BUILD_SHIPPING || UE_BUILD_TEST || IS_MONOLITHIC)
				UE_LOG(LogHttp, Display, TEXT("TravelToGame | Remove request %s"), *request->GetURL());
				FHttpModule::Get().GetHttpManager().RemoveRequest(request.ToSharedRef());
#endif
			}

			if (response.Token != SearchSessionStatus.Token || response.Host != SearchSessionStatus.Host)
			{
				SearchSessionStatus = response;
				GetWorldTimerManager().ClearTimer(HandleProcessTravelToGame);
				DelegateProcessTravelToGame.BindUObject(this, &ALobbyGameMode::OnProcessTravelToGame);
				GetWorldTimerManager().SetTimer(HandleProcessTravelToGame, DelegateProcessTravelToGame, 2.0f, false, 2.0f);
			}
		}

	}
}

void ALobbyGameMode::ShowTravelMessage(const FSearchSessionStatus& response) const
{

	SMessageBox::AddQueueMessage("MatchMaking.TravelMessage", FOnClickedOutside::CreateLambda([&, res = response, controller = lobbyController]()
	{
		SMessageBox::Get()->SetButtonsText();
		if (res.GameModeTypeId == EGameModeTypeId::Attack)
		{
			if (res.IsDefence)
			{
				//	������ {0} �������� �� ���� ����! ���� ���������� � ������� ����. ����������, ���������, ��� ����� ������ �� ������ ����� ������
				//	Players {0} attack your base! Preparations are in progress for the defense of the base. Please wait, this may take no more than one minute.
				SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "Notify.TravelMessage.Defence2.Title", "Your base is under attack!"));
				SMessageBox::Get()->SetContent(FText::Format(NSLOCTEXT("Notify", "Notify.TravelMessage.Defenc2e.Desc", "Players {0} attack your base! Preparations are in progress for the defense of the base. Please wait, this may take no more than one minute"), FText::FromString(res.UniversalName)));
			}
			else
			{
				//	���� ������� �������� ����� �� ������ {0}. ���������� �� ����� � ���, ��������� ��� ����� ������ �� ������ ����� ������.
				//	Your team is attacking the player {0}. Preparing to enter the battle, wait it can take no more than one minute.
				SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "Notify.TravelMessage.Attack2.Title", "Outgoing attack on the player"));
				SMessageBox::Get()->SetContent(FText::Format(NSLOCTEXT("Notify", "Notify.TravelMessage.Attack2.Desc", "Your team is attacking the player {0}. Preparing to enter the battle, wait it can take no more than one minute."), FText::FromString(res.UniversalName)));
			}

			//	������������� ������� � ���
			JoinGameState = EJoinGameState::Search;
		}
		else if (res.GameModeTypeId == EGameModeTypeId::DuelMatch)
		{
			//	���� ���������� � ����� � ������� {0}. ����������, ��������� ��� ����� ������ �� ����� ����� ������.
			//	Preparations are in progress for a duel with player {0}. Please, wait it may take no more than one minute.
			SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "Notify.TravelMessage.Duel.Title", "Preparing for a duel"));
			SMessageBox::Get()->SetContent(FText::Format(NSLOCTEXT("Notify", "Notify.TravelMessage.Duel2.Desc", "Preparations are in progress for a duel with player {0}. Please, wait it may take no more than one minute."), FText::FromString(res.UniversalName)));
			
			//	������������� ������� � ���
			JoinGameState = EJoinGameState::Search;
		}
		else if (res.GameModeTypeId == EGameModeTypeId::Building)
		{
			SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "Notify.TravelMessage.LobbyCOOP.Title", "Enter cooperative mode"));
			SMessageBox::Get()->SetContent(NSLOCTEXT("Notify", "Notify.TravelMessage.LobbyCOOP.Desc", "Preparing to enter cooperative mode. Please wait, this may take no more than one minute"));
			SMessageBox::Get()->SetButtonsText(NSLOCTEXT("All", "All.Yes", "Yes"), NSLOCTEXT("All", "All.No", "No"));
		}
		else
		{
			const auto GameMapName = FGameMapTypeId::GetText(res.GameMapTypeId);
			const auto GameModeName = FGameModeTypeId::GetText(res.GameModeTypeId);

			//	���������� �� ����� � ������������� ���, {0} - {1}. ���������� ���������, ��� ����� ������ �� ������ ����� ������.
			//	Preparing to enter the unfinished battle, {0} - {1}. Please wait, this may take no more than one minute.
			SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "Notify.TravelMessage.Title", "Unfinished game"));
			SMessageBox::Get()->SetContent(FText::Format(NSLOCTEXT("Notify", "Notify.TravelMessage2.Desc", "You have an unfinished battle, {0} - {1}.  Do you want to return to the battlefield? This may take no more than one minute."), GameModeName, GameMapName));
			SMessageBox::Get()->SetButtonsText(NSLOCTEXT("All", "All.Yes", "Yes"), NSLOCTEXT("All", "All.No", "No"));
		}

		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton& button)
		{
			if (button == EMessageBoxButton::Left)
			{
				JoinGameState = EJoinGameState::Continue;
				lobbyController.GameSessionContinue.Request();
			}
			else
			{
				lobbyController.GameSessionBreak.Request();
			}

			SMessageBox::RemoveQueueMessage("MatchMaking.TravelMessage");
			SMessageBox::Get()->ToggleWidget(false);
		});

		SMessageBox::Get()->SetEnableClose(false);
		SMessageBox::Get()->ToggleWidget(true);
	}));
}


void ALobbyGameMode::OnSessionMatchFounded(const FSearchSessionStatus& response)
{
	if (response.Status == ESearchSessionStatus::Complete)
	{
		if (IsMatchMakingPrepareCompleteNotified == false)
		{
			Slate_NotifyList->AddNotify(FNotifyInfo(
				FText::Format
				(
					NSLOCTEXT("Lobby", "Notify.MatchMaking.MatchPrepareComplete", "Going on the battlefield, Gamemode: {0} | Location: {1}. Start the loading should happen within 10 seconds."),
					FText::FromString(FGameModeTypeId::GetString(response.GameModeTypeId)),
					FText::FromString(FGameMapTypeId::GetString(response.GameMapTypeId))
				), FText(NSLOCTEXT("Lobby", "Notify.MatchMaking.Title", "Match making information"))
			));
		}

		IsMatchMakingPrepareCompleteNotified = true;
		if (JoinGameState == EJoinGameState::Search)
			TravelToGame(response);
		else if(JoinGameState != EJoinGameState::Continue)
			ShowTravelMessage(response);
	}
	else if (response.Status == ESearchSessionStatus::Prepare)
	{
		if (IsMatchMakingPrepareBeginNotified == false)
		{
			const auto mapName = FGameMapTypeId::GetString(response.GameMapTypeId);

			if (mapName.Len() > 0 && mapName.IsEmpty() == false)
			{
				//TODO: Serinc check game map loading time
				LoadPackageAsync(FString::Printf(TEXT("Game/Maps/%s.umap"), *mapName), FLoadPackageAsyncDelegate(), 0, EPackageFlags::PKG_Need);
			}

			Slate_NotifyList->AddNotify(FNotifyInfo(
				FText::Format
				(
					NSLOCTEXT("Lobby", "Notify.MatchMaking.MatchPrepareBegin", "Search game ended, the preparation of the entrance to the game. \r\nGamemode: {0} | Location: {1}"),
					FGameModeTypeId::GetText(response.GameModeTypeId),
					FGameMapTypeId::GetText(response.GameMapTypeId)
				), FText(NSLOCTEXT("Lobby", "Notify.MatchMaking.Title", "Match making information"))
			));
		}

		IsMatchMakingPrepareBeginNotified = true;
	}
	else if (IsAuthRequestHandled == false)
	{
		IsAuthRequestHandled = true;
		Slate_AuthForm->ToggleWidget(false);
		identityController.CheckExistAccount.Request();
	}
}

void ALobbyGameMode::OnSessionNotAuthorized()
{
	if (IsAuthRequestHandled == false)
	{
		if (Slate_AuthForm.IsValid() && (GetWorld()->IsPlayInEditor() || bUseSteamAuthentication == false))
		{
			Slate_AuthForm->ToggleWidget(true);
			IsAuthRequestHandled = true;
		}
		else
		{
			checkf(!GetWorld()->IsPlayInEditor(), TEXT("Steam not supported in Editor!"));
			GetWorldTimerManager().SetTimer(SteamLoginAttempTimerHandle, SteamLoginAttempTimerDelegate, 2.0f, true, 1.0f);
			IsAuthRequestHandled = true;
		}
	}
	else if (IsAuthenticationComplete)
	{
		IsAuthRequestHandled = false;
		IsAuthenticationComplete = false;
	}
}

void ALobbyGameMode::InitializeSlateWidgets()
{
	SFriendsCommands::GetAlt().SetDelegates(this);

	SAssignNew(Slate_MessageBoxInput, SInputWindow);

	SAssignNew(Slate_GeneralPage, SGeneralPage)
		.OnVoteChange(marketingController.VoteImprovement.GetDelegate())
		.OnVoteProposal(marketingController.VoteSentence.GetDelegate())
		.OnCreateProposal(marketingController.PushSentence.GetDelegate())
		.OnClickedSuggestion_Lambda([&](const FRequestOneParam<FString>& s) {
			promoController.TakeSocialTask.Request(s);
		})
		;
	Slate_GeneralPage->OnReedemCode.BindRaw(&promoController.ActivatePromo, &ActivatePromo::Operation::Request);

	SAssignNew(Slate_AuthForm, SAuthForm);

	SAssignNew(Slate_FractionManager, SFractionManager)
		.OnFractionSelect_Raw(&identityController.SwitchFraction, &Operation::OnSwitchFraction::Operation::Request);
	Slate_FractionManager->OnClickedItem.BindRaw(&storeController.UnlockItem, &UnlockItem::Operation::Request);


	//	TODO: Depricated
	//SAssignNew(Slate_LeagueForm, SLeagueForm);
	//	.OnMakeLeague_Raw(&leagueController.Create, &Operation::CreateLeague::Operation::Request)
	//	.OnClickedLeague_UObject(this, &ALobbyGameMode::onClickLeague)
	//	.OnSearch_UObject(this, &ALobbyGameMode::onSearchLeague);
	//
	//SAssignNew(Slate_LeagueFormInfo, SLeagueFormInfo);
	//	.OnLeagueEdit_UObject(this, &ALobbyGameMode::onLeagueEdit)
	//	.OnLeagueKick_UObject(this, &ALobbyGameMode::onLeagueKick)
	//	.OnLeagueDonate_UObject(this, &ALobbyGameMode::onLeagueDonate)
	//	.OnLeagueSetRole_UObject(this, &ALobbyGameMode::onLeagueEditRole)
	//	.PlayerMoney_UObject(this, &ALobbyGameMode::GetPlayerMoney)
	//	.PlayerDonate_UObject(this, &ALobbyGameMode::GetPlayerDonate);
	//
	//SAssignNew(Slate_TournamentForm, STournamentForm);
	//	.OnClickedTournament_UObject(this, &ALobbyGameMode::onClickTournament);
	//
	//SAssignNew(Slate_TournamentTeam, STournamentTeam);
	//	.OnTournamentTeamPay_UObject(this, &ALobbyGameMode::onClickTournamentTeamPay)
	//	.OnTournamentTeamConfirm_UObject(this, &ALobbyGameMode::onClickTournamentTeamConfirm);
	//
	//SAssignNew(Slate_TournamentTree, STournamentTree);
	//	.OnClickedJoin_UObject(this, &ALobbyGameMode::onClickTournamentJoin)
	//	.OnClickedTeam_UObject(this, &ALobbyGameMode::onClickTournamentTeamInfo);
	//
	SAssignNew(Slate_NotifyList, SNotifyList);
	
	SAssignNew(Slate_LobbyChat, SLobbyChat)
		.OnMessageSend_UObject(this, &ALobbyGameMode::onMessageSend);
	 
	chatController.GetGlobalChatMessages.OnSuccess.AddSP(Slate_LobbyChat.ToSharedRef(), &SLobbyChat::OnReceiveGlobalChatMessage);
	chatController.PostMessageToGlobalChat.OnSuccess.AddSP(Slate_LobbyChat.ToSharedRef(), &SLobbyChat::OnReceiveGlobalChatMessage);	

	SAssignNew(Slate_FriendsForm, SFriendsForm)	
		.OnRemoveFromSquad_Raw(&squadController.RemoveFromSquad, &Operation::RemoveFromSquad::Operation::Request)
		.OnLeaveFromSquad_Raw(&squadController.LeaveFromSquad, &Operation::LeaveFromSquad::Operation::Request)
		.OnSearchFriend(friendController.Search.GetDelegate());

		//.OnMessage_UObject(this, &ALobbyGameMode::onFriendWhisper);

	friendController.List.OnSuccess.AddSP(Slate_FriendsForm.ToSharedRef(), &SFriendsForm::OnFillingList, true);
	friendController.Search.OnSuccess.AddSP(Slate_FriendsForm.ToSharedRef(), &SFriendsForm::OnFillingList, false);

	squadController.GetSqaudInformation.OnSuccess.AddSP(Slate_FriendsForm.ToSharedRef(), &SFriendsForm::OnSquapInformationUpdate);
	squadController.GetSqaudInformation.OnSuccess.AddUObject(this, &ALobbyGameMode::OnUpdateSquadMembers);

	SAssignNew(Slate_SettingsManager, SSettingsManager)
		.GameUserSettings(Cast<UUTGameUserSettings>(GEngine->GameUserSettings));

	SAssignNew(Slate_LobbyContainer, SLobbyContainer)
		.RefNotifyList(Slate_NotifyList.ToSharedRef())
		.RefFriendsForm(Slate_FriendsForm.ToSharedRef())
		.RefLobbyChat(Slate_LobbyChat.ToSharedRef())
		.RefSettingsManager(Slate_SettingsManager.ToSharedRef())
		
		.OnQuit_Lambda([&]() { LobbyPlayerController()->ConsoleCommand("quit"); })
		.OnSearchSessionBegin_UObject(this, &ALobbyGameMode::OnProxySearchSessionBegin)
		.OnSearchSessionCancel_UObject(this, &ALobbyGameMode::OnProxySearchSessionCancel)
		.OnClickedBooster_Lambda([&](const EResourceTextBoxType::Type InType) {
			Slate_LobbyContainer->SetActiveScrollablyWidget(ELobbyActionButton::Store);
			GetWorldTimerManager().SetTimerForNextTick(FTimerDelegate::CreateLambda([&]() {
				if (InstanceRepository->Fraction[FractionTypeId::RIFT_Friend])
				{
					Slate_FractionManager->SetActiveFraction(InstanceRepository->Fraction[FractionTypeId::RIFT_Friend]);
				}
			}));
	})
	.OnClickedBuildMode_UObject(this, &ALobbyGameMode::OnProxyBuildingMode);

	SAssignNew(Slate_FractionSelector, SFractionSelector)
		.OnFractionSelect_Raw(&identityController.SelectFraction, &Operation::OnSelectFraction::Operation::Request);
		
	SAssignNew(Slate_LobbyAchievements, SLobbyAchievements);

	squadController.GetSqaudInformation.OnSuccess.AddSP(Slate_LobbyContainer.ToSharedRef(), &SLobbyContainer::OnSquadInformationUpdate);

	lobbyController.SearchBegin.OnSuccess.AddSP(Slate_LobbyContainer->GetContainerTopWidget(), &SLobbyContainer_Top::OnSearchSessionStatusRecived);
	lobbyController.SearchBegin.OnFailed.AddSP(Slate_LobbyContainer->GetContainerTopWidget(), &SLobbyContainer_Top::OnSearchSessionStatusFailed);
	lobbyController.SearchBegin.OnWeaponEquipRequired.Handler.AddSP(Slate_LobbyContainer->GetContainerTopWidget(), &SLobbyContainer_Top::OnSearchSessionStatusFailed);
	lobbyController.SearchBegin.OnNoAvalibleProfiles.Handler.AddSP(Slate_LobbyContainer->GetContainerTopWidget(), &SLobbyContainer_Top::OnSearchSessionStatusFailed);



	lobbyController.SearchCancel.OnSuccess.AddSP(Slate_LobbyContainer->GetContainerTopWidget(), &SLobbyContainer_Top::OnSearchSessionStatusRecived);
	lobbyController.SearchCancel.OnFailed.AddSP(Slate_LobbyContainer->GetContainerTopWidget(), &SLobbyContainer_Top::OnSearchSessionStatusFailed);	

	lobbyController.GetPlayerRatingTop.OnSuccess.AddSP(Slate_GeneralPage.ToSharedRef(), &SGeneralPage::OnFillRaiting);
	lobbyController.GetPlayerRatingTop.OnSuccess.AddSP(Slate_LobbyContainer->GetContainerTopWidget(), &SLobbyContainer_Top::OnFillRaiting);
}

void ALobbyGameMode::onMessageSend(const FString& Message, const ELobbyChatChannel::Type Channel, const FName& Tag)
{
	if (Channel == ELobbyChatChannel::Global)
	{
		chatController.PostMessageToGlobalChat.Request(Message);
		//PTR_OF_SERVICE(Identity)->ClientSendChatMessage(Message);
	}
	else if (Channel == ELobbyChatChannel::League)
	{
		//PTR_OF_SERVICE(Corporation)->RequestAddChatMessage(Message);
	}
	else if (Channel == ELobbyChatChannel::Custom && Tag != FName(NAME_None))
	{
		FUserWhisper Whisper;
		Whisper.Sender = Tag.ToString();
		Whisper.Message = Message;

		//PTR_OF_SERVICE(Identity)->RequestSendWhisper(Whisper);
	}
}

void ALobbyGameMode::onClickMessageBoxButton(uint8 _button)
{
	//HUD_MessageBox->onHide();
	//GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit");
}

// ReSharper disable once CppMemberFunctionMayBeConst
void ALobbyGameMode::OnClickCreateGameAccount(const EMessageBoxButton&)
{
	//SMessageBox::Get()->ToggleWidget(false);
	identityController.CreateGameAccount.Request(Slate_MessageBoxInput->GetInputText().ToString());
}

void ALobbyGameMode::OnSteamLoginComplete(int32 ctr, bool isOk, const FUniqueNetId& id, const FString& error)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OnSteamLoginComplete status: %d"), isOk));
	if (isOk)
	{
		
		identityController.SteamAuthentication.PostRequest(id.ToString(), GetWorldTimerManager());
		GetWorldTimerManager().PauseTimer(SteamLoginAttempTimerHandle);
		GetWorldTimerManager().ClearTimer(SteamLoginAttempTimerHandle);

		//TODO: Message Box: ����������� ����� STEAM, �������� ������
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Authentication", "Authentication.Steam.Title", "Steam Authentication"));
		SMessageBox::Get()->SetContent(FText::Format(
			NSLOCTEXT("Authentication", "Authentication.Steam.Waiting", "Waiting authentication from steam, please wait. \n{0}"), 
			FText::FromString(error))
		);
		SMessageBox::Get()->SetButtonsText();
		SMessageBox::Get()->SetEnableClose(false);
		SMessageBox::Get()->ToggleWidget(true);	
	}
	else
	{
		//TODO: Message Box: �� ������� �������������� � Steam
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Authentication", "Authentication.Steam.Title", "Steam Authentication"));
		SMessageBox::Get()->SetContent(FText::Format(
			NSLOCTEXT("Authentication", "Authentication.Steam.Error", "Authentication error, please check steam is online mode. \n{0}"),
			FText::FromString(error)
		));
		SMessageBox::Get()->SetButtonsText(FText::GetEmpty(), FButtonNameLocalization::Ok);
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton&) { LobbyPlayerController()->ConsoleCommand("quit"); });
		SMessageBox::Get()->SetEnableClose(false);
		SMessageBox::Get()->ToggleWidget(true);
	}
}

void ALobbyGameMode::OnSteamLoginAttemp()
{
	auto sub = IOnlineSubsystem::Get(FName(*FString("Steam")));
	if (sub)
	{
		auto Identity = sub->GetIdentityInterface().Get();
		if (Identity)
		{
			Identity->AddOnLoginCompleteDelegate_Handle(0, OnSteamLoginCompleteDelegate);
			auto res = Identity->Login(0, FOnlineAccountCredentials());

		}
		else
		{
			//TODO: Message Box: �� ������� �������������� ����� Steam. ������ ����������� ����������
			SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Authentication", "Authentication.Steam.Title", "Steam Authentication"));
			SMessageBox::Get()->SetContent(NSLOCTEXT("Authentication", "Authentication.Steam.ErrorNotAllow", "Authentication error, service is not work on current moment."));
			SMessageBox::Get()->SetButtonsText(FText::GetEmpty(), FButtonNameLocalization::Ok);
			SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton&) { LobbyPlayerController()->ConsoleCommand("quit"); });
			SMessageBox::Get()->SetEnableClose(false);
			SMessageBox::Get()->ToggleWidget(true);
		}
	}
	else
	{
		//TODO: Message Box: �� ������� �������������� ����� Steam. ������ Steam ����������
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Authentication", "Authentication.Steam.Title", "Steam Authentication"));
		SMessageBox::Get()->SetContent(NSLOCTEXT("Authentication", "Authentication.Steam.ErrorNotAllow", "Authentication error, service is not work on current moment."));
		SMessageBox::Get()->SetButtonsText(FText::GetEmpty(), FButtonNameLocalization::Ok);
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton&) { LobbyPlayerController()->ConsoleCommand("quit"); });
		SMessageBox::Get()->SetEnableClose(false);
		SMessageBox::Get()->ToggleWidget(true);
	}

	if (AttempCount >= 5)
	{
		GetWorldTimerManager().PauseTimer(SteamLoginAttempTimerHandle);
		GetWorldTimerManager().ClearTimer(SteamLoginAttempTimerHandle);
	}

	AttempCount++;
}

void ALobbyGameMode::StartPlay()
{
	//FWindowsNotify::Flash(true);
	ContextValidator = IOperationContainer::ContextValidatorValue;
	IOperationContainer::Context = this;
	IOperationContainer::AllowHttpRequest = true;
	IOperationContainer::ContextValidator = &ContextValidator;

	CurrentLobbyAction.Duration = FDateTime::UtcNow().ToUnixTimestamp();

	auto GameInst = Cast<UShooterGameInstance>(GetGameInstance());
	GameInst->StartMessages.Load();

	Super::StartPlay();

	if (GEngine->GameViewport->GetGameViewportWidget().IsValid() && GEngine->GameViewport->GetGameViewportWidget()->GetContent().IsValid())
	{
		GEngine->GameViewport->GetGameViewportWidget()->GetContent()->SetVisibility(EVisibility::SelfHitTestInvisible);
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	auto spawnPoint = *TActorIterator<APlayerStart>(GetWorld());
	checkf(spawnPoint, ANSI_TO_TCHAR("Spawn points not found"));


	LobbyPlayerCharacterPosition = spawnPoint->GetActorLocation();
	LobbyPlayerCharacterRotation = spawnPoint->GetActorRotation();
	
	LobbyPlayerController()->SpawnCharacterLocation = spawnPoint->GetTransform();
	LobbyPlayerController()->SetInputMode(FInputModeGameAndUI());

	UGameViewportClient* GameViewportClient = GetWorld()->GetGameViewport();
	if (GameViewportClient)
	{
		GameViewportClient->SetCaptureMouseOnClick(EMouseCaptureMode::CaptureDuringMouseDown);
	}

	if (EntityRepository)
	{
		InternalBeginPlay();
	}
	else
	{
		//
		GetWorldTimerManager().SetTimer(timer_CheckEntityRepository, FTimerDelegate::CreateLambda([&, gi = GameInst]() {
			if (gi->EntityRepository)
			{				
				gi->OnPostLoadMap(GetWorld());
				InternalBeginPlay();
			}
		}), 1.0f, true);
	}

	//SAssignNew(Slate_SettingsMenu, SUTMenuBase);
}

void ALobbyGameMode::OnPreSetItemEquip(const FPreSetEquipResponse& response)
{
	if (response.CategoryId == CategoryTypeId::Service)
	{
		if (auto Service = LobbyPlayerController()->GetInventory().FindByStrId(response.ItemId, CategoryTypeId::Service))
		{
			Slate_NotifyList->AddNotify(FNotifyInfo(FText::Format(NSLOCTEXT("Lobby", "Notify.UsingPremium", "You activate {0}, there are still {1} coupons"), Service->GetDescription().Name, response.Count)));
		}
	}
}

void ALobbyGameMode::InternalBeginPlay()
{
	GetWorldTimerManager().ClearTimer(timer_CheckEntityRepository);
	GetWorldTimerManager().SetTimer(AddLobbyStatisticTimerHandle, AddLobbyStatisticTimerDelegate, 10.0f, true, 2.0f);

	auto GameInst = Cast<UShooterGameInstance>(GetGameInstance());

	EntityRepository = GameInst->EntityRepository;
	InstanceRepository = &EntityRepository->InstanceRepository;
	Slate_LobbyContainer->GetContainerTopWidget()->SetRepository(InstanceRepository);

	IOperationContainer::UserToken.Reserve(256);
	IOperationContainer::HttpRequestList->Reserve(512);

	LobbyPlayerController()->InitializUi();
	LobbyPlayerController()->OnPreSetItemEquipDelegate.BindRaw(&hangarController.PreSetItemEquip, &ItemEquip::Operation::Request);
	LobbyPlayerController()->OnPreSetItemUnEquipDelegate.BindRaw(&hangarController.PreSetItemUnEquip, &ItemUnEquip::Operation::Request);

	hangarController.PreSetItemEquip.OnSuccess.AddUObject(this, &ALobbyGameMode::OnPreSetItemEquip);
	hangarController.PreSetItemEquip.OnSuccess.AddUObject(LobbyPlayerController(), &ALobbyPlayerController::OnPreSetItemEquip);
	hangarController.PreSetItemUnEquip.OnSuccess.AddUObject(LobbyPlayerController(), &ALobbyPlayerController::OnPreSetItemUnEquip);
	hangarController.AddonEquip.OnSuccess.AddUObject(LobbyPlayerController(), &ALobbyPlayerController::OnEquipAddonResponse);

	Slate_CharacterManager = LobbyPlayerController()->Slate_CharacterManager;
	Slate_CharacterManager->OnUnlockCharacter.BindRaw(&storeController.UnlockItem, &UnlockItem::Operation::Request);
	Slate_CharacterManager->GetSetManager()->OnUnlockPreset.BindRaw(&storeController.UnlockProfile, &UnlockProfile::Operation::Request);

	//==================================================================================================
	//	ULokaGameUserSettings
	Cast<UUTGameUserSettings>(GEngine->GameUserSettings)->InitSettings();

	Slate_SettingsManager->LoadSettings();




	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	Slate_LobbyContainer->OnSwitchScrollablyWidget.AddUObject(LobbyPlayerController(), &ALobbyPlayerController::OnSwitchedLobbySlide);
	Slate_LobbyContainer->OnSwitchScrollablyWidget.AddUObject(this, &ALobbyGameMode::OnSwitchedLobbySlide);

	Slate_LobbyContainer->AddScrollablyWidget(ELobbyActionButton::Welcome, Slate_GeneralPage.ToSharedRef(), NSLOCTEXT("ALobbyGameMode", "Lobby.General", "General"));
	Slate_LobbyContainer->AddScrollablyWidget(ELobbyActionButton::Store, Slate_FractionManager.ToSharedRef(), NSLOCTEXT("ALobbyGameMode", "Lobby.Shop", "Shop"));
	Slate_LobbyContainer->AddScrollablyWidget(ELobbyActionButton::Hangar, Slate_CharacterManager.ToSharedRef(), NSLOCTEXT("ALobbyGameMode", "Lobby.Hangar", "Hangar"));
	//Slate_LobbyContainer->AddScrollablyWidget(ELobbyActionButton::League, Slate_LeagueForm.ToSharedRef(), NSLOCTEXT("ALobbyGameMode", "Lobby.League", "League"));
	//Slate_LobbyContainer->AddScrollablyWidget(ELobbyActionButton::Tournament, Slate_TournamentForm.ToSharedRef(), NSLOCTEXT("ALobbyGameMode", "Lobby.Tournament", "Tournament"));
	Slate_LobbyContainer->AddScrollablyWidget(ELobbyActionButton::Achievements, Slate_LobbyAchievements.ToSharedRef(), NSLOCTEXT("ALobbyGameMode", "Lobby.Achievements", "Achievements"));

	hangarController.ItemUpgrade.OnSuccess.AddSP(Slate_CharacterManager->GetModifyManager(), &SCharacterManager_Modify::OnUpdateList);
	hangarController.ItemUpgrade.OnFailed.AddLambda([]() {

		SMessageBox::Get()->SetContent(NSLOCTEXT("ALobbyGameMode", "ItemUpgrade.OnFailed", "Unfortunately you did not manage to improve item."));
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("ALobbyGameMode", "ItemUpgrade.OnFailed.Title", "Upgrade failed"));
		SMessageBox::Get()->SetButtonText(EMessageBoxButton::Left);
		SMessageBox::Get()->SetButtonText(EMessageBoxButton::Middle, FButtonNameLocalization::Ok);
		SMessageBox::Get()->SetButtonText(EMessageBoxButton::Right);
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([](const EMessageBoxButton&) { SMessageBox::Get()->ToggleWidget(false); });
		SMessageBox::Get()->SetEnableClose(true);
		SMessageBox::Get()->ToggleWidget(true);

	});
	hangarController.ConfirmItemUpgrade.OnSuccess.AddUObject(LobbyPlayerController(), &ALobbyPlayerController::OnConfirmedItemUpgrade);

	Slate_CharacterManager->GetModifyManager()->SetInstanceRepository(InstanceRepository);
	Slate_CharacterManager->GetModifyManager()->OnGetUpdateList.BindRaw(&hangarController.ItemUpgrade, &Operation::ItemUpgrade::Operation::Request);
	Slate_CharacterManager->GetModifyManager()->OnConfirmUpdate.BindRaw(&hangarController.ConfirmItemUpgrade, &Operation::ConfirmItemUpgrade::Operation::Request);

	Slate_CharacterManager->IsEnableInventory = TAttribute<bool>::Create(TAttribute<bool>::FGetter::CreateLambda([&]() { return Slate_LobbyContainer.IsValid() && Slate_LobbyContainer->GetContainerTopWidget()->GetSearchSessionStatus() == ESearchSessionState::None; }));

	Slate_GeneralPage->SetSpecificWidget(Slate_LobbyChat->GetContentWidget(), false);
	Slate_GeneralPage->SetSpecificWidget(Slate_FriendsForm->GetContentWidget(), true);

	GEngine->GameViewport->AddViewportWidgetContent(Slate_AuthForm.ToSharedRef());
	GEngine->GameViewport->AddViewportWidgetContent(Slate_LobbyContainer.ToSharedRef());
	GEngine->GameViewport->AddViewportWidgetContent(Slate_FractionSelector.ToSharedRef());
	GEngine->GameViewport->AddViewportWidgetContent(SMessageBox::Get());

	storeController.ListOfInstance.OnSuccess.AddUObject(this, &ALobbyGameMode::OnStoreInitialized);
	storeController.ListOfInstance.OnSuccess.AddUObject(EntityRepository, &UEntityRepository::Initialize);
	storeController.ListOfInstance.OnSuccess.AddUObject(GameInst, &UShooterGameInstance::InitTutorialsData);

	storeController.ListOfInstance.OnFailed.AddLambda([&]()
	{
		if (storeController.ListOfInstance.AttempRequestNumber < 5)
		{
			storeController.ListOfInstance.AttempRequestNumber++;
			storeController.ListOfInstance.Request();
		}

		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Authentication", "Authentication.ListOfInstance.Title", "Steam Authentication"));
		SMessageBox::Get()->SetContent(NSLOCTEXT("Authentication", "Authentication.ListOfInstance.OnFailed", "Failed to load store data!"));
		SMessageBox::Get()->SetButtonsText(FText::GetEmpty(), FButtonNameLocalization::Ok);
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton&) { LobbyPlayerController()->ConsoleCommand("quit"); });
		SMessageBox::Get()->SetEnableClose(false);
		SMessageBox::Get()->ToggleWidget(true);
	});


	if (EntityRepository)
	{
		identityController.CheckServerStatus.Request();
	}
}

void ALobbyGameMode::OnSwitchedLobbySlide(const ELobbyActionButton& Index, const ELobbyActionButton& OldIndex)
{
	//==========================================================================================================
	{				
		CurrentLobbyAction.Direction++;
		CurrentLobbyAction.Duration = FDateTime::UtcNow().ToUnixTimestamp() - CurrentLobbyAction.Duration;
		CurrentLobbyAction.OldAction = OldIndex;
		CurrentLobbyAction.NewAction = Index;
		LobbyActionStatistic.Add(CurrentLobbyAction);
	}
	CurrentLobbyAction.Duration = FDateTime::UtcNow().ToUnixTimestamp();
	//==========================================================================================================

	if (Index == ELobbyActionButton::Store)
	{
		const auto TargetFractionId = FPlayerEntityInfo::Instance.FractionTypeId;
		if (TargetFractionId >= 0 && TargetFractionId < FractionTypeId::End && InstanceRepository->Fraction[TargetFractionId])
		{
			Slate_FractionManager->SetActiveFraction(InstanceRepository->Fraction[TargetFractionId]);
		}		
	}
}

void ALobbyGameMode::OnStoreInitialized(const FListOfInstance& data)
{
	storeController.ListOfInstance.TimerStop(GetWorldTimerManager());
	TArray<UItemAchievementEntity*> achievements;
	for (auto property : data.Achievements)
	{
		auto achievement = InstanceRepository->Achievement[property.Id];
		if (achievement && achievement->IsValidLowLevel())
		{
			achievement->Initialize(property);
			achievements.Add(achievement);
		}
	}

	Slate_LobbyAchievements->SetList(achievements);
	Slate_GeneralPage->SetBuildTime_MasterServer(data.BuildDateTime);
	Slate_GeneralPage->TargetTimeStart = data.Tournament.Start;
	Slate_GeneralPage->TargetTimeEnd = data.Tournament.End;

	SWindowVotingForChange::CostOfChangeVote = data.CostOfImprovementVote;
	SWindowDetailsForProposals::CostOfProposalPush = data.CostOfSentencePush;

	for (auto &i : InstanceRepository->Armour)
	{
		if (i && i->IsValidLowLevel())
		{
			i->CaptureThumbnail(GetWorld());
		}
	}

	for (auto &i : InstanceRepository->Weapon)
	{
		if (i && i->IsValidLowLevel())
		{
			i->CaptureThumbnail(GetWorld());
		}
	}

	for (auto &i : InstanceRepository->Module)
	{
		if (i && i->IsValidLowLevel())
		{
			i->CaptureThumbnail(GetWorld());
		}
	}

	for (auto &i : InstanceRepository->Grenade)
	{
		if (i && i->IsValidLowLevel())
		{
			i->CaptureThumbnail(GetWorld());
		}
	}
}

void ALobbyGameMode::OnUpdateSquadMembers(const FSqaudInformation& squad)
{
	SquadMembers = squad.Members;
}

TArray<FString> ALobbyGameMode::GetSquadMemberNames() const
{
	TArray<FString> names;
	for (const auto m : SquadMembers)
	{
		names.Add(m.Name);
	}
	return names;
}

void ALobbyGameMode::StopHttpQueries()
{
	IOperationContainer::AllowHttpRequest = false;
	//OnProxySearchSessionCancel();
	for (const auto reuest : IOperationContainer::HttpPoolingRequestList)
	{
		if (reuest && reuest->IsValid())
		{
			GetWorldTimerManager().PauseTimer(*reuest);
			GetWorldTimerManager().ClearTimer(*reuest);
		}
	}

	for (auto request : *IOperationContainer::HttpRequestList)
	{
		FScopeLock ScopeLock(&RequestArraysLock);
		request->OnProcessRequestComplete().Unbind();
		request->OnRequestProgress().Unbind();
		request->CancelRequest();

#if (UE_GAME || UE_BUILD_SHIPPING || UE_BUILD_TEST || IS_MONOLITHIC)
		UE_LOG(LogHttp, Display, TEXT("End Play | Remove request %s"), *request->GetURL());
		FHttpModule::Get().GetHttpManager().RemoveRequest(request.ToSharedRef());
#endif
	}

	IOperationContainer::HttpRequestList->Empty(512);
	IOperationContainer::HttpPoolingRequestList.Empty(512);
}

void ALobbyGameMode::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	//StopHttpQueries();

	GetWorldTimerManager().ClearTimer(AddLobbyStatisticTimerHandle);
	GetWorldTimerManager().ClearTimer(lobbyController.SearchStatus.TimerHandle);
	GetWorldTimerManager().ClearTimer(SteamLoginAttempTimerHandle);
	
	chatController.GetGlobalChatMessages.TimerStop(GetWorldTimerManager());
	friendController.List.TimerStop(GetWorldTimerManager());
	storeController.ListOfInstance.TimerStop(GetWorldTimerManager());
	identityController.SteamAuthentication.TimerStop(GetWorldTimerManager());
	lobbyController.GetPlayerRatingTop.TimerStop(GetWorldTimerManager());
	lobbyController.GetMatchResults.TimerStop(GetWorldTimerManager());
	promoController.StatusOfSocialTask.TimerStop(GetWorldTimerManager());
	promoController.ListOfSocialTask.TimerStop(GetWorldTimerManager());
	identityController.CheckAccountStatus.TimerStop(GetWorldTimerManager());
	marketingController.RequestImprovementsList.TimerStop(GetWorldTimerManager());

	squadController.GetSqaudInformation.TimerStop(GetWorldTimerManager());
	squadController.GetSquadInvites.TimerStop(GetWorldTimerManager());

	GetWorldTimerManager().ClearAllTimersForObject(this);

	//GetWorldTimerManager().ClearTimer(identityController.HeartBeat.TimerHandle);



	if (EndPlayReason == EEndPlayReason::EndPlayInEditor || EndPlayReason == EEndPlayReason::Quit)
	{


		//HttpRequest->CancelRequest()


		//identityController.LogOut.Request();
		SMessageBox::Reset();
	}

	//if (EndPlayReason == EEndPlayReason::EndPlayInEditor)
	//{
	//	IOperationContainer::UserToken = "";
	//}

#if WITH_EDITOR == 0

#define REMOVE_SINGLE_WIDGET(Widget)\
		if (Widget.IsValid())\
		{\
			GEngine->GameViewport->RemoveViewportWidgetContent(Widget.ToSharedRef());\
			Widget.Reset();\
		}\

#define RESET_SINGLE_WIDGET(Widget)\
		if (Widget.IsValid())\
		{\
			Widget.Reset();\
		}\

	if (GEngine && GEngine->GameViewport)
	{
		GEngine->GameViewport->RemoveViewportWidgetContent(SMessageBox::Get());
		SMessageBox::Reset();

		REMOVE_SINGLE_WIDGET(Slate_FractionSelector);
		REMOVE_SINGLE_WIDGET(Slate_AuthForm);
		REMOVE_SINGLE_WIDGET(Slate_LobbyContainer);

		RESET_SINGLE_WIDGET(Slate_MessageBoxInput);
		RESET_SINGLE_WIDGET(Slate_GeneralPage);
		RESET_SINGLE_WIDGET(Slate_SettingsManager);
		RESET_SINGLE_WIDGET(Slate_FractionManager);
		RESET_SINGLE_WIDGET(Slate_TournamentForm);
		RESET_SINGLE_WIDGET(Slate_TournamentTeam);
		RESET_SINGLE_WIDGET(Slate_TournamentTree);
		RESET_SINGLE_WIDGET(Slate_NotifyList);
		RESET_SINGLE_WIDGET(Slate_LobbyChat);
		RESET_SINGLE_WIDGET(Slate_FriendsForm);
		RESET_SINGLE_WIDGET(Slate_LeagueForm);
		RESET_SINGLE_WIDGET(Slate_LeagueFormInfo);
		RESET_SINGLE_WIDGET(Slate_CharacterManager);
		RESET_SINGLE_WIDGET(Slate_LeagueFormInfo);
	}
#endif
	Super::EndPlay(EndPlayReason);
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


ALobbyPlayerController* ALobbyGameMode::LobbyPlayerController() const
{
	if (GetWorld() == nullptr) return nullptr;
	if (GetWorld()->GetFirstPlayerController() == nullptr) return nullptr;
	return CastChecked<ALobbyPlayerController>(GetWorld()->GetFirstPlayerController());
}

// ?Temp?
void ALobbyGameMode::OnRequestImprovementsListResponse(const FImprovementViewModel& response)
{
	if(FPlayerEntityInfo::Instance.IsAllowVoteImprovement && bIsImprovementsVoteNotified && response.ImprovementViews.Num() > 0 )
	{
		bIsImprovementsVoteNotified = true;
		Slate_NotifyList->AddNotify(FNotifyInfo(
			NSLOCTEXT("Lobby", "Notify.Improvement.ImprovementsVoteNotify", "Vote for what you lack in the game and we'll add it! If you find a bug or have continuation then inform them in one of the discussions. Help us to become better."),
			NSLOCTEXT("Lobby", "Notify.Improvement.Title", "Available improvements")
		));
	}

	if (Slate_GeneralPage.IsValid())
	{
		Slate_GeneralPage->OnFillIssues(response);
	}
}

void ALobbyGameMode::OnVoteImprovementResponse(const FImprovementVoteCreate& response)
{
	FPlayerEntityInfo::Instance.IsAllowVoteImprovement = false;
	Slate_NotifyList->AddNotify(FNotifyInfo(
		NSLOCTEXT("Lobby", "Notify.VoteImprovement.OnVoteImprovementResponse", "Thank you for your vote, your opinion is important to us. Voting results every Monday.")
	));
}

void ALobbyGameMode::OnBuildingsList(const TArray<FWorldBuildingItemView>& InList)
{
	if (auto GameInst = Cast<UShooterGameInstance>(GetGameInstance()))
	{
#if !WITH_EDITOR
		auto SettingsInstance = Cast<UUTGameUserSettings>(UGameUserSettings::GetGameUserSettings());
		if (GameInst && SettingsInstance && SettingsInstance->IsLoadingBuildingMode() && GameInst->StartMessages.bIsTournamentRulesShowed == false)
		{
			SMessageBox::AddQueueMessage("LoadingBuildingMode", FOnClickedOutside::CreateUObject(this, &ALobbyGameMode::OpenBuildingMode));
		}
		else
		{
#endif
			if (bIntoBattle)
			{
#if !WITH_EDITOR
				FSearchSessionParams SearchSessionParams;
				FFlagsHelper::SetFlags(SearchSessionParams.GameModeTypeId, EGameModeTypeId::Attack | EGameModeTypeId::LostDeadMatch | EGameModeTypeId::ResearchMatch | EGameModeTypeId::TeamDeadMatch);
				OnProxySearchSessionBegin(SearchSessionParams);
#endif
			}
			else
			{
				FActorSpawnParameters ActorSpawnParameters;
				ActorSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

				auto InstRepository = &GameInst->EntityRepository->InstanceRepository;

				// Spawn
				for (auto Item : InList)
				{
					if (InstRepository->Build.Contains(Item.ModelId) && InstRepository->Build[Item.ModelId]->ComponentTemplate)
					{
						if (auto SpawnedActor = GetWorld()->SpawnActor<ABuildPlacedComponent>(InstRepository->Build[Item.ModelId]->ComponentTemplate, FTransform(Item.Rotation, Item.Position), ActorSpawnParameters))
						{
							FGuid::Parse(Item.ItemId, SpawnedActor->GUID);
							SpawnedActor->InitializeInstance(InstRepository->Build[Item.ModelId]);
							SpawnedActor->OnComponentPlaced();
							SpawnedActor->Health.X = FMath::Min<float>(Item.Health, SpawnedActor->Health.Y);
							SpawnedActor->OnRep_Health();

							int32 BuildingType = SpawnedActor->Instance->GetBuildingType();
							if (FFlagsHelper::HasAnyFlags<int32>(BuildingType, FFlagsHelper::ToFlag<int32>(EBuildingType::WarehouseMoney) | FFlagsHelper::ToFlag<int32>(EBuildingType::MinerMoney)))
							{
								SpawnedActor->PlacedBuildingData.FindOrAdd(TEXT("Storage")) = Item.Storage;
								SpawnedActor->ReInitializeBuildingData(false);
							}
						}
					}
				}
			}
#if !WITH_EDITOR
		}		
#endif
	}
}

void ALobbyGameMode::OnProxyBuildingMode()
{
	if (FPlayerEntityInfo::Instance.IsSquadLeader && SquadMembers.Num() > 1)
	{
		QueueMessageBegin("ProxyBuildingMode")
			SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "MessageBox.OpenBuildingMode.Title", "Cooperative building mode"));
			SMessageBox::Get()->SetContent(NSLOCTEXT("Notify", "MessageBox.OpenBuildingMode.Desc1", "You are a squad leader, I wish to go to your base along with a squad?"));
			SMessageBox::Get()->SetButtonsText(FButtonNameLocalization::Yes, FText::GetEmpty(), FButtonNameLocalization::No);
			SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton button) {
				SMessageBox::Get()->ToggleWidget(false);

				if (button == EMessageBoxButton::Left)
				{
					FSearchSessionParams params;
					params.GameModeTypeId = EGameModeTypeId::Building;
					OnProxySearchSessionBegin(params);
				}
				else
				{
					squadController.LeaveFromSquad.Request(FPlayerEntityInfo::Instance.PlayerId);

					if (auto GameInst = CastChecked<UShooterGameInstance>(GetGameInstance()))
					{
						SMessageBox::RemoveQueueMessage("LoadingBuildingMode");
						GameInst->StartMessages.bIsTournamentRulesShowed = true;
						const auto gm = FGameModeTypeId::GetString(EGameModeTypeId::Building);
						const auto mp = FGameMapTypeId::GetString(EGameMapTypeId::Lobby);
						UGameplayStatics::OpenLevel(GetWorld(), *mp, true, FString::Printf(TEXT("?game=%s"), *gm));
					}
				}
			});
			SMessageBox::Get()->SetEnableClose(false);
			SMessageBox::Get()->ToggleWidget(true);
		QueueMessageEnd
	}
	else if (SquadMembers.Num() > 1)
	{
		QueueMessageBegin("ProxyBuildingMode")
			SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "MessageBox.OpenBuildingMode.Title", "Cooperative building mode"));
			SMessageBox::Get()->SetContent(NSLOCTEXT("Notify", "MessageBox.OpenBuildingMode.Desc2", "You are a member of the squad, want to go into your base leaving the squad?"));
			SMessageBox::Get()->SetButtonsText(FButtonNameLocalization::Yes, FText::GetEmpty(), FButtonNameLocalization::No);
			SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton button) {
				SMessageBox::Get()->ToggleWidget(false);

				if (button == EMessageBoxButton::Left)
				{
					squadController.LeaveFromSquad.Request(FPlayerEntityInfo::Instance.PlayerId);

					if (auto GameInst = CastChecked<UShooterGameInstance>(GetGameInstance()))
					{
						SMessageBox::RemoveQueueMessage("LoadingBuildingMode");
						GameInst->StartMessages.bIsTournamentRulesShowed = true;
						const auto gm = FGameModeTypeId::GetString(EGameModeTypeId::Building);
						const auto mp = FGameMapTypeId::GetString(EGameMapTypeId::Lobby);
						UGameplayStatics::OpenLevel(GetWorld(), *mp, true, FString::Printf(TEXT("?game=%s"), *gm));
					}
				}
			});
			SMessageBox::Get()->SetEnableClose(false);
			SMessageBox::Get()->ToggleWidget(true);
		QueueMessageEnd
	}
	else
	{
		if (auto GameInst = CastChecked<UShooterGameInstance>(GetGameInstance()))
		{
			SMessageBox::RemoveQueueMessage("LoadingBuildingMode");
			GameInst->StartMessages.bIsTournamentRulesShowed = true;
			const auto gm = FGameModeTypeId::GetString(EGameModeTypeId::Building);
			const auto mp = FGameMapTypeId::GetString(EGameMapTypeId::Lobby);
			UGameplayStatics::OpenLevel(GetWorld(), *mp, true, FString::Printf(TEXT("?game=%s"), *gm));
		}
	}
}

void ALobbyGameMode::OpenBuildingMode()
{
	if(FPlayerEntityInfo::Instance.IsSquadLeader && SquadMembers.Num() > 1)
	{
		SMessageBox::AddQueueMessage("OpenBuildingMode", FOnClickedOutside::CreateLambda([&]()
		{
			SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "MessageBox.OpenBuildingMode.Title", "Cooperative building mode"));
			SMessageBox::Get()->SetContent(FText::Format(NSLOCTEXT("Notify", "MessageBox.OpenBuildingMode.Desc", " You want to go into cooperative building mode with your party members: {0}. Preparation for the joint cooperative mode can take no more than one minute."), FText::FromString(FString::Join(GetSquadMemberNames(), TEXT(",")))));

			SMessageBox::Get()->SetButtonsText(FButtonNameLocalization::Yes, FText::GetEmpty(), FButtonNameLocalization::No);
			SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton button)
			{
				if (button == EMessageBoxButton::Left)
				{
					FSearchSessionParams params;
					params.GameModeTypeId = EGameModeTypeId::Building;
					OnProxySearchSessionBegin(params);
				}
				SMessageBox::Get()->ToggleWidget(false);
				SMessageBox::RemoveQueueMessage("OpenBuildingMode");
			});
			SMessageBox::Get()->SetEnableClose(false);
			SMessageBox::Get()->ToggleWidget(true);
		}));
	}
	else
	{
		if (auto GameInst = CastChecked<UShooterGameInstance>(GetGameInstance()))
		{
			SMessageBox::RemoveQueueMessage("LoadingBuildingMode");
			GameInst->StartMessages.bIsTournamentRulesShowed = true;
			const auto gm = FGameModeTypeId::GetString(EGameModeTypeId::Building);
			const auto mp = FGameMapTypeId::GetString(EGameMapTypeId::Lobby);
			UGameplayStatics::OpenLevel(GetWorld(), *mp, true, FString::Printf(TEXT("?game=%s"), *gm));
		}
	}
}

void ALobbyGameMode::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);

	bIntoBattle = (UGameplayStatics::GetIntOption(Options, TEXT("IntoBattle"), 0) >= 1);
}

void ALobbyGameMode::OnStartTutorial(const FStartTutorialView& InData)
{
	auto GameInst = Cast<UShooterGameInstance>(GetGameInstance());
	if (InData.TutorialId != INDEX_NONE && GameInst && GameInst->CurrentTutorialId != InData.TutorialId)
	{
		GameInst->CurrentTutorialId = InData.TutorialId;
		if (GameInst->GetTutorials().Contains(GameInst->CurrentTutorialId))
		{
			GameInst->GetTutorials().FindChecked(GameInst->CurrentTutorialId)->StartTutorial(InData.StepId);
		}
	}
}