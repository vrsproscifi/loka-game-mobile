#include "LokaGame.h"
#include "GameMode/LobbyGameMode.h"

#include "Slate/Notify/SNotifyList.h"
#include "Slate/Notify/SMessageBox.h"
#include "Slate/Lobby/SGeneralPage.h"
#include "Slate/Fractions/SFractionManager_Item.h"
#include "Slate/Fractions/SFractionManager.h"

#include "PlayerController/LobbyPlayerController.h"
#include "Localization/ButtonNameLocalization.h"


void ALobbyGameMode::BindPromoHandlers()
{
	promoController.ActivatePromo.OnSuccess.AddUObject(this, &ALobbyGameMode::OnActivatePromoResponse);
	promoController.ActivatePromo.OnCodeActivated.Handler.AddUObject(this, &ALobbyGameMode::OnActivatePromoCodeActivated);
	promoController.ActivatePromo.OnCodeNotFound.Handler.AddUObject(this, &ALobbyGameMode::OnActivatePromoCodeNotFound);
}

void ALobbyGameMode::BindSocialTaskHandlers()
{
	promoController.TakeSocialTask.OnSuccess.AddUObject(this, &ALobbyGameMode::OnTakeSocialTaskResponse);
	promoController.TakeSocialTask.OnCodeActivated.Handler.AddUObject(this, &ALobbyGameMode::OnTakeSocialTaskActivated);
	promoController.TakeSocialTask.OnCodeNotFound.Handler.AddUObject(this, &ALobbyGameMode::OnTakeSocialTaskNotFound);


	promoController.ListOfSocialTask.OnSuccess.AddUObject(this, &ALobbyGameMode::OnListOfSocialTaskResponse);
	promoController.StatusOfSocialTask.OnSuccess.AddUObject(this, &ALobbyGameMode::OnStatusOfSocialTaskResponse);
}


void ALobbyGameMode::OnActivatePromoResponse(const FActivatePromoResponse& response)
{
	////TODO: Added message box with bonus
	//
	//if (response.CategoryTypeId != CategoryTypeId::End)
	//{
	//	FUnlockItemResponseContainer ToUnlock;
	//	ToUnlock.Cost = FTypeCost();
	//	ToUnlock.ItemId = response.ItemId;
	//	ToUnlock.ItemType = response.CategoryTypeId;
	//	ToUnlock.ModelId = response.ModelId;
	//
	//	OnUnlockItem(ToUnlock);
	//}
	////else
	////{
	//
	//TSharedRef<SWidget> BonusWidget = SNullWidget::NullWidget;
	//
	//if (response.CategoryTypeId != CategoryTypeId::End)
	//{
	//	if (auto Item = LobbyPlayerController()->GetInventory().FindByStrId(response.ItemId, response.CategoryTypeId))
	//	{
	//		BonusWidget = SNew(SFractionManager_Item, Item)
	//			.IsEnabled(true);
	//
	//		LobbyPlayerController()->OnItemInstallFromList(Item);
	//	}
	//}
	//else
	//{
	//	BonusWidget = SNew(SResourceTextBox)
	//		.Type(response.Cash.Donate ? EResourceTextBoxType::Donate : EResourceTextBoxType::Money)
	//		.Value(response.Cash.Money);
	//}
	//
	//SMessageBox::AddQueueMessage("PromoCode.Succes", FOnClickedOutside::CreateLambda([&, w = BonusWidget]() {
	//	SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "Notify.PromoCode.Title", "Promo Code"));
	//	SMessageBox::Get()->SetContent(
	//		SNew(SVerticalBox)
	//		+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Center).Padding(FMargin(10, 6))
	//		[
	//			SNew(STextBlock)
	//			.Text(NSLOCTEXT("Notify", "Notify.PromoCode.Desc", "You successfully activated promo code and receive bonus."))
	//		.TextStyle(&FLokaSlateStyle::Get().GetWidgetStyle<FGeneralPageStyle>("SGeneralPageStyle").YourPlaceText)
	//		]
	//	+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Center).Padding(FMargin(10, 4))
	//		[
	//			w
	//		]
	//	);
	//	SMessageBox::Get()->SetButtonsText(FText::GetEmpty(), FButtonNameLocalization::Continue);
	//	SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton) {
	//		SMessageBox::Get()->ToggleWidget(false);
	//		SMessageBox::Get()->OnMessageBoxHidden.AddLambda([&]() { SMessageBox::RemoveQueueMessage("PromoCode.Succes"); });
	//	});
	//	SMessageBox::Get()->SetEnableClose(false);
	//	SMessageBox::Get()->ToggleWidget(true);
	//}));
	////}
	//
	//Slate_GeneralPage->ClearCodeInput();
}

void ALobbyGameMode::OnActivatePromoCodeNotFound()
{
	Slate_GeneralPage->ClearCodeInput();

	SMessageBox::AddQueueMessage("PromoCode.NotFound", FOnClickedOutside::CreateLambda([&]() {
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "Notify.PromoCode.Title", "Promo Code"));
		SMessageBox::Get()->SetContent(NSLOCTEXT("Notify", "Notify.PromoCode.NotFound.Desc", "Promo code was not found"));
		SMessageBox::Get()->SetButtonsText(FText::GetEmpty(), FButtonNameLocalization::Continue);
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton) {
			SMessageBox::Get()->ToggleWidget(false);
			SMessageBox::Get()->OnMessageBoxHidden.AddLambda([&]() { SMessageBox::RemoveQueueMessage("PromoCode.NotFound"); });
		});
		SMessageBox::Get()->SetEnableClose(false);
		SMessageBox::Get()->ToggleWidget(true);
	}));
}

void ALobbyGameMode::OnActivatePromoCodeActivated()
{
	Slate_GeneralPage->ClearCodeInput();

	SMessageBox::AddQueueMessage("PromoCode.Activated", FOnClickedOutside::CreateLambda([&]() {
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "Notify.PromoCode.Title", "Promo Code"));
		SMessageBox::Get()->SetContent(NSLOCTEXT("Notify", "Notify.PromoCode.Activated.Desc", "Promo code was are completed"));
		SMessageBox::Get()->SetButtonsText(FText::GetEmpty(), FButtonNameLocalization::Continue);
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton) {
			SMessageBox::Get()->ToggleWidget(false);
			SMessageBox::Get()->OnMessageBoxHidden.AddLambda([&]() { SMessageBox::RemoveQueueMessage("PromoCode.Activated"); });
		});
		SMessageBox::Get()->SetEnableClose(false);
		SMessageBox::Get()->ToggleWidget(true);
	}));
}