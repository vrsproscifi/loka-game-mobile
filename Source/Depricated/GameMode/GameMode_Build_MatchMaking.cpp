#include "LokaGame.h"
#include "GameMode_Build.h"
#include "GameState_Build.h"

#include "EntityRepository.h"
#include "ShooterGameInstance.h"
#include "BuildPlayerController.h"

#include "SMessageBox.h"
#include "SquadController/Models/SquadRespond.h"
#include "SNotifyList.h"
#include "SquadController/Models/SendSquadDuelInvite.h"
#include "SquadController/Models/SquadInviteDirectionType.h"
#include <OnlineSubsystemTypes.h>

//==============================================================
#define CALL_CTR_FNC(Fnc, ...) \
for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)\
{\
	if (auto TargetCtrl = Cast<ABuildPlayerController>(*It))\
	{\
		TargetCtrl->##Fnc(__VA_ARGS__);\
	}\
}

#define CALL_OWNER_CTR_FNC(Fnc, ...) \
if (auto TargetCtrl = GetSessionOwner())\
{\
	TargetCtrl->##Fnc(__VA_ARGS__);\
}

void AGameMode_Build::BindSquadHandlers()
{
	squadController.GetSquadInvites.OnSuccess.AddUObject(this, &AGameMode_Build::OnGetSquadDuelInvitesResponse);

	//==============================================================
	//						RespondDuelInvite
	//
	squadController.RespondDuelInvite.OnSuccess.AddUObject(this, &AGameMode_Build::OnRespondDuelInviteResponse);
	squadController.RespondDuelInvite.OnInviteNotFound.Handler.AddUObject(this, &AGameMode_Build::OnRespondSqaudInviteInviteNotFound);
	squadController.RespondDuelInvite.OnInviteAreRecived.Handler.AddUObject(this, &AGameMode_Build::OnRespondSqaudInviteInviteAreRecived);
	squadController.RespondDuelInvite.OnNotEnouthMoney.Handler.AddUObject(this, &AGameMode_Build::OnRespondDuelInviteNotEnouthMoney);
	squadController.RespondDuelInvite.OnSenderInSquad.Handler.AddUObject(this, &AGameMode_Build::OnRespondSqaudInviteSenderInSquad);
	squadController.RespondDuelInvite.OnSenderNotEnouthMoney.Handler.AddUObject(this, &AGameMode_Build::OnRespondDuelInviteSenderNotEnouthMoney);

	//lobbyController.SearchStatus.OnSuccess.AddUObject(this, &AGameMode_Build::OnSessionMatchFounded);
	lobbyController.GetMatchResults.OnSuccess.AddUObject(this, &AGameMode_Build::OnGetMatchResults);
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//						Respons to client
void AGameMode_Build::OnRespondSqaudInviteResponse(const FRequestOneParam<FString>& playerName)					{ CALL_OWNER_CTR_FNC(ClientRespondSqaudInvite, playerName); }
void AGameMode_Build::OnRespondSqaudInviteInviteNotFound()														{ CALL_OWNER_CTR_FNC(ClientRespondSqaudInviteNotFound); }
void AGameMode_Build::OnRespondSqaudInviteInviteAreRecived(const FRequestOneParam<FString>& playerName)			{ CALL_OWNER_CTR_FNC(ClientRespondSqauInviteAreRecived, playerName); }
void AGameMode_Build::OnRespondSqaudInviteSenderInSquad(const FRequestOneParam<FString>& playerName)			{ CALL_OWNER_CTR_FNC(ClientRespondSenderInSquad, playerName); }
void AGameMode_Build::OnGetSquadDuelInvitesResponse(const FSquadInviteListContainer& invites)					{ CALL_OWNER_CTR_FNC(ClientSquadDuelInvitesResponse, invites); }
void AGameMode_Build::OnRespondDuelInviteNotEnouthMoney(const FRequestOneParam<FString>& playerName)			{ CALL_OWNER_CTR_FNC(ClientRespondDuelInviteNoMoney, playerName); }
void AGameMode_Build::OnRespondDuelInviteSenderNotEnouthMoney(const FRequestOneParam<FString>& playerName)		{ CALL_OWNER_CTR_FNC(ClientRespondDuelSenderNoMoney, playerName); }
void AGameMode_Build::OnGetMatchResults(const TArray<FLobbyMatchResultPreview>& results)						{ CALL_CTR_FNC(ClientMatchResults, results); }
void AGameMode_Build::OnRespondDuelInviteResponse(const FRequestOneParam<FString>& playerName)					{ CALL_OWNER_CTR_FNC(ClientRespondDuelInvite, playerName); }

void AGameMode_Build::ProxySendDuelAnswer(const FString& InInviteId, const bool InAnswer)
{
	squadController.RespondDuelInvite.Request(FSquadRespond(InInviteId, InAnswer));
}

//void AGameMode_Build::OnSessionMatchFounded(const FSearchSessionStatus& InResponse)
//{ 
//	if(GetNetMode() == NM_DedicatedServer && InResponse.GameModeTypeId != EGameModeTypeId::Building)
//	{
//		if (InResponse.Status == ESearchSessionStatus::Complete)
//		{
//			GetWorld()->GetTimerManager().SetTimer(ShutDownTimerHandler, this, &AGameMode_Build::OnServerShutDown, 5.0f, false, 5.0f);
//			const auto gm = FGameModeTypeId::GetString(EGameModeTypeId::Lobby);
//			const auto mp = FGameMapTypeId::GetString(EGameMapTypeId::Lobby);
//			for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
//			{
//				(*It)->ClientTravel(*FString::Printf(TEXT("%s?game=%s"), *mp, *gm), TRAVEL_Absolute);
//			}
//		}
//		else
//		{
//			CALL_CTR_FNC(ClientSessionMatchFounded, InResponse);
//		}
//	}
//	else if (GetNetMode() != NM_DedicatedServer)
//	{
//		if (InResponse.Status == ESearchSessionStatus::Complete)
//		{
//			auto SharedToken = MakeShareable(new FUniqueNetIdString(InResponse.Token));
//			auto pc = GetWorld()->GetFirstPlayerController();
//			pc->GetLocalPlayer()->SetCachedUniqueNetId(SharedToken);
//			pc->ClientTravel(InResponse.Host, TRAVEL_Absolute);	
//		}
//		else
//		{
//			CALL_CTR_FNC(ClientSessionMatchFounded, InResponse);
//		}
//	}
//}
//
////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
//void AGameMode_Build::TravelToGame(const FSearchSessionStatus& response)
//{
//	// TODO: ����� ����� ����������
//	/*
//	FGuid token;
//	if (response.Host.IsEmpty() == false && FGuid::Parse(response.Token, token))
//	{
//		auto SharedToken = MakeShareable(new FUniqueNetIdString(response.Token));
//		auto world = GetWorld();
//		auto Player = world->GetFirstLocalPlayerFromController();
//		Player->SetCachedUniqueNetId(SharedToken);
//		world->GetFirstPlayerController()->ClientTravel(response.Host, TRAVEL_Absolute);
//	}
//	*/
//}