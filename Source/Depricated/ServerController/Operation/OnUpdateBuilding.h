#pragma once
#include "ServiceHandler.h"
#include "ServiceOperation.h"
#include "RequestOneParam.h"
#include "../Models/MatchBuildingView.h"

namespace Operation
{
	namespace OnUpdateBuilding
	{
		class Operation final
			: public COperation<FMatchBuildingUpdateContainer, ObjectSerializer::JsonObject, void, ObjectSerializer::NativeObject>
		{
		public:
			Operation()
				: Super(FServerHost::MasterServer, "Node/OnUpdateBuilding")
			{

			}
		};
	}
}