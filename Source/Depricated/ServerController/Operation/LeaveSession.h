#pragma once
#include "ServiceOperation.h"
#include "RequestOneParam.h"

namespace Operation
{
	namespace LeaveSession
	{
		class Operation final
			: public COperation<FRequestOneParam<FString>, ObjectSerializer::NativeObject, void, ObjectSerializer::NativeObject>
		{
		public:
			Operation()
				: Super(FServerHost::MasterServer, "Node/LeaveSession")
			{

			}
		};
	}
}