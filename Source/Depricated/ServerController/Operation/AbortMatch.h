#pragma once
#include "ServiceHandler.h"
#include "ServiceOperation.h"


namespace Operation
{
	namespace AbortMatch
	{
		struct Request : public FOnlineJsonSerializable
		{
			virtual ~Request() { }
			int32 State;

			BEGIN_ONLINE_JSON_SERIALIZER
				ONLINE_JSON_SERIALIZE("State", State);
			END_ONLINE_JSON_SERIALIZER
		};

		class Operation final
			: public COperation<Request, ObjectSerializer::NativeObject, void, ObjectSerializer::NativeObject>
		{
		public:
			Operation()
				: Super(FServerHost::MasterServer, "Node/AbortMatch")
			{

			}
		};
	}
}
