#pragma once
#include "ServiceHandler.h"
#include "ServiceOperation.h"
#include "PoolingOperation.h"



namespace Operation
{
	namespace NodeHeartBeat
	{
		class Operation final 
			: public COperation<void, ObjectSerializer::NativeObject, void, ObjectSerializer::NativeObject>
			, public FPoolingOperation
		{
		public:
			Operation() 
				: Super(FServerHost::MasterServer, "Node/HeartBeat")
				, FPoolingOperation(2.5f, 5.0f)
			{
				TimerDelegate.BindRaw(this, &Operation::Request);
			}
		};
	}
}