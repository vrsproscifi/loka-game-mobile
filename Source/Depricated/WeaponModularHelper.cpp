// VRSPRO

#include "LokaGame.h"
#include "WeaponModularHelper.h"

#include "Module/ItemModuleEntity.h"
#include "Material/ItemMaterialEntity.h"
#include "Weapon/ItemWeaponEntity.h"
#include "EntityRepository.h"
#include "ShooterGameInstance.h"

FWeaponModularHelper::FWeaponModularHelper()
{
	
}

void FWeaponModularHelper::ClearModules()
{
	TargetModulesComponent = nullptr;
	if (InstalledModulesMesh.Num())
	{
		for (auto Component : InstalledModulesMesh)
		{
			Component->DestroyComponent(true);
		}
	}

	InstalledModulesMesh.Empty();
}

void FWeaponModularHelper::InitializeModules(const UItemWeaponEntity* Source, const bool IsUseSlot)
{
	if (TargetModulesComponent && Source && Owner)
	{
		const auto Slot = IsUseSlot ? Source->GetDescription().SlotType : ItemSlotTypeId::None;
		FString SlotString = IsUseSlot ? FString((Slot == ItemSlotTypeId::PrimaryWeapon ? "PrimaryWeapon" : "SecondaryWeapon")) : "";
		TargetModulesComponent->SetMaterial(0, Source->GetCurrentMaterial());

		for (auto &s : InstalledModulesMeshMap)
		{
			if (s.Key.ToString().Find(SlotString) != INDEX_NONE)
			{
				s.Value->SetVisibility(false, true);
				InstalledModulesMeshMap.Remove(s.Key);
			}
		}

		for (auto s : Source->SupportedSockets)
		{
			for (auto &c : InstalledModulesMesh)
			{
				if (!c->IsVisible())
				{
					InstalledModulesMeshMap.Add(*FString(SlotString + s.ToString()), c);
					c->SetSkeletalMesh(nullptr);
					c->AttachToComponent(TargetModulesComponent, FAttachmentTransformRules::SnapToTargetIncludingScale, s);
					c->bComponentUseFixedSkelBounds = true;
					c->SetVisibility(true, true);
					break;
				}
			}
		}
		
		auto GameInst = Cast<UShooterGameInstance>(Owner->GetWorld()->GetGameInstance());
		if (GameInst)
		{
			auto Repository = GameInst->EntityRepository->InstanceRepository;

			if (Source->DefaultModules.Num())
			{
				TArray<uint8> NoInstall;
				for (auto i : Source->DefaultModules)
				{
					if (i >= 0 && i < ItemModuleModelId::End && Repository.Module[i])
					{
						for (auto &m : Source->InstalledModules)
						{
							InstallModule(m, Slot);

							if (m->TargetSocket == Repository.Module[i]->TargetSocket)
							{
								NoInstall.Add(i);
							}
						}
					}
				}

				for (auto i : Source->DefaultModules)
				{
					if (NoInstall.Contains(i) == false)
					{
						if (i >= 0 && i < ItemModuleModelId::End && Repository.Module[i])
						{
							InstallModule(Repository.Module[i], Slot);
						}
					}
				}
			}
			else
			{
				for (auto i : Source->SupportedModules)
				{
					if (i >= 0 && i < ItemModuleModelId::End && Repository.Module[i])
					{
						for (auto &m : Source->InstalledModules)
						{
							InstallModule(m, Slot);
						}
					}
				}
			}
		}
	}
}

void FWeaponModularHelper::InitializeModules(const UItemWeaponEntity* Source, const TArray<TEnumAsByte<ItemModuleModelId::Type>>& Modules, const TEnumAsByte<ItemMaterialModelId::Type>& Skin)
{

}

bool FWeaponModularHelper::InstallModule(const UItemModuleEntity* InModule, const ItemSlotTypeId::Type& Slot)
{
	FName SlotString = (Slot == ItemSlotTypeId::None) ? *InModule->TargetSocket.ToString() : *FString((Slot == ItemSlotTypeId::PrimaryWeapon ? "PrimaryWeapon" : "SecondaryWeapon") + InModule->TargetSocket.ToString());

	if (InstalledModulesMeshMap.Contains(SlotString))
	{
		InstalledModulesMeshMap[SlotString]->SetSkeletalMesh(InModule->GetSkeletalMesh());
		return true;
	}

	return false;
}

bool FWeaponModularHelper::UninstallModule(const UItemModuleEntity* InModule, const ItemSlotTypeId::Type& Slot)
{
	FName SlotString = (Slot == ItemSlotTypeId::None) ? *InModule->TargetSocket.ToString() : *FString((Slot == ItemSlotTypeId::PrimaryWeapon ? "PrimaryWeapon" : "SecondaryWeapon") + InModule->TargetSocket.ToString());

	if (InstalledModulesMeshMap.Contains(SlotString))
	{
		InstalledModulesMeshMap[SlotString]->SetSkeletalMesh(nullptr);
		return true;
	}

	return false;
}

//void FWeaponModularHelper::AddReferencedObjects(FReferenceCollector& Collector)
//{
//	Collector.AddReferencedObjects(InstalledModulesMesh);
//	Collector.AddReferencedObjects(InstalledModulesMeshMap);1
//	Collector.AddReferencedObjects(TargetModulesComponent);
//}