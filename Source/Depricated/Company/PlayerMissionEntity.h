#pragma once

#include "PlayerMissionState.h"
#include "PlayerMissionProgress.h"
#include "PlayerMissionEntity.generated.h"

USTRUCT()
struct FPlayerMissionEntity
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Transient)		FPlayerMissionProgress MissionProgress;
	UPROPERTY(Transient)		FString PlayerMissionId;
	UPROPERTY(Transient)		int32 MissionTaskId;
	UPROPERTY(Transient)		PlayerMissionState MissionState;
	UPROPERTY(Transient)		int64 StartDate;
	UPROPERTY(Transient)		int64 CompleteDate;
};