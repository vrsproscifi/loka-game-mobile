#pragma once

#include "ScenarioCompanyId.h"
#include "ScenarioTaskEntity.h"
#include "PlayerMissionEntity.h"
#include "ScenarioMissionEntity.generated.h"

USTRUCT(NotBlueprintable)
struct FScenarioMissionView
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Transient)		int32 MissionId;
	UPROPERTY(Transient)		int32 AbstractMissionId;
	UPROPERTY(Transient)		int32 NextMissionId;
	UPROPERTY(Transient)		TEnumAsByte<EScenarioCompanyId::Type> CompanyId;
	UPROPERTY(Transient)		TArray<FScenarioTaskView> TaskViews;
};

USTRUCT(BlueprintType)
struct FScenarioMissionEntity
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly) FString Name;
	UPROPERTY(EditDefaultsOnly) FString Description;
	UPROPERTY(EditDefaultsOnly) FSlateBrush Image;
	UPROPERTY(EditDefaultsOnly) TArray<FScenarioTaskEntity> Tasks;
	UPROPERTY(Transient)		FScenarioMissionView Property;
	const struct FScenarioCompanyEntity* Company;
};