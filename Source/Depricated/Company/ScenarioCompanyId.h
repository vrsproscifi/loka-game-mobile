#pragma once
#include "ScenarioCompanyId.generated.h"

UENUM(BlueprintType)
namespace EScenarioCompanyId
{
	enum Type
	{
		MainCompany,
		Keepers,
		End UMETA(Hidden)
	};
}