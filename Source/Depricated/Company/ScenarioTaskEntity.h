#pragma once

#include "ScenarioTaskEntity.generated.h"


USTRUCT(NotBlueprintable)
struct FScenarioTaskView
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Transient)		int32 TaskId;
	UPROPERTY(Transient)		int32 TargetAmount;
	UPROPERTY(Transient)		int32 EquipmentLevel;
	UPROPERTY(Transient)		int32 AbstractTaskId;
	UPROPERTY(Transient)		int64 PeriodOfExecutionInSeconds;
};

USTRUCT(BlueprintType)
struct FScenarioTaskEntity
{
	GENERATED_USTRUCT_BODY()
	UPROPERTY(EditDefaultsOnly) FString Name;
	UPROPERTY(EditDefaultsOnly) FString Description;
	UPROPERTY(EditDefaultsOnly) FSlateBrush Image;
	UPROPERTY(Transient)		FScenarioTaskView Property;
	const struct FScenarioCompanyEntity* Company;
	const struct FScenarioMissionEntity* Mission;
};
