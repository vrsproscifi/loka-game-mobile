#pragma once
#include "ServiceOperation.h"
#include "../Models/PreSetEquip.h"

namespace Operation
{
	namespace ItemUnEquip
	{
		class Operation final :// public COperation<FPreSetEquipRequest, ObjectSerializer::JsonObject, FPreSetEquipResponse, ObjectSerializer::JsonObject>
		{
		public:
			Operation() : Super(FServerHost::MasterClient, "Hangar/ItemEquip") { }

		};
	}
}