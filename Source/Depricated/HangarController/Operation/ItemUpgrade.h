#pragma once
#include "ServiceHandler.h"
#include "ServiceOperation.h"
#include "IdentityController/Models/ModificationItemWrapper.h"
#include "IdentityController/Models/InvertoryItemWrapper.h"

namespace Operation
{
	namespace ItemUpgrade
	{
		struct Request final : FOnlineJsonSerializable
		{
			virtual ~Request() { }

			FString ItemId;

			int32 CategoryId;

			bool IsDonate;

			BEGIN_ONLINE_JSON_SERIALIZER
				ONLINE_JSON_SERIALIZE("ItemId", ItemId);
				ONLINE_JSON_SERIALIZE("CategoryId", CategoryId);
				ONLINE_JSON_SERIALIZE("IsDonate", IsDonate);
			END_ONLINE_JSON_SERIALIZER
		};

		class Operation final :// public COperation<ItemUpgrade::Request, ObjectSerializer::NativeObject, TArray<FItemModification>, ObjectSerializer::JsonArrayObject>
		{
		public:
			Operation() : Super(FServerHost::MasterClient, "Hangar/ItemUpgrade") { }

		};

		DECLARE_DELEGATE_OneParam(FOnGetUpdateList, const Request&);
	}

}