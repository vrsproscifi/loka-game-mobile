#pragma once
#include "ServiceHandler.h"
#include "ServiceOperation.h"
#include "../Models/AddonEquip.h"

namespace Operation
{
	namespace AddonEquip
	{
		class Operation final 
			:// public COperation<AddonEquip::Request, ObjectSerializer::NativeObject, AddonEquip::Response, ObjectSerializer::NativeObject>
		{
		public:
			IResponseHandler<void, OperationResponseType::None> OnPaymentRequired;

			Operation() 
				: Super(FServerHost::MasterClient, "Hangar/AddonEquip")
				, OnPaymentRequired(402, this)
			{ 
			
			}

		};
	}
}