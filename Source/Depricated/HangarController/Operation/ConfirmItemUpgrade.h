#pragma once
#include "ServiceHandler.h"
#include "ServiceOperation.h"
#include "IdentityController/Models/ModificationItemWrapper.h"
#include "IdentityController/Models/InvertoryItemWrapper.h"

namespace Operation
{
	namespace ConfirmItemUpgrade
	{
		struct Request final : FOnlineJsonSerializable
		{
			virtual ~Request() { }

			FString ItemId;

			int32 CategoryId;

			int32 ModificationId;

			BEGIN_ONLINE_JSON_SERIALIZER
				ONLINE_JSON_SERIALIZE("ItemId", ItemId);
			ONLINE_JSON_SERIALIZE("CategoryId", CategoryId);
			ONLINE_JSON_SERIALIZE("ModificationId", ModificationId);
			END_ONLINE_JSON_SERIALIZER
		};

		class Operation final :// public COperation<ConfirmItemUpgrade::Request, ObjectSerializer::NativeObject, FInvertoryItemWrapper, ObjectSerializer::JsonObject>
		{
		public:
			Operation() 
				: Super(FServerHost::MasterClient, "Hangar/ConfirmItemUpgrade") 
				, OnPaymentRequired(402, this)
			{ 

			}
			IResponseHandler<void, OperationResponseType::None> OnPaymentRequired;
		};

		DECLARE_DELEGATE_OneParam(FOnConfirmUpdate, const Request&);
	}
}