// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"

DECLARE_DELEGATE_OneParam(FOnClickedTeam, const FString&)

class LOKAGAME_API STournamentTree : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(STournamentTree)
	{}
	SLATE_EVENT(FOnClicked, OnClickedJoin)
	SLATE_EVENT(FOnClickedTeam, OnClickedTeam)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

	void SetActiveStep(const int32 &Index);
	void SetTeamPair(const int32 &Index, const int32& PairIndex, const struct FTournamentMatchDetails &PairTeam);

protected:

	FOnClicked OnClickedJoin;

	TArray<TSharedPtr<class STournamentTreeStep>> Steps;

	TArray<TSharedPtr<class STournamentTreeItem>> Items;

	virtual int32 OnPaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyClippingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const override;
	void GetChildPositionAndSize(const FGeometry& MyGeometry, TSharedRef<SWidget> WidgetToFind, FVector2D &OutPosition, FVector2D &OutSize) const;
};
