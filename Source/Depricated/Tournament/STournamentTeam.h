// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/TournamentTeamWidgetStyle.h"
#include "Components/SResourceTextBox.h"

DECLARE_DELEGATE_OneParam(FOnTournamentTeamConfirm, const TArray<FString>&)

class LOKAGAME_API STournamentTeam : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(STournamentTeam)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FTournamentTeamStyle>("STournamentTeamStyle"))
		, _LeagueBalance(500)
		, _OneMemberCost(5)
		, _TypeBalance(EResourceTextBoxType::Donate)
	{}
	SLATE_STYLE_ARGUMENT(FTournamentTeamStyle, Style)
	SLATE_ATTRIBUTE(int32, LeagueBalance)
	SLATE_ARGUMENT(int32, OneMemberCost)
	SLATE_ARGUMENT(EResourceTextBoxType::Type, TypeBalance)
	SLATE_EVENT(FOnTournamentTeamConfirm, OnTournamentTeamConfirm)
	SLATE_EVENT(FOnClicked, OnTournamentTeamPay)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

	void AppendRow(const struct FLeaguePlayerRow &Row, const bool IsTeam = false);
	void AppendRows(const TArray<struct FLeaguePlayerRow> &Rows, const bool IsTeam = false);
	void ClearRows(const bool IsTeam = false);

	void SetOneMemberCost(const int32&);
	void SetBalanceValue(const TAttribute<int32>&);
	void SetBalanceType(const TAttribute<EResourceTextBoxType::Type>&);

protected:

	FOnTournamentTeamConfirm OnTournamentTeamConfirm;

	TArray<TSharedPtr<struct FLeaguePlayerRow>> PlayersList;
	TSharedPtr<SListView<TSharedPtr<struct FLeaguePlayerRow>>> PlayersListWidget;

	TSharedRef<ITableRow> OnGeneratePlayerRow(TSharedPtr<struct FLeaguePlayerRow> InItem, const TSharedRef<STableViewBase>& OwnerTable, const bool IsTeam);

	TArray<TSharedPtr<struct FLeaguePlayerRow>> PlayersListTeam;
	TSharedPtr<SListView<TSharedPtr<struct FLeaguePlayerRow>>> PlayersListWidgetTeam;

	void OnPlayersListSort(EColumnSortPriority::Type, const FName&, EColumnSortMode::Type, const bool IsTeam);
	EColumnSortMode::Type GetColumnSortMode(const FName ColumnName, const bool IsTeam) const;

	void StartSort(const bool IsTeam);

	FReply OnPlayersMove(const bool IsTeam);
	FReply OnPlayersConfirm();

	FName SortByColumn[2];
	EColumnSortMode::Type SortMode[2];

	const FTournamentTeamStyle *Style;

	int32 OneMemberCost;
	TAttribute<int32> LeagueBalanceValue;
	TAttribute<EResourceTextBoxType::Type> LeagueBalanceType;

	int32 GetCurrentTeamCost() const
	{
		return OneMemberCost * PlayersListTeam.Num();
	}

	int32 GetCurrentMemberCost() const
	{
		return OneMemberCost;
	}

	int32 GetCurrentBalanceValue() const
	{
		return LeagueBalanceValue.Get() - GetCurrentTeamCost();
	}

	EResourceTextBoxType::Type GetCurrentBalanceType() const
	{
		return LeagueBalanceType.Get();
	}
};
