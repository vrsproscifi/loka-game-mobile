// VRSPRO

#include "LokaGame.h"
#include "STournamentTeam.h"
#include "SlateOptMacros.h"

#include "Slate/League/SLeagueFormInfo.h"

#include "STextBlockButton.h"

class STournamentTeamRow : public SMultiColumnTableRow< TSharedPtr<FLeaguePlayerRow> >
{
public:

	SLATE_BEGIN_ARGS(STournamentTeamRow)
		: _Item()
	{}
	SLATE_EVENT(FOnCanAcceptDrop, OnCanAcceptDrop)
	SLATE_EVENT(FOnAcceptDrop, OnAcceptDrop)
	SLATE_EVENT(FOnDragDetected, OnDragDetected)
	SLATE_ARGUMENT(TSharedPtr<FLeaguePlayerRow>, Item)
	SLATE_END_ARGS()


	virtual TSharedRef<SWidget> GenerateWidgetForColumn(const FName& ColumnName) override
	{
		if (ColumnName == FLeaguePlayerRow::ColumnNumber)
		{
			return SNew(STextBlock).Text(FText::AsNumber(Item->Number, &FNumberFormattingOptions::DefaultNoGrouping()));
		}
		else if (ColumnName == FLeaguePlayerRow::ColumnName)
		{
			return SNew(STextBlock).Text(FText::FromString(Item->Name));
		}
		else if (ColumnName == FLeaguePlayerRow::ColumnRole)
		{
			return SNew(STextBlock).Text(ELeaguePlayerRole::Localization[Item->Role]);
		}
		else if (ColumnName == FLeaguePlayerRow::ColumnKillsAvg)
		{
			return SNew(STextBlock).Text(FText::AsNumber(Item->KillsAvg, &FNumberFormattingOptions::DefaultWithGrouping()));
		}
		else if (ColumnName == FLeaguePlayerRow::ColumnActivity)
		{
			FTimespan Timespan = FTimespan::FromSeconds(Item->LastActivity);

			FText LastTime = NSLOCTEXT("SLeaguePlayerRow", "LastActivity.Online", "Online");

			if (Timespan.GetTotalDays() > 1)
			{
				LastTime = FText::Format(NSLOCTEXT("SLeaguePlayerRow", "LastActivity.Days", "{0} Days later"), FText::AsNumber(static_cast<int32>(Timespan.GetTotalDays())));
			}
			else if (Timespan.GetTotalHours() > 1)
			{
				LastTime = FText::Format(NSLOCTEXT("SLeaguePlayerRow", "LastActivity.Hours", "{0} Hours later"), FText::AsNumber(static_cast<int32>(Timespan.GetTotalHours())));
			}
			else if (Timespan.GetTotalMinutes() > 1)
			{
				LastTime = FText::Format(NSLOCTEXT("SLeaguePlayerRow", "LastActivity.Minutes", "{0} Minutes later"), FText::AsNumber(static_cast<int32>(Timespan.GetTotalMinutes())));
			}

			return SNew(STextBlock).Text(LastTime);
		}
		else
		{
			return SNew(STextBlock).Text(FText::Format(NSLOCTEXT("STournamentTeamRow", "UnsupprtedColumnText", "Unsupported Column: {0}"), FText::FromName(ColumnName)));
		}
	}

	void Construct(const FArguments& InArgs, const TSharedRef<STableViewBase>& InOwnerTableView)
	{
		Item = InArgs._Item;

		FSuperRowType::Construct(FSuperRowType::FArguments()
			.OnCanAcceptDrop(InArgs._OnCanAcceptDrop)
			.OnAcceptDrop(InArgs._OnAcceptDrop)
			.OnDragDetected(InArgs._OnDragDetected)
			.Padding(1)
			, InOwnerTableView);
	}

private:

	TSharedPtr<FLeaguePlayerRow> Item;
};

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void STournamentTeam::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	OneMemberCost = InArgs._OneMemberCost;
	LeagueBalanceValue = InArgs._LeagueBalance;
	LeagueBalanceType = InArgs._TypeBalance;
	
	OnTournamentTeamConfirm = InArgs._OnTournamentTeamConfirm;

	ChildSlot
	[
		SNew(SVerticalBox)
		+ SVerticalBox::Slot()
		.AutoHeight()
		.Padding(FMargin(40, 10, 40, 0))
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
			.FillWidth(1).VAlign(VAlign_Center)
			[
				SNew(STextBlock)
				.Text(FText::Format(NSLOCTEXT("STournamentTeam", "Header.TournamentName", "TOURNAMENT \"{0}\""), FText::FromString("GOLDEN")))
				.TextStyle(&Style->HeaderText)
			]
			+ SHorizontalBox::Slot()
			.AutoWidth().VAlign(VAlign_Center)
			[
				SNew(STextBlock)
				.Text(NSLOCTEXT("STournamentTeam", "Header.OneMemberCost", "ONE MEMBER COST: "))
				.TextStyle(&Style->SubHeaderTexts)
			]
			+ SHorizontalBox::Slot()
			.AutoWidth().VAlign(VAlign_Center)
			[
				SNew(SDPIScaler).DPIScale(Style->BalanceDPI)
				[
					SNew(SResourceTextBox)
					.Type(this, &STournamentTeam::GetCurrentBalanceType)
					.Value(this, &STournamentTeam::GetCurrentMemberCost)
				]
			]
		]
		+ SVerticalBox::Slot()
		.AutoHeight()
		.Padding(FMargin(40, 4, 40, 0))
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot().HAlign(HAlign_Left)
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot()
				.AutoWidth()
				.VAlign(VAlign_Center)
				[
					SNew(STextBlock)
					.Text(NSLOCTEXT("STournamentTeam", "Header.LeagueBalance", "LEAGUE BALANCE: "))
					.TextStyle(&Style->SubHeaderTexts)
				]
				+ SHorizontalBox::Slot()
				.AutoWidth()
				.HAlign(HAlign_Left)
				.VAlign(VAlign_Center)
				[
					SNew(SDPIScaler).DPIScale(Style->BalanceDPI)
					[
						SNew(SResourceTextBox)
						.Type(this, &STournamentTeam::GetCurrentBalanceType)
						.Value(this, &STournamentTeam::GetCurrentBalanceValue)
					]
				]
				+ SHorizontalBox::Slot()
				.FillWidth(1).VAlign(VAlign_Center)
				[
					SNew(STextBlockButton)
					.Text(NSLOCTEXT("STournamentTeam", "Header.Pay", "PAY"))
					.Style(&Style->TextBlockButton)
					.OnClicked(InArgs._OnTournamentTeamPay)
				]
			]
			+ SHorizontalBox::Slot().HAlign(HAlign_Right)
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot()
				.FillWidth(1)
				.VAlign(VAlign_Center)
				[
					SNew(STextBlock)
					.Text(NSLOCTEXT("STournamentTeam", "Header.TeamCost", "TEAM COST: "))
					.TextStyle(&Style->SubHeaderTexts)
				]
				+ SHorizontalBox::Slot()
				.AutoWidth()
				.HAlign(HAlign_Right)
				.VAlign(VAlign_Center)
				[
					SNew(SDPIScaler).DPIScale(Style->BalanceDPI)
					[
						SNew(SResourceTextBox)
						.Type(this, &STournamentTeam::GetCurrentBalanceType)
						.Value(this, &STournamentTeam::GetCurrentTeamCost)
					]
				]
			]
		]
		+ SVerticalBox::Slot()
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
			[
				SAssignNew(PlayersListWidget, SListView<TSharedPtr<FLeaguePlayerRow>>)
				.ConsumeMouseWheel(EConsumeMouseWheel::WhenScrollingPossible)
				.SelectionMode(ESelectionMode::Multi)
				.OnGenerateRow(this, &STournamentTeam::OnGeneratePlayerRow, false)
				.ListItemsSource(&PlayersList)
				.HeaderRow
				(
					SNew(SHeaderRow)
					+ SHeaderRow::Column(FLeaguePlayerRow::ColumnNumber).FillWidth(0.2f).OnSort(this, &STournamentTeam::OnPlayersListSort, false).SortMode(this, &STournamentTeam::GetColumnSortMode, FLeaguePlayerRow::ColumnNumber, false)
					[
						SNew(STextBlock).Text(NSLOCTEXT("SLeagueFormInfo", "TableHeader.Number", "#"))
					]
					+ SHeaderRow::Column(FLeaguePlayerRow::ColumnName).FillWidth(1).OnSort(this, &STournamentTeam::OnPlayersListSort, false).SortMode(this, &STournamentTeam::GetColumnSortMode, FLeaguePlayerRow::ColumnName, false)
					[
						SNew(STextBlock).Text(NSLOCTEXT("SLeagueFormInfo", "TableHeader.Name", "Player Name"))
					]
					+ SHeaderRow::Column(FLeaguePlayerRow::ColumnRole).FillWidth(1).OnSort(this, &STournamentTeam::OnPlayersListSort, false).SortMode(this, &STournamentTeam::GetColumnSortMode, FLeaguePlayerRow::ColumnRole, false)
					[
						SNew(STextBlock).Text(NSLOCTEXT("SLeagueFormInfo", "TableHeader.Role", "Player Role"))
					]
					+ SHeaderRow::Column(FLeaguePlayerRow::ColumnKillsAvg).FillWidth(1).OnSort(this, &STournamentTeam::OnPlayersListSort, false).SortMode(this, &STournamentTeam::GetColumnSortMode, FLeaguePlayerRow::ColumnKillsAvg, false)
					[
						SNew(STextBlock).Text(NSLOCTEXT("SLeagueFormInfo", "TableHeader.KillsAvg", "Kills / Deaths"))
					]
					+ SHeaderRow::Column(FLeaguePlayerRow::ColumnActivity).FillWidth(1).OnSort(this, &STournamentTeam::OnPlayersListSort, false).SortMode(this, &STournamentTeam::GetColumnSortMode, FLeaguePlayerRow::ColumnActivity, false)
					[
						SNew(STextBlock).Text(NSLOCTEXT("SLeagueFormInfo", "TableHeader.Activity", "Last Activity"))
					]
				)
			]
			+ SHorizontalBox::Slot().AutoWidth()
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot()
				[
					SNew(SButton)
					.HAlign(EHorizontalAlignment::HAlign_Center)
					.VAlign(EVerticalAlignment::VAlign_Center)
					.ButtonStyle(&Style->Buttons)
					.TextStyle(&Style->ButtonsTexts)
					.Text(FText::FromString(">>"))
					.OnClicked(this, &STournamentTeam::OnPlayersMove, true)
				]
				+ SVerticalBox::Slot()
				[
					SNew(SButton)
					.HAlign(EHorizontalAlignment::HAlign_Center)
					.VAlign(EVerticalAlignment::VAlign_Center)
					.ButtonStyle(&Style->Buttons)
					.TextStyle(&Style->ButtonsTexts)
					.Text(FText::FromString("<<"))
					.OnClicked(this, &STournamentTeam::OnPlayersMove, false)
				]
				+ SVerticalBox::Slot()
				[
					SNew(SButton)
					.HAlign(EHorizontalAlignment::HAlign_Center)
					.VAlign(EVerticalAlignment::VAlign_Center)
					.ButtonStyle(&Style->Buttons)
					.TextStyle(&Style->ButtonsTexts)
					.Text(NSLOCTEXT("STournamentTeam", "Done", "DONE"))
					.OnClicked(this, &STournamentTeam::OnPlayersConfirm)
				]
			]
			+ SHorizontalBox::Slot()
			[
				SAssignNew(PlayersListWidgetTeam, SListView<TSharedPtr<FLeaguePlayerRow>>)
				.ConsumeMouseWheel(EConsumeMouseWheel::WhenScrollingPossible)
				.SelectionMode(ESelectionMode::Multi)
				.OnGenerateRow(this, &STournamentTeam::OnGeneratePlayerRow, true)
				.ListItemsSource(&PlayersListTeam)
				.HeaderRow
				(
					SNew(SHeaderRow)
					+ SHeaderRow::Column(FLeaguePlayerRow::ColumnNumber).FillWidth(0.2f).OnSort(this, &STournamentTeam::OnPlayersListSort, true).SortMode(this, &STournamentTeam::GetColumnSortMode, FLeaguePlayerRow::ColumnNumber, true)
					[
						SNew(STextBlock).Text(NSLOCTEXT("SLeagueFormInfo", "TableHeader.Number", "#"))
					]
					+ SHeaderRow::Column(FLeaguePlayerRow::ColumnName).FillWidth(1).OnSort(this, &STournamentTeam::OnPlayersListSort, true).SortMode(this, &STournamentTeam::GetColumnSortMode, FLeaguePlayerRow::ColumnName, true)
					[
						SNew(STextBlock).Text(NSLOCTEXT("SLeagueFormInfo", "TableHeader.Name", "Player Name"))
					]
					+ SHeaderRow::Column(FLeaguePlayerRow::ColumnRole).FillWidth(1).OnSort(this, &STournamentTeam::OnPlayersListSort, true).SortMode(this, &STournamentTeam::GetColumnSortMode, FLeaguePlayerRow::ColumnRole, true)
					[
						SNew(STextBlock).Text(NSLOCTEXT("SLeagueFormInfo", "TableHeader.Role", "Player Role"))
					]
				)
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FReply STournamentTeam::OnPlayersMove(const bool IsTeam)
{
	if (IsTeam)
	{
		auto Selected = PlayersListWidget->GetSelectedItems();
		if (Selected.Num())
		{
			PlayersListTeam.Append(Selected);
			for (auto p : Selected) PlayersList.Remove(p);

			StartSort(false);
			StartSort(true);
		}
	}
	else
	{
		auto Selected = PlayersListWidgetTeam->GetSelectedItems();
		if (Selected.Num())
		{
			PlayersList.Append(Selected);
			for (auto p : Selected) PlayersListTeam.Remove(p);

			StartSort(false);
			StartSort(true);
		}
	}

	return FReply::Handled();
}

FReply STournamentTeam::OnPlayersConfirm()
{
	TArray<FString> SelectedPlayers;
	for (auto p : PlayersListTeam)
	{
		SelectedPlayers.Add(FString::FromInt(p->Index));
	}

	OnTournamentTeamConfirm.ExecuteIfBound(SelectedPlayers);
	return FReply::Handled();
}

TSharedRef<ITableRow> STournamentTeam::OnGeneratePlayerRow(TSharedPtr<FLeaguePlayerRow> InItem, const TSharedRef<STableViewBase>& OwnerTable, const bool IsTeam)
{
	return SNew(STournamentTeamRow, OwnerTable).Item(InItem);
}

void STournamentTeam::OnPlayersListSort(EColumnSortPriority::Type SortPriority, const FName& ColumnName, EColumnSortMode::Type InSortMode, const bool IsTeam)
{
	SortByColumn[IsTeam] = ColumnName;
	SortMode[IsTeam] = InSortMode;

	StartSort(IsTeam);
}

void STournamentTeam::StartSort(const bool IsTeam)
{
	if (SortByColumn[IsTeam] == FLeaguePlayerRow::ColumnNumber)
	{
		if (SortMode[IsTeam] == EColumnSortMode::Ascending)
			(IsTeam ? PlayersListTeam : PlayersList).Sort([](const TSharedPtr<FLeaguePlayerRow>& A, const TSharedPtr<FLeaguePlayerRow>& B) { return A->Number <= B->Number; });
		else
			(IsTeam ? PlayersListTeam : PlayersList).Sort([](const TSharedPtr<FLeaguePlayerRow>& A, const TSharedPtr<FLeaguePlayerRow>& B) { return A->Number >= B->Number; });
	}
	else if (SortByColumn[IsTeam] == FLeaguePlayerRow::ColumnName)
	{
		if (SortMode[IsTeam] == EColumnSortMode::Ascending)
			(IsTeam ? PlayersListTeam : PlayersList).Sort([](const TSharedPtr<FLeaguePlayerRow>& A, const TSharedPtr<FLeaguePlayerRow>& B) { return A->Name <= B->Name; });
		else
			(IsTeam ? PlayersListTeam : PlayersList).Sort([](const TSharedPtr<FLeaguePlayerRow>& A, const TSharedPtr<FLeaguePlayerRow>& B) { return A->Name >= B->Name; });
	}
	else if (SortByColumn[IsTeam] == FLeaguePlayerRow::ColumnRole)
	{
		if (SortMode[IsTeam] == EColumnSortMode::Ascending)
			(IsTeam ? PlayersListTeam : PlayersList).Sort([](const TSharedPtr<FLeaguePlayerRow>& A, const TSharedPtr<FLeaguePlayerRow>& B) { return A->Role >= B->Role; });
		else
			(IsTeam ? PlayersListTeam : PlayersList).Sort([](const TSharedPtr<FLeaguePlayerRow>& A, const TSharedPtr<FLeaguePlayerRow>& B) { return A->Role <= B->Role; });
	}
	else if (SortByColumn[IsTeam] == FLeaguePlayerRow::ColumnKillsAvg)
	{
		if (SortMode[IsTeam] == EColumnSortMode::Ascending)
			(IsTeam ? PlayersListTeam : PlayersList).Sort([](const TSharedPtr<FLeaguePlayerRow>& A, const TSharedPtr<FLeaguePlayerRow>& B) { return A->KillsAvg <= B->KillsAvg; });
		else
			(IsTeam ? PlayersListTeam : PlayersList).Sort([](const TSharedPtr<FLeaguePlayerRow>& A, const TSharedPtr<FLeaguePlayerRow>& B) { return A->KillsAvg >= B->KillsAvg; });
	}
	else if (SortByColumn[IsTeam] == FLeaguePlayerRow::ColumnActivity)
	{
		if (SortMode[IsTeam] == EColumnSortMode::Ascending)
			(IsTeam ? PlayersListTeam : PlayersList).Sort([](const TSharedPtr<FLeaguePlayerRow>& A, const TSharedPtr<FLeaguePlayerRow>& B) { return A->LastActivity <= B->LastActivity; });
		else
			(IsTeam ? PlayersListTeam : PlayersList).Sort([](const TSharedPtr<FLeaguePlayerRow>& A, const TSharedPtr<FLeaguePlayerRow>& B) { return A->LastActivity >= B->LastActivity; });
	}

	(IsTeam ? PlayersListWidgetTeam : PlayersListWidget)->RequestListRefresh();
}

EColumnSortMode::Type STournamentTeam::GetColumnSortMode(const FName ColumnName, const bool IsTeam) const
{
	if (SortByColumn[IsTeam] != ColumnName)
	{
		return EColumnSortMode::None;
	}

	return SortMode[IsTeam];
}

void STournamentTeam::AppendRow(const struct FLeaguePlayerRow &Row, const bool IsTeam)
{
	if (!IsTeam)
	{
		PlayersList.Add(MakeShareable(new FLeaguePlayerRow(Row)));
	}
	else
	{
		PlayersListTeam.Add(MakeShareable(new FLeaguePlayerRow(Row)));
	}

	StartSort(IsTeam);
}

void STournamentTeam::AppendRows(const TArray<struct FLeaguePlayerRow> &Rows, const bool IsTeam)
{
	for (auto Row : Rows)
	{
		if (!IsTeam)
		{
			PlayersList.Add(MakeShareable(new FLeaguePlayerRow(Row)));
		}
		else
		{
			PlayersListTeam.Add(MakeShareable(new FLeaguePlayerRow(Row)));
		}
	}

	StartSort(IsTeam);
}

void STournamentTeam::ClearRows(const bool IsTeam)
{
	if (!IsTeam)
	{
		PlayersList.Empty();
		PlayersListWidget->RequestListRefresh();
	}
	else
	{
		PlayersListTeam.Empty();
		PlayersListWidgetTeam->RequestListRefresh();
	}	
}

void STournamentTeam::SetOneMemberCost(const int32& Value)
{
	OneMemberCost = Value;
}

void STournamentTeam::SetBalanceValue(const TAttribute<int32>& Attr)
{
	LeagueBalanceValue = Attr;
}

void STournamentTeam::SetBalanceType(const TAttribute<EResourceTextBoxType::Type>& Attr)
{
	LeagueBalanceType = Attr;
}