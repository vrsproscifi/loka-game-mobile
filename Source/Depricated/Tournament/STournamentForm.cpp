// VRSPRO

#include "LokaGame.h"
#include "STournamentForm.h"
#include "SlateOptMacros.h"

#include "SItemListRow.h"
#include "SSliderSpin.h"
#include "STextBlockButton.h"
#include "SResourceTextBox.h"
#include "SWindowBackground.h"
#include "Layout/SScaleBox.h"

#include "Slate/Styles/TournamentFormItemWidgetStyle.h"

#include "Tournament/TournamentDetails.h"
#include "IdentityController/Models/PlayerEntityInfo.h"

class LOKAGAME_API STournamentFormItem : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(STournamentFormItem)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FTournamentFormItemStyle>("STournamentFormItemStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FTournamentFormItemStyle, Style)
	SLATE_ARGUMENT(FTournamentPreview, Info)
	SLATE_EVENT(FOnClicked, OnClicked)
	SLATE_END_ARGS()

	BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
	void Construct(const FArguments& InArgs)
	{
		Style = InArgs._Style;
		FTournamentPreview Info = InArgs._Info; //AwardType //JoinPriceType //Deadline

		ChildSlot
		[
			SNew(SBox)
			.WidthOverride(Style->Size.X)
			//.HeightOverride(Style->Size.Y)
			[
				SNew(SBorder)
				.BorderImage(&Style->Background)
				.Padding(0)
				[
					SNew(SBorder)
					.BorderImage(&Style->Border)
					.Padding(0)
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot() // Name
						.HAlign(EHorizontalAlignment::HAlign_Fill)
						.VAlign(EVerticalAlignment::VAlign_Center)
						.AutoHeight()
						.Padding(FMargin(4, 2))
						[
							SNew(STextBlock).Text(FText::FromString(Info.name)).TextStyle(&Style->TextName).Justification(ETextJustify::Center)
						]
						+ SVerticalBox::Slot() // Image
						.HAlign(EHorizontalAlignment::HAlign_Center)
						.VAlign(EVerticalAlignment::VAlign_Center)
						.AutoHeight()
						.Padding(FMargin(4, 2))
						[
							SNew(SImage)
							.Image(&Style->DefaultImage)
						]
						+ SVerticalBox::Slot() // Info & Button
						.FillHeight(1)
						.Padding(Style->InfoBoxPadding)
						[
							SNew(SVerticalBox)
							+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(0, 4))
							[
								SNew(SHorizontalBox)
								+ SHorizontalBox::Slot()
								.HAlign(EHorizontalAlignment::HAlign_Left)
								.VAlign(VAlign_Center)
								[
									SNew(STextBlock).Text(NSLOCTEXT("STournamentFormItem", "STournamentFormItem.JoinCost", "Join cost:")).TextStyle(&Style->TextInfo)
								]
								+ SHorizontalBox::Slot()
								.HAlign(EHorizontalAlignment::HAlign_Left)
								.VAlign(VAlign_Center)
								[
									SNew(SResourceTextBox)
									.Type(Info.JoinPriceType ? EResourceTextBoxType::Donate : EResourceTextBoxType::Money)
									.Value(Info.JoinPrice)
								]
							]
							+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(0, 4))
							[
								SNew(SHorizontalBox)
								+ SHorizontalBox::Slot()
								.HAlign(EHorizontalAlignment::HAlign_Left)
								.VAlign(VAlign_Center)
								[
									SNew(STextBlock).Text(NSLOCTEXT("STournamentFormItem", "STournamentFormItem.Reward", "Reward:")).TextStyle(&Style->TextInfo)
								]
								+ SHorizontalBox::Slot()
								.HAlign(EHorizontalAlignment::HAlign_Left)
								.VAlign(VAlign_Center)
								[
									SNew(SResourceTextBox)
									.Type((Info.AwardType == 2) ? EResourceTextBoxType::RealMoney : (Info.AwardType ? EResourceTextBoxType::Donate : EResourceTextBoxType::Money))
									.Value(Info.Award)
								]
							]
							+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(0, 4))
							[
								SNew(SHorizontalBox)
								+ SHorizontalBox::Slot()
								.HAlign(EHorizontalAlignment::HAlign_Left)
								.VAlign(VAlign_Center)
								[
									SNew(STextBlock).Text(NSLOCTEXT("STournamentFormItem", "STournamentFormItem.Startin", "Start in:")).TextStyle(&Style->TextInfo)
								]
								+ SHorizontalBox::Slot()
								.HAlign(EHorizontalAlignment::HAlign_Left)
								.VAlign(VAlign_Center)
								[
									SNew(SResourceTextBox)
									.Type(EResourceTextBoxType::Time)
									.Value(Info.Deadline)
								]
							]
							+ SVerticalBox::Slot().FillHeight(1).Padding(FMargin(6, 10))
							[
								SNew(STextBlock)
								.AutoWrapText(true)
								.TextStyle(&Style->TextInfo)
								.Text(FText::FromString(Info.desc))
							]
							+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(0, 4))
							.HAlign(EHorizontalAlignment::HAlign_Fill)
							.VAlign(EVerticalAlignment::VAlign_Center)
							[
								SNew(SButton)
								.ButtonStyle(&Style->Button)
								.OnClicked(InArgs._OnClicked)
								.HAlign(HAlign_Center)
								.VAlign(VAlign_Center)
								.TextStyle(&Style->TextButton)
								.Text(NSLOCTEXT("STournamentFormItem", "STournamentFormItem.Join", "Join"))
							]
						]
					]
				]
			]
		];
	}
	END_SLATE_FUNCTION_BUILD_OPTIMIZATION

protected:

	const FTournamentFormItemStyle *Style;
};


class LOKAGAME_API STournamentForm_Item : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(STournamentForm_Item)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FTournamentFormStyle>("STournamentFormStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FTournamentFormStyle, Style)
	SLATE_ARGUMENT(FTournamentPreview, Info)
	SLATE_EVENT(FOnClicked, OnClicked)
	SLATE_END_ARGS()

	BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
	void Construct(const FArguments& InArgs)
	{
		Style = InArgs._Style;

		ChildSlot
		[
			SNew(SBox).WidthOverride(320)
			[
				SNew(SBorder)
				.BorderImage(&Style->Item.Background)
				.Padding(4)
				[
					SNew(SVerticalBox)
					+ SVerticalBox::Slot().Padding(FMargin(0, 4)).AutoHeight() // Header
					[
						SNew(STextBlock)
						.Justification(ETextJustify::Center)
						.TextStyle(&Style->Item.Header)
						.Text(NSLOCTEXT("Tournament", "Tournament.Item.Header", "Custom Tournament"))
					]
					+ SVerticalBox::Slot().AutoHeight() // Image
					[
						SNew(SScaleBox)
						.Stretch(EStretch::ScaleToFit)
						[
							SNew(SImage)
							.Image(&Style->Item.Image)
						]
					]
					+ SVerticalBox::Slot().Padding(FMargin(4, 10)) // Desc
					[
						SNew(STextBlock)
						.AutoWrapText(true)
						.Justification(ETextJustify::Left)
						.TextStyle(&Style->Item.Desc)
						//.Text(NSLOCTEXT("Tournament", "Tournament.Item.Desc", "Desc"))
					]
					+ SVerticalBox::Slot().AutoHeight() // Button
					[
						SNew(SButton)
						.Text(NSLOCTEXT("Tournament", "Tournament.Item.Button", "Create Tournament"))
						.HAlign(HAlign_Center)
						.VAlign(VAlign_Center)
						.ContentPadding(FMargin(10, 4))
						.ButtonStyle(&Style->Item.Button)
						.TextStyle(&Style->Item.ButtonText)
					]
				]
			]
		];
	}
	END_SLATE_FUNCTION_BUILD_OPTIMIZATION

protected:

	const FTournamentFormStyle *Style;
};


BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void STournamentForm::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	OnClickedTournament = InArgs._OnClickedTournament;
	

	TSharedPtr<SScrollBox> ScrollBox;
	TSharedPtr<SScrollBar> ScrollBar;
	
	ChildSlot
	[
		SNew(SWindowBackground)
		.IsEnabledBlur(true)
		[
			SNew(SOverlay)
			+ SOverlay::Slot()
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot().HAlign(HAlign_Left)
				[
					SNew(STournamentForm_Item)
				]
				+ SVerticalBox::Slot()
				//SAssignNew(ListCintainer, SScrollBox)
				//.ExternalScrollbar(ScrollBar)
				//.Orientation(EOrientation::Orient_Horizontal)
			]
			+ SOverlay::Slot()
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Bottom)
			[
				SAssignNew(ScrollBar, SScrollBar)
				.Style(&FLokaSlateStyle::Get().GetWidgetStyle<FScrollBarStyle>("Default_ScrollBar"))
				.Orientation(EOrientation::Orient_Horizontal)
			]
			+ SOverlay::Slot()
			[
				SNew(SWidgetSwitcher)
				.WidgetIndex_Lambda([&]() {
				return FPlayerEntityInfo::Instance.Experience.Level >= 20;
				})
				+ SWidgetSwitcher::Slot()
				[
					SNew(SBorder)
					.BorderImage(new FSlateColorBrush(FColor::Black.WithAlpha(128)))
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					[
						SNew(SHorizontalBox)
						+ SHorizontalBox::Slot().AutoWidth().VAlign(VAlign_Center)
						[
							SNew(STextBlock)
							.TextStyle(&FLokaSlateStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_TextBlock_InDev"))
							.Text(NSLOCTEXT("All", "All.AllowFrom", "Available at ")) //
						]
						+ SHorizontalBox::Slot().AutoWidth().VAlign(VAlign_Center)
						[
							SNew(SResourceTextBox)
							.Type(EResourceTextBoxType::Level)
							.Value(20)
						]
					]
				]
				+ SWidgetSwitcher::Slot()
				[
					SNew(SBorder)
					.BorderImage(new FSlateColorBrush(FColor::Black.WithAlpha(128)))
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					[
						SNew(STextBlock)
						.TextStyle(&FLokaSlateStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_TextBlock_InDev"))
						.Text(NSLOCTEXT("All", "All.InDev", "In Development, coming soon.")) //
					]
				]
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void STournamentForm::Append(const FTournamentPreview &Info)
{
	//ListCintainer->AddSlot()
	//[
	//	SNew(STournamentFormItem)
	//	.Info(Info)
	//	.OnClicked(this, &STournamentForm::onClicked, Info.Uid)
	//];
}

void STournamentForm::Clear()
{
	//ListCintainer->ClearChildren();
}

FReply STournamentForm::onClicked(const FString Uid)
{
	OnClickedTournament.ExecuteIfBound(Uid);
	return FReply::Handled();
}