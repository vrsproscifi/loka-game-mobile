#pragma once
#include "ServiceOperation.h"
#include "RequestOneParam.h"

namespace Operation
{
	namespace OnSelectFraction
	{
		class Operation final :// public COperation<FRequestOneParam<int32>, ObjectSerializer::NativeObject, FFractionReputation, ObjectSerializer::JsonObject>
		{
		public:
			Operation() : Super(FServerHost::MasterClient, "Identity/OnSelectFraction") { }

		};

		DECLARE_DELEGATE_OneParam(FOnSwitchFraction, const FRequestOneParam<int32>&);
	}
}