#pragma once
#include "ServiceOperation.h"
#include "RequestOneParam.h"
#include "../Models/ConfirmTutorialStepResponse.h"



namespace Operation
{
	namespace OnStartTutorial
	{	
		class Operation final :// public COperation<FRequestOneParam<int32>, ObjectSerializer::NativeObject, FStartTutorialView, ObjectSerializer::JsonObject>
		{
		public:
			//	Если не удалось найти Instance урока
			IResponseHandler<void, OperationResponseType::None> OnNotFoundInstance;
			
			//	Если не удалось найти Instance этапа или этапов нету
			IResponseHandler<void, OperationResponseType::None> OnNotFoundSteps;
			
			//	Если игрок пытается еще раз начать этот этап обучения
			IResponseHandler<FStartTutorialView, OperationResponseType::JsonObject> OnStepInProgress;

			Operation() 
				: Super(FServerHost::MasterClient, "Identity/OnStartTutorial")
				, OnNotFoundInstance(400, this)
				, OnNotFoundSteps(403, this)
				, OnStepInProgress(502, this)
			{

			}

		};

	}
}