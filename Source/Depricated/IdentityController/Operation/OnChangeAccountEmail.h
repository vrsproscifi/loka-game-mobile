#pragma once
#include "ServiceHandler.h"
#include "ServiceOperation.h"
#include "RequestOneParam.h"

namespace Operation
{
	namespace OnChangeAccountEmail
	{
		class Operation final :// public COperation<FRequestOneParam<FString>, ObjectSerializer::NativeObject, FRequestOneParam<FString>, ObjectSerializer::NativeObject>
		{
		public:
			IResponseHandler<FRequestOneParam<FString>, OperationResponseType::NativeObject> OnPlayerEmailWasNotConfirmed;
			IResponseHandler<FRequestOneParam<FString>, OperationResponseType::NativeObject> OnPlayerEmailWasAlreadyConfirmed;

			IResponseHandler<FRequestOneParam<FString>, OperationResponseType::NativeObject> OnExistEmailAndAlreadyConfirmed;
			IResponseHandler<FRequestOneParam<FString>, OperationResponseType::NativeObject> OnExistEmailAndNotConfirmed;
			IResponseHandler<void, OperationResponseType::None> OnBadEmailAddresFormat;


			Operation() 
				: Super(FServerHost::MasterClient, "Identity/OnChangeAccountEmail")
				, OnPlayerEmailWasNotConfirmed(700, this)
				, OnPlayerEmailWasAlreadyConfirmed(701, this)
				, OnExistEmailAndAlreadyConfirmed(702, this)
				, OnExistEmailAndNotConfirmed(703, this)
				, OnBadEmailAddresFormat(400, this)

			{
				
			}

		};
	}
}