#pragma once
#include "ServiceOperation.h"
#include "RequestOneParam.h"

namespace Operation
{
	namespace CheckExistAccount
	{
		class Operation final :// public COperation<void, ObjectSerializer::NativeObject, FRequestOneParam<bool>, ObjectSerializer::NativeObject>
		{
		public:
			int32 AttempRequestNumber;
			Operation()
				: Super(FServerHost::MasterClient, "Identity/IsExistGameAccount", FVerb::Get)
				, AttempRequestNumber(0)
			{
			}
		};
	}
}