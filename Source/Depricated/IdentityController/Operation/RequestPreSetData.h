#pragma once
#include "ServiceOperation.h"
#include "../Models/LobbyClientPreSetData.h"



namespace Operation
{
	namespace RequestPreSetData
	{
		class Operation final :// public COperation<void, ObjectSerializer::NativeObject, TArray<FClientPreSetData>, ObjectSerializer::JsonArrayObject>
		{
		public:
			int32 AttempRequestNumber;
			Operation() : Super(FServerHost::MasterClient, "Identity/RequestPreSetData", FVerb::Get), AttempRequestNumber(0)
			{
			}
		};
	}
}