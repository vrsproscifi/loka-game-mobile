#pragma once
#include "ServiceOperation.h"
#include "../Models/PlayerEntityInfo.h"


namespace Operation
{
	namespace RequestClientData
	{
		class Operation final :// public COperation<void, ObjectSerializer::NativeObject, FPlayerEntityInfo, ObjectSerializer::JsonObject>
		{
		public:
			int32 AttempRequestNumber;
			Operation() : Super(FServerHost::MasterClient, "Identity/OnRequestClientData", FVerb::Get), AttempRequestNumber(0)
			{
			}
		};
	}
}