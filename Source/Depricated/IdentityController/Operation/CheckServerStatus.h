#pragma once
#include "ServiceOperation.h"
#include "RequestOneParam.h"
#include "ServiceHandler.h"

namespace Operation
{
	namespace CheckServerStatus
	{
		class Operation final :// public COperation<void, ObjectSerializer::NativeObject, FRequestOneParam<int32>, ObjectSerializer::NativeObject>
		{
		public:
			IResponseHandler<void, OperationResponseType::None> OnServerNotWorking;

			Operation() 
				: Super(FServerHost::MasterClient, "ServerStatus.json", FVerb::Get) 
				, OnServerNotWorking(503, this)
			{

			}

		};
	}
}
