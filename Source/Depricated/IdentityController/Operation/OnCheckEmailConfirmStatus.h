#pragma once
#include "ServiceHandler.h"
#include "ServiceOperation.h"
#include "RequestOneParam.h"
#include "PoolingOperation.h"

namespace Operation
{
	namespace OnCheckEmailConfirmStatus
	{
		class Operation final 
			:// public COperation<void, ObjectSerializer::NativeObject, FRequestOneParam<bool>, ObjectSerializer::NativeObject>
			// , public FPoolingOperation
		{
		public:
			Operation()
				: Super(FServerHost::MasterClient, "Identity/OnCheckEmailConfirmStatus", FVerb::Get)
				, FPoolingOperation(7.0f, 5.0f)
			{
				this->TimerDelegate.BindRaw(this, &Operation::Request);
			}
		};
	}
}
