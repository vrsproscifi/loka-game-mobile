#pragma once
#include "ServiceOperation.h"
#include "../Models/ChangeRegionAccountView.h"



namespace Operation
{
	namespace ChangeRegionAccount
	{
		class Operation final :// public COperation<FChangeRegionAccountView, ObjectSerializer::JsonObject, void, ObjectSerializer::JsonArrayObject>
		{
		public:
			Operation() : Super(FServerHost::MasterClient, "Identity/ChangeRegionAccount") { }

		};
	}
}