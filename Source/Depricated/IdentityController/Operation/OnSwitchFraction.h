#pragma once
#include "ServiceOperation.h"
#include "RequestOneParam.h"

namespace Operation
{
	namespace OnSwitchFraction
	{
		class Operation final :// public COperation<FRequestOneParam<int32>, ObjectSerializer::NativeObject, FRequestOneParam<int32>, ObjectSerializer::NativeObject>
		{
		public:
			Operation() : Super(FServerHost::MasterClient, "Identity/OnSwitchFraction") { }

		};

		DECLARE_DELEGATE_OneParam(FOnSwitchFraction, const FRequestOneParam<int32>&);
	}
}