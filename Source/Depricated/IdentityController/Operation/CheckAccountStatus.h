#pragma once
#include "ServiceOperation.h"
#include "../Models/AccountStatusContainer.h"
#include "PoolingOperation.h"


namespace Operation
{
	namespace CheckAccountStatus
	{
		class Operation final 
			:// public COperation<void, ObjectSerializer::NativeObject, FAccountStatusContainer, ObjectSerializer::JsonObject>
			// , public FPoolingOperation
		{
		public:
			Operation() : Super(FServerHost::MasterClient, "Identity/CheckAccountStatus", FVerb::Get) 
			{
				this->TimerDelegate.BindRaw(this, &Operation::Request);
			}

		};
	}
}