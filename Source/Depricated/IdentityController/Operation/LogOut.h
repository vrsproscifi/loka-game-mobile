#pragma once
#include "ServiceOperation.h"


namespace Operation
{
	namespace LogOut
	{
		struct Response final : FOnlineJsonSerializable
		{
			virtual ~Response() { }

			TArray<FString> Error;

			FString	Token;

			BEGIN_ONLINE_JSON_SERIALIZER
				ONLINE_JSON_SERIALIZE_ARRAY("Error", Error);
			ONLINE_JSON_SERIALIZE("Token", Token);
			END_ONLINE_JSON_SERIALIZER
		};

		class Operation final :// public COperation<void, ObjectSerializer::NativeObject, Response, ObjectSerializer::NativeObject>
		{
		public:
			Operation() : Super(FServerHost::MasterClient, "Identity/OnLogOut") { }

		};
	}
}