#pragma once
#include "ServiceOperation.h"
#include "ServiceHandler.h"
#include "RequestOneParam.h"
#include "PoolingOperation.h"


namespace Operation
{
	namespace Authentication
	{
		struct Request final : FOnlineJsonSerializable
		{
			Request(const FString& login, const FString& password, bool isAuthorization)
				: Login(login)
				, Password(password)
				, IsAuthorization(isAuthorization)
			{
				
			}

			virtual ~Request() { }

			FString Login;

			FString Password;

			bool IsAuthorization;

			BEGIN_ONLINE_JSON_SERIALIZER
				ONLINE_JSON_SERIALIZE("Login", Login);
				ONLINE_JSON_SERIALIZE("Password", Password);
				ONLINE_JSON_SERIALIZE("IsAuthorization", IsAuthorization);
			END_ONLINE_JSON_SERIALIZER
		};

		struct Response final : FOnlineJsonSerializable
		{
			virtual ~Response() { }

			TArray<FString> Error;

			FString	Token;

			BEGIN_ONLINE_JSON_SERIALIZER
				ONLINE_JSON_SERIALIZE_ARRAY("Error", Error);
				ONLINE_JSON_SERIALIZE("Token", Token);
			END_ONLINE_JSON_SERIALIZER
		};

		class Operation final :// public COperation<Request, ObjectSerializer::NativeObject, Response, ObjectSerializer::NativeObject, EOperationLogVerbosity::Error_Warning, false>
		{
		public:
			Operation() : Super(FServerHost::MasterClient, "Identity/OnAuthentication") { }

		};
	}

	namespace SteamAuthentication
	{
		class Operation final 
			:// public COperation<FRequestOneParam<FString>, ObjectSerializer::NativeObject, Authentication::Response, ObjectSerializer::NativeObject, EOperationLogVerbosity::Error_Warning, false>
			// , public FPoolingOperation
		{
			FString SteamId;
		public:
			IResponseHandler<void, OperationResponseType::None> OnPaymentRequired;

			Operation() 
				: Super(FServerHost::MasterClient, "Identity/OnSteamAuthentication") 
				, OnPaymentRequired(402, this)
				, FPoolingOperation(5.0f, 1.5f)
			{ 
				TimerDelegate.BindRaw(this, &Operation::OnRequestAttemp);
			}

			void PostRequest(const FString& steamId, FTimerManager& tm)
			{
				SteamId = steamId;
				TimerStart(tm);
			}
			
			void OnRequestAttemp()
			{
				Request(SteamId);
			}

		};
	}

	DECLARE_DELEGATE_OneParam(FOnAuthentication, const Operation::Authentication::Request &);
}