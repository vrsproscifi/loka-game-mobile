#pragma once
#include "ServiceOperation.h"
#include "../Models/LobbyClientLootData.h"



namespace Operation
{
	namespace RequestLootData
	{
		class Operation final :// public COperation<void, ObjectSerializer::NativeObject, FLobbyClientLootData, ObjectSerializer::JsonObject>
		{
		public:
			int32 AttempRequestNumber;
			Operation() : Super(FServerHost::MasterClient, "Identity/RequestLootData", FVerb::Get), AttempRequestNumber(0)
			{
			}
		};
	}
}