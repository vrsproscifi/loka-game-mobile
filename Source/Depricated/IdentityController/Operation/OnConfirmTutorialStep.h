#pragma once
#include "ServiceOperation.h"
#include "RequestOneParam.h"
#include "../Models/ConfirmTutorialStepResponse.h"



namespace Operation
{
	namespace OnConfirmTutorialStep
	{	
		class Operation final :// public COperation<FStartTutorialView, ObjectSerializer::JsonObject, FConfirmTutorialStepResponse, ObjectSerializer::JsonObject>
		{
		public:
			//	Если не удалось найти Instance этапа
			IResponseHandler<void, OperationResponseType::None> OnNotFoundStep;
			
			//	Если не удалось найти Instance вознаграждения
			IResponseHandler<void, OperationResponseType::None> OnNotFoundReward;
			
			//	Если эта обучения у игрока не равен отправляемому этапу (попытка подмены)
			IResponseHandler<void, OperationResponseType::None> OnResyncSteps;

			Operation() 
				: Super(FServerHost::MasterClient, "Identity/OnConfirmTutorialStep")
				, OnNotFoundStep(400, this)
				, OnNotFoundReward(409, this)
				, OnResyncSteps(403, this)
			{

			}

		};

	}
}