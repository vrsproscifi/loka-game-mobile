// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/VotingWidgetStyle.h"


struct FPlayerEntityInfo;
typedef TSharedPtr<FImprovementView> FImprovementViewPtr;

class LOKAGAME_API SWindowVotingForChange : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SWindowVotingForChange)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FVotingStyle>("SVotingStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FVotingStyle, Style)
	SLATE_EVENT(Operation::VoteImprovement::Operation::FOnRequest, OnVoteCreate)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);
	void OnFillChangeIssues(const TArray<FImprovementView>&, const int64&);	

	static int32 CostOfChangeVote;

protected:

	const FVotingStyle* Style;	
	TArray<FImprovementViewPtr> Items;
	TSharedPtr<SListView<FImprovementViewPtr>> Widget_List;

	Operation::VoteImprovement::Operation::FOnRequest OnVoteCreate;

	FReply OnClickedVoteCreate();

	int64 TimeEndOfVotes;
};
