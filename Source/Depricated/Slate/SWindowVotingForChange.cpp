// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SWindowVotingForChange.h"
#include "SlateOptMacros.h"

#include "SLokaToolTip.h"
#include "SResourceTextBox.h"
#include "SMessageBox.h"


#include "IdentityController/Models/PlayerEntityInfo.h"
#include "Marketing/Improvements/ImprovementProgressState.h"

class LOKAGAME_API SVotingChangeRow : public STableRow<FImprovementViewPtr>
{
public:
	SLATE_BEGIN_ARGS(SVotingChangeRow)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FVotingStyle>("SVotingStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FVotingStyle, Style)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, const TSharedRef<STableViewBase>& OwnerTableView, FImprovementViewPtr& InItem)
	{
		Style = InArgs._Style;

		STableRow<FImprovementViewPtr>::Construct(
			STableRow<FImprovementViewPtr>::FArguments()
			.Style(&Style->Changes.Row)
			.Padding(FMargin(4, Style->Changes.RowHeightPadding))
			.Content()
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().AutoWidth()
				[
					SNew(SCheckBox)
					.Style(&FLokaSlateStyle::Get().GetWidgetStyle<FCheckBoxStyle>("Default_CheckBox"))
					.IsChecked_Lambda([&]() { return IsSelected() ? ECheckBoxState::Checked : ECheckBoxState::Unchecked; })
					.Visibility(EVisibility::HitTestInvisible)
				]
				+ SHorizontalBox::Slot()
				[
					SNew(STextBlock)
					.Text(FText::FromString(InItem->Title))
					.TextStyle(&Style->Changes.VoteTitle)
				]
				+ SHorizontalBox::Slot().AutoWidth().Padding(FMargin(6, 0))
				[
					SNew(STextBlock)
					.Text(FText::AsNumber(InItem->Count))
					.TextStyle(&Style->Proposals.SpinStyle.TextCenter)
					// TODO
					//.Visibility_Lambda([](){ return (FPlayerEntityInfo::Instance.IsAllowVoteImprovement == false) ? EVisibility::Visible : EVisibility::Collapsed; })
				]
			]

		, OwnerTableView);

		SetToolTip(SNew(SLokaToolTip).Text(FText::FromString(InItem->Message)));
	}

protected:

	const FVotingStyle* Style;
};

int32 SWindowVotingForChange::CostOfChangeVote = 0;

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SWindowVotingForChange::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	OnVoteCreate = InArgs._OnVoteCreate;
	
	
	TSharedPtr<SScrollBar> ScrollBar = SNew(SScrollBar).Style(&FLokaSlateStyle::Get().GetWidgetStyle<FScrollBarStyle>("Default_ScrollBar")).Thickness(FVector2D(4, 4));

	ChildSlot
	[
		SNew(SVerticalBox)
		+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(6, 4))
		[
			SNew(STextBlock) // Rules
			.AutoWrapText(true)
			.Text(NSLOCTEXT("SWindowVotingForChange", "SWindowVotingForChange.Rules", "Select one of the options which you'd like to see in the next update of the game, for one period of voting you can only vote once, without the right to change you vote."))
			.TextStyle(&Style->Changes.VoteTimer)
		]
		+ SVerticalBox::Slot()
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
			[
				SAssignNew(Widget_List, SListView<FImprovementViewPtr>)
				// TODO
				//.SelectionMode_Lambda([&]() { return (FPlayerEntityInfo::Instance.IsAllowVoteImprovement) ? ESelectionMode::Single : ESelectionMode::None; })
				.ListItemsSource(&Items)
				.ExternalScrollbar(ScrollBar)
				.OnGenerateRow_Lambda([&](FImprovementViewPtr Item, const TSharedRef<STableViewBase>& OwnerTable) {
					return SNew(SVotingChangeRow, OwnerTable, Item);
				})
			]
			+ SHorizontalBox::Slot().AutoWidth()
			[
				ScrollBar.ToSharedRef()
			]			
		]
		+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(0, 4))
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot().Padding(FMargin(6, 0)).VAlign(VAlign_Center) // Description timer
			[
				SNew(STextBlock)
				.AutoWrapText(true)
				.TextStyle(&Style->Changes.VoteTimer)
				.Text_Lambda([&]() {
					auto Timespan = FTimespan::FromSeconds(TimeEndOfVotes - FDateTime::UtcNow().ToUnixTimestamp());
					FText TimeText;
					if (Timespan.GetTotalDays())
					{
						TimeText = FText::Format(NSLOCTEXT("All", "All.Time.WithDays", "{0} {0}|ordinal(one=Day,two=Days,few=Days,other=Days) {1} {1}|ordinal(one=Hour,two=Hours,few=Hours,other=Hours) {2} {2}|ordinal(one=Minute,two=Minuts,few=Minuts,other=Minuts)")
												 , FText::AsNumber(Timespan.GetDays(), &FNumberFormattingOptions::DefaultNoGrouping())
												 , FText::AsNumber(Timespan.GetHours(), &FNumberFormattingOptions::DefaultNoGrouping())
												 , FText::AsNumber(Timespan.GetMinutes(), &FNumberFormattingOptions::DefaultNoGrouping()));
					}
					else if (Timespan.GetHours())
					{
						TimeText = FText::Format(NSLOCTEXT("All", "All.Time.WithHours", "{0} {0}|ordinal(one=Hour,two=Hours,few=Hours,other=Hours) {1} {1}|ordinal(one=Minute,two=Minuts,few=Minuts,other=Minuts)")
												 , FText::AsNumber(Timespan.GetHours(), &FNumberFormattingOptions::DefaultNoGrouping())
												 , FText::AsNumber(Timespan.GetMinutes(), &FNumberFormattingOptions::DefaultNoGrouping()));
					}
					else if (Timespan.GetMinutes())
					{
						TimeText = FText::Format(NSLOCTEXT("All", "All.Time.WithMinutes", "{0} {0}|ordinal(one=Minute,two=Minuts,few=Minuts,other=Minuts)")
												 , FText::AsNumber(Timespan.GetMinutes(), &FNumberFormattingOptions::DefaultNoGrouping()));
					}

					return FText::Format(NSLOCTEXT("SWindowVotingForChange", "SWindowVotingForChange.VoteDoneTime", "Voting done in {0}"), TimeText);
				})
			]
			+ SHorizontalBox::Slot().AutoWidth().HAlign(HAlign_Right) // Button
			[
				SNew(SButton)
				.ButtonStyle(&Style->Changes.VoteButton)
				.ContentPadding(FMargin(6, 2))
				// TODO
				//.IsEnabled_Lambda([&]() { return (FPlayerEntityInfo::Instance.IsAllowVoteImprovement); })
				.OnClicked(this, &SWindowVotingForChange::OnClickedVoteCreate)
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot().AutoWidth().Padding(FMargin(10, 4, 4, 4)).VAlign(VAlign_Center)
					[
						SNew(STextBlock)
						.Text(NSLOCTEXT("SWindowVotingForChange", "SWindowVotingForChange.Vote", "Vote"))
						.TextStyle(&Style->Changes.VoteTimer)
					]
					+ SHorizontalBox::Slot().AutoWidth().Padding(FMargin(4, 4, 10, 4)).VAlign(VAlign_Center)
					[
						SNew(SResourceTextBox)
						.CustomSize(FVector(16, 16, 10))
						.TypeAndValue(FResourceTextBoxValue::Donate(SWindowVotingForChange::CostOfChangeVote))
					]
				]
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FReply SWindowVotingForChange::OnClickedVoteCreate()
{
	const auto SlectedItems = Widget_List->GetSelectedItems();
	if (SlectedItems.Num() && SlectedItems[0].IsValid())
	{
		OnVoteCreate.ExecuteIfBound(FImprovementVoteCreate(SlectedItems[0]->ImprovementId));
		SMessageBox::Get()->ToggleWidget(false);
	}
	return FReply::Handled();
}

void SWindowVotingForChange::OnFillChangeIssues(const TArray<FImprovementView>& InData, const int64& InTime)
{
	TimeEndOfVotes = InTime;

	Items.Empty();

	for (auto item : InData)
	{
		if (item.ProgressState == EImprovementProgressState::Voting)
		{
			Items.Add(MakeShareable(new FImprovementView(item)));
		}
	}

	Widget_List->RequestListRefresh();
}