// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/VotingWidgetStyle.h"


struct FPlayerEntityInfo;
typedef TSharedPtr<FImprovementView> FImprovementViewPtr;

class LOKAGAME_API SWindowVotingForProposals : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SWindowVotingForProposals)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FVotingStyle>("SVotingStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FVotingStyle, Style)
	SLATE_EVENT(FOnVoteAction, OnDetails)
	SLATE_EVENT(FOnClickedOutside, OnCreate)
	SLATE_EVENT(Operation::VoteSentence::Operation::FOnRequest, OnVoted)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);
	void OnFillProposalsIssues(const TArray<FImprovementView>&);

protected:

	const FVotingStyle* Style;
	TArray<FImprovementViewPtr> Items;
	TSharedPtr<SListView<FImprovementViewPtr>> Widget_List;

	FOnVoteAction OnDetails;
	FOnClickedOutside OnCreate;
	Operation::VoteSentence::Operation::FOnRequest OnVoted;
};
