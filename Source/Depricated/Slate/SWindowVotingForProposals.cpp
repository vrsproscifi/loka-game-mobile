// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SWindowVotingForProposals.h"
#include "SlateOptMacros.h"

#include "SLokaToolTip.h"
#include "SResourceTextBox.h"
#include "SSliderSpin.h"
#include "SMessageBox.h"

#include "IdentityController/Models/PlayerEntityInfo.h"

class LOKAGAME_API SVotingProposalRow : public STableRow<FImprovementViewPtr>
{
public:
	SLATE_BEGIN_ARGS(SVotingProposalRow)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FVotingStyle>("SVotingStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FVotingStyle, Style)
	SLATE_EVENT(FOnBooleanValueChanged, OnVoted)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, const TSharedRef<STableViewBase>& OwnerTableView, FImprovementViewPtr& InItem)
	{
		Style = InArgs._Style;
		OnVoted = InArgs._OnVoted;
		IsEnableVote = true;

		TArray<FText> SpinValues;
		SpinValues.Add(FText::AsNumber(InItem->Count - 1));
		SpinValues.Add(FText::AsNumber(InItem->Count));
		SpinValues.Add(FText::AsNumber(InItem->Count + 1));

		MAKE_UTF8_SYMBOL(sPlus, 0xf087);
		MAKE_UTF8_SYMBOL(sMinus, 0xf088);

		STableRow<FImprovementViewPtr>::Construct(
			STableRow<FImprovementViewPtr>::FArguments()
			.Style(&Style->Changes.Row)
			.Padding(FMargin(4, Style->Changes.RowHeightPadding))
			.Content()
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot()
				[
					SNew(STextBlock)
					.Text(FText::FromString(InItem->Message.Left(30) + "..."))
					.TextStyle(&Style->Changes.VoteTitle)
				]
				+ SHorizontalBox::Slot().AutoWidth()
				[
					SNew(SBox).WidthOverride(120)
					[
						SNew(SSliderSpin)
						.TextLeft(FText::FromString(sMinus))
						.TextRight(FText::FromString(sPlus))
						.Style(&Style->Proposals.SpinStyle)
						.Values(SpinValues)
						.ActiveValue(1)
						.IsEnabled_Lambda([&]() { return IsEnableVote; })
						.OnListValue_Lambda([&](const int32& v) {
							IsEnableVote = false; 
							OnVoted.ExecuteIfBound((v > 1));
						})
					]	 
				]
			]
		, OwnerTableView);
		
		SetToolTip(SNew(SLokaToolTip).Text(FText::FromString(InItem->Message)));
	}

protected:

	const FVotingStyle* Style;
	bool IsEnableVote;
	FOnBooleanValueChanged OnVoted;
};

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SWindowVotingForProposals::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	OnDetails = InArgs._OnDetails;
	OnCreate = InArgs._OnCreate;
	OnVoted = InArgs._OnVoted;
	

	TSharedPtr<SScrollBar> ScrollBar = SNew(SScrollBar).Style(&FLokaSlateStyle::Get().GetWidgetStyle<FScrollBarStyle>("Default_ScrollBar")).Thickness(FVector2D(4, 4));

	ChildSlot
	[
		SNew(SVerticalBox)
		+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(6, 4))
		[
			SNew(STextBlock) // Rules
			.AutoWrapText(true)
			.Text(NSLOCTEXT("SWindowVotingForProposals", "SWindowVotingForProposals.Rules", "Help us to choose the best idea for next updates, or leave your own idea."))
			.TextStyle(&Style->Changes.VoteTimer)
		]
	//	TODO
		//+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(6, 4))
		//[
		//	SNew(SButton)
		//	.ButtonStyle(&Style->Changes.VoteButton)
		//	.ContentPadding(FMargin(6, 2))
		//	.HAlign(HAlign_Center)
		//	.VAlign(VAlign_Center)
		//	.Text(NSLOCTEXT("SWindowVotingForProposals", "SWindowVotingForProposals.New", "Leave your idea"))
		//	.TextStyle(&Style->Changes.VoteTimer)
		//	.OnClicked_Lambda([&]() { OnCreate.ExecuteIfBound(); return FReply::Handled(); })
		//	.IsEnabled_Lambda([&]() { return (FDateTime::UtcNow().ToUnixTimestamp() - FPlayerEntityInfo::Instance.DateOfNextAvailableProposal >= 0); })
		//	.ToolTip(SNew(SLokaToolTip)
		//			 .Text_Lambda([&]() 
		//			{
		//				return FText::Format
		//				(
		//					NSLOCTEXT("SWindowVotingForProposals", "SWindowVotingForProposals.AvailableIn", "Available in {0} minuts"),
		//					FText::AsNumber(FTimespan::FromSeconds(FPlayerEntityInfo::Instance.DateOfNextAvailableProposal - FDateTime::UtcNow().ToUnixTimestamp()).GetMinutes())
		//				); 
		//			})
		//			.IsEnabled_Lambda([&]() {
		//				return (FPlayerEntityInfo::Instance.DateOfNextAvailableProposal - FDateTime::UtcNow().ToUnixTimestamp() >= 0); 
		//			}))
		//]
		+ SVerticalBox::Slot()
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
			[
				SAssignNew(Widget_List, SListView<FImprovementViewPtr>)
				.SelectionMode(ESelectionMode::Single)
				.ListItemsSource(&Items)
				.ExternalScrollbar(ScrollBar)
				.OnGenerateRow_Lambda([&](FImprovementViewPtr Item, const TSharedRef<STableViewBase>& OwnerTable) {
					return SNew(SVotingProposalRow, OwnerTable, Item).OnVoted_Lambda([&, i = Item.ToSharedRef()] (bool IsPlus) {
						OnVoted.ExecuteIfBound(FImprovementVoteCreate(i->ImprovementId, IsPlus));
					});
				})
				.OnSelectionChanged_Lambda([&](FImprovementViewPtr Item, ESelectInfo::Type InInfo) {
					if (Item.IsValid() && InInfo != ESelectInfo::Direct)
					{
						OnDetails.ExecuteIfBound(Item);
					}
				})
			]
			+ SHorizontalBox::Slot().AutoWidth()
			[
				ScrollBar.ToSharedRef()
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SWindowVotingForProposals::OnFillProposalsIssues(const TArray<FImprovementView>& InData)
{
	Items.Empty();

	for (auto item : InData)
	{
		Items.Add(MakeShareable(new FImprovementView(item)));
	}

	Widget_List->RequestListRefresh();
}