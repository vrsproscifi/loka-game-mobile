#pragma once
typedef FString FJsonGuid;

namespace ObjectSerializer
{
	//	UStruct Object
	struct JsonObject
	{
		template<typename Object>
		static FString Serialize(const Object& object)
		{
			FString jsonStr;
			jsonStr.Reserve(128);
			static_assert(std::is_class<Object>::value, "Is not child of UStruct");
			FJsonObjectConverter::UStructToJsonObjectString(Object::StaticStruct(), &object, jsonStr, 0, 0);
			return jsonStr;
		}

		template<typename Object>
		static bool DeSerialize(const FString& objectStr, Object &object)
		{
			static_assert(std::is_class<Object>::value, "Is not child of UStruct");
			bool isValidJson = FJsonObjectConverter::JsonObjectStringToUStruct(objectStr, &object, 0, 0);
			return isValidJson;
		}
	};

	//	UStruct Object Array
	struct JsonArrayObject
	{
		template<typename Object>
		static FString Serialize(const Object& object)
		{
			static_assert(std::is_class<Object>::value, "Is not child of UStruct");
			static_assert(false, "Serialize JsonArrayObject not supported");
			return "";
		}

		template<typename Object>
		static bool DeSerialize(const FString& objectStr, Object &object)
		{
			static_assert(std::is_class<Object>::value, "Is not child of UStruct");
			bool isValidJson = FJsonObjectConverter::JsonArrayStringToUStruct(objectStr, &object, 0, 0);
			return isValidJson;
		}
	};

	//	Online JSON Object
	struct NativeObject
	{
		template<typename Object>
		static FString Serialize(const Object& object)
		{
			static_assert(std::is_base_of<FOnlineJsonSerializable, Object>::value, "Is not child of FOnlineJsonSerializable");
			const auto jsonStr = object.ToJson();
			return jsonStr;
		}

		template<typename Object>
		static bool DeSerialize(const FString& objectStr, Object &object)
		{
			static_assert(std::is_base_of<FOnlineJsonSerializable, Object>::value, "Is not child of FOnlineJsonSerializable");
			bool isValidJson = object.FromJson(objectStr);
			return isValidJson;
		}

	};
}

enum class OperationResponseType
{
	None,

	//	UStruct Object
	JsonObject,

	//	UStruct Object Array
	JsonArray,

	//	Online JSON Object
	NativeObject,

	//	Online JSON Object Array
	//NativeArray,
};

typedef TBaseDelegate<void, const FString&> FOnRequestDelegate;
