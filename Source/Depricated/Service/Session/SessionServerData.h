#pragma once

#include "SessionServerData.generated.h"

namespace EServerType
{
	enum Type
	{
		Room,
		Tournament,
		Duel,
		End
	};
}


enum class EServerState : uint8
{
	None,
	WaitingCommand,
	PostWaitingCommand,
	WaitingPlayers,
	PostWaitingPlayers,
	Playing,
	WaitingEndMatch,
	PostWaitingEndMatch,
	End
};



USTRUCT(BlueprintType, Blueprintable)
struct FServerRandNames
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FString> Names;
};


DECLARE_DELEGATE_OneParam(FOnRequestServerState, int32&);
DECLARE_DELEGATE_OneParam(FOnRequestServerRegister, const bool);
DECLARE_DELEGATE_OneParam(FOnRequestServerRndNames, const FServerRandNames&);