#pragma once

#include "TournamentSession.generated.h"

USTRUCT(Blueprintable)
struct FTournamentSessionClient
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly)
	TArray<FString> Players;

};
