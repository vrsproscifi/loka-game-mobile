#pragma once
#include "TournamentDetails.generated.h"

UENUM()
enum class ETournamentState : uint8
{
	Preparing,

	Executing,

	Performed
};

USTRUCT(BlueprintType, Blueprintable)
struct FTournamentPreview
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly)
	ETournamentState State;

	UPROPERTY(BlueprintReadOnly)
	int32 Award;

	UPROPERTY(BlueprintReadOnly)
	int32 AwardType;

	UPROPERTY(BlueprintReadOnly)
	int32 JoinPrice;

	UPROPERTY(BlueprintReadOnly)
	int32 JoinPriceType;

	UPROPERTY(BlueprintReadOnly)
	int32 Deadline;

	UPROPERTY(BlueprintReadOnly)
	int32 FreeSlots;

	UPROPERTY(BlueprintReadOnly)
	FString Uid;
	
	UPROPERTY(BlueprintReadOnly)
	bool IsFixed;

	UPROPERTY(BlueprintReadOnly)
	FString name;

	UPROPERTY(BlueprintReadOnly)
	FString desc;
};

//DECLARE_DELEGATE_OneParam(FOnTournamentList, const TArray<FTournamentPreview>&);
