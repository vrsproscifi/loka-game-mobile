#pragma once
#include "TournamentDetails.h"
#include "TournamentPreview.generated.h"

UENUM()
enum class ETournamentRoundNumber : uint8
{
	First = 1,
	Second,
	Final
};


USTRUCT(Blueprintable)
struct FTournamentCorporationPreview
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly)  
	FString Uid;

	UPROPERTY(BlueprintReadOnly)  
	FString Name;
};

USTRUCT(Blueprintable)
struct FTournamentMatchDetails
{
	GENERATED_USTRUCT_BODY()


	UPROPERTY(BlueprintReadOnly)
	ETournamentRoundNumber RoundNumber;

	UPROPERTY(BlueprintReadOnly)
	FTournamentCorporationPreview First;

	UPROPERTY(BlueprintReadOnly)
	FTournamentCorporationPreview Second;

	UPROPERTY(BlueprintReadOnly)
	FString WinnerUid;
};

USTRUCT(Blueprintable)
struct FTournamentDetails
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly)  
	FString TournamentUid;

	UPROPERTY(BlueprintReadOnly)  
	int32 DeadlineSeconds;

	UPROPERTY(BlueprintReadOnly)
		ETournamentState State;

	UPROPERTY(BlueprintReadOnly)  
	TArray<FTournamentMatchDetails> MatchList;

	UPROPERTY(BlueprintReadOnly)
	FString name;
};

//DECLARE_DELEGATE_OneParam(FOnTournamentDetails, const FTournamentDetails&);
