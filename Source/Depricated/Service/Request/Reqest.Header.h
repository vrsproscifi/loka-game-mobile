#pragma once

#ifndef H_REQUEST_HEADER
#define H_REQUEST_HEADER

#include "RequestOneParam.h"


struct FHttpResponseCode
{
	FString Url;
	int32 Code;

	FString Data;
	FString	Error;

	static const int32 StartErrorCode = 520;
};
typedef const struct FHttpResponseCode& FHttpResponseCodeRef;


DECLARE_DELEGATE_OneParam(FOnRequestErrorHandler, FHttpResponseCodeRef);


#define DEFINE_ONLINE_SERVICE_JSON_ARRAY(DelegateName, RequestURL, DelegateType, ResponseType) \
protected: \
	FHttpRequestCompleteDelegate HTTP##DelegateName;\
	DECLARE_DELEGATE_OneParam(FOn##DelegateName##, const TArray<##ResponseType##>&);\
public: \
	FOn##DelegateName Delegate_On##DelegateName;\
	FORCEINLINE bool Request##DelegateName(const DelegateType &Request)\
	{\
		return PostRequest(RequestURL, Request.ToJson(), HTTP##DelegateName);\
	}\
void OnHTTP##DelegateName(FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSucceeded)\
{\
	TArray<ResponseType> Response;\
	if (IReqestInterface::isValidHttpResponse(HttpResponse) && IReqestInterface::JsonArrayStringToSerializable(HttpResponse, &Response))\
	{\
		Delegate_On##DelegateName.ExecuteIfBound(Response);\
	}\
}

#define DEFINE_ONLINE_SERVICE_JSON(DelegateName, RequestURL, DelegateType, ResponseType) \
protected: \
	FHttpRequestCompleteDelegate HTTP##DelegateName;\
	DECLARE_DELEGATE_OneParam(FOn##DelegateName##, const ResponseType##&);\
public: \
	FOn##DelegateName Delegate_On##DelegateName;\
	FORCEINLINE bool Request##DelegateName(const DelegateType &Request)\
	{\
		return PostRequest(RequestURL, Request.ToJson(), HTTP##DelegateName);\
	}\
void OnHTTP##DelegateName(FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSucceeded)\
{\
	ResponseType Response;\
	if (IReqestInterface::isValidHttpResponse(HttpResponse) && Response.FromJson(HttpResponse->GetContentAsString()))\
	{\
		Delegate_On##DelegateName.ExecuteIfBound(Response);\
	}\
}


#define DEFINE_ONLINE_SERVICE_POST(DelegateName, RequestURL, DelegateType) \
protected: \
	FHttpRequestCompleteDelegate HTTP##DelegateName;\
public: \
	FOn##DelegateName Delegate_On##DelegateName;\
	FORCEINLINE bool Post##DelegateName(const DelegateType &Request)\
	{\
		return PostRequest(RequestURL, Request.ToJson(), HTTP##DelegateName);\
	}\
void OnHTTP##DelegateName(FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSucceeded)\
{\
	IReqestInterface::isValidHttpResponse(HttpResponse);\
}


#define DEFINE_ONLINE_SERVICE_OBJECT(DelegateName, RequestURL, DelegateType, ResponseType) \
protected: \
	FHttpRequestCompleteDelegate HTTP##DelegateName;\
	DECLARE_DELEGATE_OneParam(FOn##DelegateName##, const ResponseType##&);\
public: \
	FOn##DelegateName Delegate_On##DelegateName;\
	FORCEINLINE bool Request##DelegateName(const DelegateType &Request)\
	{\
		return PostRequest(RequestURL, Request.ToJson(), HTTP##DelegateName);\
	}\
void OnHTTP##DelegateName(FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSucceeded)\
{\
	ResponseType Response;\
	if (IReqestInterface::isValidHttpResponse(HttpResponse) && IReqestInterface::JsonObjectStringToUStruct(HttpResponse, &Response))\
	{\
		Delegate_On##DelegateName.ExecuteIfBound(Response);\
	}\
}


#define DEFINE_ONLINE_SERVICE_ARRAY(DelegateName, RequestURL, DelegateType, ResponseType) \
protected: \
	FHttpRequestCompleteDelegate HTTP##DelegateName;\
	DECLARE_DELEGATE_OneParam(FOn##DelegateName##, const TArray<##ResponseType##>&);\
public: \
	FOn##DelegateName Delegate_On##DelegateName;\
	FORCEINLINE bool Request##DelegateName(const DelegateType &Request)\
	{\
		return PostRequest(RequestURL, Request.ToJson(), HTTP##DelegateName);\
	}\
void OnHTTP##DelegateName(FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSucceeded)\
{\
	TArray<ResponseType> Response;\
	if (IReqestInterface::isValidHttpResponse(HttpResponse) && IReqestInterface::JsonArrayStringToUStruct(HttpResponse, &Response))\
	{\
		Delegate_On##DelegateName.ExecuteIfBound(Response);\
	}\
}

#define DEFINE_ONLINE_SERVICE_DELEGATE(DelegateName)\
HTTP##DelegateName.BindRaw(this, &__CLASS__::OnHTTP##DelegateName);


#define DEFINE_SERVICE_ERROR_HANDLER(DelegateName) \
protected:\
DECLARE_MULTICAST_DELEGATE(FOnErrorHandler_##DelegateName##);\
public:\
	FOnErrorHandler_##DelegateName ErrorHandler_##DelegateName##;\
private:\
	void Catch##DelegateName##(const FString &ResponceStr)\
	{\
		ErrorHandler_##DelegateName##.Broadcast();\
	}\

#define DEFINE_SERVICE_ERROR_HANDLER_OneParam(DelegateName, ResponseType) \
protected:\
DECLARE_MULTICAST_DELEGATE_OneParam(FOnErrorHandler_##DelegateName##, const ResponseType##&);\
public:\
	FOnErrorHandler_##DelegateName ErrorHandler_##DelegateName##;\
private:\
	void Catch##DelegateName##(const FString &ResponceStr)\
	{\
		ResponseType Response;\
		if (Response.FromJson(ResponceStr))\
		{\
			ErrorHandler_##DelegateName##.Broadcast(Response);\
		}\
	}\

#define DEFINE_SERVICE_ERROR_DELEGATE(DelegateName)\
RequestErrorHandlers.FindOrAdd(__CLASS__::EHttpResponseError::##DelegateName##).BindRaw(this, &__CLASS__::Catch##DelegateName##)

#endif