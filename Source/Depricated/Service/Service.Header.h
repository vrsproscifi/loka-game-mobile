#pragma once

/** Forward declarations of all interface classes */
typedef TSharedPtr<class CCorporationSerivce, ESPMode::ThreadSafe>			FCorporationSerivcePtr;
typedef TSharedPtr<class CTournamentSerivce, ESPMode::ThreadSafe>				FTournamentSerivcePtr;

typedef class IServiceInterface* OnlineServicePtr;

#define DEFINE_SERVICE_MEMBER_DECLARATION(ServiceName)\
F##ServiceName##SerivcePtr ServiceOf##ServiceName##;


#define DEFINE_SERVICE_MEMBER_DEFINITION(ServiceName)\
ServiceOf##ServiceName = ManagerOfService->Get##ServiceName##Interface();\
checkf(ServiceOf##ServiceName##.IsValid(), ANSI_TO_TCHAR("was nullptr"));


#define PTR_OF_SERVICE(ServiceName) ServiceOf##ServiceName


#define DEFINE_SERVICE_CALLBACK_DECLARATION(DelegateName, ResponseType)\
typedef ResponseType def##DelegateName;\
void On##DelegateName(const ResponseType&);

#define DEFINE_SERVICE_CALLBACK_DEFINITION(DelegateName)\
void __CLASS__::On##DelegateName(const def##DelegateName& Response)

#define DEFINE_SERVICE_CALLBACK_BIND(DelegateName, ServiceName)\
PTR_OF_SERVICE(ServiceName)->Delegate_On##DelegateName##.BindUObject(this, &__CLASS__::On##DelegateName##);

#define DECLARE_SERVICE_ERROR_EVENT(EventName, ResponseType)\
typedef ResponseType defNotify##EventName;\
void OnNotify##EventName(const ResponseType&);

#define DEFINE_SERVICE_ERROR_EVENT(EventName)\
void __CLASS__::OnNotify##EventName(const defNotify##EventName& Response)

#define ADD_SERVICE_ERROR_EVENT(ServiceName, EventName)\
PTR_OF_SERVICE(ServiceName)->ErrorHandler_##EventName##.AddUObject(this, &__CLASS__::OnNotify##EventName##);