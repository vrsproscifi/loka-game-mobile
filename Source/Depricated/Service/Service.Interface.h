#pragma once

#include "Core.h"
#include "Ticker.h"
#include "ModuleManager.h"
#include "Identity.Token.h"
#include "OnlineDelegateMacros.h"
#include "Service.Header.h"




class IServiceInterface : public IIdentityToken
{

public:
	virtual FCorporationSerivcePtr GetCorporationInterface() const = 0;
	virtual FTournamentSerivcePtr GetTournamentInterface() const = 0;

	

	virtual ~IServiceInterface() = 0
	{

	}

};
