#pragma once

typedef TSharedPtr<FString> TokenPtr;

class IIdentityToken
{
protected:
	TokenPtr m_TokenStr;

public:
	IIdentityToken()
	{

	}

	IIdentityToken(const TokenPtr &Token)
		: m_TokenStr(Token)
	{

	}

	FString& getToken()
	{
		return *m_TokenStr;
	}

	TokenPtr& getTokenPtr()
	{
		return m_TokenStr;
	}

	const TokenPtr& getTokenPtr() const
	{
		return m_TokenStr;
	}

	void setToken(const FString &Token)
	{
		checkf(m_TokenStr.IsValid(), TEXT("TokenStr was nullptr"));
		*m_TokenStr = Token;
	}


	void setTokenPtr(const TokenPtr &Token)
	{
		checkf(Token.IsValid(), TEXT("Set nullptr Token"));
		m_TokenStr = Token;
	}
};