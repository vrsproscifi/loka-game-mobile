#include "LokaGame.h"
#include "CorporationService.h"

#define __CLASS__ CCorporationSerivce

CCorporationSerivce::CCorporationSerivce(IServiceInterface * Service)
	: IReqestInterface(Service)
	//-------------------------------------------
	, ms_QueryReqestInfo("ReqestInformation")
	, ms_QueryReqestJoin("RequestJoin")
	, ms_QueryReqestEdit("Edit")
	, ms_QueryReqestEditMemberRole("EditMemberRole")
	, ms_QueryReqestKickMember("KickMember")
{
	SERVICE_CONTROLLER = "api/Corporation";

	m_OnCorporationInfoCompleteReqest.BindRaw(this, &CCorporationSerivce::OnCorporationInfoReqestComplete);
	m_OnCorporationEditCompleteReqest.BindRaw(this, &CCorporationSerivce::OnCorporationEditCompleteReqest);

	DEFINE_ONLINE_SERVICE_DELEGATE(CorporationSearch);
	DEFINE_ONLINE_SERVICE_DELEGATE(CorporationList);

	DEFINE_ONLINE_SERVICE_DELEGATE(ToDonate);
	DEFINE_ONLINE_SERVICE_DELEGATE(GetChatMessages);
	DEFINE_ONLINE_SERVICE_DELEGATE(AddChatMessage);

	DEFINE_SERVICE_ERROR_DELEGATE(NotFoundCorporation);
	DEFINE_SERVICE_ERROR_DELEGATE(CorporationWasPrivate);
	DEFINE_SERVICE_ERROR_DELEGATE(YouAreInCorporation);
	DEFINE_SERVICE_ERROR_DELEGATE(YouAreNotInCorporation);
	DEFINE_SERVICE_ERROR_DELEGATE(PlayerAreInCorporation);
	DEFINE_SERVICE_ERROR_DELEGATE(PlayerAreNotInCorporation);
	DEFINE_SERVICE_ERROR_DELEGATE(NeedMoneyForCreate);
	DEFINE_SERVICE_ERROR_DELEGATE(CorporationNameWasExist);
	DEFINE_SERVICE_ERROR_DELEGATE(CorporationNameWasInvalid);
	DEFINE_SERVICE_ERROR_DELEGATE(CorporationAbbrWasExist);
	DEFINE_SERVICE_ERROR_DELEGATE(CorporationAbbrWasInvalid);
	DEFINE_SERVICE_ERROR_DELEGATE(CorporationPropertyWasChanged);
	DEFINE_SERVICE_ERROR_DELEGATE(CorporationMemberWasChanged);
	DEFINE_SERVICE_ERROR_DELEGATE(CorporationMemberWasKicked);
	DEFINE_SERVICE_ERROR_DELEGATE(NotAllowedPermission);
	DEFINE_SERVICE_ERROR_DELEGATE(PermissionArgumemtOutOfRange);
	DEFINE_SERVICE_ERROR_DELEGATE(CorporationDonateSuccess);
	DEFINE_SERVICE_ERROR_DELEGATE(CorporationDonateFail);
}

CCorporationSerivce::~CCorporationSerivce()
{
}


bool CCorporationSerivce::ReqestCorporationInfo(const uint32& index)
{
	return PostRequest(ms_QueryReqestInfo, FRequestOneParam<int32>(index).ToJson(), m_OnCorporationInfoCompleteReqest);
}

void CCorporationSerivce::OnCorporationInfoReqestComplete(FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSucceeded)
{
	if (isValidHttpResponse(HttpResponse))
	{
		if (HttpResponse->GetResponseCode() == FCorporationDetailGuestView::StatusCode)
		{
			FCorporationDetailGuestView Response;
			if (JsonObjectStringToUStruct(HttpResponse, &Response))
			{
				Delegate_OnCorporationViewOfGuest.ExecuteIfBound(Response);
			}
		}
		else
		{
			FCorporationDetailMemberView Response;
			if (JsonObjectStringToUStruct(HttpResponse, &Response))
			{
				Delegate_OnCorporationViewOfMember.ExecuteIfBound(Response);
			}
		}
	}
}

bool CCorporationSerivce::RequestJoin(const FRequestOneParam<uint32>& index)
{
	return PostRequest(ms_QueryReqestJoin, index.ToJson(), m_OnCorporationInfoCompleteReqest);
}

bool CCorporationSerivce::RequestEdit(const FCorporationEdit& Data)
{
	return PostRequest(ms_QueryReqestEdit, Data.ToJson(), m_OnCorporationEditCompleteReqest);
}

void CCorporationSerivce::OnCorporationEditCompleteReqest(FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSucceeded)
{
	if (isValidHttpResponse(HttpResponse) || HttpResponse->GetResponseCode() == CorporationPropertyWasChanged)
	{
		Delegate_OnCorporationAcceptEdit.ExecuteIfBound(true);
	}
	else
	{
		Delegate_OnCorporationAcceptEdit.ExecuteIfBound(false);
	}
}

bool CCorporationSerivce::RequestSetMemberRole(const FCorporationEditMemberRole &Request)
{
	return PostRequest(ms_QueryReqestEditMemberRole, Request.ToJson(), m_OnCorporationInfoCompleteReqest);
}

bool CCorporationSerivce::RequestKickMemeber(const FRequestOneParam<int32> &Request)
{
	return PostRequest(ms_QueryReqestKickMember, Request.ToJson(), m_OnCorporationInfoCompleteReqest);
}