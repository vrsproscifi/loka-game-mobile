#pragma once
#include "CorporationMemberAccess.generated.h"

UENUM()
enum class ECorporationMemberAccess : uint8
{
	Member,
	Organizer,
	Assistant,
	Leader,
};