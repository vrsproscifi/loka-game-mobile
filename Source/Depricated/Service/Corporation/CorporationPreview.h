#pragma once
#include "CorporationAccess.h"
#include "CorporationMemberAccess.h"
#include "CorporationPreview.generated.h"



USTRUCT(BlueprintType, Blueprintable)
struct FCorporationMember
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly)
	int32 PlayerId;

	UPROPERTY(BlueprintReadOnly)
	FString Name;

	UPROPERTY(BlueprintReadOnly)
	ECorporationMemberAccess Access;

	UPROPERTY(BlueprintReadOnly)
	int32 LastActivity;
};

USTRUCT(BlueprintType, Blueprintable)
 struct FCorporationDetailMemberView
{
	GENERATED_USTRUCT_BODY()
		
	static const int StatusCode = 200;


	UPROPERTY(BlueprintReadOnly)
		int32 Index;

	UPROPERTY(BlueprintReadOnly)
		int32 LeaderId;

	UPROPERTY(BlueprintReadOnly)
		int32 Donate;

	UPROPERTY(BlueprintReadOnly)
		int32 Money;

	UPROPERTY(BlueprintReadOnly)
		FString Name;

	UPROPERTY(BlueprintReadOnly)
		FString Abbr;

	UPROPERTY(BlueprintReadOnly)
		FDateTime Founded;

	UPROPERTY(BlueprintReadOnly)
		int32 JoinPrice;

	UPROPERTY(BlueprintReadOnly)
		ECorporationAccess Access;

	UPROPERTY(BlueprintReadOnly)
		TArray<FCorporationMember> Members;
};


 USTRUCT(BlueprintType, Blueprintable)
 struct FCorporationDetailGuestView
 {
	 GENERATED_USTRUCT_BODY()

		 static const int StatusCode = 201;

	 UPROPERTY(BlueprintReadOnly)
		 int32 Index;

	 UPROPERTY(BlueprintReadOnly)
		 int32 LeaderId;

	 UPROPERTY(BlueprintReadOnly)
		 FString Name;

	 UPROPERTY(BlueprintReadOnly)
		 FString Abbr;

	 UPROPERTY(BlueprintReadOnly)
		 int32 JoinPrice;

	 UPROPERTY(BlueprintReadOnly)
		 ECorporationAccess Access;

	 UPROPERTY(BlueprintReadOnly)
		 FDateTime Founded;

	 UPROPERTY(BlueprintReadOnly)
		 TArray<FCorporationMember> Members;
 };

 DECLARE_DELEGATE_OneParam(FOnCorporationMemberView, const FCorporationDetailMemberView&);
 DECLARE_DELEGATE_OneParam(FOnCorporationGuestView, const FCorporationDetailGuestView&);