#pragma once
#include "CorporationAccess.h"
#include "CorporationMemberAccess.h"
#include "CorporationDetails.generated.h"



//====================================================================================================================================================================================

USTRUCT(BlueprintType, Blueprintable)
struct FCorporationPreviewResponse
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly)
		int32 Index;

	UPROPERTY(BlueprintReadOnly)
		int32 Members;

	UPROPERTY(BlueprintReadOnly)
		int32 Rating;
	
	UPROPERTY(BlueprintReadOnly)
		int32 Position;

	UPROPERTY(BlueprintReadOnly)
		FString Name;

	UPROPERTY(BlueprintReadOnly)
		FString Abbr;

	UPROPERTY(BlueprintReadOnly)
		int32 JoinPrice;

	UPROPERTY(BlueprintReadOnly)
		ECorporationAccess Access;
};

DECLARE_DELEGATE_OneParam(FOnCorporationList, const TArray<FCorporationPreviewResponse>&);

//====================================================================================================================================================================================

USTRUCT(BlueprintType, Blueprintable)
struct FCorporationDetails
{
	GENERATED_USTRUCT_BODY()

};

struct FCorporationEdit : public FOnlineJsonSerializable
{
	FString Name;
	FString Abbr;
	int32 JoinPrice;
	int32 Access;

	BEGIN_ONLINE_JSON_SERIALIZER
		ONLINE_JSON_SERIALIZE("Name", Name);
		ONLINE_JSON_SERIALIZE("Abbr", Abbr);
		ONLINE_JSON_SERIALIZE("JoinPrice", JoinPrice);
		ONLINE_JSON_SERIALIZE("Access", Access);
	END_ONLINE_JSON_SERIALIZER
};

DECLARE_DELEGATE_OneParam(FOnCorporationAcceptEdit, const bool);

struct FCorporationEditMemberRole : public FOnlineJsonSerializable
{
	FCorporationEditMemberRole() : PlayerId(0), Access(0) {}
	FCorporationEditMemberRole(const int32 &index, const int32 &role) : PlayerId(index), Access(role) {}

	int32 PlayerId;
	int32 Access;

	BEGIN_ONLINE_JSON_SERIALIZER
		ONLINE_JSON_SERIALIZE("PlayerId", PlayerId);
		ONLINE_JSON_SERIALIZE("Access", Access);
	END_ONLINE_JSON_SERIALIZER
};

struct FCorporationDonate : public FOnlineJsonSerializable
{
	FCorporationDonate(const bool isd, const int32& v) : IsDonate(isd), Value(v) {}

	bool IsDonate;
	int32 Value;

	BEGIN_ONLINE_JSON_SERIALIZER
		ONLINE_JSON_SERIALIZE("IsDonate", IsDonate);
		ONLINE_JSON_SERIALIZE("Value", Value);
	END_ONLINE_JSON_SERIALIZER
};
