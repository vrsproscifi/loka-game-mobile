// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class LokaGameServerTarget : TargetRules
{
	public LokaGameServerTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Server;
		bUsesSteam = false;

        ExtraModuleNames.Add("LokaGame");
        ExtraModuleNames.Add("LokaGameLoading");
    }   
}
