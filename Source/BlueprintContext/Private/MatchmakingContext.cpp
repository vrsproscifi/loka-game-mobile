// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "BlueprintContext.h"


#include "UTLocalPlayer.h"

#include "MatchmakingContext.h"

#include "BlueprintContextLibrary.h"

#define LOCTEXT_NAMESPACE "UTMatchmakingContext"

REGISTER_CONTEXT(UMatchmakingContext);

UMatchmakingContext::UMatchmakingContext()
{
}

void UMatchmakingContext::Initialize()
{
	//UUTGameInstance* GameInstance = GetGameInstance<UUTGameInstance>();
	//if (GameInstance)
	//{
	//	UUTMatchmaking* UTMatchmaking = GameInstance->GetMatchmaking();
	//	if (UTMatchmaking)
	//	{
	//		UTMatchmaking->OnMatchmakingStarted().AddUObject(this, &ThisClass::OnMatchmakingStartedInternal);
	//		UTMatchmaking->OnMatchmakingStateChange().AddUObject(this, &ThisClass::OnMatchmakingStateChangeInternal);
	//		UTMatchmaking->OnMatchmakingComplete().AddUObject(this, &ThisClass::OnMatchmakingCompleteInternal);
	//		UTMatchmaking->OnPartyStateChange().AddUObject(this, &ThisClass::OnPartyStateChangeInternal);
	//	}
	//}
}

void UMatchmakingContext::Finalize()
{
	//UUTGameInstance* GameInstance = GetGameInstance<UUTGameInstance>();
	//if (GameInstance)
	//{
	//	UUTMatchmaking* UTMatchmaking = GameInstance->GetMatchmaking();
	//	if (UTMatchmaking)
	//	{
	//		UTMatchmaking->OnMatchmakingStarted().RemoveAll(this);
	//		UTMatchmaking->OnMatchmakingStateChange().RemoveAll(this);
	//		UTMatchmaking->OnMatchmakingComplete().RemoveAll(this);
	//		UTMatchmaking->OnPartyStateChange().RemoveAll(this);
	//	}
	//}
}

void UMatchmakingContext::OnMatchmakingStartedInternal(bool bRanked)
{
	OnMatchmakingStarted.Broadcast();
}

//void UMatchmakingContext::OnMatchmakingStateChangeInternal(EMatchmakingState::Type OldState, EMatchmakingState::Type NewState, const FMMAttemptState& MMState)
//{
//	OnMatchmakingStateChange.Broadcast(OldState, NewState);
//}

//void UMatchmakingContext::OnMatchmakingCompleteInternal(EMatchmakingCompleteResult EndResult)
//{
////	OnMatchmakingComplete.Broadcast(EndResult);
//}

//void UMatchmakingContext::OnPartyStateChangeInternal(EUTPartyState NewPartyState)
//{
//
//}


void UMatchmakingContext::AttemptReconnect(const FString& OldSessionId)
{
	//UUTLocalPlayer* LocalPlayer = GetOwningPlayer<UUTLocalPlayer>();
	//if (LocalPlayer)
	//{
	//	if (!LocalPlayer->IsPartyLeader())
	//	{
	//		// Show error dialog
	//		LocalPlayer->MessageBox(LOCTEXT("ReconnectNotPartyLeaderTitle", "Must Be The Party Leader"), LOCTEXT("ReconnectNotPartyLeader", "You must be the party leader to initiate matchmaking."));
	//		return;
	//	}
	//
	//	LocalPlayer->AttemptMatchmakingReconnect(OldSessionId);
	//}
}

#undef LOCTEXT_NAMESPACE