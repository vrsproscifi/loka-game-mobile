// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#ifndef __LokaGameLoading_H__
#define __LokaGameLoading_H__

#include "ModuleInterface.h"
#include "SlateBasics.h"

#if !UE_SERVER
#include "MoviePlayer.h"
#endif

/** Module interface for this game's loading screens */
class ILokaGameLoadingModule : public IModuleInterface
{
public:

#if !UE_SERVER && !WITH_EDITOR

	/** Kicks off the loading screen for in game loading (not startup) */
	virtual void PlayMovie(bool bForce) = 0;

	// Stops a movie from playing
	virtual void StopMovie() = 0;

	/** Tells the movie player that it's ok to exit and/or wait for the movie to finished */
	virtual void WaitForMovieToFinished() = 0;

	// returns true if a movie is currently being played
	virtual bool IsMoviePlaying() = 0;

	virtual void SetLoadingConfiguration(const FSlateBrush* InImage, const FText& InTitle, const FText& InTip) = 0;

#endif

};

#endif // __LokaGameLoading_H__


