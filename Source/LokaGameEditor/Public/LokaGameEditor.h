// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.
#pragma once

#ifndef __LOKAGAMEEDITOR_H__
#define __LOKAGAMEEDITOR_H__


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	Engine Includes
#include "Engine.h"
#include "Net/UnrealNetwork.h"

#include "Core.h"
#include "Platform.h"
#include "CoreUObject.h"
#include "ModuleManager.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	Slate Includes
#include "SlateCore.h"
#include "SlateBasics.h"
#include "SlateExtras.h"

#include "LokaGameEditorClasses.h"

DECLARE_LOG_CATEGORY_EXTERN(LogLokaGameEditor, All, All)

class LOKAGAMEEDITOR_API FLokaGameEditorModule : public FDefaultModuleImpl
{
public:

	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};

#endif
