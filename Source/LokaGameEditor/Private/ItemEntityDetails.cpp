﻿
#include "LokaGameEditor.h"
#include "ItemEntityDetails.h"
#include "Item/ItemBaseEntity.h"
#include "DetailLayoutBuilder.h"
#include "DetailWidgetRow.h"
#include "DetailCategoryBuilder.h"
#include "IDetailsView.h"
#include "IDetailPropertyRow.h"
#include "DetailLayoutBuilder.h"


#include "PhysicsEngine/BodySetup.h"
#include "PhysicsEngine/PhysicsAsset.h"
#include "GameSingleton.h"
#include "Models/ListOfInstance.h"

TSharedRef<IDetailCustomization> FItemEntitySaveLoadDetails::MakeInstance()
{
	return MakeShareable(new FItemEntitySaveLoadDetails);
}

void FItemEntitySaveLoadDetails::CustomizeDetails(IDetailLayoutBuilder& InDetailLayout)
{
	this->DetailLayout = &InDetailLayout;

	IDetailCategoryBuilder& CustomCategory = InDetailLayout.EditCategory("Description", NSLOCTEXT("FItemEntitySaveLoadDetails", "FItemEntitySaveLoadDetails.Title", "Description & Entity Actions"), ECategoryPriority::Important);
	
	auto TargetFont = FCoreStyle::Get().GetWidgetStyle<FTextBlockStyle>("NormalText").Font;
	TargetFont.Size = 16;

	FDetailWidgetRow& CustomRow = CustomCategory.AddCustomRow(FText::FromString("Save Load"), false);
	CustomRow.WholeRowContent()
	.HAlign(HAlign_Fill)
	[
		SNew(SVerticalBox)
		+ SVerticalBox::Slot().AutoHeight()
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
			[
				SNew(SButton)
				.HAlign(HAlign_Center)
				.VAlign(VAlign_Center)
				.Text(FText::FromString("Load Entity"))
				.ToolTipText(FText::FromString("Load property from master server."))
				.OnClicked(this, &FItemEntitySaveLoadDetails::OnClickedLoad)
			]
			+ SHorizontalBox::Slot()
			[
				SNew(SButton)
				.HAlign(HAlign_Center)
				.VAlign(VAlign_Center)
				.Text(FText::FromString("Save Entity"))
				.ToolTipText(FText::FromString("Save current property state to master server."))
				.OnClicked(this, &FItemEntitySaveLoadDetails::OnClickedSave)
			]
			+ SHorizontalBox::Slot()
			[
				SNew(SButton)
				.HAlign(HAlign_Center)
				.VAlign(VAlign_Center)
				.Text(FText::FromString("[WIP] Generate ModelId"))
				.ToolTipText(FText::FromString("Find and paste freely model index in current category."))
				.OnClicked(this, &FItemEntitySaveLoadDetails::OnClickedGenerate)
			]
		]
		+ SVerticalBox::Slot().AutoHeight()
		[
			SNew(SBorder)
			.Visibility_Lambda([&]() { 
				auto SelectedObject = GetSelectedObject();
				return (SelectedObject && SelectedObject->bEntityActionInProgress) ? EVisibility::Visible : EVisibility::Collapsed;
			})
			.BorderImage(new FSlateColorBrush(FColorList::LimeGreen))
			.Padding(8)
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Center)
			[
				SNew(STextBlock)
				.Font(TargetFont)
				.ColorAndOpacity(FColorList::Black)
				.Text(FText::FromString("Please wait, entity action is on progress."))
			]
		]
		+ SVerticalBox::Slot().AutoHeight()
		[
			SNew(SBorder)
			.Visibility_Lambda([&]() {
				auto SelectedObject = GetSelectedObject();
				return (SelectedObject && SelectedObject->bEntityActionFail) ? EVisibility::Visible : EVisibility::Collapsed;
			})
			.BorderImage(new FSlateColorBrush(FColorList::OrangeRed))
			.Padding(8)
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Center)
			[
				SNew(STextBlock)
				.Font(TargetFont)
				.ColorAndOpacity(FColorList::White)
				.Text(FText::FromString("Error in current entity action, please verfy."))
			]
		]
		+ SVerticalBox::Slot().AutoHeight()
		[
			SNew(SBorder)
			.Visibility_Lambda([&]() {
				auto SelectedObject = GetSelectedObject();
				return (SelectedObject && SelectedObject->bEntityWasLoaded == false) ? EVisibility::Visible : EVisibility::Collapsed;
			})
			.BorderImage(new FSlateColorBrush(FColorList::VioletRed))
			.Padding(8)
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Center)
			[
				SNew(STextBlock)
				.Font(TargetFont)
				.ColorAndOpacity(FColorList::White)
				.Text(FText::FromString("Please 'Load' before edit and save."))
			]
		]
	];	


	ItemMassHandle = InDetailLayout.GetProperty(GET_MEMBER_NAME_CHECKED(UItemBaseEntity, ItemMass));

	if (ItemMassHandle->IsValidHandle())
	{
		IDetailPropertyRow& PropertyRow = CustomCategory.AddProperty(GET_MEMBER_NAME_CHECKED(UItemBaseEntity, ItemMass));
		TSharedPtr<SWidget> NameWidget;
		TSharedPtr<SWidget> ValueWidget;
		FDetailWidgetRow Row;

		PropertyRow.GetDefaultWidgets(NameWidget, ValueWidget, Row);
		PropertyRow.CustomWidget(true)
		.NameContent()
		[
			ItemMassHandle->CreatePropertyNameWidget()
		]
		.ValueContent()
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
			[
				ItemMassHandle->CreatePropertyValueWidget()
			]
			+ SHorizontalBox::Slot().AutoWidth()
			[
				SNew(SButton) // Calculate mesh mass
				.Text(FText::FromString("Calculate"))
				.OnClicked(this, &FItemEntitySaveLoadDetails::OnClickedCalculate)
			]
		];
	}

}

UItemBaseEntity* FItemEntitySaveLoadDetails::GetSelectedObject()
{
	if (DetailLayout)
	{
		if (auto view = DetailLayout->GetDetailsView())
		{
			const TArray<TWeakObjectPtr<UObject>>& Objects = view->GetSelectedObjects();
			if (Objects.Num() == 1)
			{
				return Cast<UItemBaseEntity>(Objects[0].Get());
			}
		}
	}

	return nullptr;
}

FReply FItemEntitySaveLoadDetails::OnClickedCalculate()
{
	if (ItemMassHandle->IsValidHandle())
	{
		if (auto EntityItem = GetSelectedObject())
		{
			if (EntityItem->SkeletalMesh && EntityItem->SkeletalMesh->PhysicsAsset)
			{
				float CalculatedMass = .0f;
				auto PhysicsAsset = EntityItem->SkeletalMesh->PhysicsAsset;

				for (auto BodyInstance : PhysicsAsset->SkeletalBodySetups)
				{
					if (BodyInstance && BodyInstance->BoneName != NAME_None)
					{				
						const float CalcMass = BodyInstance->CalculateMass();
						CalculatedMass += CalcMass;
						UE_LOG(LogLokaGameEditor, Log, TEXT("Calculate mass >> Bone = '%s', Mass = '%.2f'"), *BodyInstance->BoneName.ToString(), CalcMass);
					}
				}

				ItemMassHandle->SetValue(CalculatedMass);
				UE_LOG(LogLokaGameEditor, Log, TEXT("Calculate mass >> Completed = '%.2f'"), CalculatedMass);
			}
		}		
	}

	return FReply::Handled();
}

FReply FItemEntitySaveLoadDetails::OnClickedLoad()
{
	if (auto EntityItem = GetSelectedObject())
	{
		EntityItem->OnProxyRequestLoad();

		UE_LOG(LogLokaGameEditor, Log, TEXT("Send request to load data for '%s'"), *EntityItem->GetName());
	}

	return FReply::Handled();
}

FReply FItemEntitySaveLoadDetails::OnClickedSave()
{
	if (auto EntityItem = GetSelectedObject())
	{
		EntityItem->OnProxyRequestSave();

		UE_LOG(LogLokaGameEditor, Log, TEXT("Send request to save data from '%s'"), *EntityItem->GetName());
	}

	return FReply::Handled();
}

FReply FItemEntitySaveLoadDetails::OnClickedGenerate()
{
	if (auto EntityItem = GetSelectedObject())
	{
		if (EntityItem->GetModelId() == INDEX_NONE)
		{
			const auto localCurrentCategory = EntityItem->GetCategory();

			TArray<int32> FoundModels;

			auto LastLoadedData = UGameSingleton::Get()->LastListOfInstance;
			auto ArrayLoadedObjects = UGameSingleton::Get()->FindList(EntityItem->GetCategory());

			for (auto TargetData : LastLoadedData.Items)
			{
				if (TargetData.CategoryTypeId == localCurrentCategory)
				{
					if (UGameSingleton::Get()->FindItem(FItemModelInfo(TargetData.ModelId, TargetData.CategoryTypeId)))
					{
						FoundModels.Add(TargetData.ModelId);
					}
				}
			}

			// Сортируем по найденым индексам по порядку			

			FoundModels.Sort(TLess<int32>());

			// Ищем свободный индекс между найденами

			int32 LastIndex = INDEX_NONE, FoundFreeIndex = INDEX_NONE;
			for (int32 i = 0; i < FoundModels.Num(); ++i)
			{
				if (LastIndex + 1 == FoundModels[i])
				{
					LastIndex = FoundModels[i];
				}
				else if(i < FoundModels.Num() - 1)
				{
					FoundFreeIndex = LastIndex + 1;
					break;
				}
			}

			// Задаём индекс моделу с соотвесвием найденых

			FNotificationInfo NotificationInfo(FText::GetEmpty());
			NotificationInfo.ExpireDuration = 20.0f;
			NotificationInfo.bUseThrobber = false;
			NotificationInfo.bUseSuccessFailIcons = true;

			if (FoundFreeIndex != INDEX_NONE)
			{
				EntityItem->EntityDescription.ModelId = FoundFreeIndex;
				NotificationInfo.Text = FText::FromString(FString::Printf(TEXT("Generated model index from free between found.\n Please recompile and reload singleton manually!")));
			}
			else
			{
				EntityItem->EntityDescription.ModelId = LastIndex + 1;
				NotificationInfo.Text = FText::FromString(FString::Printf(TEXT("Generated model index from last found.\n Please recompile and reload singleton manually!")));
			}				

			auto MyNotify = FSlateNotificationManager::Get().AddNotification(NotificationInfo);
			MyNotify->SetCompletionState(SNotificationItem::ECompletionState::CS_Success);
		}
		else
		{
			FNotificationInfo NotificationInfo(FText::FromString(FString::Printf(TEXT("Failed generate model index for %s, becuse current model index is not 'INDEX_NONE'"), *EntityItem->GetName())));
			NotificationInfo.ExpireDuration = 20.0f;
			NotificationInfo.bUseThrobber = false;
			NotificationInfo.bUseSuccessFailIcons = true;

			auto MyNotify = FSlateNotificationManager::Get().AddNotification(NotificationInfo);
			MyNotify->SetCompletionState(SNotificationItem::ECompletionState::CS_Fail);
		}
	}

	return FReply::Handled();
}
