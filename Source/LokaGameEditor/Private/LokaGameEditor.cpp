// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "LokaGameEditor.h"
#include "PropertyEditorModule.h"
#include "LevelEditor.h"
#include "ItemEntityDetails.h"
#include "TypeCostDetails.h"
#include "Editor.h"
#include "Developer/MessageLog/Public/MessageLogModule.h"
#include "ProfileHackDetails.h"
#include "IAssetRegistry.h"
#include "GameSingleton.h"
#include "ShooterGameInstance.h"

//#include "LokaGameEditor.generated.inl"

DEFINE_LOG_CATEGORY(LogLokaGameEditor);

class LOKAGAMEEDITOR_API FLokaExtenderCommands : public TCommands<FLokaExtenderCommands>
{
public:

	FLokaExtenderCommands() : TCommands<FLokaExtenderCommands>(
		"FLokaExtenderCommands", // Context name for fast lookup
		NSLOCTEXT("FLokaExtenderCommands", "FLokaExtenderCommands", "Loka Editor Extender Commands"), // Localized context name for displaying
		NAME_None, "EditorStyle")
	{}

	enum ECommand
	{
		ReloadSingleton,
		ReloadSingletonWithUpdate,
		CaptureImages,
		End
	};

	virtual void RegisterCommands() override
	{
		FUICommandInfo::MakeCommandInfo(
			this->AsShared(),
			Command[ECommand::ReloadSingleton],
			TEXT("ReloadSingleton"),
			FText::FromString("Reload Singleton"),
			FText::FromString("Reload entity items"),
			FSlateIcon("EditorStyle", "Cascade.RegenerateLowestLOD", "Cascade.RegenerateLowestLOD.Small"),
			EUserInterfaceActionType::Button,
			FInputChord()
		);

		FUICommandInfo::MakeCommandInfo(
			this->AsShared(),
			Command[ECommand::ReloadSingletonWithUpdate],
			TEXT("ReloadSingletonWithUpdate"),
			FText::FromString("Reload Singleton With Update BPs"),
			FText::FromString("Reload entity items and update data in default assets"),
			FSlateIcon("EditorStyle", "Cascade.RegenerateLowestLOD", "Cascade.RegenerateLowestLOD.Small"),
			EUserInterfaceActionType::Button,
			FInputChord()
		);

		FUICommandInfo::MakeCommandInfo(
			this->AsShared(),
			Command[ECommand::CaptureImages],
			TEXT("CaptureImages"),
			FText::FromString("Capture Images"),
			FText::FromString("Capture images from all entity items, work in progress just now not working!"),
			FSlateIcon("EditorStyle", "StaticMeshEditor.SaveThumbnail", "StaticMeshEditor.SaveThumbnail.Small"),
			EUserInterfaceActionType::Button,
			FInputChord()
		);

		UICommandList = MakeShareable(new FUICommandList());

		UICommandList->MapAction(Command[ECommand::ReloadSingleton], FExecuteAction::CreateLambda([]() {
			for (auto It = TObjectIterator<UGameSingleton>(); It; ++It)
			{
				if (It && It->IsValidLowLevel())
				{
					It->ReloadGameData();
				}
			}
		}));
		
		UICommandList->MapAction(Command[ECommand::ReloadSingletonWithUpdate], FExecuteAction::CreateLambda([]() {
			for (auto It = TObjectIterator<UGameSingleton>(); It; ++It)
			{
				if (It && It->IsValidLowLevel())
				{
					It->ReloadGameData();
					//It->LastListOfInstance;
				}
			}
		}));

		UICommandList->MapAction(Command[ECommand::CaptureImages], FExecuteAction::CreateLambda([]() {
			UGameSingleton::Get()->DoCaptureEntityImages();		
			//auto Notify = FSlateNotificationManager::Get().AddNotification(FNotificationInfo(FText::FromString("just now not working!"))).ToSharedRef();
			//Notify->SetExpireDuration(10);
			//Notify->ExpireAndFadeout();
		}));
	}

	FORCENOINLINE static FLokaExtenderCommands& GetAlt()
	{
		if (Instance.IsValid() == false)
		{
			FLokaExtenderCommands::Register();
		}

		return *(Instance.Pin());
	}

	FORCEINLINE TSharedRef<FUICommandList> GetCommandList() const { return UICommandList.ToSharedRef(); }
	FORCEINLINE TSharedRef<FUICommandInfo> GetCommand(const ECommand& InCommandTarget) const { return Command[InCommandTarget].ToSharedRef(); }

	static void GenerateMenu(FToolBarBuilder& InBuilder)
	{
		InBuilder.AddToolBarButton(GetAlt().GetCommand(ECommand::ReloadSingleton));
		InBuilder.AddComboButton(
			FUIAction(),
			FOnGetContent::CreateStatic(&FLokaExtenderCommands::GenerateSubList)
			);
		InBuilder.AddToolBarButton(GetAlt().GetCommand(ECommand::CaptureImages));
	}

protected:

	TSharedPtr<FUICommandInfo> Command[ECommand::End];
	TSharedPtr<FUICommandList> UICommandList;

	static TSharedRef<SWidget> GenerateSubList()
	{
		FMenuBuilder MenuBuilder(true, GetAlt().UICommandList);

		MenuBuilder.BeginSection(TEXT("SubList"), FText::FromString("Optional actions"));
		{
			MenuBuilder.AddMenuEntry(GetAlt().GetCommand(ECommand::ReloadSingletonWithUpdate));
		}

		return MenuBuilder.MakeWidget();
	}
};

typedef FLokaExtenderCommands::ECommand ELokaExtenderCommand;

void FLokaGameEditorModule::StartupModule()
{
	FLokaExtenderCommands::Register();

	FPropertyEditorModule& PropertyModule = FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");
	PropertyModule.RegisterCustomPropertyTypeLayout("TypeCost", FOnGetPropertyTypeCustomizationInstance::CreateStatic(&FTypeCostDetails::MakeInstance));
	PropertyModule.RegisterCustomPropertyTypeLayout("ProfileHack", FOnGetPropertyTypeCustomizationInstance::CreateStatic(&FProfileHackDetails::MakeInstance));
	PropertyModule.RegisterCustomClassLayout("ItemBaseEntity", FOnGetDetailCustomizationInstance::CreateStatic(&FItemEntitySaveLoadDetails::MakeInstance));

	FLevelEditorModule& LevelEditorModule = FModuleManager::LoadModuleChecked<FLevelEditorModule>("LevelEditor");

	TSharedPtr<FExtender> MyExtender = MakeShareable(new FExtender);
	MyExtender->AddToolBarExtension("Content", EExtensionHook::First, FLokaExtenderCommands::Get().GetCommandList(), FToolBarExtensionDelegate::CreateStatic(&FLokaExtenderCommands::GenerateMenu));
	LevelEditorModule.GetToolBarExtensibilityManager()->AddExtender(MyExtender);

	FMessageLogModule& MessageLogModule = FModuleManager::LoadModuleChecked<FMessageLogModule>("MessageLog");
	{
		FMessageLogInitializationOptions InitOptions;
		InitOptions.bDiscardDuplicates = true;
		MessageLogModule.RegisterLogListing("LokaErrors", NSLOCTEXT("LokaEditorErrors", "LokaEditorErrors.General", "LOKA Errors"), InitOptions);
	}
}

void FLokaGameEditorModule::ShutdownModule()
{

}

IMPLEMENT_MODULE(FLokaGameEditorModule, LokaGameEditor);