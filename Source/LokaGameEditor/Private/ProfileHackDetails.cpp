
#include "LokaGameEditor.h"
#include "LokaGame.h"
#include "DetailWidgetRow.h"
#include "ProfileHackDetails.h"
#include "IDetailChildrenBuilder.h"
#include "Containers/InventoryHacks.h"
#include "Editor/PropertyEditor/Public/PropertyCustomizationHelpers.h"
#include "Character/CharacterBaseEntity.h"
#include "Weapon/ItemWeaponEntity.h"
#include "Weapon/ItemPistolWeaponEntity.h"
#include "GameSingleton.h"
#include "IDetailPropertyRow.h"
#include "Module/ItemModuleEntity.h"
#include "Material/ItemMaterialEntity.h"


TSharedRef<IPropertyTypeCustomization> FProfileHackDetails::MakeInstance()
{
	return MakeShareable(new FProfileHackDetails);
}

void FProfileHackDetails::CustomizeHeader(TSharedRef<class IPropertyHandle> InStructPropertyHandle, class FDetailWidgetRow& HeaderRow, IPropertyTypeCustomizationUtils& StructCustomizationUtils)
{

}

void FProfileHackDetails::CustomizeChildren(TSharedRef<class IPropertyHandle> InStructPropertyHandle, class IDetailChildrenBuilder& StructBuilder, IPropertyTypeCustomizationUtils& StructCustomizationUtils)
{
	CharacterHandle = InStructPropertyHandle->GetChildHandle(GET_MEMBER_NAME_CHECKED(FProfileHack, CharacterModelId));
	PrimaryWeaponHandle = InStructPropertyHandle->GetChildHandle(GET_MEMBER_NAME_CHECKED(FProfileHack, PrimaryWeaponModelId));
	PrimaryWeaponSkinHandle = InStructPropertyHandle->GetChildHandle(GET_MEMBER_NAME_CHECKED(FProfileHack, PrimaryWeaponSkin));
	SecondaryWeaponHandle = InStructPropertyHandle->GetChildHandle(GET_MEMBER_NAME_CHECKED(FProfileHack, SecondaryWeaponModelId));
	SecondaryWeaponSkinHandle = InStructPropertyHandle->GetChildHandle(GET_MEMBER_NAME_CHECKED(FProfileHack, SecondaryWeaponSkin));
	PrimaryWeaponAddonHandle = InStructPropertyHandle->GetChildHandle(GET_MEMBER_NAME_CHECKED(FProfileHack, PrimaryWeaponAddons));
	SecondaryWeaponAddonHandle = InStructPropertyHandle->GetChildHandle(GET_MEMBER_NAME_CHECKED(FProfileHack, SecondaryWeaponAddons));

	FDetailWidgetRow& CharacterRow = StructBuilder.AddProperty(CharacterHandle.ToSharedRef()).CustomWidget();
	CharacterRow
	.NameContent()
	[
		SNew(STextBlock)
		.Text(FText::FromString("Character"))
	]
	.ValueContent()
	[
		SNew(SClassPropertyEntryBox)
		.AllowAbstract(true)
		.AllowNone(true)
		.HideViewOptions(false)
		.IsBlueprintBaseOnly(false)
		.MetaClass(UCharacterBaseEntity::StaticClass())
		.OnSetClass_Raw(this, &FProfileHackDetails::OnClassSelected, ETargetProperty::Character, static_cast<int32>(INDEX_NONE))
		.SelectedClass_Raw(this, &FProfileHackDetails::GetSelectedClass, ETargetProperty::Character, static_cast<int32>(INDEX_NONE))
	];

	FDetailWidgetRow& PrimaryWeaponRow = StructBuilder.AddProperty(PrimaryWeaponHandle.ToSharedRef()).CustomWidget();
	PrimaryWeaponRow
	.NameContent()
	[
		SNew(STextBlock)
		.Text(FText::FromString("Primary Weapon"))
	]
	.ValueContent()
	[
		SNew(SClassPropertyEntryBox)
		.AllowAbstract(true)
		.AllowNone(true)
		.HideViewOptions(false)
		.IsBlueprintBaseOnly(false)
		.MetaClass(UItemWeaponEntity::StaticClass())
		.OnSetClass_Raw(this, &FProfileHackDetails::OnClassSelected, ETargetProperty::PrimaryWeapon, static_cast<int32>(INDEX_NONE))
		.SelectedClass_Raw(this, &FProfileHackDetails::GetSelectedClass, ETargetProperty::PrimaryWeapon, static_cast<int32>(INDEX_NONE))
	];

	FDetailWidgetRow& SecondaryWeaponRow = StructBuilder.AddProperty(SecondaryWeaponHandle.ToSharedRef()).CustomWidget();
	SecondaryWeaponRow
	.NameContent()
	[
		SNew(STextBlock)
		.Text(FText::FromString("Secondary Weapon"))
	]
	.ValueContent()
	[
		SNew(SClassPropertyEntryBox)
		.AllowAbstract(true)
		.AllowNone(true)
		.HideViewOptions(false)
		.IsBlueprintBaseOnly(false)
		.MetaClass(UItemPistolWeaponEntity::StaticClass())
		.OnSetClass_Raw(this, &FProfileHackDetails::OnClassSelected, ETargetProperty::SecondaryWeapon, static_cast<int32>(INDEX_NONE))
		.SelectedClass_Raw(this, &FProfileHackDetails::GetSelectedClass, ETargetProperty::SecondaryWeapon, static_cast<int32>(INDEX_NONE))
	];

	FDetailWidgetRow& PrimaryWeaponSkinRow = StructBuilder.AddProperty(PrimaryWeaponSkinHandle.ToSharedRef()).CustomWidget();
	PrimaryWeaponSkinRow
	.NameContent()
	[
		SNew(STextBlock)
		.Text(FText::FromString("Primary Weapon Skin"))
	]
	.ValueContent()
	[
		SNew(SClassPropertyEntryBox)
		.AllowAbstract(true)
		.AllowNone(true)
		.HideViewOptions(false)
		.IsBlueprintBaseOnly(false)
		.MetaClass(UItemMaterialEntity::StaticClass())
		.OnSetClass_Raw(this, &FProfileHackDetails::OnClassSelected, ETargetProperty::PrimaryWeaponSkin, static_cast<int32>(INDEX_NONE))
		.SelectedClass_Raw(this, &FProfileHackDetails::GetSelectedClass, ETargetProperty::PrimaryWeaponSkin, static_cast<int32>(INDEX_NONE))
	];

	FDetailWidgetRow& SecondaryWeaponSkinRow = StructBuilder.AddProperty(SecondaryWeaponSkinHandle.ToSharedRef()).CustomWidget();
	SecondaryWeaponSkinRow
	.NameContent()
	[
		SNew(STextBlock)
		.Text(FText::FromString("Secondary Weapon Skin"))
	]
	.ValueContent()
	[
		SNew(SClassPropertyEntryBox)
		.AllowAbstract(true)
		.AllowNone(true)
		.HideViewOptions(false)
		.IsBlueprintBaseOnly(false)
		.MetaClass(UItemMaterialEntity::StaticClass())
		.OnSetClass_Raw(this, &FProfileHackDetails::OnClassSelected, ETargetProperty::SecondaryWeaponSkin, static_cast<int32>(INDEX_NONE))
		.SelectedClass_Raw(this, &FProfileHackDetails::GetSelectedClass, ETargetProperty::SecondaryWeaponSkin, static_cast<int32>(INDEX_NONE))
	];



	TSharedRef<FDetailArrayBuilder> PrimaryWeaponAddonBuilder = MakeShareable(new FDetailArrayBuilder(PrimaryWeaponAddonHandle.ToSharedRef()));
	PrimaryWeaponAddonBuilder->SetDisplayName(FText::FromString("Primary Weapon Addons"));
	PrimaryWeaponAddonBuilder->OnGenerateArrayElementWidget(FOnGenerateArrayElementWidget::CreateLambda([&](TSharedRef<IPropertyHandle> InHandle, int32 InNumber, IDetailChildrenBuilder& InBuilder)
	{
		InBuilder.AddProperty(InHandle).CustomWidget()
		.NameContent()
		[
			InHandle->CreatePropertyNameWidget()
		]
		.ValueContent()
		[
			SNew(SClassPropertyEntryBox)
			.AllowAbstract(true)
			.AllowNone(true)
			.HideViewOptions(false)
			.IsBlueprintBaseOnly(false)
			.MetaClass(UItemModuleEntity::StaticClass())
			.OnSetClass_Raw(this, &FProfileHackDetails::OnClassSelected, ETargetProperty::PrimaryAddons, InNumber)
			.SelectedClass_Raw(this, &FProfileHackDetails::GetSelectedClass, ETargetProperty::PrimaryAddons, InNumber)
		];
	}));
	StructBuilder.AddCustomBuilder(PrimaryWeaponAddonBuilder);

	TSharedRef<FDetailArrayBuilder> SecondaryWeaponAddonBuilder = MakeShareable(new FDetailArrayBuilder(SecondaryWeaponAddonHandle.ToSharedRef()));
	SecondaryWeaponAddonBuilder->SetDisplayName(FText::FromString("Secondary Weapon Addons"));
	SecondaryWeaponAddonBuilder->OnGenerateArrayElementWidget(FOnGenerateArrayElementWidget::CreateLambda([&](TSharedRef<IPropertyHandle> InHandle, int32 InNumber, IDetailChildrenBuilder& InBuilder) 
	{
		InBuilder.AddProperty(InHandle).CustomWidget()
		.NameContent()
		[
			InHandle->CreatePropertyNameWidget()
		]
		.ValueContent()
		[
			SNew(SClassPropertyEntryBox)
			.AllowAbstract(true)
			.AllowNone(true)
			.HideViewOptions(false)
			.IsBlueprintBaseOnly(false)
			.MetaClass(UItemModuleEntity::StaticClass())
			.OnSetClass_Raw(this, &FProfileHackDetails::OnClassSelected, ETargetProperty::PrimaryAddons, InNumber)
			.SelectedClass_Raw(this, &FProfileHackDetails::GetSelectedClass, ETargetProperty::PrimaryAddons, InNumber)
		];
	}));
	StructBuilder.AddCustomBuilder(SecondaryWeaponAddonBuilder);
}

void FProfileHackDetails::OnClassSelected(const UClass* InClass, const ETargetProperty InTarget, const int32 InArrayIndex)
{
	const UItemBaseEntity* TargetObject = InClass ? Cast<UItemBaseEntity>(const_cast<UClass*>(InClass)->GetDefaultObject()) : nullptr;

	switch (InTarget)
	{
		case Character: CharacterHandle->SetValue(TargetObject ? TargetObject->GetModelId() : INDEX_NONE); break;
		case PrimaryWeapon: PrimaryWeaponHandle->SetValue(TargetObject ? TargetObject->GetModelId() : INDEX_NONE); break;
		case SecondaryWeapon: SecondaryWeaponHandle->SetValue(TargetObject ? TargetObject->GetModelId() : INDEX_NONE); break;
		case PrimaryWeaponSkin: PrimaryWeaponSkinHandle->SetValue(TargetObject ? TargetObject->GetModelId() : INDEX_NONE); break;
		case SecondaryWeaponSkin: SecondaryWeaponSkinHandle->SetValue(TargetObject ? TargetObject->GetModelId() : INDEX_NONE); break;
		case PrimaryAddons: 
			PrimaryWeaponAddonHandle->GetChildHandle(InArrayIndex)->SetValue(IsSupportSelectedModule(Cast<const UItemModuleEntity>(TargetObject), PrimaryWeaponHandle.ToSharedRef(), PrimaryWeaponAddonHandle.ToSharedRef()) ? TargetObject->GetModelId() : INDEX_NONE);
			break;
		case SecondaryAddons:
			SecondaryWeaponAddonHandle->GetChildHandle(InArrayIndex)->SetValue(IsSupportSelectedModule(Cast<const UItemModuleEntity>(TargetObject), SecondaryWeaponHandle.ToSharedRef(), SecondaryWeaponAddonHandle.ToSharedRef()) ? TargetObject->GetModelId() : INDEX_NONE);
			break;
		case End: break;
		default:
			break;
	}
}

const UClass* FProfileHackDetails::GetSelectedClass(const ETargetProperty InTarget, const int32 InArrayIndex) const
{ 
	FItemModelInfo TargetInfo;
	int32 Value = INDEX_NONE;

	switch (InTarget)
	{
		case Character: 			
			CharacterHandle->GetValue(Value);
			TargetInfo = FItemModelInfo(Value, ECategoryTypeId::Character);
			break;
		case PrimaryWeapon:
			PrimaryWeaponHandle->GetValue(Value);
			TargetInfo = FItemModelInfo(Value, ECategoryTypeId::Weapon);
			break;
		case SecondaryWeapon:
			SecondaryWeaponHandle->GetValue(Value);
			TargetInfo = FItemModelInfo(Value, ECategoryTypeId::Weapon);
			break;
		case PrimaryAddons:
			PrimaryWeaponAddonHandle->GetChildHandle(InArrayIndex)->GetValue(Value);
			TargetInfo = FItemModelInfo(Value, ECategoryTypeId::Addon);
			break;
		case SecondaryAddons:
			SecondaryWeaponAddonHandle->GetChildHandle(InArrayIndex)->GetValue(Value);
			TargetInfo = FItemModelInfo(Value, ECategoryTypeId::Addon);
			break;
		case PrimaryWeaponSkin:
			PrimaryWeaponSkinHandle->GetValue(Value);
			TargetInfo = FItemModelInfo(Value, ECategoryTypeId::Material);
			break;
		case SecondaryWeaponSkin:
			SecondaryWeaponSkinHandle->GetValue(Value);
			TargetInfo = FItemModelInfo(Value, ECategoryTypeId::Material);
			
			break;
		case End: break;
		default: ;
	}

	UItemBaseEntity* FoundObject = FoundObject = UGameSingleton::Get()->FindItem(TargetInfo);
	return (FoundObject && Value != INDEX_NONE) ? FoundObject->GetClass() : nullptr;
}

bool FProfileHackDetails::IsSupportSelectedModule(const UItemModuleEntity* InTargetObject, TSharedRef<IPropertyHandle> InWeaponHandle, TSharedRef<IPropertyHandle> InArrayHandle) const
{
	int32 WeaponValue;
	uint32 Childrens;
	TArray<FName> InstalledSockets;

	InWeaponHandle->GetValue(WeaponValue);
	InArrayHandle->GetNumChildren(Childrens);
	for (uint32 i = 0; i < Childrens; ++i)
	{
		int32 PropertyValue;
		InArrayHandle->GetChildHandle(i)->GetValue(PropertyValue);

		if (auto FoundAddon = UGameSingleton::Get()->FindItem<UItemModuleEntity>(FItemModelInfo(PropertyValue, ECategoryTypeId::Addon)))
		{
			InstalledSockets.Add(FoundAddon->TargetSocket);
		}
	}

	auto TargetWeapon = UGameSingleton::Get()->FindItem<UItemWeaponEntity>(FItemModelInfo(WeaponValue, ECategoryTypeId::Weapon));
	if (InTargetObject && TargetWeapon)
	{
		if (!InstalledSockets.Contains(InTargetObject->TargetSocket) && TargetWeapon->SupportedModules.Contains(InTargetObject->GetModelId()) != false)
		{
			return true;
		}
	}

	return false;
}