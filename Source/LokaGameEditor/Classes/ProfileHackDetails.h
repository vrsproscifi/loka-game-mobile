
#pragma once

#include "CoreMinimal.h"
#include "IPropertyTypeCustomization.h"
#include "PropertyHandle.h"

class UItemModuleEntity;
class UItemWeaponEntity;
class UItemBaseEntity;

class FProfileHackDetails : public IPropertyTypeCustomization
{
public:
	static TSharedRef<IPropertyTypeCustomization> MakeInstance();

	virtual void CustomizeHeader(TSharedRef<class IPropertyHandle> InStructPropertyHandle, class FDetailWidgetRow& HeaderRow, IPropertyTypeCustomizationUtils& StructCustomizationUtils) override;
	virtual void CustomizeChildren(TSharedRef<class IPropertyHandle> InStructPropertyHandle, class IDetailChildrenBuilder& StructBuilder, IPropertyTypeCustomizationUtils& StructCustomizationUtils) override;

	enum ETargetProperty
	{
		Character,
		PrimaryWeapon,
		PrimaryWeaponSkin,
		SecondaryWeapon,
		SecondaryWeaponSkin,
		PrimaryAddons,
		SecondaryAddons,
		End
	};

protected:

	void OnClassSelected(const UClass*, const ETargetProperty, const int32 = INDEX_NONE);
	const UClass* GetSelectedClass(const ETargetProperty, const int32 = INDEX_NONE) const;

	bool IsSupportSelectedModule(const UItemModuleEntity* InTargetObject, TSharedRef<IPropertyHandle> InWeaponHandle, TSharedRef<IPropertyHandle> InArrayHandle) const;

	TSharedPtr<IPropertyHandle> CharacterHandle;
	TSharedPtr<IPropertyHandle> PrimaryWeaponHandle;
	TSharedPtr<IPropertyHandle> PrimaryWeaponSkinHandle;
	TSharedPtr<IPropertyHandle> SecondaryWeaponHandle;
	TSharedPtr<IPropertyHandle> SecondaryWeaponSkinHandle;
	TSharedPtr<IPropertyHandle> PrimaryWeaponAddonHandle;
	TSharedPtr<IPropertyHandle>	SecondaryWeaponAddonHandle;	
};