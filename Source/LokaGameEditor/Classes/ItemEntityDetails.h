
#pragma once

#include "CoreMinimal.h"
#include "Input/Reply.h"
#include "IDetailCustomization.h"

class IDetailLayoutBuilder;
class IPropertyHandle;
class UItemBaseEntity;

class FItemEntitySaveLoadDetails : public IDetailCustomization
{
public:
	static TSharedRef<IDetailCustomization> MakeInstance();

private:
	virtual void CustomizeDetails(IDetailLayoutBuilder& InDetailLayout) override;

	FReply OnClickedLoad();
	FReply OnClickedSave();
	FReply OnClickedGenerate();

	IDetailLayoutBuilder* DetailLayout;
	UItemBaseEntity* GetSelectedObject();

	TSharedPtr<IPropertyHandle> ItemMassHandle;

	FReply OnClickedCalculate();
};