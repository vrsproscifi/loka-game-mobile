// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;
using System.IO;

public class LokaGameEditorTarget : TargetRules
{
    public LokaGameEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
        bUsesSteam = true;

        ExtraModuleNames.Add("LokaGame");
        ExtraModuleNames.Add("LokaGameEditor");
        ExtraModuleNames.Add("LokaGameLoading");

        if (!IsLicenseeBuild())
        {
            ExtraModuleNames.Add("OnlineSubsystemMcp");
        }

        ExtraModuleNames.Add("OnlineSubsystemNull");
    }

    //
    // TargetRules interface.
    //
	
	bool IsLicenseeBuild()
    {
        return !Directory.Exists("Runtime/NotForLicensees");
    }
	
	//    public override GUBPProjectOptions GUBP_IncludeProjectInPromotedBuild_EditorTypeOnly(UnrealTargetPlatform HostPlatform)
    //{
    //    var Result = new GUBPProjectOptions();
    //    Result.bIsPromotable = true;
    //    Result.bSeparateGamePromotion = true;
    //    Result.bCustomWorkflowForPromotion = true;
    //    return Result;
    //}
}
