// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameState/TutorialBase.h"
#include "TutorialBuilding.generated.h"

class ABuildPlacedComponent;

UCLASS(Blueprintable, Abstract)
class LOKAGAME_API UTutorialBuilding : public UTutorialBase
{
	GENERATED_BODY()
	
public:

	UTutorialBuilding();

	UFUNCTION()
	void OnBuildingSelected(ABuildPlacedComponent* InComponent);

	UFUNCTION()
	void OnBuildingPlaced(ABuildPlacedComponent* InComponent);

	UFUNCTION()
	void OnBuildingPreRemove(ABuildPlacedComponent* InComponent);

	bool IsValidBuildingCheck(ABuildPlacedComponent* InComponent, const ETutorialAction::Type InCheck) const;
};
