#include "LokaGame.h"
#include "ConstructionPlayerState.h"
#include "ConstructionPlayerController.h"

#include "IdentityComponent.h"
#include "InventoryComponent.h"
#include "ProfileComponent.h"
#include "ProgressComponent.h"
#include "ConstructionGameState.h"
#include "ConstructionComponent.h"
#include "MarketComponent.h"
#include "Build/SBuildSystem_Inventory.h"
#include "Build/SBuildSystem_ToolBar.h"
#include "BuildSystem/BuildHelperComponent.h"
#include "ConstructionCharacter.h"
#include "UTGameViewportClient.h"
#include "SWindowBuyBuilding.h"
#include "SMessageBox.h"
#include "PlayerInventoryItem.h"
#include "RichTextHelpers.h"
#include "Item/ItemBaseEntity.h"
#include "SquadComponent/Slate/MatchMakingInformationWidget/MatchMakingInformationWidget.h"
#include "SquadComponent/Slate/SquadInformationWidget/SquadInformationWidget.h"
#include "SquadComponent.h"
#include "SLobbyContainer.h"
#include "SLobbyChat.h"
#include "ChatComponent.h"
#include "SRadialMenu.h"
#include "SResourceTextBox.h"
#include "SScaleBox.h"
#include "TutorialComponent.h"
#include "SNotifyList.h"
#include "SGuestContainer.h"
#include "Windows/SWindowGuestMode.h"
#include "StringTableRegistry.h"

AConstructionPlayerState::AConstructionPlayerState()
	: Super()
{
	MarketComponent = CreateDefaultSubobject<UMarketComponent>(TEXT("MarketComponent"));
	MarketComponent->SetIsReplicated(true);

	ChatComponent = CreateDefaultSubobject<UChatComponent>(TEXT("ChatComponent"));
	ChatComponent->SetIsReplicated(true);

	ProgressComponent->OnLoadFractionProgressComplete.BindUObject(MarketComponent, &UMarketComponent::OnLoadFractionProgressComplete);
	MarketComponent->OnUnlockItemSuccessfullyHandler.AddUObject(InventoryComponent, &UInventoryComponent::OnUnlockItemSuccessfully);
	MarketComponent->OnSellItemEvent.AddUObject(InventoryComponent, &UInventoryComponent::OnSellItemHandle);
	MarketComponent->OnUnlockItemSuccessfullyHandler.AddLambda([&](const FUnlockItemResponseContainer&)
	{
		IdentityComponent->SendRequestClientDataUpdate();
	});

	MarketComponent->OnGetInventoryItem.BindLambda([&](const FGuid& InId)
	{
		if (auto MyGameState = GetWorldGameState<AConstructionGameState>(GetWorld()))
		{
			return MyGameState->GetInventoryComponent()->FindItem(InId);
		}

		return (UPlayerInventoryItem*)nullptr;
	});

	PrimaryActorTick.bCanEverTick = true;
	SetActorTickEnabled(true);
}

void AConstructionPlayerState::OnCreateUserInterface()
{
	Super::OnCreateUserInterface();

#if !UE_SERVER
	auto TargetViewport = GetGameViewportClient();
	if (TargetViewport)
	{
		if (Slate_LobbyContainer.IsValid())
		{
			SIZE_T _ForCount = Slate_LobbyContainer->GetWindowsContainer()->GetChildren()->Num();
			for (SIZE_T i = 0; i < _ForCount; ++i)
			{
				Slate_LobbyContainer->ToggleWidget(false);
			}

			TargetViewport->RemoveViewportWidgetContent(Slate_LobbyContainer.ToSharedRef());
		}

		if (LastOwnedState)
		{
			SAssignNew(Slate_LobbyContainer, SLobbyContainer)
			.PlayerState(this);

			SAssignNew(Slate_BuyBuilding, SWindowBuyBuilding)
			.Cash_Lambda([&]() {
				return GetIdentityComponent()->GetPlayerInfo().Cash;
			});

			SAssignNew(Slate_ToolBar, SBuildSystem_ToolBar, Slate_LobbyContainer->GetWindowsContainer())
			.Tag(TEXT("Window.BuildToolBar"))
			.SelectedSlot_Lambda([this]() {
				auto MyCharacter = GetConstructionCharacter();
				auto MyBuildHelper = MyCharacter ? MyCharacter->GetBuildHelper() : nullptr;

				return MyBuildHelper ? MyBuildHelper->GetSelectedNum() : 0;
			})
			.CurrentMode_Lambda([&]() {
				auto MyCharacter = GetConstructionCharacter();
				auto MyBuildHelper = MyCharacter ? MyCharacter->GetBuildHelper() : nullptr;

				if (MyBuildHelper)
				{
					return MyBuildHelper->GetCurrentMode();
				}
				return TEnumAsByte<EBuildHelperMode::Type>(EBuildHelperMode::None);
			})
			.OnDropBuildInSlot_Lambda([&](const int32& InSlotTarget, UPlayerInventoryItem* InItemTarget) {
				auto MyCharacter = GetConstructionCharacter();
				auto MyBuildHelper = MyCharacter ? MyCharacter->GetBuildHelper() : nullptr;

				if (MyBuildHelper)
				{
					MyBuildHelper->SetSlotFromItem(InSlotTarget, InItemTarget);
				}
			});

			SAssignNew(Slate_Inventory, SBuildSystem_Inventory, Slate_LobbyContainer->GetWindowsContainer())
			.OnClickedBuyItem_Lambda([&](UPlayerInventoryItem* InItem) {
				if (InItem && InItem->GetEntity())
				{
					ClientShowBuyRequest(InItem->GetEntity()->GetModelInfo(), Slate_BuyBuilding->GetCurrentItemAmount());
				}
			})
			.OnClickedSellItem_Lambda([&](UPlayerInventoryItem* InItem) {
				ClientShowSellRequest(InItem, Slate_BuyBuilding->GetCurrentItemAmount());
			});			

			Slate_LobbyContainer->GetWindowsContainer()->AddSlot()[Slate_Inventory.ToSharedRef()];
			Slate_LobbyContainer->GetWindowsContainer()->AddSlot()[Slate_ToolBar.ToSharedRef()];
			
		}
		else
		{
			SAssignNew(Slate_LobbyContainer, SGuestContainer)
			.PlayerState(this);

			if (auto MyGameState = GetWorldGameState<AConstructionGameState>(GetWorld()))
			{
				auto SWindowGuestModeRef = SNew(SWindowGuestMode, Slate_LobbyContainer->GetWindowsContainer())
				.Data_UObject(MyGameState->GetConstructionComponent(), &UBuildingComponent::GetConstructionWorldData)
				.OnNextLocation_UObject(this, &AConstructionPlayerState::ProxyGuestEntry);

				Slate_LobbyContainer->GetWindowsContainer()->AddSlot()[SWindowGuestModeRef];
			}
		}

		SAssignNew(Slate_LobbyChat, SLobbyChat, Slate_LobbyContainer->GetWindowsContainer()).ChatComponent(ChatComponent).Tag(TEXT("Window.Chat"));
		Slate_LobbyContainer->GetWindowsContainer()->AddSlot()[Slate_LobbyChat.ToSharedRef()];

		TargetViewport->AddUsableViewportWidgetContent(Slate_LobbyContainer.ToSharedRef(), 100);
	}
#endif
}

void AConstructionPlayerState::OnOwnerAuthorization(const FGuid& InOwnerToken)
{
	if (auto MyGameState = GetWorldGameState<AConstructionGameState>(GetWorld()))
	{
		MyGameState->SetOwnerToken(this, InOwnerToken);
	}
}

void AConstructionPlayerState::OnInventoryUpdated(const TArray<UPlayerInventoryItem*> InData, const bool IsInternal)
{
	Super::OnInventoryUpdated(InData, IsInternal);

	if (Slate_Inventory.IsValid())
	{
		auto MyGameState = GetWorldGameState<AConstructionGameState>(GetWorld());
		if (MyGameState)
		{
			Slate_Inventory->OnFillList(MyGameState->GetInventoryComponent()->FindList(ECategoryTypeId::Building));
		}

		auto MyCharacter = GetConstructionCharacter();
		auto MyBuildHelper = MyCharacter ? MyCharacter->GetBuildHelper() : nullptr;

		if (MyBuildHelper)
		{
			MyBuildHelper->StartLoadSavedInventory();
		}
	}
}

void AConstructionPlayerState::OnFullyLoadedData()
{
	Super::OnFullyLoadedData();
	ChatComponent->StartTimers();

	if (auto MyGameState = GetWorldGameState<AConstructionGameState>(GetWorld()))
	{
		MyGameState->SetOwnerToken(this, GetIdentityToken());
	}

	// Find and start uncompleted tutorial;
	bool bFoundTut = false;
	for (auto tut : GetPlayerInfo().Tutorials)
	{
		if (tut.TutorialComplete == false)
		{
			GetTutorialComponent()->SendRequestTutorialStart(tut.TutorialId);
			bFoundTut = true;
			break;
		}
	}
	
	if (bFoundTut == false)
	{
		GetTutorialComponent()->SendRequestTutorialStart(7);
	}	
}

void AConstructionPlayerState::OnUpdateChatChannels(const TArray<FChatChanelModel>& InChannels)
{
	
}

void AConstructionPlayerState::OnUpdateChatMessages(const TArray<FChatMessageReciveModel>& InMessages)
{
	
}

void AConstructionPlayerState::BeginPlay()
{
	Super::BeginPlay();
}

void AConstructionPlayerState::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	auto MyGameState = GetWorldGameState<AConstructionGameState>(GetWorld());
	if (MyGameState && MyGameState->GetConstructionComponent() && LastOwnedState != MyGameState->GetConstructionComponent()->IsPlayerOwnedWorld() && GetPlayerInfo().PlayerId.IsValid())
	{
		LastOwnedState = MyGameState->GetConstructionComponent()->IsPlayerOwnedWorld();

		if (HasLocalClientState())
		{
			OnCreateUserInterface();

			if (Slate_Inventory.IsValid())
			{
				Slate_Inventory->OnFillList(MyGameState->GetInventoryComponent()->FindList(ECategoryTypeId::Building));
			}
		}

		auto MyCharacter = Cast<AConstructionCharacter>(GetBaseCharacter());
		if (HasAuthority() && MyCharacter)
		{
			MyCharacter->UpdateBuildHelper();
		}
	}
}

AConstructionCharacter* AConstructionPlayerState::GetConstructionCharacter() const
{
	if (auto MyController = GetPlayerController<AConstructionPlayerController>())
	{
		return MyController->GetConstructionCharacter();
	}

	return nullptr;
}

void AConstructionPlayerState::ToggleConstructionEscape_Implementation()
{
	if (Slate_LobbyContainer.IsValid())
	{
		Slate_LobbyContainer->ToggleWidget(!Slate_LobbyContainer->IsInInteractiveMode());
	}
}

void AConstructionPlayerState::ToggleConstructionInventory_Implementation(const bool InToggle)
{
	if (Slate_Inventory.IsValid())
	{
		Slate_Inventory->ToggleWidget(InToggle);
	}
}

void AConstructionPlayerState::ToggleConstructionToolBar_Implementation(const bool InToggle)
{
	if (Slate_ToolBar.IsValid())
	{
		Slate_ToolBar->ToggleWidget(InToggle);
	}
}

void AConstructionPlayerState::ClientToggleBuyWaiting_Implementation(const int32 InModelId, const int32 InCount, const bool InToggle)
{
	if (InToggle)
	{
		QueueMessageBegin("BuyWaitingDialog")
			SMessageBox::Get()->SetHeaderText(NSLOCTEXT("BuildMode", "BuildMode.BuyRequestWaiting.Title", "Waiting"));
			SMessageBox::Get()->SetContent(NSLOCTEXT("BuildMode", "BuildMode.BuyRequestWaiting.Desc", "Please wait, we send request to buy your item to owner."));
			SMessageBox::Get()->SetButtonsText();
			SMessageBox::Get()->SetEnableClose(true);
			SMessageBox::Get()->ToggleWidget(true);
		QueueMessageEnd
	}
	else
	{
		SMessageBox::RemoveQueueMessage(TEXT("BuyWaitingDialog"));
	}
}


void AConstructionPlayerState::ClientShowBuyRequest(const UItemBaseEntity* item)
{
	if(auto i = GetValidObject(item))
	{
		ClientShowBuyRequest(i->GetModelInfo(), 0, FString());
	}
}

void AConstructionPlayerState::ClientShowBuyRequest(const FItemModelInfo& InItemInfo)
{
	ClientShowBuyRequest(InItemInfo, 0, FString());
}

bool AConstructionPlayerState::ProxyGuestEntry_Validate(const FGuid& InId) { return true; }
void AConstructionPlayerState::ProxyGuestEntry_Implementation(const FGuid& InId)
{
	auto MyGameState = GetWorldGameState<AConstructionGameState>(GetWorld());
	if (MyGameState && MyGameState->GetConstructionComponent())
	{
		MyGameState->GetConstructionComponent()->SendGetBuildingList(InId);
	}
}

void AConstructionPlayerState::OnSwitchPlayerFraction(const UPlayerFractionEntity* fraction)
{
	ProgressComponent->OnSwitchFractionRequest(fraction);
}

void AConstructionPlayerState::ClientShowBuyRequest_Implementation(const FItemModelInfo& InItemInfo, const int32 InCount, const FString& InFromName)
{
	auto MyGameState = GetWorldGameState<AConstructionGameState>(GetWorld());
	if (MyGameState && MyGameState->GetInventoryComponent())
	{
		auto FoundItem = MyGameState->GetInventoryComponent()->FindItem(InItemInfo);
		if (FoundItem)
		{
			auto TargetItem = FoundItem->GetEntity();
			if (TargetItem && Slate_BuyBuilding.IsValid())
			{
				if (TargetItem->GetCost().IsEnough(IdentityComponent->GetPlayerInfo().Cash))
				{
					Slate_BuyBuilding->ToggleSellMode(false);
					Slate_BuyBuilding->SetCurrentItem(FoundItem);
					Slate_BuyBuilding->SetCurrentItemAmount((InCount > 0) ? InCount : Slate_BuyBuilding->GetCurrentItemAmount());

					SMessageBox::AddQueueMessage(TEXT("OnBuildItemBuyRequest"), FOnClickedOutside::CreateLambda([&, ii = TargetItem, fromn = InFromName]() {
						SMessageBox::Get()->SetHeaderText((fromn.Len() > 1) ? FText::Format(NSLOCTEXT("BuildMode", "BuildMode.BuyRequestFrom.Title", "Request buy from {0}"), FRichHelpers::TextHelper_NotifyValue.SetValue(fromn).ToText()) : NSLOCTEXT("BuildMode", "BuildMode.BuyRequest.Title", "Buy"));
						SMessageBox::Get()->SetContent(Slate_BuyBuilding.ToSharedRef());
						SMessageBox::Get()->SetButtonsText(LOCTABLE("BaseStrings", "All.Yes"), LOCTABLE("BaseStrings", "All.No"));
						SMessageBox::Get()->OnMessageBoxButton.AddLambda([&, i = ii](const EMessageBoxButton& InButton) {
							if (InButton == EMessageBoxButton::Left)
							{
								auto MyGameState = GetWorldGameState<AConstructionGameState>(GetWorld());
								if (MyGameState)
								{
									MarketComponent->UnlockItem(i, Slate_BuyBuilding->GetCurrentItemAmount(), MyGameState->GetOwnerPlayerId());
								}
							}
							SMessageBox::Get()->ToggleWidget(false);
						});
						SMessageBox::Get()->SetEnableClose(true);
						SMessageBox::Get()->ToggleWidget(true);
					}));

				}
				else
				{
					if (SquadComponent->GetSqaudInformation().Status != ESearchSessionStatus::None)
					{
						QueueMessageBegin("NoMoneyMaybeFight.Alt")
							SMessageBox::Get()->SetHeaderText(NSLOCTEXT("BuildMode", "BuildMode.NoMoney.Alt.Title", "Warning"));
							SMessageBox::Get()->SetContent(NSLOCTEXT("BuildMode", "BuildMode.NoMoney.Alt.Desc", "Search of battle is on progress, if you need cancel, cancel this manually!"));
							SMessageBox::Get()->SetButtonsText();
							SMessageBox::Get()->SetEnableClose(true);
							SMessageBox::Get()->ToggleWidget(true);
						QueueMessageEnd
					}
					else
					{
						QueueMessageBegin("NoMoneyMaybeFight", TargetItem = TargetItem)
							SMessageBox::Get()->SetHeaderText(NSLOCTEXT("BuildMode", "BuildMode.NoMoney.Title", "Not Enough"));
							SMessageBox::Get()->SetContent(FText::Format(NSLOCTEXT("BuildMode", "BuildMode.NoMoney.Desc", "{Resource} not enough to buy, maybe let's go into fight?"), TargetItem->GetCost().ToResourse()));
							SMessageBox::Get()->SetButtonsText(LOCTABLE("BaseStrings", "All.Yes"), LOCTABLE("BaseStrings", "All.No"));
							SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton& InButton) {
								if (InButton == EMessageBoxButton::Left && SquadComponent)
								{
									FSessionMatchOptions Options;
									Options.GameMode = EGameMode::AllFight.ToFlag();

									SquadComponent->SendRequestToggleSearch(Options);
								}
								SMessageBox::Get()->ToggleWidget(false);
							});
							SMessageBox::Get()->SetEnableClose(true);
							SMessageBox::Get()->ToggleWidget(true);
						QueueMessageEnd
					}
				}
			}
		}
	}
}

void AConstructionPlayerState::ClientShowSellRequest_Implementation(UPlayerInventoryItem* InItem, const int32 InCount)
{
	auto MyGameState = GetWorldGameState<AConstructionGameState>(GetWorld());
	if (InItem && MyGameState && MyGameState->GetOwnerState() == this)
	{
		Slate_BuyBuilding->ToggleSellMode(true);
		Slate_BuyBuilding->SetCurrentItem(InItem);
		Slate_BuyBuilding->SetCurrentItemAmount((InCount > 0) ? InCount : Slate_BuyBuilding->GetCurrentItemAmount());

		QueueMessageBegin("SellBuildItem", ii = InItem)
			SMessageBox::Get()->SetHeaderText(NSLOCTEXT("BuildMode", "BuildMode.SellRequest.Title", "Sell"));
			SMessageBox::Get()->SetContent(Slate_BuyBuilding.ToSharedRef());
			SMessageBox::Get()->SetButtonsText(LOCTABLE("BaseStrings", "All.Yes"), LOCTABLE("BaseStrings", "All.No"));
			SMessageBox::Get()->OnMessageBoxButton.AddLambda([&, i = ii](const EMessageBoxButton& InButton) {
				if (InButton == EMessageBoxButton::Left)
				{
					FSellItemRequest SellItemRequest;
					SellItemRequest.ItemId = i->GetEntityId().ToString();
					SellItemRequest.Amount = Slate_BuyBuilding->GetCurrentItemAmount();
					MarketComponent->SendRequestSellItem(SellItemRequest);
				}
				SMessageBox::Get()->ToggleWidget(false);
			});
			SMessageBox::Get()->SetEnableClose(true);
			SMessageBox::Get()->ToggleWidget(true);
		QueueMessageEnd
	}
	else
	{
		FNotifyInfo NotifyInfo(NSLOCTEXT("Build", "Build.Sell.NotOwner.Desc", "You are not owner and don't allow sell any item."), NSLOCTEXT("Build", "Build.Sell.NotOwner.Title", "Sell"));
		SNotifyList::Get()->AddNotify(NotifyInfo);
	}
}

void AConstructionPlayerState::ClientPasteItemIntoSlot_Implementation(const int32& InSlot, UPlayerInventoryItem* InItem)
{
	if (Slate_ToolBar.IsValid())
	{
		Slate_ToolBar->SetSlotItem(InSlot, InItem);
	}
}

void AConstructionPlayerState::ChangeAllowFlags(const int32 InFlags, const FGuid& InSender)
{
	if (auto MyCtrl = Cast<AConstructionPlayerController>(GetWorld()->GetFirstPlayerController()))
	{
		if (auto MyLocalState = MyCtrl->GetPlayerState())
		{
			MyLocalState->ServerChangeAllowFlags(this, InFlags, InSender);
		}
	}	
}


bool AConstructionPlayerState::ServerChangeAllowFlags_Validate(AConstructionPlayerState* InTo, const int32 InFlags, const FGuid& InSender) { return true; }
void AConstructionPlayerState::ServerChangeAllowFlags_Implementation(AConstructionPlayerState* InTo, const int32 InFlags, const FGuid& InSender)
{
	if (auto MyGameState = GetWorldGameState<AConstructionGameState>(GetWorld()))
	{
		if (InTo && MyGameState->GetOwnerPlayerId() == InSender)
		{
			InTo->AllowFlags = InFlags;
		}
	}
}


const FPlayerEntityInfo& AConstructionPlayerState::GetPlayerInfo()
{
	return IdentityComponent->GetPlayerInfo();
}

const FPlayerEntityInfo& AConstructionPlayerState::GetPlayerInfo() const
{
	return IdentityComponent->GetPlayerInfo();
}

void AConstructionPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AConstructionPlayerState, AllowFlags);
}