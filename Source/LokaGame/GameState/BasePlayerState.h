// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerState.h"
#include "Interfaces/LokaNetUserInterface.h"
#include "BasePlayerState.generated.h"

class UTutorialComponent;
class UFriendComponent;
class UPlayerInventoryItem;
class UProgressComponent;
class UProfileComponent;
class UInventoryComponent;
class UIdentityComponent;
class USquadComponent;

class ABasePlayerController;
class ABaseCharacter;

DECLARE_MULTICAST_DELEGATE(FOnAuthorizationComplete);

/**
 * 
 */
UCLASS()
class LOKAGAME_API ABasePlayerState 
	: public APlayerState
	, public ILokaNetUserInterface
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleInstanceOnly)	FGuid MemberId;


public:
	ABasePlayerState();

	virtual void PostInitializeComponents() override;
	virtual void ClientInitialize(class AController* C) override;

	FOnAuthorizationComplete OnAuthorizationCompleteDelegate;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	FORCEINLINE UIdentityComponent* GetIdentityComponent() const { return IdentityComponent; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	FORCEINLINE UInventoryComponent* GetInventoryComponent() const { return InventoryComponent; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	FORCEINLINE UProfileComponent* GetProfileComponent() const { return ProfileComponent; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	FORCEINLINE UProgressComponent* GetProgressComponent() const { return ProgressComponent; }
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	FORCEINLINE USquadComponent* GetSquadComponent() const { return SquadComponent; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	FORCEINLINE UFriendComponent* GetFriendComponent() const { return FriendComponent; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	FORCEINLINE UTutorialComponent* GetTutorialComponent() const { return TutorialComponent; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Runtime)
	bool HasLocalClientState() const;

	template<class T = ABasePlayerController>
	T* GetPlayerController() const
	{
		return GetValidObject<T, AActor>(GetOwner());
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Character)
	ABaseCharacter* GetBaseCharacter() const;

	virtual void InitializeSelectedProfile(const bool IsForce = false);

	UFUNCTION(BlueprintCallable, Category = Character)
	void ChangeProfile(const int32 InProfile);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerChangeProfile(const int32 InProfile);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Character)
	FORCEINLINE int32 GetCurrentProfile() const { return CachedSelectedProfile; }

protected:

	UPROPERTY(Transient)	UIdentityComponent*		IdentityComponent;
	UPROPERTY(Transient)	UInventoryComponent*	InventoryComponent;
	UPROPERTY(Transient)	UProfileComponent*		ProfileComponent;
	UPROPERTY(Transient)	UProgressComponent*		ProgressComponent;
	UPROPERTY(Transient)	USquadComponent*		SquadComponent;
	UPROPERTY(Transient)	UFriendComponent*		FriendComponent;
	UPROPERTY(Transient)	UTutorialComponent*		TutorialComponent;

	virtual void OnReactivated() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UPROPERTY()
	FGuid IdentityToken;

public:
	void CopyProperties(APlayerState* PlayerState) override;

	bool SetIdentityTokenString(const FString& InTokenString) override;
	void SetIdentityToken(const FGuid& InToken) override;
	FGuid GetIdentityToken() const override { return IdentityToken; }	
	bool IsAllowUserInventory() const override { return bIsAllowUserInventory; }

	UFUNCTION()
	virtual void OnOwnerAuthorization(const FGuid& InOwnerToken);

	UFUNCTION()
	virtual void OnInventoryUpdated(const TArray<UPlayerInventoryItem*> InData, const bool IsInternal = true);

	UFUNCTION()
	virtual void OnFullyLoadedData();

	UFUNCTION()
	virtual void OnCreateUserInterface();

	UFUNCTION()
	void OnRep_SelectedProfile();

	UPROPERTY(ReplicatedUsing=OnRep_SelectedProfile)
	int32 SelectedProfile;

	UPROPERTY()
	int32 CachedSelectedProfile;

	virtual void BeginPlay() override;

	UPROPERTY()
	bool bIsAllowUserInventory;
};
