// Copyright 1998-2013 Epic Games, Inc. All Rights Reserved.
#pragma once

#include "Types/TypeCash.h"

#include "BasePlayerState.h"

#include "Interfaces/UTTeamInterface.h"
#include "UTDamageType.h"

#include "UTCharacterVoice.h"
#include "StatManager.h"
#include "GameState/ShooterPlayerStatistics.h"

#include "UTGameVolume.h"

#include "Service/ItemServiceModelId.h"

#include "UTPlayerState.generated.h"

struct FProfileHack;
USTRUCT(BlueprintType)
struct FWeaponSpree
{
	GENERATED_USTRUCT_BODY()

	FWeaponSpree() : SpreeSoundName(NAME_None), Kills(0) {};

	FWeaponSpree(FName InSpreeSoundName) : SpreeSoundName(InSpreeSoundName), Kills(0) {};

	UPROPERTY()
	FName SpreeSoundName;

	UPROPERTY()
	int32 Kills;
};

struct FTempBanInfo
{
	// The person who voted for this ban
	TSharedPtr<const FUniqueNetId> Voter;

	// When was the vote cast.  Votes will time out over time (5mins)
	float BanTime;

	FTempBanInfo(const TSharedPtr<const FUniqueNetId> inVoter, float inBanTime)
		: Voter(inVoter)
		, BanTime(inBanTime)
	{
	}
};


USTRUCT()
struct FCoolFactorHistoricalEvent
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	float CoolFactorAmount;

	UPROPERTY()
	float TimeOccurred;

	FCoolFactorHistoricalEvent()
		: CoolFactorAmount(0)
		, TimeOccurred(0)
	{
	}
};



enum class FractionTypeId : uint8;


UCLASS()
class LOKAGAME_API AUTPlayerState : public ABasePlayerState, public IUTTeamInterface
{
	friend class AUTGameMode;

	GENERATED_BODY()

public:

	AUTPlayerState();

	//======================================================================
	//								LOKA 
	UPROPERTY(VisibleInstanceOnly, Transient)				bool IsABot;
	UPROPERTY(VisibleInstanceOnly)							FGuid TeamId;
	UPROPERTY(VisibleInstanceOnly, Transient, Replicated)	TArray<FTypeCash> Cash;
	UPROPERTY(VisibleInstanceOnly, Transient, Replicated)	TArray<FTypeCash> PremiumCash;
	UPROPERTY(VisibleInstanceOnly, Transient, Replicated)	int32 Experience;
	UPROPERTY(VisibleInstanceOnly, Transient, Replicated)	int32 PremiumExperience;
	UPROPERTY(VisibleInstanceOnly, Transient, Replicated)	int32 Reputation;
	UPROPERTY(VisibleInstanceOnly, Transient, Replicated)	int32 PremiumReputation;
	UPROPERTY(VisibleInstanceOnly, Transient, Replicated)	TEnumAsByte<UsingPremiumServiceFlag::Type> UsingPremiumService;
	UPROPERTY(VisibleInstanceOnly, Transient, Replicated)	FractionTypeId FractionId;
	UPROPERTY(VisibleInstanceOnly, Transient, Replicated)	TArray<TEnumAsByte<ItemAchievementTypeId::Type>> Achievements;
	UPROPERTY(VisibleInstanceOnly, Transient, Replicated)	float PlayerCoefficient;
	UPROPERTY(VisibleInstanceOnly, Transient)				int64 NextSpawnAttempTime;
	UPROPERTY(VisibleInstanceOnly, Transient)				int32 SpawnAttempts;

	template<bool IsPremium>
	FTypeCash* InternalTakeCash(const EGameCurrency::Type& InCurrency)
	{
		FTypeCash* localCash = nullptr;

		for (auto &v : IsPremium ? PremiumCash : Cash)
		{
			if (v.Currency == InCurrency)
			{
				localCash = &v;
				break;
			}
		}

		if (localCash == nullptr)
		{
			auto &TargetArray = (IsPremium ? PremiumCash : Cash);
			TargetArray.Add(FTypeCash(InCurrency, 0));
			localCash = &TargetArray.Last();
		}

		return localCash;
	}

	FTypeCash* TakeCash(const EGameCurrency::Type& InCurrency)
	{
		return InternalTakeCash<false>(InCurrency);
	}

	FTypeCash* TakePremiumCash(const EGameCurrency::Type& InCurrency)
	{
		return InternalTakeCash<true>(InCurrency);
	}

	void GiveCash(const EGameCurrency::Type& currency, const int64& amount)
	{
		TakeCash(currency)->Amount += amount;
	}

	bool IsRealPlayer() const
	{
		return IsABot == false && bIsDemoRecording == false && MemberId.IsValid();
	}

	bool IsActiveRealPlayer() const
	{
		return IsRealPlayer() && IsQuitter() == false;
	}

	UFUNCTION(BlueprintCallable, Category = Gameplay)
	void AddAchievement(const ItemAchievementTypeId::Type AchievementId);

	UFUNCTION(BlueprintCallable, Category = Statistics)
	bool AddDamageData(const int32& Index, const FPlayerDamageData& Data);

	TMap<int32, FPlayerDamageData>& GetDamageData();

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = Statistics)
	FPlayerDamageData& GetDamageDataLast();

	/** Get accuracy percent [0-1], by surface */
	UFUNCTION(BlueprintPure, BlueprintCallable, Category = Statistics)
	float GetAccuracyBySurface(const uint8& Surface);

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = Statistics)
	float GetAccuracyByPawn(const bool IsFriendly);

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = Statistics)
	float GetAccuracyAnyPawn();

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = Statistics)
	int32 GetHitsByPawn(const bool IsFriendly) const;

	UFUNCTION(BlueprintCallable, Category = Statistics)
	void AddShootData(const FPlayerShootData& Data);

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = Statistics)
	TArray<FPlayerShootData> GetShootData() const
	{
		return ShootData;
	}

	FORCEINLINE int32 GetKills() const
	{
		return Kills;
	}

	FORCEINLINE int32 GetDeaths() const
	{
		return Deaths;
	}

	FORCEINLINE float GetScore() const
	{
		return Score;
	}

	FORCEINLINE bool IsQuitter() const
	{
		return bQuitter;
	}


	/** Server only */
	UFUNCTION(BlueprintPure, BlueprintCallable, Category = Statistics)
	int32 GetKillsOneLife() const
	{
		int32 _value = 0;
		for (auto& i : KillsDataLast)
		{
			_value += i.Value;
		}

		return _value;
	}

	void AddExtraScore(const int32& InExtraScore)
	{
		ExtraScore += InExtraScore;
	}

	bool ApplyExtraScore()
	{
		if (ExtraScore > 0)
		{
			Score += float(ExtraScore);
			return true;
		}

		return false;
	}

	FORCEINLINE void SetCoefficient(const float c) { PlayerCoefficient = c; }

	UFUNCTION(BlueprintCallable, Category = Statistics)
	FORCEINLINE float GetCoefficient() const { return PlayerCoefficient; }

	/** helper for scoring points */
	UFUNCTION(BlueprintCallable, Category = Gameplay)
	void ScorePoints(int32 Points);

	/** player killed someone */
	UFUNCTION(BlueprintCallable, Category = Gameplay)
	void ScoreKill(AUTPlayerState* Victim, int32 Points);

	/** player died */
	UFUNCTION(BlueprintCallable, Category = Gameplay)
	void ScoreDeath(AUTPlayerState* KilledBy, int32 Points);

	void TrySendTauntMessage();

protected:

	// Damage data before die (used to detect all killers)
	UPROPERTY(Transient)
	TMap<int32, FPlayerDamageData> DamageData;

	// Last damage data (used to detect what kill me and by weapon type)
	UPROPERTY(Transient)
	FPlayerDamageData DamageDataLast;

	// Total shoots on session
	UPROPERTY(Transient)
	TArray<FPlayerShootData> ShootData;

	// Total series kills on session (only server, TMultiMap not supported as UPROPERTY)
	TMultiMap<FWeaponHelper, int32> KillsData;

	// Series kills before die (only server, FWeaponHelper not supported as TMap key on UPROPERTY)
	TMap<FWeaponHelper, int32> KillsDataLast;

	UPROPERTY(Transient)
	int32 ExtraScore;
	//======================================================================

private:
	/** Instance of a stat manager that tracks gameplay/reward stats for this Player Controller */
	UPROPERTY()
	class UStatManager *StatManager;

protected:
	/** selected character (if NULL left at default) */
	UPROPERTY(BlueprintReadOnly, ReplicatedUsing = NotifyTeamChanged, Category = PlayerState)
	TSubclassOf<class AUTCharacterContent> SelectedCharacter;

	/** Used to optimize GetUTCharacter(). */
	UPROPERTY()
	AUTCharacter* CachedCharacter;

public:
	UFUNCTION(BlueprintCallable, Category = Character)
	virtual void SetCharacter(const FString& CharacterPath);

	UFUNCTION(BlueprintCallable, Category = Character)
		virtual void SetCharacterVoice(const FString& CharacterVoicePath);

	UFUNCTION(BlueprintCallable, Reliable, Server, WithValidation, Category = Character)
	void ServerSetCharacter(const FString& CharacterPath);
	inline TSubclassOf<class AUTCharacterContent> GetSelectedCharacter() const
	{
		return SelectedCharacter;
	}

	/** Don't do engine style ping updating. */
	virtual void UpdatePing(float InPing) override;

	/** Called on client using the roundtrip time for servermove/ack. */
	virtual void CalculatePing(float NewPing);

	/** ID that can be used to consistently identify this player for spectating commands
	 * IDs are reused when players leave and new ones join, but a given player's ID remains stable and unique
	 * as long as that player is in the game
	 * NOTE: 1-based, zero is unassigned (bOnlySpectator spectators don't get a value)
	 */
	UPROPERTY(BlueprintReadOnly, Replicated, Category = PlayerState)
	uint8 SpectatingID;
	/** version of SpectatingID that is unique only amongst this player's team instead of all players in the game */
	UPROPERTY(BlueprintReadOnly, Replicated, Category = PlayerState)
	uint8 SpectatingIDTeam;

	/** player's team if we're playing a team game */
	UPROPERTY(BlueprintReadOnly, ReplicatedUsing = NotifyTeamChanged, Category = PlayerState)
	class AUTTeamInfo* Team;

	/** Whether this player has confirmed ready to play */
	UPROPERTY(BlueprintReadWrite, replicated, Category = PlayerState)
	uint32 bReadyToPlay:1;

	UPROPERTY(BlueprintReadOnly, Replicated, Category = PlayerState)
		uint32 bIsWarmingUp : 1;

	virtual void SetReadyToPlay(bool bNewReadyState);

	UPROPERTY(BlueprintReadWrite, replicated, Category = PlayerState)
		uint8 ReadyMode;

	/** Whether this spectator is a caster */
	UPROPERTY(replicated)
	uint32 bCaster : 1;

	/** Persistent so deathmessage can know about it. */
	UPROPERTY()
		uint32 bAnnounceWeaponSpree : 1;

	/** Persistent so deathmessage can know about it. */
	UPROPERTY()
		uint32 bAnnounceWeaponReward : 1;

	/** Last displayed ready state. */
	uint8 LastReadyState;

	/** Last Ready state change time. */
	float LastReadySwitchTime;

	/** Count of fast ready state changes. */
	int32 ReadySwitchCount;

	/** Voice used by this player/bot for speech (taunts, etc.). */
	UPROPERTY(BlueprintReadOnly, Category = Sounds)
		TSubclassOf<class UUTCharacterVoice> CharacterVoice;

	UPROPERTY()
		bool bShouldAutoTaunt;

	/** Last time this player sent a taunt voice message. */
	UPROPERTY()
		float LastTauntTime;

	/** Last time this player sent a need health message. */
	UPROPERTY()
		float LastNeedHealthTime;

	/** Last time this player received a behind you message. */
	UPROPERTY()
		float LastBehindYouTime;

	FTimerHandle PlayKillAnnouncement;
	FTimerHandle UpdateOldNameHandle;

	/** Delay oldname update so it's available for name change notification. */
	virtual void UpdateOldName();

	/** Whether this player plays auto-taunts. */
	virtual bool ShouldAutoTaunt() const;

	virtual void AnnounceKill();

	virtual void AnnounceSameTeam(class AUTPlayerController* ShooterPC);

	virtual void AnnounceReactionTo(const AUTPlayerState* ReactionPS) const;

	virtual void AnnounceStatus(FName NewStatus, int32 Switch=0, bool bSkipSelf=false);

	virtual bool IsOwnedByReplayController() const;

	/** Used for tracking multikills - not always correct as it is reset when player dies. */
	UPROPERTY(BlueprintReadWrite, Category = PlayerState)
	float LastKillTime;

	/** current multikill level (1 = double, 2 = multi, etc)
	 * note that the value isn't reset until a new kill comes in
	 */
	UPROPERTY(BlueprintReadWrite, Category = PlayerState)
	int32 MultiKillLevel;

	/** Current number of consecutive kills without dying. */
	UPROPERTY(Replicated, BlueprintReadWrite, Category = PlayerState)
	int32 Spree;

	/** Kills by this player.  */
	UPROPERTY(BlueprintReadWrite, replicated, Category = PlayerState)
	int32 Kills;

	/** Damage done by this player.  Not replicated. */
	UPROPERTY(BlueprintReadWrite, Category = PlayerState)
		int32 DamageDone;

	/** Damage done by this player this round.  Not replicated. */
	UPROPERTY(BlueprintReadWrite, Category = PlayerState)
		int32 RoundDamageDone;

	/** Enemy kills by this player this round. */
	UPROPERTY(BlueprintReadWrite, replicated, Category = PlayerState)
		int32 RoundKills;

	/** If limited lives, remaining lives for this player. */
	/** Enemy kills by this player this round.  Not replicated. */
	UPROPERTY(BlueprintReadWrite, replicated, Category = PlayerState)
		int32 RemainingLives;

	virtual void IncrementDamageDone(int32 AddedDamage);

	/** Max of this and game respawn time is min respawntime */
	UPROPERTY(ReplicatedUsing = OnRespawnWaitReceived)
		float RespawnWaitTime;

	UFUNCTION()
		void OnRespawnWaitReceived();

	/** whether the user quit the match */
	UPROPERTY()
		uint8 bQuitter : 1;

	/** How many times associated player has died */
	UPROPERTY(BlueprintReadOnly, replicated, ReplicatedUsing = OnDeathsReceived, Category = PlayerState)
	int32 Deaths;

	/** How many times has the player captured the flag */
	UPROPERTY(BlueprintReadWrite, replicated, Category = PlayerState)
	int32 FlagCaptures;

	/** How many times has the player returned the flag */
	UPROPERTY(BlueprintReadWrite, replicated, Category = PlayerState)
	int32 FlagReturns;

	/** How many times has the player captured the flag */
	UPROPERTY(BlueprintReadWrite, replicated, Category = PlayerState)
	int32 Assists;

	UPROPERTY(BlueprintReadOnly, replicated, Category = PlayerState)
	AUTPlayerState* LastKillerPlayerState;


	UPROPERTY(BlueprintReadOnly, Category = PlayerState, replicated)
	bool bIsRconAdmin;

	UPROPERTY(BlueprintReadOnly, replicated, Category = PlayerState)
	bool bIsDemoRecording;

	/** Whether this player currently has a limited number of lives. */
	UPROPERTY(BlueprintReadOnly, replicated, Category = PlayerState)
		bool bHasLifeLimit;

	UPROPERTY()
	bool bAllowedEarlyLeave;

	// Player Stats 

	/** This is the unique ID for stats generation*/
	UPROPERTY(replicated)
	FString StatsID;
	
	/** Add an entry to MatchHighlights only if an empty slot is found. */
	virtual void AddMatchHighlight(FName NewHighlight, float HighlightData);

	/** Set at end of match and half-time. */
	UPROPERTY(replicated)
		FName MatchHighlights[5];

	/** Set at end of match and half-time. */
	UPROPERTY(replicated)
		float MatchHighlightData[5];

	UPROPERTY()
	int32 ElapsedTime;
public:
	/** Currently awarded challenge stars. */
	UPROPERTY(replicated, BlueprintReadOnly, Category = "Game")
	int32 TotalChallengeStars;


	// How long until this player can respawn.  It's not directly replicated to the clients instead it's set
	// locally via OnDeathsReceived.  It will be set to the value of "GameState.RespawnWaitTime"

	UPROPERTY(BlueprintReadWrite, Category = PlayerState)
	float RespawnTime;

	UPROPERTY(BlueprintReadWrite, Category = PlayerState)
	float ForceRespawnTime;

	/** used for gametypes where players make a choice in order (e.g. Showdown spawn selection) to indicate selection sequence */
	UPROPERTY(Replicated, BlueprintReadOnly)
	uint8 SelectionOrder;

	/** The currently held object */
	UPROPERTY(BlueprintReadOnly, replicated, ReplicatedUsing = OnCarriedObjectChanged, Category = PlayerState)
	class AUTCarriedObject* CarriedObject;

	/** Last time this player shot the enemy flag carrier. */
	UPROPERTY(BlueprintReadOnly, Category = PlayerState)
	float LastShotFCTime;

	/** Enemy flag carrier who was last shot. */
	UPROPERTY(BlueprintReadOnly, Category = PlayerState)
	AUTPlayerState* LastShotFC;

	/** Last time this player killed the enemy flag carrier. */
	UPROPERTY(BlueprintReadOnly, Category = PlayerState)
	float LastKilledFCTime;

	/** Accumulate partial damage on enemy flag carrier for scoring. */
	UPROPERTY(BlueprintReadOnly, Category = PlayerState)
	float FCDamageAccum;

	/** Last time this player returned the flag. */
	UPROPERTY(BlueprintReadOnly, Category = PlayerState)
	float LastFlagReturnTime;

	/** Transient - used for triggering assist announcements after a kill. */
	UPROPERTY(BlueprintReadOnly, Category = PlayerState)
	bool bNeedsAssistAnnouncement;

	/** Set once final analytic event has been sent (to make sure it isn't sent more than once. */
	UPROPERTY(BlueprintReadOnly, Category = PlayerState)
		bool bSentLogoutAnalytics;

	UPROPERTY(BlueprintReadOnly, Category = PlayerState)
	TArray<FWeaponSpree> WeaponSprees;

	UPROPERTY(BlueprintReadOnly, replicated, ReplicatedUsing = OnWeaponSpreeDamage, Category = PlayerState)
	TSubclassOf<UUTDamageType> WeaponSpreeDamage;

	UFUNCTION()
	void OnWeaponSpreeDamage();

	virtual void AnnounceWeaponSpree(TSubclassOf<class UUTDamageType> UTDamage);

	UFUNCTION()
	void OnDeathsReceived();


	/** Team has changed, announce, tell pawn, etc. */
	UFUNCTION()
	virtual void HandleTeamChanged(AController* Controller);

	UFUNCTION(BlueprintNativeEvent)
	void NotifyTeamChanged();

	UFUNCTION()
	void OnCarriedObjectChanged();

	UFUNCTION()
	virtual void SetCarriedObject(AUTCarriedObject* NewCarriedObject);

	UFUNCTION()
	virtual void ClearCarriedObject(AUTCarriedObject* OldCarriedObject);

	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category = PlayerState)
	virtual void IncrementKills(TSubclassOf<UDamageType> DamageType, bool bEnemyKill, AUTPlayerState* VictimPS=NULL);

	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category = PlayerState)
	virtual void IncrementDeaths(TSubclassOf<UDamageType> DamageType, AUTPlayerState* KillerPlayerState);

	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category = PlayerState)
	virtual void AdjustScore(int32 ScoreAdjustment);

	virtual void Tick(float DeltaTime) override;

	/** Return true if character is female. */
	virtual bool IsFemale();

	UFUNCTION(Server, Reliable, WithValidation)
	virtual void ServerRequestChangeTeam(uint8 NewTeamIndex);

	UFUNCTION(BlueprintCallable, Category = Pawn)
	AUTCharacter* GetUTCharacter();

	virtual void CopyProperties(APlayerState* PlayerState) override;
	virtual void OverrideWith(APlayerState* PlayerState) override;

	// Returns the team number of the team that owns this object
	UFUNCTION()
	virtual uint8 GetTeamNum() const;
	// not applicable
	virtual void SetTeamForSideSwap_Implementation(uint8 NewTeamNum) override
	{}

	virtual bool ShouldBroadCastWelcomeMessage(bool bExiting = false) override;

	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type Reason) override;

	virtual	bool ModifyStat(FName StatName, int32 Amount, EStatMod::Type ModType);

	// Where should any chat go.  NOTE: some commands like Say and TeamSay ignore this value
	UPROPERTY(replicated)
	FName ChatDestination;

	UFUNCTION(server, reliable, withvalidation)
	virtual void ServerNextChatDestination();

	virtual void Destroyed() override;

protected:
	/*  Used to determine whether boost can be triggered. */
	UPROPERTY(Replicated, BlueprintReadOnly, Category = PlayerState)
	uint8 RemainingBoosts;
public:
	inline uint8 GetRemainingBoosts() const
	{
		return RemainingBoosts;
	}
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category = PlayerState)
	void SetRemainingBoosts(uint8 NewRemainingBoosts);

	/** if gametype supports a boost recharge/cooldown timer, the time that needs to pass for RemainingBoosts to be incremented */
	UPROPERTY(Replicated, BlueprintReadWrite, Category = PlayerState)
	float BoostRechargeTimeRemaining;

	UPROPERTY()
	TArray<class AUTInventory*> PreservedKeepOnDeathInventoryList;
	
	/** Inventory item that is created on boost. */
	UPROPERTY(Replicated, BlueprintReadWrite, Category = PlayerState)
		TSubclassOf<class AUTInventory> BoostClass;

	/** Last scoreboard position, used by minimap. */
	UPROPERTY()
		FVector ScoreCorner;
public:

	/** Cached clamped player name for display. */
	UPROPERTY(BlueprintReadWrite)
	FString ClampedName;

	/** True if clamped name is currently valid. */
	bool bHasValidClampedName;

	/** object which set clamping for my name. */
	UPROPERTY(BlueprintReadWrite)
	UObject* NameClamper;

	virtual void SetPlayerName(const FString& S) override;

	virtual void OnRep_PlayerName();
	
private:
	/** map of additional stats used for scoring display. */
	TMap< FName, float > StatsData;

public:
	/** Last time StatsData was updated - used when replicating the data. */
	UPROPERTY()
		float LastScoreStatsUpdateTime;

	/** Accessors for StatsData. */
	UFUNCTION(BlueprintCallable, Category = PlayerState)
	float GetStatsValue(FName StatsName) const;

	void SetStatsValue(FName StatsName, float NewValue);
	void ModifyStatsValue(FName StatsName, float Change);


	UFUNCTION(BlueprintCallable, Category = Badge)
	bool IsABeginner(AUTGameMode* DefaultGameMode) const;

	// transient, for TeamInfo leader updates - persistent value is stored in StatsData
	float AttackerScore;
	float SecondaryAttackerScore;
	float DefenderScore;
	float SupporterScore;

	virtual void RegisterPlayerWithSession(bool bWasFromInvite) override;
	virtual void UnregisterPlayerWithSession() override;

	// Calculated client-side by the local player when 
	UPROPERTY(BlueprintReadOnly, Category = "Game")
	bool bIsFriend;

	UPROPERTY()
	float CoolFactorCombinationWindow;

	UPROPERTY()
	float CoolFactorBleedSpeed;

	UPROPERTY()
	float MinimumConsiderationForCoolFactorHistory;

	UPROPERTY(replicated)
	float CurrentCoolFactor;

	UPROPERTY()
	TArray<FCoolFactorHistoricalEvent> CoolFactorHistory;

	UFUNCTION()
	void AddCoolFactorEvent(float CoolFactorAddition);

	UFUNCTION()
	void AddCoolFactorMinorEvent();
	public:

	// If true, the game type considers this player special.
	UPROPERTY(replicatedUsing = OnRepSpecialPlayer)
	uint32 bSpecialPlayer:1;

	// If true, the game type considers this player special for a given team
	UPROPERTY(replicatedUsing = OnRepSpecialTeamPlayer)
	uint32 bSpecialTeamPlayer:1;

	UFUNCTION()
	virtual void OnRepSpecialPlayer();

	UFUNCTION()
	virtual void OnRepSpecialTeamPlayer();

	virtual void UpdateSpecialTacComFor(AUTCharacter* Character, AUTPlayerController* UTPC);

public:

	UPROPERTY(Replicated)
	uint8 KickCount;

	virtual void OnRep_bIsInactive() override;

	
	/** Transient, used to sort players */
	UPROPERTY()
		float MatchHighlightScore;

	UFUNCTION(Client, Reliable)
	virtual void ClientReceiveRconMessage(const FString& Message);

	/** hook for blueprints */
	UFUNCTION(BlueprintCallable, Category = PlayerState)
	bool IsOnlySpectator() const
	{
		return bOnlySpectator;
	}

	UFUNCTION(Server, Reliable, WithValidation)
	virtual void ServerSetBoostItem(int PowerupIndex);

	// DO NOT USE: This is WIP temp code and may go away.
	UPROPERTY(Replicated)
	AActor* CriticalObject;

	UPROPERTY(BlueprintReadOnly, Category = "Game")
	bool bIsTalking;

public:
	/** Holds the last known location of the pawn associated with this pri */
	UPROPERTY(BlueprintReadOnly, Category = PlayerState)
	AUTGameVolume* LastKnownLocation;

	TSubclassOf<UUTCharacterVoice> GetCharacterVoiceClass();

	TMap< UClass*, int32> DamageDelt;

	virtual void SetUniqueId(const TSharedPtr<const FUniqueNetId>& InUniqueId) override;

public:
	void InitializeProfiles(const TArray<FProfileHack>& InProfiles);

};

