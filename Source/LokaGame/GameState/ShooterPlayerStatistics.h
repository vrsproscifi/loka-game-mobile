// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#pragma once
#include "Achievement/ItemAchievementTypeId.h"
#include "ShooterPlayerStatistics.generated.h"

class UItemBaseEntity;
class AShooterPlayerState;

USTRUCT(BlueprintType)
struct FWeaponHelper
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	uint8 Category;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	uint8 Model;

	FWeaponHelper() : Category(0), Model(0) {}
	FWeaponHelper(const uint8& InCategory, const uint8& InModel) : Category(InCategory), Model(InModel) {}
	FWeaponHelper(const UItemBaseEntity* Item);

	// TMap key interface begin
	friend uint32 GetTypeHash(const FWeaponHelper& Weapon)
	{
		return HashCombine(GetTypeHash(Weapon.Category), GetTypeHash(Weapon.Model));
	}

	friend bool operator==(const FWeaponHelper& Source, const FWeaponHelper& Other)
	{
		return (Source.Category == Other.Category && Source.Model == Other.Model);
	}
	// TMap key interface end

	static TMap<FWeaponHelper, ItemAchievementTypeId::Type> KillsAchievementsList;
	static TMap<FWeaponHelper, ItemAchievementTypeId::Type> SeriesKillAchievementsList;
};

USTRUCT(BlueprintType)
struct FPlayerDamageData
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FWeaponHelper Weapon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Damage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	APlayerState* Owner;

	FPlayerDamageData& operator+=(const FPlayerDamageData& Other)
	{
		this->Weapon = Other.Weapon;
		this->Damage += Other.Damage;
		this->Owner = Other.Owner;

		return *this;
	}
};

USTRUCT(BlueprintType)
struct FPlayerShootData
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FWeaponHelper Weapon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	uint8 Surface;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool IsPawn_Friendly;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool IsPawn_Enemy;

	FPlayerShootData() : Weapon(), Surface(0), IsPawn_Friendly(false), IsPawn_Enemy(false) {}
};

