#pragma once

//=======================================================
//							[ Headers ]
#include "Item/ItemModelInfo.h"
#include "BasePlayerState.h"
#include "ConstructionPlayerState.generated.h"

class SRadialMenu;
struct FChatMessageReciveModel;
struct FChatChanelModel;
class SLobbyChat;
class UChatComponent;
class SMatchMakingInformationWidget;
class SSquadInformationWidget;
class UItemBaseEntity;
class SWindowBuyBuilding;
class SBuildSystem_Inventory;
class SBuildSystem_ToolBar;
class UMarketComponent;
class UPlayerFractionEntity;
//=======================================================
//							[ Forward declaration ]
class AConstructionPlayerController;

class UIdentityComponent;
class UProgressComponent;
class UBuildingComponent;
class UInventoryComponent;
class UProfileComponent;
struct FPlayerEntityInfo;

class SBaseContainer;

namespace EAllowConstructionMode
{
	enum Type
	{
		None = 1,
		Selecting = 2,
		Building = 4,
		End
	};
}

UCLASS()
class AConstructionPlayerState : public ABasePlayerState
{
	friend class AConstructionGameState;

	GENERATED_BODY()

public:
	//=======================================================
	//							[ Methods ]
	AConstructionPlayerState();
	virtual void OnCreateUserInterface() override;
	virtual void BeginPlay() override;		
	virtual void Tick(float DeltaSeconds) override;

	//=======================================================
	//							[ Handlers ]

public:

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	FORCEINLINE UMarketComponent* GetMarketComponent() const { return MarketComponent; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	FORCEINLINE UChatComponent* GetChatComponent() const { return ChatComponent; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Character)
	AConstructionCharacter* GetConstructionCharacter() const;

	UFUNCTION(Client, Reliable)
	void ToggleConstructionEscape();

	UFUNCTION(Client, Reliable)							
	void ToggleConstructionInventory(const bool InToggle);

	UFUNCTION(Client, Reliable)							
	void ToggleConstructionToolBar(const bool InToggle);

	UFUNCTION(Client, Reliable)
	void ClientToggleBuyWaiting(const int32 InModelId, const int32 InCount, const bool InToggle = true);

	
	void ClientShowBuyRequest(const UItemBaseEntity* item);

	void ClientShowBuyRequest(const FItemModelInfo& InItemInfo);

	UFUNCTION(Client, Reliable)
	void ClientShowBuyRequest(const FItemModelInfo& InItemInfo, const int32 InCount, const FString& InFromName = FString());

	UFUNCTION(Client, Reliable)
	void ClientShowSellRequest(UPlayerInventoryItem* InItem, const int32 InCount);

	UFUNCTION(Client, Reliable)
	void ClientPasteItemIntoSlot(const int32& InSlot, UPlayerInventoryItem* InItem);

	void ChangeAllowFlags(const int32 InFlags, const FGuid& InSender);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerChangeAllowFlags(AConstructionPlayerState* InTo, const int32 InFlags, const FGuid& InSender);

	UFUNCTION(Server, Reliable, WithValidation)
	void ProxyGuestEntry(const FGuid& InId);

public:
	void OnSwitchPlayerFraction(const UPlayerFractionEntity* fraction);

public:
	const FPlayerEntityInfo& GetPlayerInfo();
	const FPlayerEntityInfo& GetPlayerInfo() const;

protected:

	TSharedPtr<SBuildSystem_ToolBar>		Slate_ToolBar;
	TSharedPtr<SBuildSystem_Inventory>		Slate_Inventory;
	TSharedPtr<SWindowBuyBuilding>			Slate_BuyBuilding;
	TSharedPtr<SBaseContainer>				Slate_LobbyContainer;
	TSharedPtr<SLobbyChat>					Slate_LobbyChat;

	UPROPERTY(Transient)	UMarketComponent*		MarketComponent;
	UPROPERTY(Transient)	UChatComponent*			ChatComponent;
	UPROPERTY(Transient)	bool					LastOwnedState;

public:
	UPROPERTY(Replicated)	int32					AllowFlags;

protected:
	virtual void OnOwnerAuthorization(const FGuid& InOwnerToken) override;
	virtual void OnInventoryUpdated(const TArray<UPlayerInventoryItem*> InData, const bool IsInternal = true) override;
	virtual void OnFullyLoadedData() override;

	void OnUpdateChatChannels(const TArray<FChatChanelModel>& InChannels);
	void OnUpdateChatMessages(const TArray<FChatMessageReciveModel>& InMessages);
};