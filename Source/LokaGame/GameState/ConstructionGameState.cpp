#include "LokaGame.h"
#include "ConstructionGameState.h"
#include "ConstructionPlayerState.h"
#include "ConstructionComponent.h"
#include "IdentityComponent.h"
#include "InventoryComponent.h"

AConstructionGameState::AConstructionGameState()
{
	ConstructionComponent = CreateDefaultSubobject<UConstructionComponent>(TEXT("ConstructionComponent"));
	ConstructionComponent->SetBuildingOwnerComponent(this);
	ConstructionComponent->SetIsReplicated(true);

	InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComponent"));
	InventoryComponent->SetIsReplicated(true);

	InventoryComponent->OnInventoryUpdate.AddUObject(this, &AConstructionGameState::OnInventoryUpdate);
}

UConstructionComponent* AConstructionGameState::GetConstructionComponent() const
{
	return GetValidObject(ConstructionComponent);
}

UInventoryComponent* AConstructionGameState::GetInventoryComponent() const
{
	return InventoryComponent;
}

bool AConstructionGameState::SetOwnerToken(AConstructionPlayerState* InFrom, const FGuid& InToken)
{
	if (InFrom == OwnerState && HasAuthority())
	{
		OwnerToken = InToken;
		OwnerPlayerId = OwnerState->GetPlayerInfo().PlayerId;
		InventoryComponent->SendRequestLootData();
		return true;
	}

	return false;
}

AConstructionPlayerState* AConstructionGameState::GetOwnerState() const
{
	return GetValidObject(OwnerState);
}

FGuid AConstructionGameState::GetOwnerPlayerId() const
{
	return OwnerPlayerId;
}

bool AConstructionGameState::SetIdentityTokenString(const FString& InTokenString)
{
	FGuid TargetGuid;
	return FGuid::Parse(InTokenString, OwnerToken);
}

void AConstructionGameState::SetIdentityToken(const FGuid& InToken)
{
	OwnerToken = InToken;
}

void AConstructionGameState::OnInventoryUpdate(const TArray<UPlayerInventoryItem*> InData)
{
	if (HasAuthority())
	{
		GetConstructionComponent()->SendGetBuildingList(OwnerPlayerId);
	}

	if (GetNetMode() != NM_DedicatedServer)
	{
		for (auto TargetPlayer : PlayerArray)
		{
			if (auto MyPlayer = Cast<AConstructionPlayerState>(TargetPlayer))
			{
				if (MyPlayer->HasLocalClientState())
				{
					MyPlayer->OnInventoryUpdated(InData, false);
				}
			}
		}
	}
}

void AConstructionGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AConstructionGameState, OwnerState);
	DOREPLIFETIME(AConstructionGameState, OwnerPlayerId);
}
