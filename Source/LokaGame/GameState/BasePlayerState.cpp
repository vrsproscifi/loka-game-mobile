// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "BasePlayerState.h"
#include "IdentityComponent.h"
#include "InventoryComponent.h"
#include "ProfileComponent.h"
#include "ProgressComponent.h"
#include "BasePlayerController.h"
#include "SquadComponent.h"
#include "BaseCharacter.h"
#include "UTGameViewportClient.h"
#include "FriendComponent.h"
#include "SFriends_Commands.h"
#include "GameSingleton.h"
#include "TutorialComponent.h"

ABasePlayerState::ABasePlayerState()
	: Super()
{
	//========================
	IdentityComponent = CreateDefaultSubobject<UIdentityComponent>(TEXT("IdentityComponent"));
	IdentityComponent->SetIsReplicated(true);

	InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComponent"));
	InventoryComponent->SetIsReplicated(true);

	ProfileComponent = CreateDefaultSubobject<UProfileComponent>(TEXT("ProfileComponent"));
	ProfileComponent->SetIsReplicated(true);

	ProgressComponent = CreateDefaultSubobject<UProgressComponent>(TEXT("ProgressComponent"));
	ProgressComponent->SetIsReplicated(true);

	SquadComponent = CreateDefaultSubobject<USquadComponent>(TEXT("SquadComponent"));
	SquadComponent->SetIsReplicated(true);

	FriendComponent = CreateDefaultSubobject<UFriendComponent>(TEXT("FriendComponent"));
	FriendComponent->SetIsReplicated(true);

	TutorialComponent = CreateDefaultSubobject<UTutorialComponent>(TEXT("TutorialComponent"));
	TutorialComponent->SetIsReplicated(true);


	IdentityComponent->OnRequestClientDataDelegate.AddDynamic(ProgressComponent, &UProgressComponent::OnRequestClientDataSuccessfully);

//	IdentityComponent->OnRequestLootDataSuccessfully.BindUObject(InventoryComponent, &UInventoryComponent::OnLoadPlayerInventorySuccessfully);
	IdentityComponent->OnRequestLootDataFailed.BindUObject(InventoryComponent, &UInventoryComponent::OnLoadPlayerInventoryFailed);	

	//========================
	if (FApp::IsGame())
	{
		InventoryComponent->OnInventoryUpdate.AddUObject(this, &ABasePlayerState::OnInventoryUpdated, true);
		IdentityComponent->OnAuthorizationEvent.AddDynamic(this, &ABasePlayerState::OnOwnerAuthorization);
		ProfileComponent->OnProfileDataUpdated.AddDynamic(this, &ABasePlayerState::OnFullyLoadedData);
	}

	SelectedProfile = EPlayerPreSetId::One;
	bIsAllowUserInventory = true;

	bReplicates = true;
}

void ABasePlayerState::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (HasLocalClientState())
	{
		OnCreateUserInterface();
	}
}

void ABasePlayerState::ClientInitialize(AController* C)
{
	Super::ClientInitialize(C);

	if (HasLocalClientState())
	{
		OnCreateUserInterface();
	}
}

void ABasePlayerState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABasePlayerState, SelectedProfile);
}

void ABasePlayerState::BeginPlay()
{
	Super::BeginPlay();
}

bool ABasePlayerState::HasLocalClientState() const
{
	if (GetPlayerController() && GetPlayerController()->IsLocalController())
	{
		if (GetNetMode() == NM_Client || GetNetMode() == NM_ListenServer || GetNetMode() == NM_Standalone)
		{
			return true;
		}
	}

	return false;
}

ABaseCharacter* ABasePlayerState::GetBaseCharacter() const
{
	if (const auto MyController = Cast<AController>(GetOwner()))
	{
		return Cast<ABaseCharacter>(MyController->GetCharacter());
	}

	return nullptr;
}

void ABasePlayerState::OnReactivated()
{
	Super::OnReactivated();

//#if UE_SERVER
//	GetIdentityComponent()->SendRequestExist();
//#endif
}

void ABasePlayerState::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	auto MyViewport = GetGameViewportClient();
	if (HasLocalClientState() && MyViewport)
	{
		MyViewport->RemoveAllViewportWidgetsAlt();
	}

	Super::EndPlay(EndPlayReason);
}

void ABasePlayerState::CopyProperties(APlayerState* PlayerState)
{
	Super::CopyProperties(PlayerState);
	
	ABasePlayerState* PS = Cast<ABasePlayerState>(PlayerState);
	if(PS)
	{
		PS->MemberId = MemberId;
	}
}

bool ABasePlayerState::SetIdentityTokenString(const FString& InTokenString)
{
	FGuid localTargetToken;
	if (FGuid::Parse(InTokenString, localTargetToken))
	{
		SetIdentityToken(localTargetToken);
		return true;
	}

	return false;
}

void ABasePlayerState::SetIdentityToken(const FGuid& InToken)
{
	IdentityToken = InToken;

	if (HasLocalClientState())
	{
		UGameSingleton::Get()->LastLocalToken = IdentityToken;
		UE_LOG(LogEntityPlayer, Log, TEXT("ABasePlayerState::SetIdentityToken >> LastLocalToken = %s"), *UGameSingleton::Get()->LastLocalToken.ToString());
	}
}

void ABasePlayerState::OnOwnerAuthorization(const FGuid& InOwnerToken)
{

}

void ABasePlayerState::OnInventoryUpdated(const TArray<UPlayerInventoryItem*> InData, const bool IsInternal)
{
	if (HasAuthority() && ProfileComponent->GetProfileDataPtr(EPlayerPreSetId::One) == nullptr && IsInternal)
	{		
		ProfileComponent->SendRequestProfilesData();
	}
}

void ABasePlayerState::ChangeProfile(const int32 InProfile)
{
	auto TargetProfile = ProfileComponent->GetProfileData(static_cast<EPlayerPreSetId::Type>(InProfile));
	if (TargetProfile.IsEnabled && TargetProfile.Items.Num())
	{
		SelectedProfile = InProfile;
		CachedSelectedProfile = InProfile;
	}

	if (!HasAuthority())
	{
		ServerChangeProfile(InProfile);
	}
}

bool ABasePlayerState::ServerChangeProfile_Validate(const int32 InProfile) { return true; }
void ABasePlayerState::ServerChangeProfile_Implementation(const int32 InProfile)
{
	ChangeProfile(InProfile);
}

void ABasePlayerState::InitializeSelectedProfile(const bool IsForce)
{
	if (auto MyCharacter = GetBaseCharacter())
	{
		if (MyCharacter->GetInstance() && IsForce == false) return;

		auto localTargetProfile = ProfileComponent->GetProfileData(static_cast<EPlayerPreSetId::Type>(SelectedProfile));
		auto localFoundCharacter = InventoryComponent->FindItem(localTargetProfile.CharacterId);

		MyCharacter->InitializeInstance(localFoundCharacter);

		for (auto Item : localTargetProfile.Items)
		{
			if (auto localFoundItem = InventoryComponent->FindItem(Item.ItemId))
			{
				MyCharacter->InitializeItem(localFoundItem);
			}
		}
	}
}

void ABasePlayerState::OnFullyLoadedData()
{
	InitializeSelectedProfile();
}

void ABasePlayerState::OnCreateUserInterface()
{
#if !UE_SERVER
	if (HasLocalClientState())
	{
		SFriendsCommands::GetAlt().SetPlayerState(this);
	}
#endif
}

void ABasePlayerState::OnRep_SelectedProfile()
{
	CachedSelectedProfile = SelectedProfile;
}
