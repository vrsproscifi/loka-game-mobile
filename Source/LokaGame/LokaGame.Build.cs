// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.IO;

public class LokaGame : ModuleRules
{
	static bool bIsGameVersionPatched = false;

	public LokaGame(ReadOnlyTargetRules Target) : base(Target)
	{
        //==================================================
        //UEBuildConfiguration
        //bUseRTTI = true;
	    OptimizeCode = CodeOptimization.InShippingBuildsOnly;
        MinFilesUsingPrecompiledHeaderOverride = 3;
        bFasterWithoutUnity = true;
        //==================================================
        //  PublicDependencyModuleNames
        PublicDependencyModuleNames.AddRange(new string[] 
        {
            "Core", 
            "CoreUObject", 
            "Engine", 
            "InputCore",
            "AssetRegistry",
            "AIModule",
            "OnlineSubsystem",
            "OnlineSubsystemUtils",
            "HTTP",
            "Json",
			"JsonUtilities",
			"ImageDownload",
            "ApexDestruction",

        });

        //==================================================
        //  PrivateDependencyModuleNames
        PrivateDependencyModuleNames.AddRange(new string[] 
        {
			"InputCore",
			"Slate",
			"SlateCore",
            "MoviePlayer",
            //"SlateReflector",
            "GameplayTasks",
            "XmlParser",
            //"LokaGameLoading",
        });

        //==================================================


        PrivateIncludePaths.AddRange(
            new string[] { 
				"LokaGame/Classes/Player",
				"LokaGame/Private",
				"LokaGame/Service",
				"LokaGame/Service/Request",
				"LokaGame/Service/Corporation",
				"LokaGame/Service/Identity",
				"LokaGame/Service/Session",
				"LokaGame/Service/Store",
				"LokaGame/Service/Tournament",
				"LokaGame/Private/UI",
				"LokaGame/Private/UI/Menu",
				"LokaGame/Private/UI/Style",
				"LokaGame/Private/UI/Widgets",
                "LokaGame/Slate",
                "LokaGame/Slate/Auth",
                "LokaGame/Slate/Character",
                "LokaGame/Slate/Components",
                "LokaGame/Slate/Fractions",
                "LokaGame/Slate/Friends",
                "LokaGame/Slate/League",
                "LokaGame/Slate/Lobby",
                "LokaGame/Slate/Notify",
                "LokaGame/Slate/Settings",
                "LokaGame/Slate/Tournament",
                "LokaGame/Slate/Weapon",
                "LokaGame/ServiceController",
                "LokaGame/ServiceController/IdentityController",
                "LokaGame/ServiceController/LeagueController",
                "LokaGame/ServiceController/LobbyController",
                "LokaGame/ServiceController/StoreController",
                "LokaGame/ServiceController/TournamentController",
				"LokaGame/ServiceController/ServerController",
                "LokaGame/Entity",		
                "LokaGame/GamePlay",

                "LokaGame/GameMode/AbstractGameMode",
                "LokaGame/GameMode/CaptureTheFlag",
                "LokaGame/GameMode/CaptureThePoint",
                "LokaGame/GameMode/DeadMatch",
				"LokaGame/GameMode/DefenseTheFlag",

                "LokaGame/GameMode/Duel",
                "LokaGame/GameMode/Gauntlet",
                "LokaGame/GameMode/LastHeroeMatch",
                "LokaGame/GameMode/Mincer",
                "LokaGame/GameMode/ProtectorDevice",
                "LokaGame/GameMode/SearchArtifact",
                "LokaGame/GameMode/ShowdownGame",
                "LokaGame/GameMode/TeamDeadMatch",
				
				"LokaGame/UnrealTournament/AIAction",
				"LokaGame/UnrealTournament/DamageType",
				"LokaGame/UnrealTournament/Pickup",
				"LokaGame/UnrealTournament/Projectile",
				"LokaGame/UnrealTournament/ReachSpec",
				
				"LokaGame/Components",
				"LokaGame/Containers"
            }
        );
		
		PublicIncludePaths.AddRange(
            new string[] {
                "LokaGame/UnrealTournament",
                "LokaGame/UnrealTournament/Gauntlet",
                "LokaGame/UnrealTournament/Online",
                "LokaGame/UnrealTournament/Slate",

																
																				
                "LokaGame/GameMode/AbstractGameMode",
                "LokaGame/GameMode/CaptureTheFlag",
                "LokaGame/GameMode/CaptureThePoint",
                "LokaGame/GameMode/DeadMatch",
				"LokaGame/GameMode/DefenseTheFlag",

                "LokaGame/GameMode/Duel",
                "LokaGame/GameMode/Gauntlet",
                "LokaGame/GameMode/LastHeroeMatch",
                "LokaGame/GameMode/Mincer",
                "LokaGame/GameMode/ProtectorDevice",
                "LokaGame/GameMode/SearchArtifact",
                "LokaGame/GameMode/ShowdownGame",
                "LokaGame/GameMode/TeamDeadMatch",
				
				"LokaGame/UnrealTournament/AIAction",
				"LokaGame/UnrealTournament/DamageType",
				"LokaGame/UnrealTournament/Pickup",
				"LokaGame/UnrealTournament/Projectile",
				"LokaGame/UnrealTournament/ReachSpec",
																
                "LokaGame/Public",
                "LokaGame/Interface",
                "LokaGame/Slate",
				"LokaGame/Entity",
                "LokaGame/Entity/Types",
				"LokaGame/Entity/Interface",
                "LokaGame/Service",
                "LokaGame/ServiceController",
                "LokaGame/PlayerController",
                "LokaGame/PlayerCharacter",
                "LokaGame/GameState",
                "LokaGame/GameMode",
                "LokaGame/GameInstance",
                "LokaGame/GamePlay",
				"LokaGame/UI",
            }
        );

		

        DynamicallyLoadedModuleNames.AddRange(
            new string[] {
				"OnlineSubsystemNull",
                "OnlineSubsystemSteam",
                "NetworkReplayStreaming",
				"NullNetworkReplayStreaming",
				"HttpNetworkReplayStreaming",
            }
        );

        PrivateIncludePathModuleNames.AddRange(
            new string[] {
				"NetworkReplayStreaming"
			}
        );
		
		PrivateIncludePaths.AddRange(new string[] {
			"LokaGame/UnrealTournament/Slate",	
			"LokaGame/UnrealTournament/Slate/Base",	
			"LokaGame/UnrealTournament/Slate/Dialogs",	
			"LokaGame/UnrealTournament/Slate/Menus",	
			"LokaGame/UnrealTournament/Slate/Panels",	
			"LokaGame/UnrealTournament/Slate/Toasts",	
			"LokaGame/UnrealTournament/Slate/Widgets",	
			"LokaGame/UnrealTournament/Slate/UIWindows",	
		});

        
        PublicDependencyModuleNames.AddRange(new string[] { 
                                                    "GameplayTasks",
                                                    "AIModule", 
                                                    "OnlineSubsystem", 
                                                    "OnlineSubsystemUtils", 
                                                    "RenderCore", 
                                                    "Navmesh", 
                                                    "Json", 
													"JsonUtilities",
                                                    "HTTP", 
				                                    "Party",
				                                    "Lobby",
                                                    //"BlueprintContext",
                                                    "EngineSettings", 
			                                        "Landscape",
                                                    "Foliage",
													"PerfCounters",
                                                    "PakFile",
                                                    });

        PrivateDependencyModuleNames.AddRange(new string[] {"FriendsAndChat", "Sockets", "Analytics", "AnalyticsET" });

        if (Target.Type != TargetRules.TargetType.Server)
        {
            PublicDependencyModuleNames.AddRange(new string[] { "AppFramework", "RHI", "SlateRHIRenderer", "MoviePlayer" });
        }

        if (Target.Type == TargetRules.TargetType.Editor)
        {
            PublicDependencyModuleNames.AddRange(new string[] { "UnrealEd", "Matinee", "PropertyEditor", "ShaderCore", "LokaGameEditor" });

			PrivateIncludePaths.AddRange(
				new string[] {
					"LokaGame/EditorInstance"
				}
			);

			GenerateBuildNumber();
		}

        PublicDefinitions.Add("WITH_PROFILE=0");
	}
	
	bool IsLicenseeBuild()
    {
        return !Directory.Exists("Runtime/NotForLicensees");
    }

	private string ModulePath
	{
		get { return ModuleDirectory; }
	}

	private string TargetVersionPath
	{
		get { return Path.GetFullPath(Path.Combine(ModulePath, "../../Source/LokaGame/")); }
	}

	bool GenerateBuildNumber()
	{
		bool bIsSuccess = false;

		if (TargetVersionPath.Length > 10)
		{
			string PROJECT_PATH = TargetVersionPath;
			string GAME_VERSION_FILE_O = Path.Combine(PROJECT_PATH, "GameVersion.h");
			string GAME_VERSION_FILE_N = Path.Combine(PROJECT_PATH, "GameVersion_tmp.h");

			System.Console.WriteLine("Target game version file: " + GAME_VERSION_FILE_O);

			if (File.Exists(GAME_VERSION_FILE_O) && bIsGameVersionPatched == false)
			{
				System.Console.WriteLine("Target game version file: Begin patching");

				File.Delete(GAME_VERSION_FILE_N);

				StreamWriter wsVersionFile = new StreamWriter(File.Create(GAME_VERSION_FILE_N));

				foreach (string strLine in File.ReadAllLines(GAME_VERSION_FILE_O))
				{
					if (strLine.Contains("#define GAME_BUILD_NUMBER"))
					{
						int startNumber = "#define GAME_BUILD_NUMBER ".Length;
						int buildNumber = int.Parse(strLine.Remove(0, startNumber)) + 1;

						wsVersionFile.WriteLine("#define GAME_BUILD_NUMBER " + buildNumber.ToString());
						System.Console.WriteLine("New build number generated: " + buildNumber.ToString());

						bIsSuccess = true;
					}
					else
					{
						wsVersionFile.WriteLine(strLine);
					}
				}

				if (bIsSuccess)
				{
					wsVersionFile.Close();

					File.Delete(GAME_VERSION_FILE_N + ".tmp");
					File.Replace(GAME_VERSION_FILE_N, GAME_VERSION_FILE_O, GAME_VERSION_FILE_N + ".tmp");

					bIsGameVersionPatched = true;

					System.Console.WriteLine("Target game version file: Updated");
				}
			}
		}

		return bIsSuccess;
	}
}
