#pragma once
#include "SlateBasics.h"
#include "SlateExtras.h"
#include <SlateGameResources.h>

//============================================================================
#define SlateStylizedWidget(TStyle, Name) const TStyle* Style = &FSlateStyleManager::Get().GetGameWidgetStyle<TStyle>(Name);


//============================================================================
#define SLATE_THEME_DECLARATION() SLATE_ARGUMENT(ESlateThemeName::Type, Theme)
#define SLATE_THEME_CONSTRUCT() ThemeName = InArgs._Theme;\
if(ThemeName < 0) ThemeName = static_cast<ESlateThemeName::Type>(0); \
if(ThemeName > 1) ThemeName = static_cast<ESlateThemeName::Type>(1); \
Theme = &Style->Theme[ThemeName];


//============================================================================
class FSlateStyleGameSet : public FSlateGameResources
{
public:
	explicit FSlateStyleGameSet(const FName& InStyleSetName)
		: FSlateGameResources(InStyleSetName)
	{

	}

	template< typename WidgetStyleType >
	const WidgetStyleType& GetGameWidgetStyle(FName PropertyName, const ANSICHAR* Specifier = nullptr) const
	{

		const FSlateWidgetStyle* ExistingStyle = GetWidgetStyleInternal(WidgetStyleType::TypeName, Join(PropertyName, Specifier));

		if (ExistingStyle == nullptr)
		{
			return WidgetStyleType::template GetDefault<WidgetStyleType>();
		}

		return *static_cast< const WidgetStyleType* >(ExistingStyle);
	}

	static TSharedRef<FSlateStyleGameSet> New(const FName& InStyleSetName, const FString& ScopeToDirectory, const FString& InBasePath = FString());

};

class FSlateStyleManager
{
public:
	static void Initialize();
	static void Shutdown();

	static void ReloadTextures();
	static const FSlateStyleGameSet& Get();
	static FName GetStyleSetName();
private:

	static TSharedRef<class FSlateStyleGameSet> Create();
	static TSharedPtr<class FSlateStyleGameSet> StyleInstance;
};