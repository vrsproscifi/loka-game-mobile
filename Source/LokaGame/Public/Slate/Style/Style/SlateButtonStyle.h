#pragma once

#include "Margin.h"
#include "SlateButtonStyle.generated.h"

USTRUCT()
struct FSlateButtonStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly)	FButtonStyle ButtonStyle;

	UPROPERTY(EditDefaultsOnly)	FTextBlockStyle TextStyle;

	UPROPERTY(EditDefaultsOnly)	FMargin Padding;
	

	FSlateButtonStyle(){}
	FSlateButtonStyle(const FMargin& padding)
		: Padding(padding)
	{
		
	}
};