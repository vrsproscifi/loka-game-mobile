#pragma once
#include <OnlineJsonSerializer.h>

template<typename T>
struct FRequestOneParam : public FOnlineJsonSerializable
{
	virtual ~FRequestOneParam() { }

	T Value;
	FRequestOneParam() : Value() { }
	FRequestOneParam(const T& val) : Value(val) { }

	operator T&() { return Value; }
	operator T() const { return Value; }


	BEGIN_ONLINE_JSON_SERIALIZER
		ONLINE_JSON_SERIALIZE("Value", Value);
	END_ONLINE_JSON_SERIALIZER
};
