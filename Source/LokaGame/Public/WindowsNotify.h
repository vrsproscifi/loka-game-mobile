#pragma once

struct FWindowsNotify
{
	static bool Notify(const FText& title, const FText& message);
	static bool Flash(bool isStart);
};