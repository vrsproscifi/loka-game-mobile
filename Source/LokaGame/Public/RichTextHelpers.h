// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once

#include "SResourceTextBoxType.h"

struct FResourceImageSize
{
	static const FResourceImageSize Default;

	FResourceImageSize(const FVector2D& imageSize, const float& fontSize)
		: Image(imageSize)
		, Font(fontSize)
	{

	}

	FText ToText() const
	{
		return FText::FromString(ToString());
	}

	FString ToString() const
	{
		return FString::Printf(TEXT("%f, %f, %f"), Image[0], Image[1], Font);
	}

	FVector2D Image;
	float Font;
};

struct FRichHelpers
{
	struct FTextHelper
	{
		FTextHelper& SetFont(const FString& InFont)
		{
			FString& fValue = Parameters.FindOrAdd("font");
			fValue = InFont;
			return *this;
		}

		FTextHelper& SetColor(const FLinearColor& InColor)
		{
			FString& fValue = Parameters.FindOrAdd("color");
			fValue = InColor.ToString();
			return *this;
		}

		FTextHelper& SetColor(const FColor& InColor)
		{
			FString& fValue = Parameters.FindOrAdd("color");
			fValue = InColor.ReinterpretAsLinear().ToString();
			return *this;
		}

		FTextHelper& SetColor(const FString& InColor)
		{
			FString& fValue = Parameters.FindOrAdd("color");
			fValue = InColor;
			return *this;
		}

		FTextHelper& SetSize(const int32& InSize)
		{
			FString& fValue = Parameters.FindOrAdd("size");
			fValue = FString::FromInt(InSize);
			return *this;
		}

		FTextHelper& SetValue(const FString& InValue)
		{
			Value = InValue;
			return *this;
		}

		FTextHelper& SetValue(const FText& InValue)
		{
			Value = InValue.ToString();
			return *this;
		}

		FString ToString() const
		{
			FString OutString("<text ");

			for (auto i : Parameters)
			{
				OutString.Append(i.Key.ToString() + "=\"" + i.Value + "\" ");
			}

			OutString.RemoveFromEnd(" ");
			OutString.Append(">");
			OutString.Append(Value);
			OutString.Append("</>");

			return OutString;
		}

		FText ToText() const
		{
			return FText::FromString(ToString());
		}

	private:
		TMap<FName, FString> Parameters;
		FString Value;
	};

	static FTextHelper TextHelper_NotifyValue;

	static const FString Money;
	static const FString Donate;
	static const FString Coin;
	static const FString Time;
	static const FString Level;

	struct FResourceHelper
	{
		FResourceHelper() : Value(0), ValueType(Money), Size(FResourceImageSize::Default) {}

		template<typename NumericType = int32>
		FResourceHelper& SetValue(const NumericType& InValue)
		{
			static_assert(TIsArithmetic<NumericType>().Value,	"FResourceHelper support only 'Arithmetic' types!");
			
			Value = static_cast<int64>(InValue);
			return *this;
		}

		FResourceHelper& SetValue(const FString& InValue)
		{
			if (InValue.IsNumeric())
			{
				Value = FCString::Atoi64(*InValue);
			}

			return *this;
		}

		FResourceHelper& SetValue(const FText& InValue)
		{
			return SetValue(InValue.ToString());
		}

		FResourceHelper& SetType(const FString& InType)
		{
			ValueType = InType;
			return *this;
		}

		FResourceHelper& SetType(const EResourceTextBoxType::Type& InType);

		FResourceHelper& SetSize(const FVector& InSize)
		{
			Size.Image = FVector2D(InSize);
			Size.Font = InSize.Z;
			return *this;
		}

		// * Set only font size
		FResourceHelper& SetSize(const float& InSize)
		{
			Size.Font = InSize;
			return *this;
		}

		// * Set only image size
		FResourceHelper& SetSize(const FVector2D& InSize)
		{
			Size.Image = InSize;
			return *this;
		}

		FResourceHelper& SetSize(const FResourceImageSize& InSize)
		{
			Size = InSize;
			return *this;
		}

		FString ToString() const
		{
			return FString("<resource type=\"" + ValueType + "\" size=\"" + FVector(Size.Image.X, Size.Image.Y, Size.Font).ToString() + "\">" + FString::FromInt(Value) + "</>");
		}

		FText ToText() const
		{
			return FText::FromString(ToString());
		}

	private:
		int64 Value;
		FString ValueType;
		FResourceImageSize Size;
	};

	static FResourceHelper ResourceHelper_Money;
	static FResourceHelper ResourceHelper_Donate;

	struct FKeyHelper
	{
		FKeyHelper& SetBinding(const FString& InBinding)
		{
			Binding = InBinding;
			return *this;
		}

		FKeyHelper& SetBackground(const bool InUse)
		{
			bIsUseBackground = InUse;
			return *this;
		}

		FString ToString() const
		{
			return FString("<key background=\"" + FString(bIsUseBackground ? "True" : "False") + "\" binding=\"" + Binding + "\"></>");
		}

		FText ToText() const
		{
			return FText::FromString(ToString());
		}

	private:
		bool bIsUseBackground;
		FString Binding;
	};
};
