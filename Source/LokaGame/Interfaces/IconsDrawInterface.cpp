// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "IconsDrawInterface.h"


// This function does not need to be modified.
UIconsDrawInterface::UIconsDrawInterface(const class FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
}

FVector IIconsDrawInterface::GetMinimapIconLocation()
{
	if (auto ThisAsActor = Cast<AActor>(this))
	{
		UFunction* GetTeamNumFunc = ThisAsActor->GetClass()->FindFunctionByName(TEXT("K2_GetMinimapIconLocation"));
		if (GetTeamNumFunc != nullptr && GetTeamNumFunc->Script.Num() > 0)
		{
			return IIconsDrawInterface::Execute_K2_GetMinimapIconLocation(ThisAsActor);
		}
		else
		{
			return ThisAsActor->GetActorLocation();
		}
	}

	return FVector::ZeroVector;
}

FVector IIconsDrawInterface::GetRadarIconLocation()
{
	if (auto ThisAsActor = Cast<AActor>(this))
	{
		UFunction* GetTeamNumFunc = ThisAsActor->GetClass()->FindFunctionByName(TEXT("K2_GetRadarIconLocation"));
		if (GetTeamNumFunc != nullptr && GetTeamNumFunc->Script.Num() > 0)
		{
			return IIconsDrawInterface::Execute_K2_GetRadarIconLocation(ThisAsActor);
		}
		else
		{
			return ThisAsActor->GetActorLocation();
		}
	}

	return FVector::ZeroVector;
}

FVector IIconsDrawInterface::GetWorldIconLocation()
{
	if (auto ThisAsActor = Cast<AActor>(this))
	{
		UFunction* GetTeamNumFunc = ThisAsActor->GetClass()->FindFunctionByName(TEXT("K2_GetWorldIconLocation"));
		if (GetTeamNumFunc != nullptr && GetTeamNumFunc->Script.Num() > 0)
		{
			return IIconsDrawInterface::Execute_K2_GetWorldIconLocation(ThisAsActor);
		}
		else
		{
			return ThisAsActor->GetActorLocation();
		}
	}

	return FVector::ZeroVector;
}