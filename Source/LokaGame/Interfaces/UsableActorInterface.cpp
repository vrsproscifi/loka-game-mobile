// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "UsableActorInterface.h"


// This function does not need to be modified.
UUsableActorInterface::UUsableActorInterface(const class FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
}

// Add default functionality here for any IUsableActorInterface functions that are not pure virtual.
