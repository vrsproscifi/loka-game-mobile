// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "HealthBarInterface.h"


// This function does not need to be modified.
UHealthBarInterface::UHealthBarInterface(const class FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
}

// Add default functionality here for any IHealthBarInterface functions that are not pure virtual.
