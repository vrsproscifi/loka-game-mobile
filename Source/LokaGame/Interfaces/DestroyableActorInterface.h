// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "DestroyableActorInterface.generated.h"

UINTERFACE(MinimalAPI, Blueprintable)
class UDestroyableActorInterface : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class LOKAGAME_API IDestroyableActorInterface
{
	GENERATED_IINTERFACE_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Gameplay)
	float GetCurrentHealth() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Gameplay)
	FVector2D GetHealth() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Gameplay)
	float GetHealthPercent() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Gameplay)
	bool IsDying() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Gameplay)
	FText GetLocalizedName() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Gameplay)
	bool GetIsVisiblyUserInterface() const;
	
protected:

	UFUNCTION(BlueprintImplementableEvent, Category = Gameplay)
	void OnDeath();
};