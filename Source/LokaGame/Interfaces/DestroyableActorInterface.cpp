// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "DestroyableActorInterface.h"


// This function does not need to be modified.
UDestroyableActorInterface::UDestroyableActorInterface(const class FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
}

// Add default functionality here for any IDestroyableActorInterface functions that are not pure virtual.
