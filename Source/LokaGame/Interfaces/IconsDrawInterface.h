// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "IconsDrawInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI, Blueprintable)
class UIconsDrawInterface : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

/**
 * 
 */
class LOKAGAME_API IIconsDrawInterface
{
	GENERATED_IINTERFACE_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	UFUNCTION(BlueprintNativeEvent)
	void DrawMinimapIcon(AHUD* InOwnerHud, UCanvas* InCanvas, FVector2D InPosition, const float InScale);

	UFUNCTION(BlueprintNativeEvent)
	void DrawRadarIcon(AHUD* InOwnerHud, UCanvas* InCanvas, FVector2D InPosition, const float InScale);

	UFUNCTION(BlueprintNativeEvent)
	void DrawWorldIcon(AHUD* InOwnerHud, UCanvas* InCanvas, FVector2D InPosition, const float InScale);

	UFUNCTION()
	virtual FVector GetMinimapIconLocation();

	UFUNCTION()
	virtual FVector GetRadarIconLocation();

	UFUNCTION()
	virtual FVector GetWorldIconLocation();

	UFUNCTION(BlueprintImplementableEvent, meta = (DisplayName="Get Minimap Icon Location"))
	FVector K2_GetMinimapIconLocation() const;

	UFUNCTION(BlueprintImplementableEvent, meta = (DisplayName = "Get Radar Icon Location"))
	FVector K2_GetRadarIconLocation() const;

	UFUNCTION(BlueprintImplementableEvent, meta = (DisplayName = "Get World Icon Location"))
	FVector K2_GetWorldIconLocation() const;

	UFUNCTION()
	virtual bool IsValidAltWorldIconCheck(AHUD* InOwnerHud) const { return false; }

	UFUNCTION()
	virtual bool IsUseAlternativeRadarMethod() const { return true; }
};
