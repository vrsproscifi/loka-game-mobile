#pragma once
#include "GameModeTypeId.generated.h"

UENUM(BlueprintType, meta = (Depricated))
namespace EGameModeTypeId
{
	enum Type //: int32
	{
		None,
		TeamDeadMatch = 1,
		LostDeadMatch = 2,
		ResearchMatch = 4,
		DuelMatch = 8,
		Lobby = 16,
		Attack = 32,
		Building = 64,
		PVE_Waves = 128,
	};
}

UENUM(BlueprintType, Category = Runtime)
enum class EBlueprintGameMode : uint8
{
	None,
	TeamDeadMatch,
	LostDeadMatch,
	ResearchMatch,
	DuelMatch,
	Attack,
	PVE_Waves,
	Construction,
	End
};


//template<struct EBaseType, enum class ValueType>
//struct LOKAGAME_API FAdvancedEnumStructure
//{
//	FAdvancedEnumStructure() : Value() {}
//	FAdvancedEnumStructure(const int32 InValue) : Value(static_cast<ValueType>(InValue)) {}
//	FAdvancedEnumStructure(const ValueType InValue) : Value(InValue) {}
//
//private:
//
//	ValueType Value;
//};

struct LOKAGAME_API EGameMode
{
public:

	EGameMode();
	EGameMode(const int32 InValue);
	EGameMode(EBlueprintGameMode InValue);

	static const EGameMode None;
	static const EGameMode TeamDeadMatch;
	static const EGameMode LostDeadMatch;
	static const EGameMode ResearchMatch;
	static const EGameMode DuelMatch;
	static const EGameMode Attack;
	static const EGameMode PVE_Waves;
	static const EGameMode Construction;

	static const EGameMode AllFight;
	
	

	FText GetDisplayName() const;
	FText GetDescription() const;
	FString GetName() const;
	FString ToString() const;
	int32 ToFlag() const;
	bool HasAnyFlags(const int32 InValue) const;
	//int32 ToFlags() const;
	TArray<EGameMode> ToArray() const;

	bool operator==(const EGameMode& Other) const;
	bool operator!=(const EGameMode& Other) const;	
	EGameMode& operator=(int32 InValue);

	int32 Value;
};


struct FGameModeTypeId
{
	static FText GetText(const EGameModeTypeId::Type& Id);
	static FText GetTextDesc(const EGameModeTypeId::Type& Id);
	static FString GetString(const EGameModeTypeId::Type& Id);
};

UENUM(BlueprintType, Category = Runtime)
enum class EBlueprintGameMap : uint8
{
	None,
	PvP,
	Outpost,
	Hangar,
	MilitaryBase,
	Tropic,
	DesertTown,
	Lobby,
	End
};

struct LOKAGAME_API EGameMap
{
public:

	EGameMap();
	EGameMap(const int32 InValue);	
	EGameMap(EBlueprintGameMap InValue);

	static const EGameMap None;
	static const EGameMap PvP;
	static const EGameMap Outpost;
	static const EGameMap Hangar;
	static const EGameMap MilitaryBase;
	static const EGameMap Tropic;
	static const EGameMap DesertTown;
	static const EGameMap Lobby;
				 
	static const EGameMap AllFight;

	FText GetDisplayName() const;
	FText GetDescription() const;
	FString GetName() const;
	FString ToString() const;
	int32 ToFlag() const;
	TArray<EGameMap> ToArray() const;

	bool operator==(const EGameMap& Other) const;
	bool operator!=(const EGameMap& Other) const;
	EGameMap& operator=(int32 InValue);

	int32 Value;
};

UENUM(BlueprintType, meta = (Depricated))
namespace EGameMapTypeId
{
	enum Type
	{
		PvP,
		Outpost,
		Polygon,
		Parking,
		CentralSt,
		Game,
		Hangar,
		MilitaryBase,
		Tropic,
		DesertTown,
		Lobby,
		End
	};
}

struct FGameMapTypeId
{
	static FString GetString(const EGameMapTypeId::Type& Id)
	{
		switch (Id)
		{
			case EGameMapTypeId::PvP: return FString("1x1");
			case EGameMapTypeId::Outpost: return FString("Outpost");
			case EGameMapTypeId::Polygon: return FString("Polygon");
			case EGameMapTypeId::Parking: return FString("Parking");
			case EGameMapTypeId::CentralSt: return FString("Central-Station");
			case EGameMapTypeId::Game: return FString("main");
			case EGameMapTypeId::Hangar: return FString("Hangar2");
			case EGameMapTypeId::MilitaryBase: return FString("MilitaryBase");
			case EGameMapTypeId::Tropic: return FString("Tropic");
			case EGameMapTypeId::DesertTown: return FString("DesertTown");
			case EGameMapTypeId::Lobby: return FString("LobbyRoom");
		}

		return FString();
	}

	static FText GetText(const EGameMapTypeId::Type& Id)
	{
		switch (Id)
		{
			case EGameMapTypeId::PvP: return NSLOCTEXT("EGameMapTypeId", "EGameMapTypeId.1x1", "1x1 [WIP]");
			case EGameMapTypeId::Outpost: return NSLOCTEXT("EGameMapTypeId", "EGameMapTypeId.Outpost", "Outpost");
			case EGameMapTypeId::Polygon: return NSLOCTEXT("EGameMapTypeId", "EGameMapTypeId.Polygon", "Polygon");
			case EGameMapTypeId::Parking: return NSLOCTEXT("EGameMapTypeId", "EGameMapTypeId.Parking", "Parking");
			case EGameMapTypeId::CentralSt: return NSLOCTEXT("EGameMapTypeId", "EGameMapTypeId.CentralSt", "Central-Station");
			case EGameMapTypeId::Game: return NSLOCTEXT("EGameMapTypeId", "EGameMapTypeId.Game", "Open World");
			case EGameMapTypeId::Hangar: return NSLOCTEXT("EGameMapTypeId", "EGameMapTypeId.Hangar", "Hangar");
			case EGameMapTypeId::MilitaryBase: return NSLOCTEXT("EGameMapTypeId", "EGameMapTypeId.MilitaryBase", "Military Base");
			case EGameMapTypeId::Tropic: return NSLOCTEXT("EGameMapTypeId", "EGameMapTypeId.Tropic", "Tropic");
			case EGameMapTypeId::DesertTown: return NSLOCTEXT("EGameMapTypeId", "EGameMapTypeId.DesertTown", "Desert Town");
			case EGameMapTypeId::Lobby: return NSLOCTEXT("EGameMapTypeId", "EGameMapTypeId.Lobby", "Lobby");
		}

		return FText::GetEmpty();
	}

	static FText GetText(const FString& Id)
	{
		if (Id.Equals("1x1")) return GetText(EGameMapTypeId::PvP);
		else if (Id.Equals("Outpost")) return GetText(EGameMapTypeId::Outpost);
		else if (Id.Equals("Polygon")) return GetText(EGameMapTypeId::Polygon);
		else if (Id.Equals("Parking")) return GetText(EGameMapTypeId::Parking);
		else if (Id.Equals("Central-Station")) return GetText(EGameMapTypeId::CentralSt);
		else if (Id.Equals("main")) return GetText(EGameMapTypeId::Game);
		else if (Id.Equals("Hangar2")) return GetText(EGameMapTypeId::Hangar);
		else if (Id.Equals("MilitaryBase")) return GetText(EGameMapTypeId::MilitaryBase);
		else if (Id.Equals("Tropic")) return GetText(EGameMapTypeId::Tropic);
		else if (Id.Equals("DesertTown")) return GetText(EGameMapTypeId::DesertTown);
		else if (Id.Equals("LobbyRoom")) return GetText(EGameMapTypeId::Lobby);
		else return FText::GetEmpty();
	}
};