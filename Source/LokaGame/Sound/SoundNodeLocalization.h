// VRSPRO

#pragma once

#include "Sound/SoundNode.h"
#include "SoundNodeLocalization.generated.h"

/**
 * 
 */
UCLASS()
class LOKAGAME_API USoundNodeLocalization : public USoundNode
{
	GENERATED_BODY()
	
public:
	USoundNodeLocalization();

	virtual void ParseNodes(FAudioDevice* AudioDevice, const UPTRINT NodeWaveInstanceHash, FActiveSound& ActiveSound, const struct FSoundParseParameters& ParseParams, TArray<FWaveInstance*>& WaveInstances) override;
	virtual void CreateStartingConnectors(void) override;
	virtual int32 GetMaxChildNodes() const override;
	virtual int32 GetMinChildNodes() const override;
#if WITH_EDITOR
	virtual FText GetInputPinName(int32 PinIndex) const override;
#endif

protected:

	TArray<FCultureRef> SupportedLanguagesArray;
};
