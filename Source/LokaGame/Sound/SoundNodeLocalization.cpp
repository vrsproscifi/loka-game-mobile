// VRSPRO

#include "LokaGame.h"
#include "SoundNodeLocalization.h"

USoundNodeLocalization::USoundNodeLocalization() 
	: Super()
{
	FInternationalization::Get().GetCulturesWithAvailableLocalization(FPaths::GetGameLocalizationPaths(), SupportedLanguagesArray, false);
}

void USoundNodeLocalization::ParseNodes(FAudioDevice* AudioDevice, const UPTRINT NodeWaveInstanceHash, FActiveSound& ActiveSound, const struct FSoundParseParameters& ParseParams, TArray<FWaveInstance*>& WaveInstances)
{
	auto CurrentLang = FInternationalization::Get().GetCurrentCulture();

	SSIZE_T _count = 0;
	for (auto &l : SupportedLanguagesArray)
	{
		if (l->GetLCID() == CurrentLang->GetLCID())
		{
			break;
		}
		++_count;
	}

	if (_count < ChildNodes.Num() && ChildNodes[_count])
	{
		ChildNodes[_count]->ParseNodes(AudioDevice, GetNodeWaveInstanceHash(NodeWaveInstanceHash, ChildNodes[_count], _count), ActiveSound, ParseParams, WaveInstances);
	}
}

void USoundNodeLocalization::CreateStartingConnectors()
{
	for (auto &l : SupportedLanguagesArray)
	{
		InsertChildNode(ChildNodes.Num());
	}
}

int32 USoundNodeLocalization::GetMaxChildNodes() const
{
	return GetMinChildNodes();
}

int32 USoundNodeLocalization::GetMinChildNodes() const
{
	return SupportedLanguagesArray.Num();
}

#if WITH_EDITOR
FText USoundNodeLocalization::GetInputPinName(int32 PinIndex) const
{
	SSIZE_T _count = 0;
	for (auto &l : SupportedLanguagesArray)
	{
		if (_count == PinIndex)
		{
			return FText::FromString(l->GetEnglishName());
		}
		++_count;
	}

	return FText::FromString("Unknown");
}
#endif

