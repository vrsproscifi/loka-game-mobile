#include "LokaGame.h"
//#include "WindowsNotify.h"
//#include "AllowWindowsPlatformTypes.h"
//#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
//
//#include <SDKDDKVer.h>
//
//// Windows Header Files:
//#include <Windows.h>
//#include <sal.h>
//#include <Psapi.h>
//#include <strsafe.h>
//#include <ObjBase.h>
//#include <ShObjIdl.h>
//#include <propvarutil.h>
//#include <functiondiscoverykeys.h>
//#include <intsafe.h>
//#include <guiddef.h>
//
//#include <roapi.h>
//#include <wrl/client.h>
//#include <wrl/implements.h>
//#include <windows.ui.notifications.h>
//
//
//using namespace Microsoft::WRL;
//using namespace ABI::Windows::UI::Notifications;
//using namespace ABI::Windows::Data::Xml::Dom;
//using namespace Windows::Foundation;
//
//HRESULT SetImageSrc(_In_z_ wchar_t *imagePath, _In_ IXmlDocument *toastXml);
//HRESULT CreateToastXml(_In_ IToastNotificationManagerStatics *toastManager, _Outptr_ IXmlDocument** inputXml);
//HRESULT SetTextValues(_In_reads_(textValuesCount) wchar_t **textValues, _In_ UINT32 textValuesCount, _In_reads_(textValuesCount) UINT32 *textValuesLengths, _In_ IXmlDocument *toastXml);
//HRESULT SetNodeValueString(_In_ HSTRING inputString, _In_ IXmlNode *node, _In_ IXmlDocument *xml);
//
//class StringReferenceWrapper
//{
//public:
//
//	// Constructor which takes an existing string buffer and its length as the parameters.
//	// It fills an HSTRING_HEADER struct with the parameter.      
//	// Warning: The caller must ensure the lifetime of the buffer outlives this      
//	// object as it does not make a copy of the wide string memory.      
//
//	StringReferenceWrapper(_In_reads_(length) PCWSTR stringRef, _In_ UINT32 length) throw()
//	{
//		HRESULT hr = WindowsCreateStringReference(stringRef, length, &_header, &_hstring);
//
//		if (FAILED(hr))
//		{
//			RaiseException(static_cast<DWORD>(STATUS_INVALID_PARAMETER), EXCEPTION_NONCONTINUABLE, 0, nullptr);
//		}
//	}
//
//	~StringReferenceWrapper()
//	{
//		WindowsDeleteString(_hstring);
//	}
//
//	template <size_t N>
//	StringReferenceWrapper(_In_reads_(N) wchar_t const (&stringRef)[N]) throw()
//	{
//		UINT32 length = N - 1;
//		HRESULT hr = WindowsCreateStringReference(stringRef, length, &_header, &_hstring);
//
//		if (FAILED(hr))
//		{
//			RaiseException(static_cast<DWORD>(STATUS_INVALID_PARAMETER), EXCEPTION_NONCONTINUABLE, 0, nullptr);
//		}
//	}
//
//	template <size_t _>
//	StringReferenceWrapper(_In_reads_(_) wchar_t(&stringRef)[_]) throw()
//	{
//		UINT32 length;
//		HRESULT hr = SizeTToUInt32(wcslen(stringRef), &length);
//
//		if (FAILED(hr))
//		{
//			RaiseException(static_cast<DWORD>(STATUS_INVALID_PARAMETER), EXCEPTION_NONCONTINUABLE, 0, nullptr);
//		}
//
//		WindowsCreateStringReference(stringRef, length, &_header, &_hstring);
//	}
//
//	HSTRING Get() const throw()
//	{
//		return _hstring;
//	}
//
//
//private:
//	HSTRING             _hstring;
//	HSTRING_HEADER      _header;
//};
//
//
//
//
//#pragma optimize("", off)
//bool FWindowsNotify::Flash(bool isStart)
//{
//	const auto procId = FWindowsPlatformProcess::GetCurrentProcessId();
//	const auto handler = FWindowsPlatformMisc::GetTopLevelWindowHandle(procId);
//
//	FlashWindow(handler, isStart);
//	return false;
//}
//
//
//// Create the toast XML from a template
//HRESULT CreateToastXml(_In_ IToastNotificationManagerStatics *toastManager, _Outptr_ IXmlDocument** inputXml)
//{
//	// Retrieve the template XML
//	HRESULT hr = toastManager->GetTemplateContent(ToastTemplateType_ToastImageAndText04, inputXml);
//	if (SUCCEEDED(hr))
//	{
//		wchar_t *imagePath = _wfullpath(nullptr, L"toastImageAndText.png", MAX_PATH);
//
//		hr = imagePath != nullptr ? S_OK : HRESULT_FROM_WIN32(ERROR_FILE_NOT_FOUND);
//		if (SUCCEEDED(hr))
//		{
//			hr = SetImageSrc(imagePath, *inputXml);
//			if (SUCCEEDED(hr))
//			{
//				wchar_t* textValues[] = {
//					L"Line 1",
//					L"Line 2",
//					L"Line 3"
//				};
//
//				UINT32 textLengths[] = { 6, 6, 6 };
//
//				hr = SetTextValues(textValues, 3, textLengths, *inputXml);
//			}
//		}
//	}
//	return hr;
//}
//
//// Set the value of the "src" attribute of the "image" node
//HRESULT SetImageSrc(_In_z_ wchar_t *imagePath, _In_ IXmlDocument *toastXml)
//{
//	wchar_t imageSrc[MAX_PATH] = L"file:///";
//	HRESULT hr = StringCchCat(imageSrc, ARRAYSIZE(imageSrc), imagePath);
//	if (SUCCEEDED(hr))
//	{
//		ComPtr<IXmlNodeList> nodeList;
//		hr = toastXml->GetElementsByTagName(StringReferenceWrapper(L"image").Get(), &nodeList);
//		if (SUCCEEDED(hr))
//		{
//			ComPtr<IXmlNode> imageNode;
//			hr = nodeList->Item(0, &imageNode);
//			if (SUCCEEDED(hr))
//			{
//				ComPtr<IXmlNamedNodeMap> attributes;
//
//				hr = imageNode->get_Attributes(&attributes);
//				if (SUCCEEDED(hr))
//				{
//					ComPtr<IXmlNode> srcAttribute;
//
//					hr = attributes->GetNamedItem(StringReferenceWrapper(L"src").Get(), &srcAttribute);
//					if (SUCCEEDED(hr))
//					{
//						hr = SetNodeValueString(StringReferenceWrapper(imageSrc).Get(), srcAttribute.Get(), toastXml);
//					}
//				}
//			}
//		}
//	}
//	return hr;
//}
//
//// Set the values of each of the text nodes
//HRESULT SetTextValues(_In_reads_(textValuesCount) wchar_t **textValues, _In_ UINT32 textValuesCount, _In_reads_(textValuesCount) UINT32 *textValuesLengths, _In_ IXmlDocument *toastXml)
//{
//	HRESULT hr = textValues != nullptr && textValuesCount > 0 ? S_OK : E_INVALIDARG;
//	if (SUCCEEDED(hr))
//	{
//		ComPtr<IXmlNodeList> nodeList;
//		hr = toastXml->GetElementsByTagName(StringReferenceWrapper(L"text").Get(), &nodeList);
//		if (SUCCEEDED(hr))
//		{
//			UINT32 nodeListLength;
//			hr = nodeList->get_Length(&nodeListLength);
//			if (SUCCEEDED(hr))
//			{
//				hr = textValuesCount <= nodeListLength ? S_OK : E_INVALIDARG;
//				if (SUCCEEDED(hr))
//				{
//					for (UINT32 i = 0; i < textValuesCount; i++)
//					{
//						ComPtr<IXmlNode> textNode;
//						hr = nodeList->Item(i, &textNode);
//						if (SUCCEEDED(hr))
//						{
//							hr = SetNodeValueString(StringReferenceWrapper(textValues[i], textValuesLengths[i]).Get(), textNode.Get(), toastXml);
//						}
//					}
//				}
//			}
//		}
//	}
//	return hr;
//}
//
//HRESULT SetNodeValueString(_In_ HSTRING inputString, _In_ IXmlNode *node, _In_ IXmlDocument *xml)
//{
//	ComPtr<IXmlText> inputText;
//
//	HRESULT hr = xml->CreateTextNode(inputString, &inputText);
//	if (SUCCEEDED(hr))
//	{
//		ComPtr<IXmlNode> inputTextNode;
//
//		hr = inputText.As(&inputTextNode);
//		if (SUCCEEDED(hr))
//		{
//			ComPtr<IXmlNode> pAppendedChild;
//			hr = node->AppendChild(inputTextNode.Get(), &pAppendedChild);
//		}
//	}
//
//	return hr;
//}
//
//// Create and display the toast
//HRESULT CreateToast(_In_ IToastNotificationManagerStatics *toastManager, _In_ IXmlDocument *xml)
//{
//	ComPtr<IToastNotifier> notifier;
//	HRESULT hr = toastManager->CreateToastNotifierWithId(StringReferenceWrapper(L"Microsoft.Samples.DesktopToasts22").Get(), &notifier);
//	if (SUCCEEDED(hr))
//	{
//		ComPtr<IToastNotificationFactory> factory;
//		hr = GetActivationFactory(StringReferenceWrapper(RuntimeClass_Windows_UI_Notifications_ToastNotification).Get(), &factory);
//		if (SUCCEEDED(hr))
//		{
//			ComPtr<IToastNotification> toast;
//			hr = factory->CreateToastNotification(xml, &toast);
//			if (SUCCEEDED(hr))
//			{
//				notifier->Show(toast.Get());
//
//				// Register the event handlers
//				//EventRegistrationToken activatedToken, dismissedToken, failedToken;
//				//ComPtr<ToastEventHandler> eventHandler(new ToastEventHandler(_hwnd, _hEdit));
//				//
//				//hr = toast->add_Activated(eventHandler.Get(), &activatedToken);
//				//if (SUCCEEDED(hr))
//				//{
//				//	hr = toast->add_Dismissed(eventHandler.Get(), &dismissedToken);
//				//	if (SUCCEEDED(hr))
//				//	{
//				//		hr = toast->add_Failed(eventHandler.Get(), &failedToken);
//				//		if (SUCCEEDED(hr))
//				//		{
//				//			hr = notifier->Show(toast.Get());
//				//		}
//				//	}
//				//}
//			}
//		}
//	}
//	return hr;
//}
//
//
//
//bool FWindowsNotify::Notify(const FText& title, const FText& message)
//{
//	ComPtr<IToastNotificationManagerStatics> toastStatics;
//	HRESULT hr = GetActivationFactory(StringReferenceWrapper(RuntimeClass_Windows_UI_Notifications_ToastNotificationManager).Get(), &toastStatics);
//
//	if (SUCCEEDED(hr))
//	{
//		ComPtr<IXmlDocument> toastXml;
//		hr = CreateToastXml(toastStatics.Get(), &toastXml);
//		if (SUCCEEDED(hr))
//		{
//			hr = CreateToast(toastStatics.Get(), toastXml.Get());
//		}
//	}
//
//	return SUCCEEDED(hr);
//}