#include "LokaGame.h"
#include "RichTextHelpers.h"
#include "SResourceTextBoxType.h"


const FResourceImageSize FResourceImageSize::Default = FResourceImageSize(FVector2D(16, 16), 12);
const FString FRichHelpers::Money = TEXT("money");
const FString FRichHelpers::Donate = TEXT("donate");
const FString FRichHelpers::Coin = TEXT("coin");
const FString FRichHelpers::Time = TEXT("time");
const FString FRichHelpers::Level = TEXT("level");

FRichHelpers::FTextHelper FRichHelpers::TextHelper_NotifyValue = FRichHelpers::FTextHelper().SetColor(FColorList::SkyBlue);

FRichHelpers::FResourceHelper FRichHelpers::ResourceHelper_Money = FRichHelpers::FResourceHelper().SetType(EResourceTextBoxType::Money);
FRichHelpers::FResourceHelper FRichHelpers::ResourceHelper_Donate = FRichHelpers::FResourceHelper().SetType(EResourceTextBoxType::Donate);

FRichHelpers::FResourceHelper& FRichHelpers::FResourceHelper::SetType(const EResourceTextBoxType::Type& InType)
{ 
	switch (InType)
	{
		case EResourceTextBoxType::Money: 
			ValueType = FRichHelpers::Money;
		break;
		case EResourceTextBoxType::Donate: 
			ValueType = FRichHelpers::Donate;
		break;
		case EResourceTextBoxType::Coin:
			ValueType = FRichHelpers::Coin;
		break;
		case EResourceTextBoxType::Time: 
			ValueType = FRichHelpers::Time;
		break;
		case EResourceTextBoxType::Level:
			ValueType = FRichHelpers::Level;
		break;
	}

	return *this;
}
