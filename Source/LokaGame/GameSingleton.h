#pragma once
#include "Entity/Item/CategoryTypeId.h"

#include "Fraction/FractionTypeId.h"
#include "Entity/Achievement/ItemAchievementTypeId.h"
#include "ServiceController/StoreController/Models/ListOfInstance.h"
#include "Models/PlayerEntityInfo.h"
#include "GameSingleton.generated.h"

class AWorldPreviewItem;
class UTutorialBase;
class UIdentityComponent;
struct FRequestExecuteError;
enum class ERequestErrorCode : uint8;
class FOperationRequest;
class UItemAchievementEntity;
class UFractionEntity;
class UItemBaseEntity;
struct FItemModelInfo;



UCLASS()
class LOKAGAME_API UGameSingleton : public UObject
{
	GENERATED_BODY()

public:
	UGameSingleton();

	void LoadGameData();
	void ReloadGameData();
	bool IsLoadedData() const;

	//TODO: ����� ������� ����� �� �������
	//UItemBaseEntity* FindItem(const int32& id) const;
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Singleton)
	UItemBaseEntity* FindItem(const int32 InModel, const TEnumAsByte<CategoryTypeId::Type> InCategory) const;
	UItemBaseEntity* FindItem(const FItemModelInfo& InModelInfo) const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Singleton)
	TArray<UDataAsset*> GetDataAssets() const { return DataAssets; }

	template<class TAsset>
	TArray<TAsset*> GetDataAssets() const
	{
		TArray<TAsset*> Answer;
		auto SourceAssets = GetDataAssets();

		for (auto Asset : SourceAssets)
		{
			if (auto CastedAsset = Cast<TAsset>(Asset))
			{
				Answer.Add(CastedAsset);
			}
		}

		return Answer;
	}

	template<class TObject>
	TObject* FindItem(const int32& InModel, const CategoryTypeId::Type& InCategory) const
	{
		return Cast<TObject>(FindItem(InModel, InCategory));
	}

	template<class TObject>
	TObject* FindItem(const FItemModelInfo& InModelInfo) const
	{
		return Cast<TObject>(FindItem(InModelInfo));
	}

	template<class TObject>
	TObject* FindItemChecked(const int32& InModel, const CategoryTypeId::Type& InCategory) const
	{
		return CastChecked<TObject>(FindItem(InModel, InCategory));
	}

	template<class TObject>
	TObject* FindItemChecked(const FItemModelInfo& InModelInfo) const
	{
		return CastChecked<TObject>(FindItem(InModelInfo));
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Singleton)
	TArray<UItemBaseEntity*> FindList(const CategoryTypeId::Type& InCategory) const;
	
	template<class TObject>
	TArray<TObject*> FindList(const CategoryTypeId::Type& InCategory) const
	{
		TArray<TObject*> ReturnValue;
		auto FoundList = FindList(InCategory);
		for (auto Item : FoundList)
		{
			if (auto TargetTyped = Cast<TObject>(Item))
			{
				ReturnValue.Add(TargetTyped);
			}
		}

		return ReturnValue;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Singleton)
	UFractionEntity* FindFraction(const FractionTypeId& fraction) const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Singleton)
	UItemAchievementEntity* FindAchievement(const TEnumAsByte<ItemAchievementTypeId::Type>& achievement) const;

	FORCEINLINE TArray<UItemBaseEntity*> GetGameItemInstanceList() const { return GameItemInstanceList; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Singleton)
	UTutorialBase* GetTutorial(const int32 InIndex) const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Singleton)
	FORCEINLINE TArray<UTutorialBase*> GetTutorials() const { return TutorialList; }

	static ThisClass* Get();

protected:
	//=======================================================
	TSharedPtr<FOperationRequest>	ListOfProducts;
	void OnListOfProductsSuccessfully(const FListOfInstance& data);
	void OnListOfProductsFailed(const FRequestExecuteError& error);

	//=======================================================
	UPROPERTY()	TArray<TSubclassOf<UItemBaseEntity>>						GameItemObjectList;

	UPROPERTY()	TArray<UItemBaseEntity*>									GameItemInstanceList;
	
	//TODO: ����� ������� ����� �� �������
	//UPROPERTY()	TMap<uint32, UItemBaseEntity*>							GameItemInstanceMap;
	//=======================================================
	UPROPERTY()	TArray<UFractionEntity*>									GameFractionList;
	UPROPERTY()	TMap<FractionTypeId, UFractionEntity*>						GameFractionMap;

	//=======================================================
	UPROPERTY()	TArray<UItemAchievementEntity*>								GameAchievementList;
	UPROPERTY()	TMap<TEnumAsByte<ItemAchievementTypeId::Type>, UItemAchievementEntity*>	GameAchievementMap;

	UPROPERTY() TArray<UDataAsset*>											DataAssets;
	UPROPERTY() TArray<UTutorialBase*>										TutorialList;

protected:

	virtual void PostInitProperties() override;

#if WITH_EDITOR
	TSharedPtr<SNotificationItem> MyNotify;
#endif
public:
	// Used for generate freely model index
	UPROPERTY()
	FListOfInstance LastListOfInstance;

protected:

	UPROPERTY() FPlayerEntityInfo LocalPlayerEntityInfo;

public:

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Runtime)
	FPlayerEntityInfo GetSavedPlayerEntityInfo() const;

	UFUNCTION(BlueprintCallable, Category = Runtime)
	void UpdateSavedPlayerEntityInfo(const UIdentityComponent* InComponent);

	UPROPERTY() FGuid LastLocalToken;

	//======================================================================================[ Capture Images

public:

	void DoCaptureEntityImages();

protected:

	UPROPERTY() AWorldPreviewItem*			PreviewItem;
	UPROPERTY() ASceneCapture2D*			CaptureWorker;
	UPROPERTY() ADirectionalLight*			PointLight;
	UPROPERTY() USkyLightComponent*			SkyLight;
	UPROPERTY() UMaterialInstanceDynamic*	DynamicMaterialForCapture;
	UPROPERTY() UWorld*						CaptureWorld;

	TQueue<UItemBaseEntity*> CaptureQueue;

	UWorld* GetCaptureWorld();

	float cTimer;
	int32 cState;

	bool Tick(float DeltaSeconds);

	FTickerDelegate TickDelegate;
	FDelegateHandle TickDelegateHandle;

	bool CheckIsExistPackage(const UObject* InObject) const;
};