﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "HoverMovementComponent.h"
#include "TransportHoverboard.h"
#include "PlayerInventoryItem.h"
#include "BaseCharacter.h"

UHoverMovementComponent::UHoverMovementComponent()
	: Super()
{
	MaxSmoothLocationDistance = 100.0f;
	MaxSmoothRotationOffset = .2f;
}

void UHoverMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	if (ShouldSkipUpdate(DeltaTime))
	{
		return;
	}

	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	UpdateMovement(DeltaTime);

	if ((GetNetMode() == NM_DedicatedServer || GetNetMode() == NM_Standalone) && GetWorld() && LastUpdateInputTime < GetWorld()->GetTimeSeconds())
	{
		HoverMoveReplicated.InputVector = FMath::VInterpTo(HoverMoveReplicated.InputVector, FVector::ZeroVector, DeltaTime, 10.0f);
	}
}

float UHoverMovementComponent::GetMaxSpeed() const
{
	return GetHoverSettings().Speed;
}

void UHoverMovementComponent::UpdateCorrection(const float InDeltaTime)
{
	Velocity = HoverMoveReplicated.Velocity;

	PerformMovement(InDeltaTime);

	const FVector OldComponentLocation = UpdatedComponent->GetComponentLocation();
	//const FRotator OldComponentRotation = UpdatedComponent->GetComponentRotation();

	if (FVector::Dist(HoverMoveReplicated.Location, OldComponentLocation) >= MaxSmoothLocationDistance)
	{
		UpdatedComponent->SetWorldLocation(HoverMoveReplicated.Location);
		//MoveUpdatedComponent(HoverMoveReplicated.Location - OldComponentLocation, OldComponentRotation, false);
	}
	else
	{
		UpdatedComponent->SetWorldLocation(FMath::VInterpConstantTo(OldComponentLocation, HoverMoveReplicated.Location, InDeltaTime, 1.0f));
	}

	//if (!FMath::IsNearlyEqual(HoverMoveReplicated.Rotation.Vector() | OldComponentRotation.Vector(), MaxSmoothRotationOffset))
	//{
	//	UpdatedComponent->SetWorldRotation(HoverMoveReplicated.Rotation);
	//}
	//else
	//{
	//	UpdatedComponent->SetWorldRotation(FMath::RInterpTo(OldComponentRotation, HoverMoveReplicated.Rotation, InDeltaTime, 2.0f));
	//}
}

void UHoverMovementComponent::UpdateMovement(const float InDeltaTime)
{
	if (PawnOwner && (PawnOwner->HasAuthority() || PawnOwner->IsLocallyControlled()))
	{
		// Correction after update
		if (PawnOwner->IsLocallyControlled())
		{
			if (PawnOwner->HasAuthority())
			{
				ServerUpdateInputMove_Implementation(ConsumeInputVector());
			}
			else
			{
				ServerUpdateInputMove(ConsumeInputVector());
				UpdateCorrection(InDeltaTime);

				APlayerController* PC = Cast<APlayerController>(PawnOwner->GetController());
				APlayerCameraManager* PlayerCameraManager = (PC ? PC->PlayerCameraManager : nullptr);
				if (PlayerCameraManager != nullptr && PlayerCameraManager->bUseClientSideCameraUpdates)
				{
					PlayerCameraManager->bShouldSendClientSideCameraUpdate = true;
				}
			}			
		}

		if (UpdatedComponent && PawnOwner->HasAuthority())
		{
			PerformMovement(InDeltaTime);

			HoverMoveReplicated.Velocity = Velocity;
			HoverMoveReplicated.Location = UpdatedComponent->GetComponentLocation();
			HoverMoveReplicated.Rotation = UpdatedComponent->GetComponentRotation();
		}
	}
	else
	{
		UpdateCorrection(InDeltaTime);
	}	
}

void UHoverMovementComponent::PerformMovement(const float InDeltaTime)
{
	FHitResult Hit(1.0f), HitZCheck;
	FVector InputVector = HoverMoveReplicated.InputVector;// GetPendingInputVector().GetClampedToMaxSize(1.0f);
	
	const float AnalogInputModifier = (InputVector.SizeSquared2D() > 0.f ? InputVector.Size2D() : 0.f);	
	const float MaxPawnSpeed = GetMaxSpeed() * AnalogInputModifier;
	const bool bExceedingMaxSpeed = IsExceedingMaxSpeed(MaxPawnSpeed);

	const FVector OldVelocity = Velocity;

	if (AnalogInputModifier > .0f && !bExceedingMaxSpeed) // Acceleration
	{
		Velocity += (InputVector * (Velocity.Size2D() + FMath::Abs(GetAccelerationForce())) - Velocity) * InDeltaTime;
	}
	else // Brake
	{
		if (Velocity.SizeSquared() > 0.f)
		{			
			const float VelSize = FMath::Max(Velocity.Size() - FMath::Abs(GetBrakeForce()) * InDeltaTime, 0.f);
			Velocity = Velocity.GetSafeNormal() * VelSize;			

			if (bExceedingMaxSpeed && Velocity.SizeSquared2D() < FMath::Square(MaxPawnSpeed))
			{
				Velocity = (OldVelocity.GetSafeNormal() * InputVector) * MaxPawnSpeed;
			}			
		}
	}	

	const FVector ComponentLocation = UpdatedComponent->GetComponentLocation();
	FRotator TargetRotation = UpdatedComponent->GetComponentRotation();

	auto UpdatedAsPrimitive = Cast<UPrimitiveComponent>(UpdatedComponent);
	auto HoverOwner = Cast<ATransportHoverboard>(PawnOwner);

	if (UpdatedAsPrimitive)
	{
		Velocity.Z = OldVelocity.Z;

		const bool IsUpMovementActive = !FMath::IsNearlyZero(InputVector.Z);
		const float ZHeight = IsUpMovementActive ? GetHoverHeightUp() : GetHoverHeight();
		const FQuat ComponentQuat = UpdatedComponent->GetComponentQuat();		
		const FVector ZCheckLocation = ComponentLocation - FVector::UpVector * ZHeight;

		FCollisionQueryParams CollisionQueryParams(TEXT("HoverZCheckLocation"), true, PawnOwner);

		if (HoverOwner)
		{
			for (int32 i = 0; i < HoverOwner->GetSupportedSeats(); ++i)
			{
				CollisionQueryParams.AddIgnoredActor(HoverOwner->GetPassenger(i));
			}
		}

		CollisionQueryParams.bTraceAsyncScene = true;

		FCollisionResponseParams CollisionResponseParams;

		GetWorld()->SweepSingleByChannel(HitZCheck, ComponentLocation, ZCheckLocation, ComponentQuat,
										 UpdatedAsPrimitive->GetCollisionObjectType(), UpdatedAsPrimitive->GetCollisionShape(), 
										 CollisionQueryParams, CollisionResponseParams);

		// GetGravityZ = 980.665 cm/s
		//if (HitZCheck.bBlockingHit == false || HitZCheck.Time > .1f)
		{			
			Velocity.Z = FMath::FInterpTo(Velocity.Z, Velocity.Z + GetGravityZ(), InDeltaTime, 1.0f);
		}

		const float SurfaceDotUp = HitZCheck.ImpactNormal | TargetRotation.RotateVector(FVector::UpVector);
		GEngine->AddOnScreenDebugMessage(0, 1, FColor::Green, *FString::Printf(TEXT("UpVector DotProduct: %.4f"), SurfaceDotUp));

		const float SurfaceDot = HitZCheck.ImpactNormal | TargetRotation.RotateVector(FVector::ForwardVector);
		GEngine->AddOnScreenDebugMessage(1, 1, FColor::Green, *FString::Printf(TEXT("ForwardVector DotProduct: %.4f"), SurfaceDot));

		const float DotMultipler = SurfaceDotUp - (SurfaceDot > .0f ? SurfaceDot : -SurfaceDot);
		GEngine->AddOnScreenDebugMessage(2, 1, FColor::Orange, *FString::Printf(TEXT("Surface DotMultipler: %.2f"), DotMultipler));

		const float MyHoverForce = FMath::Lerp(GetHoverForce(), .0f, FMath::Clamp(HitZCheck.Time, .0f, 1.0f)) * DotMultipler;
		GEngine->AddOnScreenDebugMessage(3, 1, FColor::Orange, *FString::Printf(TEXT("HoverForce: %.2f"), MyHoverForce));

		Velocity.Z = FMath::FInterpTo(Velocity.Z, InputVector.Z >= .0f ? Velocity.Z + MyHoverForce : Velocity.Z - MyHoverForce, InDeltaTime, 1.0f);
		GEngine->AddOnScreenDebugMessage(4, 1, FColor::Orange, *FString::Printf(TEXT("Velocity.Z: %.2f"), Velocity.Z));
		
		if (HitZCheck.bBlockingHit && Velocity.Z < .0f && FMath::IsNearlyZero(AnalogInputModifier + Velocity.SizeSquared2D(), .5f * DotMultipler))
		{
			Velocity.Z = FMath::FInterpTo(Velocity.Z, .0f, InDeltaTime, 10.0f * DotMultipler);
			GEngine->AddOnScreenDebugMessage(5, 1, FColorList::LimeGreen, *FString::Printf(TEXT("Blend to zero Velocity.Z: %.2f"), Velocity.Z));
		}	
	}

	Velocity = Velocity.GetClampedToMaxSize2D(GetMaxSpeed());	

	if (Velocity.SizeSquared2D() > .0f)
	{
		TargetRotation = FMath::RInterpTo(UpdatedComponent->GetComponentRotation(), Velocity.GetSafeNormal2D().Rotation(), InDeltaTime, 5.5f);
	}

	const FVector Delta = Velocity * InDeltaTime;

	// TODO: Trace натыкается на перса, решено каналом колизии в блупринтах.
	if (!Delta.IsNearlyZero())
	{
		SafeMoveUpdatedComponent(Delta, TargetRotation, true, Hit);
		
		if (Hit.IsValidBlockingHit())
		{
			SlideAlongSurface(Delta, 1.f - Hit.Time, Hit.Normal, Hit, true);

			const FVector NewLocation = UpdatedComponent->GetComponentLocation();
			Velocity = ((NewLocation - ComponentLocation) / InDeltaTime);

			GEngine->AddOnScreenDebugMessage(6, 1, FColorList::LimeGreen, *FString::Printf(TEXT("SlideAlongSurface & Correction: %.2f"), Velocity.SizeSquared()));
		}

		UpdateComponentVelocity();
	}
}

bool UHoverMovementComponent::IsExceedingMaxSpeed(float InMaxSpeed) const
{
	InMaxSpeed = FMath::Max(0.f, InMaxSpeed);
	const float MaxSpeedSquared = FMath::Square(InMaxSpeed);

	// Allow 1% error tolerance, to account for numeric imprecision.
	const float OverVelocityPercent = 1.01f;
	return (Velocity.SizeSquared2D() > MaxSpeedSquared * OverVelocityPercent);
}

FHoverTransportSettings UHoverMovementComponent::GetHoverSettings() const
{
	if (auto MyHoverTransport = Cast<ATransportHoverboard>(PawnOwner))
	{
		if (MyHoverTransport->GetInstance() && MyHoverTransport->GetInstance()->GetEntity<UItemHoverTransportEntity>())
		{
			return MyHoverTransport->GetInstance()->GetEntity<UItemHoverTransportEntity>()->GetHoverSettings();
		}
	}

	return FHoverTransportSettings();
}

float UHoverMovementComponent::GetAccelerationForce() const
{
	return GetHoverSettings().AccelerationForce;
}

float UHoverMovementComponent::GetBrakeForce() const
{
	return GetHoverSettings().BrakeForce;
}

float UHoverMovementComponent::GetHoverHeight() const
{
	return GetHoverSettings().HoverHeight;
}

float UHoverMovementComponent::GetHoverHeightUp() const
{
	return GetHoverSettings().HoverHeightUp;
}

float UHoverMovementComponent::GetHoverForce() const
{
	return GetHoverSettings().HoverForce;
}

bool UHoverMovementComponent::ServerUpdateInputMove_Validate(const FVector& InInputVector) { return true; }
void UHoverMovementComponent::ServerUpdateInputMove_Implementation(const FVector& InInputVector)
{
	if (PawnOwner && PawnOwner->HasAuthority())
	{
		if (GetWorld())
		{
			LastUpdateInputTime = GetWorld()->GetTimeSeconds() + 1.0f;
		}

		HoverMoveReplicated.InputVector = InInputVector.GetClampedToMaxSize(1.0f);
		//PawnOwner->AddMovementInput(HoverMoveReplicated.InputVector);		
	}
}

void UHoverMovementComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UHoverMovementComponent, HoverMoveReplicated);
}