// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Transport/ItemHoverTransportEntity.h"
#include "HoverMovementComponent.generated.h"


USTRUCT()
struct FRepHoverMovement
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY()		FVector_NetQuantizeNormal		InputVector;
	UPROPERTY()		FVector_NetQuantize				Velocity;
	UPROPERTY()		FVector							Location;
	UPROPERTY()		FRotator						Rotation;
};



/**
 * 
 */
UCLASS()
class LOKAGAME_API UHoverMovementComponent : public UPawnMovementComponent
{
	GENERATED_BODY()
	
public:

	UHoverMovementComponent();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual float GetMaxSpeed() const override;

	virtual float GetAccelerationForce() const;
	virtual float GetBrakeForce() const;
	virtual float GetHoverHeight() const;
	virtual float GetHoverHeightUp() const;
	virtual float GetHoverForce() const;

protected:

	virtual void UpdateCorrection(const float InDeltaTime);
	virtual void UpdateMovement(const float InDeltaTime);
	virtual void PerformMovement(const float InDeltaTime);

	bool IsExceedingMaxSpeed(float InMaxSpeed) const;

	FHoverTransportSettings GetHoverSettings() const;

	UFUNCTION(Reliable, Server, WithValidation)
	void ServerUpdateInputMove(const FVector& InInputVector);

	UPROPERTY(Replicated)
	FRepHoverMovement HoverMoveReplicated;

	UPROPERTY(BlueprintReadOnly, Category = Network)
	float LastUpdateInputTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Network)
	float MaxSmoothLocationDistance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Network)
	float MaxSmoothRotationOffset;
};
