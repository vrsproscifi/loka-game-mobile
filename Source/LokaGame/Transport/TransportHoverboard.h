// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Transport/TransportBase.h"
#include "TransportHoverboard.generated.h"

class UHoverMovementComponent;
/**
 * 
 */
UCLASS()
class LOKAGAME_API ATransportHoverboard : public ATransportBase
{
	GENERATED_BODY()
	
public:

	ATransportHoverboard();
	
	virtual void MoveForward(float InValue) override;
	virtual void MoveRight(float InValue) override;
	virtual void MoveUp(float InValue) override;

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UCapsuleComponent* CapsuleComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	USkeletalMeshComponent* MeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UHoverMovementComponent* MovementComponent;

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	TMap<int32, FName> SeatSockets;

	UPROPERTY(BlueprintReadOnly, Category = Gameplay)
	TMap<UPrimitiveComponent*, FCollisionResponseContainer> OriginCollision;

	UFUNCTION()
	void OnCharacterEntry(ABaseCharacter* InCharacter);

	UFUNCTION()
	void OnCharacterLeave(ABaseCharacter* InCharacter);
};
