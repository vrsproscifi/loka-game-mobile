// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "TransportHoverboard.h"
#include "HoverMovementComponent.h"
#include "BaseCharacter.h"
#include "BasePlayerController.h"

ATransportHoverboard::ATransportHoverboard()
	: Super()
{
	CapsuleComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule"));
	CapsuleComponent->SetCollisionProfileName(TEXT("Transport"));
	CapsuleComponent->bDynamicObstacle = true;
	RootComponent = CapsuleComponent;

	MovementComponent = CreateDefaultSubobject<UHoverMovementComponent>(TEXT("Movement"));
	MovementComponent->SetUpdatedComponent(CapsuleComponent);

	MeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	MeshComponent->SetCollisionProfileName(TEXT("Transport"));
	MeshComponent->SetupAttachment(CapsuleComponent);

	OnUserEntry.AddDynamic(this, &ATransportHoverboard::OnCharacterEntry);
	OnUserExit.AddDynamic(this, &ATransportHoverboard::OnCharacterLeave);
}

void ATransportHoverboard::MoveForward(float InValue)
{
	if (FMath::IsNearlyZero(InValue) == false)
	{
		FVector ForwardVector = GetControlRotation().RotateVector(FVector::ForwardVector);
		ForwardVector.Z = .0f;
		AddMovementInput(ForwardVector * InValue);
	}
}

void ATransportHoverboard::MoveRight(float InValue)
{
	if (FMath::IsNearlyZero(InValue) == false)
	{
		FVector RightVector = GetControlRotation().RotateVector(FVector::RightVector);
		RightVector.Z = .0f;
		AddMovementInput(RightVector * InValue);
	}
}

void ATransportHoverboard::MoveUp(float InValue)
{
	if (FMath::IsNearlyZero(InValue) == false)
	{
		AddMovementInput(FVector::UpVector * InValue);
	}
}

void ATransportHoverboard::OnCharacterEntry(ABaseCharacter* InCharacter)
{
	if (InCharacter && InCharacter->IsValidLowLevel())
	{
		const auto CurrentSeatId = GetSeatIdFor(InCharacter);
		if (SeatSockets.Contains(CurrentSeatId))
		{
			auto TargetSeatSocket = SeatSockets.FindChecked(CurrentSeatId);
			
			auto CharacterComponents = InCharacter->GetComponentsByClass(UPrimitiveComponent::StaticClass());
			for (auto MyComp : CharacterComponents)
			{
				if (auto TargetComp = Cast<UPrimitiveComponent>(MyComp))
				{
					// For storage default col, to restore on leave
					OriginCollision.Add(TargetComp, TargetComp->GetCollisionResponseToChannels());

					TargetComp->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
					TargetComp->SetCollisionResponseToChannel(COLLISION_TRANSPORT, ECR_Ignore);
				}
			}

			if (HasAuthority())
			{
				if (InCharacter->GetCharacterMovement())
				{
					InCharacter->GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_None);
				}

				InCharacter->AttachToComponent(MeshComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale, TargetSeatSocket);

				if (auto MyCtrl = Cast<ABasePlayerController>(GetController()))
				{
					MyCtrl->SetViewTarget(this);
				}
			}			
		}
	}
}

void ATransportHoverboard::OnCharacterLeave(ABaseCharacter* InCharacter)
{
	if (InCharacter && InCharacter->IsValidLowLevel())
	{
		auto CharacterComponents = InCharacter->GetComponentsByClass(UPrimitiveComponent::StaticClass());
		for (auto MyComp : CharacterComponents)
		{
			if (auto TargetComp = Cast<UPrimitiveComponent>(MyComp))
			{
				if (OriginCollision.Contains(TargetComp))
				{
					TargetComp->SetCollisionResponseToChannels(OriginCollision.FindAndRemoveChecked(TargetComp));						
				}
			}
		}

		if (HasAuthority())
		{
			InCharacter->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);

			if (InCharacter->GetCharacterMovement())
			{
				InCharacter->GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Falling);
			}

			if (auto MyCtrl = InCharacter->GetBaseController())
			{
				MyCtrl->SetViewTarget(InCharacter);
			}
		}		
	}
}
