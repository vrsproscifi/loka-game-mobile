﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "TransportBase.h"
#include "BaseCharacter.h"
#include "BasePlayerController.h"

ATransportBase::ATransportBase()
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	bReplicateMovement = true;
}

void ATransportBase::OnFocusStart_Implementation(AActor* InInstigator)
{
	
}

void ATransportBase::OnFocusLost_Implementation(AActor* InInstigator)
{
	
}

void ATransportBase::OnInteractStart_Implementation(AActor* InInstigator)
{
	if (!GetPassenger())
	{
		DriverEntry(Cast<ABaseCharacter>(InInstigator));
	}
}

void ATransportBase::OnInteractLost_Implementation(AActor* InInstigator)
{
	
}

void ATransportBase::OnRep_Instance()
{
	
}

void ATransportBase::OnRep_Seats()
{
	if (LastSeats != Seats)
	{
		for (int32 i = 0; i < Seats.Num(); ++i)
		{
			if (LastSeats.IsValidIndex(i) == false && Seats.IsValidIndex(i))
			{
				OnUserEntry.Broadcast(Seats[i]);
			}
		}

		for (int32 i = 0; i < LastSeats.Num(); ++i)
		{
			if (Seats.IsValidIndex(i) == false && LastSeats.IsValidIndex(i))
			{
				OnUserExit.Broadcast(LastSeats[i]);
			}
		}

		LastSeats = Seats;
	}
}

void ATransportBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATransportBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &ATransportBase::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ATransportBase::MoveRight);
	PlayerInputComponent->BindAxis("MoveUp", this, &ATransportBase::MoveUp);
	PlayerInputComponent->BindAxis("Turn", this, &ATransportBase::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ATransportBase::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &ATransportBase::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ATransportBase::LookUpAtRate);

	PlayerInputComponent->BindAction("UseAny", IE_Pressed, this, &ATransportBase::OnUseAnyAction);
}

void ATransportBase::TurnAtRate(float InValue)
{
	AddControllerYawInput(InValue * 45.0f * GetWorld()->GetDeltaSeconds());
}

void ATransportBase::LookUpAtRate(float InValue)
{
	AddControllerPitchInput(InValue * 45.0f * GetWorld()->GetDeltaSeconds());
}

void ATransportBase::OnUseAnyAction()
{
	DriverExit(GetPassenger());
}

void ATransportBase::InitializeInstance(UPlayerInventoryItem* InInstance)
{
	if (HasAuthority())
	{
		Instance = InInstance;
		OnRep_Instance();
	}
}

int32 ATransportBase::GetSupportedSeats() const
{
	return 1;
}

ABaseCharacter* ATransportBase::GetDriver() const
{
	return GetPassenger(0);
}

ABaseCharacter* ATransportBase::GetPassenger(const int32 SeatId) const
{
	return Seats.IsValidIndex(SeatId) ? Seats[SeatId] : nullptr;
}

int32 ATransportBase::GetSeatIdFor(ABaseCharacter* InUser) const
{
	return Seats.Find(InUser);
}

bool ATransportBase::DriverEntry(ABaseCharacter* InUser)
{
	return PassengerEntry(InUser, 0);
}

bool ATransportBase::DriverExit(ABaseCharacter* InUser)
{
	return PassengerExit(InUser);
}

bool ATransportBase::PassengerEntry(ABaseCharacter* InUser, const int32 SeatId)
{
	if (HasAuthority())
	{
		if (InUser && InUser->IsValidLowLevel() && InUser->GetTransport() != this)
		{
			ABasePlayerController* CharCtrl = InUser->GetBaseController();
			if (CharCtrl && SeatId < GetSupportedSeats() && SeatId >= 0)
			{
				if (Seats.IsValidIndex(SeatId) == false)
				{
					Seats.Insert(InUser, SeatId);
				}
				else
				{
					Seats[SeatId] = InUser;
				}

				if (SeatId == 0) // Driver
				{
					CharCtrl->Possess(this);
				}

				InUser->ControlledTransport = this;
				OnRep_Seats();
				return true;
			}
		}
	}
	else
	{
		ServerPassengerEntry(InUser, SeatId);
	}

	return false;
}

bool ATransportBase::PassengerExit(ABaseCharacter* InUser)
{
	if (HasAuthority())
	{
		if (InUser && InUser->IsValidLowLevel() && InUser->GetTransport() == this)
		{
			ABasePlayerController* CharCtrl = InUser->GetBaseController() ? InUser->GetBaseController() : Cast<ABasePlayerController>(GetController());
			if (CharCtrl)
			{
				const int32 TargetSeatId = GetSeatIdFor(InUser);
				if (TargetSeatId != INDEX_NONE)
				{
					Seats.RemoveAt(TargetSeatId);
				}

				if (TargetSeatId == 0) // Driver
				{
					CharCtrl->Possess(InUser);
				}

				InUser->ControlledTransport = nullptr;
				OnRep_Seats();
				return true;
			}
		}
	}
	else
	{
		ServerPassengerExit(InUser);
	}

	return false;
}

bool ATransportBase::ServerPassengerEntry_Validate(ABaseCharacter* InUser, const int32 SeatId) { return true; }
void ATransportBase::ServerPassengerEntry_Implementation(ABaseCharacter* InUser, const int32 SeatId)
{
	PassengerEntry(InUser, SeatId);
}

bool ATransportBase::ServerPassengerExit_Validate(ABaseCharacter* InUser) { return true; }
void ATransportBase::ServerPassengerExit_Implementation(ABaseCharacter* InUser)
{
	PassengerExit(InUser);
}

void ATransportBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATransportBase, Instance);
	DOREPLIFETIME(ATransportBase, Seats);
}
