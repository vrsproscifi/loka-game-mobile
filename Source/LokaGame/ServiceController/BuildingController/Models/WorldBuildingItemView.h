#pragma once

#include "Containers/LokaGuid.h"
#include "WorldBuildingAttachView.h"
#include "WorldBuildingItemView.generated.h"

USTRUCT(Blueprintable)
struct FBuildingPropertyContainer
{
	GENERATED_USTRUCT_BODY()

	/*
	 * �������� ���������
	 */
	UPROPERTY()	FString Key;

	/*
	* �� ��������� (FGuid)
	*/
	UPROPERTY()	FLokaGuid BuildingId;
	UPROPERTY()	FString Value;
};

USTRUCT(Blueprintable)
struct FWorldBuildingProperty
{
	GENERATED_USTRUCT_BODY()

	/* - Default - �������� �� ���������(��������).Value - ������� �������� � ��������� ������.
	 * - ��������, Key = storage. Default -- ������������ ���������������. Value - ������� ������
	 * - ��������, Key = buildingtime. Default -- ������� ��������. Value - ������� �������� �� �����
	 */
	UPROPERTY()	FString Default;

	/* Value - ������� �������� � ��������� ������.*/
	UPROPERTY()	FString Value;

	template<typename T>
	inline T GetValue() const;

	template<typename T>
	inline void SetValue(const T& InValue);

	template<typename T>
	T GetDefault() const
	{
		T TempValue;
		TTypeFromString<T>::FromString(TempValue, *Default);
		return TempValue;
	}

	FIntPoint ToIntPoint() const
	{
		return FIntPoint(GetValue<int32>(), GetDefault<int32>());
	}
};


template<typename T>
inline T FWorldBuildingProperty::GetValue() const
{
	T TempValue;
	TTypeFromString<T>::FromString(TempValue, *Value);
	return TempValue;
}

template<typename T>
inline void FWorldBuildingProperty::SetValue(const T& InValue)
{
	Value = TTypeToString<T>::ToString(InValue);
}

template<>
inline FGuid FWorldBuildingProperty::GetValue<FGuid>() const
{
	FGuid TempValue;
	FGuid::Parse(Value, TempValue);
	return TempValue;
}

template<>
inline void FWorldBuildingProperty::SetValue<FGuid>(const FGuid& InValue)
{
	Value = InValue.ToString();
}

USTRUCT()
struct FWorldBuildingItemView
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()	FLokaGuid ItemId;
	UPROPERTY()	FLokaGuid NewItemId;
	UPROPERTY()	uint32 ModelId;
	UPROPERTY()	FVector Position;
	UPROPERTY()	FRotator Rotation;
	UPROPERTY()	uint64 Storage;
	UPROPERTY()	uint64 Health;
	UPROPERTY()	TArray<FWorldBuildingAttachView> Sockets;
	UPROPERTY()	TMap<FString, FWorldBuildingProperty> Properties;

	friend bool operator==(const FWorldBuildingItemView& Lhs, const FWorldBuildingItemView& Rhs)
	{
		return	Lhs.ItemId == Rhs.ItemId &&
				Lhs.NewItemId == Rhs.NewItemId &&
				Lhs.ModelId == Rhs.ModelId &&
				Lhs.Position == Rhs.Position &&
				Lhs.Rotation == Rhs.Rotation;
	}
};


USTRUCT()
struct FPlayerNameIdPair
{
	GENERATED_USTRUCT_BODY()

	//	PlayerId, Type: FGuid, nullable (FGuid::Empty)
	UPROPERTY()	FLokaGuid Id;
	UPROPERTY()	FString Name;

	friend bool operator==(const FPlayerNameIdPair& Lhs, const FPlayerNameIdPair& Rhs)
	{
		return Lhs.Id == Rhs.Id && Lhs.Name == Rhs.Name;
	}

	friend bool operator!=(const FPlayerNameIdPair& Lhs, const FPlayerNameIdPair& Rhs)
	{
		return !(Lhs == Rhs);
	}
};

USTRUCT()
struct FPlayerWorldModel
{
	GENERATED_USTRUCT_BODY()

	//	���� Current.Id == FGuid::Empty, �� ����� ������� ���� �����
	//	�� �� ������ ������ �������� � �������� �� �� ������
	UPROPERTY()	FPlayerNameIdPair Current;
	UPROPERTY()	FPlayerNameIdPair Preview;
	UPROPERTY()	FPlayerNameIdPair Next;
	UPROPERTY()	TArray<FWorldBuildingItemView> Buildings;

	friend bool operator==(const FPlayerWorldModel& Lhs, const FPlayerWorldModel& Rhs)
	{
		return Lhs.Buildings == Rhs.Buildings && Lhs.Current == Rhs.Current && Lhs.Preview == Rhs.Preview && Lhs.Next == Rhs.Next;
	}

	friend bool operator!=(const FPlayerWorldModel& Lhs, const FPlayerWorldModel& Rhs)
	{
		return !(Lhs == Rhs);
	}

	bool IsEqual(const FPlayerWorldModel& InOther, const bool InIsCheckBuildings = false)
	{
		if (InIsCheckBuildings)
		{
			return *this == InOther;
		}

		return this->Current == InOther.Current && this->Preview == InOther.Preview && this->Next == InOther.Next;
	}
};


USTRUCT()
struct FWorldBuildingStorageView
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()	FLokaGuid ItemId;
	UPROPERTY()	uint64 Storage;
};

USTRUCT()
struct FHarvestResponse
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()	FWorldBuildingStorageView Storage;
	UPROPERTY()	uint64 Harvested;
};

USTRUCT(BlueprintType)
struct FUpdateBuildingProperty
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()	FLokaGuid BuildingId;
	UPROPERTY()	FString Key;
	UPROPERTY()	FString Value;
};

