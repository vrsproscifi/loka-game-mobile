#pragma once
#include "WorldBuildingAttachType.h"
#include "WorldBuildingAttachView.generated.h"


USTRUCT()
struct FWorldBuildingAttachView
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()	FString TargetId;
	UPROPERTY()	TEnumAsByte<WorldBuildingAttachType::Type> AttachFrom;
	UPROPERTY()	TEnumAsByte<WorldBuildingAttachType::Type> AttachTo;
};
