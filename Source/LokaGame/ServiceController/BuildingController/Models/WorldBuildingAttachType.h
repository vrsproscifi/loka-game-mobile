#pragma once
#include "WorldBuildingAttachType.generated.h"

UENUM()
namespace WorldBuildingAttachType
{
	enum Type
	{
		Left,
		Right,
		Forward,
		Back,

		BottomLeft,
		BottomRight,
		BottomForward,
		BottomBack,
	};
}
