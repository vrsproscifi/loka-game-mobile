#pragma once

#include "Containers/LokaGuid.h"
#include "PlayerRatingTopContainer.generated.h"


USTRUCT()
struct FPlayerRatingTopContainer
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()	FLokaGuid Id;
	UPROPERTY()	FString Name;
	UPROPERTY()	int32 Score;
	UPROPERTY()	int32 Level;
	UPROPERTY()	int32 ArmourLevel;
	UPROPERTY()	bool IsOnline;
};


USTRUCT()
struct FPlayerPvEMatchStatisticView
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY() FLokaGuid PlayerId;
	UPROPERTY() FString PlayerName;
	UPROPERTY() int32 Score;
	UPROPERTY() int32 Level;

	friend inline bool operator<(const FPlayerPvEMatchStatisticView& A, const FPlayerPvEMatchStatisticView& B)
	{
		return (A.Score < B.Score);
	}

	friend inline bool operator>(const FPlayerPvEMatchStatisticView& A, const FPlayerPvEMatchStatisticView& B)
	{
		return (A.Score > B.Score);
	}
};

USTRUCT()
struct FPlayerRatingTopView
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()	TArray<FPlayerRatingTopContainer> PlayerRating;
	UPROPERTY()	TArray<FPlayerPvEMatchStatisticView> PvERating;
};