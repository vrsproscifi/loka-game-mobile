#pragma once
#include "LobbyMatchResultDetailed.generated.h"

namespace EGameModeTypeId { enum Type : int32; }
namespace EGameMapTypeId { enum Type : int32; }

USTRUCT()
struct FLobbyMatchResultDetailed
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY()
		FString MatchId;

	UPROPERTY()
		TEnumAsByte<EGameModeTypeId::Type> GameModeTypeId;

	UPROPERTY()
		TEnumAsByte<EGameMapTypeId::Type> GameMapTypeId;

	UPROPERTY()
		bool IsWin;
};