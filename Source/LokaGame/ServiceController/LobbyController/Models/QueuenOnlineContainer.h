#pragma once
#include "QueuenOnlineContainer.generated.h"


namespace EGameModeTypeId { enum Type : int32; }


USTRUCT()
struct FQueuenOnlineContainer
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
		TEnumAsByte<EGameModeTypeId::Type> GameModeTypeId;

	UPROPERTY()
		int32 Count;
};