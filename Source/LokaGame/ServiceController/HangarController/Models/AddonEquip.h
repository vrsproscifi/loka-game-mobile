#pragma once
#include <Item/ItemSetSlotId.h>
#include <Item/CategoryTypeId.h>


namespace Operation
{
	namespace AddonEquip
	{
		struct Request final : FOnlineJsonSerializable
		{
			Request(
				const FGuid& itemId,
				const int32& addonModelId,
				const int32& addonSlot,
				const CategoryTypeId::Type& categoryTypeId = CategoryTypeId::Addon)
				: ItemId(itemId.ToString())
				, AddonSlot(addonSlot)
				, AddonModelId(addonModelId)
				, CategoryTypeId(categoryTypeId)
			{

			}

			FString ItemId;

			int32 AddonSlot;

			int32 AddonModelId;

			int32 CategoryTypeId;


			virtual ~Request() { }

			BEGIN_ONLINE_JSON_SERIALIZER
				ONLINE_JSON_SERIALIZE("ItemId", ItemId);
				ONLINE_JSON_SERIALIZE("AddonSlot", AddonSlot);
				ONLINE_JSON_SERIALIZE("AddonModelId", AddonModelId);
			END_ONLINE_JSON_SERIALIZER
		};

		struct Response final : FOnlineJsonSerializable
		{
			FString ItemId;

			int32 AddonSlot;

			int32 AddonModelId;

			FString AddonItemId;
			
			BEGIN_ONLINE_JSON_SERIALIZER
				ONLINE_JSON_SERIALIZE("ItemId", ItemId);
				ONLINE_JSON_SERIALIZE("AddonSlot", AddonSlot);
				ONLINE_JSON_SERIALIZE("AddonModelId", AddonModelId);
				ONLINE_JSON_SERIALIZE("AddonItemId", AddonItemId);
			END_ONLINE_JSON_SERIALIZER
		};

	}
}