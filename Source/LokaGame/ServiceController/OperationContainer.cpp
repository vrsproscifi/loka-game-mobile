#include "LokaGame.h"
#include "OperationContainer.h"
#include "Runtime/Core/Public/Misc/Base64.h"
#include "GameFramework/GameMode.h"

FString IOperationContainer::UserToken;
TSharedPtr<TArray<FHttpRequestPtr>, ESPMode::ThreadSafe> IOperationContainer::HttpRequestList = TSharedPtr<TArray<FHttpRequestPtr>, ESPMode::ThreadSafe>(new TArray<FHttpRequestPtr>());
TArray<FTimerHandle*> IOperationContainer::HttpPoolingRequestList;

#define MASTER_SERVER_ADDRESS 2

//
#if MASTER_SERVER_ADDRESS == 0			// FOR LOCAL SENTIKE BUILD
FServerHost const FServerHost::MasterClient = FServerHost("http://localhost:4444", "client");
FServerHost const FServerHost::MasterServer = FServerHost("http://localhost:4444", "server");
#elif MASTER_SERVER_ADDRESS == 1		// FOR Development BUILD
FServerHost const FServerHost::MasterClient = FServerHost("http://78.46.46.148:4443", "client");
FServerHost const FServerHost::MasterServer = FServerHost("http://78.46.46.148:4443", "server");
#elif MASTER_SERVER_ADDRESS == 2		// FOR Stable BUILD
FServerHost const FServerHost::MasterClient = FServerHost("https://www.lokagame.com:4445", "client");
FServerHost const FServerHost::MasterServer = FServerHost("https://www.lokagame.com:4445", "server");
#endif

FServerHost const FServerHost::ClusterServer = FServerHost("http://lokagame.com", "server");

FString const FVerb::Post = "POST";
FString const FVerb::Get = "GET";

SIZE_T IOperationContainer::AllowHttpRequest = true;


bool IOperationContainer::SendRequest(const FServerHost& host, const FString& Url, const FString& Data, const FHttpRequestCompleteDelegate& Delegate, TWeakPtr<IHttpRequest>& request, const FString& Verb)
{
	return SendRequest(host, Url, Data, UserToken, Delegate, request, Verb);
}

bool IOperationContainer::SendRequest(const FServerHost& host, const FString& Url, const FString& Data, const FString& Token, const FHttpRequestCompleteDelegate& Delegate, TWeakPtr<IHttpRequest>& request, const FString& Verb)
{
	if (AllowHttpRequest == false)
	{
		return false;
	}

	FHttpModule::Get().SetHttpTimeout(60.0f);
	FHttpModule::Get().SetMaxReadBufferSize(8 * 1024 * 1024);
	TSharedRef<IHttpRequest> HttpRequest = FHttpModule::Get().CreateRequest();
	request = HttpRequest;
	HttpRequestList->Add(HttpRequest);
	HttpRequest->SetContentAsString(Data);
	HttpRequest->OnProcessRequestComplete() = Delegate;
	HttpRequest->SetHeader(TEXT("Content-Type"), TEXT("application/json"));

	if (Token.IsEmpty() == false)
	{
		auto TokenBase = FBase64::Encode(Token);
		HttpRequest->SetHeader(TEXT("Authorization"), FString::Printf(TEXT("%s %s"), *host.Scheme, *TokenBase));
	}

	auto fullUrl = FString::Printf(TEXT("%s/%s"), *host.Host, *Url);

	HttpRequest->SetVerb(Verb);
	HttpRequest->SetURL(fullUrl);

	return HttpRequest->ProcessRequest();
}
