#pragma once

#include "Entity/Character/CharacterModelId.h"
#include "CharacterContainer.generated.h"


USTRUCT()
struct FCharacterAssetContainer
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
		FGuid AssetId;

	UPROPERTY()
		FGuid ItemId;
};

USTRUCT()
struct FCharacterContainer
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
		FGuid CharacterId;

	UPROPERTY()
		TEnumAsByte<ECharacterModelId::Type> ModelId;

	UPROPERTY()
		TArray<FCharacterAssetContainer> Assets;
};
