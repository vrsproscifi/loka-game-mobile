#pragma once

#include "Entity/Types/TypeCost.h"
#include "Entity/Achievement/ItemAchievementTypeId.h"
#include "AchievementContainer.generated.h"


USTRUCT()
struct FAchievementInstance
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY()
		int32 Amount;

	UPROPERTY()
		FTypeCost Reward;
};

USTRUCT()
struct FAchievementContainer
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
		TEnumAsByte<ItemAchievementTypeId::Type> Id;

	UPROPERTY()
		TArray<FAchievementInstance> Instances;
};
