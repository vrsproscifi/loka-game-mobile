#pragma once
#include "IdentityController/Models/LobbyClientPreSetData.h"
#include "UnlockProfile.generated.h"

namespace EPlayerPreSetId
{
	enum Type : int32;
}

USTRUCT()
struct FUnlockProfileResponse : public FClientPreSetData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
		TEnumAsByte<EPlayerPreSetId::Type> ProfileSlotId;
};