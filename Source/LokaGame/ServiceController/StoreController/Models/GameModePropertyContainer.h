#pragma once

#include "TypeCost.h"
#include "GameModeTypeId.h"
#include "GameModePropertyContainer.generated.h"

USTRUCT()
struct FGameModePropertyContainer
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
		TEnumAsByte<EGameModeTypeId::Type> Id;

	UPROPERTY()
		int32 PrepareTimeInSeconds;

	UPROPERTY()
		int32 MinimumLevel;

	UPROPERTY()
		FTypeCost JoinCost;
};