#pragma once
#include "Entity/Item/CategoryTypeId.h"
//#include "Entity/Fraction/FractionTypeId.h"
#include "Entity/Item/CategoryTypeId.h"
#include "Entity/Types/TypeCost.h"
#include "Entity/Types/TypeBonus.h"
#include "ItemBaseContainer.generated.h"


enum class FractionTypeId : uint8;




namespace CharacterModelFlagId
{
	enum Type : int32;
}

USTRUCT()
struct FItemBaseContainer
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()		int32 ModelId;

	UPROPERTY()		uint8 Level;


	UPROPERTY()		int32 CharacterModelsFlag;

	UPROPERTY()	int64 Performance;
	UPROPERTY()	int64 Storage;

	UPROPERTY()		FractionTypeId FractionId;

	UPROPERTY()		TEnumAsByte<CategoryTypeId::Type> CategoryTypeId;

	UPROPERTY()		FTypeCost Cost;

	UPROPERTY()		TMap<FString, FString> Properties;
};