#pragma once

#include "TutorialShortView.h"
#include "CharacterContainer.h"
#include "FractionContainer.h"
#include "ItemAmmoContainer.h"
#include "ItemBaseContainer.h"
#include "AchievementContainer.h"
#include "ListOfInstance.generated.h"

USTRUCT(NotBlueprintable)
struct FTournamentContainer
{
	GENERATED_USTRUCT_BODY()
	
	UPROPERTY()
		int32 Start;

	UPROPERTY()
		int32 End;
};


USTRUCT(NotBlueprintable)
struct FListOfInstance
{
	GENERATED_USTRUCT_BODY()
	UPROPERTY()	TArray<FTutorialShortView> Tutorials;
	UPROPERTY()	TArray<FAchievementContainer> Achievements;
	UPROPERTY()	TArray<FItemBaseContainer> Items;
	UPROPERTY()	TArray<FFractionContainer> Fraction;
	UPROPERTY()	int32 BuildDateTime;
	UPROPERTY()	int32 CostOfImprovementVote;
	UPROPERTY()	int32 CostOfSentencePush;	
	UPROPERTY()	FTournamentContainer Tournament;
};

