#pragma once

#include "Entity/Item/CategoryTypeId.h"
#include "TutorialShortView.generated.h"

USTRUCT()
struct FTutorialStepView
{
	GENERATED_USTRUCT_BODY()
	
	UPROPERTY()	int64 StepId;
	UPROPERTY()	TEnumAsByte<CategoryTypeId::Type> RewardCategoryId;
	UPROPERTY()	int32 RewardModelId;
	UPROPERTY() uint64 RewardGive;
};


USTRUCT()
struct FTutorialShortView
{
	GENERATED_USTRUCT_BODY()
	
	UPROPERTY()	int64 TutorialId;
	UPROPERTY()	FString TutorialName;
	UPROPERTY()	TArray<FTutorialStepView> Steps;
};
