#pragma once
#include "Service/ItemServiceModelId.h"
#include "PlayerUsingServiceView.generated.h"


USTRUCT()
struct FPlayerUsingServiceView
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()		TEnumAsByte<ServiceTypeId::Type> ServiceTypeId;

	UPROPERTY()		int64 ExpirationDate;

	UPROPERTY()		int64 NotifyDate;
};
