#pragma once

#include "AccountContactView.generated.h"

USTRUCT()
struct FAccountContactView
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY() FString Value;
	UPROPERTY() bool IsConfirmed;
};