#pragma once

#include <Types/TypeCash.h>
#include "RaitingContainer.h"
#include "PlayerUsingServiceView.h"
#include "AccountContactView.h"
#include "ServiceController/BuildingController/Models/WorldBuildingItemView.h"
#include "PlayerTutorialView.h"
#include "PlayerExperience.h"
#include "Fraction/FractionReputation.h"
#include "AccountStatusContainer.generated.h"


USTRUCT()
struct FAccountStatusContainer
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()	FAccountContactView Email;
	UPROPERTY()	FAccountContactView Phone;
	UPROPERTY() TArray<FWorldBuildingStorageView> Storage;
	UPROPERTY()	FString TutorialId;	// GUID

	UPROPERTY() TArray<FPlayerTutorialView> Tutorials;



	UPROPERTY()
		FTypeCash Cash;

	UPROPERTY()
		FPlayerExperience Experience;

	UPROPERTY()
		TArray<FPlayerUsingServiceView> UsingService;

	UPROPERTY()
		TArray<FFractionReputation> FractionExperience;

	UPROPERTY()
		FRaitingContainer  Raiting;

	UPROPERTY()
		bool IsSquadLeader;

	UPROPERTY()
		bool IsAllowVoteImprovement;
		
	UPROPERTY() int64 DateOfNextAvailableProposal;
};