#pragma once

#include "Fraction/FractionTypeId.h"
#include "CharacterItemWrapper.h"
#include "InvertoryItemWrapper.h"
#include "LobbyClientLootData.generated.h"

USTRUCT()
struct FLobbyClientLootData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
		TArray<FInvertoryItemWrapper> WeaponList;

	UPROPERTY()
		TArray<FInvertoryItemWrapper> GrenadeList;

	UPROPERTY()
		TArray<FInvertoryItemWrapper> KitList;

	UPROPERTY()
		TArray<FInvertoryItemWrapper> ArmourList;

	UPROPERTY()
		TArray<FInvertoryItemWrapper> CharacterList;

	UPROPERTY()
		TArray<FInvertoryItemWrapper> ServiceList;

	UPROPERTY()
		TArray<FInvertoryItemWrapper> BuildingList;
};