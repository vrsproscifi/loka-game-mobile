#pragma once
#include "Achievement/ItemAchievementTypeId.h"
#include "PlayeAchievementContainer.generated.h"


USTRUCT()
struct FPlayeAchievementContainer
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY()
		TEnumAsByte<ItemAchievementTypeId::Type> Id;

	UPROPERTY()
		int32 Amount;
};