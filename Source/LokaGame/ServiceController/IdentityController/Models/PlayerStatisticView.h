#pragma once
#include "PlayerStatisticView.generated.h"


UENUM()
namespace PlayerThreatLevel
{
	enum Type
	{
		Green,  //  From 0 to 2
		Bronze, //  From 2 to 4
		Silver, //  From 4 to 5
		Gold    //  From 5 to 7
	};
}

USTRUCT()
struct FPlayerStatisticView
{
	GENERATED_USTRUCT_BODY()

		//------------------------------------------------------------
		//                            [ Kills ]
		UPROPERTY() int64 Kills;
	/// <summary>
	/// Series of 3 Kill
	/// </summary>
	UPROPERTY() int64 SmallSeriesKill;

	/// <summary>
	/// Series of 5 Kill
	/// </summary>
	UPROPERTY() int64 ShortSeriesKill;

	/// <summary>
	/// Series of 7 Kill
	/// </summary>
	UPROPERTY() int64 LargeSeriesKill;

	/// <summary>
	/// Series of more 7 Kill
	/// </summary>
	UPROPERTY() int64 EpicSeriesKill;


	//------------------------------------------------------------
	//                            [ Deads ]
	UPROPERTY() int64 Deads;

	//------------------------------------------------------------
	//                            [ Assists ]
	UPROPERTY() int64 Assists;


	//------------------------------------------------------------
	//                            [ TeamKills ]
	UPROPERTY() int64 TeamKills;

		//================================================================================

		//------------------------------------------------------------
		//                            [ Shots ]
		UPROPERTY() int64 Shots;

	//------------------------------------------------------------
	//                            [ Hits ]
	UPROPERTY() int64 Hits;


	//------------------------------------------------------------
	//                            [ IncomingDamage ]
	UPROPERTY() int64 IncomingDamage;

	//------------------------------------------------------------
	//                            [ OutgoingDamage ]
	UPROPERTY() int64 OutgoingDamage;
			//================================================================================

		UPROPERTY() int64 Wins;

	UPROPERTY() int64 Loses;

	UPROPERTY() int64 Draws;

		//================================================================================

		UPROPERTY() float Threat;
	UPROPERTY() TEnumAsByte<PlayerThreatLevel::Type> ThreatLevel;
	UPROPERTY() TEnumAsByte<PlayerThreatLevel::Type> LastThreatLevel;
};