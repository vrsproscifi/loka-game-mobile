#pragma once
#include "ChangeRegionAccountView.generated.h"

USTRUCT()
struct FChangeRegionAccountView
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
		int32 Region;

	UPROPERTY()
		FString Language;

	FChangeRegionAccountView() : Region(0), Language("en") {}
	FChangeRegionAccountView(const FString& InLanguage) : Region(0), Language(InLanguage) {}
};

