#pragma once

#include "ModificationItemWrapper.generated.h"

USTRUCT()
struct FItemModification
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
		uint8 Type;

	UPROPERTY()
		float value;
};

UENUM()
enum class EArmourModificationTypeId : uint8
{
	None,

	/// <summary>
	/// ���
	/// </summary>
	Mass,

	/// <summary>
	/// ���������
	/// </summary>
	Reliability,

	/// <summary>
	/// ������ �� ������������	
	/// </summary>
	Antibleeding,

	/// <summary>
	/// �����
	/// </summary>
	Armor,

	/// <summary>
	/// �������� ������������	
	/// </summary>
	Movementspeed,

	/// <summary>
	/// ������� �������	
	/// </summary>
	Energycosts,

	/// <summary>
	/// �������������� �������	
	/// </summary>
	Energyregeneration,

	/// <summary>
	/// �����������
	/// </summary>
	HealthRegeneration,

	/// <summary>
	/// ����������� ���	
	/// </summary>
	Carriedweight,

	/// <summary>
	/// ����� ���������	
	/// </summary>
	Amountofoxygen,

	End
};

UENUM()
enum class EWeaponModificationTypeId : uint8
{
	None,

	/// <summary>
	/// ����
	/// </summary>
	Damage,

	/// <summary>
	/// �������������
	/// </summary>
	Piercing,

	/// <summary>
	/// ���������
	/// </summary>
	Range,

	/// <summary>
	/// ���������
	/// </summary>
	Dispersion,

	/// <summary>
	/// ������
	/// </summary>
	Feedback,

	/// <summary>
	/// ������� �� �����	
	/// </summary>
	Spread,

	/// <summary>
	/// �������� �����������	
	/// </summary>
	ReloadDuration,

	/// <summary>
	/// ����� ������������	
	/// </summary>
	Aimingtime,

	/// <summary>
	/// ����������������	
	/// </summary>
	FireRate,

	/// <summary>
	/// ���
	/// </summary>
	Mass,

	/// <summary>
	/// ���������
	/// </summary>
	Reliability,

	End
};
