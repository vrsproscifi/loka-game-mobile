#pragma once
#include "ConfirmTutorialStepResponse.generated.h"

USTRUCT(Blueprintable)
struct FStartTutorialView
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()	int64 TutorialId;
	UPROPERTY() int64 StepId;
	UPROPERTY() bool TutorialComplete;
};

USTRUCT(Blueprintable)
struct FConfirmTutorialStepResponse
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()	int64 TutorialId;
	UPROPERTY() int64 StepId;
	UPROPERTY() int64 NextStepId;
	
	//	Если произошло исключение при выдаче награды =)
	//	Этап обучения все равно будет завершен
	UPROPERTY()	bool RewardReceived;
	
	//	Если не найден следующий этап, считается что обучение завершено.
	//	Если добавить новый этап в БД и вызвать завершение нового этапа, то статус будет обновлен
	UPROPERTY()	bool TutorialComplete;
};