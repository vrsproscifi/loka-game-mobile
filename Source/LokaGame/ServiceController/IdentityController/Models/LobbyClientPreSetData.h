#pragma once

#include "Item/ItemSetSlotId.h"
#include "Character/CharacterModelId.h"
#include "LobbyClientPreSetData.generated.h"

USTRUCT()
struct FPlayerInventoryItemContainer
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()	FLokaGuid ItemId;
	UPROPERTY()	int32 SlotId;

	EItemSetSlotId::Type AsSlotId() const
	{
		return static_cast<EItemSetSlotId::Type>(SlotId);
	}

	friend bool operator==(const FPlayerInventoryItemContainer& Lhs, const FPlayerInventoryItemContainer& Rhs)
	{
		return Lhs.ItemId == Rhs.ItemId && Lhs.SlotId == Rhs.SlotId;
	}
};

USTRUCT(BlueprintType)
struct FClientPreSetData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()	FLokaGuid AssetId;
	UPROPERTY()	bool IsEnabled;
	UPROPERTY()	FString Name;
	UPROPERTY()	FLokaGuid CharacterId;
	UPROPERTY()	TArray<FPlayerInventoryItemContainer> Items;

	FClientPreSetData() : AssetId(), IsEnabled(false), Name(), CharacterId(), Items() {}

	friend bool operator==(const FClientPreSetData& Lhs, const FClientPreSetData& Rhs)
	{
		return	Lhs.AssetId == Rhs.AssetId && 
				Lhs.IsEnabled == Rhs.IsEnabled &&
				Lhs.Name == Rhs.Name &&
				Lhs.CharacterId == Rhs.CharacterId &&
				Lhs.Items == Rhs.Items;
	}
};
