#pragma once
#include <Runtime/Online/HTTP/Public/Http.h>

struct FVerb
{
	static const FString Post;
	static const FString Get;
};

struct FServerHost
{
	static const FServerHost MasterClient;
	static const FServerHost MasterServer;
	static const FServerHost ClusterServer;

	FServerHost(const FString &host, const FString &scheme)
		: Host(host)
		, Scheme(scheme)
	{

	}
	const FString Host;
	const FString Scheme;
};

DECLARE_MULTICAST_DELEGATE(FOnStartLeaveCurrentMode);
class IBaseOperation;
class IOperationContainer
{
public:
	static SIZE_T AllowHttpRequest ;

	static TSharedPtr<TArray<FHttpRequestPtr>, ESPMode::ThreadSafe> HttpRequestList;
	static TArray<FTimerHandle*> HttpPoolingRequestList;

	static FString UserToken;

	static bool SendRequest(const FServerHost& host, const FString& Url, const FString& Data, const FHttpRequestCompleteDelegate& Delegate, TWeakPtr<IHttpRequest>& request, const FString& Verb = FVerb::Post);
	static bool SendRequest(const FServerHost& host, const FString& Url, const FString& Data, const FString& Token, const FHttpRequestCompleteDelegate& Delegate, TWeakPtr<IHttpRequest>& request, const FString& Verb = FVerb::Post);

};
