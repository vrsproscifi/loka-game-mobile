#pragma once

namespace PlayerControllerSetId
{
	enum Type
	{
		End = 5
	};
}

namespace PlayerControllerMemberId
{
	enum Type
	{
		Party = 3,
		End = 8
	};
}