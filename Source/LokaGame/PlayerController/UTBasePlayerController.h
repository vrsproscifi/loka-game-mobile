// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Interfaces/UTTeamInterface.h"
#include "InventoryStructs.h"
#include "BasePlayerController.h"
#include "Slate/Game/SGameHUD.h"
#include "Item/CategoryTypeId.h"
#include "Weapon/ItemWeaponModelId.h"
#include "UTBasePlayerController.generated.h"

class UUTGameViewportClient;
class AUTPlayerState;

UENUM(BlueprintType)
namespace EInputMode
{
	enum Type
	{
		EIM_None,
		EIM_GameOnly,
		EIM_GameAndUI,
		EIM_UIOnly,
	};
}



UCLASS()
class LOKAGAME_API AUTBasePlayerController : public ABasePlayerController, public IUTTeamInterface
{
	GENERATED_UCLASS_BODY()


	virtual void SetupInputComponent() override;

	virtual void Destroyed() override;

	virtual void InitInputSystem() override;


	void InitPlayerState() override;

	UFUNCTION()
	virtual void Talk();

	UFUNCTION()
	virtual void TeamTalk();

	/*UFUNCTION(exec)
	virtual void Say(FString Message);

	UFUNCTION(exec)
	virtual void TeamSay(FString Message);

	UFUNCTION(reliable, server, WithValidation)
	virtual void ServerSay(const FString& Message, bool bTeamMessage);

	UFUNCTION(exec)
	void LobbySay(FString Message);

	UFUNCTION(reliable, server, WithValidation)
	void ServerLobbySay(const FString& Message);


	UFUNCTION(reliable, client)
	virtual void ClientSay(class AUTPlayerState* Speaker, const FString& Message, FName Destination);*/

	uint8 GetTeamNum() const override;
	// not applicable
	virtual void SetTeamForSideSwap_Implementation(uint8 NewTeamNum) override
	{}

	// A quick function so I don't have to keep adding one when I want to test something.  @REMOVEME: Before the final version
	UFUNCTION(exec)
	virtual void DebugTest(FString TestCommand);

	UFUNCTION(Server, Reliable, WithValidation)
	virtual void ServerDebugTest(const FString& TestCommand);


public:

	UFUNCTION(BlueprintCallable, Category = PlayerController)
	virtual UUTLocalPlayer* GetUTLocalPlayer();

public:
	// Let the game's player controller know there was a network failure message.
	virtual void HandleNetworkFailureMessage(enum ENetworkFailure::Type FailureType, const FString& ErrorString);

	/**Check to see if this PC can chat. Called on Client and server independantly*/
	bool AllowTextMessage(const FString& Msg);

	/**The accumulation of time added per message. Once overflowed the player must wait for this to return to 0*/
	float ChatOverflowTime;
	bool bOverflowed;
	FText SpamText;
	FString LastChatMessage;



#if !UE_SERVER
	virtual void Tick(float DeltaTime) override;
#endif

	UPROPERTY()
	TEnumAsByte<EInputMode::Type> InputMode;

	// If set to true, the player controller will pop up a menu as soon as it's safe
	bool bRequestShowMenu;

public:
	UPROPERTY()
	bool bSpectatorMouseChangesView;

	// Will actually query the UParty once persistent parties are enabled
	bool IsPartyLeader() { return true; }


	virtual void ClientEnableNetworkVoice_Implementation(bool bEnable);

	UFUNCTION(exec)
	void StartVOIPTalking();

	UFUNCTION(exec)
	void StopVOIPTalking();

protected:
	// Sends a message directly to a user.  
	virtual void DirectSay(const FString& Message);

	// Forward the direct say to alternate servers
	virtual bool ForwardDirectSay(AUTPlayerState* SenderPlayerState, FString& FinalMessage);


	//===============================================================] LOKA Inventory

protected:

	virtual void SetPawn(APawn* InPawn) override;
	virtual void SetSpectatorPawn(class ASpectatorPawn* NewSpectatorPawn) override;

public:

	template<const int32 Value>
	void OnSelectPreset_Helper()
	{
		if (GetCharacter() == nullptr)
		{
			OnSelectPreset(Value);
		}
	}

	template<const bool Value>
	void OnScrollPreset_Helper()
	{
		OnScrollProfile(Value);
	}

	void OnScrollProfile(const bool IsNext) const;
	void OnSelectPreset(const int32 PresetId);

	// UI Pointers
	TSharedPtr<class SGameHUD> UI_GameHUD;
	TSharedPtr<class SScoreboard> UI_Scoreboard;
	TSharedPtr<class SPresetSelector> Slate_PresetSelector;
	TSharedPtr<class SGameResults> UI_GameResults;
	TSharedPtr<class SAboutKiller> UI_AboutKiller;
	TSharedPtr<class SSniperScope> Slate_SniperScope;	
	TSharedPtr<class SGameContainer> Slate_GameContainer;

	// UI Try Create
	bool TryCreateHUD();
	bool TryCreateScoreboard();
	bool TryCreatePresetSelector();
	bool TryCreateGameMenu();
	bool TryCreateAboutKiller();
	bool TryCreateSniperScopeWidget();

	void OnToggleInGameMenu();
	// UI Toggle [client only]


	UFUNCTION(BlueprintCallable, Category = UserInterface)
	void ToggleGameHUD(const bool Toggle);

	UFUNCTION(BlueprintCallable, Category = UserInterface)
	virtual void ToggleScoreboard(const bool Toggle);

	UFUNCTION(BlueprintCallable, Category = UserInterface)
	void ToggleGameTimer(const bool Toggle);

	UFUNCTION(BlueprintCallable, Category = UserInterface)
	void ToggleGameMenu(const bool Toggle);

	// UI Toggle [server to client]


	UFUNCTION(reliable, client, BlueprintCallable, Category = UserInterface)
	void ClientToggleGameHUD(const bool Toggle);

	UFUNCTION(reliable, client, BlueprintCallable, Category = UserInterface)
	void ClientToggleScoreboard(const bool Toggle);

	//UFUNCTION(reliable, client, BlueprintCallable, Category = UserInterface)
	//void ClientToggleGameTimer(const bool Toggle);

	//====================================[ HUD ]

	UFUNCTION(reliable, client, BlueprintCallable, Category = UserInterface)
	void ClientSetLiveBarFlashing(const bool isHealth);

	UFUNCTION(reliable, client, BlueprintCallable, Category = UserInterface)
	void ClientShowTextTimed(const int32 Time, const ETimedTextMessage Message = ETimedTextMessage::Respawn);

	UFUNCTION(reliable, client, BlueprintCallable, Category = UserInterface)
	void ClientShowActionMessage(const EActionMessage MessageID, const int32& Score);	

	UFUNCTION(reliable, client, BlueprintCallable, Category = UserInterface)
	void ClientShowAchievement(const uint8 AchievementId);

	void ToggleChatWindow();

	UFUNCTION(reliable, client)
	void ClientChatEvent(const FString &Name, const FString &Message, const FLinearColor &Color, const ESendMessageTo SendTo);

	UFUNCTION(reliable, server, WithValidation)
	void ServerChatEvent(const FString &Message, const ESendMessageTo SendTo);

	//====================================[ Scoreboard & Timer ]

	UFUNCTION(reliable, client)
	void ClientAppendKillRow(const AUTPlayerState* Killer, const AUTPlayerState* Target, const uint8 weaponCategory = ECategoryTypeId::End, const uint8 weaponModel = ItemWeaponModelId::End, const bool IsLocal = false);

	//====================================[ Game Results & Menu ]

	UFUNCTION(reliable, client)
	void ClientShowMatchResults(const int32 &MatchWinner);

	//====================================[ Kill-Cam ]

	UFUNCTION()
	void OnRep_MyLastKiller();

	UPROPERTY(ReplicatedUsing=OnRep_MyLastKiller)
	const AActor* MyLastKiller;

	UPROPERTY()
	const APawn* LastCharacter;

	//====================================[ Base Overrides ]

	virtual void SetPlayer(UPlayer* InPlayer) override;

	UFUNCTION(reliable, client)
	void MakeLastAliveShow();
};