#pragma once

//=======================================================
//							[ Headers ]
#include "BasePlayerController.h"
#include "ConstructionPlayerController.generated.h"

class AWorldPreviewItem;
//=======================================================
//							[ Forward declaration ]
class AConstructionPlayerState;
class AConstructionCharacter;

UCLASS()
class AConstructionPlayerController : public ABasePlayerController
{
	GENERATED_BODY()


	//=======================================================
	//							[ Methods ]
	AConstructionPlayerController(const FObjectInitializer& ObjectInitializer);
	virtual void SetupInputComponent() override;
	virtual void PostInitializeComponents() override;
	virtual void BeginPlay() override;

public:
	AConstructionPlayerState* GetPlayerState() const;
	AConstructionCharacter* GetConstructionCharacter() const;
	void SetWorldPreviewItem(AWorldPreviewItem* InWorldPreviewItem);
	//=======================================================
	//							[ Handlers ]

	virtual void OnEscape() override;
	void OnToggleInventory();
	void ProcessInputPitch(float InValue);
	void ProcessInputYaw(float InValue);

protected:

	UFUNCTION(BlueprintCallable, Category = TEST)
	void ShowEarlyAccessDialog();

	//=======================================================
	//							[ Property ]

	UPROPERTY()
	AWorldPreviewItem* WorldPreviewItem;

	//=======================================================
	//							[ Widgets ]
};