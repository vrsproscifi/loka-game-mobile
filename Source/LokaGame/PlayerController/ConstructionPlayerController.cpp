#include "LokaGame.h"
#include "ConstructionPlayerState.h"
#include "ConstructionPlayerController.h"
#include "ConstructionCharacter.h"
#include "ConstructionCameraManager.h"
#include "WorldPreviewItem.h"

#include "SMessageBox.h"
#include "SMesBoxContentEarlyNotify.h"

AConstructionPlayerController::AConstructionPlayerController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PlayerCameraManagerClass = AConstructionCameraManager::StaticClass();

}

void AConstructionPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();	

	InputComponent->BindAction(TEXT("BuildInventory"), IE_Pressed, this, &AConstructionPlayerController::OnToggleInventory).bConsumeInput = false;

	InputComponent->BindAxis("Turn", this, &AConstructionPlayerController::ProcessInputYaw).bConsumeInput = false;
	InputComponent->BindAxis("LookUp", this, &AConstructionPlayerController::ProcessInputPitch).bConsumeInput = false;
}

void AConstructionPlayerController::ProcessInputPitch(float InValue)
{
	if (WorldPreviewItem)
	{
		WorldPreviewItem->AddRotationToMesh(FRotator(InValue, 0, 0));
	}
}

void AConstructionPlayerController::ProcessInputYaw(float InValue)
{
	if (WorldPreviewItem)
	{
		WorldPreviewItem->AddRotationToMesh(FRotator(0, InValue, 0));
	}
}

void AConstructionPlayerController::ShowEarlyAccessDialog()
{
#if !UE_SERVER
	QueueBegin(SMessageBox, TEXT("EarlyAccessDialog"))
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("EarlyNotify", "EarlyNotify.Title", "Early Access"));
		SMessageBox::Get()->SetContent(SNew(SMesBoxContentEarlyNotify));
		SMessageBox::Get()->SetAllowedSize(FBox2D(FVector2D(600, 400), FVector2D(1200, 600)));
		SMessageBox::Get()->SetButtonsText(NSLOCTEXT("EarlyNotify", "EarlyNotify.Buy", "Get Early Access"));
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([](const EMessageBoxButton InButton)
		{
			FPlatformProcess::LaunchURL(TEXT("https://lokagame.com/promo"), nullptr, nullptr);
		});
		SMessageBox::Get()->SetEnableClose(true);
		SMessageBox::Get()->ToggleWidget(true);
	QueueEnd
#endif
}

void AConstructionPlayerController::SetWorldPreviewItem(AWorldPreviewItem* InWorldPreviewItem)
{
	WorldPreviewItem = InWorldPreviewItem;
}

void AConstructionPlayerController::OnEscape()
{
	if (auto MyPlayerState = GetPlayerState())
	{
		MyPlayerState->ToggleConstructionEscape();
	}
}

void AConstructionPlayerController::PostInitializeComponents()
{
	Super::PostInitializeComponents();
}

void AConstructionPlayerController::BeginPlay()
{
	Super::BeginPlay();
}

AConstructionPlayerState* AConstructionPlayerController::GetPlayerState() const
{
	return Cast<AConstructionPlayerState>(PlayerState);
}

AConstructionCharacter* AConstructionPlayerController::GetConstructionCharacter() const
{
	return Cast<AConstructionCharacter>(GetCharacter());
}

void AConstructionPlayerController::OnToggleInventory()
{
	if (auto MyCharacter = GetConstructionCharacter())
	{
		MyCharacter->OnToggleInventory();
	}
}
