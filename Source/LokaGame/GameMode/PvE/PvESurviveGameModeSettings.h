#pragma once
#include "PvESurviveGameModeSettings.generated.h"

//	Unique settings for each stage
USTRUCT(BlueprintType, Category = "PvE")
struct FPvESurviveWaveSettings
{
	GENERATED_USTRUCT_BODY()

	//	How many bots can be created for the wave
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Bots", meta = (ClampMin = "5.0", ClampMax = "1000.0", UIMin = "5.0", UIMax = "1000.0"))
		int32 NumberOfBotsPerWave;
	
	//	How many bots can be created during the time interval
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Bots", meta = (ClampMin = "1.0", ClampMax = "30.0", UIMin = "1.0", UIMax = "30.0"))
		int32 NumberOfBotsPerInterval;
	
	//	After some time in seconds, there are new boats during the wave
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Time", meta = (ClampMin = "1.0", ClampMax = "100.0", UIMin = "1.0", UIMax = "100.0"))
		int32 IntevalOfBotSpawns;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Time", meta = (ClampMin = "1.0", ClampMax = "100.0", UIMin = "1.0", UIMax = "100.0"))
		int32 IntevalOfPlayersSpawns;

	//	How much time per minute can continues current wave
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Time", meta = (ClampMin = "1.0", ClampMax = "30.0", UIMin = "1.0", UIMax = "30.0"))
		int32 WaveTimeInMinuts;

	FORCEINLINE int32 GetIntevalOfBotSpawns() const
	{
		return IntevalOfBotSpawns < 1 ? 3 : IntevalOfBotSpawns;
	}

	FORCEINLINE int32 GetIntevalOfPlayersSpawns() const
	{
		return IntevalOfPlayersSpawns < 1 ? 5 : IntevalOfPlayersSpawns;
	}


	FORCEINLINE int32 GetNumberOfBotsToSpawn() const
	{
		return NumberOfBotsPerInterval > NumberOfBotsPerWave ? NumberOfBotsPerWave : NumberOfBotsPerInterval;
	}

	FString ToString() const
	{
		return FString::Printf(TEXT("NumberOfBotsPerWave: %d | NumberOfBotsPerInterval: %d[safe: %d] | IntevalOfBotSpawns: %d[safe: %d] | WaveTimeInMinuts: %d"), NumberOfBotsPerWave, NumberOfBotsPerInterval, GetNumberOfBotsToSpawn(), IntevalOfBotSpawns, GetIntevalOfBotSpawns(), WaveTimeInMinuts);
	}

};

//	Unique settings for each difficulty level
USTRUCT(BlueprintType, Category = "PvE")
struct FPvESurviveGameModeSettings
{
	GENERATED_USTRUCT_BODY()

	//-------------------------------------------
	
	//	Use if you have installed one wave and you need to repeat it several times
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		bool UseLoopWaves;

	//	Use if you have installed one wave and you need to repeat it several times
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ClampMin = "0.0", ClampMax = "300.0", UIMin = "0.0", UIMax = "300.0"))
		int32 NumberOfWaves;


	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ClampMin = "1.0", ClampMax = "20.0", UIMin = "1.0", UIMax = "20.0"))
		int32 DelayBetweenWaves;

	FORCEINLINE int32 GetDelayBetweenWaves() const
	{
		return DelayBetweenWaves < 2 ? 5 : DelayBetweenWaves;
	}

	//	Unique settings for each stage
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<FPvESurviveWaveSettings> Wave;

	//-------------------------------------------

	//	The number of lives the player
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ClampMin = "10.0", ClampMax = "300.0", UIMin = "10.0", UIMax = "300.0"))
		int32 NumberOfPlayerHealth;

	//	Regenerate life after the end of the wave
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ClampMin = "0.0", ClampMax = "300.0", UIMin = "0.0", UIMax = "300.0"))
		int32 NumberOfHealthToRegenerate;

	//-------------------------------------------

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ClampMin = "0.01", ClampMax = "7.9", UIMin = "0.01", UIMax = "7.9"))
		float LevelOfBotsDifficulty;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		bool UseDifficultyOverride;
		
	FORCEINLINE int32 GetWaveCount() const
	{
		return UseLoopWaves ? NumberOfWaves : Wave.Num();
	}

	FORCEINLINE FPvESurviveWaveSettings GetWave(const int32& wave) const
	{	
		return UseLoopWaves ? Wave[0] : Wave[wave];
	}
};
