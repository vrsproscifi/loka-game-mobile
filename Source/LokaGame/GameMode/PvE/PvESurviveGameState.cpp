#include "LokaGame.h"
#include "PvESurviveGameState.h"

APvESurviveGameState::APvESurviveGameState()
{

}

void APvESurviveGameState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APvESurviveGameState, CurrentRound);
	DOREPLIFETIME(APvESurviveGameState, NumberOfRounds);
}


