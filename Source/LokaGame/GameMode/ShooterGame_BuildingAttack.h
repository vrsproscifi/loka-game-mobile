// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "BuildingController/Models/WorldBuildingItemView.h"
#include "GameMode/AbstractGameMode/UTTeamGameMode.h"

#include "ShooterGame_BuildingAttack.generated.h"

class ABuildPlacedComponent;
class AUTPlayerState;

#define DEFEND_ON_PLAYER_TEAM 1
#define ATTACK_ON_PLAYER_TEAM 0

UCLASS()
class LOKAGAME_API AShooterGame_BuildingAttack : public AUTTeamGameMode
{
	GENERATED_BODY()
	
public:

	AShooterGame_BuildingAttack();
	
	virtual void StartPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;
	virtual void InitGameState() override;
	virtual void EndMatch() override;

	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual bool IsAllowJoinInProgress() const override { return false; }
	virtual void HandleMatchHasStarted() override;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Team)
	FORCEINLINE AUTTeamInfo* GetOwnerTeam() const { return OwnerTeam; };

protected:


	virtual void DefaultTimer() override;

	void OnBuildingsList(const TArray<FWorldBuildingItemView>& InList);
	void SwitchTeamsIfNeed();

	UPROPERTY()
	int32 TeamAttackWinningScore;

	UPROPERTY()
	TMap<FGuid, ABuildPlacedComponent*> BuildingsList;

	UPROPERTY()
	TMap<FGuid, ABuildPlacedComponent*> StorageComponents;

	UPROPERTY()
	TMap<FGuid, ABuildPlacedComponent*> DamagedBuildingsList;

	UPROPERTY()
	ABuildPlacedComponent* GeneralTargetBuilding;

	virtual void DetermineMatchWinner() override;
	virtual void AddExtraResult(AUTPlayerState* PlayerState) override;

	UFUNCTION()
	void OnComponentTakeDamage(ABuildPlacedComponent* InComponent);

	UFUNCTION()
	void OnComponentDeath(ABuildPlacedComponent* InComponent);

	UPROPERTY(Transient) 
	uint64 ContextValidator;

	UPROPERTY()
	FGuid OwnerId;

	UPROPERTY()
	FString OwnerName;

	UPROPERTY()
	AUTTeamInfo* OwnerTeam;

	UPROPERTY()
	AUTPlayerState* OwnerState;

	UPROPERTY()
	bool bDeveloperIsEnemy;
};
