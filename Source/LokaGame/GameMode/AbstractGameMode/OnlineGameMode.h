// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "OnlineGameMode.generated.h"

class UBaseLocalMessage;
struct FBaseLocalMessageData;
class ABasePlayerController;
class AOnlineGameState;
class UNodeComponent;
struct FNodeSessionMatch;
struct FNodeSessionMember;
struct FSessionMatchOptions;

//
UCLASS(Config = Game, Abstract)
class LOKAGAME_API AOnlineGameMode : public AGameMode
{
  GENERATED_BODY()

	//==================================================
public:
  AOnlineGameMode();
	UNodeComponent* GetNodeComponent();
	UNodeComponent* GetNodeComponent() const;

	const FSessionMatchOptions& GetMatchOptions() const;
	const FNodeSessionMatch& GetMatchInformation() const;
	const TArray<FNodeSessionMember>& GetMatchMebers() const;
	
	const FNodeSessionMember* FindMatchMember(const FString& InMemberId) const;
	uint64 GetMatchMebersNum() const;

	//==================================================
	virtual void InitGameState() override;

protected:
	FTimerHandle TimerHandle_DefaultTimer;
	void EndMatch() override;
	void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;
	void PreInitializeComponents() override;
	virtual void DefaultTimer();

	//	���������� ��������� ����� �� ������ ������, ���� ����� ��� ������ � �������
	virtual void SendEndMatchRequest(const struct FMatchEndResult& result);
protected:
	virtual bool IsAllowJoinInProgress() const;
	virtual void SendCheckAvalibleMember() const;
	virtual void OnGetAvalibleMembers(const TArray<FNodeSessionMember>& members);

public:
	/** Transitions to WaitingToStart and calls BeginPlay on actors. */
	virtual void StartPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	virtual void PreLogin(const FString& Options, const FString& Address, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage) override;
	virtual void PostLogin(APlayerController* NewPlayer) override;

	void SendLeaveSessionRequest(const FGuid& member);

public:
	template< class T = AOnlineGameState>
	T* GetValidGameState() const
	{
		return GetValidObject<T, AGameStateBase>(GameState);
	}

	UWorld* GetValidWorld() const
	{
		return GetValidObject(GetWorld());
	}

public:
	UPROPERTY()    TArray<class AUTTeamInfo*> Teams;

	virtual void OnInitializeMatchInformation();

public:

	virtual void SendLocalizaedMessage(TSubclassOf<UBaseLocalMessage> InClass, ABasePlayerController* InTo, FBaseLocalMessageData& InMessage);
	virtual void BroadcastLocalizedMessage(TSubclassOf<UBaseLocalMessage> InClass, FBaseLocalMessageData& InMessage);
};