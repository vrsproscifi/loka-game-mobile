// VRSPRO

#include "LokaGame.h"
#include "CapturePoint.h"
#include "UTGameState.h"
#include "UTPlayerState.h"
#include "UTPlayerController.h"
#include "ShooterGame_CapturePoint.h"
#include "CapturePointSquadAI.h"
#include "NodeComponent.h"

AShooterGame_CapturePoint::AShooterGame_CapturePoint() : Super()
{
	ResearchRate = .25f;
	SquadType = ACapturePointSquadAI::StaticClass();
}

void AShooterGame_CapturePoint::Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, TSubclassOf<UDamageType> DamageType)
{
	AUTGameMode::Killed(Killer, KilledPlayer, KilledPawn, DamageType);
}

void AShooterGame_CapturePoint::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	if (UGameplayStatics::HasOption(Options, "ResearchRate"))
	{
		const FString RawValue = UGameplayStatics::ParseOption(Options, "ResearchRate");
		ResearchRate = FCString::Atof(*RawValue);

		if (ResearchRate < .1f)
		{
			ResearchRate = .25f;
		}
	}
		
	Super::InitGame(MapName, Options, ErrorMessage);
}

void AShooterGame_CapturePoint::OnInitializeMatchInformation()
{
	Super::OnInitializeMatchInformation();

	if (UTGameState)
	{
		UTGameState->ResetTeamsScore();
		UTGameState->GoalScore = 100.0f;
	}
}

void AShooterGame_CapturePoint::StartPlay() 
{
	Super::StartPlay();

	TArray<AActor*> Actors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ACapturePoint::StaticClass(), Actors);

	for (auto a : Actors)
	{
		ACapturePoint* Point = Cast<ACapturePoint>(a);
		if (Point)
		{
			Point->OnCapturePointHold.BindUObject(this, &AShooterGame_CapturePoint::OnCapturePointHold);
			CapturePoints.Add(Point);
		}
	}
}

void AShooterGame_CapturePoint::DefaultTimer()
{
	Super::DefaultTimer();

	if (GetMatchState() == MatchState::InProgress)
	{
		if (UTGameState)
		{
			for (auto Point : CapturePoints)
			{
				if (Point->IsHold())
				{
					UTGameState->GiveTeamScore(Point->GetHoldTeam(), ResearchRate);
				}
			}

			if (UTGameState->GetTeamScore(0) >= 100.0f || UTGameState->GetTeamScore(1) >= 100.0f)
			{
				EndMatch();
			}
		}
	}
}

void AShooterGame_CapturePoint::InitGameState()
{
	Super::InitGameState();

	if (UTGameState)
	{
		UTGameState->ResetTeamsScore();
	}
}

void AShooterGame_CapturePoint::OnCapturePointHold(const ACapturePoint* Point, TMap<AUTPlayerState*, float> Players)
{
	if (UTGameState)
	{
		// Update score points per team
		if (Point->IsHold())
		{
			const int32 HoldTeam = Point->GetHoldTeam();
	
			for (SSIZE_T i = 0; i < UTGameState->GetNumTeams(); ++i)
			{
				if (i == HoldTeam)
				{
					UTGameState->GiveTeamScore(i, 5.0f);
				}
				else
				{					
					UTGameState->GiveTeamScore(i, -5.0f);
					UTGameState->SetTeamScore(i, FMath::Max(UTGameState->GetTeamScore(i), .0f));
				}
			}
		}
	
		// Update points per hold players
		for (auto p : Players)
		{
			p.Key->ScorePoints(p.Value);
	
			if (auto ctrl = Cast<AUTPlayerController>(p.Key->GetOwner()))
			{
				ctrl->ClientShowActionMessage(EActionMessage::Capture, p.Value);
			}
		}
	}
}