// VRSPRO

#pragma once

#include "UTTeamGameMode.h"
#include "ShooterGame_CapturePoint.generated.h"

class ACapturePoint;
class AUTPlayerState;

UCLASS()
class LOKAGAME_API AShooterGame_CapturePoint : public AUTTeamGameMode
{
	GENERATED_BODY()

public:

	AShooterGame_CapturePoint();
	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;
	virtual void OnInitializeMatchInformation() override;

protected:
	virtual void StartPlay() override;
	virtual void DefaultTimer() override;
	virtual void InitGameState() override;
	virtual void OnCapturePointHold(const ACapturePoint*, TMap<AUTPlayerState*, float>);
	virtual void Killed(class AController* Killer, class AController* KilledPlayer, class APawn* KilledPawn, TSubclassOf<UDamageType> DamageType) override;

	UPROPERTY(BlueprintReadOnly, Category = GamePlay)
	TArray<ACapturePoint*> CapturePoints;

	UPROPERTY(config)
	float ResearchRate;
};
