// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once

#include "UTSquadAI.h"

#include "CapturePointSquadAI.generated.h"

class ACapturePoint;

//* CarriedObject always null for this squad
UCLASS(NotPlaceable)
class LOKAGAME_API ACapturePointSquadAI : public AUTSquadAI
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(BlueprintReadOnly, Category = Squad)
	TArray<ACapturePoint*> Points;

	/** precast to ACapturePoint for convenience */
	UPROPERTY(BlueprintReadOnly, Category = Squad)
	ACapturePoint* GamePointObjective;

	/** current friendly flag carrier hide location */
	UPROPERTY()
	FRouteCacheItem HideTarget;
	/** time flag carrier reached hide location, used for tracking how long it was able to hide */
	UPROPERTY()
	float StartHideTime;
	/** last time flag carrier was discovered while hiding, used to make the FC fight/run away for a while before trying to hide again */
	UPROPERTY()
	float HidingSpotDiscoveredTime;
	/** recent hiding spots to avoid reusing a spot too quickly */
	UPROPERTY()
	TArray< TWeakObjectPtr<UUTPathNode> > UsedHidingSpots;
	/** alternate routes for getting back home with enemy flag */
	UPROPERTY()
	TArray<FAlternateRoute> CapRoutes;

	virtual void SetObjective(AActor* InObjective) override;

	virtual void Initialize(AUTTeamInfo* InTeam, FName InOrders) override;
	virtual bool MustKeepEnemy(APawn* TheEnemy) override;
	virtual bool ShouldUseTranslocator(AUTBot* B) override;
	virtual float ModifyEnemyRating(float CurrentRating, const FBotEnemyInfo& EnemyInfo, AUTBot* B) override;
	virtual bool CheckSquadObjectives(AUTBot* B) override;

	/** return whether the passed in location is close to an enemy base (e.g. if enemy carrier is this close, it's about to score) */
	virtual bool IsNearEnemyBase(const FVector& TestLoc);

	virtual void NotifyObjectiveEvent(AActor* InObjective, AController* InstigatedBy, FName EventName) override;
	virtual bool HasHighPriorityObjective(AUTBot* B);

	virtual bool TryPathTowardObjective(AUTBot* B, AActor* Goal, bool bAllowDetours, const FString& SuccessGoalString) override;

	virtual void GetPossibleEnemyGoals(AUTBot* B, const FBotEnemyInfo* EnemyInfo, TArray<FPredictedGoal>& Goals) override;

	virtual void DrawDebugSquadRoute(AUTBot* B) const override;

	ACapturePoint* FindNearCapturePoint(const bool IsEnemy = true, AUTBot* B = nullptr) const;
	bool IsActorNearPoint(AActor* InActor, ACapturePoint* InPoint) const;
	void MaybeChangeCapturePoint();

protected:

	UPROPERTY()
	float LastPointChanged;
};
