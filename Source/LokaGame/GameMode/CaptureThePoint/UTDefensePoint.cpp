// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#include "LokaGame.h"
#include "UTBot.h"
#include "UTGameObjective.h"
#include "UTDefensePoint.h"
#include "UTRecastNavMesh.h"
#include "Weapon/ShooterWeapon.h"

AUTDefensePoint::AUTDefensePoint(const FObjectInitializer& OI)
	: Super(OI)
{
	Icon = OI.CreateOptionalDefaultSubobject<UBillboardComponent>(this, FName(TEXT("Icon")));
	if (Icon != nullptr)
	{
		Icon->bHiddenInGame = true;
		RootComponent = Icon;
	}
#if WITH_EDITORONLY_DATA
	EditorArrow = OI.CreateEditorOnlyDefaultSubobject<UArrowComponent>(this, TEXT("EditorArrow"));
	if (EditorArrow != nullptr)
	{
		EditorArrow->SetupAttachment(RootComponent);
		EditorArrow->ArrowSize = 0.5f;
		EditorArrow->ArrowColor = FLinearColor(0.0f, 1.0f, 0.0f, 1.0f).ToFColor(false);
	}
#endif

	BasePriority = 2;
}

void AUTDefensePoint::BeginPlay()
{
	Super::BeginPlay();

	if (Objective != nullptr)
	{
		Objective->DefensePoints.AddUnique(this);
	}

	AUTRecastNavMesh* NavMesh = GetUTNavData(GetWorld());
	if (NavMesh != nullptr)
	{
		MyNode = NavMesh->FindNearestNode(GetActorLocation(), NavMesh->GetPOIExtent(this));
		if (MyNode == nullptr)
		{
			// try a bit lower
			MyNode = NavMesh->FindNearestNode(GetActorLocation() - FVector(0.0f, 0.0f, GetSimpleCollisionHalfHeight()), NavMesh->GetPOIExtent(this));
		}
	}
}
void AUTDefensePoint::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	if (Objective != nullptr)
	{
		Objective->DefensePoints.Remove(this);
	}
}

int32 AUTDefensePoint::GetPriorityFor(AUTBot* B) const
{
	int32 Priority = BasePriority * 100;
	if (MyNode != nullptr && (B->Personality.MapAwareness > 0.0f || B->Skill + B->Personality.MapAwareness >= 5.0f))
	{
		Priority += 50 * FMath::Clamp<float>((float(MyNode->NearbyKills) * 0.33f / float(MyNode->NearbyDeaths)), 0.0f, 1.0f);
	}
	if (B->Personality.Accuracy - B->Personality.Aggressiveness >= 1.0f || (B->GetUTChar() != nullptr && B->GetUTChar()->GetWeapon() != nullptr && B->GetUTChar()->GetWeapon()->IsSniping()))
	{
		Priority += 50;
	}
	return Priority;
}