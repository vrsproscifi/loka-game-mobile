// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once

#include "UTTeamGameMode.h"
#include "UTTeamDMGameMode.generated.h"

UCLASS()
class LOKAGAME_API AUTTeamDMGameMode : public AUTTeamGameMode
{
  GENERATED_BODY()

  //==================================================
public:
  AUTTeamDMGameMode();

		/** whether to reduce team score for team kills */
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = TeamDM)
		bool bScoreTeamKills;
	/** whether to reduce team score for suicides */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = TeamDM)
		bool bScoreSuicides;

	virtual void ScoreKill_Implementation(AController* Killer, AController* Other, APawn* KilledPawn, TSubclassOf<UDamageType> DamageType) override;
	virtual void ScoreTeamKill_Implementation(AController* Killer, AController* Other, APawn* KilledPawn, TSubclassOf<UDamageType> DamageType) override;
	virtual AUTPlayerState* IsThereAWinner_Implementation(bool& bTied) override;
};