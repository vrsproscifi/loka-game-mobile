// VRSPRO

#include "LokaGame.h"
#include "RHI.h"
#include "AudioDevice.h"
#include "LokaGameUserSettings.h"


ULokaGameUserSettings::ULokaGameUserSettings(const FObjectInitializer& PCIP) 
: Super(PCIP)
{
	SetToDefaults();

	RHIGetAvailableResolutions(ScreenResolutionArray, true);
	
	UE_LOG(LogClass, Display, TEXT("[LokaGameUserSettings]: Created..."));
}

void ULokaGameUserSettings::SetToDefaults()
{
	Super::SetToDefaults();

	GeneralQualityLevel = EQualityLevels::Low;
	bIsFirstRun = true;

	AudioVolume_Master = 1.0f;
	AudioVolume_SFX = 0.6f;
	AudioVolume_UI = 1.0f;
	AudioVolume_Music = 1.0f;

	bShowPing = false;
	bShowFPS = false;

	Language = (FPlatformMisc::GetDefaultLocale() == "ru") ? "ru" : "en";
	FInternationalization::Get().SetCurrentCulture(Language);

	UE_LOG(LogClass, Display, TEXT("[LokaGameUserSettings]: SetToDefaults"));
}

ULokaGameUserSettings::~ULokaGameUserSettings()
{
	UE_LOG(LogClass, Display, TEXT("[LokaGameUserSettings]: Destroyed"));
}

void ULokaGameUserSettings::LoadSettings(bool bForceReload)
{
	Super::LoadSettings(bForceReload);	
	
	Scalability::LoadState(*GGameUserSettingsIni);
	ScalabilityQuality = Scalability::GetQualityLevels();
}

void ULokaGameUserSettings::ConfirmSettings(const bool _confirm)
{
	if (_confirm)
	{
		ULokaGameUserSettings::ConfirmVideoMode();	
		FInternationalization::Get().SetCurrentCulture(Language);
	}
	else
	{
		ULokaGameUserSettings::RevertVideoMode();
	}	
}

void ULokaGameUserSettings::InitSettings()
{
	SupportedLanguagesArray.Empty();

	FInternationalization *_culture = &FInternationalization::Get();
	checkf(_culture, TEXT("_culture was nullptr"));

	_culture->GetCulturesWithAvailableLocalization(FPaths::GetGameLocalizationPaths(), SupportedLanguagesArray, false);

	FInternationalization::Get().SetCurrentCulture(Language);	

	SetSoundClassVolume("Master", AudioVolume_Master);
	SetSoundClassVolume("SFX", AudioVolume_SFX);
	SetSoundClassVolume("UI", AudioVolume_UI);
	SetSoundClassVolume("Music", AudioVolume_Music);

	if (bIsFirstRun)
	{
		bIsFirstRun = false;
		GeneralQualityLevel = EQualityLevels::Personal;
		ScalabilityQuality = Scalability::BenchmarkQualityLevels(10);
		Scalability::SetQualityLevels(ScalabilityQuality);
		Scalability::SaveState(GGameUserSettingsIni);

		ApplyResolutionSettings(false);

		SaveConfig(CPF_Config, *GGameUserSettingsIni);
	}

	if (EQualityLevels::Personal != GeneralQualityLevel)
	{
		ScalabilityQuality.SetFromSingleQualityLevel(static_cast<int32>(GeneralQualityLevel));
		Scalability::SetQualityLevels(ScalabilityQuality);
	}

	Super::ApplySettings(true);

	UE_LOG(LogClass, Log, TEXT("[ULokaGameUserSettings]:InitSettings"));
}

int32 ULokaGameUserSettings::GetCurrentLanguage() const
{
	return FInternationalization::Get().GetCurrentCulture()->GetLCID();
}

void ULokaGameUserSettings::SetCurrentLanguage(const int32 &_lcid)
{
	for (auto &_loc : SupportedLanguagesArray)
	{
		if (_loc->GetLCID() == _lcid)
		{
			Language = _loc->GetTwoLetterISOLanguageName();
			FInternationalization::Get().SetCurrentCulture(Language);
			break;
		}
	}	
}

bool ULokaGameUserSettings::SetSoundClassVolume(FString ClassName, float Volume)
{
	//FAudioDevice* AudioDevice = GEngine->GetMainAudioDevice();
	//
	//if (!AudioDevice) return false;
	//
	//for (auto i = AudioDevice->SoundClasses.CreateIterator(); i; ++i)
	//{
	//	USoundClass* SoundClass = i.Key();
	//	FString SoundClassName = SoundClass->GetFullName();
	//
	//	if (SoundClassName.Contains("Game") && SoundClassName.Contains("." + ClassName))
	//	{
	//		SoundClass->Properties.Volume = Volume;
	//		return true;
	//	}
	//}

	return false;
}

float ULokaGameUserSettings::GetSoundClassVolume(FString ClassName)
{
	FAudioDevice* AudioDevice = GEngine->GetMainAudioDevice();

	if (!AudioDevice) return -1.0f;

	//for (auto i = AudioDevice->SoundClasses.CreateIterator(); i; ++i)
	//{
	//	USoundClass* SoundClass = i.Key();
	//	FString SoundClassName;
	//
	//	if (SoundClassName.Contains("Game") && SoundClassName.Contains("." + ClassName))
	//	{
	//		return SoundClass->Properties.Volume;
	//	}
	//}

	return -1.0f;
}