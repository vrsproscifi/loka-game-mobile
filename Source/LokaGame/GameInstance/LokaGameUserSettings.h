// VRSPRO
#pragma once

#include "GameFramework/GameUserSettings.h"
#include "LokaGameUserSettings.generated.h"

USTRUCT(Blueprintable, BlueprintType, Category = "Loka Game | Runtime | Controlls")
struct FLokaBindedKeys
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Blueprintable, BlueprintReadWrite)
		FString ActionName;

	UPROPERTY(Blueprintable, BlueprintReadWrite)
		FString StringKey;

	UPROPERTY(Blueprintable, BlueprintReadWrite)
		uint32 bGamepad : 1;

	UPROPERTY(Blueprintable, BlueprintReadWrite)
		uint32 bIsAxis : 1;
	
	UPROPERTY(Blueprintable, BlueprintReadWrite)
		float Scale; 
};

USTRUCT(Blueprintable, BlueprintType, Category = "Loka Game | Runtime | Localization")
struct FLokaLocalization
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Blueprintable, BlueprintReadOnly, Category = "Loka Game | Runtime | Localization")
		FString Name;

	UPROPERTY(Blueprintable, BlueprintReadOnly, Category = "Loka Game | Runtime | Localization")
		FString NativeName;

	UPROPERTY(Blueprintable, BlueprintReadOnly, Category = "Loka Game | Runtime | Localization")
		int32 Index;
};

UENUM(Blueprintable, BlueprintType, Category = "Loka Game | Runtime")
enum class EdetectedVideoCardPC : uint8
{
	Unknown,
	NVIDIA,
	AMD,
	Intel,
};

UENUM(Blueprintable, BlueprintType, Category = "Loka Game | Runtime | Graphics")
enum class EQualityLevels : uint8
{
	Low,
	Medium,
	High,
	Epic,
	Cinematic,
	Personal
};

UENUM(Blueprintable, BlueprintType, Category = "Loka Game | Runtime | Graphics")
enum class ELokaWindowMode : uint8
{
	Fullscreen,
	WindowedFullscreen,
	Windowed,
	WindowedMirror,
};

USTRUCT(Blueprintable, BlueprintType, Category = "Loka Game | Runtime | Graphics")
struct FdetectedVideoResolutions
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Blueprintable, BlueprintReadOnly, Category = "Loka Game | Runtime | Graphics")
	int32 Width;

	UPROPERTY(Blueprintable, BlueprintReadOnly, Category = "Loka Game | Runtime | Graphics")
	int32 Height;

	UPROPERTY(Blueprintable, BlueprintReadOnly, Category = "Loka Game | Runtime | Graphics")
	int32 RefreshRate;
};

UCLASS(config = GameUserSettings, Blueprintable)
class LOKAGAME_API ULokaGameUserSettings : public UGameUserSettings
{
	friend class SSettingsManager;

	GENERATED_BODY()
	
public:
	ULokaGameUserSettings(const FObjectInitializer& PCIP);
	~ULokaGameUserSettings();

protected:
	UPROPERTY(config)
		bool bIsFirstRun;

	UPROPERTY(config)
		EQualityLevels GeneralQualityLevel;

	UPROPERTY(config)
		float AudioVolume_Master;

	UPROPERTY(config)
		float AudioVolume_SFX;

	UPROPERTY(config)
		float AudioVolume_UI;

	UPROPERTY(config)
		float AudioVolume_Music;

	UPROPERTY(config)
		FString Language;

	UPROPERTY(config)
		bool bShowPing;
	
	UPROPERTY(config)
		bool bShowFPS;

	virtual void SetToDefaults() override;
	
	bool SetSoundClassVolume(FString ClassName, float Volume);
	float GetSoundClassVolume(FString ClassName);

	// Native property
	FScreenResolutionArray ScreenResolutionArray;
	TArray<FCultureRef> SupportedLanguagesArray;

	//																										[FUNCTIONS]
public:
	virtual void LoadSettings(bool bForceReload = false) override;
	
	void ConfirmSettings(const bool _confirm);
	void InitSettings();

	int32 GetCurrentLanguage() const;
	void SetCurrentLanguage(const int32 &_lcid);

	FORCEINLINE bool IsShowPing() const { return bShowPing; }
	FORCEINLINE bool IsShowFPS() const { return bShowFPS; }

	FORCEINLINE void SetShowPing(const bool InShow) { bShowPing = InShow; }
	FORCEINLINE void SetShowFPS(const bool InShow) { bShowFPS = InShow; }

	FORCEINLINE FScreenResolutionArray GetScreenResolutions() const { return ScreenResolutionArray; }

	void initInputSettings();

	FORCEINLINE TArray<FCultureRef>* GetLanguages() { return &SupportedLanguagesArray; }
};
