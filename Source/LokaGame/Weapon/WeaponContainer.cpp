
#include "LokaGame.h"
#include "WeaponContainer.h"

const EWeaponType EWeaponType::Invalid(EShooterWeaponType::None);
const EWeaponType EWeaponType::AssaultRifle(EShooterWeaponType::AssaultRifle);
const EWeaponType EWeaponType::SniperRifle(EShooterWeaponType::SniperRifle);
const EWeaponType EWeaponType::GrenadeLauncher(EShooterWeaponType::GrenadeLauncher);
const EWeaponType EWeaponType::RocketLauncher(EShooterWeaponType::RocketLauncher);
const EWeaponType EWeaponType::Shootgun(EShooterWeaponType::Shootgun);
const EWeaponType EWeaponType::Pistol(EShooterWeaponType::Pistol);
const EWeaponType EWeaponType::LightMachineGun(EShooterWeaponType::LightMachineGun);
const EWeaponType EWeaponType::SubMachineGun(EShooterWeaponType::SubMachineGun);
const EWeaponType EWeaponType::All(	
	EWeaponType::AssaultRifle		.ToFlag()| 
	EWeaponType::SniperRifle		.ToFlag()| 
	EWeaponType::GrenadeLauncher	.ToFlag()| 
	EWeaponType::RocketLauncher		.ToFlag()|
	EWeaponType::Shootgun			.ToFlag()| 
	EWeaponType::Pistol				.ToFlag()| 
	EWeaponType::LightMachineGun	.ToFlag()| 
	EWeaponType::SubMachineGun		.ToFlag()
);

EWeaponType::EWeaponType()
	: Value(0)
{
	
}

EWeaponType::EWeaponType(const int32 InValue)
	: Value(InValue)
{
	
}

EWeaponType::EWeaponType(EShooterWeaponType InValue)
	: Value(1 << InValue)
{
	
}

FText EWeaponType::GetDisplayName() const
{
	if (*this == AssaultRifle)			return NSLOCTEXT("EWeaponType", "EWeaponType.Name.AssaultRifle", "Assault Rifle");
	if (*this == SniperRifle)			return NSLOCTEXT("EWeaponType", "EWeaponType.Name.SniperRifle", "Sniper Rifle");
	if (*this == GrenadeLauncher)		return NSLOCTEXT("EWeaponType", "EWeaponType.Name.GrenadeLauncher", "Grenade Launcher");
	if (*this == RocketLauncher)		return NSLOCTEXT("EWeaponType", "EWeaponType.Name.RocketLauncher", "Rocket Launcher");
	if (*this == Shootgun)				return NSLOCTEXT("EWeaponType", "EWeaponType.Name.Shootgun", "Shootgun");
	if (*this == Pistol)				return NSLOCTEXT("EWeaponType", "EWeaponType.Name.Pistol", "Pistol");
	if (*this == LightMachineGun)		return NSLOCTEXT("EWeaponType", "EWeaponType.Name.LightMachineGun", "Light-Machine Gun");
	if (*this == SubMachineGun)			return NSLOCTEXT("EWeaponType", "EWeaponType.Name.SubMachineGun", "Sub-Machine Gun");
	if (*this == Invalid)				return NSLOCTEXT("EWeaponType", "EWeaponType.Name.End", "None");
	if (*this == All)					return NSLOCTEXT("EWeaponType", "EWeaponType.Name.All", "All");

	return NSLOCTEXT("EWeaponType", "EWeaponType.Name.Mixed", "Mixed");
}

FText EWeaponType::GetDescription() const
{
	if (*this == AssaultRifle)			return NSLOCTEXT("EWeaponType", "EWeaponType.Desc.AssaultRifle", "Assault Rifle");
	if (*this == SniperRifle)			return NSLOCTEXT("EWeaponType", "EWeaponType.Desc.SniperRifle", "Sniper Rifle");
	if (*this == GrenadeLauncher)		return NSLOCTEXT("EWeaponType", "EWeaponType.Desc.GrenadeLauncher", "Grenade Launcher");
	if (*this == RocketLauncher)		return NSLOCTEXT("EWeaponType", "EWeaponType.Desc.RocketLauncher", "Rocket Launcher");
	if (*this == Shootgun)				return NSLOCTEXT("EWeaponType", "EWeaponType.Desc.Shootgun", "Shootgun");
	if (*this == Pistol)				return NSLOCTEXT("EWeaponType", "EWeaponType.Desc.Pistol", "Pistol");
	if (*this == LightMachineGun)		return NSLOCTEXT("EWeaponType", "EWeaponType.Desc.LightMachineGun", "Light-Machine Gun");
	if (*this == SubMachineGun)			return NSLOCTEXT("EWeaponType", "EWeaponType.Desc.SubMachineGun", "Sub-Machine Gun");
	if (*this == All)					return NSLOCTEXT("EWeaponType", "EWeaponType.Name.All", "All");

	return NSLOCTEXT("EWeaponType", "EWeaponType.Name.Mixed", "Mixed");
}

FString EWeaponType::ToString() const
{
	FString OutString;

	for (auto i : ToArray())
	{
		OutString.Append(i.GetDisplayName().ToString() + " | ");
	}

	OutString.RemoveFromEnd(" | ");
	return OutString;
}

int32 EWeaponType::ToFlag() const
{
	return Value;
}

TArray<EWeaponType> EWeaponType::ToArray() const
{
	TArray<EWeaponType> OutArray;

	for (int32 i = 0; i < static_cast<int32>(EShooterWeaponType::End); ++i)
	{
		int32 localValue = static_cast<int32>(Value);
		if (FFlagsHelper::HasAnyFlags<int32>(localValue, FFlagsHelper::ToFlag<int32>(i)))
		{
			OutArray.Add(EWeaponType(static_cast<EShooterWeaponType>(i)));
		}
	}

	return OutArray;
}

bool EWeaponType::IsValid() const
{
	return this->Value != EShooterWeaponType::None;
}

bool EWeaponType::operator==(const EWeaponType& Other) const
{
	return this->Value == Other.Value;
}

bool EWeaponType::operator!=(const EWeaponType& Other) const
{
	return this->Value != Other.Value;
}

EWeaponType& EWeaponType::operator=(int32 InValue)
{
	Value = InValue;
	return *this;
}
