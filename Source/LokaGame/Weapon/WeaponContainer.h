// VRSPRO

#pragma once

#include "WeaponContainer.generated.h"

UENUM(BlueprintType)
namespace EShooterWeaponSlots
{
	enum Type
	{
		Primary,
		Secondary,
		Grenade,
		End
	};
}

typedef EShooterWeaponSlots::Type EShooterWeaponSlot;

UENUM(BlueprintType)
namespace EShooterWeaponTypes
{
	enum Type
	{
		None,
		AssaultRifle,
		SniperRifle,
		GrenadeLauncher,
		RocketLauncher,
		Shootgun,
		Pistol,
		LightMachineGun,
		SubMachineGun,
		End
	};
}

typedef EShooterWeaponTypes::Type EShooterWeaponType;

struct EWeaponType
{

public:
	EWeaponType();
	EWeaponType(const int32 InValue);
	EWeaponType(EShooterWeaponType InValue);

	static const EWeaponType Invalid;
	static const EWeaponType AssaultRifle;
	static const EWeaponType SniperRifle;
	static const EWeaponType GrenadeLauncher;
	static const EWeaponType RocketLauncher;
	static const EWeaponType Shootgun;
	static const EWeaponType Pistol;
	static const EWeaponType LightMachineGun;
	static const EWeaponType SubMachineGun;

	static const EWeaponType All;

	FText GetDisplayName() const;
	FText GetDescription() const;
	FString ToString() const;
	int32 ToFlag() const;
	TArray<EWeaponType> ToArray() const;
	bool IsValid() const;

	bool operator==(const EWeaponType& Other) const;
	bool operator!=(const EWeaponType& Other) const;
	EWeaponType& operator=(int32 InValue);

protected:

	int32 Value;
};

UENUM(BlueprintType)
enum class EEWeaponState : uint8
{
	idle,
	firing,
	throwing,
	reloading,
	equiping,

	unequiping,	// no server
	end,
};

UENUM(BlueprintType)
namespace EAmmoSlot
{
	enum Type
	{
		Primary,
		Secondary,
		End UMETA(Hidden)
	};
}