// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Weapon/WeaponContainer.h"
#include "Weapon/ItemWeaponEntity.h"
#include "ShooterWeapon.generated.h"


class UItemAmmoEntity;
class UPlayerInventoryItemWeapon;
class UItemModuleEntity;
class UItemMaterialEntity;
class UItemModuleScopeOpticalEntity;
class AShooterImpactEffect;

//USTRUCT()
//struct FInstantHitInfo
//{
//	GENERATED_USTRUCT_BODY()
//
//	UPROPERTY()		FVector		Origin;
//	UPROPERTY()		float		ReticleSpread;
//	UPROPERTY()		int32		RandomSeed;
//};

USTRUCT()
struct FInstantShootData
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY()		FHitResult	Impact;
	UPROPERTY()		FVector		ShootDir;
	UPROPERTY()		FVector		TraceEnd;

	FInstantShootData() : Impact(), ShootDir(FVector::ZeroVector), TraceEnd(FVector::ZeroVector) {}
	FInstantShootData(const FHitResult& InImpact, const FVector& InShootDir, const FVector& InTraceEnd) : Impact(InImpact), ShootDir(InShootDir), TraceEnd(InTraceEnd) {}
};

USTRUCT()
struct FFeedbackData
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY()
	FVector2D Feedback;

	UPROPERTY()
	float ModiferCrouched;

	UPROPERTY()
	float ModiferTargeting;
};

UENUM(BlueprintType)
namespace EWeaponState
{
	enum Type
	{
		Idle,
		Firing,
		Reloading,
		Equipping,
	};
}

UENUM(BlueprintType)
namespace ECurrentWeaponUseAs
{
	enum Type
	{
		Instant,
		Shotgun,
		Projectile,
		End
	};
}

UCLASS(Blueprintable)
class AShooterWeapon : public AActor
{
	GENERATED_UCLASS_BODY()

	/** perform initial setup */
	virtual void PostInitializeComponents() override;

	virtual void Destroyed() override;

	//////////////////////////////////////////////////////////////////////////
	// Ammo
	
	/** [server] add ammo */
	void GiveAmmo(int AddAmount);

	/** [server] set ammo */
	void SetAmmo(int AddAmount);

	/** [server] get ammo */
	int32 GetAmmo() const;

	/** consume a bullet */
	void UseAmmo();

	//////////////////////////////////////////////////////////////////////////
	// Inventory

	/** weapon is being equipped by owner pawn */
	virtual void OnEquip();

	/** weapon is now equipped by owner pawn */
	virtual void OnEquipFinished();

	/** weapon is holstered by owner pawn */
	virtual void OnUnEquip();

	/** [server] weapon was added to pawn's inventory */
	virtual void OnEnterInventory(AUTCharacter* NewOwner);

	/** [server] weapon was removed from pawn's inventory */
	virtual void OnLeaveInventory();

	/** check if it's currently equipped */
	bool IsEquipped() const;

	/** check if mesh is already attached */
	bool IsAttachedToPawn() const;


	//////////////////////////////////////////////////////////////////////////
	// Input

	/** [local + server] start weapon fire */
	virtual void StartFire();

	/** [local + server] stop weapon fire */
	virtual void StopFire();

	/** [all] start weapon reload */
	virtual void StartReload(bool bFromReplication = false);

	/** [local + server] interrupt weapon reload */
	virtual void StopReload();

	/** [server] performs actual reload */
	virtual void ReloadWeapon();

	/** trigger reload from server */
	UFUNCTION(reliable, client)
	void ClientStartReload();


	//////////////////////////////////////////////////////////////////////////
	// Control

	/** check if weapon can fire */
	bool CanFire() const;

	/** check if weapon can be reloaded */
	bool CanReload() const;


	//////////////////////////////////////////////////////////////////////////
	// Reading data

	/** get current weapon state */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	EWeaponState::Type GetCurrentState() const;

	/** get current ammo amount (total) */
	int32 GetCurrentAmmo() const;

	/** get current ammo amount (clip) */
	int32 GetCurrentAmmoInClip() const;

	/** get clip size */
	int32 GetAmmoPerClip() const;

	/** get max ammo amount */
	int32 GetMaxAmmo() const;

	/** get weapon mesh (needs pawn owner to determine variant) */
	USkeletalMeshComponent* GetWeaponMesh() const;

	/** get pawn owner */
	UFUNCTION(BlueprintCallable, Category="Game|Weapon")
	class AUTCharacter* GetPawnOwner() const;

	/** Weapon Materials to replace when aiming */
	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	TArray<FWeaponAimingParam> ToReplace;
	
	/** check if weapon has infinite ammo (include owner's cheats) */
	bool HasInfiniteAmmo() const;

	/** check if weapon has infinite clip (include owner's cheats) */
	bool HasInfiniteClip() const;

	/** set the weapon's owning pawn */
	void SetOwningPawn(AUTCharacter* AUTCharacter);

	/** gets last time when this weapon was switched to */
	float GetEquipStartedTime() const;

	/** gets the duration of equipping weapon*/
	float GetEquipDuration() const;

public:

	UFUNCTION()
	virtual void OnRep_Instance();

	UFUNCTION(BlueprintCallable, Category = Inventory)
	void InitializeInstance(UPlayerInventoryItemWeapon* InInstance);

	UPROPERTY(ReplicatedUsing=OnRep_Instance)
	UPlayerInventoryItemWeapon* Instance;
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = GamePlay)
	UPlayerInventoryItemWeapon* GetInstance() const
	{
		return Instance;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = GamePlay)
	UItemWeaponEntity* GetInstanceEntity() const;

	UFUNCTION(BlueprintCallable, Category = GamePlay)
	FWeaponPropertyBase GetWeaponProperty() const;

	UFUNCTION(BlueprintCallable, Category = GamePlay)
	FWeaponConfiguration GetWeaponConfiguration() const;

	/** pawn owner */
	UPROPERTY(Transient, ReplicatedUsing=OnRep_MyPawn)
	class AUTCharacter* MyPawn;



private:
	/** weapon mesh: 1st person view */
	UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
	USkeletalMeshComponent* Mesh1P;

	/** weapon mesh: 3rd person view */
	UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
	USkeletalMeshComponent* Mesh3P;
protected:

	/** firing audio (bLoopedFireSound set) */
	UPROPERTY(Transient)
	UAudioComponent* FireAC;


	/** spawned component for muzzle FX */
	UPROPERTY(Transient)
	UParticleSystemComponent* MuzzlePSC;

	/** spawned component for second muzzle FX (Needed for split screen) */
	UPROPERTY(Transient)
	UParticleSystemComponent* MuzzlePSCSecondary;



	/** is fire animation playing? */
	uint32 bPlayingFireAnim : 1;

	/** is weapon currently equipped? */
	uint32 bIsEquipped : 1;

	/** is weapon fire active? */
	uint32 bWantsToFire : 1;

	/** is reload animation playing? */
	UPROPERTY(Transient, ReplicatedUsing=OnRep_Reload)
	uint32 bPendingReload : 1;

	/** is equip animation playing? */
	uint32 bPendingEquip : 1;

	/** weapon is refiring */
	uint32 bRefiring;

	/** current weapon state */
	EWeaponState::Type CurrentState;

	/** time of last successful weapon fire */
	float LastFireTime;

	/** last time when this weapon was switched to */
	float EquipStartedTime;

	/** how much time weapon needs to be equipped */
	float EquipDuration;

	/** current ammo - inside clip */
	UPROPERTY(Transient, ReplicatedUsing = OnRep_CountAmmo)
	int32 CurrentAmmoInClip;

	/** burst counter, used for replicating fire events to remote clients */
	UPROPERTY(Transient, ReplicatedUsing=OnRep_BurstCounter)
	int32 BurstCounter;

	/** Handle for efficient management of OnEquipFinished timer */
	FTimerHandle TimerHandle_OnEquipFinished;

	/** Handle for efficient management of StopReload timer */
	FTimerHandle TimerHandle_StopReload;

	/** Handle for efficient management of ReloadWeapon timer */
	FTimerHandle TimerHandle_ReloadWeapon;

	/** Handle for efficient management of HandleFiring timer */
	FTimerHandle TimerHandle_HandleFiring;

	//////////////////////////////////////////////////////////////////////////
	// Input - server side

	UFUNCTION(reliable, server, WithValidation)
	void ServerStartFire();

	UFUNCTION(reliable, server, WithValidation)
	void ServerStopFire();

	UFUNCTION(reliable, server, WithValidation)
	void ServerStartReload();

	UFUNCTION(reliable, server, WithValidation)
	void ServerStopReload();


	//////////////////////////////////////////////////////////////////////////
	// Replication & effects

	UFUNCTION()
	void OnRep_MyPawn();

	UFUNCTION()
	void OnRep_BurstCounter();

	UFUNCTION()
	void OnRep_Reload();

	/** Called in network play to do the cosmetic fx for firing */
	virtual void SimulateWeaponFire();

	/** Called in network play to stop cosmetic fx (e.g. for a looping shot). */
	virtual void StopSimulatingWeaponFire();


	//////////////////////////////////////////////////////////////////////////
	// Weapon usage

	/** [local] weapon specific fire implementation */
	virtual void FireWeapon();

	/** [server] fire & update ammo */
	UFUNCTION(reliable, server, WithValidation)
	void ServerHandleFiring();

	/** [local + server] handle weapon fire */
	void HandleFiring();

	/** [local + server] firing started */
	virtual void OnBurstStarted();

	/** [local + server] firing finished */
	virtual void OnBurstFinished();

	/** update weapon state */
	void SetWeaponState(EWeaponState::Type NewState);

	/** determine current weapon state */
	void DetermineWeaponState();


	//////////////////////////////////////////////////////////////////////////
	// Inventory

	/** attaches weapon mesh to pawn's mesh */
	void AttachMeshToPawn();

	/** detaches weapon mesh from pawn */
	void DetachMeshFromPawn();


	//////////////////////////////////////////////////////////////////////////
	// Weapon usage helpers
public:

	/** play weapon sounds */
	UAudioComponent* PlayWeaponSound(USoundCue* Sound);

	/** play weapon animations */
	float PlayWeaponAnimation(const FWeaponAnim& Animation);

	/** stop playing weapon animations */
	void StopWeaponAnimation(const FWeaponAnim& Animation);

	/** Get the aim of the weapon, allowing for adjustments to be made by the weapon */
	virtual FVector GetAdjustedAim() const;

	/** Get the aim of the camera */
	FVector GetCameraAim() const;

	/** get the originating location for camera damage */
	FVector GetCameraDamageStartLocation(const FVector& AimDir) const;

	/** get the muzzle location of the weapon */
	FVector GetMuzzleLocation() const;

	/** get direction of weapon's muzzle */
	FVector GetMuzzleDirection() const;

	/** find hit */
	FHitResult WeaponTrace(const FVector& TraceFrom, const FVector& TraceTo) const;

	void ToggleAimingParams(const bool IsAiming);

	void ToggleSniperScope(const bool);

public:
	/** Returns Mesh1P subobject **/
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Mesh)
	USkeletalMeshComponent* GetMesh1P() const;

	/** Returns Mesh3P subobject **/
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Mesh)
	USkeletalMeshComponent* GetMesh3P() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Mesh)
	float GetWeaponMass() const;

	virtual void ApplyOffsetIfAI(FVector &Origin);
	virtual float GetWeaponCoefficient() const;

	//Ammo Usage

	UPROPERTY(Transient, Replicated)
	TEnumAsByte<EAmmoSlot::Type> CurrentAmmo;

	UFUNCTION()
	void OnRep_CountAmmo();

	UPROPERTY(Transient, ReplicatedUsing=OnRep_CountAmmo)
	int32 CountAmmo[EAmmoSlot::End];

	UPROPERTY()
	UItemAmmoEntity* AmmoInstance[EAmmoSlot::End];

	UFUNCTION()
	void SetCurrentAmmo(const TEnumAsByte<EAmmoSlot::Type> Ammo);


	UFUNCTION(reliable, server, WithValidation)
	void ServerSetCurrentAmmo(const EAmmoSlot::Type Ammo);


	UItemAmmoEntity* GetCurrentAmmoInstance() const;

	UPROPERTY(BlueprintReadOnly, Category = Gameplay)
	UItemModuleScopeOpticalEntity* ScopeModule;

	UPROPERTY(BlueprintReadOnly, Category = Gameplay)
	TArray<UMaterialInstanceDynamic*> ScopeModuleMIDS;

	UPROPERTY()
	TArray<USkeletalMeshComponent*> InstalledModulesMesh1P;

	UPROPERTY()
	TArray<USkeletalMeshComponent*> InstalledModulesMesh3P;

	UPROPERTY()
	TMap<FName, USkeletalMeshComponent*> InstalledModulesMeshMap;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Weapon)
	UItemModuleEntity* FindInstalledModuleEntity(const UClass* InTargetClass, int32& OutMeshIndex) const;

	void PlayMontageWithDuration(USkeletalMeshComponent* PlayTarget, UAnimMontage* Montage, float Duration);

protected:

	virtual void TickActor(float DeltaTime, enum ELevelTick TickType, FActorTickFunction& ThisTickFunction) override;

	UPROPERTY()
	float TargetRecoilX;

	UPROPERTY()
	float TargetRecoilY;

	UPROPERTY(Replicated)
	TArray<FFeedbackData> FeedbackData;

	UPROPERTY(VisibleAnywhere, Category = Component)
	USceneCaptureComponent2D* CaptureComp;

	void ProccesFeedback(float DeltaTime);
	void MakeFeedback();

	UPROPERTY()
	bool bIsCancelReload;

public:

	//====================================================] UT

	virtual bool DoAssistedJump()
	{
		return false;
	}

	virtual void UpdateViewBob(float DeltaTime);

	/** indicates this weapon is most useful in melee range (used by AI) */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = AI)
	bool IsMeleeWeapon() const;

	/** indicates AI should prioritize accuracy over evasion (low skill bots will stop moving, higher skill bots prioritize strafing and avoid actions that move enemy across view */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = AI)
	bool IsPrioritizeAccuracy() const;

	/** indicates AI should target for splash damage (e.g. shoot at feet or nearby walls) */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = AI)
	bool IsRecommendSplashDamage() const;

	/** indicates AI should consider firing at enemies that aren't currently visible but are close to being so
	  * generally set for rapid fire weapons or weapons with spinup/spindown costs */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = AI)
	bool IsRecommendSuppressiveFire() const;

	/** indicates this is a sniping weapon (for AI, will prioritize headshots and long range targeting) */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = AI)
	bool IsSniping() const;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
		float BaseAISelectRating;
	/** AI switches to the weapon that returns the highest rating */
	UFUNCTION(BlueprintNativeEvent, Category = AI)
		float GetAISelectRating();

	UFUNCTION(BlueprintNativeEvent, Category = AI)
		bool CanAttack(AActor* Target, const FVector& TargetLoc, bool bDirectOnly, UPARAM(ref) FVector& OptimalTargetLoc);

	inline bool CanAttack(AActor* Target, const FVector& TargetLoc, bool bDirectOnly)
	{
		FVector UnusedOptimalLoc;
		return CanAttack(Target, TargetLoc, bDirectOnly, UnusedOptimalLoc);
	}

	UFUNCTION(BlueprintNativeEvent, Category = AI)
		float SuggestAttackStyle();
	/** return a value from -1 to 1 for suggested method of defense for AI when fighting a player with this weapon, where < 0 indicates back off and fire from afar while > 0 indicates AI should advance/charge */
	UFUNCTION(BlueprintNativeEvent, Category = AI)
		float SuggestDefenseStyle();

	FVector FirstPMeshOffset;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Animation)
	FTransform GetAimOffsetTransform() const;

	//====================================================] Multi-types in-one

protected:

	// Determine use as for network (init on start fire from ammo)
	UPROPERTY(Replicated)
	TEnumAsByte<ECurrentWeaponUseAs::Type> WeaponUseAs;

	void FireWeapon_Instant();
	void FireWeapon_Shotgun();
	void FireWeapon_Projectile();

	//====================================================] Shotgun & Instant
	UFUNCTION(reliable, server, WithValidation)
	void ServerNotifyInstantShoot(const TArray<FInstantShootData>& InShootData);

	void ProcessInstantHit(const TArray<FInstantShootData>& InShootData);
	void ProcessInstantHit_Confirmed(const TArray<FInstantShootData>& InShootData);

public:

	bool ShouldDealDamage(AActor* TestActor) const;

protected:

	void DealDamage(const TArray<FInstantShootData>& InShootData);

	UFUNCTION()
	void OnRep_ShootNotifyInstant();

	UPROPERTY(Transient, ReplicatedUsing = OnRep_ShootNotifyInstant)
	TArray<FInstantShootData> ShootNotifyInstant;

	float CurrentFiringSpread;

public:

	float GetCurrentSpread() const;

protected:

	void SimulateInstantHit(const FVector& Origin, int32 RandomSeed, float ReticleSpread);
	void SpawnImpactEffects(const FHitResult& Impact);
	void SpawnTrailEffect(const FVector& EndPoint);

	UFUNCTION(reliable, server, WithValidation)
	void ServerFireProjectile(FVector Origin, FVector_NetQuantizeNormal ShootDir);

public:

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FName GetMuzzleSocketName() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	USceneComponent* GetMuzzleSocketComponent(const bool IsFirstView = true) const;

	UFUNCTION(BlueprintCallable, Category = Cosmetic)
	void SetTempMaterial(UMaterialInterface* InMaterial);
};

