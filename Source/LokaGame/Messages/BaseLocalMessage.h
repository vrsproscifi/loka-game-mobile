// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/LocalMessage.h"
#include "BaseLocalMessage.generated.h"

struct FResourceTextBoxValue;
class ABasePlayerState;
class ABasePlayerController;

FArchive& operator<<(FArchive& Ar, FResourceTextBoxValue& Value);

USTRUCT(Blueprintable)
struct FVariantMessageData
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(BlueprintReadOnly)
	FString ValueType;

	UPROPERTY(BlueprintReadOnly)
	TArray<uint8> ValueData;

	FText GetDataAsText(const bool IsSupportRich = true);

};

USTRUCT(Blueprintable)
struct FBaseLocalMessageData
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(BlueprintReadOnly)
	int32 Index;

	UPROPERTY(BlueprintReadOnly)
	ABasePlayerController* LocalController;

	UPROPERTY(BlueprintReadOnly)
	ABasePlayerState* PlayerStateOne;

	UPROPERTY(BlueprintReadOnly)
	ABasePlayerState* PlayerStateTwo;

	UPROPERTY(BlueprintReadOnly, NotReplicated)
	TMap<FString, FVariantMessageData> OptionalData;

	UPROPERTY(BlueprintReadOnly)
	UObject* OptionalObject;

	UPROPERTY()
	TArray<FString> OptionalDataKeys;

	UPROPERTY()
	TArray<FVariantMessageData> OptionalDataValues;

	FBaseLocalMessageData() : Index(INDEX_NONE), LocalController(nullptr), PlayerStateOne(nullptr), PlayerStateTwo(nullptr), OptionalData(), OptionalObject(nullptr) {}
	FBaseLocalMessageData(const FClientReceiveData& ClientData);

	template<typename TTarget>
	FBaseLocalMessageData& AddOptionalData(const FString& InKey, TTarget InData)
	{
		if (OptionalData.Contains(InKey) == false)
		{
			FVariantMessageData Data;
			FBufferArchive Buffer;
			Buffer << InData;
			Data.ValueData = static_cast<TArray<uint8>>(Buffer);
			Data.ValueType = TNameOf<TTarget>().GetName();
			OptionalData.Add(InKey, Data);
			ReInitOptionalData();
		}

		return *this;
	}

	void ReInitOptionalData();
};


UCLASS(Blueprintable, Abstract, NotPlaceable, meta = (ShowWorldContextPin))
class LOKAGAME_API UBaseLocalMessage : public ULocalMessage
{
	GENERATED_BODY()
	
public:

	virtual void ClientReceive(const FClientReceiveData& ClientData) const override;

	UFUNCTION(BlueprintNativeEvent, Category = Message)
	void OnReceive(const FBaseLocalMessageData& MessageData) const;

protected:

	UPROPERTY(EditDefaultsOnly, Category = Message)
	FText Message;
};
