// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "BaseLocalMessage.h"

#include "PlayerController/BasePlayerController.h"
#include "GameState/BasePlayerState.h"
#include "SResourceTextBoxValue.h"
#include "RichTextHelpers.h"

FArchive& operator<<(FArchive& Ar, FResourceTextBoxValue& Value)
{
	uint8 u8v = Value.Type;
	Ar << u8v;
	Ar << Value.Value;
	Value.Type = static_cast<EResourceTextBoxType::Type>(u8v);
	return Ar;
}

FText FVariantMessageData::GetDataAsText(const bool IsSupportRich)
{
	FText ReturnText;
	if (IsSupportRich)
	{
		if (ValueType.Equals(TNameOf<int8>().GetName(), ESearchCase::IgnoreCase))
		{
			int8 Value;
			FMemoryReader Reader(ValueData, true); Reader << Value;
			ReturnText = FRichHelpers::TextHelper_NotifyValue.SetValue(FText::AsNumber(Value)).ToText();
		}
		else if (ValueType.Equals(TNameOf<uint8>().GetName(), ESearchCase::IgnoreCase))
		{
			uint8 Value;
			FMemoryReader Reader(ValueData, true); Reader << Value;
			ReturnText = FRichHelpers::TextHelper_NotifyValue.SetValue(FText::AsNumber(Value)).ToText();
		}
		else if (ValueType.Equals(TNameOf<int16>().GetName(), ESearchCase::IgnoreCase))
		{
			int16 Value;
			FMemoryReader Reader(ValueData, true); Reader << Value;
			ReturnText = FRichHelpers::TextHelper_NotifyValue.SetValue(FText::AsNumber(Value)).ToText();
		}
		else if (ValueType.Equals(TNameOf<uint16>().GetName(), ESearchCase::IgnoreCase))
		{
			uint16 Value;
			FMemoryReader Reader(ValueData, true); Reader << Value;
			ReturnText = FRichHelpers::TextHelper_NotifyValue.SetValue(FText::AsNumber(Value)).ToText();
		}
		else if (ValueType.Equals(TNameOf<int32>().GetName(), ESearchCase::IgnoreCase))
		{
			int32 Value;
			FMemoryReader Reader(ValueData, true); Reader << Value;
			ReturnText = FRichHelpers::TextHelper_NotifyValue.SetValue(FText::AsNumber(Value)).ToText();
		}
		else if (ValueType.Equals(TNameOf<uint32>().GetName(), ESearchCase::IgnoreCase))
		{
			uint32 Value;
			FMemoryReader Reader(ValueData, true); Reader << Value;
			ReturnText = FRichHelpers::TextHelper_NotifyValue.SetValue(FText::AsNumber(Value)).ToText();
		}
		else if (ValueType.Equals(TNameOf<int64>().GetName(), ESearchCase::IgnoreCase))
		{
			int64 Value;
			FMemoryReader Reader(ValueData, true); Reader << Value;
			ReturnText = FRichHelpers::TextHelper_NotifyValue.SetValue(FText::AsNumber(Value)).ToText();
		}
		else if (ValueType.Equals(TNameOf<uint64>().GetName(), ESearchCase::IgnoreCase))
		{
			uint64 Value;
			FMemoryReader Reader(ValueData, true); Reader << Value;
			ReturnText = FRichHelpers::TextHelper_NotifyValue.SetValue(FText::AsNumber(Value)).ToText();
		}
		else if (ValueType.Equals(TNameOf<float>().GetName(), ESearchCase::IgnoreCase))
		{
			float Value;
			FMemoryReader Reader(ValueData, true); Reader << Value;
			ReturnText = FRichHelpers::TextHelper_NotifyValue.SetValue(FText::AsNumber(Value)).ToText();
		}
		else if (ValueType.Equals(TNameOf<double>().GetName(), ESearchCase::IgnoreCase))
		{
			double Value;
			FMemoryReader Reader(ValueData, true); Reader << Value;
			ReturnText = FRichHelpers::TextHelper_NotifyValue.SetValue(FText::AsNumber(Value)).ToText();
		}
		else if (ValueType.Equals(TNameOf<FResourceTextBoxValue>().GetName(), ESearchCase::IgnoreCase))
		{
			FResourceTextBoxValue Value;
			FMemoryReader Reader(ValueData, true); Reader << Value;
			ReturnText = FRichHelpers::FResourceHelper().SetValue(Value.Value).SetType(Value.Type).ToText();
		}
		else if (ValueType.Equals(TNameOf<FString>().GetName(), ESearchCase::IgnoreCase))
		{
			FString Value;
			FMemoryReader Reader(ValueData, true); Reader << Value;
			ReturnText = FRichHelpers::TextHelper_NotifyValue.SetValue(Value).ToText();
		}
		else if (ValueType.Equals(TNameOf<FText>().GetName(), ESearchCase::IgnoreCase))
		{
			FText Value;
			FMemoryReader Reader(ValueData, true); Reader << Value;
			ReturnText = FRichHelpers::TextHelper_NotifyValue.SetValue(Value).ToText();
		}
	}

	return ReturnText;
}

FBaseLocalMessageData::FBaseLocalMessageData(const FClientReceiveData& ClientData)
{
	this->Index = ClientData.MessageIndex;
	this->LocalController = Cast<ABasePlayerController>(ClientData.LocalPC);
	this->PlayerStateOne = Cast<ABasePlayerState>(ClientData.RelatedPlayerState_1);
	this->PlayerStateTwo = Cast<ABasePlayerState>(ClientData.RelatedPlayerState_2);
	this->OptionalObject = ClientData.OptionalObject;
}

void FBaseLocalMessageData::ReInitOptionalData()
{
	if (OptionalData.Num())
	{
		OptionalDataKeys.Empty();
		OptionalDataValues.Empty();

		OptionalData.GenerateKeyArray(OptionalDataKeys);
		OptionalData.GenerateValueArray(OptionalDataValues);
	}
	else
	{
		for (int32 i = 0; i < OptionalDataKeys.Num(); ++i)
		{
			OptionalData.Add(OptionalDataKeys[i], OptionalDataValues[i]);
		}

		OptionalDataKeys.Empty();
		OptionalDataValues.Empty();
	}
}

void UBaseLocalMessage::ClientReceive(const FClientReceiveData& ClientData) const
{
	OnReceive(FBaseLocalMessageData(ClientData));
}

void UBaseLocalMessage::OnReceive_Implementation(const FBaseLocalMessageData& MessageData) const
{
	
}