// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "LokaGame.h"
#include "LokaGameClasses.h"
#include "LokaStyle.h"
#include "AssetRegistryModule.h"
#include "IAssetRegistry.h"

#include "Online.h"
#include "ARFilter.h"


#include "Entity/Item/CategoryTypeId.h"
#include "Entity/Weapon/ItemWeaponModelId.h"
#include "Entity/Grenade/ItemGrenadeModelId.h"
#include "GameSingleton.h"
#include "UTGameViewportClient.h"
#include "GameState/ShooterPlayerStatistics.h"
#include "Localization/BaseStringsTable.h"

IMPLEMENT_PRIMARY_GAME_MODULE(FLokaGameModule, LokaGame, "LokaGame") ;

UUTGameViewportClient* GetGameViewportClient()
{
	if (GEngine && GEngine->IsValidLowLevel())
	{
		if (GEngine->GameViewport && GEngine->GameViewport->IsValidLowLevel())
		{
			if (auto v = Cast<UUTGameViewportClient>(GEngine->GameViewport))
			{
				return v;
			}
		}
	}
	return nullptr;
}

void FLokaGameModule::StartupModule()
{
	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(TEXT("AssetRegistry"));
	//Hot reload hack
	FSlateStyleRegistry::UnRegisterSlateStyle(FLokaStyle::GetStyleSetName());
	FLokaStyle::Initialize();
	//Hot reload hack
	//===================================================================================
	FSlateStyleRegistry::UnRegisterSlateStyle(FSlateStyleManager::GetStyleSetName());
	FSlateStyleManager::Initialize();
	FWeaponHelper::KillsAchievementsList.Empty();
	FWeaponHelper::SeriesKillAchievementsList.Empty();
	//============================================================================================================================================================================================[ Assault Rifle Kills
	FWeaponHelper::KillsAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::MilitaryDark_AssaultRifle), ItemAchievementTypeId::Kill_By_Rifle);
	FWeaponHelper::KillsAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::MilitarySilver_AssaultRifle), ItemAchievementTypeId::Kill_By_Rifle);
	FWeaponHelper::KillsAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::ScifiDark_AssaultRifle), ItemAchievementTypeId::Kill_By_Rifle);
	FWeaponHelper::KillsAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::ScifiWhite_AssaultRifle), ItemAchievementTypeId::Kill_By_Rifle);
	FWeaponHelper::KillsAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::LOKA_SunGazer), ItemAchievementTypeId::Kill_By_Rifle);
	//============================================================================================================================================================================================[ Assault Rifle Series kill
	FWeaponHelper::SeriesKillAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::MilitaryDark_AssaultRifle), ItemAchievementTypeId::SeriesKill_By_Rifle);
	FWeaponHelper::SeriesKillAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::MilitarySilver_AssaultRifle), ItemAchievementTypeId::SeriesKill_By_Rifle);
	FWeaponHelper::SeriesKillAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::ScifiDark_AssaultRifle), ItemAchievementTypeId::SeriesKill_By_Rifle);
	FWeaponHelper::SeriesKillAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::ScifiWhite_AssaultRifle), ItemAchievementTypeId::SeriesKill_By_Rifle);
	FWeaponHelper::SeriesKillAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::LOKA_SunGazer), ItemAchievementTypeId::SeriesKill_By_Rifle);
	//============================================================================================================================================================================================[ Sniper Rifle Kills
	FWeaponHelper::KillsAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::MilitaryDark_SniperRifle), ItemAchievementTypeId::Kill_By_SniperRifle);
	FWeaponHelper::KillsAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::MilitarySilver_SniperRifle), ItemAchievementTypeId::Kill_By_SniperRifle);
	FWeaponHelper::KillsAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::ScifiDark_SniperRifle), ItemAchievementTypeId::Kill_By_SniperRifle);
	FWeaponHelper::KillsAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::ScifiWhite_SniperRifle), ItemAchievementTypeId::Kill_By_SniperRifle);
	FWeaponHelper::KillsAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::LOKA_DTASRS), ItemAchievementTypeId::Kill_By_SniperRifle);
	//============================================================================================================================================================================================[ Sniper Rifle Series kill
	FWeaponHelper::SeriesKillAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::MilitaryDark_SniperRifle), ItemAchievementTypeId::SeriesKill_By_SniperRifle);
	FWeaponHelper::SeriesKillAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::MilitarySilver_SniperRifle), ItemAchievementTypeId::SeriesKill_By_SniperRifle);
	FWeaponHelper::SeriesKillAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::ScifiDark_SniperRifle), ItemAchievementTypeId::SeriesKill_By_SniperRifle);
	FWeaponHelper::SeriesKillAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::ScifiWhite_SniperRifle), ItemAchievementTypeId::SeriesKill_By_SniperRifle);
	FWeaponHelper::SeriesKillAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::LOKA_DTASRS), ItemAchievementTypeId::SeriesKill_By_SniperRifle);
	//============================================================================================================================================================================================[ Shotgun Kills
	FWeaponHelper::KillsAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::MilitaryDark_Shotgun), ItemAchievementTypeId::Kill_By_ShotGun);
	FWeaponHelper::KillsAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::MilitarySilver_Shotgun), ItemAchievementTypeId::Kill_By_ShotGun);
	FWeaponHelper::KillsAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::ScifiDark_Shotgun), ItemAchievementTypeId::Kill_By_ShotGun);
	FWeaponHelper::KillsAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::ScifiWhite_Shotgun), ItemAchievementTypeId::Kill_By_ShotGun);
	FWeaponHelper::KillsAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::LOKA_BenelliM4), ItemAchievementTypeId::Kill_By_ShotGun);
	//============================================================================================================================================================================================[ Shotgun Series kill
	FWeaponHelper::SeriesKillAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::MilitaryDark_Shotgun), ItemAchievementTypeId::SeriesKill_By_ShotGun);
	FWeaponHelper::SeriesKillAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::MilitarySilver_Shotgun), ItemAchievementTypeId::SeriesKill_By_ShotGun);
	FWeaponHelper::SeriesKillAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::ScifiDark_Shotgun), ItemAchievementTypeId::SeriesKill_By_ShotGun);
	FWeaponHelper::SeriesKillAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::ScifiWhite_Shotgun), ItemAchievementTypeId::SeriesKill_By_ShotGun);
	FWeaponHelper::SeriesKillAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::LOKA_BenelliM4), ItemAchievementTypeId::SeriesKill_By_ShotGun);
	//============================================================================================================================================================================================[ Pistol Kills
	FWeaponHelper::KillsAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::MilitaryDark_Pistol), ItemAchievementTypeId::Kill_By_Pistol);
	FWeaponHelper::KillsAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::MilitarySilver_Pistol), ItemAchievementTypeId::Kill_By_Pistol);
	FWeaponHelper::KillsAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::ScifiDark_Pistol), ItemAchievementTypeId::Kill_By_Pistol);
	FWeaponHelper::KillsAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::ScifiWhite_Pistol), ItemAchievementTypeId::Kill_By_Pistol);
	FWeaponHelper::KillsAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::LOKA_MasterTrust), ItemAchievementTypeId::Kill_By_Pistol);
	//============================================================================================================================================================================================[ Pistol Series kill
	FWeaponHelper::SeriesKillAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::MilitaryDark_Pistol), ItemAchievementTypeId::SeriesKill_By_Pistol);
	FWeaponHelper::SeriesKillAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::MilitarySilver_Pistol), ItemAchievementTypeId::SeriesKill_By_Pistol);
	FWeaponHelper::SeriesKillAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::ScifiDark_Pistol), ItemAchievementTypeId::SeriesKill_By_Pistol);
	FWeaponHelper::SeriesKillAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::ScifiWhite_Pistol), ItemAchievementTypeId::SeriesKill_By_Pistol);
	FWeaponHelper::SeriesKillAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::LOKA_MasterTrust), ItemAchievementTypeId::SeriesKill_By_Pistol);
	//============================================================================================================================================================================================[ Grenade Kills
	FWeaponHelper::KillsAchievementsList.Add(FWeaponHelper(CategoryTypeId::Grenade, ItemGrenadeModelId::Explosive), ItemAchievementTypeId::Kill_By_Grenade);
	FWeaponHelper::KillsAchievementsList.Add(FWeaponHelper(CategoryTypeId::Grenade, ItemGrenadeModelId::Precision), ItemAchievementTypeId::Kill_By_Grenade);
	//============================================================================================================================================================================================[ Grenade Series kill
	FWeaponHelper::SeriesKillAchievementsList.Add(FWeaponHelper(CategoryTypeId::Grenade, ItemGrenadeModelId::Explosive), ItemAchievementTypeId::SeriesKill_By_Grenade);
	FWeaponHelper::SeriesKillAchievementsList.Add(FWeaponHelper(CategoryTypeId::Grenade, ItemGrenadeModelId::Precision), ItemAchievementTypeId::SeriesKill_By_Grenade);
	//============================================================================================================================================================================================[ Light Machine Gun Kills
	FWeaponHelper::KillsAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::LOKA_Stoner96), ItemAchievementTypeId::Kill_By_LightMachineGun);
	//============================================================================================================================================================================================[ Light Machine Gun Series kill
	FWeaponHelper::SeriesKillAchievementsList.Add(FWeaponHelper(CategoryTypeId::Weapon, ItemWeaponModelId::LOKA_Stoner96), ItemAchievementTypeId::SeriesKill_By_LightMachineGun);
#if !UE_SERVER
	FLokaBaseStringsTable::Instance = MakeShareable(new FLokaBaseStringsTable());
#endif
}

void FLokaGameModule::ShutdownModule()
{
#if !UE_SERVER
	FLokaBaseStringsTable::Instance.Reset();
#endif
	FLokaStyle::Shutdown();
	FSlateStyleManager::Shutdown();
}

DEFINE_LOG_CATEGORY(LogShooter)
DEFINE_LOG_CATEGORY(LogShooterWeapon)
DEFINE_LOG_CATEGORY(LogEntityItem)
DEFINE_LOG_CATEGORY(LogEntityPlayer)
DEFINE_LOG_CATEGORY(LogBuildSystem)

#include "Runtime/PakFile/Public/IPlatformFilePak.h"

#if WITH_PROFILE
#include "OnlineSubsystemMcp.h"
#include "GameServiceMcp.h"
#endif

DEFINE_LOG_CATEGORY(UT);
DEFINE_LOG_CATEGORY(UTNet);
DEFINE_LOG_CATEGORY(UTLoading);

FCollisionResponseParams WorldResponseParams = []()
{
	FCollisionResponseParams Result(ECR_Ignore);
	Result.CollisionResponse.WorldStatic = ECR_Block;
	Result.CollisionResponse.WorldDynamic = ECR_Block;
	return Result;
}();

#include "Particles/ParticleSystemComponent.h"
#include "Particles/ParticleModuleRequired.h"
#include "Particles/ParticleLODLevel.h"

bool IsLoopingParticleSystem(const UParticleSystem* PSys)
{
	for (int32 i = 0; i < PSys->Emitters.Num(); i++)
	{
		if (PSys->Emitters[i]->GetLODLevel(0)->RequiredModule->EmitterLoops <= 0 && PSys->Emitters[i]->GetLODLevel(0)->RequiredModule->bEnabled)
		{
			return true;
		}
	}
	return false;
}

void UnregisterComponentTree(USceneComponent* Comp)
{
	if (Comp != nullptr)
	{
		TArray<USceneComponent*> Children;
		Comp->GetChildrenComponents(true, Children);
		Comp->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
		Comp->UnregisterComponent();
		for (USceneComponent* Child : Children)
		{
			Child->UnregisterComponent();
		}
	}
}

APhysicsVolume* FindPhysicsVolume(UWorld* World, const FVector& TestLoc, const FCollisionShape& Shape)
{
	APhysicsVolume* NewVolume = World->GetDefaultPhysicsVolume();
	// check for all volumes that overlap the component
	TArray<FOverlapResult> Hits;
	static FName NAME_PhysicsVolumeTrace = FName(TEXT("PhysicsVolumeTrace"));
	FComponentQueryParams Params(NAME_PhysicsVolumeTrace, NULL);
	World->OverlapMultiByChannel(Hits, TestLoc, FQuat::Identity, ECC_Pawn, Shape, Params);
	for (int32 HitIdx = 0; HitIdx < Hits.Num(); HitIdx++)
	{
		const FOverlapResult& Link = Hits[HitIdx];
		APhysicsVolume* const V = Cast<APhysicsVolume>(Link.GetActor());
		if (V != nullptr && V->Priority > NewVolume->Priority && (V->bPhysicsOnContact || V->EncompassesPoint(TestLoc)))
		{
			NewVolume = V;
		}
	}
	return NewVolume;
}

float GetLocationGravityZ(UWorld* World, const FVector& TestLoc, const FCollisionShape& Shape)
{
	APhysicsVolume* Volume = FindPhysicsVolume(World, TestLoc, Shape);
	return (Volume != nullptr) ? Volume->GetGravityZ() : World->GetDefaultGravityZ();
}

UMeshComponent* CreateCustomDepthOutlineMesh(UMeshComponent* Archetype, AActor* Owner)
{
	UMeshComponent* CustomDepthMesh = DuplicateObject<UMeshComponent>(Archetype, Owner);
	CustomDepthMesh->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
	CustomDepthMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision); // make sure because could be in ragdoll
	CustomDepthMesh->SetSimulatePhysics(false);
	CustomDepthMesh->SetCastShadow(false);
	if (Cast<USkinnedMeshComponent>(CustomDepthMesh) != nullptr)
	{
		((USkinnedMeshComponent*)CustomDepthMesh)->SetMasterPoseComponent((USkinnedMeshComponent*)Archetype);
	}
	for (int32 i = 0; i < CustomDepthMesh->GetNumMaterials(); i++)
	{
		CustomDepthMesh->SetMaterial(i, UMaterial::GetDefaultMaterial(MD_Surface));
	}
	CustomDepthMesh->BoundsScale = 15000.f;
	CustomDepthMesh->bVisible = true;
	CustomDepthMesh->bHiddenInGame = false;
	CustomDepthMesh->bRenderInMainPass = false;
	CustomDepthMesh->bRenderCustomDepth = true;
	CustomDepthMesh->AttachToComponent(Archetype, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	CustomDepthMesh->RelativeLocation = FVector::ZeroVector;
	CustomDepthMesh->RelativeRotation = FRotator::ZeroRotator;
	CustomDepthMesh->RelativeScale3D = FVector(1.0f, 1.0f, 1.0f);
	return CustomDepthMesh;
}

void GetAllAssetData(UClass* BaseClass, TArray<FAssetData>& AssetList, bool bRequireEntitlements)
{
	// calling this with UBlueprint::StaticClass() is probably a bug where the user intended to call GetAllBlueprintAssetData()
	if (BaseClass == UBlueprint::StaticClass())
	{
#if DO_GUARD_SLOW
		UE_LOG(UT, Fatal, TEXT("GetAllAssetData() should not be used for blueprints; call GetAllBlueprintAssetData() instead"));
#else
		UE_LOG(UT, Error, TEXT("GetAllAssetData() should not be used for blueprints; call GetAllBlueprintAssetData() instead"));
#endif
		return;
	}
	// force disable local entitlement checks on dedicated server
	bRequireEntitlements = bRequireEntitlements && !IsRunningDedicatedServer();
	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(TEXT("AssetRegistry"));
	IAssetRegistry& AssetRegistry = AssetRegistryModule.Get();
	AssetRegistry.ScanPathsSynchronous({
		TEXT("/Game/1LOKAgame/Blueprints/DataAssets"),
	});
	FARFilter ARFilter;
	if (BaseClass != nullptr)
	{
		ARFilter.ClassNames.Add(BaseClass->GetFName());
		// Add any old names to the list in case things haven't been resaved
		TArray<FName> OldNames = FLinkerLoad::FindPreviousNamesForClass(BaseClass->GetPathName(), false);
		ARFilter.ClassNames.Append(OldNames);
	}
	ARFilter.bRecursivePaths = true;
	ARFilter.bIncludeOnlyOnDiskAssets = true;
	ARFilter.bRecursiveClasses = true;
	AssetRegistry.GetAssets(ARFilter, AssetList);
}

void GetAllBlueprintAssetData(UClass* BaseClass, TArray<FAssetData>& AssetList, bool bRequireEntitlements)
{
	// force disable local entitlement checks on dedicated server
	bRequireEntitlements = bRequireEntitlements && !IsRunningDedicatedServer();
	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(TEXT("AssetRegistry"));
	IAssetRegistry& AssetRegistry = AssetRegistryModule.Get();
	TArray<FString> RootPaths;
	FPackageName::QueryRootContentPaths(RootPaths);
	// HACK: workaround for terrible registry performance when scanning; limit search paths to improve perf a bit
	RootPaths.Remove(TEXT("/Engine/"));
	RootPaths.Remove(TEXT("/Game/"));
	RootPaths.Remove(TEXT("/Paper2D/"));
	RootPaths.Add(TEXT("/Game/1LOKAgame/Blueprints/"));
	// Cooked data has the asset data already set up
	AssetRegistry.ScanPathsSynchronous(RootPaths);
	FARFilter ARFilter;
	ARFilter.ClassNames.Add(UBlueprint::StaticClass()->GetFName());
	/*for (int32 PathIndex = 0; PathIndex < RootPaths.Num(); PathIndex++)
	{
	ARFilter.PackagePaths.Add(FName(*RootPaths[PathIndex]));
	}*/
	ARFilter.bRecursivePaths = true;
	ARFilter.bRecursiveClasses = true;
	ARFilter.bIncludeOnlyOnDiskAssets = true;
	if (BaseClass == nullptr)
	{
		AssetRegistry.GetAssets(ARFilter, AssetList);
	}
	else
	{
		// TODO: the below filtering is torturous because the asset registry does not contain full inheritance information for blueprints
		// nor does it return full class paths when you request a class tree
		TArray<FAssetData> LocalAssetList;
		AssetRegistry.GetAssets(ARFilter, LocalAssetList);
		TSet<FString> UnloadedBaseClassPaths;
		// first pass: determine the inheritance that we can trivially verify are the correct class because their parent is in memory
		for (int32 i = 0; i < LocalAssetList.Num(); i++)
		{
			const FString* LoadedParentClass = LocalAssetList[i].TagsAndValues.Find("ParentClass");
			if (LoadedParentClass != nullptr && !LoadedParentClass->IsEmpty())
			{
				UClass* Class = FindObject<UClass>(ANY_PACKAGE, **LoadedParentClass);
				if (Class == nullptr)
				{
					// apparently you have to 'load' native classes once for FindObject() to reach them
					// figure out if this parent is such a class and if so, allow LoadObject()
					FString ParentPackage = *LoadedParentClass;
					ConstructorHelpers::StripObjectClass(ParentPackage);
					if (ParentPackage.StartsWith(TEXT("/Script/")))
					{
						ParentPackage = ParentPackage.LeftChop(ParentPackage.Len() - ParentPackage.Find(TEXT(".")));
						if (FindObject<UPackage>(NULL, *ParentPackage) != nullptr)
						{
							Class = LoadObject<UClass>(NULL, **LoadedParentClass, NULL, LOAD_NoWarn | LOAD_Quiet);
						}
					}
				}
				if (Class != nullptr)
				{
					if (Class->IsChildOf(BaseClass))
					{
						AssetList.Add(LocalAssetList[i]);
						const FString* GenClassPath = LocalAssetList[i].TagsAndValues.Find("GeneratedClass");
						if (GenClassPath != nullptr)
						{
							UnloadedBaseClassPaths.Add(*GenClassPath);
						}
					}
					LocalAssetList.RemoveAt(i);
					i--;
				}
			}
			else
			{
				// asset info is missing; fail
				LocalAssetList.RemoveAt(i);
				i--;
			}
		}
		// now go through the remainder and match blueprints against an unloaded parent
		// if we find no new matching assets, the rest must be the wrong super
		bool bFoundAny = false;
		do
		{
			bFoundAny = false;
			for (int32 i = 0; i < LocalAssetList.Num(); i++)
			{
				if (UnloadedBaseClassPaths.Find(*LocalAssetList[i].TagsAndValues.Find("ParentClass")))
				{
					AssetList.Add(LocalAssetList[i]);
					const FString* GenClassPath = LocalAssetList[i].TagsAndValues.Find("GeneratedClass");
					if (GenClassPath != nullptr)
					{
						UnloadedBaseClassPaths.Add(*GenClassPath);
					}
					LocalAssetList.RemoveAt(i);
					i--;
					bFoundAny = true;
				}
			}
		} while (bFoundAny && LocalAssetList.Num() > 0);
	}
}

void SetTimerUFunc(UObject* Obj, FName FuncName, float Time, bool bLooping)
{
	if (Obj != nullptr)
	{
		const UWorld* const World = GEngine->GetWorldFromContextObject(Obj, EGetWorldErrorMode::LogAndReturnNull);
		if (World != nullptr)
		{
			UFunction* const Func = Obj->FindFunction(FuncName);
			if (Func == nullptr)
			{
				UE_LOG(UT, Warning, TEXT("SetTimer: Object %s does not have a function named '%s'"), *Obj->GetName(), *FuncName.ToString());
			}
			else if (Func->ParmsSize > 0)
			{
				// User passed in a valid function, but one that takes parameters
				// FTimerDynamicDelegate expects zero parameters and will choke on execution if it tries
				// to execute a mismatched function
				UE_LOG(UT, Warning, TEXT("SetTimer passed a function (%s) that expects parameters."), *FuncName.ToString());
			}
			else
			{
				FTimerDynamicDelegate Delegate;
				Delegate.BindUFunction(Obj, FuncName);
				FTimerHandle Handle = World->GetTimerManager().K2_FindDynamicTimerHandle(Delegate);
				World->GetTimerManager().SetTimer(Handle, Delegate, Time, bLooping);
			}
		}
	}
}

bool IsTimerActiveUFunc(UObject* Obj, FName FuncName, float* TotalTime, float* ElapsedTime)
{
	if (Obj != nullptr)
	{
		const UWorld* const World = GEngine->GetWorldFromContextObject(Obj, EGetWorldErrorMode::LogAndReturnNull);
		if (World != nullptr)
		{
			UFunction* const Func = Obj->FindFunction(FuncName);
			if (Func == nullptr)
			{
				UE_LOG(UT, Warning, TEXT("IsTimerActive: Object %s does not have a function named '%s'"), *Obj->GetName(), *FuncName.ToString());
				return false;
			}
			else
			{
				FTimerDynamicDelegate Delegate;
				Delegate.BindUFunction(Obj, FuncName);
				FTimerHandle Handle = World->GetTimerManager().K2_FindDynamicTimerHandle(Delegate);
				if (World->GetTimerManager().IsTimerActive(Handle))
				{
					if (TotalTime != nullptr)
					{
						*TotalTime = World->GetTimerManager().GetTimerRate(Handle);
					}
					if (ElapsedTime != nullptr)
					{
						*ElapsedTime = World->GetTimerManager().GetTimerElapsed(Handle);
					}
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

void ClearTimerUFunc(UObject* Obj, FName FuncName)
{
	if (Obj != nullptr)
	{
		const UWorld* const World = GEngine->GetWorldFromContextObject(Obj, EGetWorldErrorMode::LogAndReturnNull);
		if (World != nullptr)
		{
			UFunction* const Func = Obj->FindFunction(FuncName);
			if (Func == nullptr)
			{
				UE_LOG(UT, Warning, TEXT("ClearTimer: Object %s does not have a function named '%s'"), *Obj->GetName(), *FuncName.ToString());
			}
			else
			{
				FTimerDynamicDelegate Delegate;
				Delegate.BindUFunction(Obj, FuncName);
				FTimerHandle Handle = World->GetTimerManager().K2_FindDynamicTimerHandle(Delegate);
				return World->GetTimerManager().ClearTimer(Handle);
			}
		}
	}
}
