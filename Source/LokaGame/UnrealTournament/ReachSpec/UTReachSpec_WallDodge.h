// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "UTReachSpec.h"
#include "UTReachSpec_WallDodge.generated.h"

class AUTBot;
class AUTCharacter;
class UUTCharacterMovement;

UCLASS()
class UUTReachSpec_WallDodge : public UUTReachSpec
{
	GENERATED_BODY()
public:
	
	UUTReachSpec_WallDodge(const FObjectInitializer& OI)
		: Super(OI)
	{
		PathColor = FLinearColor(1.0f, 1.0f, 0.0f);
	}

	/** point on jump source poly that we should start at to move towards wall (generally a specific edge of the poly to get around outcroppings and such) */
	UPROPERTY()
	FVector JumpStart;
	/** target point on the surface of the wall */
	UPROPERTY()
	FVector WallPoint;
	/** wall normal/dodge direction */
	UPROPERTY()
	FVector WallNormal;

	virtual int32 CostFor(int32 DefaultCost, const FUTPathLink& OwnerLink, APawn* Asker, const FNavAgentProperties& AgentProps, const FUTReachParams& ReachParams, AController* RequestOwner, NavNodeRef StartPoly, const class AUTRecastNavMesh* NavMesh) override;
	virtual bool WaitForMove(const FUTPathLink& OwnerLink, APawn* Asker, const FComponentBasedPosition& MovePos, const FRouteCacheItem& Target) const override;
	virtual bool OverrideAirControl(const FUTPathLink& OwnerLink, APawn* Asker, const FComponentBasedPosition& MovePos, const FRouteCacheItem& Target) const override;
	virtual bool GetMovePoints(const FUTPathLink& OwnerLink, const FVector& StartLoc, APawn* Asker, const FNavAgentProperties& AgentProps, const struct FRouteCacheItem& Target, const TArray<FRouteCacheItem>& FullRoute, const class AUTRecastNavMesh* NavMesh, TArray<FComponentBasedPosition>& MovePoints) const override;
};