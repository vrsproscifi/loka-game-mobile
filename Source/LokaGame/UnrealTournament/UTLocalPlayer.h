#pragma once
#include "UTLocalPlayer.generated.h"

class AUTPlayerState;
class UUTKillcamPlayback;

UCLASS(config=Engine)
class LOKAGAME_API UUTLocalPlayer : public ULocalPlayer
{
	GENERATED_UCLASS_BODY()

public:
	virtual bool Exec(UWorld* InWorld, const TCHAR* Cmd, FOutputDevice& Ar) override;

public:

	/** accessors for default URL options */
	virtual FString GetDefaultURLOption(const TCHAR* Key) const;
	virtual void SetDefaultURLOption(const FString& Key, const FString& Value);
	virtual void ClearDefaultURLOption(const FString& Key);

	
public:


protected:

	UPROPERTY()
	UUTKillcamPlayback* KillcamPlayback;

public:
	/**
	* Returns the killcam manager object for this player.
	*/
	UUTKillcamPlayback* GetKillcamPlaybackManager() const { return KillcamPlayback; }
};

