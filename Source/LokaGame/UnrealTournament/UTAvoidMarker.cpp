// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#include "LokaGame.h"
#include "UTAvoidMarker.h"
#include "UTBot.h"
#include "UTGameState.h"

AUTAvoidMarker::AUTAvoidMarker(const FObjectInitializer& OI)
: Super(OI)
{
	Capsule = OI.CreateOptionalDefaultSubobject<UCapsuleComponent>(this, FName(TEXT("Capsule")));
	if (Capsule != nullptr)
	{
		Capsule->SetCapsuleSize(225.0f, 100.0f);
		Capsule->SetCollisionProfileName(FName(TEXT("ProjectileOverlap")));
		Capsule->OnComponentBeginOverlap.AddDynamic(this, &AUTAvoidMarker::OnOverlapBegin);
		RootComponent = Capsule;
	}

	TeamNum = 255;
}

void AUTAvoidMarker::BeginPlay()
{
	Super::BeginPlay();

	if (GetOwner() == nullptr)
	{
		UE_LOG(UT, Warning, TEXT("UTAvoidMarker requires an owner"));
		Destroy();
	}
}

void AUTAvoidMarker::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (GetOwner() == nullptr)
	{
		// owner expired and didn't clean us up
		Destroy();
	}
	else
	{
		APawn* P = Cast<APawn>(OtherActor);
		if (P != nullptr)
		{
			AUTBot* B = Cast<AUTBot>(P->Controller);
			AUTGameState* GS = GetWorld()->GetGameState<AUTGameState>();
			if (B != nullptr && (bFriendlyAvoid || GS == nullptr || !GS->OnSameTeam(B, this)))
			{
				B->AddFearSpot(this);
			}
		}
	}
}
