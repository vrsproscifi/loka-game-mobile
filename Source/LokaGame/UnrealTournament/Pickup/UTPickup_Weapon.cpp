// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "UTPickup_Weapon.h"

#include "UTCharacter.h"
#include "UTBot.h"
#include "ShooterGameInstance.h"

#include "Weapon/ItemWeaponEntity.h"
#include "Module/ItemModuleEntity.h"
#include "Material/ItemMaterialEntity.h"

#include "Weapon/ShooterWeapon.h"

AUTPickup_Weapon::AUTPickup_Weapon(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMeshComp"));
	WeaponMesh->SetupAttachment(RootComponent);

	Weapon = ItemWeaponModelId::LOKA_SunGazer;
	Material = ItemMaterialModelId::Default;
}

void AUTPickup_Weapon::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	InitializeMesh();
}

bool AUTPickup_Weapon::AllowPickupBy_Implementation(APawn* Other, bool bDefaultAllowPickup)
{
	return Super::AllowPickupBy_Implementation(Other, bDefaultAllowPickup);
}

void AUTPickup_Weapon::GiveTo_Implementation(APawn* Target)
{
	Super::GiveTo_Implementation(Target);

	//if (auto GiveToChar = Cast<AUTCharacter>(Target))
	//{
	//	if (auto GameInst = Cast<UShooterGameInstance>(GetGameInstance()))
	//	{
	//		if (GameInst->EntityRepository && GameInst->EntityRepository->InstanceRepository.Weapon[Weapon])
	//		{
	//			GiveWeaponTo(GiveToChar, GameInst->EntityRepository->InstanceRepository.Weapon[Weapon], Modules, Modifers, Material);
	//		}
	//	}
	//}
}

void AUTPickup_Weapon::StartSleeping_Implementation()
{
	Super::StartSleeping_Implementation();
}

void AUTPickup_Weapon::WakeUp_Implementation()
{
	Super::WakeUp_Implementation();
}

void AUTPickup_Weapon::BeginPlay()
{
	Super::BeginPlay();
}

void AUTPickup_Weapon::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
}

void AUTPickup_Weapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AUTPickup_Weapon::SetPickupHidden(bool bNowHidden)
{	
	TimerEffect->SetVisibility(bNowHidden);

	if (GhostMeshMaterial)
	{
		for (int32 i = 0; i < WeaponMesh->GetNumMaterials(); ++i)
		{
			WeaponMesh->SetMaterial(i, bNowHidden ? GhostMeshMaterial : nullptr);
		}
	}
	else
	{
		WeaponMesh->SetVisibility(!bNowHidden);
	}
}

void AUTPickup_Weapon::GiveWeaponTo(AUTCharacter* Pawn, UItemWeaponEntity* InInstance, const TArray<TEnumAsByte<ItemModuleModelId::Type>> InModules, const TMap<FString, float> InModifers, const ItemMaterialModelId::Type InMaterial)
{
	AUTCharacter *ShooterCharacter = Pawn;
	auto GameInst = Cast<UShooterGameInstance>(GetGameInstance());

	//TODO: SeNTIKE
	//if (GameInst && ShooterCharacter && InInstance)
	//{
	//	TGuardValue<bool> GuardTemplateNameFlag(GIsReconstructingBlueprintInstances, true);
	//
	//	AShooterWeapon* NewWeapon = GetWorld()->SpawnActorDeferred<AShooterWeapon>(AShooterWeapon::StaticClass(), FTransform(), ShooterCharacter, ShooterCharacter, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	//	NewWeapon->Instance = DuplicateObject(InInstance, NewWeapon, *FString::Printf(TEXT("Player_Weapon_%d_%s"), InInstance->GetModelId(), *GetName()));
	//
	//	for (auto i : InModules)
	//	{
	//		if (i >= 0 && i < ItemModuleModelId::End && GameInst->EntityRepository->InstanceRepository.Module[i])
	//		{
	//			NewWeapon->InstalledModules.Add(DuplicateObject(GameInst->EntityRepository->InstanceRepository.Module[i], NewWeapon, *FString::Printf(TEXT("Player_Weapon_Module_%d_%s"), GameInst->EntityRepository->InstanceRepository.Module[i]->GetModelId(), *GetName())));
	//		}
	//	}
	//
	//	TArray<uint8> NoInstall;
	//	for (auto i : NewWeapon->Instance->DefaultModules)
	//	{
	//		if (i >= 0 && i < ItemModuleModelId::End && GameInst->EntityRepository->InstanceRepository.Module[i])
	//		{
	//			for (auto &m : NewWeapon->InstalledModules)
	//			{
	//				if (m->TargetSocket == GameInst->EntityRepository->InstanceRepository.Module[i]->TargetSocket)
	//				{
	//					NoInstall.Add(i);
	//				}
	//			}
	//		}
	//	}
	//
	//	for (auto i : NewWeapon->Instance->DefaultModules)
	//	{
	//		if (NoInstall.Contains(i) == false)
	//		{
	//			if (i >= 0 && i < ItemModuleModelId::End && GameInst->EntityRepository->InstanceRepository.Module[i])
	//			{
	//				NewWeapon->InstalledModules.Add(DuplicateObject(GameInst->EntityRepository->InstanceRepository.Module[i], NewWeapon, *FString::Printf(TEXT("Player_Weapon_Module_%d_%s"), GameInst->EntityRepository->InstanceRepository.Module[i]->GetModelId(), *GetName())));
	//			}
	//		}
	//	}
	//
	//	if (InMaterial < ItemMaterialModelId::End)
	//	{
	//		NewWeapon->InstalledMaterial = InMaterial;
	//	}
	//
	//	NewWeapon->Instance->PostDuplicateEntity(InInstance);
	//	NewWeapon->Instance->ApplyModifers(InModifers);
	//	NewWeapon->FinishSpawning(FTransform());
	//	ShooterCharacter->AddWeapon(NewWeapon);
	//	ShooterCharacter->EquipWeapon(NewWeapon);
	//
	//	UE_LOG(LogNet, Log, TEXT("AShooterPickup_Weapon::GiveWeaponTo: %s"), *InInstance->GetDescription().Name.ToString());
	//}
}

void AUTPickup_Weapon::InitializeMesh()
{
	//if (auto GameInst = Cast<UShooterGameInstance>(GetGameInstance()))
	//{
	//	if (GameInst->EntityRepository && GameInst->EntityRepository->InstanceRepository.Weapon[Weapon])
	//	{
	//		WeaponInstance = GameInst->EntityRepository->InstanceRepository.Weapon[Weapon];
	//		WeaponMesh->SetSkeletalMesh(WeaponInstance->GetSkeletalMesh());

	//		FVector Offset = GetRootComponent()->ComponentToWorld.InverseTransformVectorNoScale(WeaponMesh->Bounds.Origin - WeaponMesh->GetComponentToWorld().GetLocation());
	//		WeaponMesh->SetRelativeLocation(WeaponMesh->GetRelativeTransform().GetLocation() - Offset);
	//		// if there's a rotation component, set it up to rotate the pickup mesh
	//		if (GetWorld()->WorldType != EWorldType::Editor) // not if editor preview, because that will cause the preview component to exist in game and we want it to be editor only
	//		{
	//			TArray<URotatingMovementComponent*> RotationComps;
	//			GetComponents<URotatingMovementComponent>(RotationComps);
	//			if (RotationComps.Num() > 0)
	//			{
	//				RotationComps[0]->SetUpdatedComponent(WeaponMesh);
	//				RotationComps[0]->PivotTranslation = Offset;
	//			}
	//		}
	//	}
	//}
}

float AUTPickup_Weapon::BotDesireability_Implementation(APawn* Asker, AController* RequestOwner, float TotalDistance)
{
	AUTBot* MyBotController = Cast<AUTBot>(RequestOwner);
	AUTCharacter* MyBotCharacter = Cast<AUTCharacter>(Asker);
	//TODO: SeNTIKE
	//if (MyBotController && MyBotCharacter)
	//{
	//	if (MyBotCharacter->GetWeapon() && MyBotCharacter->GetWeapon()->GetInstance())
	//	{
	//		auto TargetWeaponInstance = MyBotCharacter->GetWeapon()->GetInstance();
	//		if (TargetWeaponInstance->GetInstanceModelId() == Weapon)
	//		{
	//			return .0f;
	//		}
	//		else
	//		{
	//			return MyBotController->IsFavoriteWeapon(Weapon) ? 1.0f : .5f;
	//		}
	//	}		
	//}

	return .0f;
}

float AUTPickup_Weapon::DetourWeight_Implementation(APawn* Asker, float TotalDistance)
{
	return .0f;
}

//=====================================[ Alternative draw

void AUTPickup_Weapon::DrawMinimapIcon_Implementation(AHUD* InOwnerHud, UCanvas* InCanvas, FVector2D InPosition, const float InScale)
{
	if (WeaponInstance && InCanvas && WeaponInstance->GetIcon())
	{
		FVector2D TargetSize(64, 64);
		TargetSize *= InScale;

		if (auto TileAsMaterial = Cast<UMaterialInterface>(WeaponInstance->GetIcon()->GetResourceObject()))
		{			
			FCanvasTileItem TileItem(InPosition - TargetSize / 2, TileAsMaterial->GetRenderProxy(false), TargetSize);
			InCanvas->DrawItem(TileItem);
		}
		else if (auto TileAsTexture = Cast<UTexture>(WeaponInstance->GetIcon()->GetResourceObject()))
		{
			const auto UV = WeaponInstance->GetIcon()->GetUVRegion();

			if (UV.bIsValid)
			{
				InCanvas->DrawTile(TileAsTexture, InPosition.X - TargetSize.X / 2, InPosition.Y - TargetSize.Y / 2, TargetSize.X, TargetSize.Y, UV.Min.X, UV.Min.Y, UV.Max.X, UV.Max.Y);
			}
			else
			{
				auto Brush = WeaponInstance->GetIcon();
				InCanvas->DrawTile(TileAsTexture, InPosition.X - TargetSize.X / 2, InPosition.Y - TargetSize.Y / 2, TargetSize.X, TargetSize.Y, 0, 0, Brush->ImageSize.X, Brush->ImageSize.Y);
			}
		}
	}
}