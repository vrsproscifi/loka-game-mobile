// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "UTDamageType.h"
#include "UTDmgType_Suicide.generated.h"

UCLASS()
class LOKAGAME_API UUTDmgType_Suicide : public UUTDamageType
{
	GENERATED_UCLASS_BODY()
};
