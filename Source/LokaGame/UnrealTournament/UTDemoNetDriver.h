// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once

#include "Runtime/Engine/Classes/Engine/DemoNetDriver.h"
#include "UTDemoRecSpectator.h"

#include "UTDemoNetDriver.generated.h"

UCLASS(CustomConstructor)
class LOKAGAME_API UUTDemoNetDriver : public UDemoNetDriver
{
	GENERATED_BODY()
public:
	virtual void ProcessRemoteFunction(AActor* Actor, UFunction* Function, void* Parameters, FOutParmRec* OutParms, FFrame* Stack, UObject* SubObject) override
	{
		if (IsServer() && ClientConnections.Num() > 0 && Cast<AUTDemoRecSpectator>(Actor) != nullptr)
		{
			InternalProcessRemoteFunction(Actor, SubObject, ClientConnections[0], Function, Parameters, OutParms, Stack, true);
		}
		else
		{
			Super::ProcessRemoteFunction(Actor, Function, Parameters, OutParms, Stack, SubObject);
		}
	}

	virtual bool ShouldSaveCheckpoint() /*override*/;
protected:
	virtual void WriteGameSpecificDemoHeader(TArray<FString>& GameSpecificData) override;
	virtual bool ProcessGameSpecificDemoHeader(const TArray<FString>& GameSpecificData, FString& Error) override;
};