// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "UTLocalMessage.h"

#include "UTCarriedObjectMessage.generated.h"

UCLASS()
class LOKAGAME_API UUTCarriedObjectMessage : public UUTLocalMessage
{
	GENERATED_UCLASS_BODY()
};