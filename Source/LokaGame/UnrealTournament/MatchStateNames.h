#pragma once

/** Defines the current state of the game. */

namespace MatchState
{
	extern LOKAGAME_API const FName PlayerIntro;					// Playing the player intro in the match summary window
	extern LOKAGAME_API const FName CountdownToBegin;				// We are entering this map, actors are not yet ticking
	extern LOKAGAME_API const FName MatchEnteringOvertime;			// The game is entering overtime
	extern LOKAGAME_API const FName MatchIsInOvertime;				// The game is in overtime
	extern LOKAGAME_API const FName MapVoteHappening;				// The game is in mapvote stage
	extern LOKAGAME_API const FName MatchIntermission;				// The game is in a round intermission
	extern LOKAGAME_API const FName MatchExitingIntermission;		// Exiting Halftime
	extern LOKAGAME_API const FName MatchRankedAbandon;				// Exiting Halftime
}