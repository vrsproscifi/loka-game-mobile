// editor-creatable bot character data
// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once

#include "UTBot.h"
#include "InventoryHacks.h"
#include "UTBotCharacter.generated.h"

UCLASS()
class LOKAGAME_API UUTBotCharacter : public UDataAsset
{
GENERATED_BODY()
public:
	/** if set a UTProfileItem is required for this character to be available */
	UPROPERTY(EditAnywhere, AssetRegistrySearchable, Meta = (DisplayName = "Requires Online Item"))
	bool bRequiresItem;

	/** alternate names to use when there are not enough bot characters for the number of players requested and we need to put in duplicates */
	UPROPERTY(EditAnywhere)
	TArray<FString> AltNames;

	/** bot skill rating (0 - 7.9) */
	UPROPERTY(EditAnywhere, AssetRegistrySearchable, Meta = (ClampMin = "0", UIMin = "0", ClampMax = "7.9", UIMax = "7.9"))
	float Skill;
	/** bot personality attributes affecting behavior */
	UPROPERTY(EditAnywhere)
	FBotPersonality Personality;
	
	/** character content (primary mesh/material) */
	UPROPERTY(EditAnywhere, Meta = (MetaClass = "UTCharacterContent"))
	FStringClassReference CharacterContent;

	/* Voice associated with this character. */
	UPROPERTY(EditAnywhere, Meta = (MetaClass = "UTCharacterVoice"))
	FStringClassReference CharacterVoice;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Entity)
	TArray<FProfileHack> Profiles;
};