// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once

#include "UTAIAction.h"
#include "UTBot.h"
#include "UTDefensePoint.h"
#include "UTPickup.h"

#include "UTAIAction_Camp.generated.h"

UCLASS()
class LOKAGAME_API UUTAIAction_Camp : public UUTAIAction
{
	GENERATED_BODY()

	UUTAIAction_Camp(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	{}

	float CampEndTime;

	virtual void Started() override
	{
		CampEndTime = GetWorld()->TimeSeconds + 3.0f;
		// if camping pickup, set end time to re-evaluate right after it respawns
		TArray<AActor*> Touching;
		GetPawn()->GetOverlappingActors(Touching, AUTPickup::StaticClass());
		for (AActor* Touched : Touching)
		{
			AUTPickup* Pickup = Cast<AUTPickup>(Touched);
			if (Pickup != nullptr)
			{
				float RespawnOffset = Pickup->GetRespawnTimeOffset(GetPawn());
				if (RespawnOffset > 0.0f)
				{
					CampEndTime = FMath::Min<float>(CampEndTime, GetWorld()->TimeSeconds + RespawnOffset);
				}
			}
		}
	}

	virtual bool SetFocusForNoTarget()
	{
		if (GetOuterAUTBot()->GetDefensePoint() != nullptr && GetUTNavData(GetWorld())->HasReachedTarget(GetOuterAUTBot()->GetPawn(), GetOuterAUTBot()->GetPawn()->GetNavAgentPropertiesRef(), FRouteCacheItem(GetOuterAUTBot()->GetDefensePoint())))
		{
			GetOuterAUTBot()->SetFocalPoint(GetOuterAUTBot()->GetDefensePoint()->GetActorLocation() + GetOuterAUTBot()->GetDefensePoint()->GetActorRotation().Vector() * 1000.0f);
		}
		else
		{
			const TArray<const FBotEnemyInfo>& EnemyList = GetOuterAUTBot()->GetEnemyList(true);
			if (EnemyList.Num() == 0)
			{
				GetOuterAUTBot()->SetFocalPoint(GetOuterAUTBot()->GetPawn()->GetActorLocation() + GetOuterAUTBot()->GetPawn()->GetActorRotation().Vector() * 1000.0f);
			}
			else
			{
				FVector AvgEnemyLoc = FVector::ZeroVector;
				for (const FBotEnemyInfo& TestEnemy : EnemyList)
				{
					AvgEnemyLoc += TestEnemy.LastKnownLoc / EnemyList.Num();
				}
				GetOuterAUTBot()->SetFocalPoint(AvgEnemyLoc);
			}
		}
		return true;
	}

	virtual bool Update(float DeltaTime) override
	{
		return GetWorld()->TimeSeconds >= CampEndTime;
	}
};