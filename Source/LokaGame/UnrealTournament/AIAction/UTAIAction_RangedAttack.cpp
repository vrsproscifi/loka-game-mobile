// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#include "LokaGame.h"
#include "UTAIAction_RangedAttack.h"
#include "UTSquadAI.h"
#include "UTGameState.h"
#include "UTCharacter.h"
#include "Weapon/ShooterWeapon.h"

UUTAIAction_RangedAttack::UUTAIAction_RangedAttack(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
}

bool UUTAIAction_RangedAttack::IsEnemyAttackable() const
{
	AShooterWeapon* MyWeap = (GetUTChar() != nullptr) ? GetUTChar()->GetWeapon() : NULL;
	bool bCanTryIndirect = (MyWeap != nullptr && (GetOuterAUTBot()->Skill >= 2.0f || GetOuterAUTBot()->IsFavoriteWeapon(MyWeap)));
	// abort if can't attack predicted enemy loc or enemy info is too outdated to trust its accuracy
	return (GetTarget() == GetEnemy() && /*!Pawn.RecommendLongRangedAttack() &&*/ (GetWorld()->TimeSeconds - GetOuterAUTBot()->GetEnemyInfo(GetEnemy(), true)->LastFullUpdateTime > 1.0f || MyWeap == nullptr || !MyWeap->CanAttack(GetEnemy(), GetOuterAUTBot()->GetEnemyLocation(GetEnemy(), true), bCanTryIndirect)));
}

void UUTAIAction_RangedAttack::Started()
{
	bEnemyWasAttackable = IsEnemyAttackable();

	GetOuterAUTBot()->ClearMoveTarget();
	if (GetUTChar() != nullptr && (GetUTChar()->GetWeapon() == nullptr || GetUTChar()->GetWeapon()->IsMeleeWeapon()))
	{
		GetOuterAUTBot()->SwitchToBestWeapon();
	}
	// maybe crouch to throw off opponent aim
	if (GetEnemy() != nullptr && GetCharacter() != nullptr && GetWeapon() != nullptr && GetWeapon()->IsPrioritizeAccuracy() && (GetOuterAUTBot()->GetEnemyLocation(GetEnemy(), false) - GetPawn()->GetActorLocation()).Size() > 4000.0f &&
		FMath::FRand() < FMath::Max<float>(5.0f, GetOuterAUTBot()->Skill) * 0.05f - 0.1f - (GetOuterAUTBot()->Personality.Jumpiness - 0.5f))
	{
		GetCharacter()->GetCharacterMovement()->bWantsToCrouch = true;
	}
	SetTimerUFunc(this, TEXT("FirstShotTimer"), 0.2f, false);
}

void UUTAIAction_RangedAttack::Ended(bool bAborted)
{
	Super::Ended(bAborted);
	if (GetCharacter() != nullptr) // could have ended because we're dead
	{
		GetCharacter()->GetCharacterMovement()->bWantsToCrouch = false;
	}
}

bool UUTAIAction_RangedAttack::FindStrafeDest()
{
	if (/*!GetCharacter()->bCanStrafe || */ GetWeapon() == nullptr || GetOuterAUTBot()->Skill + GetOuterAUTBot()->Personality.MovementAbility < 1.5f + 3.5f * FMath::FRand())
	{
		// can't strafe, no weapon to check distance with or not skilled enough
		return false;
	}
	else
	{
		AUTRecastNavMesh* NavData = GetUTNavData(GetWorld());
		NavNodeRef MyPoly = (NavData != nullptr) ? NavData->UTFindNearestPoly(GetCharacter()->GetNavAgentLocation(), GetCharacter()->GetSimpleCollisionCylinderExtent()) : INVALID_NAVNODEREF;
		if (MyPoly != INVALID_NAVNODEREF)
		{
			TArray<NavNodeRef> AdjacentPolys;
			NavData->FindAdjacentPolys(GetPawn(), GetPawn()->GetNavAgentPropertiesRef(), MyPoly, true, AdjacentPolys);
			if (AdjacentPolys.Num() > 0)
			{
				const FVector TargetLoc = GetTarget()->GetActorLocation();
				float TargetDist = (TargetLoc - GetPawn()->GetActorLocation()).Size();
				// consider backstep opposed to always charging if enemy objective, depending on combat style and weapon
				bool bAllowBackwards = true;
				AUTGameState* GS = GetWorld()->GetGameState<AUTGameState>();
				if (GS != nullptr && !GS->OnSameTeam(GetTarget(), GetOuterAUTBot()))
				{
					bAllowBackwards = (GetOuterAUTBot()->Personality.Aggressiveness + GetWeapon()->SuggestAttackStyle() <= 0.0);
				}
				float MaxRange = /* TODO: (UTWeap != None ? UTWeap.GetOptimalRangeFor(Focus) : Pawn.Weapon.MaxRange()) */ 10000.0f;
				FCollisionQueryParams TraceParams(FName(TEXT("FindStrafeDest")), false, GetPawn());
				TraceParams.AddIgnoredActor(GetTarget());
				// pick a random point linked to anchor that we can shoot target from
				int32 Start = FMath::RandHelper(AdjacentPolys.Num());
				int32 i = Start;
				do
				{
					const FVector TestLoc = NavData->GetPolyCenter(AdjacentPolys[i]);
					// allow points within range, that aren't significantly backtracking unless allowed,
					// and that we can still hit target from
					float Dist = (TestLoc - TargetLoc).Size();
					if ( (Dist <= MaxRange || Dist < TargetDist) && (bAllowBackwards || Dist <= TargetDist + 250.0) &&
						!GetWorld()->LineTraceTestByChannel(TestLoc + FVector(0.0f, 0.0f, GetPawn()->GetSimpleCollisionHalfHeight()), TargetLoc, ECC_Visibility, TraceParams) )
					{
						GetOuterAUTBot()->SetMoveTargetDirect(FRouteCacheItem(TestLoc, AdjacentPolys[i]));
						return true;
					}
					i++;
					if (i == AdjacentPolys.Num())
					{
						i = 0;
					}
				} while (i != Start);
			}
		}
		return false;
	}
}

void UUTAIAction_RangedAttack::FirstShotTimer()
{
	if ( (GetUTChar() != nullptr && GetUTChar()->GetWeapon() != nullptr && GetUTChar()->GetWeapon()->IsMeleeWeapon()) || GetTarget() == nullptr || GetTarget() != GetOuterAUTBot()->GetFocusActor() ||
		(GetTarget() != GetEnemy() && GetTarget() != GetSquad()->GetObjective() && GetEnemy() != nullptr && GetOuterAUTBot()->LineOfSightTo(GetEnemy())) )
	{
		GetOuterAUTBot()->WhatToDoNext();
	}
	else
	{
		// maybe crouch to throw off opponent aim
		if (GetEnemy() != nullptr && GetCharacter() != nullptr && GetWeapon() != nullptr && GetWeapon()->IsPrioritizeAccuracy() && FMath::FRand() < 0.5f - GetOuterAUTBot()->Skill * 0.05f  - 0.5f * GetOuterAUTBot()->Personality.Jumpiness)
		{
			GetCharacter()->GetCharacterMovement()->bWantsToCrouch = true;
		}
		if (!FindStrafeDest())
		{
			SetTimerUFunc(this, TEXT("EndTimer"), 0.2f + (0.5f + 0.5f * FMath::FRand()) * 0.4f * (7.0f - GetOuterAUTBot()->Skill), false);
		}
	}
}

void UUTAIAction_RangedAttack::EndTimer()
{
	// just used as a marker
}

bool UUTAIAction_RangedAttack::Update(float DeltaTime)
{
	return GetTarget() == nullptr || ( !GetOuterAUTBot()->GetMoveTarget().IsValid() &&
									!IsTimerActiveUFunc(this, TEXT("FirstShotTimer")) &&
									!IsTimerActiveUFunc(this, TEXT("EndTimer")) &&
									(GetUTChar() == nullptr || GetUTChar()->GetWeapon() == nullptr) ) ||
		(GetUTChar() != nullptr && GetUTChar()->GetWeapon() != nullptr && !GetUTChar()->GetWeapon()->CanFire()) ||
		(GetTarget() && GetOuterAUTBot()->IsEnemyVisible(Cast<APawn>(GetTarget())) == false && GetUTChar() != nullptr && GetUTChar()->GetWeapon() != nullptr && !GetUTChar()->GetWeapon()->CanAttack(GetTarget(), GetTarget()->GetActorLocation(), false));
}