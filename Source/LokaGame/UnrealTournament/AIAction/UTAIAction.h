// a discrete action performed by AI, such as "hunt player" or "grab flag"
// replacement for what were AI states in UT3 AI code
// only one action may be in use at a time and an action is generally active until it returns true from Update() or is explicitly aborted/replaced by outside code
// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once

#include "UTBot.h"
#include "UTAIAction.generated.h"

class AUTBot;
class AUTCharacter;
class AShooterWeapon;

UCLASS(NotPlaceable, Abstract, DefaultToInstanced, CustomConstructor, Within = UTBot)
class LOKAGAME_API UUTAIAction : public UObject
{
	GENERATED_UCLASS_BODY()

	UUTAIAction(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	{}

	virtual UWorld* GetWorld() const override;
	
	// convenience accessors
	APawn* GetPawn() const;
	ACharacter* GetCharacter() const;
	AUTCharacter* GetUTChar() const;
	AUTSquadAI* GetSquad() const;
	APawn* GetEnemy() const;
	AActor* GetTarget() const;
	AShooterWeapon* GetWeapon() const;

	/** ticks the action; returns true on completion */
	virtual bool Update(float DeltaTime) PURE_VIRTUAL(UUTAIAction::Update, return true;);

	/** called when the action is assigned to the AI */
	virtual void Started()
	{}
	/** called when the action is stopped */
	virtual void Ended(bool bAborted);

	/** return whether or not the bot can fall (or jump) off ledges during this action */
	virtual bool AllowWalkOffLedges()
	{
		return true;
	}

	/** notification of Pawn's move being blocked by a wall - return true to override default handling */
	virtual bool NotifyMoveBlocked(const FHitResult& Impact)
	{
		return false;
	}
	/** notification of Pawn's move being obstructed because it hit a ledge - return true to override default handling */
	virtual bool NotifyHitLedge()
	{
		return false;
	}

	/** called repeatedly while enemy is valid but not visible; combat actions often react to this by changing position, aborting, etc */
	virtual void EnemyNotVisible()
	{}

	/** called to optionally set bot's focus when no target/enemy to look at */
	virtual bool SetFocusForNoTarget()
	{
		return false;
	}
};