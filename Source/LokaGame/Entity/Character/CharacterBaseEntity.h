// VRSPRO

#pragma once

#include "Item/ItemBaseEntity.h"
#include "CharacterBaseEntity.generated.h"


USTRUCT(BlueprintType)
struct FCharacterSpeedModifers
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere) float ModiferRunning;
	UPROPERTY(EditAnywhere) float ModiferTargeting;
	UPROPERTY(EditAnywhere) float ModiferCrouching;
	UPROPERTY(EditAnywhere) float ModiferSwiming;

	FCharacterSpeedModifers() 
		: ModiferRunning(0)
		, ModiferTargeting(0)
		, ModiferCrouching(0)
		, ModiferSwiming(0)
	{}

	FORCEINLINE bool IsValid() const
	{
		return (ModiferRunning != .0f && ModiferTargeting != .0f && ModiferCrouching != .0f && ModiferSwiming != .0f);
	}
};

USTRUCT(BlueprintType)
struct FCharacterMeshes
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere) USkeletalMesh*				Mesh1P;
	UPROPERTY(EditAnywhere) TSubclassOf<UAnimInstance>	MeshAnim1P;
	UPROPERTY(EditAnywhere) USkeletalMesh*				Mesh3P;
	UPROPERTY(EditAnywhere) TSubclassOf<UAnimInstance>	MeshAnim3P;
};

UCLASS(Blueprintable)
class LOKAGAME_API UCharacterBaseEntity
	: public UItemBaseEntity
{
	GENERATED_BODY()

public:
	UCharacterBaseEntity();

	int32 GetModelIdAsFlag() const;

	/** sound played when health is low */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	class USoundCue* LowHealthSound;

	/** when low health effects should start */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float LowHealthPercentage = 0.5f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TSubclassOf<AActor> VisualActor;

	virtual bool IsSupportedForNetworking() const override
	{
		return true;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Mesh)
	FORCEINLINE USkeletalMesh* GetSkeletalMesh1P() const { return SkeletalMesh1P; }

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Character)
	TSubclassOf<class ACharacterAbility> CharacterAbility;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Character, Meta = (MetaClass = "UTCharacterContent"))
	FStringClassReference CharacterContent;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Character)
	FORCEINLINE	FCharacterSpeedModifers GetCharacterSpeedModifers() const { return SpeedModifers; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Character)
	FORCEINLINE FCharacterMeshes GetConstructionMeshData() const { return ConstructionMesh; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Character)
	FORCEINLINE FCharacterMeshes GetProfileMeshData() const { return ProfileMesh; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Character)
	FORCEINLINE FCharacterMeshes GetFightMeshData() const { return FightMesh; }

	virtual bool IsValidItem() const override;

protected:

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Mesh, meta = (Depricated))
	USkeletalMesh* SkeletalMesh1P;	

	UPROPERTY(EditDefaultsOnly, Category = Character, meta = (ShowOnlyInnerProperties))
	FCharacterSpeedModifers SpeedModifers;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Mesh)
	FCharacterMeshes ConstructionMesh;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Mesh)
	FCharacterMeshes FightMesh;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Mesh)
	FCharacterMeshes ProfileMesh;
};
