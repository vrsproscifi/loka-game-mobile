// VRSPRO

#include "LokaGame.h"
#include "CharacterBaseEntity.h"

UCharacterBaseEntity::UCharacterBaseEntity()
	: LowHealthSound(nullptr)
{
	EntityDescription.CategoryType = ECategoryTypeId::Character;
	EntityDescription.Name = FText::FromString(ANSI_TO_TCHAR("Default Object"));
	EntityDescription.Description = FText::FromString(ANSI_TO_TCHAR("Default Object"));

	SpeedModifers.ModiferRunning = 1.8f;
	SpeedModifers.ModiferTargeting = 0.7f;
	SpeedModifers.ModiferCrouching = 0.6f;
	SpeedModifers.ModiferSwiming = 0.8f;
}

int32 UCharacterBaseEntity::GetModelIdAsFlag() const
{
	return FFlagsHelper::ToFlag<int32>(GetModelId());
}

bool UCharacterBaseEntity::IsValidItem() const
{
	const bool IsValidMeshes = FightMesh.Mesh3P && ConstructionMesh.Mesh3P && FightMesh.Mesh1P;
	const bool IsValidSpeedData = Property.Speed.MaxAmount > 0 && SpeedModifers.IsValid();

	return IsValidMeshes && IsValidSpeedData;
}

