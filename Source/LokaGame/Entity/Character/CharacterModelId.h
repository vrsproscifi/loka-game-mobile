#pragma once

#include "CharacterModelId.generated.h"

UENUM()
namespace ECharacterModelId
{
	enum Type
	{
		PeopleMan,
		PeopleWoman,
		RobotMan,
		RobotWoman,
		RobotXb33,
		CyborgLady,
		RobotSprinter,
		PeopleSoldier,
		//CyborgTrooper,
		//CyborgDroidEpic,
		End UMETA(Hidden)
	};
}
