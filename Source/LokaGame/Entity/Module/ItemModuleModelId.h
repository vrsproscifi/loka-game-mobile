#pragma once

#include "ItemModuleModelId.generated.h"

UENUM()
namespace ItemModuleModelId
{
	enum Type
	{
		Default,
		Scope_TitanLine_Num,
		Silent_BenelliM4,
		Butt_BenelliM4,
		Scope_rail_OpticalX16_DTA,
		Scope_rail_CollimatorOptical,
		TLS_MT_1,
		Silent_MT_3,
		Scope_TitanLine,
		Scope_rail_Colimator,
		Scope_rail_Opticalx4_SG,
		Scope_rail_Opticalx8_SG,
		TLS_SG,
		Barrel_SG_Short,
		FlameArrestor_SG,
		Silent_SG,
		Silent_SG_long,
		Scope_SW,
		Scope_SD,
		Scope_Optical_SD,
		Silent_MT,
		FlameArrestor_MT,
		Stoner96_Default,
		Scope_OpticalX2_SD,
		Scope_OpticalX4_SD,
		Scope_OpticalX8_SD,
		Scope_TitanLine3,
		Scope_rail_MS_DocterSight,
		Scope_rail_MS_DocterSightBig,
		Scope_rail_HoloSight,
		End UMETA(Hidden)
	};
}
