// VRSPRO

#pragma once

#include "Entity/Module/ItemModuleEntity.h"
#include "ItemModuleScopeOpticalEntity.generated.h"

UCLASS()
class LOKAGAME_API UItemModuleScopeOpticalEntity : public UItemModuleEntity
{
	friend class AShooterWeapon;
	friend class AShooterWeapon;

	GENERATED_BODY()
	
public:

	UItemModuleScopeOpticalEntity();

	virtual void PostInitProperties() override;
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FORCEINLINE float GetScopeDistance() const { return Distance; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FORCEINLINE float GetScopeFOV(const float SourceFOV) const { return SourceFOV / (Distance / 50.0f); }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FORCEINLINE float GetScopeRenderTargetFOV(const float SourceFOV) const { return SourceFOV / (DistanceRenderTarget / 50.0f); }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FORCEINLINE bool IsUseRenderTarget() const { return bIsUseRenderTarget; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FORCEINLINE FName GetTextureParameterName() const { return TextureParameterName; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FORCEINLINE FName GetRenderTargetSocket() const { return RenderTargetSocket; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FORCEINLINE bool IsSupportLiveParams() const { return bIsSupportLiveParams; }	

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FORCEINLINE FVector GetAimOffset() const { return SubAimOffset; }	

protected:

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	FName TextureParameterName;

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	FName RenderTargetSocket;

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	bool bIsUseRenderTarget;

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	TArray<int32> ReplaceIndexes;

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	UMaterialInterface* ReplaceToMaterial;

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	float Distance;

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	float DistanceRenderTarget;

	UPROPERTY()
	UTextureRenderTarget2D* RenderTextureScope;

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	bool bIsSupportLiveParams;

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	FName TPN_MaxAmmo;

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	FName TPN_CurrentAmmo;

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	FName TPN_AllAmmo;

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	FVector SubAimOffset;
};
