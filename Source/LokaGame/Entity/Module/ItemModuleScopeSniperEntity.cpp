// VRSPRO

#include "LokaGame.h"
#include "ItemModuleScopeSniperEntity.h"


UItemModuleScopeSniperEntity::UItemModuleScopeSniperEntity()
	: Super()
{
	ModuleType = EItemModuleType::ScopeSniper;
}

void UItemModuleScopeSniperEntity::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UItemModuleScopeSniperEntity, Scope);
	DOREPLIFETIME(UItemModuleScopeSniperEntity, Background);
	DOREPLIFETIME(UItemModuleScopeSniperEntity, Distance);
}