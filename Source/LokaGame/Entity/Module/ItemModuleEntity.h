// VRSPRO

#pragma once

#include "Item/ItemBaseEntity.h"
#include "ItemModuleEntity.generated.h"


UENUM() // For in game shooter weapon
namespace EItemModuleType
{
	enum Type
	{
		ScopeColimator,
		ScopeSniper,
		ScopeOptical,
		TrunkSimple,
		TrunkFlameArrestor,
		TrunkSilent,
		End
	};
}

UCLASS()
class LOKAGAME_API UItemModuleEntity 
	: public UItemBaseEntity
{
	GENERATED_BODY()
	
public:
	UItemModuleEntity();

	UPROPERTY(EditDefaultsOnly, Category = Default)
	FName TargetSocket;	

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	TEnumAsByte<EItemModuleType::Type> GetModuleType() const
	{
		return ModuleType;
	}

	virtual bool IsSupportedForNetworking() const override
	{
		return true;
	}

protected:

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	TEnumAsByte<EItemModuleType::Type> ModuleType;
};
