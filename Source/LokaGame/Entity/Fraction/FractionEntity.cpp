// VRSPRO

#include "LokaGame.h"
#include "FractionEntity.h"

#include "Weapon/ItemWeaponEntity.h"
#include "Armour/ItemArmourEntity.h"
#include "Ammo/ItemAmmoEntity.h"
#include "Module/ItemModuleEntity.h"
#include "Grenade/ItemGrenadeEntity.h"
#include "StoreController/Models/FractionContainer.h"

UFractionEntity::UFractionEntity()
{
	EntityImage = FSlateNoResource();
	EntityDescription.CategoryType = ECategoryTypeId::Weapon;
	EntityDescription.SlotType = EItemSlotTypeId::PrimaryWeapon;
	EntityDescription.Name = FText::FromString("Undefined");
	EntityDescription.Description = FText::FromString("Undefined");
}

void UFractionEntity::Initialize(const FFractionContainer& initializer)
{
	EntityDescription.ModelId = static_cast<int32>(initializer.ModelId);
}


TArray<UItemBaseEntity*> UFractionEntity::GetItemsByLevel(const int32& Level, const bool IsShop) const
{
	TArray<UItemBaseEntity*> Result;

	//TODO: SeNTIke
	//for (auto i : WeaponMemberList)
	//{
	//	if (i->GetLevel() == Level && i->IsAllowToDisplay)
	//	{
	//		Result.Add(i);
	//	}
	//}
	//
	//for (auto i : ArmourMemberList)
	//{
	//	if (i->GetLevel() == Level && i->IsAllowToDisplay)
	//	{
	//		Result.Add(i);
	//	}
	//}
	//
	//for (auto i : AmmoMemberList)
	//{
	//	if (i->GetLevel() == Level && i->IsAllowToDisplay)
	//	{
	//		Result.Add(i);
	//	}
	//}
	//
	//if (IsShop == false)
	//{
	//	for (auto i : ModuleMemberList)
	//	{
	//		if (i->GetLevel() == Level && i->IsAllowToDisplay)
	//		{
	//			Result.Add(i);
	//		}
	//	}
	//}
	//
	//for (auto i : GrenadeMemberList)
	//{
	//	if (i->GetLevel() == Level && i->IsAllowToDisplay)
	//	{
	//		Result.Add(i);
	//	}
	//}
	//
	//for (auto i : ServiceMemberList)
	//{
	//	if (i->GetLevel() == Level && i->IsAllowToDisplay)
	//	{
	//		Result.Add(i);
	//	}
	//}

	return Result;
}
