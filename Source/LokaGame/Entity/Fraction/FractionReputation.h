#pragma once


#include "FractionReputation.generated.h"

enum class FractionTypeId : uint8;

USTRUCT()
struct FFractionReputation
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()		FLokaGuid EntityId;

	UPROPERTY()		FractionTypeId Id;
	
	UPROPERTY()		int32 CurrentRank;

	UPROPERTY()		int32 LastRank;

	UPROPERTY()		int32 Reputation;

	UPROPERTY()		int32 NextReputation;
};
