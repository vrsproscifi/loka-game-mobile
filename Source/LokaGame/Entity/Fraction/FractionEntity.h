// VRSPRO

#pragma once
#include "FractionRank.h"
#include "Entity/Item/AbstractItemEntity.h"
#include "FractionEntity.generated.h"

class UItemBaseEntity;

UCLASS(Blueprintable)
class LOKAGAME_API UFractionEntity 
	: public UAbstractItemEntity
{
	GENERATED_BODY()

public:
	UFractionEntity();

	//==================================================================
	//							[ Property ]
	//==================================================================
protected:
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TArray<FFractionRank> RankList;

public:
	const TArray<FFractionRank>& GetRankList() const
	{
		return RankList;
	}

	const TArray<FFractionRank>& GetRankList()
	{
		return RankList;
	}

public:
	const FFractionRank* GetRank(const uint32& rank) const
	{
		if(RankList.IsValidIndex(rank))
		{
			return &RankList[rank];
		}
		return &RankList.Last();
	}

	const FFractionRank* GetRank(const uint32& rank)
	{
		if (RankList.IsValidIndex(rank))
		{
			return &RankList[rank];
		}
		return &RankList.Last();
	}

public:
	UPROPERTY(Transient)		TArray<UItemBaseEntity*> Items;

	FractionTypeId GetModelId() const
	{
		return GetModelIdAs<FractionTypeId>();
	}

	//==================================================================
	//							[ Methods ]
	//==================================================================
	TArray<class UItemBaseEntity*> GetItemsByLevel(const int32&, const bool IsShop) const;
	void Initialize(const struct FFractionContainer& initializer);
};
