// VRSPRO

#pragma once
#include <Item/ItemBaseEntity.h>
#include "ItemAmmoEntity.generated.h"

UENUM(Blueprintable)
namespace EItemAmmoType
{
	enum Type
	{
		Instant,
		Scintilla,
		Projectile,
		End UMETA(Hidden)
	};
}

USTRUCT(Blueprintable)
struct FAmmoPropertyBase
{
	GENERATED_USTRUCT_BODY()

	/**Armor-Piercing for calculate damage, in future */
	UPROPERTY(EditDefaultsOnly)
	float Piercing;

	/**Damage type apply on hit, need for detect damage type (Explosion, Momentum) */
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UDamageType> DamageType;

	FAmmoPropertyBase() : Piercing(.1f), DamageType(UDamageType::StaticClass()) {}
};

USTRUCT(Blueprintable)
struct FAmmoPropertyInstant : public FAmmoPropertyBase
{
	GENERATED_USTRUCT_BODY()

	/**Distance limit for bullet in meters */
	UPROPERTY(EditDefaultsOnly, meta = (UIMin = 10.0f, UIMax = 2000.0f))
	float Distance;

	/**Effect used on hit, template */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	TSubclassOf<class AShooterImpactEffect> ImpactEffect;

	UPROPERTY(EditDefaultsOnly, Category = Effects)
	UParticleSystem* TraceFX;

	UPROPERTY(EditDefaultsOnly, Category = Effects)
	FVector TraceFXScale;

	FAmmoPropertyInstant() 
		: FAmmoPropertyBase()
		, ImpactEffect() 
		, Distance(10.0f)
		, TraceFXScale(1.0f)
	{

	}
};

USTRUCT(Blueprintable)
struct FAmmoPropertyScintilla : public FAmmoPropertyInstant
{
	GENERATED_USTRUCT_BODY()
		
	/**Minimum count of scintilla */
	UPROPERTY(EditDefaultsOnly, meta = (UIMin = 1, UIMax = 80))
	int32 Min;

	/**Maximum count of scintilla */
	UPROPERTY(EditDefaultsOnly, meta = (UIMin = 10, UIMax = 100))
	int32 Max;

	/**Grouping of shot in degrees */
	UPROPERTY(EditDefaultsOnly, meta = (UIMin = 1.0f, UIMax = 50.0f))
	float Grouping;

	FAmmoPropertyScintilla() : FAmmoPropertyInstant(), Min(10), Max(40), Grouping(5.0f) {}
};

USTRUCT(Blueprintable)
struct FAmmoPropertyProjectile : public FAmmoPropertyBase
{
	GENERATED_USTRUCT_BODY()

	/**Radius of explosion in meters */
	UPROPERTY(EditDefaultsOnly, meta = (UIMin = 1.0f, UIMax = 50.0f))
	float Radius;

	/**Projectile template */
	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	TSubclassOf<class AUTProjectile> Projectile;

	FAmmoPropertyProjectile() : FAmmoPropertyBase(), Radius(5.0f), Projectile() {}
};

UCLASS(Blueprintable)
class LOKAGAME_API UItemAmmoEntity
	: public UItemBaseEntity
{
	GENERATED_BODY()

public:
	UItemAmmoEntity();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FORCEINLINE TEnumAsByte<EItemAmmoType::Type> GetAmmoType() const { return AmmoType; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FORCEINLINE FAmmoPropertyInstant GetPropertyInstant() const { return PropertyInstant; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FORCEINLINE FAmmoPropertyScintilla GetPropertyScintilla() const { return PropertyScintilla; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FORCEINLINE FAmmoPropertyProjectile GetPropertyProjectile() const { return PropertyProjectile; }

protected:
	/**Ammo type */
	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	TEnumAsByte<EItemAmmoType::Type> AmmoType;

	/**PropertyInstant used if AmmoType == Instant */
	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	FAmmoPropertyInstant PropertyInstant;

	/**PropertyScintilla used if AmmoType == Scintilla */
	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	FAmmoPropertyScintilla PropertyScintilla;

	/**PropertyProjectile used if AmmoType == Projectile */
	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	FAmmoPropertyProjectile PropertyProjectile;

#if WITH_EDITOR
	virtual bool CanEditChange(const UProperty* InProperty) const override;
#endif
};
