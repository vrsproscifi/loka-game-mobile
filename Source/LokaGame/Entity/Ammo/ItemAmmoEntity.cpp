// VRSPRO

#include "LokaGame.h"
#include "ItemAmmoEntity.h"

UItemAmmoEntity::UItemAmmoEntity()
{
	EntityDescription.CategoryType = CategoryTypeId::Ammo;
}

#if WITH_EDITOR
bool UItemAmmoEntity::CanEditChange(const UProperty* InProperty) const
{
	bool bIsEditable = Super::CanEditChange(InProperty);
	if (bIsEditable && InProperty)
	{
		const FName PropertyName = InProperty->GetFName();

		if (PropertyName == GET_MEMBER_NAME_CHECKED(UItemAmmoEntity, PropertyInstant))
		{
			bIsEditable = (AmmoType == EItemAmmoType::Instant);
		}
		else if (PropertyName == GET_MEMBER_NAME_CHECKED(UItemAmmoEntity, PropertyScintilla))
		{
			bIsEditable = (AmmoType == EItemAmmoType::Scintilla);
		}
		else if (PropertyName == GET_MEMBER_NAME_CHECKED(UItemAmmoEntity, PropertyProjectile))
		{
			bIsEditable = (AmmoType == EItemAmmoType::Projectile);
		}
	}

	return bIsEditable;
}
#endif