// VRSPRO

#pragma once

#include "Weapon/ItemWeaponModelId.h"
#include "DuelStructures.generated.h"

UENUM(BlueprintType)
namespace EDuelWeaponTypes
{
	enum Type
	{
		All,
		Shotguns,
		Rifles,
		Snipers,
		Pistols,
		LMGs,
		SMGs,
		End UMETA(Hidden)
	};
}

USTRUCT(BlueprintType)
struct FDuelWeapons
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditAnywhere)
	TArray<TEnumAsByte<ItemWeaponModelId::Type>> Supported;

	static FText GetText(const EDuelWeaponTypes::Type Value)
	{
		switch (Value)
		{
			case EDuelWeaponTypes::All: return NSLOCTEXT("EDuelWeaponTypes", "EDuelWeaponTypes.All", "Randomly");
			case EDuelWeaponTypes::Shotguns: return NSLOCTEXT("EDuelWeaponTypes", "EDuelWeaponTypes.Shotguns", "Shotguns");
			case EDuelWeaponTypes::Rifles: return NSLOCTEXT("EDuelWeaponTypes", "EDuelWeaponTypes.Rifles", "Assault Rifles");
			case EDuelWeaponTypes::Snipers: return NSLOCTEXT("EDuelWeaponTypes", "EDuelWeaponTypes.Snipers", "Sniper Rifles");
			case EDuelWeaponTypes::Pistols: return NSLOCTEXT("EDuelWeaponTypes", "EDuelWeaponTypes.Pistols", "Pistols");
			case EDuelWeaponTypes::LMGs: return NSLOCTEXT("EDuelWeaponTypes", "EDuelWeaponTypes.LMGs", "Light Machine Guns");
			case EDuelWeaponTypes::SMGs: return NSLOCTEXT("EDuelWeaponTypes", "EDuelWeaponTypes.SMGs", "Sub-machine Guns");
			default:
				break;
		}

		return FText::GetEmpty();
	}
};