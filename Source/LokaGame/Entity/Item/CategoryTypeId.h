#pragma once

#include "CategoryTypeId.generated.h"

UENUM(BlueprintType)
namespace CategoryTypeId
{
	enum Type
	{
		Weapon,
		Armour,
		Ammo,
		Service,
		Addon,
		Character,
		Profile,
		Material,

		Kit,	// Old achivemt?
		Grenade,

		Building,

		Currency,

		Transport,

		End UMETA(Hidden)
	};
}

typedef CategoryTypeId::Type ECategoryTypeId;

struct FCategoryTypeId
{
	static FText GetText(const ECategoryTypeId Type)
	{
		switch (Type)
		{
			case ECategoryTypeId::Weapon: 
				return NSLOCTEXT("ECategoryTypeId", "ECategoryTypeId.Weapon", "Weapon");
			case ECategoryTypeId::Armour: 
				return NSLOCTEXT("ECategoryTypeId", "ECategoryTypeId.Armour", "Armour");
			case ECategoryTypeId::Ammo: 
				return NSLOCTEXT("ECategoryTypeId", "ECategoryTypeId.Ammo", "Ammo");
			case ECategoryTypeId::Service: 
				return NSLOCTEXT("ECategoryTypeId", "ECategoryTypeId.Service", "Service");
			case ECategoryTypeId::Addon:
				return NSLOCTEXT("ECategoryTypeId", "ECategoryTypeId.Addon", "Addon");
			case ECategoryTypeId::Character:
				return NSLOCTEXT("ECategoryTypeId", "ECategoryTypeId.Character", "Character");
			case ECategoryTypeId::Profile:
				return NSLOCTEXT("ECategoryTypeId", "ECategoryTypeId.Profile", "Profile");
			case ECategoryTypeId::Material:
				return NSLOCTEXT("ECategoryTypeId", "ECategoryTypeId.Material", "Material");
			//case ECategoryTypeId::Achievement:
			//	return NSLOCTEXT("ECategoryTypeId", "ECategoryTypeId.Achievement", "Achievement");
			case ECategoryTypeId::Grenade:
				return NSLOCTEXT("ECategoryTypeId", "ECategoryTypeId.Grenade", "Grenade");
			case ECategoryTypeId::Transport:
				return NSLOCTEXT("ECategoryTypeId", "ECategoryTypeId.Transport", "Transport");
			default: 
				return FText::GetEmpty();
		}
	}
};

