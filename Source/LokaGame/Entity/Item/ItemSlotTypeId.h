#pragma once
#include "ItemSlotTypeId.generated.h"


UENUM()
namespace ItemSlotTypeId
{
	enum Type
	{
		None,

		Character,

		Helmet,
		Mask,

		Gloves,
		Backpack,
		Body,

		Pants,
		Boots,

		PrimaryWeapon,
		SecondaryWeapon,

		Grenade,
		Kit,

		End UMETA(Hidden)
	};
}

typedef ItemSlotTypeId::Type EItemSlotTypeId;