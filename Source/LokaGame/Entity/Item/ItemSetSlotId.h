#pragma once
#include "ItemSlotTypeId.h"

namespace EItemSetSlotId
{
	enum Type
	{
		PrimaryWeapon = EItemSlotTypeId::PrimaryWeapon,
		SecondaryWeapon = EItemSlotTypeId::SecondaryWeapon,
		Head = EItemSlotTypeId::Helmet,
		Mask = EItemSlotTypeId::Mask,
		Gloves = EItemSlotTypeId::Gloves,
		Body = EItemSlotTypeId::Body,
		Backpack = EItemSlotTypeId::Backpack,
		Pants = EItemSlotTypeId::Pants,
		Bots = EItemSlotTypeId::Boots,
		Explosive_1 = EItemSlotTypeId::Grenade,
		Explosive_2,
		Explosive_3,
		Other_1,
		Other_2,
		Other_3,
		End
	};
}
