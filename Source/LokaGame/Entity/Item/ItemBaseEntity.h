#pragma once

#include "ItemModelInfo.h"
#include "Animation/AnimInstance.h"


#include "TypeDescription.h"


#include "IdentityController/Models/ModificationItemWrapper.h"
#include "Material/ItemMaterialModelId.h"
#include "Animation/AnimInstance.h"
#include "Animation/AnimMontage.h"
#include "TypeRegeneration.h"
#include "SResourceTextBoxValue.h"
#include "ItemBaseEntity.generated.h"

class UPlayerInventoryItem;
struct FRequestExecuteError;
enum class ERequestErrorCode : uint8;
struct FEditorItemProperty;
class FOperationRequest;
USTRUCT(Blueprintable)
struct FArmourProperty
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		FTypeRegeneration Health;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		FTypeRegeneration Armour;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		FTypeRegeneration Energy;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		FTypeRegeneration Stamina;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		FTypeRegeneration Speed;

};

//	TODO: Rename to FCharacterAnim
USTRUCT(BlueprintType)
struct FWeaponAnim
{
	GENERATED_USTRUCT_BODY()

	/** animation played on pawn (1st person view) */
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
	UAnimMontage* Pawn1P;

	/** animation played on pawn (3rd person view) */
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
	UAnimMontage* Pawn3P;
};

USTRUCT(BlueprintType)
struct FTwoAnimSequence
{
	GENERATED_USTRUCT_BODY()

	/** animation played on pawn (1st person view) */
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
	UAnimSequence* Pawn1P;

	/** animation played on pawn (3rd person view) */
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
	UAnimSequence* Pawn3P;
};

USTRUCT(BlueprintType)
struct FCaptureImageSettings
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Configuration)	float		DistanceMultipler;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Configuration)	FTransform	TransformOffsets;

	FCaptureImageSettings() : DistanceMultipler(1.8f), TransformOffsets(FTransform::Identity) {}

	bool IsValid() const { return DistanceMultipler != 1.8f || TransformOffsets.Equals(FTransform::Identity) == false; }
};

namespace EValueDisplayAs
{
	enum Type
	{
		Float,
		Meters,
		Kilograms,
		FireRate,
		Degres,
		Seconds,
		Percent,
		End
	};
}

struct FDisplayParam
{
	FText Name;
	float Value;
	EValueDisplayAs::Type DisplayAs;

	FDisplayParam() : Name(), Value(.0f), DisplayAs(EValueDisplayAs::Float) {}
	FDisplayParam(const FText& _Name, const float _Value) : Name(_Name), Value(_Value), DisplayAs(EValueDisplayAs::Float) {}
	FDisplayParam(const FText& _Name, const float _Value, const EValueDisplayAs::Type _DisplayAs) : Name(_Name), Value(_Value), DisplayAs(_DisplayAs) {}
};

struct FResourceTextBoxValue;
class UItemMaterialEntity;
class UFractionEntity;

UCLASS(Blueprintable, Abstract, hidecategories=Object)
class LOKAGAME_API UItemBaseEntity
	: public UObject
{
	friend class AAssetCaptureScene;
	friend class ULokaEditorEngine;
	friend class ULKBlueprintFunctionLibrary;
	friend class UGameSingleton;

#if WITH_EDITOR
	friend class FItemEntitySaveLoadDetails;
#endif

	GENERATED_BODY()

	//=====================================
	//	Description
protected:
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Description, meta = (ShowOnlyInnerProperties))
	FTypeDescription EntityDescription;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Description)
	FSlateBrush EntityImage;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Description)
	FSlateBrush EntityIcon;
	
	UPROPERTY()
	UFractionEntity* Fraction;

public:
	virtual void Initialize(const struct FItemBaseContainer& initializer);

	FItemModelInfo GetModelInfo() const
	{
		return FItemModelInfo(GetModelId(), GetCategory());
	}

	void SetFraction(UFractionEntity* fraction)
	{
		Fraction = fraction;
	}

	UFractionEntity* GetFraction() const;

	const int32& GetModelId() const
	{
		return EntityDescription.ModelId;
	}

	template<typename TInstanceModelId>
	const TInstanceModelId& GetModelId() const
	{
		return static_cast<TInstanceModelId>(GetModelId());
	}

	const FTypeDescription& GetDescription() const
	{
		return EntityDescription;
	}

	bool IsObjectEqual(const TEnumAsByte<CategoryTypeId::Type>& category, const int32& model = INDEX_NONE) const
	{
		return  GetCategory() == category && (GetModelId() == model || model == INDEX_NONE);
	}

	const TEnumAsByte<CategoryTypeId::Type>& GetCategory() const
	{
		return EntityDescription.CategoryType;
	}

	FResourceTextBoxValue GetCostAsResource() const
	{
		return EntityDescription.Cost.GetCostAsResource();
	}

	const FTypeCost& GetCost() const
	{
		return EntityDescription.Cost;
	}

	const FTypeCost& GetSubscribeCost() const
	{
		return EntityDescription.SubscribeCost;
	}

	const FractionTypeId& GetFractionId() const
	{
		return EntityDescription.FractionId;
	}

	const FSlateBrush* GetImage() const
	{
		return &EntityImage;
	}

	const FSlateBrush* GetIcon() const
	{
		return &EntityIcon;
	}

	const uint8& GetLevel() const
	{
		return EntityDescription.Level;
	}

	FText GetLevelAsText() const
	{
		return FText::AsNumber(EntityDescription.Level);
	}

	//=======================================================
	//	Name

	FORCEINLINE_DEBUGGABLE const FText& GetItemNameRef() const
	{
		return GetDescription().Name;
	}

	FORCEINLINE_DEBUGGABLE FText GetItemName() const
	{
		return GetDescription().Name;
	}

	//=======================================================
	//	Desc

	FORCEINLINE_DEBUGGABLE const FText& GetItemDescriptionRef() const
	{
		return GetDescription().Description;
	}

	FORCEINLINE_DEBUGGABLE FText GetItemDescription() const
	{
		return GetDescription().Description;
	}

	//==================================================================
	//							[ Property ]
	//==================================================================
public:

	UItemBaseEntity();
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Description)		bool IsAllowToDisplay;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Core)
	virtual bool IsValidItem() const { return false; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Animation)
	FORCEINLINE	TSubclassOf<UAnimInstance> GetAnimClass() const { return AnimClass; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Mesh)
	USkeletalMesh* GetSkeletalMesh() const;

	virtual TArray<FDisplayParam> GetDisplayParams() const
	{ 
		TArray<FDisplayParam> Values;
		return Values;
	}

	virtual TArray<FDisplayParam> GetDisplayModifers() const
	{
		TArray<FDisplayParam> Values;
		return Values;
	}

	virtual void ApplyMaterial(UItemMaterialEntity*);
	virtual void ApplyMaterial(const FString&);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Mesh)
	UMaterialInterface* GetMaterialByModel(const int32 InModelId) const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Mesh)
	UMaterialInterface* GetCurrentMaterial() const;
	UItemMaterialEntity* GetCurrentItemMaterial() const;

	virtual void PostInitProperties() override;

	void CaptureThumbnail(UWorld* World);

	UPROPERTY()
	TArray<class UItemMaterialEntity*> SkinList;


public:

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Description)
	FArmourProperty Property;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Description)
	bool IsAllowToBuy;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Description)
	FORCEINLINE float GetRewardBonus() const
	{
		return RewardBonus;
	}

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Mesh)
	FVector RelativeLocationInCapturing;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
	TSubclassOf<UPlayerInventoryItem> GetInventoryClass() const { return InventoryClass; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Description)
	FORCEINLINE float GetMass() const { return ItemMass; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = CaptureImage)
	FORCEINLINE FCaptureImageSettings GetCaptureImageSettings() const { return CaptureImageSettings; }

protected:

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Mesh)
	USkeletalMesh* SkeletalMesh;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = CaptureImage)
	FCaptureImageSettings CaptureImageSettings;

	// Temp here, for calculate mass into item while editing.
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Description, meta = (DisplayName = "Mass In Kg"))
	float ItemMass;

	UPROPERTY(EditDefaultsOnly, Category = Mesh)
	UMaterialInterface* SkeletalMeshMaterials[ItemMaterialModelId::End];

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
	TSubclassOf<UAnimInstance> AnimClass;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Mesh)
	UTextureRenderTarget2D* RenderTexture;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Mesh)
	UMaterialInstanceDynamic* RenderMaterial;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Mesh)
	UMaterialInterface* RenderMaterialInst;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Inventory)
	TSubclassOf<UPlayerInventoryItem> InventoryClass;

	UPROPERTY()
	UItemMaterialEntity* SkinMaterial;

	UPROPERTY(EditDefaultsOnly, Category = Description)
	float RewardBonus;

	virtual void PostDuplicate(bool bDuplicateForPIE) override;

#if WITH_EDITOR
private:
	TSharedPtr<FOperationRequest>	LoadItem;
	TSharedPtr<FOperationRequest>	UpdateItem;

	void OnLoadItemSuccessfully(const FEditorItemProperty& prop);
	void OnLoadItemFailed(const FRequestExecuteError& error);

	void OnUpdateItemSuccessfully(const FEditorItemProperty& prop);
	void OnUpdateItemFailed(const FRequestExecuteError& error);

public:

	bool bEntityActionInProgress;
	bool bEntityActionFail;
	bool bEntityWasLoaded;

	virtual void OnProxyRequestLoad();
	virtual void OnProxyRequestSave();
#endif


public:
	//==================================================================
	//						Predicates

	//	���������� �� ����������� ������
	static bool SortByLevelAsc(UItemBaseEntity& a, UItemBaseEntity& b)
	{
		return a.GetLevel() < b.GetLevel();
	}

	//	���������� �� �������� ������
	static bool SortByLevelDesc(UItemBaseEntity& a, UItemBaseEntity& b)
	{
		return a.GetLevel() > b.GetLevel();
	}
};
