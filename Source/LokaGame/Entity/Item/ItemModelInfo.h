#pragma once
#include "Item/CategoryTypeId.h"
#include "ItemModelInfo.generated.h"

USTRUCT()
struct FItemModelInfo
{
	GENERATED_USTRUCT_BODY()

	//==========================
	//			[ Property ]
	UPROPERTY(EditAnywhere)	int32 ModelId;
	UPROPERTY(EditAnywhere)	TEnumAsByte<CategoryTypeId::Type> CategoryTypeId;

	FItemModelInfo(){}
	FItemModelInfo(const uint32& model, const TEnumAsByte<CategoryTypeId::Type>& category)
		: ModelId(model)
		, CategoryTypeId(category)
	{
		
	}

};