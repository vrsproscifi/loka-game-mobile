// VRSPRO

#include "LokaGame.h"
#include "Material/ItemMaterialEntity.h"
#include "StoreController/Models/ItemBaseContainer.h"

#include "SlateMaterialBrush.h"
#include "AssetCaptureScene.h"
#include "RequestManager/RequestManager.h"
#include "LoadItemRequest.h"
#include "EditorItemProperty.h"
#include "PlayerInventoryItem.h"
#include "Fraction/FractionEntity.h"

//NewObject<UTextureRenderTarget2D>(GetTransientPackage(), NAME_None, RF_Transient);

UItemBaseEntity::UItemBaseEntity() 
	: Super()
	, IsAllowToDisplay(true)
{
	InventoryClass = UPlayerInventoryItem::StaticClass();
	IsAllowToBuy = false;
	EntityDescription.ModelId = INDEX_NONE;

#if WITH_EDITOR
	LoadItem = FOperationRequest::Factory(this, FServerHost::MasterClient, "Editor/LoadItem");
	LoadItem->BindObject<FEditorItemProperty>(200).AddUObject(this, &UItemBaseEntity::OnLoadItemSuccessfully);
	LoadItem->OnFailedResponse.BindUObject(this, &UItemBaseEntity::OnLoadItemFailed);

	UpdateItem = FOperationRequest::Factory(this, FServerHost::MasterClient, "Editor/UpdateItem");
	UpdateItem->BindObject<FEditorItemProperty>(200).AddUObject(this, &UItemBaseEntity::OnUpdateItemSuccessfully);
	UpdateItem->OnFailedResponse.BindUObject(this, &UItemBaseEntity::OnUpdateItemFailed);

	bEntityActionInProgress = false;
	bEntityActionFail = false;
	bEntityWasLoaded = false;

#endif

	if (!HasAnyFlags(RF_ClassDefaultObject | RF_ArchetypeObject))
	{
		static ConstructorHelpers::FObjectFinder<UMaterialInterface> RenderMatOb(TEXT("/Game/1LOKAgame/Blueprints/Util/NewTextureRenderTarget2D_Mat_Inst"));
		RenderMaterialInst = RenderMatOb.Object;
	}
}

USkeletalMesh* UItemBaseEntity::GetSkeletalMesh() const
{
	return GetValidObject(SkeletalMesh);
}

void UItemBaseEntity::PostInitProperties()
{
	Super::PostInitProperties();

	//if (!RenderTexture && GetSkeletalMesh())
	//{
	//	TGuardValue<bool> GuardTemplateNameFlag(GIsReconstructingBlueprintInstances, true);

	//	RenderTexture = NewObject<UTextureRenderTarget2D>(this);
	//	RenderTexture->InitAutoFormat(1024.f, 512.f);
	//	RenderTexture->CompressionSettings = TextureCompressionSettings::TC_EditorIcon;
	//	RenderTexture->LODGroup = TextureGroup::TEXTUREGROUP_UI;
	//	//RenderTexture->MipGenSettings = TextureMipGenSettings::TMGS_NoMipmaps;
	//	//RenderTexture->PowerOfTwoMode = ETexturePowerOfTwoSetting::PadToSquarePowerOfTwo;
	//	RenderTexture->OverrideFormat = EPixelFormat::PF_FloatRGB;
	//}
}

void UItemBaseEntity::PostDuplicate(bool bDuplicateForPIE)
{
	Super::PostDuplicate(bDuplicateForPIE);

	if (GetSkeletalMesh())
	{
		TGuardValue<bool> GuardTemplateNameFlag(GIsReconstructingBlueprintInstances, true);

		RenderTexture = NewObject<UTextureRenderTarget2D>(this);
		RenderTexture->InitAutoFormat(1024.f, 512.f);
		RenderTexture->CompressionSettings = TextureCompressionSettings::TC_EditorIcon;
		RenderTexture->LODGroup = TextureGroup::TEXTUREGROUP_UI;
		RenderTexture->OverrideFormat = EPixelFormat::PF_FloatRGB;
		//
		if (!RenderMaterial)
		{
			RenderMaterial = UMaterialInstanceDynamic::Create(RenderMaterialInst, this);
		}

		RenderMaterial->SetTextureParameterValue(TEXT("Texture"), RenderTexture);

		EntityImage.ImageType = ESlateBrushImageType::FullColor;
		EntityImage.DrawAs = ESlateBrushDrawType::Image;
		EntityImage.SetResourceObject(RenderMaterial);
		EntityImage.ImageSize = FVector2D(RenderTexture->SizeX, RenderTexture->SizeY);
	}
}

void UItemBaseEntity::CaptureThumbnail(UWorld* World)
{
	if (World && GetSkeletalMesh())
	{
		TGuardValue<bool> GuardTemplateNameFlag(GIsReconstructingBlueprintInstances, true);

		if (!RenderTexture)
		{
			RenderTexture = NewObject<UTextureRenderTarget2D>(this);
			RenderTexture->InitAutoFormat(1024.f, 512.f);
			RenderTexture->CompressionSettings = TextureCompressionSettings::TC_EditorIcon;
			RenderTexture->LODGroup = TextureGroup::TEXTUREGROUP_UI;
			RenderTexture->OverrideFormat = EPixelFormat::PF_FloatRGB;
		}

		TArray<AActor*> Actors;
		UGameplayStatics::GetAllActorsOfClass(World, AAssetCaptureScene::StaticClass(), Actors);

		if (!RenderMaterial)
		{
			RenderMaterial = UMaterialInstanceDynamic::Create(RenderMaterialInst, this);			
		}

		RenderMaterial->SetTextureParameterValue(TEXT("Texture"), RenderTexture);
		
		EntityImage.ImageType = ESlateBrushImageType::FullColor;
		EntityImage.DrawAs = ESlateBrushDrawType::Image;
		EntityImage.SetResourceObject(RenderMaterial);
		EntityImage.ImageSize = FVector2D(RenderTexture->SizeX, RenderTexture->SizeY);

		if (Actors.Num())
		{
			auto TargetActor = Cast<AAssetCaptureScene>(Actors[0]);
			TargetActor->BeginCapture(this);
		}
	}
}

void UItemBaseEntity::Initialize(const FItemBaseContainer& initializer)
{
	this->EntityDescription.Level = initializer.Level;
	this->EntityDescription.FractionId = initializer.FractionId;
	this->EntityDescription.Cost = initializer.Cost;
	this->EntityDescription.CharacterModelFlagId = initializer.CharacterModelsFlag;
}

UFractionEntity* UItemBaseEntity::GetFraction() const
{
	return GetValidObject(Fraction);
}

void UItemBaseEntity::ApplyMaterial(UItemMaterialEntity* Material)
{
	SkinMaterial = Material;
}

void UItemBaseEntity::ApplyMaterial(const FString& StrId)
{
	FGuid guid;
	FGuid::Parse(StrId, guid);

	//TODO: SeNTIke

	//for (auto &s : SkinList)
	//{
	//	if (s->GetEntityId() == guid)
	//	{
	//		ApplyMaterial(s);
	//		break;
	//	}
	//}
}

UMaterialInterface* UItemBaseEntity::GetMaterialByModel(const int32 InModelId) const
{
	if (InModelId < ItemMaterialModelId::End && SkeletalMeshMaterials[InModelId])
	{
		return GetValidObject(SkeletalMeshMaterials[InModelId]);
	}
	return nullptr;
}

UMaterialInterface* UItemBaseEntity::GetCurrentMaterial() const
{
	//TODO: SeNTIke
	//if (SkinMaterial)
	//{
	//	return SkeletalMeshMaterials[SkinMaterial->GetInstanceModelId()];
	//}

	return SkeletalMeshMaterials[0];
}

UItemMaterialEntity* UItemBaseEntity::GetCurrentItemMaterial() const
{
	if (SkinMaterial)
	{
		return SkinMaterial;
	}
	else if (SkinList.Num())
	{
		return SkinList[0];
	}

	return nullptr;
}

#if WITH_EDITOR 

void UItemBaseEntity::OnProxyRequestLoad()
{
	//auto request = FLoadItemRequest(GetModelId(), GetCategory());
	//LoadItem->SendRequestObject<FLoadItemRequest>(request);
	bEntityActionInProgress = true;
	UE_LOG(LogCore, Warning, TEXT("UItemBaseEntity::OnProxyRequestLoad >> %s"), *GetFName().ToString());
}

void UItemBaseEntity::OnProxyRequestSave()
{
	if (!bEntityWasLoaded)
	{
		bEntityActionFail = true;
		return;
	}

	FEditorItemProperty p;

	p.ModelId = GetModelId();
	p.CategoryTypeId = GetCategory();


	p.FractionId = GetFractionId();
	p.Level = GetLevel();


	p.Cost = GetCost();
	p.SubscribeCost = GetSubscribeCost();
	p.DefaultAmount = GetDescription().DefaultAmount;


	p.Performance = GetDescription().Perfomance;
	p.Storage = GetDescription().Storage;

	UpdateItem->SendRequestObject<FEditorItemProperty>(p);

	bEntityActionInProgress = true;
	UE_LOG(LogCore, Warning, TEXT("UItemBaseEntity::OnProxyRequestSave >> %s"), *GetFName().ToString());
}

void UItemBaseEntity::OnLoadItemSuccessfully(const FEditorItemProperty& prop)
{
	EntityDescription.DefaultAmount = prop.DefaultAmount;
	EntityDescription.SubscribeCost = prop.SubscribeCost;
	EntityDescription.Cost = prop.Cost;

	EntityDescription.FractionId = prop.FractionId;
	EntityDescription.Level = prop.Level;

	EntityDescription.Perfomance = prop.Performance;
	EntityDescription.Storage = prop.Storage;

	bEntityActionInProgress = false;
	bEntityActionFail = false;

	bEntityWasLoaded = true;
}

void UItemBaseEntity::OnLoadItemFailed(const FRequestExecuteError& error)
{
	bEntityActionInProgress = false;
	bEntityActionFail = true;
}


void UItemBaseEntity::OnUpdateItemSuccessfully(const FEditorItemProperty& prop)
{
	bEntityActionInProgress = false;	
	bEntityActionFail = false;
}

void UItemBaseEntity::OnUpdateItemFailed(const FRequestExecuteError& error)
{
	bEntityActionInProgress = false;
	bEntityActionFail = true;
}

#endif