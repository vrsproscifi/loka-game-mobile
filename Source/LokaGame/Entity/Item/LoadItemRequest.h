#pragma once
#include "Entity/Item/CategoryTypeId.h"
#include "LoadItemRequest.generated.h"

USTRUCT()
struct FLoadItemRequest
{
	GENERATED_USTRUCT_BODY()

	//==========================
	//			[ Property ]

	UPROPERTY()	uint32 ModelId;
	UPROPERTY()	TEnumAsByte<CategoryTypeId::Type> CategoryTypeId;

	//==========================
	//			[ Constructor ]

	FLoadItemRequest()
	{

	}

	FLoadItemRequest(const uint32 modelId, const TEnumAsByte<CategoryTypeId::Type>& categoryTypeId)
		: ModelId(modelId)
		, CategoryTypeId(categoryTypeId)
	{

	}

	FLoadItemRequest(const int32 modelId, const TEnumAsByte<CategoryTypeId::Type>& categoryTypeId)
		: ModelId(static_cast<int32>(modelId))
		, CategoryTypeId(categoryTypeId)
	{

	}

};