// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Entity/Item/ItemBaseEntity.h"
#include "ItemBuildModelId.h"
#include "ItemBuildEntity.generated.h"

class ABuildPlacedComponent;

UENUM(BlueprintType, Meta = (Bitflags))
namespace EBuildingType
{
	enum Type
	{
		None,
		WarehouseMoney,
		MinerMoney,
		End
	};
}
ENUM_CLASS_FLAGS(EBuildingType::Type)

UCLASS()
class LOKAGAME_API UItemBuildEntity 
	: public UItemBaseEntity
{
	GENERATED_BODY()
	
public:

	UItemBuildEntity();
	virtual void Initialize(const struct FItemBaseContainer& initializer);

	UPROPERTY(EditDefaultsOnly, Category = Default)
	TSubclassOf<ABuildPlacedComponent> ComponentTemplate;


	virtual bool IsSupportedForNetworking() const override
	{
		return true;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Default, Meta = (Bitmask, BitmaskEnum = "EBuildingType"))
	FORCEINLINE int32 GetBuildingType() const { return BuildingType; }
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Default)
	FORCEINLINE FText GetCategoryName() const { return BuildingCategory; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Default)
	FORCEINLINE FText GetSubCategoryName() const { return BuildingSubCategory; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Default)
	TMap<FName, UMaterialInterface*> GetReplaceMaterials() const { return ReplaceMaterials; }

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Default, Meta = (Bitmask, BitmaskEnum = "EBuildingType"))
	int32 BuildingType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Default)
	FText BuildingCategory;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Default)
	FText BuildingSubCategory;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Default)
	TMap<FName, UMaterialInterface*> ReplaceMaterials;
};
