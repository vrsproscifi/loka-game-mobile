// VRSPRO

#pragma once
#include "ItemArmourEntity.h"
#include "ItemArmourBody.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class LOKAGAME_API UItemArmourBody
	: public UItemArmourEntity
{
	GENERATED_BODY()

public:
	UItemArmourBody();

	/** effect played on respawn */
	UPROPERTY(VisibleDefaultsOnly, Category = Pawn)
	UParticleSystemComponent* SheildFXComp;
};
