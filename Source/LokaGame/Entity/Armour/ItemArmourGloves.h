// VRSPRO

#pragma once
#include "ItemArmourEntity.h"
#include "ItemArmourGloves.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class LOKAGAME_API UItemArmourGloves
	: public UItemArmourEntity
{
	GENERATED_BODY()

public:
	UItemArmourGloves();
};
