// VRSPRO

#pragma once
#include "ItemArmourEntity.h"
#include "ItemArmourBoots.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class LOKAGAME_API UItemArmourBoots
	: public UItemArmourEntity
{
	GENERATED_BODY()

public:
	UItemArmourBoots();
};
