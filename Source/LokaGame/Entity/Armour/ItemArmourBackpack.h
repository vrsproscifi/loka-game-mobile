// VRSPRO

#pragma once
#include "ItemArmourEntity.h"
#include "ItemArmourBackpack.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class LOKAGAME_API UItemArmourBackpack
	: public UItemArmourEntity
{
	GENERATED_BODY()

public:
	UItemArmourBackpack();
};
