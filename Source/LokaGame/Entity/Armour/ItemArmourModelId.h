#pragma once

#include "ItemArmourModelId.generated.h"

UENUM(BlueprintType, Category = "Repository | Entity | Armour")
namespace ItemArmourModelId
{
	enum Type
	{
		HelmetB1,
		BodyB1,
		BackpackB1,
		PantsB1,
		HelmetR1,
		HelmetR2,	//5
		BodyR2,		//6
		PantsR2,	//7
		GlovesR2,	//8
		KeepersR2_Jacket,
		KeepersR2_Pants,
		KeepersR2_Gloves,
		Keepers_H07Helmet,
		Keepers_H08Helmet,
		Bonus_DaftPunkHelmet1,
		Bonus_DaftPunkHelmet2,
		Bonus_DonaldTrump,
		Bonus_Unicorn,
		Bonus_Baby,
		Bonus_Goblin,
		KeepersR3_Jacket,
		KeepersR3_Pants,
		KeepersR3_Gloves,
		KeepersR4_Jacket,
		KeepersR4_Pants,
		KeepersR4_Gloves,
		KeepersR5_Jacket,
		KeepersR5_Pants,
		KeepersR5_Gloves,
		KeepersR6_Jacket,
		KeepersR6_Pants,
		KeepersR6_Gloves,

		// Need add to db
		Bonus_Bane,
		Bonus_Bizon,
		Bonus_Bull,
		Bonus_ChinaGuy,
		Bonus_ChinaBoss,
		Bonus_Chuk,
		Bonus_Cow,
		Bonus_Deer,
		Bonus_Gorilla,
		Bonus_Guard,
		Bonus_Joker,
		Bonus_OldMan,
		Bonus_Pig,
		Bonus_Protivogaz,
		Bonus_Shaokan,
		Bonus_SnoopDog,
		Bonus_Triceratops,

		End UMETA(Hidden)
	};
}
