// VRSPRO

#pragma once
#include "ItemArmourEntity.h"
#include "ItemArmourPants.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class LOKAGAME_API UItemArmourPants
	: public UItemArmourEntity
{
	GENERATED_BODY()

public:
	UItemArmourPants();
};
