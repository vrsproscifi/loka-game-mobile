// VRSPRO

#include "LokaGame.h"
#include "ItemArmourEntity.h"


UItemArmourEntity::UItemArmourEntity()
{
	EntityDescription.CategoryType = CategoryTypeId::Armour;
}

TArray<FDisplayParam> UItemArmourEntity::GetDisplayParams() const
{
	TArray<FDisplayParam> ReturnValue;

	ReturnValue.Add(FDisplayParam(NSLOCTEXT("UItemArmourEntity", "ArmourEntity.Health", "Health"), Property.Health.Amount));
	ReturnValue.Add(FDisplayParam(NSLOCTEXT("UItemArmourEntity", "ArmourEntity.Armour", "Armour"), Property.Armour.Amount));
	ReturnValue.Add(FDisplayParam(NSLOCTEXT("UItemArmourEntity", "ArmourEntity.Energy", "Energy"), Property.Energy.Amount));
	ReturnValue.Add(FDisplayParam(NSLOCTEXT("UItemArmourEntity", "ArmourEntity.Stamina", "Stamina"), Property.Stamina.Amount));

	return ReturnValue;
}