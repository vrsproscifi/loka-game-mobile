// VRSPRO

#pragma once
#include <Item/ItemBaseEntity.h>
#include "ItemArmourEntity.generated.h"



UCLASS(Blueprintable)
class LOKAGAME_API UItemArmourEntity
	: public UItemBaseEntity
{
	GENERATED_BODY()
public:
	UItemArmourEntity();

	virtual TArray<FDisplayParam> GetDisplayParams() const override;

	virtual bool IsSupportedForNetworking() const override
	{
		return true;
	}
};
