// VRSPRO

#pragma once

#include "Entity/Item/ItemBaseEntity.h"
#include "ItemMaterialModelId.h"
#include "ItemMaterialEntity.generated.h"

UCLASS()
class LOKAGAME_API UItemMaterialEntity 
	: public UItemBaseEntity
{
	GENERATED_BODY()

public:

	UItemMaterialEntity();

};
