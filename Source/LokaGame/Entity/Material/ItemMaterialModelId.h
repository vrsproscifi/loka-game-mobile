#pragma once

#include "ItemMaterialModelId.generated.h"

UENUM()
namespace ItemMaterialModelId
{
	enum Type
	{
		Default,
		White,
		Black,
		SandCamo,
		GreenCamo,
		GreenPixelCamo,
		Silver,
		Gold,
		End UMETA(Hidden)
	};
}
