// VRSPRO

#pragma once

#include "Entity/Item/ItemBaseEntity.h"
#include "ItemGrenadeModelId.h"
#include "ItemGrenadeEntity.generated.h"

class AShooterExplosionEffect;

USTRUCT(Blueprintable)
struct FItemGrenadeProperty
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, Category = Gameplay)
	float Radius;

	UPROPERTY(EditAnywhere, Category = Gameplay)
	int32 Damage;

	// * Use 0.0 to ditonate on hit any surface
	UPROPERTY(EditAnywhere, Category = Gameplay)
	float DetonationIn;

	UPROPERTY(EditAnywhere, Category = Gameplay)
	TSubclassOf<UDamageType> DamageType;

	UPROPERTY(EditAnywhere, Category = Gameplay)
	UParticleSystem* AirFX;

	UPROPERTY(EditAnywhere, Category = Gameplay)
	UParticleSystem* ExplosionFX;

	UPROPERTY(EditAnywhere, Category = Gameplay)
	USoundCue* ExplosionSound;
};

UCLASS()
class LOKAGAME_API UItemGrenadeEntity 
	: public UItemBaseEntity
{
	GENERATED_BODY()
	
public:

	UItemGrenadeEntity();

	virtual bool IsSupportedForNetworking() const override
	{
		return true;
	}
	
	FORCEINLINE FItemGrenadeProperty GetGrenadeProperty() const
	{
		return GrenadeProperty;
	}

protected:

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	FItemGrenadeProperty GrenadeProperty;
};
