// VRSPRO

#pragma once

#include "Entity/Item/ItemBaseEntity.h"
#include "ItemServiceModelId.h"
#include "ItemServiceEntity.generated.h"


UCLASS()
class LOKAGAME_API UItemServiceEntity
	: public UItemBaseEntity
{
	GENERATED_BODY()
	
public:

	UItemServiceEntity();

	virtual bool IsSupportedForNetworking() const override
	{
		return true;
	}
};
