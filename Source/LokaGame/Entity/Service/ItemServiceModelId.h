#pragma once

#include "ItemServiceModelId.generated.h"

UENUM()
namespace ItemServiceModelId
{
	enum Type
	{
		PremiumAccount_0001Hour,            //  Premium Account for 1 hour
		PremiumAccount_0006Hour,            //  Premium Account for 6 hour
		PremiumAccount_0024Hour,            //  Premium Account for 1 day
		PremiumAccount_0072Hour,            //  Premium Account for 3 day
		PremiumAccount_0168Hour,            //  Premium Account for 7 day
		PremiumAccount_0744Hour,            //  Premium Account for 1 month
		PremiumAccount_2232Hour,            //  Premium Account for 3 month
		PremiumAccount_4464Hour,            //  Premium Account for 6 month

		BoosterOfReputation_0001Hour,       //  Premium Account for 1 hour
		BoosterOfReputation_0006Hour,       //  Premium Account for 6 hour
		BoosterOfReputation_0024Hour,       //  Premium Account for 1 day
		BoosterOfReputation_0072Hour,       //  Premium Account for 3 day
		BoosterOfReputation_0168Hour,       //  Premium Account for 7 day
		BoosterOfReputation_0744Hour,       //  Premium Account for 1 month
		BoosterOfReputation_2232Hour,       //  Premium Account for 3 month
		BoosterOfReputation_4464Hour,       //  Premium Account for 6 month

		BoosterOfExperience_0001Hour,       //  Premium Account for 1 hour
		BoosterOfExperience_0006Hour,       //  Premium Account for 6 hour
		BoosterOfExperience_0024Hour,       //  Premium Account for 1 day
		BoosterOfExperience_0072Hour,       //  Premium Account for 3 day
		BoosterOfExperience_0168Hour,       //  Premium Account for 7 day
		BoosterOfExperience_0744Hour,       //  Premium Account for 1 month
		BoosterOfExperience_2232Hour,       //  Premium Account for 3 month
		BoosterOfExperience_4464Hour,       //  Premium Account for 6 month

		BoosterOfMoney_0001Hour,            //  Premium Account for 1 hour
		BoosterOfMoney_0006Hour,            //  Premium Account for 6 hour
		BoosterOfMoney_0024Hour,            //  Premium Account for 1 day
		BoosterOfMoney_0072Hour,            //  Premium Account for 3 day
		BoosterOfMoney_0168Hour,            //  Premium Account for 7 day
		BoosterOfMoney_0744Hour,            //  Premium Account for 1 month
		BoosterOfMoney_2232Hour,            //  Premium Account for 3 month
		BoosterOfMoney_4464Hour,            //  Premium Account for 6 month 

		End UMETA(Hidden)
	};
}

UENUM()
namespace ServiceTypeId
{
	enum Type
	{
		None,
		PremiumAccount,
		BoosterOfReputation,
		BoosterOfExperience,
		BoosterOfMoney,
		End UMETA(Hidden)
	};
}


UENUM()
namespace UsingPremiumServiceFlag
{
	enum Type
	{
		None = 0,
		PremiumAccount = 1,
		BoosterOfReputation = 2,
		BoosterOfExperience = 4,
		BoosterOfMoney = 8
	};
}