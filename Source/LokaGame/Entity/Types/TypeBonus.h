#pragma once
#include "TypeBonus.generated.h"

USTRUCT(Blueprintable)
struct FTypeBonus
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly)
		int32 Money;

	UPROPERTY(BlueprintReadOnly)
		int32 Experience;

	UPROPERTY(BlueprintReadOnly)
		int32 Reputation;
};