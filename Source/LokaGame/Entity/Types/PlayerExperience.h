#pragma once
#include "Fraction/FractionTypeId.h"
#include "PlayerExperience.generated.h"

USTRUCT()
struct FPlayerExperience
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()		int32 Level;
	
	UPROPERTY()		int32 LastLevel;

	UPROPERTY()		int32 SkillPoints;

	UPROPERTY()		int32 Experience;

	UPROPERTY()		int32 NextExperience;
};