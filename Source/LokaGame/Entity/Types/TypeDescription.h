#pragma once

#include "TypeCost.h"
#include <Item/ItemSlotTypeId.h>
#include "Item/CategoryTypeId.h"
#include "Fraction/FractionTypeId.h"
#include "TypeDescription.generated.h"


USTRUCT(Blueprintable)
struct FTypeDescription
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)							FText										Name;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, meta = (MultiLine))		FText										Description;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)							int32										ModelId;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)							int32										Perfomance;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)							int32										Storage;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)							int32										Duration;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)							int32										DefaultAmount;


	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)							FractionTypeId								FractionId;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)							TEnumAsByte<CategoryTypeId::Type>			CategoryType;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)							TEnumAsByte<ItemSlotTypeId::Type>			SlotType;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, meta = (Bitmask, BitmaskEnum = "ECharacterModelId"))							
																			int32										CharacterModelFlagId;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)							uint8										Level;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)							FTypeCost									Cost;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)							FTypeCost									SubscribeCost;

	const int32& GetModelId() const
	{
		return ModelId;
	}

	template<typename TInstanceModelId>
	const TInstanceModelId GetModelId() const
	{
		return static_cast<TInstanceModelId>(ModelId);
	}


};
