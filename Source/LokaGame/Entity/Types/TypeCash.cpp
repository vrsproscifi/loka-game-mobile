#include "LokaGame.h"
#include "TypeCash.h"
#include "SResourceTextBoxType.h"

FResourceTextBoxValue FTypeCash::AsResource()
{
	return FResourceTextBoxValue(static_cast<EResourceTextBoxType::Type>(Currency.GetValue()), Amount);
}

FResourceTextBoxValue FTypeCash::AsResource() const
{
	return FResourceTextBoxValue(static_cast<EResourceTextBoxType::Type>(Currency.GetValue()), Amount);
}