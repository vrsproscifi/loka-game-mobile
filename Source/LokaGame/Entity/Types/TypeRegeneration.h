#pragma once
#include "TypeRegeneration.generated.h"

USTRUCT(Blueprintable)
struct FTypeRegeneration
{
	GENERATED_USTRUCT_BODY()

	//* Current amount
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		float Amount;

	//* Maximum amount or default
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		float MaxAmount;

	//* Maximum regenerated maybe percent of MaxAmount
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		float MaxRegenerated;

	//* Regeneration speed in seconds
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		float Regeneration;

	FTypeRegeneration& operator+=(const FTypeRegeneration& Other)
	{
		this->Amount += Other.Amount;
		this->MaxAmount += Other.MaxAmount;
		this->MaxRegenerated += Other.MaxRegenerated;
		this->Regeneration += Other.Regeneration;

		return *this;
	}
};