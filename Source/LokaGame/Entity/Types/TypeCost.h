#pragma once
#include "GameCurrency.h"
#include "Slate/Components/SResourceTextBoxValue.h"
#include "TypeCost.generated.h"

struct FTypeCash;

USTRUCT(Blueprintable)
struct FTypeCost
{
	GENERATED_USTRUCT_BODY()

	FORCEINLINE FTypeCost()
		: Amount(0)
		, Currency(0)
	{

	}

	FORCEINLINE FTypeCost(const int64& amount, const EGameCurrency::Type& currenecy)
		: Amount(amount)
		, Currency(currenecy)
	{

	}

	FString ToString() const;
	FText ToText() const;
	FText ToResourse() const;
	EResourceTextBoxType::Type AsResourceTextBoxType() const;

	bool IsEnough(const FTypeCash* InOther) const;
	bool IsEnough(const TArray<FTypeCash>& InOther) const;

	const int64& GetAmount() const
	{
		return Amount;
	}

	FResourceTextBoxValue GetCostAsResource();
	FResourceTextBoxValue GetCostAsResource() const;

	UPROPERTY(EditAnywhere)		int64								Amount;
	UPROPERTY(EditAnywhere)		TEnumAsByte<EGameCurrency::Type>	Currency;
};
