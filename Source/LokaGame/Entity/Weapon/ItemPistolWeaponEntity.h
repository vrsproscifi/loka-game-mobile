// VRSPRO

#pragma once

#include "ItemWeaponEntity.h"
#include "ItemPistolWeaponEntity.generated.h"

UCLASS()
class LOKAGAME_API UItemPistolWeaponEntity
	: public UItemWeaponEntity
{
	GENERATED_BODY()

	//==================================================================
	//							[ Methods ]
	//==================================================================
public:	
	// Sets default values for this actor's properties
	UItemPistolWeaponEntity();
	
};
