// VRSPRO

#pragma once

#include "Item/ItemBaseEntity.h"
#include "Module/ItemModuleModelId.h"
#include "Weapon/WeaponContainer.h"
#include "ItemWeaponEntity.generated.h"

class UItemAmmoEntity;
class UItemModuleEntity;


USTRUCT()
struct FWeaponAimingParam
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, Category = Visual)
	UMaterialInterface *Material;

	UPROPERTY(EditDefaultsOnly, Category = Visual)
	int32 ReplaceIndex;

	UMaterialInterface *OriginMaterial;
};

USTRUCT(BlueprintType)
struct FWeaponPropertyBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)
	int32 AmmoPerClip;

	// 60 / FireRate = Time Between Shots
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)
	float FireRate;

	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)
	float ReloadDuration;

	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)
	float Mass;

	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)
	float Feedback;

	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)
	float Spread;

	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)
	float Range;

	// Temp
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)
	float Damage;

	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)
	bool IsPartlyReload;

	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)
	bool IsAutomaticFire;

	// Calculated from rate of fire
	float TimeBetweenShots() const
	{
		return 60.0f / FireRate;
	}

	FWeaponPropertyBase()
		: AmmoPerClip(4)
		, FireRate(200)
		, ReloadDuration(1.5f)
		, Mass(0)
		, Feedback(0)
		, Spread(0)
		, Range(1000)
		, Damage(10)
		, IsPartlyReload(false)
		, IsAutomaticFire(true)
	{}
};

USTRUCT(Blueprintable)
struct FWeaponConfiguration
{
	GENERATED_USTRUCT_BODY()

	/** crosshair parts icons (left, top, right, bottom and center) */
	UPROPERTY(EditDefaultsOnly, Category = HUD)
		FCanvasIcon Crosshair[5];

	/** crosshair parts icons when targeting (left, top, right, bottom and center) */
	UPROPERTY(EditDefaultsOnly, Category = HUD)
		FCanvasIcon AimingCrosshair[5];

	/** only use red colored center part of aiming crosshair */
	UPROPERTY(EditDefaultsOnly, Category = HUD)
		bool UseLaserDot;

	/** false = default crosshair */
	UPROPERTY(EditDefaultsOnly, Category = HUD)
		bool UseCustomCrosshair;

	/** false = use custom one if set, otherwise default crosshair */
	UPROPERTY(EditDefaultsOnly, Category = HUD)
		bool UseCustomAimingCrosshair;

	/** true - crosshair will not be shown unless aiming with the weapon */
	UPROPERTY(EditDefaultsOnly, Category = HUD)
		bool bHideCrosshairWhileNotAiming;

	/** FX for muzzle flash */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
		UParticleSystem* MuzzleFX;

	/** camera shake on firing */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
		TSubclassOf<UCameraShake> FireCameraShake;

	/** force feedback effect to play when the weapon is fired */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
		UForceFeedbackEffect *FireForceFeedback;

	/** single fire sound (bLoopedFireSound not set) */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* FireSound;

	/** looped fire sound (bLoopedFireSound set) */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* FireLoopSound;

	/** finished burst sound (bLoopedFireSound set) */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* FireFinishSound;

	/** out of ammo sound */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* OutOfAmmoSound;

	/** reload sound */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* ReloadSound;

	/** reload animations */
	UPROPERTY(EditDefaultsOnly, Category = Animation)
		FWeaponAnim ReloadAnim;

	/** equip sound */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* EquipSound;

	/** equip animations */
	UPROPERTY(EditDefaultsOnly, Category = Animation)
		FWeaponAnim EquipAnim;

	/** fire animations */
	UPROPERTY(EditDefaultsOnly, Category = Animation)
		FWeaponAnim FireAnim;

	/** is muzzle FX looped? */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
		uint32 bLoopedMuzzleFX : 1;

	/** is fire sound looped? */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		uint32 bLoopedFireSound : 1;

	/** is fire animation looped? */
	UPROPERTY(EditDefaultsOnly, Category = Animation)
		uint32 bLoopedFireAnim : 1;

	// * Offset if targeting
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		FTransform AnimOffsetTransform;

	// * Offset if not targeting
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		FTransform IdleOffsetTransform;

	// * Offset if targeting without module
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		FTransform DefaultOffsetTransform;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		UAnimMontage* WeaponMontageFire;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		UAnimMontage* WeaponMontageReload;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		UAnimMontage* PawnFPPMontageFire;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		UAnimMontage* PawnFPPMontageReload;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		UAnimMontage* PawnTPPMontageFire;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		UAnimMontage* PawnTPPMontageReload;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		FTwoAnimSequence Default;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		FTwoAnimSequence Walk;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		FTwoAnimSequence Run;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		FTwoAnimSequence Targeting;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		FTwoAnimSequence WalkTargeting;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		FTwoAnimSequence Jump;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		FWeaponAnim FireInTargeting;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Fire)
		FName Name_InstantMuzzle;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Fire)
		FName Name_ProjMuzzle;
};

USTRUCT()
struct FKostilModule
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	TArray<UItemModuleEntity*> Array;
};

UCLASS()
class LOKAGAME_API UItemWeaponEntity 
	: public UItemBaseEntity
{
	GENERATED_BODY()

	//==================================================================
	//							[ Methods ]
	//==================================================================
public:	
	// Sets default values for this actor's properties
	UItemWeaponEntity();
	
	virtual TArray<FDisplayParam> GetDisplayParams() const override;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Weapon)
	FORCEINLINE FWeaponPropertyBase GetWeaponProperty() const {	return WeaponProperty; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Weapon)
	FORCEINLINE FWeaponConfiguration GetWeaponConfiguration() const	{ return WeaponConfiguration; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Weapon)
	FORCEINLINE TEnumAsByte<EShooterWeaponTypes::Type> GetWeaponType() const { return WeaponType; }



	UFUNCTION()
	UItemAmmoEntity* GetPrimaryAmmo() const;

	UFUNCTION()
	UItemAmmoEntity* GetSecondaryAmmo() const;

	UPROPERTY(EditDefaultsOnly, Category = Default)
	TArray<FName> SupportedSockets;

	UPROPERTY()
	TMap<FName, FKostilModule> Modules;

	UPROPERTY(EditDefaultsOnly, Category = Default)
	TSubclassOf<class UItemAmmoEntity> AmmoInstance[EAmmoSlot::End];

	UPROPERTY()
	UItemAmmoEntity* Ammo[EAmmoSlot::End];

	UPROPERTY(EditDefaultsOnly, Category = Default)
	TArray<TEnumAsByte<ItemModuleModelId::Type>> SupportedModules;

	virtual void PostInitProperties() override;

	UPROPERTY(EditDefaultsOnly, Category = Default)
	TArray<TEnumAsByte<ItemModuleModelId::Type>> DefaultModules;

	virtual bool IsValidItem() const override;

protected:

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Gameplay)
	TEnumAsByte<EShooterWeaponTypes::Type> WeaponType;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Gameplay, meta = (ShowOnlyInnerProperties))
	FWeaponConfiguration WeaponConfiguration;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Gameplay)
	FWeaponPropertyBase WeaponProperty;

	UPROPERTY()
	TSubclassOf<UCameraShake> FireCameraShakeDefault;

};
