// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Entity/Transport/ItemTransportEntity.h"
#include "ItemHoverTransportEntity.generated.h"


USTRUCT(Blueprintable, Category = Settings)
struct FHoverTransportSettings
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Settings)	float Speed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Settings)	float AccelerationForce;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Settings)	float BrakeForce;	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Settings)	float HoverForce;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Settings)	float HoverHeight;	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Settings)	float HoverHeightUp;
};

UCLASS()
class LOKAGAME_API UItemHoverTransportEntity : public UItemTransportEntity
{
	GENERATED_BODY()
	
public:

	UItemHoverTransportEntity();
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Settings)
	const FHoverTransportSettings& GetHoverSettings() const { return HoverSettings; }

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Settings, meta = (ShowOnlyInnerProperties))
	FHoverTransportSettings HoverSettings;
};
