// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Entity/Item/ItemBaseEntity.h"
#include "ItemTransportEntity.generated.h"

class ATransportBase;
/**
 * 
 */
UCLASS()
class LOKAGAME_API UItemTransportEntity : public UItemBaseEntity
{
	GENERATED_BODY()
	
public:

	UItemTransportEntity();
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Template)
	TSubclassOf<ATransportBase> GetTransportTemplate() const;

protected:

	UPROPERTY(EditDefaultsOnly, Category = Template)	TSubclassOf<ATransportBase> TransportTemplate;
	
};
