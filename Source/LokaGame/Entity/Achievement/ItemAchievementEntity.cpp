// VRSPRO

#include "LokaGame.h"
#include "ItemAchievementEntity.h"


UItemAchievementEntity::UItemAchievementEntity()
	: Super()
	, CurrentProgress(0)
{
	IsVisible = true;
}

void UItemAchievementEntity::Initialize(const FAchievementContainer& initializer)
{
	EntityDescription.ModelId = initializer.Id;
	Instances = initializer.Instances;
}

FItemAchievementProperty UItemAchievementEntity::GetAchievementProperty()
{
	FItemAchievementProperty property;
	property.Level = 0;
	property.MaxLevel = Instances.Num();
	property.CurrentProgress = CurrentProgress;

	for (FAchievementInstance instance : Instances)
	{
		property.Level++;
		if (CurrentProgress < instance.Amount)
		{
			property.NeedProgress = instance.Amount;
			property.Reward = instance.Reward;
			break;
		}
	}


	return property;
}

