// VRSPRO

#pragma once
#include "Types/TypeCost.h"

#include "Entity/Item/AbstractItemEntity.h"
#include "StoreController/Models/AchievementContainer.h"
#include "ItemAchievementEntity.generated.h"


USTRUCT()
struct FItemAchievementProperty
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	int32 Level;

	UPROPERTY()
	int32 MaxLevel;

	UPROPERTY()
	int32 CurrentProgress;

	UPROPERTY()
	int32 NeedProgress;

	UPROPERTY()
	FTypeCost Reward;
};

UCLASS()
class LOKAGAME_API UItemAchievementEntity 
	: public UAbstractItemEntity
{
	GENERATED_BODY()
	
public:

	UItemAchievementEntity();
	void Initialize(const struct FAchievementContainer& initializer);

	const TEnumAsByte<ItemAchievementTypeId::Type> GetModelId() const
	{
		return GetModelIdAs<ItemAchievementTypeId::Type>();
	}

	UPROPERTY()
	int32 CurrentProgress;

	UPROPERTY()
	TArray<FAchievementInstance> Instances;

	UFUNCTION()
	FItemAchievementProperty GetAchievementProperty();

	UPROPERTY(EditDefaultsOnly, Category = Sound)
	FSlateSound Sound;

	UPROPERTY(EditDefaultsOnly, Category = Default)
	bool IsVisible;
};
