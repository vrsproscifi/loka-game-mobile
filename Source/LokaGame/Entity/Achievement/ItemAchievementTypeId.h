#pragma once
#include "ItemAchievementTypeId.generated.h"

UENUM()
namespace ItemAchievementTypeId
{
	enum Type
	{
		None UMETA(Hidden),

		// Kill By
		/// <summary>
		/// kill players
		/// </summary>
		Kill_Total,

		/// <summary>
		/// kill players from rifles
		/// </summary>
		Kill_By_Rifle,

		/// <summary>
		/// kill players from sniper rifles
		/// </summary>          
		Kill_By_SniperRifle,

		/// <summary>
		/// kill players from minigun
		/// </summary>          
		Kill_By_LightMachineGun,

		/// <summary>
		/// kill players from shotgun
		/// </summary>          
		Kill_By_ShotGun,

		/// <summary>
		/// kill players from Pistol
		/// </summary>          
		Kill_By_Pistol,

		/// <summary>
		/// kill players from submachine gun(SMG)
		/// </summary>      
		Kill_By_SMG,

		/// <summary>
		/// kill players from Knife
		/// </summary>          
		Kill_By_Knife,

		/// <summary>
		/// kill players from Grenade
		/// </summary>          
		Kill_By_Grenade,

		/// <summary>
		/// kill players from Mine
		/// </summary>          
		Kill_By_Mine,

		// Series Kill By
		/// <summary>
		/// SeriesKill players
		/// </summary>
		SeriesKill_Total,

		/// <summary>
		/// SeriesKill players from rifles
		/// </summary>
		SeriesKill_By_Rifle,

		/// <summary>
		/// SeriesKill players from sniper rifles
		/// </summary>          
		SeriesKill_By_SniperRifle,

		/// <summary>
		/// SeriesKill players from minigun
		/// </summary>          
		SeriesKill_By_LightMachineGun,

		/// <summary>
		/// SeriesKill players from shotgun
		/// </summary>          
		SeriesKill_By_ShotGun,

		/// <summary>
		/// SeriesKill players from Pistol
		/// </summary>          
		SeriesKill_By_Pistol,

		/// <summary>
		/// SeriesKill players from submachine gun(SMG)
		/// </summary>      
		SeriesKill_By_SMG,

		/// <summary>
		/// SeriesKill players from Knife
		/// </summary>          
		SeriesKill_By_Knife,

		/// <summary>
		/// SeriesKill players from Grenade
		/// </summary>          
		SeriesKill_By_Grenade,

		/// <summary>
		/// SeriesKill players from Mine
		/// </summary>          
		SeriesKill_By_Mine,

		// Fraction Reputation
		Reputation_RIFT,
		Reputation_RIFT_Friend,
		Reputation_Keepers,
		Reputation_Keepers_Friend,

		// Player Experience
		Experience_Level,

		// Match Medals
		Medal_Match_Best_Shooter,
		Medal_Match_Best_Player,
		Medal_Match_Best_Killer,
		Medal_Match_Best_Hero,
		Medal_Match_FirstBlood,
		Medal_Match_Headshot,
		Medal_Match_SeriesKill_5,
		Medal_Match_SeriesKill_10,
		Medal_Match_SeriesKill_15,
		Medal_Match_SeriesKill_20,
		Medal_Match_SeriesKill_25,
		Medal_Match_SeriesKill_30,
		End UMETA(Hidden)
	};
}
