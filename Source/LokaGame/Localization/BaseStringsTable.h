// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "StringTableRegistry.h"

class LOKAGAME_API FLokaBaseStringsTable
{
public:

	FLokaBaseStringsTable();
	~FLokaBaseStringsTable();

	static FText GetDayOfWeek(const EDayOfWeek InDay);
	static FText GetMonthOfYear(const EMonthOfYear InMonth);

	enum EBaseStrings
	{
		All,
		Yes,
		No,
		Ok,
		Apply,
		Select,
		Next,
		Back,
		Continue,
		Cancel,
		Close,
		Accept,
		Decline,
		End
	};

	static FText GetBaseText(const EBaseStrings InString);

#if !UE_SERVER
	static TSharedPtr<FLokaBaseStringsTable> Instance;
#endif
};