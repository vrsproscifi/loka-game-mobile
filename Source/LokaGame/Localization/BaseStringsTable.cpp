// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "BaseStringsTable.h"


FLokaBaseStringsTable::FLokaBaseStringsTable()
{
	if (FStringTableRegistry::Get().FindStringTable("BaseStrings").IsValid()) return;

	LOCTABLE_NEW("BaseStrings", "All");
	// Week day names
	LOCTABLE_SETSTRING("BaseStrings", "DayOfWeek.Monday", "Monday");
	LOCTABLE_SETSTRING("BaseStrings", "DayOfWeek.Tuesday", "Tuesday");
	LOCTABLE_SETSTRING("BaseStrings", "DayOfWeek.Wednesday", "Wednesday");
	LOCTABLE_SETSTRING("BaseStrings", "DayOfWeek.Thursday", "Thursday");
	LOCTABLE_SETSTRING("BaseStrings", "DayOfWeek.Friday", "Friday");
	LOCTABLE_SETSTRING("BaseStrings", "DayOfWeek.Saturday", "Saturday");
	LOCTABLE_SETSTRING("BaseStrings", "DayOfWeek.Sunday", "Sunday");
	// Month names
	LOCTABLE_SETSTRING("BaseStrings", "MonthOfYear.January", "January");
	LOCTABLE_SETSTRING("BaseStrings", "MonthOfYear.February", "February");
	LOCTABLE_SETSTRING("BaseStrings", "MonthOfYear.March", "March");
	LOCTABLE_SETSTRING("BaseStrings", "MonthOfYear.April", "April");
	LOCTABLE_SETSTRING("BaseStrings", "MonthOfYear.May", "May");
	LOCTABLE_SETSTRING("BaseStrings", "MonthOfYear.June", "June");
	LOCTABLE_SETSTRING("BaseStrings", "MonthOfYear.July", "July");
	LOCTABLE_SETSTRING("BaseStrings", "MonthOfYear.August", "August");
	LOCTABLE_SETSTRING("BaseStrings", "MonthOfYear.September", "September");
	LOCTABLE_SETSTRING("BaseStrings", "MonthOfYear.October", "October");
	LOCTABLE_SETSTRING("BaseStrings", "MonthOfYear.November", "November");
	LOCTABLE_SETSTRING("BaseStrings", "MonthOfYear.December", "December");
	// Base strings
	LOCTABLE_SETSTRING("BaseStrings", "All.All", "All");
	LOCTABLE_SETSTRING("BaseStrings", "All.Yes", "Yes");
	LOCTABLE_SETSTRING("BaseStrings", "All.No", "No");
	LOCTABLE_SETSTRING("BaseStrings", "All.Ok", "Ok");
	LOCTABLE_SETSTRING("BaseStrings", "All.Apply", "Apply");
	LOCTABLE_SETSTRING("BaseStrings", "All.Select", "Select");
	LOCTABLE_SETSTRING("BaseStrings", "All.Next", "Next");
	LOCTABLE_SETSTRING("BaseStrings", "All.Back", "Back");
	LOCTABLE_SETSTRING("BaseStrings", "All.Continue", "Continue");
	LOCTABLE_SETSTRING("BaseStrings", "All.Cancel", "Cancel");
	LOCTABLE_SETSTRING("BaseStrings", "All.Close", "Close");
	LOCTABLE_SETSTRING("BaseStrings", "All.Accept", "Accept");
	LOCTABLE_SETSTRING("BaseStrings", "All.Decline", "Decline");
}

FLokaBaseStringsTable::~FLokaBaseStringsTable()
{
	
}

FText FLokaBaseStringsTable::GetDayOfWeek(const EDayOfWeek InDay)
{
	switch (InDay) 
	{ 
		case EDayOfWeek::Monday: 	return LOCTABLE("BaseStrings", "DayOfWeek.Monday");
		case EDayOfWeek::Tuesday: 	return LOCTABLE("BaseStrings", "DayOfWeek.Tuesday");
		case EDayOfWeek::Wednesday: return LOCTABLE("BaseStrings", "DayOfWeek.Wednesday");
		case EDayOfWeek::Thursday:	return LOCTABLE("BaseStrings", "DayOfWeek.Thursday");
		case EDayOfWeek::Friday: 	return LOCTABLE("BaseStrings", "DayOfWeek.Friday");
		case EDayOfWeek::Saturday:	return LOCTABLE("BaseStrings", "DayOfWeek.Saturday");
		case EDayOfWeek::Sunday:	return LOCTABLE("BaseStrings", "DayOfWeek.Sunday");
	}

	return FText::GetEmpty();
}

FText FLokaBaseStringsTable::GetMonthOfYear(const EMonthOfYear InMonth)
{
	switch (InMonth) 
	{ 
		case EMonthOfYear::January:		return LOCTABLE("BaseStrings", "MonthOfYear.January");
		case EMonthOfYear::February:	return LOCTABLE("BaseStrings", "MonthOfYear.February");
		case EMonthOfYear::March:		return LOCTABLE("BaseStrings", "MonthOfYear.March");
		case EMonthOfYear::April:		return LOCTABLE("BaseStrings", "MonthOfYear.April");
		case EMonthOfYear::May:			return LOCTABLE("BaseStrings", "MonthOfYear.May");
		case EMonthOfYear::June: 		return LOCTABLE("BaseStrings", "MonthOfYear.June");
		case EMonthOfYear::July: 		return LOCTABLE("BaseStrings", "MonthOfYear.July");
		case EMonthOfYear::August:		return LOCTABLE("BaseStrings", "MonthOfYear.August");
		case EMonthOfYear::September: 	return LOCTABLE("BaseStrings", "MonthOfYear.September");
		case EMonthOfYear::October:		return LOCTABLE("BaseStrings", "MonthOfYear.October");
		case EMonthOfYear::November: 	return LOCTABLE("BaseStrings", "MonthOfYear.November");
		case EMonthOfYear::December: 	return LOCTABLE("BaseStrings", "MonthOfYear.December");
	}

	return FText::GetEmpty();
}

FText FLokaBaseStringsTable::GetBaseText(const EBaseStrings InString)
{
	switch(InString) 
	{
		case All:		 return	LOCTABLE("BaseStrings", "All.All");
		case Yes:		 return	LOCTABLE("BaseStrings", "All.Yes");
		case No: 		 return	LOCTABLE("BaseStrings", "All.No");
		case Ok: 		 return	LOCTABLE("BaseStrings", "All.Ok");
		case Apply: 	 return	LOCTABLE("BaseStrings", "All.Apply");
		case Select: 	 return	LOCTABLE("BaseStrings", "All.Select");
		case Next:		 return	LOCTABLE("BaseStrings", "All.Next");
		case Back:		 return	LOCTABLE("BaseStrings", "All.Back");
		case Continue:	 return	LOCTABLE("BaseStrings", "All.Continue");
		case Cancel: 	 return	LOCTABLE("BaseStrings", "All.Cancel");
		case Close:		 return	LOCTABLE("BaseStrings", "All.Close");
		case Accept: 	 return	LOCTABLE("BaseStrings", "All.Accept");
		case Decline:	 return	LOCTABLE("BaseStrings", "All.Decline");
	}
		
	return FText::GetEmpty();
}

#if !UE_SERVER
TSharedPtr<FLokaBaseStringsTable> FLokaBaseStringsTable::Instance;
#endif