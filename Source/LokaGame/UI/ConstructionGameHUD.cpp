#include "LokaGame.h"
#include "ConstructionGameHUD.h"

AConstructionGameHUD::AConstructionGameHUD()
	: Super()
{

}

void AConstructionGameHUD::DrawHUD()
{
	Super::DrawHUD();

	if (Canvas)
	{
		FCanvasTileItem TileItem(FVector2D(Canvas->SizeX / 2, Canvas->SizeY / 2), FVector2D(3, 3), FColor::White);
		TileItem.PivotPoint = FVector2D(.5f, .5f);
		
		Canvas->DrawItem(TileItem);
	}
}
