// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "BaseHUD.generated.h"


USTRUCT()
struct FBaseHUDFonts
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY()		UFont*	Tiny;
	UPROPERTY()		UFont*	Small;
	UPROPERTY()		UFont*	Medium;
	UPROPERTY()		UFont*	Big;
	UPROPERTY()		UFont*	Large;
	UPROPERTY()		UFont*	Normal;
	UPROPERTY()		UFont*	Number;
	UPROPERTY()		UFont*	Awesome;
};

UCLASS()
class LOKAGAME_API ABaseHUD : public AHUD
{
	GENERATED_BODY()
	
public:

	ABaseHUD();

	virtual void DrawHUD() override;

	bool CorrectProject(const FVector &Location, FVector2D &OutScreen);
	
protected:

	virtual void DrawWorldIcons();
	virtual void DrawGameVersion();
	virtual void DrawDebugInfo();
	
	UPROPERTY(EditDefaultsOnly, Category = Fonts)
	FBaseHUDFonts BaseFonts;
};
