#pragma once


#include "BaseHUD.h"
#include "ConstructionGameHUD.generated.h"


UCLASS()
class AConstructionGameHUD : public ABaseHUD
{
GENERATED_BODY()

public:

	AConstructionGameHUD();

	virtual void DrawHUD() override;
};