#pragma once

//=======================================================
//							[ Headers ]
#include "PlayerDataEntity.h"
#include "Fraction/FractionReputation.h"
#include "PlayerFractionEntity.generated.h"

class UItemBaseEntity;
class UFractionEntity;
struct FTypeDescription;
struct FFractionRank;

enum class FractionTypeId : uint8;

UCLASS()
class UPlayerFractionEntity : public UPlayerDataEntity
{
	GENERATED_BODY()

	UPlayerFractionEntity();
	//==========================================================
	//	Progress
protected:
	UPROPERTY(Replicated)	FFractionReputation Progress;

public:
	FORCEINLINE_DEBUGGABLE void SetProgress(const FFractionReputation& progress)
	{
		Progress = progress;
	}

	FORCEINLINE_DEBUGGABLE const FFractionReputation& GetProgress() const
	{
		return Progress;
	}

	FORCEINLINE_DEBUGGABLE const FFractionReputation& GetProgress()
	{
		return Progress;
	}

	FractionTypeId GetModelId() const;

	FTypeDescription GetDescription();
	FTypeDescription GetDescription() const;

	//========================================
	//			[ Image ]
	const FSlateBrush* GetImage() const;
	const FSlateBrush* GetImage();

	//========================================
	//			[ Name ]
	FText GetFractionName();
	FText GetFractionName() const;

	//========================================
	//			[ Description ]
	FText GetFractionDescription();
	FText GetFractionDescription() const;

	//========================================
	//			[ Reputation ]
	FText GetFractionRank();
	FText GetFractionRank() const;

	FText GetFractionReputation();
	FText GetFractionReputation() const;


	//========================================
	//			[ Rank ]
	FORCEINLINE_DEBUGGABLE const FFractionRank* GetRank(const uint32& rank);
	FORCEINLINE_DEBUGGABLE const FFractionRank* GetRank(const uint32& rank) const;

	const FFractionRank* GetCurrentRank() const;
	const FFractionRank* GetCurrentRank();

	//========================================
	// Items
	const TArray<UItemBaseEntity*>* GetItemList() const;
	const TArray<UItemBaseEntity*>* GetItemList();

	//========================================
	//			[ Entity ]
	void SetFractionEntity(UFractionEntity* fraction);
	FORCEINLINE_DEBUGGABLE UFractionEntity* GetFractionEntity()	{ return Entity; }
	FORCEINLINE_DEBUGGABLE UFractionEntity* GetFractionEntity() const {	return Entity; }


protected:
	//========================================
	//			[ Property ]
	UPROPERTY()									UFractionEntity*	Entity;
	UPROPERTY(ReplicatedUsing = OnRep_Instance) FractionTypeId		FractionId;

	// ��� ������ SetFractionEntity ����������� FractionId �� ����������� Instance
	// FractionId ������������� ��� ��������� Entity �� ������� �������������
	UFUNCTION() void OnRep_Instance();

	//========================================
	//			[ Current Fraction ]
public:
	void SetCurrentFractionReference(const UPlayerFractionEntity* fraction)
	{
		CurrentFractionEntity = fraction;
	}

protected:
	UPROPERTY(Replicated) const UPlayerFractionEntity* CurrentFractionEntity;

public:
	FORCEINLINE_DEBUGGABLE bool IsCurrentFraction() const
	{
		return this == CurrentFractionEntity;
	}

	FORCEINLINE_DEBUGGABLE bool IsCurrentFraction()
	{
		return this == CurrentFractionEntity;
	}
};