#pragma once

//=======================================================
//							[ Headers ]
#include "Item/ItemModelInfo.h"
#include "PlayerDataEntity.h"
#include "PlayerInventoryItem.generated.h"

struct FInvertoryItemWrapper;
class UItemBaseEntity;

UCLASS(ClassGroup = (Utility, Common), BlueprintType, hideCategories = (Trigger, PhysicsVolume), meta = (BlueprintSpawnableComponent))
class LOKAGAME_API UPlayerInventoryItem : public UPlayerDataEntity
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable, Category = Inventory)
	virtual void InitializeInstanceDynamic(UItemBaseEntity* InInstance, const bool InIsReplicated = true);
	virtual void InitializeAddons(const FInvertoryItemWrapper& MyData, const TArray<UPlayerInventoryItem*>& InAddonsList);
	virtual void SetItemEntity(UItemBaseEntity* entity);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
	USkeletalMesh* GetPreviewMesh() const;

	template<class T = UItemBaseEntity>
	T* GetEntity() const
	{
		return GetValidObject<T, UItemBaseEntity>(EntityInstance);
	}

	const FItemModelInfo& GetEntityModel() const
	{
		return EntityModel;
	}

	FORCEINLINE void SetCount(const int32& InCount) { Count = InCount; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
	FORCEINLINE int32 GetCount() const { return Count; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
	UItemBaseEntity* GetEntityBase() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
	FORCEINLINE TArray<UPlayerInventoryItem*> GetAvailableAddons() const { return AvailableAddons; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
	FORCEINLINE TArray<UPlayerInventoryItem*> GetInstalledAddons() const { return InstalledAddons; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
	virtual float GetMass() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
	FORCEINLINE int32 GetInstalledMaterialModelId() const { return InstalledMaterial; }

protected:
	UFUNCTION()	
	void OnRep_Instance();

	UPROPERTY(Replicated)							int32								Count;
	UPROPERTY(ReplicatedUsing = OnRep_Instance)		FItemModelInfo						EntityModel;
	UPROPERTY()										UItemBaseEntity*					EntityInstance;
	UPROPERTY(Replicated)							TArray<UPlayerInventoryItem*>		InstalledAddons;
	UPROPERTY(Replicated)							TArray<UPlayerInventoryItem*>		AvailableAddons;
	UPROPERTY(Replicated)							int32								InstalledMaterial;
};