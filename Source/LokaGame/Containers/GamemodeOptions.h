// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "NodeComponent/NodeSessionMatch.h"
#include "GamemodeOptions.generated.h"


/**
 * 
 */
UCLASS()
class LOKAGAME_API UGamemodeOptions : public UDataAsset
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditDefaultsOnly, Category = GameMode)
	FNodeSessionMatch Options;
	
	UPROPERTY(EditDefaultsOnly, Category = GameMode)
	TSubclassOf<class AUTGameMode> TargetGameMode;
};
