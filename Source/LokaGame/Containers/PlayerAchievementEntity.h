#pragma once

//=======================================================
//							[ Headers ]
#include "PlayerDataEntity.h"
#include "Achievement/ItemAchievementEntity.h"
#include "PlayerAchievementEntity.generated.h"

UCLASS()
class UPlayerAchievementEntity : public UPlayerDataEntity
{
	GENERATED_BODY()

public:
	UPlayerAchievementEntity();
	UPROPERTY() UItemAchievementEntity* Entity;
};