#pragma once

//=======================================================
//							[ Headers ]
#include "Entity/Item/ItemSlotTypeId.h"
#include "PlayerProfileActor.generated.h"


class UPlayerProfileItem;
class UPlayerInventoryItem;

UCLASS()
class APlayerProfileActor : public AActor
{
	GENERATED_BODY()

public:
	APlayerProfileActor();
	//==============================================
	void SetCharacterEntity(UPlayerInventoryItem* character);


	//==============================================
	UFUNCTION()									void OnRep_Instance() const;

	//==============================================
	UPROPERTY(ReplicatedUsing = OnRep_Instance)	UPlayerInventoryItem*	CharacterEntity;
	UPROPERTY()									USkeletalMeshComponent* CharacterMesh;
	UPROPERTY(Replicated)						UPlayerProfileItem*		Equip[ItemSlotTypeId::End];

};