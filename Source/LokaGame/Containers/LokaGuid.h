// VRSPRO

#pragma once

#include "Misc/Guid.h"
#include "LokaGuid.generated.h"

#if CPP      //noexport class

struct FLokaGuid
{
public:

	FLokaGuid() : Source() {}
	FLokaGuid(const FGuid& InGuid) : Source(InGuid) {}

	operator FGuid&() { return Source; }
	operator FGuid() const { return Source; }

	/**
	* Compares two GUIDs for equality.
	*
	* @param X The first GUID to compare.
	* @param Y The second GUID to compare.
	* @return true if the GUIDs are equal, false otherwise.
	*/
	friend bool operator==(const FLokaGuid& X, const FLokaGuid& Y)
	{
		return X.Source == Y.Source;
	}

	friend bool operator==(const FLokaGuid& X, const FGuid& Y)
	{
		return X.Source == Y;
	}

	friend bool operator==(const FGuid& X, const FLokaGuid& Y)
	{
		return X == Y.Source;
	}

	FLokaGuid& operator=(const FGuid &InGuid)
	{
		Source = InGuid;
		return *this;
	}

	/**
	* Compares two GUIDs for inequality.
	*
	* @param X The first GUID to compare.
	* @param Y The second GUID to compare.
	* @return true if the GUIDs are not equal, false otherwise.
	*/
	friend bool operator!=(const FLokaGuid& X, const FLokaGuid& Y)
	{
		return X.Source != Y.Source;
	}

	/**
	* Exports the GUIDs value to a string.
	*
	* @param ValueStr Will hold the string value.
	* @param DefaultValue The default value.
	* @param Parent Not used.
	* @param PortFlags Not used.
	* @param ExportRootScope Not used.
	* @return true on success, false otherwise.
	* @see ImportTextItem
	*/
	LOKAGAME_API bool ExportTextItem(FString& ValueStr, FLokaGuid const& DefaultValue, UObject* Parent, int32 PortFlags, class UObject* ExportRootScope) const;

	/**
	* Imports the GUIDs value from a text buffer.
	*
	* @param Buffer The text buffer to import from.
	* @param PortFlags Not used.
	* @param Parent Not used.
	* @param ErrorText The output device for error logging.
	* @return true on success, false otherwise.
	* @see ExportTextItem
	*/
	LOKAGAME_API bool ImportTextItem(const TCHAR*& Buffer, int32 PortFlags, class UObject* Parent, FOutputDevice* ErrorText);

	void Invalidate()
	{
		Source.Invalidate();
	}

	bool IsValid() const
	{
		return Source.IsValid();
	}

	friend FArchive& operator<<(FArchive& Ar, FLokaGuid& LokaGuid)
	{
		return Ar << LokaGuid.Source;
	}

	bool Serialize(FArchive& Ar)
	{
		Ar << *this;
		return true;
	}

	friend uint32 GetTypeHash(const FLokaGuid& Guid)
	{
		return FCrc::MemCrc_DEPRECATED(&Guid.Source, sizeof(FGuid));
	}

	FString ToString(const EGuidFormats InFormat = EGuidFormats::DigitsWithHyphens) const;
	static FLokaGuid FromString(const FString& InGuidStr);

	FGuid Source;
};

#endif

#if !CPP      //noexport class

USTRUCT(immutable, noexport, BlueprintType)
struct FLokaGuid
{
	UPROPERTY(EditAnywhere, SaveGame, Category = LokaGuid)
	FGuid Source;
};

#endif

template <> struct TIsPODType<FLokaGuid> { enum { Value = true }; };

template<>
struct TStructOpsTypeTraits<FLokaGuid> : public TStructOpsTypeTraitsBase2<FLokaGuid>
{
	enum
	{
		WithExportTextItem = true,
		WithImportTextItem = true,
		WithZeroConstructor = true,
		WithSerializer = true,
	};
};
IMPLEMENT_STRUCT(LokaGuid);


template<> struct TBaseStructure<FLokaGuid>
{
	LOKAGAME_API static UScriptStruct* Get();
};
