// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "PlayerInventoryItemWeapon.h"
#include "Models/InvertoryItemWrapper.h"
#include "GameSingleton.h"

void UPlayerInventoryItemWeapon::SetItemEntity(UItemBaseEntity* InEntity)
{
	Super::SetItemEntity(InEntity);

	if (GetEntityWeapon())
	{
		InstalledMaterial = 0;

		WeaponProperty = GetEntityWeapon()->GetWeaponProperty();

		for (auto DefaultModule : GetEntityWeapon()->DefaultModules)
		{
			if (DefaultModule != INDEX_NONE)
			{
				if (auto localEntityInstance = UGameSingleton::Get()->FindItem(DefaultModule, ECategoryTypeId::Addon))
				{
					InstalledAddons.Add(NewObject<UPlayerInventoryItem>(this, localEntityInstance->GetInventoryClass()));
					InstalledAddons.Last()->InitializeInstanceDynamic(localEntityInstance);
				}
			}
		}
	}
}

void UPlayerInventoryItemWeapon::InitializeAddons(const FInvertoryItemWrapper& MyData, const TArray<UPlayerInventoryItem*>& InAddonsList)
{
	Super::InitializeAddons(MyData, InAddonsList);

	if (GetEntityWeapon())
	{
		WeaponProperty = GetEntityWeapon()->GetWeaponProperty();

		if (MyData.Modifications.Num())
		{
			TMap<FString, float> ModifersList;

			for (auto Modifer : MyData.Modifications)
			{
				if (Modifer.Type > 0)
				{
					ModifersList.Add(GetEnumValueToString<EWeaponModificationTypeId>("EWeaponModificationTypeId", static_cast<EWeaponModificationTypeId>(Modifer.Type)), Modifer.value / 100.0f);
				}
			}

			ApplyModifers(&WeaponProperty, FWeaponPropertyBase::StaticStruct(), ModifersList);
		}
	}
}

bool UPlayerInventoryItemWeapon::ApplyModifers(void* InTargetStruct, UScriptStruct* InTargetClass, const TMap<FString, float>& InModifers)
{
	bool bSuccessfully = false;
	
	if (InModifers.Num())
	{
		for (TFieldIterator<UProperty> PropIt(InTargetClass); PropIt; ++PropIt)
		{
			UProperty* PropertyPtr = *PropIt;
			FString PropertyName = PropertyPtr->GetName();

			if (InModifers.Contains(PropertyName))
			{
				const float &ModiferValue = InModifers.FindChecked(PropertyName);

				void* Value = PropertyPtr->ContainerPtrToValuePtr<uint8>(InTargetStruct);
				if (auto propertyAsInt = Cast<UIntProperty>(PropertyPtr))
				{
					auto OriginValue = propertyAsInt->GetPropertyValue(Value);
					auto TargetValue = OriginValue + float(OriginValue) * ModiferValue;
					propertyAsInt->SetPropertyValue(Value, TargetValue);
					bSuccessfully = true;
				}
				else if (auto propertyAsFloat = Cast<UFloatProperty>(PropertyPtr))
				{
					auto OriginValue = propertyAsFloat->GetPropertyValue(Value);
					auto TargetValue = OriginValue + OriginValue * ModiferValue;
					propertyAsFloat->SetPropertyValue(Value, TargetValue);
					bSuccessfully = true;
				}
			}
		}
	}

	return bSuccessfully;
}

void UPlayerInventoryItemWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UPlayerInventoryItemWeapon, WeaponProperty);
}
