#include "LokaGame.h"
#include "PlayerProfileItem.h"
#include "PlayerProfileActor.h"
#include "PlayerInventoryItem.h"

APlayerProfileActor::APlayerProfileActor()
{
	//for(auto i = 0; i < ItemSlotTypeId::End; i++)
	//{
	//	auto slot = Equip[i] = CreateDefaultSubobject<UProfileCharacterEquip>(*FString::Printf(TEXT("Equip_%d"), i));	
	//		
	//	slot->SetupAttachment(GetMesh());
	//	slot->GetMesh()->SetCollisionObjectType(ECC_Pawn);
	//	slot->GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	//	slot->GetMesh()->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);
	//	slot->GetMesh()->SetMasterPoseComponent(GetMesh());
	//}
}

void APlayerProfileActor::SetCharacterEntity(UPlayerInventoryItem* character)
{
	CharacterEntity = character;
	OnRep_Instance();
}

void APlayerProfileActor::OnRep_Instance() const
{
	if (auto c = GetValidObject(CharacterEntity))
	{
		CharacterMesh->SetSkeletalMesh(c->GetPreviewMesh());
	}
}


void APlayerProfileActor::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APlayerProfileActor, CharacterEntity);
	DOREPLIFETIME(APlayerProfileActor, Equip);
}
