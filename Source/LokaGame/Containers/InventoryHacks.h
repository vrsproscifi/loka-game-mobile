// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/DataAsset.h"
#include "InventoryHacks.generated.h"


USTRUCT(BlueprintType)
struct FProfileHack
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditAnywhere) int32			CharacterModelId;
	UPROPERTY(EditAnywhere)	int32			PrimaryWeaponModelId;
	UPROPERTY(EditAnywhere)	int32			PrimaryWeaponSkin;
	UPROPERTY(EditAnywhere)	TArray<int32>	PrimaryWeaponAddons;
	UPROPERTY(EditAnywhere)	int32			SecondaryWeaponModelId;
	UPROPERTY(EditAnywhere)	int32			SecondaryWeaponSkin;
	UPROPERTY(EditAnywhere)	TArray<int32>	SecondaryWeaponAddons;

	FProfileHack()
		: CharacterModelId(-1)
		, PrimaryWeaponModelId(-1)
		, PrimaryWeaponSkin(-1)
		, PrimaryWeaponAddons()
		, SecondaryWeaponModelId(-1)
		, SecondaryWeaponSkin(-1)
		, SecondaryWeaponAddons()
	{}
};

UCLASS()
class LOKAGAME_API UInventoryHack : public UDataAsset
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditDefaultsOnly, Category = Hack)
	TArray<FProfileHack> HackedProfiles;

};
