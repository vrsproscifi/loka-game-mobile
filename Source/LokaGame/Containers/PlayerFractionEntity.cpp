#include "LokaGame.h"
#include "PlayerFractionEntity.h"
#include "Fraction/FractionEntity.h"
#include "GameSingleton.h"

UPlayerFractionEntity::UPlayerFractionEntity()
	: Entity(nullptr)
	, FractionId(FractionTypeId::End)
	, CurrentFractionEntity(nullptr)
{

}

FractionTypeId UPlayerFractionEntity::GetModelId() const
{
	if (auto e = GetValidObject(Entity))
	{
		return e->GetModelIdAs<FractionTypeId>();
	}
	return FractionId;
}

FTypeDescription UPlayerFractionEntity::GetDescription() const
{
	if (auto e = GetValidObject(Entity))
	{
		return e->GetDescription();
	}
	return FTypeDescription();
}

FTypeDescription UPlayerFractionEntity::GetDescription()
{
	if (auto e = GetValidObject(Entity))
	{
		return e->GetDescription();
	}
	return FTypeDescription();
}

const FSlateBrush* UPlayerFractionEntity::GetImage() const
{
	if (auto e = GetValidObject(Entity))
	{
		return e->GetImage();
	}
	return nullptr;
}

const FSlateBrush* UPlayerFractionEntity::GetImage()
{
	if (auto e = GetValidObject(Entity))
	{
		return e->GetImage();
	}
	return nullptr;
}

void UPlayerFractionEntity::SetFractionEntity(UFractionEntity* fraction)
{
	Entity = fraction;
	if(auto e = GetValidObject(Entity))
	{
		FractionId = e->GetModelId();
	}
}

void UPlayerFractionEntity::OnRep_Instance()
{
	Entity = UGameSingleton::Get()->FindFraction(FractionId);
}

FText UPlayerFractionEntity::GetFractionName()
{
	if (auto e = GetValidObject(Entity))
	{
		return e->GetDescription().Name;
	}
	return FText::GetEmpty();
}

FText UPlayerFractionEntity::GetFractionName() const
{
	if (auto e = GetValidObject(Entity))
	{
		return e->GetDescription().Name;
	}
	return FText::GetEmpty();
}


FText UPlayerFractionEntity::GetFractionDescription()
{
	if (auto e = GetValidObject(Entity))
	{
		return e->GetDescription().Description;
	}
	return FText::GetEmpty();
}

FText UPlayerFractionEntity::GetFractionDescription() const
{
	if (auto e = GetValidObject(Entity))
	{
		return e->GetDescription().Description;
	}
	return FText::GetEmpty();
}

FText UPlayerFractionEntity::GetFractionRank() const
{
	if (auto r = GetCurrentRank())
	{
		return r->Name;
	}
	return FText::GetEmpty();
}

FText UPlayerFractionEntity::GetFractionReputation()
{
	return FText::FromString(FString::Printf(TEXT("%d / %d"), GetProgress().Reputation, GetProgress().NextReputation));
}

FText UPlayerFractionEntity::GetFractionReputation() const
{
	return FText::FromString(FString::Printf(TEXT("%d / %d"), GetProgress().Reputation, GetProgress().NextReputation));
}

FText UPlayerFractionEntity::GetFractionRank()
{
	if(auto r = GetCurrentRank())
	{
		return r->Name;
	}
	return FText::GetEmpty();
}

const TArray<UItemBaseEntity*>* UPlayerFractionEntity::GetItemList() const
{
	if (auto e = GetFractionEntity())
	{
		return &e->Items;
	}
	return nullptr;
}

const TArray<UItemBaseEntity*>* UPlayerFractionEntity::GetItemList()
{
	if (auto e = GetFractionEntity())
	{
		return &e->Items;
	}
	return nullptr;
}

const FFractionRank* UPlayerFractionEntity::GetCurrentRank() const
{
	return GetRank(Progress.CurrentRank);
}

const FFractionRank* UPlayerFractionEntity::GetCurrentRank()
{
	return GetRank(Progress.CurrentRank);
}

const FFractionRank* UPlayerFractionEntity::GetRank(const uint32& rank)
{
	if (auto e = GetValidObject(Entity))
	{
		return e->GetRank(rank);
	}
	return nullptr;
}

const FFractionRank* UPlayerFractionEntity::GetRank(const uint32& rank) const
{
	if (auto e = GetValidObject(Entity))
	{
		return e->GetRank(rank);
	}
	return nullptr;
}

void UPlayerFractionEntity::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UPlayerFractionEntity, Progress);
	DOREPLIFETIME(UPlayerFractionEntity, FractionId);
	DOREPLIFETIME(UPlayerFractionEntity, CurrentFractionEntity);
}