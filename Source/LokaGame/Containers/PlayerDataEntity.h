#pragma once

//=======================================================
//							[ Headers ]
#include "PlayerDataEntity.generated.h"

class UBasicGameComponent;

UCLASS(Abstract)
class UPlayerDataEntity : public USceneComponent
{
	GENERATED_BODY()

public:
  UPlayerDataEntity();

protected:
	UBasicGameComponent* GetOwnerComponent() const;

	template<typename TOwnerType>
	TOwnerType* GetOwnerComponent() const
	{
		return GetValidObject<TOwnerType, AActor>(GetOwner());
	}

public:	

	bool SetEntityId(const FString& id)
	{
		return FGuid::Parse(id, EntityId);
	}

	void SetEntityId(const FGuid& id)
	{
		EntityId = id;
	}
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Entity)
	const FGuid& GetEntityId() const
	{
		return EntityId;
	}
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Entity)
	bool IsEqual(const FGuid& id) const
	{
		return EntityId == id;
	}

	bool IsEqual(const FString& InStringIndex) const
	{
		FGuid TargetGuid;
		return FGuid::Parse(InStringIndex, TargetGuid) && IsEqual(TargetGuid);
	}

protected:

	UPROPERTY(Replicated) FGuid EntityId;
};