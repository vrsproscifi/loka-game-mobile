// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#pragma once
#include "ShooterSpectatorPawn.generated.h"

class AUTCharacter;

UCLASS(config = Game, Blueprintable, BlueprintType)
class AShooterSpectatorPawn : public ASpectatorPawn
{
	GENERATED_UCLASS_BODY()

	// Begin ASpectatorPawn overrides
	/** Overridden to implement Key Bindings the match the player controls */
	virtual void SetupPlayerInputComponent(class UInputComponent* InInputComponent) override;
	// End Pawn overrides
	
	// Frame rate linked look
	void LookUpAtRate(float Val);

public:

	virtual void PossessedBy(class AController* NewController) override;
	virtual void UnPossessed() override;

	virtual void MoveForward(float Val) override;
	virtual void MoveRight(float Val) override;
	virtual void MoveUp_World(float Val) override;

	virtual void AddControllerYawInput(float Val) override;
	virtual void AddControllerPitchInput(float Val) override;

	bool IsAllowControlling() const;
	virtual void InitializeTarget();

protected:

	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY()
	const AActor* TargetView;

	UPROPERTY()
	const APawn* LastPawn;

	UPROPERTY()
	FVector LastPawnLocation;

	float TargetViewSetTime;

	UPROPERTY()
	USpringArmComponent* SpringArm;
};
