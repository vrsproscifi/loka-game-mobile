// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BaseCharacter.generated.h"

class ATransportBase;
class IUsableActorInterface;
class UPlayerInventoryItem;
class ABasePlayerController;

UCLASS()
class LOKAGAME_API ABaseCharacter : public ACharacter
{
	friend class ATransportBase;

	GENERATED_BODY()

public:
	ABaseCharacter(const FObjectInitializer& ObjectInitializer);
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual void Tick(float DeltaTime) override;

	virtual void MoveForward(float Value);
	virtual void MoveRight(float Value);
	virtual void MoveUp(float Value);
	virtual void TurnAtRate(float Value);
	virtual void LookUpAtRate(float Value);

	template<bool Value>
	void OnToggleRunning_Helper() { OnToggleRunning(Value, true); }

	void OnToggleRunning_Alt();

	template<bool Value>
	void OnSwitchView_Helper() { OnSwitchView(Value); }
	void OnSwitchView_Alt() { OnSwitchView(!bIsFarView); }

	template<bool Value>
	void OnJump_Helper() { OnJump(Value); }
	virtual void OnJump(const bool InToggle);

	template<bool Value>
	void OnCrouch_Helper() { OnCrouch(Value); }
	void OnCrouch_Alt() { OnCrouch(!bIsCrouched); }
	virtual void OnCrouch(const bool InToggle);

	UFUNCTION(BlueprintCallable, Reliable, Server, WithValidation, Category = Input)
	void OnUseAny(const bool IsStart);

	UFUNCTION(BlueprintCallable, Reliable, Server, WithValidation, Category = Input)
	void OnSwitchView(const bool InToggle);

	UFUNCTION(BlueprintCallable, Reliable, Server, WithValidation, Category = Input)
	void OnToggleRunning(const bool InToggle, const bool InRequired = false);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Character)
	FORCEINLINE bool IsRunning() const { return bIsRunning; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Character)
	FORCEINLINE bool IsRequiredRunning() const{ return bIsRequiredRunning; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Character)
	UPlayerInventoryItem* GetInstance() const;

	UFUNCTION(BlueprintCallable, Category = Character)
	virtual void InitializeInstance(UPlayerInventoryItem* InInstance);

	UFUNCTION(BlueprintCallable, Category = Character)
	virtual void InitializeItem(UPlayerInventoryItem* InItem);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Character)
	AActor* GetUsableObject();

	template<typename T = APlayerController>
	T* GetSafeController() const
	{
		return GetValidObject<T, AController>(GetController());
	}

	template<typename T = ABasePlayerController>
	T* GetBaseController() const
	{
		return GetValidObject<T, AController>(GetController());
	}

	UWorld* GetSafeWorld()
	{
		return GetValidObject(GetWorld());
	}

	UWorld* GetSafeWorld() const
	{
		return GetValidObject(GetWorld());
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Transport)
	ATransportBase*	GetTransport() const { return ControlledTransport; }

protected:

	UPROPERTY()
	TScriptInterface<IUsableActorInterface> UsableTargetObject;

	UFUNCTION()
	virtual void OnRep_IsFarView() {}

	UPROPERTY(ReplicatedUsing = OnRep_IsFarView)
	bool bIsFarView;

	UFUNCTION()
	virtual void OnRep_IsRunning() {}

	UPROPERTY(ReplicatedUsing=OnRep_IsRunning)
	bool bIsRunning;

	UPROPERTY(ReplicatedUsing = OnRep_IsRunning)
	bool bIsRequiredRunning;

	UFUNCTION()
	virtual void OnRep_Instance();

	UPROPERTY(ReplicatedUsing = OnRep_Instance)
	UPlayerInventoryItem* Instance;

	UPROPERTY(BlueprintReadWrite, Replicated, Category = Transport)
	ATransportBase*	ControlledTransport;

	UPROPERTY(EditDefaultsOnly, Category = Fonts)
	UFont* Icon_Name;

	UPROPERTY(EditDefaultsOnly, Category = Fonts)
	UFont* Icon_Awesome;
};
