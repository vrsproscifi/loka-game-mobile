// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "BaseCharacter.h"

#include "PlayerInventoryItem.h"
#include "Engine/ActorChannel.h"
#include "Interfaces/UsableActorInterface.h"

ABaseCharacter::ABaseCharacter(const FObjectInitializer& ObjectInitializer) 
	: Super(ObjectInitializer)
	, Instance(nullptr)
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	bIsFarView = false;
	bIsRunning = false;

#if !UE_SERVER
	static ConstructorHelpers::FObjectFinder<UFont> FontNameOb(TEXT("/Game/1LOKAgame/UserInterface/Fonts/Days"));
	Icon_Name = FontNameOb.Object;

	static ConstructorHelpers::FObjectFinder<UFont> FontAwesomeOb(TEXT("/Game/1LOKAgame/UserInterface/Fonts/FontAwesome"));
	Icon_Awesome = FontAwesomeOb.Object;
#endif
}

void ABaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &ABaseCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ABaseCharacter::MoveRight);
	PlayerInputComponent->BindAxis("MoveUp", this, &ABaseCharacter::MoveUp);
	PlayerInputComponent->BindAxis("Turn", this, &ABaseCharacter::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ABaseCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &ABaseCharacter::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ABaseCharacter::LookUpAtRate);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ABaseCharacter::OnJump_Helper<true>).bConsumeInput = false;
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ABaseCharacter::OnJump_Helper<false>).bConsumeInput = false;

	PlayerInputComponent->BindAction("SwitchView", IE_Pressed, this, &ABaseCharacter::OnSwitchView_Alt).bConsumeInput = false;

	PlayerInputComponent->BindAction("Run", IE_Pressed, this, &ABaseCharacter::OnToggleRunning_Helper<true>).bConsumeInput = false;
	PlayerInputComponent->BindAction("Run", IE_Released, this, &ABaseCharacter::OnToggleRunning_Helper<false>).bConsumeInput = false;
	PlayerInputComponent->BindAction("RunToggle", IE_Pressed, this, &ABaseCharacter::OnToggleRunning_Alt).bConsumeInput = false;

	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &ABaseCharacter::OnCrouch_Helper<true>).bConsumeInput = false;
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &ABaseCharacter::OnCrouch_Helper<false>).bConsumeInput = false;
	PlayerInputComponent->BindAction("CrouchToggle", IE_Pressed, this, &ABaseCharacter::OnCrouch_Alt).bConsumeInput = false;
}

void ABaseCharacter::OnJump(const bool InToggle)
{
	if (InToggle)
	{
		Jump();
	}
}

void ABaseCharacter::OnCrouch(const bool InToggle)
{
	if (InToggle)
	{
		Crouch(false);
	}
	else
	{
		UnCrouch(false);
	}
}

bool ABaseCharacter::OnUseAny_Validate(const bool IsPressed) { return true; }
void ABaseCharacter::OnUseAny_Implementation(const bool IsPressed)
{
	if (UsableTargetObject)
	{
		if (IsPressed)
		{
			IUsableActorInterface::Execute_OnInteractStart(UsableTargetObject.GetObject(), this);
		}
		else
		{
			IUsableActorInterface::Execute_OnInteractLost(UsableTargetObject.GetObject(), this);
		}
	}
}

void ABaseCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// find out which way is forward
		const FRotator Rotation = GetControlRotation();
		FRotator YawRotation = FRotator(0, Rotation.Yaw, 0);

		AddMovementInput(FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X), Value);
	}
}

void ABaseCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// find out which way is right
		const FRotator Rotation = GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		AddMovementInput(FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y), Value);
	}
}

void ABaseCharacter::MoveUp(float Value)
{
	if (Value != 0.0f)
	{
		AddMovementInput(FVector(0.f, 0.f, 1.f), Value);
	}
}

void ABaseCharacter::TurnAtRate(float Val)
{
	AddControllerYawInput(Val * 45.0f * GetWorld()->GetDeltaSeconds());
}

void ABaseCharacter::LookUpAtRate(float Val)
{
	AddControllerPitchInput(Val * 45.0f * GetWorld()->GetDeltaSeconds());
}

void ABaseCharacter::OnToggleRunning_Alt()
{
	OnToggleRunning(!IsRequiredRunning(), true);
}

bool ABaseCharacter::OnSwitchView_Validate(const bool InToggle) { return true; }
void ABaseCharacter::OnSwitchView_Implementation(const bool InToggle)
{
	bIsFarView = InToggle;
	OnRep_IsFarView();
}

bool ABaseCharacter::OnToggleRunning_Validate(const bool InToggle, const bool InRequired) { return true; }
void ABaseCharacter::OnToggleRunning_Implementation(const bool InToggle, const bool InRequired)
{
	bIsRunning = InToggle;
	if (InRequired)
	{
		bIsRequiredRunning = InToggle;
	}
}

void ABaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	auto tmpUsable = GetUsableObject();
	auto tmpInterface = Cast<IUsableActorInterface>(tmpUsable);

	if (UsableTargetObject.GetInterface() != tmpInterface)
	{
		if (UsableTargetObject)
		{
			IUsableActorInterface::Execute_OnFocusLost(UsableTargetObject.GetObject(), this);
		}

		UsableTargetObject.SetObject(tmpUsable);
		UsableTargetObject.SetInterface(tmpInterface);

		if (UsableTargetObject)
		{
			IUsableActorInterface::Execute_OnFocusStart(UsableTargetObject.GetObject(), this);
		}
	}
}


AActor* ABaseCharacter::GetUsableObject()
{
	AActor* Answer = nullptr;
	if (const auto world = GetSafeWorld())
	{
		if(const auto PlayerController = GetSafeController())
		{
			FVector TraceStart = FVector::ZeroVector;
			FRotator CamRot;
			PlayerController->GetPlayerViewPoint(TraceStart, CamRot);
			const float tDist = bIsFarView ? 10.0f : 3.0f;
			FVector TraceEnd = TraceStart + CamRot.Vector() * 100.0f * tDist;

			FHitResult HitResult;
			FCollisionQueryParams QueryFindUsableObject(TEXT("FindUsableObject"), false, this);

			FCollisionResponseParams ResponseFindUsableObject = FCollisionResponseParams::DefaultResponseParam;
			ResponseFindUsableObject.CollisionResponse.SetAllChannels(ECR_Block);
			ResponseFindUsableObject.CollisionResponse.SetResponse(COLLISION_USABLE, ECR_Block);

			if (world->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, COLLISION_USABLE, QueryFindUsableObject, ResponseFindUsableObject))
			{
				Answer = HitResult.GetActor();
			}
		}
	}
	return GetValidObject(Answer);
}

void ABaseCharacter::InitializeInstance(UPlayerInventoryItem* InInstance)
{
	Instance = InInstance;
	OnRep_Instance();
}

void ABaseCharacter::InitializeItem(UPlayerInventoryItem* InItem)
{
	
}

UPlayerInventoryItem* ABaseCharacter::GetInstance() const
{
	return GetValidObject(Instance);
}

void ABaseCharacter::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABaseCharacter, Instance);
	DOREPLIFETIME(ABaseCharacter, ControlledTransport);
	DOREPLIFETIME(ABaseCharacter, bIsRunning);
	DOREPLIFETIME_CONDITION(ABaseCharacter, bIsRequiredRunning, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(ABaseCharacter, bIsFarView, COND_OwnerOnly);
}

void ABaseCharacter::OnRep_Instance()
{

}