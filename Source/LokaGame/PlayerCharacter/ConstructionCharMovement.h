// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "ConstructionCharMovement.generated.h"

class UCharacterBaseEntity;

UCLASS()
class LOKAGAME_API UConstructionCharMovement : public UCharacterMovementComponent
{
	GENERATED_BODY()
	
public:

	// Current speed limit
	virtual float GetMaxSpeed() const override;

	// Try make smooth stop
	virtual void StopActiveMovement() override;
	
	// Try make smooth acceleration
	virtual float GetMaxAcceleration() const override;

	UCharacterBaseEntity* GetCharacterInstance() const;
	
};
