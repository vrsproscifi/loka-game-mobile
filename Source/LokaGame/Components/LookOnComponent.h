// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "LookOnComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class LOKAGAME_API ULookOnComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	ULookOnComponent();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = Targeting)
	void SetFilterClass(TSubclassOf<AActor> InClass);

	UFUNCTION(BlueprintCallable, Category = Targeting)
	void SetTargetRange(const float InDistance);

	UFUNCTION(BlueprintCallable, Category = Targeting)
	void SetTargetVisionAngle(const float InDegress);

	UFUNCTION(BlueprintCallable, Category = Targeting)
	void SetTargetCollisionChannel(const ECollisionChannel InCollisionChannel);

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = Targeting)
	FORCEINLINE float GetSourceRange() const { return GetHumanRange() * 100.0f; }

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = Targeting)
	FORCEINLINE float GetHumanRange() const { return TraceDistance; }

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = Targeting)
	FORCEINLINE float GetSourceAngle() const { return GetHumanAngle() / 2.0f; }

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = Targeting)
	FORCEINLINE float GetHumanAngle() const { return PeripheralVisionAngle; }

	UFUNCTION(BlueprintCallable, Category = Targeting)
	void RequestFindingTarget();

	UFUNCTION(BlueprintCallable, Category = Targeting)
	AActor* FindTarget();

protected:

	UPROPERTY(EditDefaultsOnly, Category = Targeting, meta = (DisplayName = "Filter By Class"))
	TSubclassOf<AActor> FilterClass;

	UPROPERTY()
	AActor* TargetActor;

	UPROPERTY(EditDefaultsOnly, Category = Targeting, meta = (DisplayName = "Finding range"))
	float TraceDistance;

	UPROPERTY(EditDefaultsOnly, Category = Targeting, meta = (DisplayName = "Finding cone"))
	float PeripheralVisionAngle;

	UPROPERTY()
	float PeripheralVisionCosine;

	UPROPERTY(EditDefaultsOnly, Category = Targeting, meta = (DisplayName = "[WIP] Is Auto Re-Finding"))
	bool bIsAutoFinding;

	UPROPERTY(EditDefaultsOnly, Category = Targeting, meta = (DisplayName = "[WIP] Interval of Auto Re-Finding"))
	float AutoFindingInterval;

	UPROPERTY(EditDefaultsOnly, Category = Targeting, meta = (DisplayName = "Finding Collision Channel"))
	TEnumAsByte<ECollisionChannel> TargetCollisionChannel;
};
