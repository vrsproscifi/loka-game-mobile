﻿#include "LokaGame.h"
#include "BuildingComponent.h"
#include "RequestManager/RequestManager.h"
#include "Build/ItemBuildEntity.h"
#include "GameSingleton.h"
#include "BuildSystem/BuildPlacedComponent.h"
#include "ConstructionGameState.h"
#include "ConstructionPlayerState.h"
#include "InventoryComponent.h"
#include "PlayerInventoryItem.h"

UBuildingComponent::UBuildingComponent()
	: BuildingOwnerComponent(nullptr)
{
	RequestUpdateProperty = CreateRequest(FServerHost::MasterClient, "Building/OnUpdateBuildingProperty");
	RequestUpdateProperty->BindObject<FUpdateBuildingProperty>(200).AddUObject(this, &UBuildingComponent::OnUpdateProperty);
	RequestUpdateProperty->OnFailedResponse.BindUObject(this, &UBuildingComponent::OnUpdatePropertyFailed);


	GetBuildingProperty = CreateRequest(FServerHost::MasterClient, "Building/GetListOfProperties", 10.0f, true, FTimerDelegate::CreateUObject(this, &UBuildingComponent::SendGetBuildingPropertyRequest));
	GetBuildingProperty->BindArray<TArray<FBuildingPropertyContainer>>(200).AddUObject(this, &UBuildingComponent::OnGetBuildingProperty_Successfully);

}

TAttribute<FGuid> UBuildingComponent::GetIdentityToken() const
{
	return TAttribute<FGuid>::Create([this]
	{
		if (auto gs = Cast<ILokaNetUserInterface>(GetBuildingOwnerComponent()))
		{
			return gs->GetIdentityToken();
		}

		return FGuid();
	});
}

void UBuildingComponent::SendGetBuildingList(const FGuid& InPlayerId) const
{
	checkf(GetBuildingList.IsValid(), TEXT("GetBuildingList request not created!"));

	GetBuildingList->SendRequestObject<FString>(InPlayerId.ToString(), GetIdentityToken());
}

bool UBuildingComponent::SendRequestUpdateProperty_Validate(const FUpdateBuildingProperty& InData) { return true; }
void UBuildingComponent::SendRequestUpdateProperty_Implementation(const FUpdateBuildingProperty& InData)
{
	RequestUpdateProperty->SendRequestObject(InData);
}

void UBuildingComponent::UpdateBuildingsOwner(AActor* InNewOwner)
{
	if (HasAuthority() && BuildingOwnerComponent != InNewOwner)
	{
		BuildingOwnerComponent = InNewOwner;

		for (auto& b : WorldBuildings)
		{
			if (b.Value && b.Value->IsValidLowLevel())
			{
				b.Value->SetOwner(InNewOwner);
			}
		}
	}
}

void UBuildingComponent::SendGetBuildingPropertyRequest()
{
	GetBuildingProperty->GetRequest();
}

void UBuildingComponent::OnGetBuildingProperty_Successfully(const TArray<FBuildingPropertyContainer>& response)
{
	for (auto MyProperty : response)
	{
		if (WorldBuildings.Contains(MyProperty.BuildingId.Source))
		{
			WorldBuildings.FindChecked(MyProperty.BuildingId.Source)->UpdateDynamicPropertyData(MyProperty.Key, MyProperty.Value, true);
		}
	}
}

void UBuildingComponent::OnUpdateProperty(const FUpdateBuildingProperty& InData)
{
	if (WorldBuildings.Contains(InData.BuildingId.Source))
	{
		WorldBuildings.FindChecked(InData.BuildingId.Source)->UpdateDynamicPropertyData(InData.Key, InData.Value, true);
	}
}

void UBuildingComponent::OnUpdatePropertyFailed(const FRequestExecuteError& InError)
{
	
}

void UBuildingComponent::OnGetBuildingListSuccessfully(const FPlayerWorldModel& response)
{
	if (LastBuildingsData.IsEqual(response) == false)
	{
		LastBuildingsData = response;

		ConstructionWorldData.PrevName = LastBuildingsData.Preview.Name;
		ConstructionWorldData.CurrentName = LastBuildingsData.Current.Name;
		ConstructionWorldData.NextName = LastBuildingsData.Next.Name;

		ConstructionWorldData.CurrentId = LastBuildingsData.Current.Id.Source;
		ConstructionWorldData.PrevId = LastBuildingsData.Preview.Id.Source;
		ConstructionWorldData.NextId = LastBuildingsData.Next.Id.Source;

		for (auto wBuildingPair : WorldBuildings)
		{
			if (wBuildingPair.Value && wBuildingPair.Value->IsValidLowLevel())
			{
				wBuildingPair.Value->Destroy(true);
			}
		}

		WorldBuildings.Empty();
	}

	//=======================================
	FActorSpawnParameters ActorSpawnParameters;
	ActorSpawnParameters.Owner = GetBuildingOwnerComponent();
	ActorSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	//=======================================
	TArray<ABuildPlacedComponent*> placed;
	GetPreplasedWorldObjects(placed);

	const auto MyGameState = GetWorldGameState<AConstructionGameState>(GetWorld());
	const auto MyGameSingleton = UGameSingleton::Get();
	const auto MyInventoryComponent = MyGameState ? MyGameState->GetInventoryComponent() : nullptr;

	//=======================================
	for (auto i : response.Buildings)
	{
		//=================================================
		if (WorldBuildings.Contains(i.ItemId))
		{
			continue;
		}

		//=================================================
		UPlayerInventoryItem* InventoryInstance = nullptr;

		if (MyInventoryComponent)
		{
			InventoryInstance = MyInventoryComponent->FindItem(i.ModelId, ECategoryTypeId::Building);
		}

		//============================================
		auto SingletonInstance = MyGameSingleton->FindItem<UItemBuildEntity>(i.ModelId, CategoryTypeId::Building);
		if (SingletonInstance == nullptr)
		{
			continue;
		}

		//============================================
		auto SpawnedActor = GetPlacedObject(placed, i.ModelId);
		if (SpawnedActor == nullptr)
		{
			SpawnedActor = GetWorld()->SpawnActor<ABuildPlacedComponent>(SingletonInstance->ComponentTemplate, FTransform(i.Rotation, i.Position), ActorSpawnParameters);
		}

		if (SpawnedActor)
		{
			//============================================
			SpawnedActor->GUID = i.ItemId.Source;
		
			//============================================
			if (InventoryInstance)
			{
				SpawnedActor->InitializeInstance(InventoryInstance);
			}
			else
			{
				SpawnedActor->InitializeInstance(SingletonInstance);
			}

			SpawnedActor->OnComponentPlaced();
			SpawnedActor->Health.X = FMath::Min<float>(i.Health, SpawnedActor->Health.Y);
			SpawnedActor->OnRep_Health();

			for (auto PropertyPair : i.Properties)
			{
				//	TODO:: Передавай структуру. Default - значение из постройки (инстанса). Value - текущее значение в постройке игрока.
				//	Например, Key = storage. Default -- максимальная вместительность. Value - сколько сейчас
				//	Например, Key = buildingtime. Default -- сколько строится. Value - сколько осталось до конца
				//	Например, Key = profile. Default -- empty. Value - guid профиля
				SpawnedActor->InitializeDynamicPropertyData(PropertyPair.Key, PropertyPair.Value);
			}

			WorldBuildings.Add(SpawnedActor->GUID, SpawnedActor);

			if (SpawnedActor->Health.X < SpawnedActor->Health.Y)
			{
				DamagedBuildings.Add(SpawnedActor->GUID, SpawnedActor);
			}
		}
	}

	if (WorldBuildings.Num())
	{
		TArray<ABuildPlacedComponent*> WorldBuildingsList;
		WorldBuildings.GenerateValueArray(WorldBuildingsList);
		OnBuildingsLoaded.Broadcast(WorldBuildingsList);

		if (DamagedBuildings.Num())
		{
			TArray<ABuildPlacedComponent*> DamagedBuildingsList;
			DamagedBuildings.GenerateValueArray(DamagedBuildingsList);
			OnDamagedBuildingsFound.Broadcast(DamagedBuildingsList);
		}
	}

	GetBuildingProperty->StartTimer(&GetWorld()->GetTimerManager());
}

void UBuildingComponent::OnGetBuildingListFailed(const FRequestExecuteError& error) const
{

}


ABuildPlacedComponent* UBuildingComponent::GetPlacedObject(const TArray<ABuildPlacedComponent*>& objects, const uint32& model)
{
	for (auto preplaced : objects)
	{
		if (preplaced && preplaced->GetPrePlacedInstanceId() == model)
		{
			return preplaced;
		}
	}

	return nullptr;
}


void UBuildingComponent::GetPreplasedWorldObjects(TArray<ABuildPlacedComponent*>& objects) const
{
	TArray<AActor*> FindPlacedBuildings;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABuildPlacedComponent::StaticClass(), FindPlacedBuildings);

	for (auto PlacedBuilding : FindPlacedBuildings)
	{
		auto PrePlacedBuilding = Cast<ABuildPlacedComponent>(PlacedBuilding);
		if (PrePlacedBuilding && PrePlacedBuilding->IsPrePlaced())
		{
			objects.Add(PrePlacedBuilding);
		}
	}
}

bool UBuildingComponent::IsPlayerOwnedWorld() const
{
	auto MyGameState = GetWorldGameState<AConstructionGameState>(GetWorld());
	if (MyGameState)
	{
		return MyGameState->GetOwnerPlayerId() == ConstructionWorldData.CurrentId;
	}

	return false;
}

void UBuildingComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UBuildingComponent, BuildingOwnerComponent);	
	DOREPLIFETIME(UBuildingComponent, ConstructionWorldData);
}
