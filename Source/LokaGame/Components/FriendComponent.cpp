﻿#include "LokaGame.h"
#include "FriendComponent.h"
#include "RequestManager/RequestManager.h"
#include "SNotifyList.h"
#include "RichTextHelpers.h"

DEFINE_LOG_CATEGORY(LogFriendComponent);

UFriendComponent::UFriendComponent()
{
	//==========================================
	RequestList = CreateRequest(FServerHost::MasterClient, "Friend/GetList", 2.0f, true, FTimerDelegate::CreateUObject(this, &UFriendComponent::SendRequestList));
	RequestList->BindArray<TArray<FPlayerBasicInformation>>(200).AddUObject(this, &UFriendComponent::OnList);

	//==========================================
	RequestSearch = CreateRequest(FServerHost::MasterClient, "Friend/Search");
	RequestSearch->BindArray<TArray<FPlayerBasicInformation>>(200).AddUObject(this, &UFriendComponent::OnSearch);

	//==========================================
	RequestAdd = CreateRequest(FServerHost::MasterClient, "Friend/Add");
	RequestAdd->BindObject<FString>(200).AddUObject(this, &UFriendComponent::OnAdd);

	//==========================================
	RequestRemove = CreateRequest(FServerHost::MasterClient, "Friend/Remove");
	RequestRemove->BindObject<FString>(200).AddUObject(this, &UFriendComponent::OnRemove);

	//==========================================
	RequestRespond = CreateRequest(FServerHost::MasterClient, "Friend/ConfirmAdd");
	RequestRespond->BindObject<FAddFriendRequestRespond>(200).AddUObject(this, &UFriendComponent::OnRespond);

	
}

void UFriendComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(UFriendComponent, FriendsList, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(UFriendComponent, LastSearchResult, COND_OwnerOnly);
}

//===========================================================================================================================================================

void UFriendComponent::OnAuthorizationComplete()
{
	StartTimer(RequestList);
}

//===========================================================================================================================================================
//																Send requests/Отправка запросов

void UFriendComponent::SendRequestList_Implementation()
{
	RequestList->GetRequest();
}

void UFriendComponent::SendRequestSearch_Implementation(const FString& InPlayerName)
{
	RequestSearch->SendRequestObject(InPlayerName);
}

void UFriendComponent::SendRequestAdd_Implementation(const FGuid& InPlayerId)
{
	RequestAdd->SendRequestObject(InPlayerId);
}

void UFriendComponent::SendRequestRemove_Implementation(const FGuid& InPlayerId)
{
	RequestRemove->SendRequestObject(InPlayerId);
}

void UFriendComponent::SendRequestRespond_Implementation(const FAddFriendRequestRespond& InResponse)
{
	RequestRespond->SendRequestObject(InResponse);
}

//===========================================================================================================================================================
//																Receive answers/Получение ответов   

void UFriendComponent::OnList(const TArray<FPlayerBasicInformation>& InList)
{
	FriendsList = InList;
	OnRep_FriendsList();
}

void UFriendComponent::OnSearch(const TArray<FPlayerBasicInformation>& InList)
{
	LastSearchResult = InList;
	OnRep_LastSearchResult();
}

void UFriendComponent::OnAdd(const FString& InPlayerName)
{
	OnClientAdd(InPlayerName);
	UE_LOG(LogFriendComponent, Log, TEXT("OnAdd >> InPlayerName: %s"), *InPlayerName);
}

void UFriendComponent::OnRemove(const FString& InPlayerName)
{
	OnClientRemove(InPlayerName);
	UE_LOG(LogFriendComponent, Log, TEXT("OnRemove >> InPlayerName: %s"), *InPlayerName);
}

void UFriendComponent::OnRespond(const FAddFriendRequestRespond& InResponse)
{
	OnClientRespond(InResponse);
	UE_LOG(LogFriendComponent, Log, TEXT("OnRespond >> InResponse: %s"), *InResponse.ToString());
}

//===========================================================================================================================================================
//																Client receive answers/Получение ответов для клиента   

void UFriendComponent::OnClientList_Implementation(const TArray<FPlayerBasicInformation>& InList)
{
	
}

void UFriendComponent::OnClientSearch_Implementation(const TArray<FPlayerBasicInformation>& InList)
{
	
}

void UFriendComponent::OnClientAdd_Implementation(const FString& InPlayerName)
{
	FNotifyInfo info;
	info.Title = NSLOCTEXT("UFriendComponent", "Notify.Friends.Title", "Friends");
	info.Content = FText::Format(NSLOCTEXT("UFriendComponent", "Notify.OnAdd.Content", "You sent invite into friends for \"{0}\"."), FRichHelpers::TextHelper_NotifyValue.SetValue(InPlayerName).ToText());
	AddNotify(info);
}

void UFriendComponent::OnClientRemove_Implementation(const FString& InPlayerName)
{
	FNotifyInfo info;
	info.Title = NSLOCTEXT("UFriendComponent", "Notify.Friends.Title", "Friends");
	info.Content = FText::Format(NSLOCTEXT("UFriendComponent", "Notify.OnRemove.Content", "You removed \"{0}\" from friends."), FRichHelpers::TextHelper_NotifyValue.SetValue(InPlayerName).ToText());
	AddNotify(info);
}

void UFriendComponent::OnClientRespond_Implementation(const FAddFriendRequestRespond& InResponse)
{
	FNotifyInfo info;
	info.Title = NSLOCTEXT("UFriendComponent", "Notify.Friends.Title", "Friends");
	if (InResponse.IsAccept)
	{
		info.Content = FText::Format(NSLOCTEXT("UFriendComponent", "Notify.OnRespond.Content.Accepted", "You have accepted the request into friends from \"{0}\"."), FRichHelpers::TextHelper_NotifyValue.SetValue(InResponse.PlayerName).ToText());
	}
	else
	{
		info.Content = FText::Format(NSLOCTEXT("UFriendComponent", "Notify.OnRespond.Content.Rejected", "You is decline the request into friends from \"{0}\"."), FRichHelpers::TextHelper_NotifyValue.SetValue(InResponse.PlayerName).ToText());
	}

	AddNotify(info);
}

void UFriendComponent::OnRep_FriendsList()
{
	OnListUpdate.Broadcast(FriendsList);
}

void UFriendComponent::OnRep_LastSearchResult()
{
	OnSearchListUpdate.Broadcast(LastSearchResult);
}
