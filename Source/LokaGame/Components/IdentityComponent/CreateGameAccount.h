#pragma once
#include "CreateGameAccount.generated.h"

USTRUCT()
struct FCreateGameAccountRequest
{
	GENERATED_USTRUCT_BODY()

	//==========================
	//			[ Property ]
	UPROPERTY()	FString Login;

	//==========================
	//			[ Constructor ]

	FCreateGameAccountRequest()
	{
	}

	FCreateGameAccountRequest(const FString& login)
		: Login(login)
	{

	}

};

USTRUCT()
struct FCreateGameAccountResponse
{
	GENERATED_USTRUCT_BODY()


};