#include "LokaGame.h"
#include "BasicGameComponent.h"
#include "ConstructionPlayerController.h"
#include "ConstructionPlayerState.h"
#include "SNotifyList.h"
#include "RequestManager/RequestManager.h"
#include "OnlineGameMode.h"
#include "GameSingleton.h"
#include "Interfaces/LokaNetUserInterface.h"

UBasicGameComponent::UBasicGameComponent()
{
	auto ps = GetOwnerState();
	if(ps)
	{
		ps->OnAuthorizationCompleteDelegate.AddUObject(this, &UBasicGameComponent::OnAuthorizationComplete_Proxy);
	}
}

void UBasicGameComponent::AddNotify(const FNotifyInfo& info)
{
	SNotifyList::Get()->AddNotify(info);
}


ABasePlayerController* UBasicGameComponent::GetOwnerController() const
{
	if(const auto o = GetOwnerState())
	{
		return GetValidObject<ABasePlayerController, AActor>(o->GetOwner());
	}
	return nullptr;
}

ABasePlayerState* UBasicGameComponent::GetOwnerState() const
{
	return Cast<ABasePlayerState>(GetValidObject(GetOwner()));
}

//===================================================================
//  Game  Mode Helper

AGameMode* UBasicGameComponent::GetBasicGameMode() const
{
  if(auto w = GetValidWorld())
  {
    return Cast<AGameMode>(GetValidObject(w->GetAuthGameMode()));
  }
  return nullptr;
}

AOnlineGameMode* UBasicGameComponent::GetOnlineGameMode() const
{
  return Cast<AOnlineGameMode>(GetBasicGameMode());
}

//===================================================================

void UBasicGameComponent::PostInitProperties()
{
	Super::PostInitProperties();
}

TSharedPtr<FOperationRequest> UBasicGameComponent::CreateRequest(const FServerHost& server_host, const FString& operation_name, TokenAttrib token, const float& rate, const bool& loop, FTimerDelegate timer)
{
  return AddRequest(TSharedPtr<FOperationRequest>(new FOperationRequest(this, server_host, operation_name, rate, loop, timer, token)));
}

TSharedPtr<FOperationRequest> UBasicGameComponent::CreateRequest(const FServerHost& server_host, const FString& operation_name, const float& rate, const bool& loop, FTimerDelegate timer, TokenAttrib token, const uint64& timerLimit, const bool& timerValidate)
{
  return AddRequest(TSharedPtr<FOperationRequest>(new FOperationRequest(this, server_host, operation_name, rate, loop, timer, token, timerLimit, timerValidate)));
}

TSharedPtr<FOperationRequest> UBasicGameComponent::CreateRequest(const FServerHost& server_host,
	const FString& operation_name, const float& rate, const bool& loop, FTimerDelegate timer, const uint64& timerLimit,
	const bool& timerValidate)
{
	return CreateRequest(server_host, operation_name, rate, loop, timer, GetIdentityToken(), timerLimit, timerValidate);
}

TSharedPtr<FOperationRequest> UBasicGameComponent::AddRequest(TSharedPtr<FOperationRequest> request)
{
  OperationRequests.Add(request);
  return request;
}

TAttribute<FGuid> UBasicGameComponent::GetIdentityToken() const
{
	return TAttribute<FGuid>::Create([&] () -> FGuid
	{
		if (GetOwnerController() && GetOwnerController()->IsLocalController())
		{
			if (GetNetMode() == NM_Client || GetNetMode() == NM_ListenServer || GetNetMode() == NM_Standalone)
			{
				return UGameSingleton::Get()->LastLocalToken;
			}
		}		

		if (auto MyUser = Cast<ILokaNetUserInterface>(GetOwner()))
		{
			return MyUser->GetIdentityToken();
		}

		return FGuid();
	});
}

bool UBasicGameComponent::HasAuthority() const
{
	return (GetOwner() && GetOwner()->HasAuthority());
}

bool UBasicGameComponent::HasLocalClientComponent() const
{
	if (GetOwnerController() && GetOwnerController()->IsLocalController())
	{
		if (GetNetMode() == NM_Client || GetNetMode() == NM_ListenServer || GetNetMode() == NM_Standalone)
		{
			return true;
		}
	}

	return false;
}

TScriptInterface<ILokaNetUserInterface> UBasicGameComponent::GetUserOwner() const
{
	return TScriptInterface<ILokaNetUserInterface>(GetOwner());
}

void UBasicGameComponent::BeginPlay()
{
	Super::BeginPlay();

	if (!HasAnyFlags(RF_ClassDefaultObject | RF_ArchetypeObject))
	{
		if (GetOwnerController() && GetOwnerController()->IsLocalController())
		{
			if (GetNetMode() == NM_Client || GetNetMode() == NM_ListenServer || GetNetMode() == NM_Standalone)
			{
				if (GetWorld() && GetWorld()->IsValidLowLevel())
				{
					if (FApp::IsGame())
					{
						CreateGameWidgets();
					}
				}
			}
		}
	}
}

void UBasicGameComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
  //===============================
	if (auto tm = GetTimerManager())
	{
		tm->ClearAllTimersForObject(this);
    for (auto &r : OperationRequests)
    {
      r->StopTimer(tm);
    }
	}

  //===============================
  OperationRequests.Reset();

  //===============================
	Super::EndPlay(EndPlayReason);
}



void UBasicGameComponent::OnAuthorizationComplete()
{

}


UWorld* UBasicGameComponent::GetValidWorld() const
{
	return GetValidObject(GetWorld());
}

UWorld* UBasicGameComponent::GetValidWorld()
{
  return GetValidObject(GetWorld());
}

FTimerManager* UBasicGameComponent::GetTimerManager() const
{
	if(auto w = GetValidWorld())
	{
		return &w->GetTimerManager();
	}
	return nullptr;
}

FTimerManager* UBasicGameComponent::GetTimerManager()
{
	if (auto w = GetValidWorld())
	{
		return &w->GetTimerManager();
	}
	return nullptr;
}

void UBasicGameComponent::StartTimer(TSharedPtr<FOperationRequest>& operation, const float& delay) const
{
	if (auto tm = GetTimerManager())
	{
		if (operation.IsValid() && operation.Get())
		{
			operation->StartTimer(tm, delay);
		}
	}
}

void UBasicGameComponent::StartTimer(TSharedPtr<FOperationRequest>& operation, const float& delay)
{
	if (auto tm = GetTimerManager())
	{
		if (operation.IsValid() && operation.Get())
		{
			operation->StartTimer(tm, delay);
		}
	}
}

void UBasicGameComponent::StopTimer(TSharedPtr<FOperationRequest>& operation) const
{
	if (auto tm = GetTimerManager())
	{
		if (operation.IsValid() && operation.Get())
		{
			operation->StopTimer(tm);
		}
	}
}

void UBasicGameComponent::StopTimer(TSharedPtr<FOperationRequest>& operation)
{
	if (auto tm = GetTimerManager())
	{
		if (operation.IsValid() && operation.Get())
		{
			operation->StopTimer(tm);
		}
	}
}