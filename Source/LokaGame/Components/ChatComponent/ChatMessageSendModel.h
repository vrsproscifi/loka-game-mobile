#pragma once


#include "ChatMessageSendModel.generated.h"

USTRUCT()
struct FChatMessageSendModel
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()					FLokaGuid									ChanelId;
	UPROPERTY()					FString										Message;
};
