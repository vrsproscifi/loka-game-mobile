#pragma once


#include "ChatChanelModel.generated.h"

UENUM()
namespace EChatChanelGroup
{
	enum Type
	{
		Global,
		Private,
		Squad,
		Match,
		Clan,
		End
	};
}

USTRUCT()
struct FChatChanelModel
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()					FLokaGuid									ChanelId;
	UPROPERTY()					FString										ChanelName;

	UPROPERTY()					FString										GroupId;
	UPROPERTY()					TEnumAsByte<EChatChanelGroup::Type>			ChanelGroup;

	friend bool operator==(const FChatChanelModel& LHS, const FChatChanelModel& RHS)
	{
		return LHS.ChanelId == RHS.ChanelId && LHS.ChanelName == RHS.ChanelName && LHS.GroupId == RHS.GroupId && LHS.ChanelGroup == RHS.ChanelGroup;
	}
};