#pragma once


#include "ChatMessageReciveModel.generated.h"

USTRUCT()
struct FChatMessageReciveModel
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()					FLokaGuid									ChanelId;
	UPROPERTY()					FLokaGuid									MessageId;
	UPROPERTY()					FLokaGuid									MemberId;
	UPROPERTY()					FLokaGuid									PlayerId;
	UPROPERTY()					FString										PlayerName;

	UPROPERTY()					FString										Message;
	UPROPERTY()					uint64										CreatedDate;

	friend bool operator==(const FChatMessageReciveModel& LHS, const FChatMessageReciveModel& RHS)
	{
		return	LHS.ChanelId == RHS.ChanelId && 
				LHS.MessageId == RHS.MessageId &&
				LHS.MemberId == RHS.MemberId &&
				LHS.PlayerId == RHS.PlayerId &&
				LHS.PlayerName == RHS.PlayerName &&
				LHS.Message == RHS.Message &&
				LHS.CreatedDate == RHS.CreatedDate;
	}
};
