#pragma once
#include "RequestDataContainer.generated.h"

USTRUCT()
struct FBasicDataParser
{
	GENERATED_USTRUCT_BODY()

	template<typename TIn, typename TOut>
	static inline bool Parse(const TIn& value, TOut& result);
};

template<typename TIn, typename TOut>
inline bool FBasicDataParser::Parse(const TIn& value, TOut& result)
{
	result = static_cast<TOut>(value);
	return true;
}

template<>
inline bool FBasicDataParser::Parse<FString, FGuid>(const FString& value, FGuid& result)
{
	FGuid id;
	if (FGuid::Parse(value, id))
	{
		result = id;
		return true;
	}
	else
	{
		return false;
	}
}

template<>
inline bool FBasicDataParser::Parse<FString, FLokaGuid>(const FString& value, FLokaGuid& result)
{
	FGuid id;
	if (FGuid::Parse(value, id))
	{
		result.Source = id;
		return true;
	}
	else
	{
		return false;
	}
}

template<>
inline bool FBasicDataParser::Parse<FString, FName>(const FString& value, FName& result)
{
	result = FName(*value);
	return true;
}

USTRUCT()
struct FTextDataContiner : public FBasicDataParser
{
	GENERATED_USTRUCT_BODY()
	UPROPERTY()	FString Value;
	FTextDataContiner()	{}
	FTextDataContiner(const ANSICHAR* value) : Value(UTF8_TO_TCHAR(value)) {}
	FTextDataContiner(const FName& value) : Value(value.ToString()) {}
	FTextDataContiner(const FText& value) : Value(value.ToString())	{}
	FTextDataContiner(const FString& value) : Value(value){}
	FTextDataContiner(const FGuid& value) : Value(value.ToString(EGuidFormats::DigitsWithHyphens)) {}
	FTextDataContiner(const FLokaGuid& value) : Value(value.ToString()) {}
};

USTRUCT()
struct FINT32DataContiner : public FBasicDataParser
{
	GENERATED_USTRUCT_BODY()
	UPROPERTY()	int32 Value;
	FINT32DataContiner() {}
	FINT32DataContiner(const int32& value) : Value(value) {}
};

USTRUCT()
struct FINT64DataContiner : public FBasicDataParser
{
	GENERATED_USTRUCT_BODY()
	UPROPERTY()	int64 Value;
	FINT64DataContiner() {}
	FINT64DataContiner(const int64& value) : Value(value) {}
	FINT64DataContiner(const int32& value) : Value(value) {}
};

USTRUCT()
struct FUINT32DataContiner : public FBasicDataParser
{
	GENERATED_USTRUCT_BODY()
	UPROPERTY()	uint32 Value;
	FUINT32DataContiner() {}
	FUINT32DataContiner(const uint32& value) : Value(value) {}
};

USTRUCT()
struct FUINT64DataContiner : public FBasicDataParser
{
	GENERATED_USTRUCT_BODY()
	UPROPERTY()	uint64 Value;
	FUINT64DataContiner() {}
	FUINT64DataContiner(const uint32& value) : Value(value) {}
	FUINT64DataContiner(const uint64& value) : Value(value) {}
};


USTRUCT()
struct FFloatDataContiner : public FBasicDataParser
{
	GENERATED_USTRUCT_BODY()
	UPROPERTY()	float Value;
	FFloatDataContiner() {}
	FFloatDataContiner(const float& value) : Value(value) {}
};


USTRUCT()
struct FEnumDataContiner : public FBasicDataParser
{
	GENERATED_USTRUCT_BODY()
	UPROPERTY()	uint64 Value;
	FEnumDataContiner() {}
	FEnumDataContiner(const uint64& value) : Value(value) {}
	FEnumDataContiner(const uint32& value) : Value(value) {}
	FEnumDataContiner(const int64& value) : Value(value) {}
	FEnumDataContiner(const int32& value) : Value(value) {}
	FEnumDataContiner(const uint8& value) : Value(value) {}
};