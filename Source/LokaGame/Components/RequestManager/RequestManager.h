#pragma once

#include "ServiceController/OperationContainer.h"
#include "FailedOperationResponse.h"
#include "RequestDataContainer.h"
#include "JsonObjectConverter.h"

typedef const TAttribute<FGuid>& TokenAttribRef;
typedef TAttribute<FGuid> TokenAttrib;


class FOperationRequest;
//=========================================================
//						[Request Handler]

class FBaseRequestHandler
{
protected:
	DECLARE_DELEGATE(FOnRequestHandlerUnbind);

	FOnRequestHandlerUnbind OnRequestHandlerUnbind;

public:
	mutable FDateTime	LastHandleDate;
	mutable FString		LastHandleContent;
	mutable const FOperationRequest* Operation;

	FBaseRequestHandler(const FOperationRequest* operation);
	virtual ~FBaseRequestHandler() = 0;

	virtual bool OnDeSerializeProxy(const FString& objectStr) const;
	virtual bool OnDeSerialize(const FString& objectStr) const = 0;
	virtual void Unbind() = 0;
};

class FCallBackRequestHandler : public FBaseRequestHandler
{
public:
	DECLARE_MULTICAST_DELEGATE(FOnDeSerialize);

	FOnDeSerialize Handler;

	virtual void Unbind() override
	{
		Handler.Clear();
	}

	FCallBackRequestHandler(const FOperationRequest* const operation)
		: FBaseRequestHandler(operation)
	{

	}

	virtual bool OnDeSerialize(const FString& /*objectStr*/) const override
	{
		if (Handler.IsBound())
		{
			Handler.Broadcast();
		}

		return true;
	}
};

//=========================================================
//						[Object Handler]

template<class TResponseObject>
class FObjectRequestHandler : public FBaseRequestHandler
{ 
public:	
	DECLARE_MULTICAST_DELEGATE_OneParam(FOnDeSerialize, const TResponseObject&)

	virtual void Unbind() override
	{
		Handler.Clear();
	}

	FObjectRequestHandler(const FOperationRequest* const operation)
		: FBaseRequestHandler(operation)
	{

	}

	FOnDeSerialize Handler;
	virtual bool OnDeSerialize(const FString& objectStr) const override
	{
		TResponseObject object;
		
		//===============================
#ifdef _MSC_VER  
		static_assert(std::is_class<TResponseObject>::value, "Is not child of UStruct");
#endif
		if (FJsonObjectConverter::JsonObjectStringToUStruct(objectStr, &object, 0, 0))
		{
			//===============================
			if (Handler.IsBound())
			{
				Handler.Broadcast(object);
			}
			return true;
		}
		return false;
	}
};

#define DECLARE_REQUEST_HANDLER(CONTAINER, TYPE) \
template<> \
class FObjectRequestHandler<TYPE> : public FBaseRequestHandler \
{\
public:\
	DECLARE_MULTICAST_DELEGATE_OneParam(FOnDeSerialize, const TYPE&)\
	FOnDeSerialize Handler;\
	virtual void Unbind() override\
	{\
		Handler.Clear();\
	}\
	FObjectRequestHandler(const FOperationRequest* const operation):FBaseRequestHandler(operation){}\
	virtual bool OnDeSerialize(const FString& objectStr) const override\
	{\
		CONTAINER object;\
		if (FJsonObjectConverter::JsonObjectStringToUStruct(objectStr, &object, 0, 0))\
		{\
			if (Handler.IsBound())\
			{\
				TYPE value;\
				if (CONTAINER::Parse(object.Value, value))\
				{\
					Handler.Broadcast(value);\
				}\
				else\
				{\
					return false;\
				}\
			}\
			return true;\
		}\
		return false;\
	}\
};

DECLARE_REQUEST_HANDLER(FTextDataContiner, FGuid);
DECLARE_REQUEST_HANDLER(FTextDataContiner, FLokaGuid);
DECLARE_REQUEST_HANDLER(FTextDataContiner, FString);
DECLARE_REQUEST_HANDLER(FTextDataContiner, FName);

DECLARE_REQUEST_HANDLER(FINT32DataContiner, bool);
DECLARE_REQUEST_HANDLER(FINT32DataContiner, int32);
DECLARE_REQUEST_HANDLER(FUINT32DataContiner, uint32);
DECLARE_REQUEST_HANDLER(FINT64DataContiner, int64);
DECLARE_REQUEST_HANDLER(FUINT64DataContiner, uint64);
DECLARE_REQUEST_HANDLER(FFloatDataContiner, float);

//=========================================================
//						[Array Handler]

template<class TResponseObject>
class FArrayObjectRequestHandler : public FBaseRequestHandler
{
public:
	DECLARE_MULTICAST_DELEGATE_OneParam(FOnDeSerialize, const TResponseObject&)


	void Unbind() override
	{
		Handler.Clear();
	}

	FOnDeSerialize Handler;
	
	FArrayObjectRequestHandler(const FOperationRequest* const operation)
		: FBaseRequestHandler(operation)
	{

	}

	virtual bool OnDeSerialize(const FString& objectStr) const override
	{
		TResponseObject object;

		//===============================
#ifdef _MSC_VER  
		static_assert(std::is_class<TResponseObject>::value, "Is not child of UStruct");
#endif
		if (FJsonObjectConverter::JsonArrayStringToUStruct(objectStr, &object, 0, 0))
		{
			//===============================
			if (Handler.IsBound())
			{
				Handler.Broadcast(object);
			}
			return true;
		}
		return false;
	}
};


class FOperationRequest
{
	//======================================
	//				Property
	TMap<int32, TSharedPtr<FBaseRequestHandler>> Handlers;
	mutable TWeakPtr<IHttpRequest>		Request;
	const	FServerHost					ServerHost;
	const	FString						OperationName;
	const	TokenAttrib					Token;

	mutable FDateTime					LastExecuteDate;
	mutable FRequestExecuteError		LastExecuteError;
	mutable FRequestExecuteError		PrewExecuteError;

	//======================================
	//				Timer

	mutable FDateTime					TimerStartDate;
	mutable FDateTime					TimerLastExecuteDate;

	mutable FTimerHandle				TimerHandle;
	mutable FTimerDelegate				TimerDelegate;
	mutable FTimerDelegate				TimerProxy;
	FHttpRequestCompleteDelegate		OnRequestComplete;

  //  For debug, not use!
	mutable const FTimerManager*			TimerManager;

	const float							TimerRate;
	const bool							TimerLoop;
	
	const bool							TimerValidate;
	const uint64						TimerLimit;
	mutable uint64						TimerItter;
	const UObject*						Context;

public:
	FString GetItterationString() const;
	uint64 GetItterationLimit() const;
	uint64 GetItterationNum() const;

	bool ItterationInc();
	void ResetItterations();

	bool IsValidTimer() const;
	FORCEINLINE bool IsTimerActive() const { return TimerHandle.IsValid(); }

	FTimerDelegate& GetTimerDelegate()
	{
		return TimerDelegate;
	}

	FTimerDelegate& GetTimerDelegate() const
	{
		return TimerDelegate;
	}

	void StartTimer(FTimerManager* InManager, const float& InStartDelay = 0);
	void StopTimer(FTimerManager* InManager);

	void OnHttpRequestComplete(FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSucceeded) const;

	void OnTimerItterationProxy();

	//======================================
	//				Contructor
public:
	//	��� �������������� ������ �� ����� �������, �������� ������ ������� ��� ��������������
	FOnFailedOperationResponse OnFailedResponse;

	//	��� ��������� ������� ��������� ������
	FOnFailedOperationResponse OnLoseAttemps;

public:
	static TSharedPtr<FOperationRequest> Factory
	(
		const UObject* context
		, const FServerHost& server_host
		, const FString& operation_name
		, TokenAttribRef token
		, const float& rate = 0
		, const bool& loop = false
		, FTimerDelegate timer = FTimerDelegate()
	)
	{
		return TSharedPtr<FOperationRequest>(new FOperationRequest(context, server_host, operation_name, rate, loop, timer, token));
	}

	static TSharedPtr<FOperationRequest> Factory
	(
		const UObject* context
		, const FServerHost& server_host
		, const FString& operation_name
		, const float& rate = 0
		, const bool& loop = false
		, FTimerDelegate timer = FTimerDelegate()
		, TokenAttrib token = TokenAttrib()
		, const uint64& timerLimit = 5
		, const bool& timerValidate = false
	)
	{
		return TSharedPtr<FOperationRequest>(new FOperationRequest(context, server_host, operation_name, rate, loop, timer, token, timerLimit, timerValidate));
	}

	FOperationRequest(
        const UObject* context
		, const FServerHost& server_host
		, const FString& operation_name
		, const float& rate
		, const bool& loop
		, FTimerDelegate timer
		, const	TokenAttrib	token
		, const uint64& timerLimit = 5
		, const bool& timerValidate = false
	)
		: ServerHost(server_host)
		, OperationName(operation_name)
		, Token(token)
		, TimerDelegate(timer)
		, TimerRate(rate)
		, TimerLoop(loop)
		, TimerValidate(timerValidate)
		, TimerLimit(timerLimit)
		, TimerItter(0)
		, Context(context)
	{
		OnRequestComplete.BindLambda([this, &c = Context](FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSucceeded)
		{
			if(GetValidObject(c))
			{
				FOperationRequest::OnHttpRequestComplete(HttpRequest, HttpResponse, bSucceeded);
			}
		});

		TimerProxy.BindRaw(this, &FOperationRequest::OnTimerItterationProxy);
	}


public:
	~FOperationRequest() 
	{
		Handlers.Empty();
		//================================
		OnLoseAttemps.Unbind();
		OnFailedResponse.Unbind();
		OnRequestComplete.Unbind();

		//================================
		TimerDelegate.Unbind();
		TimerProxy.Unbind();
		TimerHandle.Invalidate();

		//================================
		TimerManager = nullptr;

		if (Request.IsValid())
		{
			Request.Pin()->OnProcessRequestComplete().Unbind();
			Request.Pin()->CancelRequest();
		}
	}

	//======================================
	//				Methods
private:	
	//	���������� �� ������ �����, ���� ��� ��������
	FGuid ChoseToken(TokenAttribRef token) const
	{
		if(Token.IsSet())
		{
			if (Token.Get().IsValid())
			{
				return Token.Get();
			}
		}
		return token.Get();
	}

	//------------------------------------------
	//	���������� ��������������� ������
	void SendRequest(const FString& content, const FString& verb, TokenAttribRef token) const
	{
		LastExecuteDate = FDateTime::Now();
		auto RequestToken = ChoseToken(token);
		auto StringToken = RequestToken.IsValid() ? RequestToken.ToString() : FString();
		IOperationContainer::SendRequest(ServerHost, OperationName, content, StringToken, OnRequestComplete, Request, verb);
	}

public:
	//------------------------------------------
	//	���������� ������ ������� ��� ����������
	void GetRequest(TokenAttribRef token = TokenAttrib()) const
	{
		SendRequest("", FVerb::Get, token);
	}

	//------------------------------------------
	//	�������� ������� ������
	template<typename TRequestObject>
	FORCEINLINE void SendRequestObject(const TRequestObject& object, TokenAttribRef token = TokenAttrib()) const;

	//template<typename TRequestObject>
	//void GetRequestParam(const TRequestObject& object, TokenAttribRef token = "") const
	//{
	//	FString objectStr;
	//	if (FJsonObjectConverter::UStructToJsonObjectString(objectStr, &object, 0, 0))
	//	{
	//		SendRequest(objectStr, token);
	//	}
	//}


	//======================================
	//				Binding
	
	/*! ������ �������������: Request->Bind(200).AddUObject(this, &Class::Method);*/
	FCallBackRequestHandler::FOnDeSerialize& Bind(const int32& code)
	{
		if (Handlers.Contains(code) == false)
		{
			auto handler = new FCallBackRequestHandler(this);
			Handlers.Add(code, TSharedPtr<FCallBackRequestHandler>(handler));
		}
		return StaticCastSharedPtr<FCallBackRequestHandler>(Handlers[code])->Handler;
	}

	/*! ������ �������������: Request->BindObject<FSquadInviteInformation>(200).AddUObject(this, &Class::Method);*/
	template<typename TResponseObject>
	typename FObjectRequestHandler<TResponseObject>::FOnDeSerialize& BindObject(const int32& code)
	{
		if(Handlers.Contains(code) == false)
		{
			auto handler = new FObjectRequestHandler<TResponseObject>(this);
			Handlers.Add(code, TSharedPtr<FObjectRequestHandler<TResponseObject>>(handler));
		}
		return StaticCastSharedPtr<FObjectRequestHandler<TResponseObject>>(Handlers[code])->Handler;
	}

	/*! ������ �������������: Request->BindArray<TArray<FSquadInviteInformation>>(200).AddUObject(this, &Class::Method);*/
	template<typename TResponseObject>
	typename FArrayObjectRequestHandler<TResponseObject>::FOnDeSerialize& BindArray(const int32& code)
	{
		if (Handlers.Contains(code) == false)
		{
			auto handler = new FArrayObjectRequestHandler<TResponseObject>(this);
			Handlers.Add(code, TSharedPtr<FArrayObjectRequestHandler<TResponseObject>>(handler));
		}
		return StaticCastSharedPtr<FArrayObjectRequestHandler<TResponseObject>>(Handlers[code])->Handler;
	}
};

template<typename TRequestObject>
FORCEINLINE void FOperationRequest::SendRequestObject(const TRequestObject& object, TokenAttribRef token) const
{
	FString jsonStr;
	jsonStr.Reserve(128);
#ifdef _MSC_VER  
	static_assert(std::is_class<TRequestObject>::value, "Is not child of UStruct");
#endif
	FJsonObjectConverter::UStructToJsonObjectString(TRequestObject::StaticStruct(), &object, jsonStr, 0, 0);

	SendRequest(jsonStr, FVerb::Post, token);
}

template<>
FORCEINLINE void FOperationRequest::SendRequestObject<FGuid>(const FGuid& object, TokenAttribRef token) const
{
	SendRequestObject<FTextDataContiner>(object, token);
}

template<>
FORCEINLINE void FOperationRequest::SendRequestObject<FLokaGuid>(const FLokaGuid& object, TokenAttribRef token) const
{
	SendRequestObject<FTextDataContiner>(object, token);
}

template<>
FORCEINLINE void FOperationRequest::SendRequestObject<FString>(const FString& object, TokenAttribRef token) const
{
	SendRequestObject<FTextDataContiner>(object, token);
}

template<>
FORCEINLINE void FOperationRequest::SendRequestObject<FText>(const FText& object, TokenAttribRef token) const
{
	SendRequestObject<FTextDataContiner>(object, token);
}

template<>
FORCEINLINE void FOperationRequest::SendRequestObject<FName>(const FName& object, TokenAttribRef token) const
{
	SendRequestObject<FTextDataContiner>(object, token);
}

template<>
FORCEINLINE void FOperationRequest::SendRequestObject<int32>(const int32& object, TokenAttribRef token) const
{
	SendRequestObject<FINT32DataContiner>(object, token);
}

template<>
FORCEINLINE void FOperationRequest::SendRequestObject<int64>(const int64& object, TokenAttribRef token) const
{
	SendRequestObject<FINT32DataContiner>(object, token);
}

template<>
FORCEINLINE void FOperationRequest::SendRequestObject<uint32>(const uint32& object, TokenAttribRef token) const
{
	SendRequestObject<FINT32DataContiner>(object, token);
}

template<>
FORCEINLINE void FOperationRequest::SendRequestObject<uint64>(const uint64& object, TokenAttribRef token) const
{
	SendRequestObject<FINT32DataContiner>(object, token);
}

template<>
FORCEINLINE void FOperationRequest::SendRequestObject<float>(const float& object, TokenAttribRef token) const
{
	SendRequestObject<FFloatDataContiner>(object, token);
}