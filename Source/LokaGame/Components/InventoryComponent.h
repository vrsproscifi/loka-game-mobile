#pragma once
#include "BasicGameComponent.h"
#include "TypeDescription.h"
#include "IdentityController/Models/InvertoryItemWrapper.h"
#include "InventoryComponent.generated.h"

struct FUnlockItemResponseContainer;
struct FItemModelInfo;
class UPlayerInventoryItem;
class UItemBaseEntity;

DECLARE_MULTICAST_DELEGATE_OneParam(FOnInventorySignature, const TArray<UPlayerInventoryItem*>);

UCLASS(Transient)
class UInventoryComponent : public UBasicGameComponent
{
	GENERATED_BODY()

public:	

	UInventoryComponent();
	void CreateGameWidgets() override;
	
	UFUNCTION(Reliable, Server, WithValidation) void SendRequestLootData();

	void OnLoadPlayerInventorySuccessfully(const TArray<FInvertoryItemWrapper>& response);
	void OnLoadPlayerInventoryFailed(const FRequestExecuteError& error) const;
	void OnUnlockItemSuccessfully(const FUnlockItemResponseContainer& response);
	void OnSellItemHandle(const FGuid& InItemId);

	UPlayerInventoryItem* FindItem(const int32& InModelId, const CategoryTypeId::Type& InCategory) const;
	UPlayerInventoryItem* FindItem(const FItemModelInfo& InModelInfo) const;
	UPlayerInventoryItem* FindItem(const FGuid& InIndex) const;
	UPlayerInventoryItem* FindItem(const FString& InIndex) const;
	TArray<UPlayerInventoryItem*> FindList(const CategoryTypeId::Type& InCategory) const;

	FOnInventorySignature OnInventoryUpdate;

protected:

	TSharedPtr<FOperationRequest>				RequestLootData;

	UFUNCTION()									void OnLootData(const TArray<FInvertoryItemWrapper>& InData);

	UFUNCTION()									void OnRep_Items();
	UPROPERTY(ReplicatedUsing=OnRep_Items)		TArray<UPlayerInventoryItem*> Items;
	UPROPERTY()									TMap<FGuid, UPlayerInventoryItem*> ItemMap;

	UPlayerInventoryItem* CreateAndInsertItem(const FInvertoryItemWrapper& item);

	// Links from item with addons, need after items loaded by non-linear loading.
	// ������� � �������� � �����������, ����� �������� ����� �������� �������� �.�. �������� ��������� ��� ���������.
	void OnPostLoadHandle(const TArray<FInvertoryItemWrapper>& InLoadData);

	virtual void OnAuthorizationComplete() override;
};