﻿#include "LokaGame.h"
#include "ConstructionComponent.h"
#include "BuildingController/Models/WorldBuildingItemView.h"
#include "RequestManager/RequestManager.h"
#include "SMessageBox.h"
#include "PlayerInventoryItem.h"
#include "Build/ItemBuildEntity.h"
#include "BuildSystem/MinerPlacedComponent.h"
#include "SNotifyList.h"
#include "StringTableRegistry.h"
#include "MessageBoxCfg.h"

UConstructionComponent::UConstructionComponent()
{
	//	TODO:: После того как закончишь, нужно будет сменить GetListOfBuildings2 на GetListOfBuildings
	GetBuildingList = CreateRequest(FServerHost::MasterClient, "Building/GetListOfBuildings2");
	GetBuildingList->BindObject<FPlayerWorldModel>(200).AddUObject(this, &UConstructionComponent::OnGetBuildingListSuccessfully);
	GetBuildingList->OnFailedResponse.BindUObject(this, &UConstructionComponent::OnGetBuildingListFailed);

	Place = CreateRequest(FServerHost::MasterClient, "Building/PlaceObject");
	Place->BindObject<FWorldBuildingItemView>(200).AddUObject(this, &UConstructionComponent::OnBuildingPlaceSuccess);
	Place->Bind(400).AddUObject(this, &UConstructionComponent::ClientPlaceObjectOnFailed);
	Place->Bind(404).AddUObject(this, &UConstructionComponent::ClientPlaceObjectOnNotFoundInstance);
	Place->Bind(409).AddUObject(this, &UConstructionComponent::ClientPlaceObjectOnNotEnouthItem);	
	Place->OnFailedResponse.BindUObject(this, &UConstructionComponent::ClientPlaceObjectOnError);

	Remove = CreateRequest(FServerHost::MasterClient, "Building/RemoveObject");
	Remove->Bind(400).AddUObject(this, &UConstructionComponent::ClientRemoveObjectOnFailed);

	RequestHarvest = CreateRequest(FServerHost::MasterClient, "Building/OnHarvest");
	RequestHarvest->BindObject<FHarvestResponse>(200).AddUObject(this, &UConstructionComponent::OnHarvest);
	RequestHarvest->BindObject<FGuid>(400).AddUObject(this, &UConstructionComponent::OnHarvest_NotEnouthSpace);
	RequestHarvest->OnFailedResponse.BindUObject(this, &UConstructionComponent::OnHarvest_Failed);

}

void UConstructionComponent::OnHarvest(const  FHarvestResponse& response)
{
	if (WorldBuildings.Contains(response.Storage.ItemId) && response.Harvested)
	{
		if (auto TargetBuilding = Cast<AMinerPlacedComponent>(WorldBuildings.FindChecked(response.Storage.ItemId)))
		{
			TargetBuilding->UpdateDynamicPropertyData(AWarehousePlacedComponent::ParameterName_Storage, FString::FromInt(response.Storage.Storage), true);
			TargetBuilding->ClientHarvestSuccess(response.Harvested);
		}
	}
}



void UConstructionComponent::OnHarvest_NotEnouthSpace_Implementation(const FGuid& buildingId)
{
	// TODO: Такие вещи как GameState не имеющие игрока владельца не умеют передавать RPC на клиент
#if !UE_SERVER
	FNotifyInfo NotifyInfo;
	NotifyInfo.Title = NSLOCTEXT("All", "OnHarvest.NotEnouthSpace.Title", "There is not enough storage space!");
	NotifyInfo.Content = NSLOCTEXT("All", "OnHarvest.NotEnouthSpace.Desc", "To collect the earned funds, there is not enough space in the data centers (storages). Build more vaults so you do not lose prey!");

	QueueMessageBegin("OnHarvest.OnNotFoundInstance")
		SMessageBox::Get()->SetHeaderText(NotifyInfo.Title);
		SMessageBox::Get()->SetContent(NotifyInfo.Content);
		SMessageBox::Get()->SetButtonsText(LOCTABLE("BaseStrings", "All.Ok"));
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton&) { SMessageBox::Get()->ToggleWidget(false); });
		SMessageBox::Get()->SetEnableClose(true);
		SMessageBox::Get()->ToggleWidget(true);
	QueueMessageEnd


	AddNotify(NotifyInfo);
#endif
}

void UConstructionComponent::OnHarvest_Failed_Implementation(const FRequestExecuteError& error)
{
	// TODO: Такие вещи как GameState не имеющие игрока владельца не умеют передавать RPC на клиент
#if !UE_SERVER
	FNotifyInfo NotifyInfo;
	NotifyInfo.Title = NSLOCTEXT("Notify", "Notify.OnHarvest.Failed", "Error in harvesting");
	NotifyInfo.Content = NSLOCTEXT("Notify", "Notify.OnHarvest.Failed", "There was an error during the harvest. Try again later or contact support.");
	AddNotify(NotifyInfo);
#endif
}

bool UConstructionComponent::OnBuildingPlaceSuccess_Validate(const FWorldBuildingItemView& InData) { return true; }
void UConstructionComponent::OnBuildingPlaceSuccess_Implementation(const FWorldBuildingItemView& InData)
{
	if (WorldBuildings.Contains(InData.ItemId))
	{
		auto FoundBuilding = WorldBuildings.FindChecked(InData.ItemId);
		FoundBuilding->GUID = InData.NewItemId;

		FoundBuilding->SetOwner(GetBuildingOwnerComponent());

		for (auto Property : InData.Properties)
		{
			FoundBuilding->InitializeDynamicPropertyData(Property.Key, Property.Value);
		}		

		WorldBuildings.Remove(InData.ItemId);
		WorldBuildings.Add(FoundBuilding->GUID, FoundBuilding);
	}
}

bool UConstructionComponent::RequestBuildingPlace_Validate(ABuildPlacedComponent* InBuilding) { return true; }
void UConstructionComponent::RequestBuildingPlace_Implementation(ABuildPlacedComponent* InBuilding)
{
	if (InBuilding)
	{
		FWorldBuildingItemView BuildingItemView;

		BuildingItemView.Health = InBuilding->Health.Y;
		BuildingItemView.ItemId = InBuilding->GUID;
		BuildingItemView.ModelId = InBuilding->GetInstance()->GetEntity()->GetModelId();
		BuildingItemView.Position = InBuilding->GetActorLocation();
		BuildingItemView.Rotation = InBuilding->GetActorRotation();

		for (auto Parent : InBuilding->ParentBuildComponents)
		{
			if (Parent && Parent->IsValidLowLevel())
			{
				FWorldBuildingAttachView BuildingAttachView;
				BuildingAttachView.TargetId = Parent->GUID.ToString();

				BuildingItemView.Sockets.Add(BuildingAttachView);
			}
		}		

		WorldBuildings.Add(InBuilding->GUID, InBuilding);
		Place->SendRequestObject<FWorldBuildingItemView>(BuildingItemView);
	}
}

bool UConstructionComponent::RequestBuildingRemove_Validate(ABuildPlacedComponent* InBuilding) { return true; }
void UConstructionComponent::RequestBuildingRemove_Implementation(ABuildPlacedComponent* InBuilding)
{
	if (InBuilding)
	{
		Remove->SendRequestObject<FString>(InBuilding->GUID.ToString());
	}
}

bool UConstructionComponent::SendRequestHarvest_Validate(ABuildPlacedComponent* InBuilding) { return true; }
void UConstructionComponent::SendRequestHarvest_Implementation(ABuildPlacedComponent* InBuilding)
{
	if (InBuilding)
	{
		RequestHarvest->SendRequestObject<FString>(InBuilding->GUID.ToString());
	}
}

void UConstructionComponent::ClientPlaceObjectOnNotFoundInstance_Implementation()
{
	QueueMessageBegin("PlaceObject.OnNotFoundInstance")
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("All", "PlaceObject.OnNotFoundInstance.Title", "Instance not found"));
		SMessageBox::Get()->SetContent(NSLOCTEXT("All", "PlaceObject.OnNotFoundInstance.Desc", "Instance not found."));

		SMessageBox::Get()->SetButtonsText(LOCTABLE("BaseStrings", "All.Ok"));
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton&) { SMessageBox::Get()->ToggleWidget(false); });
		SMessageBox::Get()->SetEnableClose(true);
		SMessageBox::Get()->ToggleWidget(true);
	QueueMessageEnd
}

void UConstructionComponent::ClientPlaceObjectOnNotEnouthItem_Implementation()
{
	QueueMessageBegin("PlaceObject.OnNotEnouthItem")
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("All", "PlaceObject.OnNotEnouthItem.Title", "No details"));
		SMessageBox::Get()->SetContent(NSLOCTEXT("All", "PlaceObject.OnNotEnouthItem.Desc", "Insufficient facilities for construction."));

		SMessageBox::Get()->SetButtonsText(LOCTABLE("BaseStrings", "All.Ok"));
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton&) { SMessageBox::Get()->ToggleWidget(false); });
		SMessageBox::Get()->SetEnableClose(true);
		SMessageBox::Get()->ToggleWidget(true);
	QueueMessageEnd
}

void UConstructionComponent::ClientPlaceObjectOnError_Implementation(const FRequestExecuteError& error)
{
	UE_LOG(LogInit, Display, TEXT("UConstructionComponent::ClientPlaceObjectOnError | %s"), *error.ToString());

	QueueMessageBegin("PlaceObject.OnFailed")
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("All", "PlaceObject.OnFailed.Title", "Unknown error while building"));
		SMessageBox::Get()->SetContent(NSLOCTEXT("All", "PlaceObject.OnError.Desc", "Unknown error while building #2."));

	SMessageBox::Get()->SetButtonsText(LOCTABLE("BaseStrings", "All.Ok"));
	SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton&) { SMessageBox::Get()->ToggleWidget(false); });
	SMessageBox::Get()->SetEnableClose(true);
	SMessageBox::Get()->ToggleWidget(true);
	QueueMessageEnd
}

void UConstructionComponent::ClientPlaceObjectOnFailed_Implementation()
{
	QueueMessageBegin("PlaceObject.OnFailed")
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("All", "PlaceObject.OnFailed.Title", "Unknown error while building"));
		SMessageBox::Get()->SetContent(NSLOCTEXT("All", "PlaceObject.OnFailed.Desc", "Unknown error while building."));

		SMessageBox::Get()->SetButtonsText(LOCTABLE("BaseStrings", "All.Ok"));
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton&) { SMessageBox::Get()->ToggleWidget(false); });
		SMessageBox::Get()->SetEnableClose(true);
		SMessageBox::Get()->ToggleWidget(true);
	QueueMessageEnd
}

void UConstructionComponent::ClientRemoveObjectOnFailed_Implementation()
{
	QueueMessageBegin("RemoveObject.OnFailed")
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("All", "RemoveObject.OnFailed.Title", "Error while destroy the build"));
		SMessageBox::Get()->SetContent(NSLOCTEXT("All", "RemoveObject.OnFailed.Desc", "Error while destroy the build"));

		SMessageBox::Get()->SetButtonsText(LOCTABLE("BaseStrings", "All.Ok"));
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton&) { SMessageBox::Get()->ToggleWidget(false); });
		SMessageBox::Get()->SetEnableClose(true);
		SMessageBox::Get()->ToggleWidget(true);
	QueueMessageEnd
}