#pragma once
#include "ServiceController/BuildingController/Models/WorldBuildingItemView.h"
#include "MatchBuildingInformation.generated.h"

USTRUCT()
struct FMatchBuildingInformation
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()	FString OwnerId;
	UPROPERTY()	FString OwnerName;
	UPROPERTY()	TArray<FWorldBuildingItemView> Buildings;
};
