#pragma once

#include "Fraction/FractionTypeId.h"
#include "NodeSessionMember.generated.h"


USTRUCT()
struct FNodeSessionMember
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()		FLokaGuid PlayerId;
	UPROPERTY()		FLokaGuid MemberId;
	UPROPERTY()		FLokaGuid TokenId;
	UPROPERTY()		FLokaGuid TeamId;
	UPROPERTY()		FLokaGuid SquadId;
	UPROPERTY()		FString PlayerName;
	UPROPERTY()		FractionTypeId FractionId;	

	FString ToString() const
	{
		TMap<FString, FStringFormatArg> args;
		args.Add("PlayerId", FStringFormatArg(PlayerId.ToString()));
		args.Add("MemberId", FStringFormatArg(MemberId.ToString()));
		args.Add("TokenId", FStringFormatArg(TokenId.ToString()));
		args.Add("TeamId", FStringFormatArg(TeamId.ToString()));
		args.Add("SquadId", FStringFormatArg(SquadId.ToString()));
		args.Add("PlayerName", FStringFormatArg(PlayerName));


		return FString::Format(TEXT("> {PlayerName} [MemberId: {MemberId} | TeamId: {TeamId}] | [PlayerId: {PlayerId} | TokenId: {TokenId}] | SquadId: {SquadId}"), args);
	}

};