#pragma once
#include "MatchHeartBeatView.generated.h"


USTRUCT()
struct FMatchHeartBeatView
{
	GENERATED_USTRUCT_BODY()

	FMatchHeartBeatView(){}
	FMatchHeartBeatView(int32 timeLeftInSeconds, bool isAllowJoinNewPlayers) 
		: TimeLeftInSeconds(timeLeftInSeconds)
		, IsAllowJoinNewPlayers(isAllowJoinNewPlayers)
	{
	}

	UPROPERTY() int32 TimeLeftInSeconds;
	UPROPERTY() bool IsAllowJoinNewPlayers;
};

