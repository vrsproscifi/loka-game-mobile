#pragma once

#include "MatchBuildingView.generated.h"

USTRUCT()
struct FMatchBuildingView
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()	FString ItemId;
	UPROPERTY()	uint64 Health;
	UPROPERTY()	uint64 Storage;
};

USTRUCT()
struct FMatchBuildingUpdateContainer
{
	GENERATED_USTRUCT_BODY()
	UPROPERTY()	TArray<FMatchBuildingView> Items;
};
