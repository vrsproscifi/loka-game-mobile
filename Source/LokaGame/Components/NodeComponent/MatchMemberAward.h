#pragma once
#include "MatchMemberAward.generated.h"

enum class FractionTypeId : uint8;


USTRUCT()
struct FMatchMemberAward
{
	GENERATED_USTRUCT_BODY()
	
	UPROPERTY()	int64 Money;
	UPROPERTY()	int64 Donate;
	UPROPERTY()	int64 Bitcoin;
	UPROPERTY()	int64 Experience;
	UPROPERTY()	int64 Reputation;
	UPROPERTY()	FractionTypeId FractionId;
};