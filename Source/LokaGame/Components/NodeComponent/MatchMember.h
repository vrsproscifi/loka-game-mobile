#pragma once

#include "Entity/Service/ItemServiceModelId.h"


#include "MatchMemberAward.h"
#include "MatchMemberAchievement.h"
#include "MatchMember.generated.h"


USTRUCT()
struct FMatchMemberStatisticContainer
{
	GENERATED_USTRUCT_BODY()

	/// <summary>
	/// Count of kills
	/// </summary>
	UPROPERTY() 
		int64 Kills;

	/// <summary>
	/// Series of 3 Kill
	/// </summary>
	UPROPERTY() 
		int64 SmallSeriesKill;

	/// <summary>
	/// Series of 5 Kill
	/// </summary>
	UPROPERTY() 
		int64 ShortSeriesKill;

	/// <summary>
	/// Series of 7 Kill
	/// </summary>
	UPROPERTY() 
		int64 LargeSeriesKill;

	/// <summary>
	/// Series more 7 Kill
	/// </summary>
	UPROPERTY() 
		int64 EpicSeriesKill;

	/// <summary>
	/// Count of Deads
	/// </summary>
	UPROPERTY() 
		int64 Deads;

	/// <summary>
	/// Count of Assist in kill enemy
	/// </summary>
	UPROPERTY() 
		int64 Assists;

	/// <summary>
	/// Count of Assist in kill friend
	/// </summary>
	UPROPERTY() 
		int64 TeamKills;

	/// <summary>
	/// Count of shots per match
	/// </summary>
	UPROPERTY() 
		int64 Shots;

	/// <summary>
	/// Count of hit shots per match
	/// </summary>
	UPROPERTY() 
		int64 Hits;

	UPROPERTY()
		int64 Score;

	UPROPERTY()
		TEnumAsByte<UsingPremiumServiceFlag::Type> Service;
};

USTRUCT()
struct FMatchMember
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
		FString MemberId;

	UPROPERTY()
		FMatchMemberStatisticContainer Statistic;

	UPROPERTY()
	TArray<FMatchMemberAchievement> Achievements;

	UPROPERTY()
	FMatchMemberAward Award;
};