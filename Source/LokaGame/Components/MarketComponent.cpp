#include "LokaGame.h"
#include "MarketComponent.h"
#include "MarketComponent/MarketWindow.h"
#include "UTGameViewportClient.h"
#include "Models/UnlockItemContainer.h"
#include "Item/ItemBaseEntity.h"

//======================================
//				Models
#include "RequestManager/RequestManager.h"
#include "GameSingleton.h"
#include "SNotifyList.h"
#include "RichTextHelpers.h"
#include "ConstructionPlayerState.h"
#include "PlayerInventoryItem.h"

UMarketComponent::UMarketComponent()
{
	//=========================================================
	//					[ Store ]
	OnUnlockItem = CreateRequest(FServerHost::MasterClient, "Store/BuyItem");
	OnUnlockItem->BindObject<FUnlockItemResponseContainer>(200).AddUObject(this, &UMarketComponent::OnUnlockItemSuccessfully);
	OnUnlockItem->OnFailedResponse.BindUObject(this, &UMarketComponent::OnUnlockItemFailed);

	RequestSellItem = CreateRequest(FServerHost::MasterClient, "Store/SellItem");
	RequestSellItem->BindObject<FSellItemResponse>(200).AddUObject(this, &UMarketComponent::OnSellItem);
	RequestSellItem->OnFailedResponse.BindUObject(this, &UMarketComponent::OnSellItem_Failed);

	//	Invalid amount, if amount <= 0
	//RequestSellItem->Bind(400);

	//	item not found
	//RequestSellItem->Bind(404);

	//	Sell not allowed
	//RequestSellItem->Bind(409);
}

void UMarketComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UMarketComponent, SelectedItem);
}

void UMarketComponent::OnUnlockItemSuccessfully(const FUnlockItemResponseContainer& response)
{
	if (HasAuthority())
	{
		OnUnlockItemSuccessfullyHandler.Broadcast(response);
		OnItemUnlocked.Broadcast(UGameSingleton::Get()->FindItem(response.ModelId, response.CategoryTypeId));
		ClientNotifyUnlockItem(response);
	}	
}

void UMarketComponent::ClientNotifyUnlockItem_Implementation(const FUnlockItemResponseContainer& response)
{
	if (auto FoundItem = UGameSingleton::Get()->FindItem(response.ModelId, response.CategoryTypeId))
	{
		auto FormatedDescription = FText::Format(NSLOCTEXT("UMarketComponent", "Notify.UnlockItem.Desc", "You successfully purscahe \"{0}\" with amount {1}"), 
												 FRichHelpers::TextHelper_NotifyValue.SetValue(FoundItem->GetItemName()).ToText(), 
												 FRichHelpers::TextHelper_NotifyValue.SetValue(FText::AsNumber(response.AmountBought)).ToText()
												);

		FNotifyInfo NotifyInfo(FormatedDescription, NSLOCTEXT("UMarketComponent", "Notify.UnlockItem.Title", "Buyed item."));
		NotifyInfo.Image = FoundItem->GetImage();
		NotifyInfo.ShowTime = 15.0f;

		SNotifyList::Get()->AddNotify(NotifyInfo);

		OnItemUnlocked.Broadcast(FoundItem);
	}
}

void UMarketComponent::OnUnlockItemFailed(const FRequestExecuteError& error) const
{
}


void UMarketComponent::UnlockItem(const UItemBaseEntity* item, const uint32& amount, const FGuid& recipient)
{
	FUnlockItemRequestContainer request;
	request.ItemId = item->GetModelId();
	request.ItemType = item->GetCategory();
	request.Amount = amount;
	request.Target = recipient;

	UnlockItem(request);
}

void UMarketComponent::UnlockItem(const FUnlockItemRequestContainer& item)
{
	if (HasAuthority())
	{
		OnUnlockItem->SendRequestObject(item);
	}
	else
	{
		ServerUnlockItem(item);
	}
}

bool UMarketComponent::ServerUnlockItem_Validate(const FUnlockItemRequestContainer& item) { return true; }
void UMarketComponent::ServerUnlockItem_Implementation(const FUnlockItemRequestContainer& item)
{
	UnlockItem(item);
}

void UMarketComponent::OnSelectedItem(const UItemBaseEntity* InItem)
{
	OnServerSelectedItem(InItem->GetModelInfo());
}

bool UMarketComponent::OnServerSelectedItem_Validate(const FItemModelInfo& InItem) { return true; }
void UMarketComponent::OnServerSelectedItem_Implementation(const FItemModelInfo& InItem)
{
	SelectedItem = InItem;
}

void UMarketComponent::OnLoadFractionProgressComplete(const TArray<const UPlayerFractionEntity*>& progress)
{
	AConstructionPlayerState* ConstructionPlayerState = Cast<AConstructionPlayerState>(GetOwnerState());

	//SAssignNew(MarketPlaceWindow, SMarketPlaceWindow, progress)
	//.OnSelectItem_UObject(this, &UMarketComponent::OnSelectedItem)
	//.OnPurchaseItem_UObject(ConstructionPlayerState, &AConstructionPlayerState::ClientShowBuyRequest)
	//.OnSwitchPlayerFraction_UObject(ConstructionPlayerState, &AConstructionPlayerState::OnSwitchPlayerFraction);
	//
	//if (auto TargetViewport = GetGameViewportClient())
	//{
	//	TargetViewport->AddUsableViewportWidgetContent(MarketPlaceWindow.ToSharedRef());
	//}	
	//MarketPlaceWindow->ToggleWidget(true);
}

void UMarketComponent::ToggleMarketWindow_Implementation(const bool InToggle)
{
	if (MarketPlaceWindow.IsValid())
	{
		MarketPlaceWindow->ToggleWidget(InToggle);
	}
}

void UMarketComponent::CreateGameWidgets()
{
}

bool UMarketComponent::SendRequestSellItem_Validate(const FSellItemRequest& request) { return true; }
void UMarketComponent::SendRequestSellItem_Implementation(const FSellItemRequest& request)
{
	RequestSellItem->SendRequestObject(request);
}

void UMarketComponent::OnSellItem(const FSellItemResponse& response)
{
	OnSellItemEvent.Broadcast(response.ItemId);
	ClientNotifySellItem(response);
}

void UMarketComponent::OnSellItem_Failed(const FRequestExecuteError& error)
{
	
}

void UMarketComponent::ClientNotifySellItem_Implementation(const FSellItemResponse& response)
{
	if (OnGetInventoryItem.IsBound())
	{
		auto FoundItem = OnGetInventoryItem.Execute(response.ItemId);
		if (FoundItem && FoundItem->GetEntity())
		{
			auto FormatedDescription = FText::Format(NSLOCTEXT("UMarketComponent", "Notify.SellItem.Desc", "You successfully sold \"{0}\" with amount of {1}, and receive part of price equal {2}"),
				FRichHelpers::TextHelper_NotifyValue.SetValue(FoundItem->GetEntity()->GetItemName()).ToText(),
				FRichHelpers::TextHelper_NotifyValue.SetValue(FText::AsNumber(response.Selled)).ToText(),
				FRichHelpers::FResourceHelper().SetValue((FoundItem->GetEntity()->GetCost().Amount * response.Selled) / 2).SetType(FoundItem->GetEntity()->GetCostAsResource().Type).ToText()
			);

			FNotifyInfo NotifyInfo(FormatedDescription, NSLOCTEXT("UMarketComponent", "Notify.SellItem.Title", "Sell item."));
			NotifyInfo.Image = FoundItem->GetEntity()->GetImage();
			NotifyInfo.ShowTime = 15.0f;

			SNotifyList::Get()->AddNotify(NotifyInfo);

			OnSellItemEvent.Broadcast(response.ItemId);
		}		
	}
}