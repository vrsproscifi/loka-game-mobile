#pragma once


#include "Components/Shared/PlayerOnlineStatus.h"
#include "PlayerBasicInformation.generated.h"

USTRUCT(BlueprintType)
struct FPlayerBasicInformation
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly)					FLokaGuid									PlayerId;
	UPROPERTY(BlueprintReadOnly)					FString										Name;
	UPROPERTY(BlueprintReadOnly, meta = (Bitmask))	TEnumAsByte<PlayerFriendStatus::Type>		Status;
	UPROPERTY(BlueprintReadOnly)					TEnumAsByte<ActionPlayerRequestState::Type> State;
	UPROPERTY()										uint64										LastActivityDate;
	UPROPERTY()										uint64										Score;

	friend bool operator==(const FPlayerBasicInformation& Lhs, const FPlayerBasicInformation& Rhs)
	{
		return Lhs.PlayerId == Rhs.PlayerId && 
			Lhs.Name == Rhs.Name && 
			Lhs.Status == Rhs.Status && 
			Lhs.State == Rhs.State && 
			Lhs.LastActivityDate == Rhs.LastActivityDate && 
			Lhs.Score == Rhs.Score;
	}

	friend bool operator!=(const FPlayerBasicInformation& Lhs, const FPlayerBasicInformation& Rhs)
	{
		return !(Lhs == Rhs);
	}
};

USTRUCT()
struct FAddFriendRequestRespond
{
	GENERATED_USTRUCT_BODY()

	FAddFriendRequestRespond(){}
	FAddFriendRequestRespond(const FLokaGuid& id, const bool& accepted)
		: PlayerId(id), IsAccept(accepted)
	{
		
	}

	FString ToString() const
	{
		return FString::Printf(TEXT("FAddFriendRequestRespond(PlayerId = %s, PlayerName = %s, IsAccept = %s)"), *PlayerId.ToString(), *PlayerName, IsAccept ? TEXT("True") : TEXT("False"));
	}

	UPROPERTY()	FLokaGuid	PlayerId;
	UPROPERTY()	FString		PlayerName;
	UPROPERTY()	bool		IsAccept;
};