#include "LokaGame.h"
#include "IdentityComponent.h"
#include "ConstructionPlayerController.h"
#include "Models/PlayerEntityInfo.h"

//======================================
//				Slate
#include "SMessageBox.h"
#include "SInputWindow.h"
#include "SAuthForm.h"
//				Slate screen container
#include "UTGameViewportClient.h"

//======================================
//				Models
#include "RequestManager/RequestManager.h"
#include "IdentityComponent/CheckExistAccount.h"
#include "IdentityComponent/RequestClientData.h"
#include "IdentityComponent/RequestLootData.h"
#include "IdentityComponent/CreateGameAccount.h"
#include "IdentityComponent/SelectFraction.h"
#include "IdentityComponent/SwitchFraction.h"
#include "ConstructionPlayerState.h"
#include "Models/LobbyClientLootData.h"
#include "GameSingleton.h"
#include "SFractionSelector.h"
#include "Fraction/FractionEntity.h"
#include "StringTableRegistry.h"
#include "SMesBoxContentEarlyNotify.h"

//======================================
//				Steam support
#if STEAM_SUPPORT == 1
#include <OnlineIdentityInterface.h>
#include <OnlineSubsystem.h>
#endif

//======================================
UIdentityComponent::UIdentityComponent()
{
	RequestAuthorization = CreateRequest(FServerHost::MasterClient, "Identity/OnAuthentication", FGuid());
	RequestAuthorization->BindObject<FPlayerAuthorizationResponse>(200).AddUObject(this, &UIdentityComponent::OnAuthorization);
	RequestAuthorization->OnFailedResponse.BindUObject(this, &UIdentityComponent::OnAuthorizationFailed);

	RequestClientData = CreateRequest(FServerHost::MasterClient, "Identity/OnRequestClientData");
	RequestClientData->BindObject<FPlayerEntityInfo>(200).AddUObject(this, &UIdentityComponent::OnPlayerData);

	RequestClientDataUpdate = CreateRequest(FServerHost::MasterClient, "Identity/OnRequestClientData", 10, true, FTimerDelegate::CreateUObject(this, &UIdentityComponent::SendRequestClientDataUpdate));
	RequestClientDataUpdate->BindObject<FPlayerEntityInfo>(200).AddUObject(this, &UIdentityComponent::OnPlayerDataUpdate);
	
	RequestExist = CreateRequest(FServerHost::MasterClient, "Identity/IsExistGameAccount");
	RequestExist->Bind(200).AddUObject(this, &UIdentityComponent::OnExist);
	RequestExist->Bind(404).AddUObject(this, &UIdentityComponent::OnExistNotFound);
	RequestExist->Bind(401).AddUObject(this, &UIdentityComponent::OnExistNotAuthorized);
	RequestExist->Bind(900).AddUObject(this, &UIdentityComponent::OnExistNotAuthorized);
	RequestExist->Bind(901).AddUObject(this, &UIdentityComponent::OnExistNotAuthorized);
	RequestExist->Bind(902).AddUObject(this, &UIdentityComponent::OnExistNotAuthorized);
	RequestExist->Bind(903).AddUObject(this, &UIdentityComponent::OnExistNotAuthorized);
	RequestExist->Bind(904).AddUObject(this, &UIdentityComponent::OnExistNotAuthorized);
	RequestExist->Bind(905).AddUObject(this, &UIdentityComponent::OnExistNotAuthorized);

	RequestCreate = CreateRequest(FServerHost::MasterClient, "Identity/CreateGameAccount");
	RequestCreate->BindObject<bool>(200).AddUObject(this, &UIdentityComponent::OnCreate);
	RequestCreate->Bind(400).AddUObject(this, &UIdentityComponent::OnCreateIncorrect);

	RequestSelectFraction = CreateRequest(FServerHost::MasterClient, "Identity/OnSelectFraction");
	//RequestSelectFraction->Bind(200).AddUObject(this, &UIdentityComponent::OnSelectFractionSuccessfully);
	//RequestSelectFraction->OnFailedResponse.BindUObject(this, &UIdentityComponent::OnSelectFractionFailed);
		
	RequestTopPlayers = CreateRequest(FServerHost::MasterClient, "Session/GetPlayerRatingTop", 20, true, FTimerDelegate::CreateUObject(this, &UIdentityComponent::SendRequestTopPlayers));
	RequestTopPlayers->BindObject<FPlayerRatingTopView>(200).AddUObject(this, &UIdentityComponent::OnTopPlayers);

	bIsSimulation = false;
}

void UIdentityComponent::OnAuthorizeAsSimulation()
{
	UE_LOG(LogEntityPlayer, Display, TEXT("UIdentityComponent::OnAuthorizeAsSimulation[%s][%d]"), *GetIdentityToken().Get().ToString(), static_cast<int32>(GetNetMode()));

	bIsSimulation = true;
	OnRep_Simulation();
}

void UIdentityComponent::BeginPlay()
{
	Super::BeginPlay();
	OnRep_Simulation();
}

// Send requests/Отправка запросов

bool UIdentityComponent::SendRequestAuthorization_Validate(const FPlayerAuthorizationRequest& InRequest) { return true; }
void UIdentityComponent::SendRequestAuthorization_Implementation(const FPlayerAuthorizationRequest& InRequest)
{
	RequestAuthorization->SendRequestObject(InRequest);
}

bool UIdentityComponent::SendRequestChangeRegion_Validate() { return true; }
void UIdentityComponent::SendRequestChangeRegion_Implementation() {}

bool UIdentityComponent::SendRequestClientData_Validate() { return true; }
void UIdentityComponent::SendRequestClientData_Implementation()
{
	RequestClientData->GetRequest();
}

bool UIdentityComponent::SendRequestClientDataUpdate_Validate() { return true; }
void UIdentityComponent::SendRequestClientDataUpdate_Implementation()
{
	RequestClientDataUpdate->GetRequest();
}

bool UIdentityComponent::SendRequestStatus_Validate() { return true; }
void UIdentityComponent::SendRequestStatus_Implementation() {}

bool UIdentityComponent::SendRequestExist_Validate() { return true; }
void UIdentityComponent::SendRequestExist_Implementation()
{
	RequestExist->GetRequest();
}

bool UIdentityComponent::SendRequestServerStatus_Validate() { return true; }
void UIdentityComponent::SendRequestServerStatus_Implementation() {}

bool UIdentityComponent::SendRequestCreate_Validate(const FString& InPlayerName) { return true; }
void UIdentityComponent::SendRequestCreate_Implementation(const FString& InPlayerName)
{
	RequestCreate->SendRequestObject(InPlayerName);
}

bool UIdentityComponent::SendRequestChangeEmail_Validate() { return true; }
void UIdentityComponent::SendRequestChangeEmail_Implementation() {}

bool UIdentityComponent::SendRequestEmailStatus_Validate() { return true; }
void UIdentityComponent::SendRequestEmailStatus_Implementation() {}

bool UIdentityComponent::SendRequestEmailCode_Validate() { return true; }
void UIdentityComponent::SendRequestEmailCode_Implementation() {}

bool UIdentityComponent::SendRequestSelectFraction_Validate(const int32& InFraction) { return true; }
void UIdentityComponent::SendRequestSelectFraction_Implementation(const int32& InFraction)
{
	RequestSelectFraction->SendRequestObject(InFraction);
}

bool UIdentityComponent::SendRequestTopPlayers_Validate() { return true; }
void UIdentityComponent::SendRequestTopPlayers_Implementation()
{
	RequestTopPlayers->GetRequest();
}

// Receive answers/Получение ответов

void UIdentityComponent::OnAuthorization(const FPlayerAuthorizationResponse& InData)
{
	if (HasAuthority())
	{
		IsAuthorized = false;

		if (InData.Status == ELoginFromRemoteStatus::Successfully)
		{
			//---------------------------------------
			UnconfirmedPlayerName = InData.AccountName;

			//---------------------------------------
			if (InData.AccountId.IsValid() == false)
			{
				//  NSLOCTEXT("Authentication", "Authentication.OnAuthorization.EmptyToken", "Invalid token")
				ShowErrorOnAuthForm(ELoginFromRemoteStatus::IsNotAllowed);
			}
			else
			{
				auto state = GetOwnerState();
				if (state)
				{
					state->SetIdentityToken(InData.AccountId);
				}

				SendRequestExist();
			}
		}
		else
		{
			ShowErrorOnAuthForm(InData.Status);
		}
	}
}

void UIdentityComponent::OnAuthorizationFailed(const FRequestExecuteError& InErrorData)
{
	
}

void UIdentityComponent::OnExist()
{
	auto MyOwnerState = GetOwnerState();
	if (MyOwnerState)
	{
		MyOwnerState->SetIdentityToken(GetIdentityToken().Get());
	}

	ToggleAuthForm(false);

	OnAuthorizationEvent.Broadcast(GetIdentityToken().Get());	

	RequestClientData->GetRequest();
}

void UIdentityComponent::OnExistNotFound()
{
	OnClientExistNotFound();
}

void UIdentityComponent::OnExistNotAuthorized()
{
	ToggleAuthForm(true);
}

void UIdentityComponent::OnCreate(const bool& InResponse)
{
	if (InResponse)
	{
		SendRequestExist();
	}

	OnClientCreate(InResponse);
}

void UIdentityComponent::OnCreateIncorrect()
{
	OnClientCreateIncorrect();
}

void UIdentityComponent::OnPlayerData(const FPlayerEntityInfo& InData)
{
	UE_LOG(LogEntityPlayer, Display, TEXT("UIdentityComponent::OnRequestClientDataSuccessfully[%s][%d]"), *GetIdentityToken().Get().ToString(), static_cast<int32>(GetNetMode()));

	//======================================================
	SetPlayerInfo(InData);

	//======================================================
	if (IsAuthorized == false)
	{
		//	Устанавливаем EpicAccountId только на клиенте
		//	для возможности определить у кого произошел crash
#if !UE_SERVER
		FGenericPlatformMisc::SetEpicAccountId(InData.PlayerId.ToString());
#endif

		if (auto state = GetOwnerState())
		{
			IsAuthorized = true;
			state->SetPlayerName(InData.Login);

			//	Вызвываем событие OnAuthorizationComplete,
			//	Что бы выполнить загрузку друзей, отряда и т.д..
			state->OnAuthorizationCompleteDelegate.Broadcast();
		}
	}

	//======================================================
	OnRequestClientDataDelegate.Broadcast(InData);
	//SendRequestLootData();

	if (GetWorld())
	{
		RequestClientDataUpdate->StartTimer(&GetWorld()->GetTimerManager());
		RequestTopPlayers->StartTimer(&GetWorld()->GetTimerManager());
	}
}

void UIdentityComponent::OnPlayerDataUpdate(const FPlayerEntityInfo& InData)
{
	SetPlayerInfo(InData);
}

void UIdentityComponent::OnTopPlayers(const FPlayerRatingTopView& InData)
{
	RatingTopView = InData;
	OnRep_RatingTopView();
}

void UIdentityComponent::OnClientExistNotFound_Implementation()
{
	if (Slate_MessageBoxInput.IsValid() == false)
	{
		SAssignNew(Slate_MessageBoxInput, SInputWindow);
	}

	Slate_MessageBoxInput->SetContent(NSLOCTEXT("Authentication", "Authentication.CreateAccount", "Enter your name for game."));

	QueueMessageBegin("ClientAccountNotFound")
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Authentication", "Authentication.Title", "Authentication"));
		SMessageBox::Get()->SetContent(Slate_MessageBoxInput.ToSharedRef());
		SMessageBox::Get()->SetButtonsText(LOCTABLE("BaseStrings", "All.Continue"));
		SMessageBox::Get()->SetEnableClose(false);
		SMessageBox::Get()->SetIsOnceClickButtons(true);
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton) -> void {
			SendRequestCreate(Slate_MessageBoxInput->GetInputText().ToString());
		});
		SMessageBox::Get()->ToggleWidget(true);
	QueueMessageEnd
}

void UIdentityComponent::OnClientCreate_Implementation(const bool InResponse)
{
	if (InResponse)
	{
		if (SMessageBox::Get()->GetQueueId().IsEqual(FName(TEXT("ClientAccountNotFound"))))
		{
			SMessageBox::Get()->ToggleWidget(false);
		}
	}
	else
	{
		if (Slate_MessageBoxInput.IsValid())
		{
			Slate_MessageBoxInput->SetInputError(NSLOCTEXT("Authentication", "Authentication.CreateAccount.Busy", "The entered name is busy, try again)"));
		}
	}
}

void UIdentityComponent::OnClientCreateIncorrect_Implementation()
{
	if (Slate_MessageBoxInput.IsValid())
	{
		Slate_MessageBoxInput->SetInputError(NSLOCTEXT("Authentication", "Authentication.CreateAccount.Incorrect", "The entered name is incorrect, try again)"));
	}
}

void UIdentityComponent::OnRep_PlayerInfo()
{
	if (HasLocalClientComponent())
	{
		if (PlayerInfo.FractionTypeId == FractionTypeId::End)
		{
			// TODO: Временое решение, пока не будет готов магазин смысла не имеет к тому же что-то по фракции не грузится.
			SendRequestSelectFraction(static_cast<int32>(FractionTypeId::Keepers));

			//SAssignNew(Slate_FractionSelector, SFractionSelector).OnFractionSelected_Lambda([&](const UFractionEntity* InFraction) -> void {
			//	if (InFraction)
			//	{
			//		SendRequestSelectFraction(static_cast<int32>(InFraction->GetModelId()));
			//	}

			//	if (SMessageBox::Get()->GetQueueId().IsEqual(FName(TEXT("ClientFractionNotFound"))))
			//	{
			//		SMessageBox::Get()->ToggleWidget(false);
			//	}
			//});

			//Slate_FractionSelector->AddFraction(UGameSingleton::Get()->FindFraction(FractionTypeId::Keepers));
			//Slate_FractionSelector->AddFraction(UGameSingleton::Get()->FindFraction(FractionTypeId::RIFT));

			//QueueMessageBegin("ClientFractionNotFound", w = Slate_FractionSelector.ToSharedRef())
			//	SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Authentication", "Authentication.Title", "Authentication"));
			//	SMessageBox::Get()->SetContent(w);
			//	SMessageBox::Get()->SetButtonsText();
			//	SMessageBox::Get()->SetEnableClose(false);
			//	SMessageBox::Get()->ToggleWidget(true);
			//QueueMessageEnd
		}

		UGameSingleton::Get()->UpdateSavedPlayerEntityInfo(this);
	}
}

void UIdentityComponent::OnRep_RatingTopView()
{
	OnTopPlayersEvent.Broadcast(RatingTopView);
}

void UIdentityComponent::SetPlayerInfo(const FPlayerEntityInfo& info)
{
	UE_LOG(LogEntityPlayer, Display, TEXT("UIdentityComponent::SetPlayerInfo[%s][%d][%s]"), *GetIdentityToken().Get().ToString(), static_cast<int32>(GetNetMode()), *info.LeagueId.ToString());

	PlayerInfo = info;
	OnRep_PlayerInfo();
}

void UIdentityComponent::CreateGameWidgets()
{
	SAssignNew(Slate_AuthForm, SAuthForm)
		.OnAuthentication_UObject(this, &UIdentityComponent::SendRequestAuthorization)
		.OnQuit_Lambda([&]() { GetOwnerController()->ConsoleCommand("quit"); });

	if (auto TargetViewport = GetGameViewportClient())
	{
		TargetViewport->AddUsableViewportWidgetContent_AlwaysVisible(Slate_AuthForm.ToSharedRef(), 150);
	}			
}

void UIdentityComponent::ToggleAuthForm_Implementation(const bool InToggle)
{
	if (Slate_AuthForm.IsValid())
	{
		Slate_AuthForm->ToggleWidget(InToggle);
	}
}

void UIdentityComponent::ShowErrorOnAuthForm_Implementation(const ELoginFromRemoteStatus InCode)
{
	if (Slate_AuthForm.IsValid())
	{
		switch (InCode)
		{
		case ELoginFromRemoteStatus::LoginNotFound:
			Slate_AuthForm->OnErrorMessage(NSLOCTEXT("Authentication", "Authentication.OnAuthorization.LoginNotFound", "Login not found!")); break;

		case ELoginFromRemoteStatus::LoginWasExist:
			Slate_AuthForm->OnErrorMessage(NSLOCTEXT("Authentication", "Authentication.OnAuthorization.LoginWasExist", "Login was exist!")); break;

		case ELoginFromRemoteStatus::InvalidPassword:
			Slate_AuthForm->OnErrorMessage(NSLOCTEXT("Authentication", "Authentication.OnAuthorization.InvalidPassword", "Invalid password")); break;

		case ELoginFromRemoteStatus::BadRequest:
			Slate_AuthForm->OnErrorMessage(NSLOCTEXT("Authentication", "Authentication.OnAuthorization.BadRequest", "Bad request")); break;

		case ELoginFromRemoteStatus::IsNotAllowed:
			Slate_AuthForm->OnErrorMessage(NSLOCTEXT("Authentication", "Authentication.OnAuthorization.IsNotAllowed", "Authentication is not allowed")); break;

		case ELoginFromRemoteStatus::IsLockedOut:
			Slate_AuthForm->OnErrorMessage(NSLOCTEXT("Authentication", "Authentication.OnAuthorization.IsLockedOut", "Account is locked out")); break;

		case ELoginFromRemoteStatus::RequiresTwoFactor:
			Slate_AuthForm->OnErrorMessage(NSLOCTEXT("Authentication", "Authentication.OnAuthorization.RequiresTwoFactor", "RequiresTwoFactor, not impl")); break;

		case ELoginFromRemoteStatus::SessionExist:
			Slate_AuthForm->OnErrorMessage(NSLOCTEXT("Authentication", "Authentication.OnAuthorization.SessionExist", "Player was logged")); break;

		case ELoginFromRemoteStatus::GameNotPurchased:
		case ELoginFromRemoteStatus::GameNotAvailable:
		{
			QueueBegin(SMessageBox, TEXT("EarlyAccessDialog"))
				SMessageBox::Get()->SetHeaderText(NSLOCTEXT("EarlyNotify", "EarlyNotify.Title", "Early Access"));
				SMessageBox::Get()->SetContent(SNew(SMesBoxContentEarlyNotify));
				SMessageBox::Get()->SetAllowedSize(FBox2D(FVector2D(600, 400), FVector2D(1200, 600)));
				SMessageBox::Get()->SetButtonsText(InCode == ELoginFromRemoteStatus::GameNotPurchased ? NSLOCTEXT("EarlyNotify", "EarlyNotify.Buy", "Get Early Access") : FText::GetEmpty());
				SMessageBox::Get()->OnMessageBoxButton.AddLambda([](const EMessageBoxButton InButton)
				{
					FPlatformProcess::LaunchURL(TEXT("https://lokagame.com/promo"), nullptr, nullptr);
				});
				SMessageBox::Get()->SetEnableClose(true);
				SMessageBox::Get()->ToggleWidget(true);
			QueueEnd

			break;
		}
			
		default:
			Slate_AuthForm->OnErrorMessage(FText::Format(NSLOCTEXT("Authentication", "Authentication.OnAuthorization.Unknown", "Unknown: {0}"), static_cast<int32>(InCode))); break;
		}
	}
}

void UIdentityComponent::OnRep_Simulation()
{
	if (bIsSimulation)
	{
		ToggleAuthForm(false);
	}
}

void UIdentityComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UIdentityComponent, PlayerInfo);
	DOREPLIFETIME_CONDITION(UIdentityComponent, bIsSimulation, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(UIdentityComponent, RatingTopView, COND_OwnerOnly);
}
