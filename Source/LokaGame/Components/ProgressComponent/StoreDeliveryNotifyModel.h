#pragma once


#include "StoreDeliveryNotifyModel.generated.h"

USTRUCT()
struct FStoreDeliveryNotifyModel
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY() FLokaGuid SenderId;
	UPROPERTY() FString SernderName;


	UPROPERTY() uint64 CreatedDate;
			    
	UPROPERTY() uint8 ModelId;
	UPROPERTY() uint8 CategoryTypeId;
			    
	UPROPERTY() uint64 Amount;
};