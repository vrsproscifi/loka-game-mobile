#pragma once
#include "GameModeTypeId.h"
#include "MatchInformationPreviewModel.generated.h"

enum class FractionTypeId : uint8;

USTRUCT()
struct FMatchInformationPreviewModel
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY() FString EntityId;
	UPROPERTY() uint64 StartDate;
	UPROPERTY() uint64 FinishDate;

	UPROPERTY() uint32 GameModeId;
	UPROPERTY() uint32 GameMapId;


	UPROPERTY() uint64 Money;
	UPROPERTY() uint64 Donate;
	UPROPERTY() uint64 Bitcoin;

	UPROPERTY() FractionTypeId FractionId;
	UPROPERTY() uint64 Reputation;
	UPROPERTY() uint64 Experience;


	UPROPERTY() int64 Score;
	UPROPERTY() uint64 Kills;
	UPROPERTY() uint64 Deads;
	UPROPERTY() uint64 Assists;

	FORCEINLINE EGameMap GetGameMap() const
	{
		return EGameMap(GameMapId);
	}

	FORCEINLINE EGameMode GetGameMode() const
	{
		return EGameMode(GameModeId);
	}
};