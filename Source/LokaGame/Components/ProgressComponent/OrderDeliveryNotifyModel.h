#pragma once


#include "OrderDeliveryNotifyModel.generated.h"

USTRUCT()
struct FOrderDeliveryNotifyModel
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY() FLokaGuid OrderId;
	UPROPERTY() uint64 CreatedDate;
			    
	UPROPERTY() uint8 ModelId;
	UPROPERTY() uint8 CategoryTypeId;
			    
	UPROPERTY() uint64 Amount;
	UPROPERTY() uint64 LastAmount;

};