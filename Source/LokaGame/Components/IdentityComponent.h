﻿#pragma once
#include "MessageBoxCfg.h"

#include "Models/PlayerEntityInfo.h"
#include "BasicGameComponent.h"
#include "Models/LobbyClientPreSetData.h"
#include "IdentityComponent/Authorization.h"
#include "Components/RequestManager/FailedOperationResponse.h"
#include "IdentityController/Models/InvertoryItemWrapper.h"
#include "IdentityComponent.generated.h"


class UPlayerFractionEntity;
class FOperationRequest;



#define STEAM_SUPPORT 0

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnRequestClientDataSuccessfully, const FPlayerEntityInfo&, PlayerInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAuthorization, const FGuid&, AuthorizationResponse);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnTopPlayers, const FPlayerRatingTopView&);

UCLASS(Transient)
class UIdentityComponent : public UBasicGameComponent
{
	GENERATED_BODY()

	UPROPERTY() bool IsAuthorized = false;

public:

	UIdentityComponent();

	void OnAuthorizeAsSimulation();
	virtual void BeginPlay() override;

	// Send requests/Отправка запросов

	UFUNCTION(Reliable, Server, WithValidation) void SendRequestAuthorization(const FPlayerAuthorizationRequest& InRequest);
	UFUNCTION(Reliable, Server, WithValidation) void SendRequestChangeRegion();
	UFUNCTION(Reliable, Server, WithValidation) void SendRequestClientData();
	UFUNCTION(Reliable, Server, WithValidation) void SendRequestClientDataUpdate();
	UFUNCTION(Reliable, Server, WithValidation) void SendRequestStatus();
	UFUNCTION(Reliable, Server, WithValidation) void SendRequestExist();
	UFUNCTION(Reliable, Server, WithValidation) void SendRequestServerStatus();
	UFUNCTION(Reliable, Server, WithValidation) void SendRequestCreate(const FString& InPlayerName);
	UFUNCTION(Reliable, Server, WithValidation) void SendRequestChangeEmail();
	UFUNCTION(Reliable, Server, WithValidation) void SendRequestEmailStatus();
	UFUNCTION(Reliable, Server, WithValidation) void SendRequestEmailCode();
	UFUNCTION(Reliable, Server, WithValidation) void SendRequestSelectFraction(const int32& InFraction);
	UFUNCTION(Reliable, Server, WithValidation) void SendRequestTopPlayers();

	FOnTopPlayers OnTopPlayersEvent;

protected:

	// Requests/Запросы

	TSharedPtr<FOperationRequest>	RequestAuthorization;
	TSharedPtr<FOperationRequest>	RequestExist;
	TSharedPtr<FOperationRequest>	RequestCreate;
	TSharedPtr<FOperationRequest>	RequestClientData;
	TSharedPtr<FOperationRequest>	RequestClientDataUpdate;
	TSharedPtr<FOperationRequest>	RequestStatus;	
	TSharedPtr<FOperationRequest>	RequestServerStatus;
	TSharedPtr<FOperationRequest>	RequestChangeEmail;
	TSharedPtr<FOperationRequest>	RequestEmailStatus;
	TSharedPtr<FOperationRequest>	RequestEmailCode;
	TSharedPtr<FOperationRequest>	RequestSelectFraction;
	TSharedPtr<FOperationRequest>	RequestChangeRegion;
	TSharedPtr<FOperationRequest>	RequestTopPlayers;

	// Receive answers/Получение ответов

	UFUNCTION() void OnAuthorization(const FPlayerAuthorizationResponse& InData);
	UFUNCTION() void OnAuthorizationFailed(const FRequestExecuteError& InErrorData);

	UFUNCTION() void OnExist();
	UFUNCTION() void OnExistNotFound();
	UFUNCTION() void OnExistNotAuthorized();

	UFUNCTION() void OnCreate(const bool& InResponse);
	UFUNCTION() void OnCreateIncorrect();
	
	UFUNCTION() void OnPlayerData(const FPlayerEntityInfo& InData);
	UFUNCTION() void OnPlayerDataUpdate(const FPlayerEntityInfo& InData);

	UFUNCTION() void OnTopPlayers(const FPlayerRatingTopView& InData);

	UFUNCTION(Reliable, Client)	void OnClientExistNotFound();
	UFUNCTION(Reliable, Client)	void OnClientCreate(const bool InResponse);
	UFUNCTION(Reliable, Client) void OnClientCreateIncorrect();

	//	Временное имя игрока выданное при авторизации (полученное с сайта)
	FString UnconfirmedPlayerName;


	UFUNCTION() void OnRep_PlayerInfo();
	UPROPERTY(ReplicatedUsing = OnRep_PlayerInfo) FPlayerEntityInfo PlayerInfo;

	UFUNCTION() void OnRep_RatingTopView();
	UPROPERTY(ReplicatedUsing = OnRep_RatingTopView) FPlayerRatingTopView RatingTopView;

	FORCEINLINE_DEBUGGABLE void SetPlayerInfo(const FPlayerEntityInfo& info);

public:
	FORCEINLINE_DEBUGGABLE const FPlayerEntityInfo& GetPlayerInfo()
	{
		return PlayerInfo;
	}

	FORCEINLINE_DEBUGGABLE const FPlayerEntityInfo& GetPlayerInfo() const
	{
		return PlayerInfo;
	}

protected:
	void CreateGameWidgets() override;

	TSharedPtr<class SInputWindow>		Slate_MessageBoxInput;
	TSharedPtr<class SAuthForm>			Slate_AuthForm;
	TSharedPtr<class SFractionSelector> Slate_FractionSelector;

public:

	UPROPERTY(BlueprintAssignable, Category = Identity)
	FOnAuthorization OnAuthorizationEvent;
	
	// TODO: Неизвестные события, пояснить или удалить.

	FOnRequestClientDataSuccessfully	OnRequestClientDataDelegate;
	FOnFailedOperationResponse			OnRequestLootDataFailed;

	UFUNCTION(Reliable, Client) void ToggleAuthForm(const bool InToggle);
	UFUNCTION(Reliable, Client) void ShowErrorOnAuthForm(const ELoginFromRemoteStatus InCode);

protected:

	UPROPERTY(BlueprintReadOnly, Category = Identity)
	FGuid IdentityToken;

	UFUNCTION()
	void OnRep_Simulation();

	UPROPERTY(ReplicatedUsing = OnRep_Simulation)
	bool bIsSimulation;
};