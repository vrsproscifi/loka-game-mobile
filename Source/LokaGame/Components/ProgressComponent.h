#pragma once


#include "BasicGameComponent.h"
#include "ProgressComponent/OrderDeliveryNotifyModel.h"
#include "ProgressComponent/MatchInformationPreviewModel.h"
#include "ProgressComponent/StoreDeliveryNotifyModel.h"
#include "Models/PlayerEntityInfo.h"
#include "ProgressComponent.generated.h"


enum class FractionTypeId : unsigned char;
class UPlayerAchievementEntity;
class UPlayerFractionEntity;
class FOperationRequest;


DECLARE_DELEGATE_OneParam(FOnLoadFractionProgressComplete, const TArray<const UPlayerFractionEntity*>&)

UCLASS(Transient)
class UProgressComponent : public UBasicGameComponent
{
	GENERATED_BODY()

public:
	FOnLoadFractionProgressComplete OnLoadFractionProgressComplete;

public:
	UProgressComponent();

	UFUNCTION()
	void OnRequestClientDataSuccessfully(const FPlayerEntityInfo& response);
	//============================================


	void CreateGameWidgets() override;

private:
	//====================================================================
	//	Fraction
	UPROPERTY(Replicated) const UPlayerFractionEntity*					CurrentFraction;
	UPROPERTY(Replicated) TArray<const UPlayerFractionEntity*>			ConstFractionList;
	UPROPERTY(Replicated) TArray<UPlayerFractionEntity*>				FractionList;
	UPROPERTY() TMap<FGuid, UPlayerFractionEntity*>						FractionIdMap;
	UPROPERTY() TMap<FractionTypeId, UPlayerFractionEntity*>			FractionModelMap;
	void AddFraction(UPlayerFractionEntity* fraction);

public:
	const UPlayerFractionEntity* GetFraction(const FGuid& fraction) const;
	UPlayerFractionEntity* GetFraction(const FGuid& fraction);

	const UPlayerFractionEntity* GetFraction(const FractionTypeId& fraction) const;
	UPlayerFractionEntity* GetFraction(const FractionTypeId& fraction);

	void SetCurrentFraction(const UPlayerFractionEntity* fraction);

	FORCEINLINE const UPlayerFractionEntity* GetCurrentFraction() const { return CurrentFraction; }

	//=========================================================
	//					[ SwitchFraction ]
private:
	TSharedPtr<FOperationRequest> SwitchFraction;

public:
	UFUNCTION(Server, Reliable, WithValidation) 
	void OnSwitchFractionRequest(const UPlayerFractionEntity* fraction);
	bool OnSwitchFractionRequest_Validate(const UPlayerFractionEntity* fraction) { return true; }
	void OnSwitchFractionRequest_Implementation(const UPlayerFractionEntity* fraction);

	UFUNCTION(Client, Reliable) void OnSwitchFractionSuccessfully_Client(const FLokaGuid& fraction);
								void OnSwitchFractionSuccessfully_Server(const FLokaGuid& fraction);

	void OnSwitchFractionFailed(const FRequestExecuteError& error) const;

private:
	//====================================================================
	//	Achievement
	UPROPERTY(Replicated) TArray<const UPlayerAchievementEntity*>	ConstAchievement;
	UPROPERTY(Replicated) TArray<UPlayerAchievementEntity*>			Achievement;


	//====================================================================
private:
	TSharedPtr<FOperationRequest> GetMatchNotifyRequest;
	UFUNCTION(Client, Reliable) void OnGetMatchNotifySuccessfully(const TArray<FMatchInformationPreviewModel>& matches);

private:
	TSharedPtr<FOperationRequest> GetOrderNotifyRequest;
	UFUNCTION(Client, Reliable) void OnGetOrderNotifySuccessfully(const TArray<FOrderDeliveryNotifyModel>& orders);


private:
	TSharedPtr<FOperationRequest> GetStoreNotifyRequest;
	UFUNCTION(Client, Reliable) void OnGetStoreNotifySuccessfully(const TArray<FStoreDeliveryNotifyModel>& orders);
}; 