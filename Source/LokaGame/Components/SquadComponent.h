﻿#pragma once

#include "BasicGameComponent.h"
#include "SquadComponent/SqaudInformation.h"
#include "SquadComponent/SquadInviteInformation.h"
#include "SquadComponent.generated.h"


class FOperationRequest;

DECLARE_LOG_CATEGORY_EXTERN(LogSquadComponent, Log, All);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnSquadUpdate, const FSqaudInformation&);

UCLASS(Transient)
class USquadComponent : public UBasicGameComponent
{
	GENERATED_BODY()

public:

	USquadComponent();

	/*! Событие вызываемое при успешной авторизации*/
	void OnAuthorizationComplete() override;

	FOnSquadUpdate OnSquadUpdate;

protected:

	TSharedPtr<FOperationRequest> RequestSquadInformation;
	TSharedPtr<FOperationRequest> RequestStartSearch;
	TSharedPtr<FOperationRequest> RequestStopSearch;
	TSharedPtr<FOperationRequest> RequestInvites;
	TSharedPtr<FOperationRequest> RequestAdd;
	TSharedPtr<FOperationRequest> RequestRespondInvite;
	TSharedPtr<FOperationRequest> RequestRemove;

public:
	/*! Событие для смены статуса поиска боя
	* Если SqaudInformation.Status == ESearchSessionStatus::None, то отправляется запрос на поиск боя и блокируется кнопка
	* Если SqaudInformation.AllowCancelSearch(), то отправляется запрос на отмену боя
	* Иначе, метод ничего не делает
	*/
	UFUNCTION(Reliable, Server, WithValidation, BlueprintCallable, Category = Squad)
	void SendRequestToggleSearch(const FSessionMatchOptions& InOptions);
	bool SendRequestToggleSearch_Validate(const FSessionMatchOptions& InOptions) { return true; }

	UFUNCTION(Reliable, Server, WithValidation, BlueprintCallable, Category = Squad)
	void SendRequestSquadInformation();
	bool SendRequestSquadInformation_Validate() { return true; }

	UFUNCTION(Reliable, Server, WithValidation, BlueprintCallable, Category = Squad)
	void SendRequestInvites();
	bool SendRequestInvites_Validate() { return true; }

	UFUNCTION(Reliable, Server, WithValidation, BlueprintCallable, Category = Squad)
	void SendRequestAdd(const FGuid& InPlayerId, const FSessionMatchOptions& InOptions, const ESquadInviteDirection& InDirection);
	bool SendRequestAdd_Validate(const FGuid& InPlayerId, const FSessionMatchOptions& InOptions, const ESquadInviteDirection& InDirection) { return true; }

	UFUNCTION(Reliable, Server, WithValidation, BlueprintCallable, Category = Squad)
	void SendRequestRespondInvite(const FSquadInviteInformation& InInformation, const bool InAproved = false);
	bool SendRequestRespondInvite_Validate(const FSquadInviteInformation& InInformation, const bool InAproved) { return true; }

	UFUNCTION(Reliable, Server, WithValidation, BlueprintCallable, Category = Squad)
	void SendRequestRemove(const FLokaGuid& InMemberId);
	bool SendRequestRemove_Validate(const FLokaGuid& InMemberId) { return true; }

protected:

	UFUNCTION() void OnRep_SqaudInformation();

	/*! Предыдущее состояние отряда*/
	UPROPERTY(Replicated) FSqaudInformation LastSqaudInformation;

	/*! Текущее состояние отряда*/
	UPROPERTY(ReplicatedUsing = OnRep_SqaudInformation) FSqaudInformation SqaudInformation;

	UFUNCTION() void OnSquadInformation(const FSqaudInformation& InInformation);
	UFUNCTION() void OnStartSearch();
	UFUNCTION() void OnStopSearch();
	UFUNCTION() void OnInvites(const TArray<FSquadInviteInformation>& InInvites);
	UFUNCTION() void OnAdd(const FSquadInviteInformation& InInformation);
	UFUNCTION() void OnAddErrorCode(const int32 InCode);
	UFUNCTION() void OnAddErrorCodeWithString(const FString& InPlayerName, const int32 InCode);
	UFUNCTION() void OnRespondInvite(const FSquadInviteInformation& InInformation);
	UFUNCTION() void OnRespondInviteErrorCode(const int32 InCode);
	UFUNCTION() void OnRespondInviteErrorCodeWithString(const FString& InPlayerName, const int32 InCode);
	UFUNCTION() void OnRemove(const FString& InPlayerName);


	UFUNCTION(Reliable, Client) void OnRequestStartSearch_Failed(const FRequestExecuteError&error);
	UFUNCTION(Reliable, Client) void OnRequestStartSearch_ErrorOnDataBase();
	UFUNCTION(Reliable, Client) void OnRequestStartSearch_NotHaveEnabledProfiles();
	UFUNCTION(Reliable, Client) void OnRequestStartSearch_NotHavePrimaryWeapon();
	UFUNCTION(Reliable, Client) void OnRequestStartSearch_MembersNotReady();


	UFUNCTION(Reliable, Client) void OnClientMatchMakingProgress();

	UFUNCTION(Reliable, Client) void OnClientStartSearch();
	UFUNCTION(Reliable, Client) void OnClientStopSearch();
	UFUNCTION(Reliable, Client) void OnClientInvites(const TArray<FSquadInviteInformation>& InInvites);
	UFUNCTION(Reliable, Client) void OnClientRemove(const FString& InPlayerName);
	//==========================================================================================================================================================
	UFUNCTION(Reliable, Client) void OnClientAdd(const FSquadInviteInformation& InInformation);
	UFUNCTION(Reliable, Client) void OnClientAddErrorCode(const int32 InCode);
	UFUNCTION(Reliable, Client) void OnClientAddErrorCodeWithString(const FString& InPlayerName, const int32 InCode);
	//==========================================================================================================================================================
	UFUNCTION(Reliable, Client) void OnClientRespondInvite(const FSquadInviteInformation& InInformation);
	UFUNCTION(Reliable, Client) void OnClientRespondInviteErrorCode(const int32 InCode);
	UFUNCTION(Reliable, Client) void OnClientRespondInviteErrorCodeWithString(const FString& InPlayerName, const int32 InCode);
	
public:

	bool IsAllowTravel() const;
	bool IsAllowServerQueries() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Squad)
	FSqaudInformation GetSqaudInformation() const
	{
		return SqaudInformation;
	}
};