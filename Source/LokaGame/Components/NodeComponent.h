#pragma once

//==============================================
#include "BasicGameComponent.h"
#include "NodeComponent/NodeSessionMatch.h"
#include "NodeComponent/MatchHeartBeatView.h"
#include "NodeComponent.generated.h"

struct FConfirmMemberContainer;
//==============================================
class FOperationRequest;


//==============================================
DECLARE_MULTICAST_DELEGATE_OneParam(FOnGetMatchInformationSuccessfully, const FNodeSessionMatch&);

//==============================================
UCLASS(Transient)
class UNodeComponent : public UBasicGameComponent
{
	friend class AShooterGame_CapturePoint;
	friend class AShooterGame_FreeForAll;
	friend class AShooterGame_Duel;

	GENERATED_BODY()

protected:
	UPROPERTY(Replicated) FNodeSessionMatch MatchInformation;

public:
	UNodeComponent();
	TAttribute<FGuid> GetIdentityToken() const override;

	static bool IsAllowMasterServerQueries();

	FORCEINLINE_DEBUGGABLE const FNodeSessionMatch& GetMatchInformation() const
	{
		return MatchInformation;
	}

	FORCEINLINE_DEBUGGABLE const TArray<FNodeSessionMember>& GetMatchMebers() const
	{
		return MatchInformation.Members;
	}

	//======================================================================================
	//	GetMatchInformationRequest
private:
	void SendGetMatchInformationRequest() const;
	TSharedPtr<FOperationRequest> GetMatchInformationRequest;

	//	�����������, ����� ������� ������� �������� ���������� � �����
	void OnGetMatchInformation_Successfully(const FNodeSessionMatch& information);

	//	�����������, ����� �� ������� �������� ���������� � �����
	void OnGetMatchInformation_Failed(const FRequestExecuteError& error) const;

	//	�����������, ����� ����������� ������� ��������� ���������� � �����
	void OnGetMatchInformation_Complete(const FRequestExecuteError& error) const;

public:
	//	��������� ������ �� ��������� ���������� � �����
	void BeginPoolingMatchInformation();
	void OnSimulatedInformation(const FNodeSessionMatch& information)
	{
		OnGetMatchInformation_Successfully(information);
	}

	//======================================================================================
	//	StartMatchRequest
private :
	TSharedPtr<FOperationRequest> StartMatchRequest;
	void SendStartMatchRequest() const;

  void OnSendStartMatch_Successfully();
  void OnSendStartMatch_Failed(const FRequestExecuteError& error) const;
  void OnSendStartMatch_Complete(const FRequestExecuteError& error) const;


public:
	FOnGetMatchInformationSuccessfully OnGetMatchInformationSuccessfullyDelegate;
	void BeginPoolingStartMatchRequest();

	//======================================================================================
	//	CompleteMatchRequest
private:
	TSharedPtr<FOperationRequest> CompleteMatchRequest;

public:
	void SendCompleteMatchRequest(const struct FMatchEndResult& result) const;


	//======================================================================================
	//	GameNodeHeartBeatRequest
private:
	void SendGameNodeHeartBeatRequest() const;
	TSharedPtr<FOperationRequest> GameNodeHeartBeatRequest;

	//	�����������, ����� ������� ������� �������� ����������
	void OnSendGameNodeHeartBeatRequest_Successfully() const;

	//	�����������, ����� �� ������� �������� ���������� 
	void OnSendGameNodeHeartBeatRequest_Failed(const FRequestExecuteError& error) const;

	//	�����������, ����� ����������� ������� 
	void OnSendGameNodeHeartBeatRequest_Complete(const FRequestExecuteError& error) const;

	//======================================================================================
	//	GetAvalibleMembersRequest
public:
	void SendGetAvalibleMembersRequest(const FMatchHeartBeatView& view) const;
	TSharedPtr<FOperationRequest> GetAvalibleMembersRequest;


	//======================================================================================
	//	ConfirmJoinNewMembersRequest
public:
	void SendConfirmJoinNewMembersRequest(const TArray<FConfirmMemberContainer>& members) const;
	TSharedPtr<FOperationRequest> ConfirmJoinNewMembersRequest;

	//======================================================================================
	//	ConfirmJoinNewMembersRequest
public:
	void SendLeaveMemberRequest(const FGuid& member) const;
	TSharedPtr<FOperationRequest> LeaveMemberRequest;

};