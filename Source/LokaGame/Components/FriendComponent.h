﻿#pragma once
#include "BasicGameComponent.h"
#include "FriendComponent/PlayerBasicInformation.h"
#include "FriendComponent.generated.h"

class FOperationRequest;

DECLARE_LOG_CATEGORY_EXTERN(LogFriendComponent, Log, All);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnFriendsUpdate, const TArray<FPlayerBasicInformation>&);

UCLASS(Transient)
class UFriendComponent : public UBasicGameComponent
{
	GENERATED_BODY()

public:

	UFriendComponent();

	FOnFriendsUpdate OnListUpdate;
	FOnFriendsUpdate OnSearchListUpdate;

protected:

	/*! Событие вызываемое при успешной авторизации*/
	void OnAuthorizationComplete() override;

protected:

	// Requests/Запросы

	TSharedPtr<FOperationRequest> RequestList;
	TSharedPtr<FOperationRequest> RequestSearch;
	TSharedPtr<FOperationRequest> RequestAdd;
	TSharedPtr<FOperationRequest> RequestRemove;
	TSharedPtr<FOperationRequest> RequestRespond;	

public:

	// Send requests/Отправка запросов

	UFUNCTION(Reliable, Server, WithValidation) 
	void SendRequestList();
	bool SendRequestList_Validate() { return true; }

	UFUNCTION(Reliable, Server, WithValidation)
	void SendRequestSearch(const FString& InPlayerName);
	bool SendRequestSearch_Validate(const FString& InPlayerName) { return true; }

	UFUNCTION(Reliable, Server, WithValidation)
	void SendRequestAdd(const FGuid& InPlayerId);
	bool SendRequestAdd_Validate(const FGuid& InPlayerId) { return true; }

	UFUNCTION(Reliable, Server, WithValidation)
	void SendRequestRemove(const FGuid& InPlayerId);
	bool SendRequestRemove_Validate(const FGuid& InPlayerId) { return true; }

	UFUNCTION(Reliable, Server, WithValidation)
	void SendRequestRespond(const FAddFriendRequestRespond& InResponse);
	bool SendRequestRespond_Validate(const FAddFriendRequestRespond& InResponse) { return true; }

protected:

	// Receive answers/Получение ответов

	UFUNCTION()	void OnList(const TArray<FPlayerBasicInformation>& InList);
	UFUNCTION()	void OnSearch(const TArray<FPlayerBasicInformation>& InList);
	UFUNCTION()	void OnAdd(const FString& InPlayerName);
	UFUNCTION()	void OnRemove(const FString& InPlayerName);
	UFUNCTION()	void OnRespond(const FAddFriendRequestRespond& InResponse);

	// Client receive answers/Получение ответов для клиента

	UFUNCTION(Reliable, Client)	void OnClientList(const TArray<FPlayerBasicInformation>& InList);
	UFUNCTION(Reliable, Client)	void OnClientSearch(const TArray<FPlayerBasicInformation>& InList);
	UFUNCTION(Reliable, Client)	void OnClientAdd(const FString& InPlayerName);
	UFUNCTION(Reliable, Client)	void OnClientRemove(const FString& InPlayerName);
	UFUNCTION(Reliable, Client)	void OnClientRespond(const FAddFriendRequestRespond& InResponse);

	// Friends & Search Lists Replicated/Списки друзей и поиска с репликацией

	UFUNCTION() void OnRep_FriendsList();
	UFUNCTION() void OnRep_LastSearchResult();

	UPROPERTY(ReplicatedUsing = OnRep_FriendsList)
	TArray<FPlayerBasicInformation> FriendsList;

	UPROPERTY(ReplicatedUsing = OnRep_LastSearchResult)
	TArray<FPlayerBasicInformation> LastSearchResult;

public:

	// Helper & Get Functions/Вспомогательные и получаемые функции

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Contacts)
	FORCEINLINE TArray<FPlayerBasicInformation> GetFriendsList() const { return FriendsList; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Contacts)
	FORCEINLINE TArray<FPlayerBasicInformation> GetLastSearchResult() const { return LastSearchResult; }
};