#pragma once

#include "LokaGuid.h"
#include "RequestManager/FailedOperationResponse.h"
#include "BasicGameComponent.generated.h"

struct FServerHost;
struct FNotifyInfo;
enum class ERequestErrorCode : uint8;

class ILokaNetUserInterface;
class FOperationRequest;
class ABasePlayerController;
class ABasePlayerState;
class UUTGameViewportClient;
class AOnlineGameMode;

typedef const TAttribute<FGuid>& TokenAttribRef;
typedef TAttribute<FGuid> TokenAttrib;

UCLASS(Transient)
class UBasicGameComponent : public USceneComponent
{
	GENERATED_BODY()

public:
	UBasicGameComponent();

	static void AddNotify(const FNotifyInfo& info);

	virtual TAttribute<FGuid> GetIdentityToken() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Runtime)
	bool HasAuthority() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Runtime)
	bool HasLocalClientComponent() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Identity)
	TScriptInterface<ILokaNetUserInterface> GetUserOwner() const;

	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

private:
	FORCENOINLINE void OnAuthorizationComplete_Proxy()
	{
		if (HasAuthority())
		{
			OnAuthorizationComplete();
		}
	}

protected:
	virtual void OnAuthorizationComplete();


	UWorld* GetValidWorld() const;
	UWorld* GetValidWorld();

	FTimerManager* GetTimerManager() const;
	FTimerManager* GetTimerManager();

	void StartTimer(TSharedPtr<FOperationRequest>& operation, const float& delay = 0) const;
	void StartTimer(TSharedPtr<FOperationRequest>& operation, const float& delay = 0);

	void StopTimer(TSharedPtr<FOperationRequest>& operation) const;
	void StopTimer(TSharedPtr<FOperationRequest>& operation);

	ABasePlayerController* GetOwnerController() const;
	ABasePlayerState* GetOwnerState() const;

  AGameMode* GetBasicGameMode() const;
  AOnlineGameMode* GetOnlineGameMode() const;

  template<typename TGameMode = AOnlineGameMode>
  TGameMode* GetGameMode() const
  {
    return Cast<TGameMode>(GetBasicGameMode());
  }

	virtual void PostInitProperties() override;
	virtual void CreateGameWidgets() {}

protected:
	TArray<TSharedPtr<FOperationRequest>> OperationRequests;
	TSharedPtr<FOperationRequest> CreateRequest(const FServerHost& server_host, const FString& operation_name, TokenAttrib token, const float& rate = 0, const bool& loop = false, FTimerDelegate timer = FTimerDelegate());
	TSharedPtr<FOperationRequest> CreateRequest(const FServerHost& server_host, const FString& operation_name, const float& rate, const bool& loop , FTimerDelegate timer, TokenAttrib token, const uint64& timerLimit, const bool& timerValidate);
  
	TSharedPtr<FOperationRequest> CreateRequest(const FServerHost& server_host, const FString& operation_name, const float& rate = 0, const bool& loop = false, FTimerDelegate timer = FTimerDelegate(), const uint64& timerLimit = 5, const bool& timerValidate = false);

private:
	TSharedPtr<FOperationRequest> AddRequest(TSharedPtr<FOperationRequest> request);
};