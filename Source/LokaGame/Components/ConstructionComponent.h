#pragma once

#include "BuildingComponent.h"
#include "ServiceController/BuildingController/Models/WorldBuildingItemView.h" // Temp
#include "ConstructionComponent.generated.h"


UCLASS(Transient)
class UConstructionComponent : public UBuildingComponent
{
	GENERATED_BODY()
public:
	UConstructionComponent();	

public:

	UFUNCTION(Server, Reliable, WithValidation)			void OnBuildingPlaceSuccess(const FWorldBuildingItemView& InData);
	UFUNCTION(Server, Reliable, WithValidation)			void RequestBuildingPlace(ABuildPlacedComponent* InBuilding);
	UFUNCTION(Server, Reliable, WithValidation)			void RequestBuildingRemove(ABuildPlacedComponent* InBuilding);
	UFUNCTION(Server, Reliable, WithValidation)			void SendRequestHarvest(ABuildPlacedComponent* InBuilding);

private:
	TSharedPtr<FOperationRequest>						RequestHarvest;
	TSharedPtr<FOperationRequest>						Repair;
	TSharedPtr<FOperationRequest>						Place;
	TSharedPtr<FOperationRequest>						Remove;

	void OnHarvest(const FHarvestResponse& response);
	UFUNCTION(Client, Reliable) void OnHarvest_NotEnouthSpace(const FGuid& buildingId);
	UFUNCTION(Client, Reliable) void OnHarvest_Failed(const FRequestExecuteError& error);

	
	
	UFUNCTION(Client, Reliable)							void ClientPlaceObjectOnError(const FRequestExecuteError& error);
	UFUNCTION(Client, Reliable)							void ClientPlaceObjectOnNotFoundInstance();
	UFUNCTION(Client, Reliable)							void ClientPlaceObjectOnNotEnouthItem();
	UFUNCTION(Client, Reliable)							void ClientPlaceObjectOnFailed();	
	UFUNCTION(Client, Reliable)							void ClientRemoveObjectOnFailed();
	
};