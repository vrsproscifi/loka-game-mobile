// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "TutorialComponent.h"
#include "RequestManager/RequestManager.h"
#include "GameSingleton.h"
#include "TutorialBase.h"
#include "TutorialBuilding.h"
#include "Item/ItemBaseEntity.h"
#include "ConstructionPlayerState.h"
#include "MarketComponent.h"
#include "ConstructionCharacter.h"
#include "BuildSystem/BuildHelperComponent.h"

UTutorialComponent::UTutorialComponent()
	: Super()
{

	//	, OnNotFoundStep(400, this)
	//	, OnNotFoundReward(409, this)
	//	, OnResyncSteps(403, this)

	RequestTutorialStep = CreateRequest(FServerHost::MasterClient, "Identity/OnConfirmTutorialStep");
	RequestTutorialStep->BindObject<FConfirmTutorialStepResponse>(200).AddUObject(this, &UTutorialComponent::OnTutorialStep);


	//	, OnNotFoundInstance(400, this)
	//	, OnNotFoundSteps(403, this)
	//	, OnStepInProgress(502, this)

	RequestTutorialStart = CreateRequest(FServerHost::MasterClient, "Identity/OnStartTutorial");
	RequestTutorialStart->BindObject<FStartTutorialView>(200).AddUObject(this, &UTutorialComponent::OnTutorialStart);
	RequestTutorialStart->BindObject<FStartTutorialView>(502).AddUObject(this, &UTutorialComponent::OnTutorialStart);
}

void UTutorialComponent::OnAuthorizationComplete()
{
	Super::OnAuthorizationComplete();

	auto MyOwner = Cast<AConstructionPlayerState>(GetOwnerState());
	if (MyOwner && HasLocalClientComponent())
	{
		MyOwner->GetMarketComponent()->OnItemUnlocked.AddUObject(this, &UTutorialComponent::OnEvent_BuyItem);
	}
}

void UTutorialComponent::OnEvent_PlaceBuilding(ABuildPlacedComponent* InBuilding)
{
	if (auto TargetTutorial = Cast<UTutorialBuilding>(CurrentTutorial))
	{
		TargetTutorial->OnBuildingPlaced(InBuilding);
	}
}

void UTutorialComponent::OnEvent_RemoveBuilding(ABuildPlacedComponent* InBuilding)
{
	if (auto TargetTutorial = Cast<UTutorialBuilding>(CurrentTutorial))
	{
		TargetTutorial->OnBuildingPreRemove(InBuilding);
	}
}

void UTutorialComponent::OnEvent_SelectBuilding(ABuildPlacedComponent* InBuilding)
{
	if (auto TargetTutorial = Cast<UTutorialBuilding>(CurrentTutorial))
	{
		TargetTutorial->OnBuildingSelected(InBuilding);
	}
}

void UTutorialComponent::OnEvent_BuyItem(UItemBaseEntity* InItem)
{
	if (InItem && CurrentTutorial && CurrentTutorial->Stages.IsValidIndex(CurrentTutorial->GetCurrentStage()))
	{
		auto cStage = CurrentTutorial->Stages[CurrentTutorial->GetCurrentStage()];
		if (cStage.TargetAction == ETutorialAction::BuyItem && cStage.CompleteSpeciferPrimary == InItem->GetCategory() && (cStage.CompleteSpeciferSecondary == InItem->GetModelId() || cStage.CompleteSpeciferSecondary == INDEX_NONE))
		{
			CurrentTutorial->EndCurrentStage();
		}
	}
}

void UTutorialComponent::OnComponentDestroyed(bool bDestroyingHierarchy)
{
	Super::OnComponentDestroyed(bDestroyingHierarchy);

	if (CurrentTutorial) CurrentTutorial->ForceHide();
}

bool UTutorialComponent::SendRequestTutorialStep_Validate(const FStartTutorialView& InData) { return true; }
void UTutorialComponent::SendRequestTutorialStep_Implementation(const FStartTutorialView& InData)
{
	RequestTutorialStep->SendRequestObject(InData);
}

bool UTutorialComponent::SendRequestTutorialStart_Validate(const int32 InTutorialIndex) { return true; }
void UTutorialComponent::SendRequestTutorialStart_Implementation(const int32 InTutorialIndex)
{
	RequestTutorialStart->SendRequestObject(InTutorialIndex);
}

void UTutorialComponent::OnTutorialStep(const FConfirmTutorialStepResponse& InResponse)
{
	CurrentData = InResponse;
	OnRep_TutorialState();
}

void UTutorialComponent::OnTutorialStart(const FStartTutorialView& InResponse)
{
	CurrentStartData = InResponse;
	OnRep_TutorialStart();
}

void UTutorialComponent::OnRep_TutorialState()
{
	CurrentTutorial = UGameSingleton::Get()->GetTutorial(CurrentData.TutorialId);
	if (CurrentTutorial && HasLocalClientComponent())
	{
		CurrentTutorial->TutorialComponent = this;
		CurrentTutorial->OnConfirmCurrentStage(CurrentData);
	}
}

void UTutorialComponent::OnRep_TutorialStart()
{
	CurrentTutorial = UGameSingleton::Get()->GetTutorial(CurrentStartData.TutorialId);
	if (CurrentTutorial && HasLocalClientComponent())
	{
		CurrentTutorial->TutorialComponent = this;
		if (CurrentTutorial->StartTutorial(CurrentStartData.StepId) == false && CurrentStartData.TutorialComplete == false)
		{
			CurrentTutorial->ForceStage(CurrentStartData.StepId);
		}
	}
}

void UTutorialComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(UTutorialComponent, CurrentData, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(UTutorialComponent, CurrentStartData, COND_OwnerOnly);
}
