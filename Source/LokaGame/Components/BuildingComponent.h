#pragma once
#include "BasicGameComponent.h"
#include "NodeComponent/MatchBuildingInformation.h"
#include "BuildingComponent.generated.h"

class SBuildSystem_ToolBar;
class SBuildSystem_Inventory;
class FOperationRequest;
struct FWorldBuildingItemView;
class ABuildPlacedComponent;
class ConstructionGameState;
struct FPlayerWorldModel;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnBuildingsSignature, const TArray<ABuildPlacedComponent*>, InBuildings);

USTRUCT(Blueprintable)
struct FConstructionWorldData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly)	FString PrevName;
	UPROPERTY(BlueprintReadOnly)	FString CurrentName;
	UPROPERTY(BlueprintReadOnly)	FString NextName;

	UPROPERTY(BlueprintReadOnly)	FGuid CurrentId;
	UPROPERTY(BlueprintReadOnly)	FGuid PrevId;
	UPROPERTY(BlueprintReadOnly)	FGuid NextId;
};


UCLASS(Abstract, Transient)
class UBuildingComponent : public UBasicGameComponent
{
	GENERATED_BODY()
public:
	UBuildingComponent();
	void SendGetBuildingList(const FGuid& InPlayerId) const;

	TAttribute<FGuid> GetIdentityToken() const override;

	UPROPERTY(BlueprintAssignable, Category = Buildings)
	FOnBuildingsSignature OnBuildingsLoaded;

	UPROPERTY(BlueprintAssignable, Category = Buildings)
	FOnBuildingsSignature OnDamagedBuildingsFound;

	UFUNCTION(Reliable, Server, WithValidation, BlueprintCallable, Category = Buildings)
	void SendRequestUpdateProperty(const FUpdateBuildingProperty& InData);

	UFUNCTION(BlueprintCallable, Category = Buildings)
	void UpdateBuildingsOwner(AActor* InNewOwner);

	void SendGetBuildingPropertyRequest();
	void OnGetBuildingProperty_Successfully(const TArray<FBuildingPropertyContainer>& response);

protected:

	TSharedPtr<FOperationRequest>	GetBuildingProperty;
	TSharedPtr<FOperationRequest>	GetBuildingList;
	TSharedPtr<FOperationRequest>	RequestUpdateProperty;

	void OnUpdateProperty(const FUpdateBuildingProperty& InData);
	void OnUpdatePropertyFailed(const FRequestExecuteError& InError);

	virtual void OnGetBuildingListSuccessfully(const FPlayerWorldModel& response);
	virtual void OnGetBuildingListFailed(const FRequestExecuteError& error) const;
	
	static ABuildPlacedComponent* GetPlacedObject(const TArray<ABuildPlacedComponent*>& objects, const uint32& model);
	void GetPreplasedWorldObjects(TArray<ABuildPlacedComponent*>& objects) const;

	UPROPERTY()		TMap<FGuid, ABuildPlacedComponent*> WorldBuildings;
	UPROPERTY()		TMap<FGuid, ABuildPlacedComponent*> StorageBuildings;
	UPROPERTY()		TMap<FGuid, ABuildPlacedComponent*> DamagedBuildings;

public:
	void SetBuildingOwnerComponent(AActor* owner)
	{
		BuildingOwnerComponent = owner;
	}

	AActor* GetBuildingOwnerComponent() const
	{
		return BuildingOwnerComponent;
	}

	AActor* GetBuildingOwnerComponent()
	{
		return BuildingOwnerComponent;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Owning)
	bool IsPlayerOwnedWorld() const;

	FORCEINLINE const FConstructionWorldData* GetConstructionWorldData() const { return &ConstructionWorldData; }

protected:

	UPROPERTY(Replicated)	AActor*					BuildingOwnerComponent;
	UPROPERTY(Replicated)	FConstructionWorldData	ConstructionWorldData;
	UPROPERTY()				FPlayerWorldModel		LastBuildingsData;
};