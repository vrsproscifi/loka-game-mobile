#pragma once
#include "Utilities/SUsableCompoundWidget.h"
#include "SquadComponent/SqaudInformation.h"

struct FSqaudInformation;
struct FSessionMatchOptions;
typedef TPair<FText, TAttribute<FText>> FTextKeyValue;
typedef TPairInitializer<FText, TAttribute<FText>> FITextKeyValue;

DECLARE_DELEGATE(FOnSwitchSearchQueueState)

class SMatchMakingInformationWidget
	: public SUsableCompoundWidget
{
	TAttribute<FSqaudInformation> SqaudInformation;
	TArray<TSharedPtr<FTextKeyValue>> SessionOptions;

public:
	SLATE_BEGIN_ARGS(SMatchMakingInformationWidget)
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_ATTRIBUTE(FSqaudInformation, SqaudInformation)
	SLATE_EVENT(FOnSwitchSearchQueueState, OnSwitchSearchQueueState)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	const FSqaudInformation& GetSqaudInformation() const;
	const FSessionMatchOptions& GetMatchOptions() const;

	
	EVisibility GetDetailsVisibility() const;
	bool IsAllowDisplayDetails() const;

	FText GetGameModeName() const;
	FText GetGameMapName() const;
	FText GetButtonText() const;

	static FText GetKeyValue(const FText& key, const FText& value);

};
