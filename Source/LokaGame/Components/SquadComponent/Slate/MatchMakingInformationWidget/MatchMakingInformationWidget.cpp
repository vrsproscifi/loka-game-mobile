#include "LokaGame.h"
#include "MatchMakingInformationWidget.h"
#include "GameModeTypeId.h"


class SSearchSessionRow : public SMultiColumnTableRow<TSharedPtr<FTextKeyValue>>
{
	typedef SMultiColumnTableRow<TSharedPtr<FTextKeyValue>> SSuper;

public:
	SLATE_BEGIN_ARGS(SSearchSessionRow)
	{}
	SLATE_END_ARGS()

	BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
		void Construct(const FArguments& InArgs, const TSharedRef<STableViewBase>& OwnerTable, TSharedPtr<FTextKeyValue> Item)
	{
		Instance = Item;

		SSuper::Construct(SSuper::FArguments().Padding(FMargin(0, 2)), OwnerTable);
	}
	END_SLATE_FUNCTION_BUILD_OPTIMIZATION

	virtual TSharedRef<SWidget> GenerateWidgetForColumn(const FName& InColumnName) override
	{
		if (InColumnName == TEXT("Name"))
		{
			return SNew(STextBlock).Text(Instance->Key).Margin(FMargin(5, 0, 0, 0));
		}
		else if (InColumnName == TEXT("Value"))
		{
			return SNew(STextBlock).Text(Instance->Value).Margin(FMargin(5, 0, 0, 0));
		}
		return SNew(STextBlock);
	}

protected:

	TSharedPtr<FTextKeyValue> Instance;
};

void SMatchMakingInformationWidget::Construct(const FArguments& InArgs)
{
	SqaudInformation = InArgs._SqaudInformation;
	SessionOptions = {
		MakeShareable(new FTextKeyValue(FITextKeyValue(NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SessionOption.GameMode", "Game mode"), TAttribute<FText>::Create(TAttribute<FText>::FGetter::CreateRaw(this, &SMatchMakingInformationWidget::GetGameModeName))))),
		MakeShareable(new FTextKeyValue(FITextKeyValue(NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SessionOption.GameMap", "Game map"), TAttribute<FText>::Create(TAttribute<FText>::FGetter::CreateRaw(this, &SMatchMakingInformationWidget::GetGameMapName))))),
	};

	
	ChildSlot
	.HAlign(HAlign_Center)
	.VAlign(VAlign_Top)
	.Padding(0, 10, 0, 0)
	[
		SNew(SBox)
		.WidthOverride(300)
		[
			SNew(SBorder)
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot()
				.AutoHeight()
				[
					SNew(SButton)
					.Text(this, &SMatchMakingInformationWidget::GetButtonText)
					.OnClicked_Lambda([this, d = InArgs._OnSwitchSearchQueueState]
					{
						d.ExecuteIfBound();
						return FReply::Handled();
					})
				]
				+ SVerticalBox::Slot()
				.AutoHeight()
				[
					SNew(SBox)
					.Visibility(this, &SMatchMakingInformationWidget::GetDetailsVisibility)
					[
						SNew(SListView<TSharedPtr<FTextKeyValue>>)
						.ListItemsSource(&SessionOptions)
						.SelectionMode(ESelectionMode::None)
						.HeaderRow(SNew(SHeaderRow)
						+ SHeaderRow::Column("Name").FillWidth(.2f).HeaderContentPadding(FMargin(0, 4))[SNew(STextBlock).Margin(FMargin(5, 0, 0, 0))]
						+ SHeaderRow::Column("Value").FillWidth(1.f).HeaderContentPadding(FMargin(0, 4))[SNew(STextBlock)])
						.OnGenerateRow_Lambda([&](TSharedPtr<FTextKeyValue> Item, const TSharedRef<STableViewBase>& OwnerTable) {
							return SNew(SSearchSessionRow, OwnerTable, Item);
						})
					]
				]
			]
		]
	];

}

const FSqaudInformation& SMatchMakingInformationWidget::GetSqaudInformation() const
{
	return SqaudInformation.Get();
}

const FSessionMatchOptions& SMatchMakingInformationWidget::GetMatchOptions() const
{
	return GetSqaudInformation().Options;
}

EVisibility SMatchMakingInformationWidget::GetDetailsVisibility() const
{
	if (IsAllowDisplayDetails())
	{
		return EVisibility::Visible;
	}
	return EVisibility::Collapsed;
}

bool SMatchMakingInformationWidget::IsAllowDisplayDetails() const
{
	return GetSqaudInformation().Status != ESearchSessionStatus::None;
}

FText SMatchMakingInformationWidget::GetGameModeName() const
{
	return GetMatchOptions().GetGameMode().GetDisplayName();
}

FText SMatchMakingInformationWidget::GetGameMapName() const
{
	return GetMatchOptions().GetGameMap().GetDisplayName();
}

FText SMatchMakingInformationWidget::GetButtonText() const
{
	const auto status = GetSqaudInformation().Status;
	if (status == ESearchSessionStatus::None)
	{
		if (GetSqaudInformation().IsInSquad() && GetSqaudInformation().IsLeader == false)
		{
			if (GetSqaudInformation().IsReady)
			{
				return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SquadNotReady", "Not ready");
			}
			return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SquadReady", "Ready");
		}
		return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SessionSearchBegin", "Into Battle");
	}
	
	if (status == ESearchSessionStatus::MatchSearch)
	{
		return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SessionSearchCancel", "Cancel");
	}

	//============================================================================================
	//	Client states
	if (status == ESearchSessionStatus::SearchStarting)
	{
		return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SessionSearchStarting", "Prepare to search");
	}

	if (status == ESearchSessionStatus::SearchStopping)
	{
		return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SessionSearchStopping", "Search canceling");
	}

	//============================================================================================
	//	Complete
	if (status == ESearchSessionStatus::FoundContinue)
	{
		return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SessionSearchComplete", "Complete");
	}
	
	if (status == ESearchSessionStatus::FoundPrepare)
	{
		return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SessionSearchFoundPrepare", "Prepare to join");
	}

	if (status == ESearchSessionStatus::FoundError)
	{
		return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SessionSearchFoundError", "Join error");
	}
	
	if (status == ESearchSessionStatus::FoundJoin)
	{
		return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SessionSearchFoundJoin", "Entering the game");
	}

	if (status == ESearchSessionStatus::NotEnouthMoney)
	{
		return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.NotEnouthMoney", "Not enouth money");
	}

	return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SessionSearchWait", "Waiting");
}

FText SMatchMakingInformationWidget::GetKeyValue(const FText& key, const FText& value)
{
	return FText::Format(FText::FromString(TEXT("{0} : {1}")), key, value);
}
