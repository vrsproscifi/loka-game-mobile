#include "LokaGame.h"
#include "ChatComponent.h"
#include "RequestManager/RequestManager.h"


UChatComponent::UChatComponent()
{
	RequestGetListOfChanels = CreateRequest(FServerHost::MasterClient, "Chat/GetListOfChanels", 10.0f, true, FTimerDelegate::CreateUObject(this, &UChatComponent::SendRequestGetListOfChanels));
	RequestGetListOfChanels->BindArray<TArray<FChatChanelModel>>(200).AddUObject(this, &UChatComponent::OnChannels);

	RequestGetListOfMembers = CreateRequest(FServerHost::MasterClient, "Chat/GetListOfMembers", 8.0f, true, FTimerDelegate::CreateUObject(this, &UChatComponent::SendRequestGetListOfMembers));
	RequestGetListOfMembers->BindArray<TArray<FChatMemberModel>>(200).AddUObject(this, &UChatComponent::OnMembers);

	RequestGetListOfMessages = CreateRequest(FServerHost::MasterClient, "Chat/GetListOfMessages", 2.0f, true, FTimerDelegate::CreateUObject(this, &UChatComponent::SendRequestGetListOfMessages));
	RequestGetListOfMessages->BindArray<TArray<FChatMessageReciveModel>>(200).AddUObject(this, &UChatComponent::OnMessages);

	RequestPostMessageTo = CreateRequest(FServerHost::MasterClient, "Chat/PostMessageTo");
	RequestPostMessageTo->Bind(200).AddUObject(this, &UChatComponent::SendRequestGetListOfMessages);
}

void UChatComponent::StartTimers()
{
	if (GetWorld())
	{
		RequestGetListOfChanels->StartTimer(&GetWorld()->GetTimerManager());
		RequestGetListOfMembers->StartTimer(&GetWorld()->GetTimerManager());
		RequestGetListOfMessages->StartTimer(&GetWorld()->GetTimerManager());
	}
}

bool UChatComponent::SendRequestGetListOfChanels_Validate() { return true; }
void UChatComponent::SendRequestGetListOfChanels_Implementation()
{
	RequestGetListOfChanels->GetRequest();
}

bool UChatComponent::SendRequestGetListOfMembers_Validate() { return true; }
void UChatComponent::SendRequestGetListOfMembers_Implementation()
{
	RequestGetListOfMembers->GetRequest();
}

bool UChatComponent::SendRequestGetListOfMessages_Validate() { return true; }
void UChatComponent::SendRequestGetListOfMessages_Implementation()
{
	RequestGetListOfMessages->GetRequest();
}

bool UChatComponent::SendRequestPostMessageTo_Validate(const FChatMessageSendModel& InMessage) { return true; }
void UChatComponent::SendRequestPostMessageTo_Implementation(const FChatMessageSendModel& InMessage)
{
	RequestPostMessageTo->SendRequestObject<FChatMessageSendModel>(InMessage);
}

void UChatComponent::OnChannels(const TArray<FChatChanelModel>& InChannels)
{
	if (Channels != InChannels)
	{
		Channels = InChannels;
		OnRep_Channels();
	}
}

void UChatComponent::OnMembers(const TArray<FChatMemberModel>& InMembers)
{

}

void UChatComponent::OnMessages(const TArray<FChatMessageReciveModel>& InMessages)
{
	if (Messages != InMessages)
	{
		Messages = InMessages;
		OnRep_Messages();
	}
}

void UChatComponent::OnRep_Channels()
{
	OnChannelsUpdate.Broadcast(Channels);
}

void UChatComponent::OnRep_Messages()
{
	OnMessagesUpdate.Broadcast(Messages);
}

void UChatComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(UChatComponent, Channels, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(UChatComponent, Messages, COND_OwnerOnly);
}