#pragma once

#include "SlateButtonStyle.h"
#include "SlateBorderStyle.h"
#include "SlateStyleComponent.h"
#include "MarketCatalogStyle.generated.h"

USTRUCT()
struct FMarketFractionContainerStyle
{
	GENERATED_USTRUCT_BODY()
	UPROPERTY(EditDefaultsOnly) FSlateBorderStyle		Background			= FSlateBorderStyle(0);
	UPROPERTY(EditDefaultsOnly) FSlateBorderStyle		FractionBackground	= FSlateBorderStyle(0);
	UPROPERTY(EditDefaultsOnly) FButtonStyle			FractionButtonStyle;
};

USTRUCT()
struct FMarketFractionProgressStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly) FSlateBorderStyle		Border = FSlateBorderStyle(5);
	UPROPERTY(EditDefaultsOnly) FSlateBorderStyle		Background = FSlateBorderStyle(0);

	UPROPERTY(EditDefaultsOnly) FTextBlockStyle			FractionNameStyle;
	UPROPERTY(EditDefaultsOnly) FTextBlockStyle			FractionRankStyle;
	UPROPERTY(EditDefaultsOnly) FTextBlockStyle			FractionReputationStyle;

	UPROPERTY(EditDefaultsOnly) FSlateButtonStyle		JoinFraction;
};


USTRUCT()
struct FMarketCatalogContainerStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly) FMargin		ItemsPadding = FMargin(5, 4);
	UPROPERTY(EditDefaultsOnly) FVector2D	ItemSize = FVector2D(220, 250);

	UPROPERTY(EditDefaultsOnly) FSlateBorderStyle	Background = FSlateBorderStyle(0);
};

USTRUCT()
struct FMarketFractionItemStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly) FMargin				ImagePadding = FMargin(5);
	UPROPERTY(EditDefaultsOnly) FMargin				LevelPadding = FMargin(10);
	UPROPERTY(EditDefaultsOnly) FTextBlockStyle		LevelStyle;
	UPROPERTY(EditDefaultsOnly) FSlateBorderStyle	Background = FSlateBorderStyle(0);
};

USTRUCT()
struct FMarketCatalogItemStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly) FMargin								ItemsPadding = FMargin(5, 4);
	UPROPERTY(EditDefaultsOnly) FVector2D							ItemSize = FVector2D(220, 250);
	UPROPERTY(EditDefaultsOnly) FMarketFractionItemStyle			Fraction;

	UPROPERTY(EditDefaultsOnly) TEnumAsByte<ETextJustify::Type>		HeaderJustify = ETextJustify::Center;
	UPROPERTY(EditDefaultsOnly) FSlateBorderStyle					HeaderBackground = FSlateBorderStyle(0);
	UPROPERTY(EditDefaultsOnly) FTextBlockStyle						HeaderStyle;

	UPROPERTY(EditDefaultsOnly) TEnumAsByte<ETextJustify::Type>		CostJustify = ETextJustify::Right;
	UPROPERTY(EditDefaultsOnly) FSlateBorderStyle					CostBackground = FSlateBorderStyle(10);
};

USTRUCT()
struct FMarketItemtDescriptionStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly) TEnumAsByte<ETextJustify::Type>		HeaderJustify = ETextJustify::Center;
	UPROPERTY(EditDefaultsOnly) FSlateBorderStyle					HeaderBackground = FSlateBorderStyle(0);
	UPROPERTY(EditDefaultsOnly) FTextBlockStyle						HeaderStyle;

	UPROPERTY(EditDefaultsOnly) TEnumAsByte<ETextJustify::Type>		DescriptionJustify = ETextJustify::Center;
	UPROPERTY(EditDefaultsOnly) FSlateBorderStyle					DescriptionBackground = FSlateBorderStyle(0);
	UPROPERTY(EditDefaultsOnly) FTextBlockStyle						DescriptionStyle;

	UPROPERTY(EditDefaultsOnly) TEnumAsByte<ETextJustify::Type>		CostJustify = ETextJustify::Right;
	UPROPERTY(EditDefaultsOnly) FSlateBorderStyle					CostBackground = FSlateBorderStyle(10);
	UPROPERTY(EditDefaultsOnly) FTextBlockStyle						CostStyle;

	UPROPERTY(EditDefaultsOnly) FButtonStyle						PurchaseButtonStyle;
};

USTRUCT()
struct FMarketCatalogStyle : public FSlateStyleComponent
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = "Window", EditDefaultsOnly)	FSlateBorderStyle				WindowBorder = FSlateBorderStyle(FMargin(5, 0, 0, 0));
	UPROPERTY(Category = "Window", EditDefaultsOnly)	FMargin							WindowScreenPadding = FMargin(50, 150, 50, 100);

	UPROPERTY(Category = "Fraction", EditDefaultsOnly)	FMarketFractionContainerStyle	FractionContainer;

	UPROPERTY(Category = "Fraction", EditDefaultsOnly)	FMarketFractionProgressStyle	FractionProgress;

	UPROPERTY(Category = "Catalog", EditDefaultsOnly)	FMarketCatalogContainerStyle	CatalogContainer;

	UPROPERTY(Category = "Catalog", EditDefaultsOnly)	FMarketCatalogItemStyle			CatalogItem;

	UPROPERTY(Category = "Catalog", EditDefaultsOnly)	FMarketItemtDescriptionStyle	Description;

	SlateStyleContructor(FMarketCatalogStyle);
};

UCLASS(hidecategories = Object, MinimalAPI)
class UMarketCatalogStyleContainer : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	UPROPERTY(Category = Appearance, EditAnywhere, meta = (ShowOnlyInnerProperties))
	FMarketCatalogStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >(&WidgetStyle);
	}
};