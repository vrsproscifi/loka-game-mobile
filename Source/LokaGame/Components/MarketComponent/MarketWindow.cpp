﻿#include "LokaGame.h"
#include "MarketWindow.h"
#include "MarketFractionContainer.h"
#include "MarketFractionProgress.h"
#include "MarketCatalogContainer.h"
#include "MarketDescriptionContainer.h"

void SMarketPlaceWindow::Construct(const FArguments& InArgs, const TArray<const UPlayerFractionEntity*>& fractions, const UPlayerFractionEntity* selectedFraction)
{
	//==============================================
	OnSelectItem = InArgs._OnSelectItem;
	OnPurchaseItem = InArgs._OnPurchaseItem;
	OnSwitchPlayerFraction = InArgs._OnSwitchPlayerFraction;
	SelectedFraction = selectedFraction;
	
	//==============================================
	if (GetValidObject(SelectedFraction) == nullptr)
	{
		if (fractions.IsValidIndex(0))
		{
			SelectedFraction = fractions[0];
		}
	}

	//==============================================
	if (auto f = GetValidObject(SelectedFraction))
	{
		SelectedItem = f->GetItemList()->Last();
	}

	//==============================================
	ChildSlot
	[
		SNew(SOverlay)
		+ SOverlay::Slot()
		.HAlign(HAlign_Left)
		.VAlign(VAlign_Top)
		.Padding(Style->WindowScreenPadding)
		[
			SNew(SBorder)
			.Padding(Style->WindowBorder.Padding)
			.BorderImage(&Style->WindowBorder.Brush)
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot()
				.AutoHeight()
				[
					SAssignNew(FractionProgress, SMarketFractionProgress, &SelectedFraction)
					.OnPlayerFractionSwitch(OnSwitchPlayerFraction)
				]
				+ SVerticalBox::Slot()
				.FillHeight(1)
				[
					SNew(SHorizontalBox)
					//============================
					//	Фракции
					+ SHorizontalBox::Slot()
					.AutoWidth()
					[
						SAssignNew(Fractions, SMarketFractionContainer, fractions, &SelectedFraction)
						.OnFractionSwitch_Raw(this, &SMarketPlaceWindow::OnSwitchFractionPage)
					]
					//============================
					//	Магазин
					+ SHorizontalBox::Slot()
					.FillWidth(1)
					[
						SAssignNew(Catalog, SMarketCatalogContainer, &SelectedFraction)
						.OnSelectItem(this, &SMarketPlaceWindow::OnSelectItemHandle)
					]				
				]
			]
		]
		+ SOverlay::Slot()
		.HAlign(EHorizontalAlignment::HAlign_Center)
		.VAlign(EVerticalAlignment::VAlign_Bottom)
		.Padding(FMargin(0, 0, 0, 100))
		[
			SAssignNew(Description, SMarketDescriptionContainer, &SelectedItem)
			.OnPurchaseItem(InArgs._OnPurchaseItem)
		]
	];
}

void SMarketPlaceWindow::ToggleWidget(const bool InToggle)
{
	SWidget::SetVisibility(InToggle ? EVisibility::SelfHitTestInvisible : EVisibility::Hidden);
	OnToggleInteractive.ExecuteIfBound(InToggle);
}

bool SMarketPlaceWindow::IsInInteractiveMode() const
{
	return (Visibility.Get() == EVisibility::Visible || Visibility.Get() == EVisibility::SelfHitTestInvisible);
}

void SMarketPlaceWindow::OnSwitchFractionPage(const UPlayerFractionEntity* fraction)
{
	SelectedFraction = fraction;
	Catalog->CatalogRefresh();
}
