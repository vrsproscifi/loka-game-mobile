#pragma once
#include "MarketCatalogStyle.h"
#include "MarketFractionWidget.h"

class SMarketFractionContainer
	: public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SMarketFractionContainer) {}
	SLATE_EVENT(FOnMarketFractionSwitch, OnFractionSwitch)
	SLATE_EVENT(FOnMarketFractionSwitch, OnPlayerFractionSwitch)
	SLATE_END_ARGS()

	FOnMarketFractionSwitch OnPlayerFractionSwitch;

	void Construct(const FArguments& InArgs, const TArray<const UPlayerFractionEntity*>& fractions, const UPlayerFractionEntity** selectedFraction)
	{
		OnPlayerFractionSwitch = InArgs._OnPlayerFractionSwitch;
		Fraction = selectedFraction;

		//====================================
		TSharedPtr<SVerticalBox> container;

		//====================================
		ChildSlot
		[
			SNew(SBorder)
			.BorderImage(&Style->FractionContainer.Background.Brush)
			.Padding(Style->FractionContainer.Background.Padding)
			[
				SAssignNew(container, SVerticalBox)
			]
		];

		//====================================
		for(auto f : fractions)
		{
			TSharedPtr<SMarketFractionWidget> w;
			container->AddSlot()
			.AutoHeight()
			[
				SAssignNew(w, SMarketFractionWidget, f)
				.OnFractionSwitch(InArgs._OnFractionSwitch)
				.IsSelected_Lambda([this, f]
				{
					return Fraction && f == *Fraction;
				})
			];
			Fractions.Add(f->GetModelId(), w);
		}
	}
private:
	SlateStylizedWidget(FMarketCatalogStyle, "MarketCatalogStyle");

private:
	TMap<FractionTypeId, TSharedPtr<SMarketFractionWidget>>	Fractions;
	const UPlayerFractionEntity** Fraction;
};