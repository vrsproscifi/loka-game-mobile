#pragma once
#include "Item/ItemBaseEntity.h"
#include "Fraction/FractionEntity.h"
#include "MarketCatalogStyle.h"

class SFractionItemLevelComponent
		: public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SFractionItemLevelComponent) {}
	SLATE_END_ARGS()


	void Construct(const FArguments& InArgs, const UItemBaseEntity** item)
	{
		Item = item;
		ChildSlot
		[
			SNew(SBorder)
			.BorderImage(&Style->CatalogItem.Fraction.Background.Brush)
			[
				SNew(SWrapBox)
				+ SWrapBox::Slot()
				.Padding(Style->CatalogItem.Fraction.ImagePadding)
				[
					SNew(SImage)
					.Image(this, &SFractionItemLevelComponent::GetFractionIcon)
				]
				+ SWrapBox::Slot()
				[
					SNew(STextBlock)
					.Margin(Style->CatalogItem.Fraction.LevelPadding)
					.Text(this, &SFractionItemLevelComponent::GetLevelAsText)
					.TextStyle(&Style->CatalogItem.Fraction.LevelStyle)		
				]		
			]
		];
	}

private:
	SlateStylizedWidget(FMarketCatalogStyle, "MarketCatalogStyle");


private:
	FText GetLevelAsText() const
	{
		if(auto item = GetItem())
		{
			return item->GetLevelAsText();
		}
		return FText::GetEmpty();
	}

	const UItemBaseEntity* GetItem() const
	{
		if (Item)
		{
			return GetValidObject(*Item);
		}
		return nullptr;
	}

	const FSlateBrush* GetFractionIcon() const
	{
		if (auto item = GetItem())
		{
			if (auto f = item->GetFraction())
			{
				return f->GetImage();
			}
		}
		return nullptr;
	}
	const UItemBaseEntity** Item;
};
