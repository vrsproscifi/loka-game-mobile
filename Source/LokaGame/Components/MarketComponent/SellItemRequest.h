#pragma once
#include "SellItemRequest.generated.h"

USTRUCT(BlueprintType)
struct FSellItemRequest
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()	FString ItemId;
	UPROPERTY()	uint64 Amount;
};