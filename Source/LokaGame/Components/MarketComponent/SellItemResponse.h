#pragma once

#include "TypeCost.h"
#include "SellItemResponse.generated.h"

USTRUCT(BlueprintType)
struct FSellItemResponse
{
	GENERATED_USTRUCT_BODY()

	/* 
	FGuid ItemId
	*/
	UPROPERTY()	FLokaGuid ItemId;

	/* 
	The amount for which it was sold
	*/
	UPROPERTY()	FTypeCost Summ;

	/*
	How much is left after the sale
	*/
	UPROPERTY()	uint64 Amount;

	/*
	How many was sold
	*/
	UPROPERTY()	uint64 Selled;
};