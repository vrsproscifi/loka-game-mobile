#pragma once
#include "Containers/PlayerFractionEntity.h"
#include "Entity/Fraction/FractionTypeId.h"
#include "MarketCatalogStyle.h"

DECLARE_DELEGATE_OneParam(FOnMarketFractionSwitch, const UPlayerFractionEntity*);

class SMarketFractionWidget
	: public SButton
{
	SLATE_BEGIN_ARGS(SMarketFractionWidget) {}
	SLATE_EVENT(FOnMarketFractionSwitch, OnFractionSwitch)
		SLATE_EVENT(FOnMarketFractionSwitch, OnPlayerFractionSwitch)
		SLATE_ATTRIBUTE(bool, IsSelected)
		SLATE_END_ARGS()

		FOnMarketFractionSwitch OnPlayerFractionSwitch;

	const FractionTypeId GetId() const
	{
		//if (Fraction == nullptr)
		//{
		//	return FractionTypeId::End;
		//}
		return Fraction->GetModelId();
	}

	FReply OnFractionSwitch() const
	{
		FractionSwitchDelegate.ExecuteIfBound(Fraction);
		return FReply::Handled();
	}

	void Construct(const FArguments& InArgs, const UPlayerFractionEntity* fraction)
	{
		OnPlayerFractionSwitch = InArgs._OnPlayerFractionSwitch;
		FractionSwitchDelegate = InArgs._OnFractionSwitch;
		IsSelected = InArgs._IsSelected;
		Fraction = fraction;

		SButton::Construct(
		SButton::FArguments()
		.ButtonStyle(&Style->FractionContainer.FractionButtonStyle)
		.OnClicked(this, &SMarketFractionWidget::OnFractionSwitch)
		.ContentPadding(FMargin(0))
		[
			SNew(SBorder)
			.Padding(0)
			.BorderImage(&Style->FractionContainer.FractionBackground.Brush)
			[
				SNew(SBorder)
				.Padding(Style->FractionContainer.FractionBackground.Padding)
				.BorderImage_Lambda([this]
				{
					if (IsSelected.Get())
					{
						return &Style->FractionContainer.FractionButtonStyle.Pressed;
					}
					else
					{
						return &Style->FractionContainer.FractionButtonStyle.Normal;
					}
				})
				[
					SNew(SImage)
					.Image_UObject(Fraction, &UPlayerFractionEntity::GetImage)
				]
			]
		]);
	}

private:
	SlateStylizedWidget(FMarketCatalogStyle, "MarketCatalogStyle");

private:
	FOnMarketFractionSwitch FractionSwitchDelegate;
	const UPlayerFractionEntity* Fraction;
	TAttribute<bool> IsSelected;
};