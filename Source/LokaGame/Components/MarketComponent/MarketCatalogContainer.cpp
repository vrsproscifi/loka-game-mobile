#include "LokaGame.h"
#include "MarketCatalogContainer.h"
#include "PlayerFractionEntity.h"
#include "MarketCatalogProduct.h"

void SMarketCatalogContainer::Construct(const FArguments& InArgs, const UPlayerFractionEntity** selectedFraction)
{
	OnSelectItem = InArgs._OnSelectItem;
	SelectedFraction = selectedFraction;
	ChildSlot
	[
		SNew(SBox)
		.WidthOverride(Style->CatalogContainer.ItemSize.X * 2 + 20)
		[
			SNew(SBorder)
			.BorderImage(&Style->CatalogContainer.Background.Brush)
			[
				SAssignNew(ProductsTileWidget, STileView<UItemBaseEntity*>)
				.SelectionMode(ESelectionMode::None)
				.ListItemsSource(&ItemsList)
				.ItemWidth(Style->CatalogContainer.ItemSize.X)
				.ItemHeight(Style->CatalogContainer.ItemSize.Y)
				.AllowOverscroll(EAllowOverscroll::Yes)
				.ItemAlignment(EListItemAlignment::Fill)
				.ScrollbarVisibility(EVisibility::Visible)
				.OnGenerateTile(this, &SMarketCatalogContainer::OnProductsGenerateTile)
			]
		]
	];

	CatalogRefresh();
}

void SMarketCatalogContainer::CatalogRefresh()
{
	if (ProductsTileWidget.IsValid() && ProductsTileWidget.Get())
	{
		if (SelectedFraction)
		{
			if (auto f = GetValidObject(*SelectedFraction))
			{				
				ItemsList = f->GetItemList()->FilterByPredicate([&](const UItemBaseEntity* item)
				{
					return	item->GetCategory() != CategoryTypeId::Building
						&& item->GetCategory() != CategoryTypeId::Character
						&& item->GetCategory() != CategoryTypeId::Currency
						&& item->GetCategory() != CategoryTypeId::Addon
						&& item->GetCategory() != CategoryTypeId::Material;
				});

				ItemsList.Sort(UItemBaseEntity::SortByLevelAsc);
			}
		}
		ProductsTileWidget->RequestListRefresh();
	}
}

TSharedRef<ITableRow> SMarketCatalogContainer::OnProductsGenerateTile(UItemBaseEntity* Item, const TSharedRef<STableViewBase>& Table) const
{
	return SNew(STableRow<UItemBaseEntity*>, Table)
	.Padding(Style->CatalogContainer.ItemsPadding)
	[
		SNew(SMarketCatalogProduct, Item)
		.OnSelectItem(OnSelectItem)
	];
}
