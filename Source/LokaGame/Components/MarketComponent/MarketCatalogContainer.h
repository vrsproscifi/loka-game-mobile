#pragma once
#include "Item/ItemBaseEntity.h"
#include "MarketCatalogStyle.h"

class UPlayerFractionEntity;

DECLARE_DELEGATE_OneParam(FOnUniversalItemAction, const UItemBaseEntity*);

class SMarketCatalogContainer
	: public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SMarketCatalogContainer) {}
	SLATE_EVENT(FOnUniversalItemAction, OnSelectItem)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, const UPlayerFractionEntity** selectedFraction);
	void CatalogRefresh();
private:
	FOnUniversalItemAction OnSelectItem;
	SlateStylizedWidget(FMarketCatalogStyle, "MarketCatalogStyle");


private:
	TSharedRef<ITableRow> OnProductsGenerateTile(UItemBaseEntity* Item, const TSharedRef<STableViewBase>& Table) const;
	TArray<UItemBaseEntity*> ItemsList;

	TSharedPtr<STileView<UItemBaseEntity*>> ProductsTileWidget;
	TArray<UItemBaseEntity*>				ItemsList_Source;
	const UPlayerFractionEntity**			SelectedFraction;
};
