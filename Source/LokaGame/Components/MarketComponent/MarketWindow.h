#pragma once
#include "Utilities/SUsableCompoundWidget.h"
#include "MarketCatalogStyle.h"

class SMarketCatalogContainer;
class SMarketFractionProgress;
class SMarketCatalogWidget;
class SMarketFractionContainer;
class SMarketDescriptionContainer;

class UPlayerFractionEntity;
class UItemBaseEntity;

DECLARE_DELEGATE_OneParam(FOnUniversalItemAction, const UItemBaseEntity*);
DECLARE_DELEGATE_OneParam(FOnMarketFractionSwitch, const UPlayerFractionEntity*);

class SMarketPlaceWindow
	: public SUsableCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SMarketPlaceWindow)
	{
		_Visibility = EVisibility::Hidden;
	}
	SLATE_EVENT(FOnUniversalItemAction, OnSelectItem)
	SLATE_EVENT(FOnUniversalItemAction, OnPurchaseItem)
	SLATE_EVENT(FOnMarketFractionSwitch, OnSwitchPlayerFraction)
	SLATE_END_ARGS()
	void Construct(const FArguments& InArgs, const TArray<const UPlayerFractionEntity*>& fractions, const UPlayerFractionEntity* selectedFraction = nullptr);

public:
	virtual void ToggleWidget(const bool InToggle) override;
	virtual bool IsInInteractiveMode() const override;

public:
	FOnMarketFractionSwitch OnSwitchPlayerFraction;

private:
	const UItemBaseEntity* SelectedItem;
	FOnUniversalItemAction OnSelectItem;
	FOnUniversalItemAction OnPurchaseItem;

	void OnSelectItemHandle(const UItemBaseEntity* item)
	{
		SelectedItem = item;
		OnSelectItem.ExecuteIfBound(item);
	}

	FReply OnPurchaseClicked() const
	{
		OnPurchaseItem.ExecuteIfBound(SelectedItem);
		return FReply::Handled();
	}

	void OnSwitchFractionPage(const UPlayerFractionEntity* fraction);

private:
	SlateStylizedWidget(FMarketCatalogStyle, "MarketCatalogStyle");

	const UPlayerFractionEntity*				SelectedFraction;			
	TSharedPtr<SMarketFractionProgress>			FractionProgress;
	TSharedPtr<SMarketFractionContainer>		Fractions;
	TSharedPtr<SMarketCatalogContainer>			Catalog;
	TSharedPtr<SMarketDescriptionContainer>		Description;
};
