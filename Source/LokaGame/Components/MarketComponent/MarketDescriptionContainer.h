#pragma once
#include "FractionItemLevelComponent.h"
#include "SResourceTextBox.h"

class UItemBaseEntity;

DECLARE_DELEGATE_OneParam(FOnUniversalItemAction, const UItemBaseEntity*);

class SMarketDescriptionContainer
	: public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SMarketDescriptionContainer) {}
		SLATE_EVENT(FOnUniversalItemAction, OnPurchaseItem)
	SLATE_END_ARGS()

	SlateStylizedWidget(FMarketCatalogStyle, "MarketCatalogStyle");

	void Construct(const FArguments& InArgs, const UItemBaseEntity** selectedItem)
	{
		SelectedItem = selectedItem;
		OnPurchaseItem = InArgs._OnPurchaseItem;

		ChildSlot
		.HAlign(EHorizontalAlignment::HAlign_Center)
		.VAlign(EVerticalAlignment::VAlign_Bottom)
		.Padding(FMargin(0, 0, 0, 100))
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
			.AutoHeight()
			[
				SNew(SBorder)
				.Padding(0)
				.BorderImage(&Style->Description.HeaderBackground.Brush)
				[
					SNew(SBox)
					.HeightOverride(50)
					.WidthOverride(600)
					[
						SNew(SHorizontalBox)
						+ SHorizontalBox::Slot()
						.AutoWidth()
						[
							SNew(SFractionItemLevelComponent, SelectedItem)
						]
						+ SHorizontalBox::Slot()
						.VAlign(VAlign_Center)
						.FillWidth(1)
						[
							SNew(STextBlock)
							.TextStyle(&Style->Description.HeaderStyle)
							.Justification(Style->Description.HeaderJustify)
							.Text(this, &SMarketDescriptionContainer::GetItemName)
						]
						+ SHorizontalBox::Slot()
						.AutoWidth()
						[
							SNew(SBorder)
							.Padding(Style->CatalogItem.CostBackground.Padding)
							.BorderImage(&Style->CatalogItem.CostBackground.Brush)
							[
								SNew(SResourceTextBox).CustomSize(FVector(16, 16, 12))
								.TypeAndValue(this, &SMarketDescriptionContainer::GetCost)
							]
						]
					]
				]
			]
			+ SVerticalBox::Slot()
			.AutoHeight()
			[
				SNew(SBorder)
				.BorderImage(&Style->Description.DescriptionBackground.Brush)
				[
					SNew(STextBlock)
					.TextStyle(&Style->Description.DescriptionStyle)
					.Justification(Style->Description.DescriptionJustify)
					.Text(this, &SMarketDescriptionContainer::GetItemDescription)
				]
			]
			+ SVerticalBox::Slot()
			.AutoHeight()
			[
				SNew(SButton)
				.ButtonStyle(&Style->Description.PurchaseButtonStyle)
				.OnClicked(this, &SMarketDescriptionContainer::OnPurchaseClicked)
				.Text(NSLOCTEXT("UMarketComponent", "Item.Purchase.Name", "Purchase"))
			]
			
		];
	}

private:
	FText GetItemName() const
	{
		if(auto i = GetItem())
		{
			return i->GetItemName();
		}
		return FText::GetEmpty();
	}

	FText GetItemDescription() const
	{
		if (auto i = GetItem())
		{
			return i->GetItemDescription();
		}
		return FText::GetEmpty();
	}

	FResourceTextBoxValue GetCost() const
	{
		if (auto i = GetItem())
		{
			return i->GetCostAsResource();
		}
		return FResourceTextBoxValue();
	}

	FORCEINLINE_DEBUGGABLE const UItemBaseEntity* GetItem() const
	{
		if (SelectedItem)
		{
			return GetValidObject(*SelectedItem);
		}
		return nullptr;
	}

private:
	FReply OnPurchaseClicked() const
	{
		OnPurchaseItem.ExecuteIfBound(*SelectedItem);
		return FReply::Handled();
	}

private:
	const UItemBaseEntity** SelectedItem;
	FOnUniversalItemAction OnPurchaseItem;
};