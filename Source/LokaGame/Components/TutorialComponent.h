// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BasicGameComponent.h"
#include "Models/ConfirmTutorialStepResponse.h"
#include "TutorialComponent.generated.h"

class UItemBaseEntity;
class ABuildPlacedComponent;
class UTutorialBase;
/**
 * 
 */
UCLASS()
class LOKAGAME_API UTutorialComponent : public UBasicGameComponent
{
	GENERATED_BODY()
	
public:

	UTutorialComponent();
	
	virtual void OnAuthorizationComplete() override;

	UFUNCTION(Reliable, Server, WithValidation, BlueprintCallable, Category = Tutorial)
	void SendRequestTutorialStep(const FStartTutorialView& InData);

	UFUNCTION(Reliable, Server, WithValidation, BlueprintCallable, Category = Tutorial)
	void SendRequestTutorialStart(const int32 InTutorialIndex);

	// Buildings events
	UFUNCTION() void OnEvent_PlaceBuilding(ABuildPlacedComponent* InBuilding);
	UFUNCTION() void OnEvent_RemoveBuilding(ABuildPlacedComponent* InBuilding);
	UFUNCTION() void OnEvent_SelectBuilding(ABuildPlacedComponent* InBuilding);

	// Shop events
	UFUNCTION() void OnEvent_BuyItem(UItemBaseEntity* InItem);

	virtual void OnComponentDestroyed(bool bDestroyingHierarchy) override;

protected:

	TSharedPtr<FOperationRequest>	RequestTutorialStep;
	TSharedPtr<FOperationRequest>	RequestTutorialStart;

	void OnTutorialStep(const FConfirmTutorialStepResponse& InResponse);
	void OnTutorialStart(const FStartTutorialView& InResponse);

	UFUNCTION()		
	void OnRep_TutorialState();

	UFUNCTION()
	void OnRep_TutorialStart();

	UPROPERTY(ReplicatedUsing = OnRep_TutorialState)	FConfirmTutorialStepResponse			CurrentData;
	UPROPERTY(ReplicatedUsing = OnRep_TutorialStart)	FStartTutorialView						CurrentStartData;
	UPROPERTY()											UTutorialBase*							CurrentTutorial;
};
