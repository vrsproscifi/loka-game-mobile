// VRSPRO

#include "LokaGame.h"
#include "AssetCaptureScene.h"

#include "Item/ItemBaseEntity.h"
#include "Module/ItemModuleEntity.h"
#include "Weapon/ItemWeaponEntity.h"

// Sets default values
AAssetCaptureScene::AAssetCaptureScene()
{
	Scene = CreateDefaultSubobject<USceneComponent>("Scene");
	RootComponent = Scene;

	Arrow = CreateDefaultSubobject<UArrowComponent>("Arrow");
	Arrow->SetupAttachment(RootComponent);

	Capture = CreateDefaultSubobject<USceneCaptureComponent2D>("Capture");
	Capture->SetupAttachment(Arrow);
	Capture->bCaptureEveryFrame = false;

	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>("SkeletalMesh");
	SkeletalMesh->SetupAttachment(RootComponent);
	Capture->ShowOnlyComponent(SkeletalMesh);

	Cube = CreateDefaultSubobject<UStaticMeshComponent>("Cube");
	Cube->SetupAttachment(RootComponent);
	
	Light = CreateDefaultSubobject<UPointLightComponent>("Light");
	Light->SetupAttachment(RootComponent);

	//TODO: SeNTIke
	//ConstructModulesHelper(SkeletalMesh);	

 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	CurrentDelta = .0f;
}

void AAssetCaptureScene::BeginCapture(UItemBaseEntity* Target)
{
	if (Target->RenderTexture)
	{
		Queue.Insert(Target, 0);
	}
}

void AAssetCaptureScene::RealCapture(UItemBaseEntity* Target)
{	
	SkeletalMesh->SetRelativeLocation(MeshZero);

	if (Target->RelativeLocationInCapturing != FVector::ZeroVector)
	{
		SkeletalMesh->SetRelativeLocation(Target->RelativeLocationInCapturing);
	}

	SkeletalMesh->SetSkeletalMesh(Target->GetSkeletalMesh());

	if (auto TargetWeapon = Cast<UItemWeaponEntity>(Target))
	{
		SkeletalMesh->SetRelativeRotation(FRotator(0, 0, 0));
		//TODO: SeNTIke
		//InitializeModules(TargetWeapon);
	}
	else
	{
		SkeletalMesh->SetRelativeRotation(FRotator(0, 90, 0));
		SkeletalMesh->SetMaterial(0, Target->GetCurrentMaterial());
	}	
	
	SkeletalMesh->UpdateBounds();

	//ProjectOnTo(SkeletalMesh->GetComponentLocation()); X
	const auto cPos = SkeletalMesh->Bounds.GetBox().GetCenter();
	FVector2D CenterPos(cPos.Y, cPos.Z);

	const auto oPos = SkeletalMesh->GetComponentLocation();
	FVector2D ObjectPos(oPos.Y, oPos.Z);

	CenterPos -= ObjectPos;	

	if (Target->IsA<UItemModuleEntity>())
	{
		SkeletalMesh->AddLocalOffset(FVector(-CenterPos.X, 50, -CenterPos.Y)); //FVector(X, Z, Y)
	}
	else if (Target->IsA<UItemWeaponEntity>())
	{
		SkeletalMesh->AddLocalOffset(FVector(0, -CenterPos.X, -CenterPos.Y)); //FVector(X, Z, Y)
	}
	else
	{
		SkeletalMesh->AddLocalOffset(FVector(-CenterPos.X, 0, -CenterPos.Y)); //FVector(X, Z, Y)
	}
	//SkeletalMesh->SetRelativeLocation(TestV);
	UE_LOG(LogCore, Log, TEXT("Capture Thumbnail for %s, Bounds offset: %s"), *Target->GetName(), *CenterPos.ToString());

	Capture->TextureTarget = Target->RenderTexture;
	//Capture->UpdateComponentToWorld();
	Capture->UpdateContent();

	//if (auto Weapon = Cast<UItemWeaponEntity>(Target))
	//{
	//	if (Weapon->WeaponProperty.Mass <= .0f)
	//	{
	//		Weapon->WeaponProperty.Mass = SkeletalMesh->CalculateMass();
	//	}
	//}
}

// Called when the game starts or when spawned
void AAssetCaptureScene::BeginPlay()
{
	Super::BeginPlay();
	
	MeshZero = SkeletalMesh->RelativeLocation;		
}

// Called every frame
void AAssetCaptureScene::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	CurrentDelta += DeltaTime;

	if (CurrentDelta > 0.2f && Queue.Num())
	{
		RealCapture(Queue.Pop());
		CurrentDelta = .0f;
	}
}
