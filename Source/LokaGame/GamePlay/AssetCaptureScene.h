// VRSPRO

#pragma once

#include "GameFramework/Actor.h"
//#include "Weapon/WeaponModularHelper.h"
#include "AssetCaptureScene.generated.h"

class UItemBaseEntity;

UCLASS()
class LOKAGAME_API AAssetCaptureScene : public AActor
{
	GENERATED_BODY()
	
public:	

	//DECLARE_FWeaponModularHelper();
	// Sets default values for this actor's properties
	AAssetCaptureScene();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	void BeginCapture(UItemBaseEntity* Target);

protected:

	void RealCapture(UItemBaseEntity* Target);

	UPROPERTY(VisibleAnywhere, Category = Component)
	UArrowComponent* Arrow;

	UPROPERTY(VisibleAnywhere, Category = Component)
	USceneComponent* Scene;

	UPROPERTY(VisibleAnywhere, Category = Component)
	UStaticMeshComponent* Cube;

	UPROPERTY(VisibleAnywhere, Category = Component)
	USkeletalMeshComponent* SkeletalMesh;

	UPROPERTY(VisibleAnywhere, Category = Component)
	UPointLightComponent* Light;

	UPROPERTY(VisibleAnywhere, Category = Component)
	USceneCaptureComponent2D* Capture;

	UPROPERTY()
	TArray<UItemBaseEntity*> Queue;

	UPROPERTY()
	float CurrentDelta;

	UPROPERTY()
	FVector MeshZero;
};
