// VRSPRO

#pragma once

#include "CharacterAbility_Installable.h"
#include "CharacterAbility_HealthPost.generated.h"

class AUTCharacter;
class AUTPlayerState;

UCLASS(abstract)
class LOKAGAME_API ACharacterAbility_HealthPost : public ACharacterAbility_Installable
{
	GENERATED_BODY()

public:
	ACharacterAbility_HealthPost();
	
	virtual bool RequestEndActivate() override;

	virtual void RequestDeactivate(const bool IsDestroy = false) override;
	virtual void PostInitializeComponents() override;

protected:

	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh)
	UVectorFieldComponent* PostRoot;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh)
	UStaticMeshComponent* PostMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh)
	USphereComponent* PostSphere;

	UPROPERTY()
	TArray<AUTCharacter*> Characters;

	UPROPERTY()
	TMap<APlayerState*, float> RegeneratedHealths;

	UPROPERTY(EditDefaultsOnly, Category = Ability)
	float HealthRegenerationSpeed;

	UFUNCTION()
	void OnBeginOverlapSphere(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION()
	void OnEndOverlapSphere(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	void GiveRewardFrom(APlayerState* InState);
};
