// VRSPRO

#pragma once

#include "GameFramework/Actor.h"
#include "BaseTurret.generated.h"

class AUTPlayerState;

UCLASS()
class LOKAGAME_API ABaseTurret : public AActor
{
	GENERATED_BODY()
	
public:	

	ABaseTurret();
	virtual void BeginPlay() override;
	virtual void Tick( float DeltaSeconds ) override;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FORCEINLINE float GetHealth() const
	{
		return Health.X;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FORCEINLINE float GetHealthPercent() const
	{
		return Health.X / Health.Y;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FORCEINLINE bool IsDying() const
	{
		return bIsDie;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FORCEINLINE bool GetCanFire() const
	{
		return bCanFire;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FORCEINLINE FText GetLocalizedName() const
	{
		return DisplayNameUI;
	}

	UFUNCTION(BlueprintCallable, Category = Gameplay)
	static ACharacter *FindTarget(const FVector& TurretOffset, const ABaseTurret *Turret);

protected:

	/** X - Current, Y - Maximum */
	UPROPERTY(Replicated, EditDefaultsOnly, Category = Gameplay)
	FVector2D Health;
	
	/** Total score points to give all killers */
	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	float TotalScoreReward;

	/** Sphere radius, maximum attack area */
	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	float TriggerSphereRadius;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = UI)
	FText DisplayNameUI;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Gameplay)
	USphereComponent* SphereRadius;

	UPROPERTY(Replicated, BlueprintReadWrite, Category = Gameplay)
	bool bIsDie;

	UPROPERTY(Replicated, BlueprintReadWrite, Category = Gameplay)
	bool bCanFire;

	UPROPERTY()
	TMap<AUTPlayerState*, float> DamageByPlayers;

	UPROPERTY(BlueprintReadWrite, Category = Gameplay)
	TArray<ACharacter*> CharactersIntoRadius;

	UFUNCTION(BlueprintImplementableEvent, Category = Gameplay)
	void OnDeath();

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
	virtual void NotifyActorEndOverlap(AActor* OtherActor) override;

	virtual void PostActorCreated() override;
	// OR
	//virtual void PostInitializeComponents() override;
};
