// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GamePlay/CharacterAbility.h"
#include "CharacterAbility_Flying.generated.h"

/**
 * 
 */
UCLASS(abstract)
class LOKAGAME_API ACharacterAbility_Flying : public ACharacterAbility
{
	GENERATED_BODY()
	
public:

	ACharacterAbility_Flying();
	
	virtual bool RequestStartActivate() override;
	virtual bool RequestEndActivate() override;

	virtual void RequestDeactivate(const bool IsDestroy = false) override;
	virtual void PostInitializeComponents() override;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Ability)
	FORCEINLINE float GetFlyPower() const { return FlyPower; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Ability)
	FORCEINLINE float GetFlySpeed() const { return FlySpeed; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Ability)
	FORCEINLINE bool IsApplyUpMove() const { return bIsApplyUpMove; }

	virtual void OnAbilityInput(const EAbilityInput, const bool) override;
	void AttachComponents();

protected:

	virtual void OnRep_IsActivated() override;

	UFUNCTION()
	virtual void OnRep_IsApplyUpMove();

	UPROPERTY(EditDefaultsOnly, Category = Flying)
	float FlyPower;

	UPROPERTY(EditDefaultsOnly, Category = Flying)
	float FlySpeed;

	UPROPERTY(ReplicatedUsing=OnRep_IsApplyUpMove)
	bool bIsApplyUpMove;

	UPROPERTY(VisibleDefaultsOnly, Category = Effects)
	UParticleSystemComponent* AirEngineParticleOne;

	UPROPERTY(VisibleDefaultsOnly, Category = Effects)
	UParticleSystemComponent* AirEngineParticleTwo;

	UPROPERTY(VisibleDefaultsOnly, Category = Effects)
	UParticleSystemComponent* AirEngineBoostParticleOne;

	UPROPERTY(VisibleDefaultsOnly, Category = Effects)
	UParticleSystemComponent* AirEngineBoostParticleTwo;

	UPROPERTY(VisibleDefaultsOnly, Category = Effects)
	UAudioComponent* AirEngineOn;

	UPROPERTY(VisibleDefaultsOnly, Category = Effects)
	UAudioComponent* AirEngineOff;

	UPROPERTY(VisibleDefaultsOnly, Category = Effects)
	UAudioComponent* AirEngineLoop;

	UPROPERTY(VisibleDefaultsOnly, Category = Effects)
	UAudioComponent* AirEngineStart;

	UPROPERTY(VisibleDefaultsOnly, Category = Effects)
	UAudioComponent* AirEngineLoopFlying;

	UPROPERTY(VisibleDefaultsOnly, Category = Effects)
	UAudioComponent* AirEngineStop;

	UPROPERTY(EditDefaultsOnly, Category = Effects)
	FName SocketAirEngineOne;

	UPROPERTY(EditDefaultsOnly, Category = Effects)
	FName SocketAirEngineTwo;

	UPROPERTY(EditDefaultsOnly, Category = Effects)
	FName SocketAirEngineSound;

	UPROPERTY(EditDefaultsOnly, Category = Effects)
	FName AirEngineParticleParameter;

	// * X - Default, Y - Started
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	FVector2D AirEngineParticleValues;
};
