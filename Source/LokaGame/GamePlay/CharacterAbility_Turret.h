// VRSPRO

#pragma once

#include "CharacterAbility_Installable.h"
#include "CharacterAbility_Turret.generated.h"


class AUTPlayerState;
class AShooterImpactEffect;

USTRUCT()
struct FInstantHitInfo
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	FVector Origin;

	UPROPERTY()
	float ReticleSpread;

	UPROPERTY()
	int32 RandomSeed;
};

UCLASS(abstract)
class LOKAGAME_API ACharacterAbility_Turret : public ACharacterAbility_Installable
{
	GENERATED_BODY()
	
public:

	ACharacterAbility_Turret();

	virtual bool RequestEndActivate() override;

	virtual void RequestDeactivate(const bool IsDestroy = false) override;
	virtual void PostInitializeComponents() override;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FORCEINLINE bool GetCanFire() const
	{
		return bCanFire;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FORCEINLINE FRotator GetTurretTargetRotation() const
	{
		return TurretTargetRotation.GetNormalized();
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FORCEINLINE FRotator GetTurretTargetRotationRelative() const
	{
		return (TurretTargetRotation - GetActorRotation()).GetNormalized();
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FORCEINLINE float GetCrankingSpeed() const
	{
		return CrankingSpeed;
	}

protected:

	virtual void OnRep_IsActivated() override;

	UPROPERTY(Replicated)
	FRotator TurretTargetRotation;

	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh)
	USkeletalMeshComponent* TurretMesh;

	//====================================================================================================================================================================[ Turret 

	/** Sphere radius, maximum attack area */
	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	float TriggerSphereRadius;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Gameplay)
	USphereComponent* SphereRadius;

	UPROPERTY(Replicated, BlueprintReadWrite, Category = Gameplay)
	bool bCanFire;

	UPROPERTY(BlueprintReadWrite, Category = Gameplay)
	TArray<ACharacter*> Characters;

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
	virtual void NotifyActorEndOverlap(AActor* OtherActor) override;

	virtual void PostActorCreated() override;

	UFUNCTION(BlueprintCallable, Category = Gameplay)
	void FindTarget();

	void FireTurret();
	void SimulateFireTurret();

	UPROPERTY(EditDefaultsOnly, Category = Fire)
	FVector FireOffset;

	UPROPERTY(EditDefaultsOnly, Category = Fire)
	FVector RotateOffset;

	UPROPERTY(EditDefaultsOnly, Category = Fire)
	TSubclassOf<AShooterImpactEffect> ImpactEffect;

	UPROPERTY(EditDefaultsOnly, Category = Fire)
	UParticleSystem* FireEffect;

	UPROPERTY(EditDefaultsOnly, Category = Fire)
	float FireRate;

	UPROPERTY(EditDefaultsOnly, Category = Fire)
	float Damage;

	UPROPERTY(EditDefaultsOnly, Category = Fire)
	FVector2D YawClamp;

	UPROPERTY(EditDefaultsOnly, Category = Fire)
	FVector2D PitchClamp;

	UFUNCTION()
	void OnRep_HitNotify();

	UPROPERTY()
	ACharacter* TargetCharacter;

	UPROPERTY(Transient, ReplicatedUsing = OnRep_HitNotify)
	FInstantHitInfo HitNotify;

	FTimerHandle timer_HandleFire
		, timer_HandleFindTarget;

	UPROPERTY(EditDefaultsOnly, Category = Fire)
	USoundCue* FireSound;

	UPROPERTY(EditDefaultsOnly, Category = Fire)
	FVector2D FireSpread;

	UPROPERTY(Replicated)
	float CrankingSpeed;

	UPROPERTY(EditDefaultsOnly, Category = Fire)
	float CrankingSpeedToFire;

	UPROPERTY(EditDefaultsOnly, Category = Fire)
	float CrankingSpeedInterp;

	UPROPERTY(EditDefaultsOnly, Category = Fire)
	float RotationInterp;

	UPROPERTY(EditDefaultsOnly, Category = Fire)
	UParticleSystem* TraceFX;

	float CurrentSpread;
};
