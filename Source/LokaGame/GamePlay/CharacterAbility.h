// VRSPRO

#pragma once

#include "GameFramework/Actor.h"
#include "CharacterAbility.generated.h"

//DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAbilityDeactivated, class AShooterCharacter*, DeactivatedBy);

UENUM()
namespace EEAbilityInput
{
	enum Type
	{
		None,
		Fire,
		Aiming,
		Jump,
		End
	};
}

typedef EEAbilityInput::Type EAbilityInput;

UCLASS(abstract)
class LOKAGAME_API ACharacterAbility : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACharacterAbility();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = Ability)
	virtual bool RequestStartActivate();

	UFUNCTION(BlueprintCallable, Category = Ability)
	virtual bool RequestEndActivate();

	UFUNCTION(BlueprintCallable, Category = Ability)
	virtual void RequestDeactivate(const bool IsDestroy = false);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Ability)
	FORCEINLINE bool IsInstallable() const { return bIsInstallable; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Ability)
	FORCEINLINE bool IsAttachable() const { return bIsAttachable; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Ability)
	FORCEINLINE bool IsActivated() const { return bIsActivated; }

	//UPROPERTY(BlueprintAssignable, Category = Ability)
	//FOnAbilityDeactivated OnAbilityDeactivated;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Ability)
	virtual bool IsAllowActivate() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Ability)
	virtual bool IsAllowDeactivate() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Ability)
	float GetAbilityReadyPercent() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Ability)
	float GetAbilityUsePercent() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Ability)
	FORCEINLINE	FText GetAbilityName() const { return Name; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Ability)
	FORCEINLINE	FText GetAbilityDescription() const { return Description; }

	FKey KeyToUse;

	FORCEINLINE const FSlateBrush* GetIcon() const
	{
		return &Icon;
	}

	virtual void OnAbilityInput(const EAbilityInput, const bool);

protected:

	UFUNCTION()
	virtual void OnRep_IsActivated();

	UPROPERTY(EditDefaultsOnly, Category = Ability)
	FText Name;

	UPROPERTY(EditDefaultsOnly, Category = Ability)
	FText Description;

	UPROPERTY(EditDefaultsOnly, Category = Ability)
	bool bIsInstallable;

	UPROPERTY(EditDefaultsOnly, Category = Ability)
	bool bIsAttachable;
	
	UPROPERTY(ReplicatedUsing=OnRep_IsActivated)
	bool bIsActivated;

	UPROPERTY(EditDefaultsOnly, Category = Ability)
	float ActiveTime;

	UPROPERTY(EditDefaultsOnly, Category = Ability)
	float IntervalTime;

	UPROPERTY(EditDefaultsOnly, Category = Ability)
	float IntervalDeactivate;

	UPROPERTY(Replicated, BlueprintReadOnly, Category = Ability)
	float ActivatedTime;

	UPROPERTY(Replicated, BlueprintReadOnly, Category = Ability)
	float DeactivatedTime;	

	UPROPERTY(Replicated, BlueprintReadOnly, Category = Ability)
	float AuthorityTime;

	UPROPERTY(EditDefaultsOnly, Category = Ability)
	float LifeTimeOwnerDeath;

	UPROPERTY(EditDefaultsOnly, Category = Ability)
	FSlateBrush Icon;

	FTimerHandle timer_Deactivate;

	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION(BlueprintImplementableEvent, Category = Ability)
	void OnActiveStateChanged(const bool IsActivated);

	UFUNCTION(BlueprintImplementableEvent, Category = Ability, meta = (DisplayName="OnAbilityInput"))
	void K2_OnAbilityInput(const EEAbilityInput::Type InInput, const bool IsHold);

	UFUNCTION(Server, Reliable, WithValidation)
	void OnAbilityInputServer(const EEAbilityInput::Type InInput, const bool IsHold);

private:

	float AuthorityTimeUpdateInterval;
};
