// VRSPRO

#include "LokaGame.h"
#include "CharacterAbility_Shield.h"

#include "UTCharacter.h"
#include "UTGameUserSettings.h"

ACharacterAbility_Shield::ACharacterAbility_Shield()
	: Super()
{
	ShieldMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ShieldMesh"));
	ShieldMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	RootComponent = ShieldMesh;
	
	ShieldMeshDecor = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ShieldDecorativeMesh"));
	ShieldMeshDecor->SetupAttachment(RootComponent);
	ShieldMeshDecor->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void ACharacterAbility_Shield::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	AttachToActor(GetOwner(), FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	SetActorRelativeTransform(AttachTransform);
	ShieldMesh->SetVisibility(false, true);
}

bool ACharacterAbility_Shield::RequestStartActivate()
{
	if (IsAllowActivate())
	{
		return true;
	}

	return false;
}

bool ACharacterAbility_Shield::RequestEndActivate()
{
	if (IsAllowActivate())
	{
		Super::RequestEndActivate();
		OnRep_IsActivated();

		return true;	
	}

	return false;
}

void ACharacterAbility_Shield::RequestDeactivate(const bool IsDestroy)
{
	Super::RequestDeactivate(IsDestroy);
	OnRep_IsActivated();
}

void ACharacterAbility_Shield::OnRep_IsActivated()
{
	auto MyCharacter = GetValidObject<AUTCharacter, AActor>(GetOwner());
	auto MySettings = Cast<UUTGameUserSettings>(GEngine->GetGameUserSettings());

	if (bIsActivated)
	{
		ShieldMesh->SetVisibility(true, true);		
		ShieldMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
		ShieldMeshDecor->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
		if (MyCharacter && MySettings && MySettings->IsAllowChangeViewByAbility() && GetNetMode() != NM_DedicatedServer)
		{
			MyCharacter->SetIsThirdView(true);
		}
	}
	else
	{
		ShieldMesh->SetVisibility(false, true);
		ShieldMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		ShieldMeshDecor->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		
		if (MyCharacter && MySettings && MySettings->IsAllowChangeViewByAbility() && GetNetMode() != NM_DedicatedServer)
		{
			MyCharacter->SetIsThirdView(false);
		}
	}
}

void ACharacterAbility_Shield::OnAbilityInput(const EAbilityInput InInput, const bool IsHold)
{
	Super::OnAbilityInput(InInput, IsHold);

	if (Role == ROLE_Authority && InInput == EAbilityInput::Fire && bIsActivated)
	{
		RequestDeactivate();
	}
}
