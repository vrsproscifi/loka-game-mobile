// VRSPRO

#pragma once

#include "GamePlay/CharacterAbility.h"
#include "CharacterAbility_Shield.generated.h"

/**
 * 
 */
UCLASS(abstract)
class LOKAGAME_API ACharacterAbility_Shield : public ACharacterAbility
{
	GENERATED_BODY()
	
public:

	ACharacterAbility_Shield();
	
	virtual bool RequestStartActivate() override;
	virtual bool RequestEndActivate() override;

	virtual void RequestDeactivate(const bool IsDestroy = false) override;
	virtual void PostInitializeComponents() override;

	virtual void OnAbilityInput(const EAbilityInput, const bool) override;

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh)
	UStaticMeshComponent* ShieldMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh)
	UStaticMeshComponent* ShieldMeshDecor;

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	FTransform AttachTransform;

	virtual void OnRep_IsActivated() override;
};
