// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once

#include "GameModeTypeId.h"
#include "Interfaces/UTTeamInterface.h"
#include "UTPlayerStart.generated.h"

UCLASS()
class LOKAGAME_API AUTPlayerStart : public APlayerStart, public IUTTeamInterface
{
	GENERATED_BODY()

public:

	AUTPlayerStart(const FObjectInitializer& ObjectInitializer);

	/** if set, this player state will be rated higher if the team is on defense */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerStart)
	bool bDefensiveStart;

	/** pickup associated with this start. If it's a WeaponLocker, the contents will be given to players upon spawning. Otherwise, it can be used as a UI hint for spawn selection */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerStart)
	class AUTPickup* AssociatedPickup;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerStart, meta = (Bitmask, BitmaskEnum = "EBlueprintGameMode"))
	int32 AllowGameModes;

	// * Don't edit on level editor, use "Support GameModes"
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerStart)
	int32 SupportGameModes;	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerStart, meta = (DisplayName="Support GameModes", NoElementDuplicate))
	TArray<TEnumAsByte<EGameModeTypeId::Type>> SupportGameModesFixed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerStart)
	uint8 TeamNum;

	virtual uint8 GetTeamNum() const
	{
		return TeamNum;
	}

	virtual void SetTeamForSideSwap_Implementation(uint8 NewTeamNum)
	{
		if (Role == ROLE_Authority)
		{
			TeamNum = NewTeamNum;
		}
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	bool IsAnyCharacterOverlap() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	bool IsOtherTeamCharacterOverlap() const;

#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

protected:

	UPROPERTY(EditAnywhere, Category = PlayerStart)
	UCapsuleComponent* Capsule_Radius;
};