// VRSPRO

#pragma once

#include "GameFramework/Actor.h"
#include "UsableObject.generated.h"

UCLASS(Abstract)
class LOKAGAME_API AUsableObject : public AActor
{
	GENERATED_BODY()
	
public:	
	AUsableObject();

	UFUNCTION(BlueprintCallable, Category = Gameplay)
	virtual void StartUse(AActor* ObjectInstigator) PURE_VIRTUAL(AUsableObject::StartUse, );

	UFUNCTION(BlueprintCallable, Category = Gameplay)
	virtual void EndUse(AActor* ObjectInstigator) PURE_VIRTUAL(AUsableObject::EndUse, );

	UFUNCTION(BlueprintCallable, Category = Gameplay)
	virtual void StartFocus(AActor* ObjectInstigator) PURE_VIRTUAL(AUsableObject::StartFocus, );

	UFUNCTION(BlueprintCallable, Category = Gameplay)
	virtual void EndFocus(AActor* ObjectInstigator) PURE_VIRTUAL(AUsableObject::EndFocus, );

	UFUNCTION(BlueprintCallable, Category = Gameplay)
	virtual bool IsTimedUse() const
	{
		return false;
	}

	UFUNCTION(BlueprintCallable, Category = Gameplay)
	virtual float GetTimeUse() const
	{
		return .0f;
	}

	UPROPERTY(BlueprintReadOnly, Category = Gameplay)
	FText ObjectName;
};
