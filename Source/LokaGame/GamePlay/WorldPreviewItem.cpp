// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "Item/ItemBaseEntity.h"
#include "Weapon/ItemWeaponEntity.h"
#include "WorldPreviewItem.h"
#include "GameSingleton.h"
#include "Module/ItemModuleEntity.h"
#include "PlayerInventoryItem.h"
#include "PlayerInventoryItemWeapon.h"
#include "Build/ItemBuildEntity.h"
#include "BuildSystem/BuildPlacedComponent.h"

AWorldPreviewItem::AWorldPreviewItem()
	: Super()
{
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	MeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetupAttachment(RootComponent);

	ChildActorComponent = CreateDefaultSubobject<UChildActorComponent>(TEXT("ChildActorComponent"));
	ChildActorComponent->SetupAttachment(RootComponent);

	for (SIZE_T i = 0; i < 3; ++i)
	{
		auto module = CreateDefaultSubobject<USkeletalMeshComponent>(*FString::Printf(TEXT("Module_%d"), i));

		module->SetupAttachment(MeshComponent);
		module->bReceivesDecals = false;
		module->bComponentUseFixedSkelBounds = true;
		module->SetVisibility(false, true);
		module->bEnableUpdateRateOptimizations = true;
		module->SetComponentTickEnabled(false);
		InstalledModulesMesh.Add(module);
	}

	PrimaryActorTick.bCanEverTick = true;
}

void AWorldPreviewItem::InitializeInstance(UPlayerInventoryItem* InInstance, const bool IsControlled)
{
	ChildActorComponent->DestroyChildActor();

	MeshComponent->SetRelativeLocation(FVector::ZeroVector);
	ChildActorComponent->SetRelativeLocation(FVector::ZeroVector);

	Instance = InInstance;

	if (Cast<UPlayerInventoryItemWeapon>(Instance))
	{
		InitializeAsWeapon();
	}
	else if (Instance)
	{
		if (auto TargetAsBuild = Instance->GetEntity<UItemBuildEntity>())
		{
			MeshComponent->SetSkeletalMesh(nullptr);

			ChildActorComponent->SetChildActorClass(TargetAsBuild->ComponentTemplate.Get());
			ChildActorComponent->CreateChildActor();

			if (auto ChildActor = Cast<ABuildPlacedComponent>(ChildActorComponent->GetChildActor()))
			{
				ChildActor->PostInitializeComponents();
				ChildActor->InitializeInstance(Instance);
			}
		}
		else
		{
			MeshComponent->SetSkeletalMesh(Instance->GetPreviewMesh());
		}

		InitializeModules();
	}
}

void AWorldPreviewItem::InitializeInstance(UItemBaseEntity* InInstance, const bool IsControlled)
{
	if (InInstance && InInstance->IsValidLowLevel())
	{
		Instance = NewObject<UPlayerInventoryItem>(this, InInstance->GetInventoryClass());
		Instance->SetItemEntity(InInstance);

		InitializeInstance(Instance, IsControlled);
	}
}

void AWorldPreviewItem::InitializeModules()
{
	for (SIZE_T i = 0; i < 3; ++i)
	{
		if (InstalledModulesMesh[i]->IsVisible())
		{
			InstalledModulesMesh[i]->SetVisibility(false, true);
		}
	}
}

void AWorldPreviewItem::InitializeAsWeapon()
{
	for (SIZE_T i = 0; i < 3; ++i)
	{
		InstalledModulesMesh[i]->SetVisibility(false, true);
		InstalledModulesMesh[i]->SetSkeletalMesh(nullptr);
	}

	auto WeaponInstance = Cast<UPlayerInventoryItemWeapon>(Instance);
	auto EntityInstance = WeaponInstance ? WeaponInstance->GetEntityWeapon() : nullptr;
	if (EntityInstance)
	{
		MeshComponent->SetSkeletalMesh(EntityInstance->GetSkeletalMesh());
		MeshComponent->SetAnimInstanceClass(EntityInstance->GetAnimClass());
		MeshComponent->SetMaterial(0, EntityInstance->GetMaterialByModel(Instance->GetInstalledMaterialModelId()));

		for (auto &m : Instance->GetInstalledAddons())
		{
			UItemModuleEntity* localModule = m ? m->GetEntity<UItemModuleEntity>() : nullptr;
			if (localModule)
			{
				for (SIZE_T i = 0; i < 3; ++i)
				{
					if (!InstalledModulesMesh[i]->IsVisible())
					{
						InstalledModulesMesh[i]->AttachToComponent(MeshComponent, FAttachmentTransformRules::SnapToTargetIncludingScale, localModule->TargetSocket);
						InstalledModulesMesh[i]->SetSkeletalMesh(localModule->GetSkeletalMesh());
						InstalledModulesMesh[i]->CastShadow = true;
						InstalledModulesMesh[i]->SetVisibility(true, true);
						InstalledModulesMesh[i]->SetComponentTickEnabled(true);
						break;
					}
				}
			}
		}
	}
}

void AWorldPreviewItem::AddRotationToMesh(const FRotator& InDelta)
{
	MeshComponent->AddRelativeRotation(InDelta);
}

void AWorldPreviewItem::ForceTextureStream()
{
	MeshComponent->PrestreamTextures(2.1f, false);

	for (SIZE_T i = 0; i < 3; ++i)
	{
		if (InstalledModulesMesh[i]->IsVisible())
		{
			InstalledModulesMesh[i]->PrestreamTextures(2.1f, false);
		}
	}
}

void AWorldPreviewItem::CalculateAndApplyCenter()
{
	MeshComponent->UpdateBounds();
	MeshComponent->SetRelativeLocation(-MeshComponent->Bounds.Origin);

	if (auto ChildActor = ChildActorComponent->GetChildActor())
	{
		FVector aOrigin;
		FVector aExtent;
		ChildActor->GetActorBounds(false, aOrigin, aExtent);
		ChildActorComponent->SetRelativeLocation(-aOrigin);
	}
}

AActor* AWorldPreviewItem::GetChildActor() const
{
	return ChildActorComponent->GetChildActor();
}

void AWorldPreviewItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

