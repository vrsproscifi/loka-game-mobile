// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.
#pragma once

#ifndef __LOKAGAME_H__
#define __LOKAGAME_H__

#include "Templates/Casts.h"

//============================================================================
#define ENUM_FORWARD_DECLARATION(EnumName) namespace EnumName { enum Type: int32; }
#define ENUMCLASS_FORWARD_DECLARATION(EnumName) enum class EnumName : uint8;

//============================================================================
template <typename TObject>
TObject* GetValidObject(TObject* object)
{
	if(object && object->IsValidLowLevel())
	{
		return object;
	}
	return nullptr;
}

template <typename TTo, typename TFrom>
TTo* GetValidObject(TFrom* object)
{
	if (object && object->IsValidLowLevel())
	{
		return Cast<TTo>(object);
	}
	return nullptr;
}

//============================================================================

#define Varible_Check(object)		(object && object != nullptr)
#define Varible_Remove(object)		\
	delete object; \
	object = nullptr;

#define PLACE_WIDGET(_Widget, _Zorder)			GEngine->GameViewport->AddViewportWidgetContent(_Widget, _Zorder)
#define DEL_WIDGET(_Widget)						GEngine->GameViewport->RemoveViewportWidgetContent(_Widget)
////

#define MAKE_UTF8_SYMBOL(Name, Symbol)\
const TCHAR Name[] = { static_cast<TCHAR>(Symbol), 0 };

#define ASSIGN_UTF8_SYMBOL(Variable, Symbol)\
const TCHAR _##Variable[] = { static_cast<TCHAR>(Symbol), 0 };\
Variable = FString(_##Variable);

#define GET_PERCENT_OF(Value, Percent)\
float(Value) / 100.0f * Percent

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	Engine Includes
#include "Engine.h"
#include "Net/UnrealNetwork.h"

#include "Core.h"
#include "Platform.h"
#include "CoreUObject.h"
#include "ModuleManager.h"
#include "Containers/LokaGuid.h"
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	Slate Includes
#include "SlateCore.h"
#include "SlateBasics.h"
#include "SlateExtras.h"
#include "Slate/LokaStyle.h"
#include <OnlineJsonSerializer.h>


#include "LokaGameClasses.h"


#include "ParticleDefinitions.h"
#include "SlateStyleManager.h"

LOKAGAME_API DECLARE_LOG_CATEGORY_EXTERN(UT, Log, All);
LOKAGAME_API DECLARE_LOG_CATEGORY_EXTERN(UTNet, Log, All);
LOKAGAME_API DECLARE_LOG_CATEGORY_EXTERN(UTLoading, Log, All);

#define COLLISION_PROJECTILE					ECC_GameTraceChannel1
#define COLLISION_TRACE_WEAPON					ECC_GameTraceChannel2
#define COLLISION_PROJECTILE_SHOOTABLE			ECC_GameTraceChannel3
#define COLLISION_TELEPORTING_OBJECT			ECC_GameTraceChannel4
#define COLLISION_PAWNOVERLAP					ECC_GameTraceChannel5
#define COLLISION_TRACE_WEAPONNOCHARACTER		ECC_GameTraceChannel6
#define COLLISION_TRANSDISK						ECC_GameTraceChannel7

#define COLLISION_PLAYERBUILDING_OBJECT			ECC_GameTraceChannel8
#define COLLISION_PLAYERBUILDING_BOX			ECC_GameTraceChannel9
#define COLLISION_USABLE						ECC_GameTraceChannel10


/** handy response params for world-only checks */
extern FCollisionResponseParams WorldResponseParams;

/** utility to find out if a particle system loops */
extern bool IsLoopingParticleSystem(const UParticleSystem* PSys);

/** utility to detach and unregister a component and all its children */
extern void UnregisterComponentTree(USceneComponent* Comp);

/** utility to retrieve the highest priority physics volume overlapping the passed in primitive */
extern APhysicsVolume* FindPhysicsVolume(UWorld* World, const FVector& TestLoc, const FCollisionShape& Shape);
/** get GravityZ at the given location of the given world */
extern float GetLocationGravityZ(UWorld* World, const FVector& TestLoc, const FCollisionShape& Shape);

/** utility to create a copy of a mesh for outline effect purposes
* the mesh is initialized but not registered
*/
extern UMeshComponent* CreateCustomDepthOutlineMesh(UMeshComponent* Archetype, AActor* Owner);

/** workaround for FCanvasIcon not having a constructor you can pass in the values to */
FORCEINLINE FCanvasIcon MakeCanvasIcon(UTexture* Tex, float InU, float InV, float InUL, float InVL)
{
	FCanvasIcon Result;
	Result.Texture = Tex;
	Result.U = InU;
	Result.V = InV;
	Result.UL = InUL;
	Result.VL = InVL;
	return Result;
}


/** returns asset data for all assets of the specified class
* do not use for Blueprints as you can only query for all blueprints period; use GetAllBlueprintAssetData() to query the blueprint's underlying class
* if bRequireEntitlements is set, assets on disk for which no local player has the required entitlement will not be returned
*
* WARNING: the asset registry does a class name search not a path search so the returned assets may not actually be the class you want in the case of name conflicts
*			if you load any returned assets always verify that you got back what you were expecting!
*/
extern LOKAGAME_API void GetAllAssetData(UClass* BaseClass, TArray<FAssetData>& AssetList, bool bRequireEntitlements = true);

/** returns asset data for all blueprints of the specified base class in the asset registry
* this does not actually load assets, so it's fast in a cooked build, although the first time it is run
* in an uncooked build it will hitch while scanning the asset registry
* if bRequireEntitlements is set, assets on disk for which no local player has the required entitlement will not be returned
*/
extern LOKAGAME_API void GetAllBlueprintAssetData(UClass* BaseClass, TArray<FAssetData>& AssetList, bool bRequireEntitlements = true);

/** timer manipulation for UFUNCTIONs that doesn't require a timer handle */
extern LOKAGAME_API void SetTimerUFunc(UObject* Obj, FName FuncName, float Time, bool bLooping = false);
extern LOKAGAME_API bool IsTimerActiveUFunc(UObject* Obj, FName FuncName, float* TotalTime = NULL, float* ElapsedTime = NULL);
extern LOKAGAME_API void ClearTimerUFunc(UObject* Obj, FName FuncName);

class UBehaviorTreeComponent;

DECLARE_LOG_CATEGORY_EXTERN(LogShooter, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(LogShooterWeapon, Log, All);

DECLARE_LOG_CATEGORY_EXTERN(LogEntityItem, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(LogEntityPlayer, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(LogBuildSystem, Log, All);

/** when you modify this, please note that this information can be saved with instances
* also DefaultEngine.ini [/Script/Engine.CollisionProfile] should match with this list **/
#define COLLISION_WEAPON			ECC_GameTraceChannel1
#define COLLISION_PICKUP			ECC_GameTraceChannel3

#define MAX_PLAYER_NAME_LENGTH 16


/** Set to 1 to pretend we're building for console even on a PC, for testing purposes */
#define SHOOTER_SIMULATE_CONSOLE_UI	0

#if PLATFORM_PS4 || PLATFORM_XBOXONE || SHOOTER_SIMULATE_CONSOLE_UI
#define SHOOTER_CONSOLE_UI 1
#else
#define SHOOTER_CONSOLE_UI 0
#endif


#endif

class FLokaGameModule : public FDefaultGameModuleImpl
{
public:

	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};

template<typename TEnum>
static FORCEINLINE FString GetEnumValueToString(const FString& Name, TEnum Value)
{
	if (const UEnum* enumPtr = FindObject<UEnum>(ANY_PACKAGE, *Name, true))
	{
		return enumPtr->GetNameStringByIndex(static_cast<int32>(Value));
	}
	return FString("Invalid");
}

struct FFlagsHelper
{
	template<typename VariableType, typename FlagType>
	static FORCEINLINE bool HasAnyFlags(const VariableType Variable, const FlagType& InFlags)
	{
		return !!(Variable & VariableType(InFlags));
	}

	template<typename VariableType, typename FlagType>
	static FORCEINLINE bool HasAllFlags(const VariableType Variable, const FlagType& InFlags)
	{
		return ((Variable & InFlags) == InFlags);
	}

	template<typename VariableType, typename FlagType>
	static FORCEINLINE void SetFlags(VariableType& Variable, const FlagType& FlagsToSet)
	{
		Variable |= VariableType(FlagsToSet);
	}

	template<typename VariableType, typename FlagType>
	static FORCEINLINE void ToggleFlags(VariableType& Variable, const FlagType& FlagsToSet)
	{
		if (HasAnyFlags<VariableType, FlagType>(Variable, FlagsToSet))
		{
			ClearFlags<VariableType, FlagType>(Variable, FlagsToSet);
		}
		else
		{
			SetFlags<VariableType, FlagType>(Variable, Variable | FlagsToSet);
		}
	}

	template<typename VariableType, typename FlagType>
	static FORCEINLINE void ClearFlags(VariableType& Variable, const FlagType& FlagsToClear)
	{
		Variable &= ~VariableType(FlagsToClear);
	}

	template<typename VariableType>
	static FORCEINLINE VariableType ToFlag(const VariableType& Variable)
	{
		return 1 << Variable;
	}
};

//============================================================================
template <typename TObject>
TObject* GetWorldGameState(UWorld* world)
{
	if (auto w = GetValidObject(world))
	{
		if (auto gs = GetValidObject(w->GetGameState<TObject>()))
		{
			return gs;
		}
	}
	return nullptr;
}

//============================================================================
class UUTGameViewportClient;
extern UUTGameViewportClient* GetGameViewportClient();