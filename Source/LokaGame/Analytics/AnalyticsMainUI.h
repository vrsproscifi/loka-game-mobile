// VRSPRO

#pragma once

#include "AnalyticsMainUI.generated.h"

UENUM(Blueprintable, Category = Analytics)
namespace ELobbyMainAnalytic
{
	enum Type
	{
		SlideGeneral,
		SlideShop,
		SlideHangar,
		SlideLeague,
		SlideTournament,
		SlideAchievements,
		Contacts,
		Chat,
		Notifications,
		Settings,
		End
	};
}