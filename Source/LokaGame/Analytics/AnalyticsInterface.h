// VRSPRO

#pragma once

#include "AnalyticsInterface.generated.h"

UENUM()
namespace EAnalyticCategory
{
	enum Type
	{
		Main,					// Main actions including Slide switches and radio buttons on bottom panel and top panel with tooltips
		SlideGeneral,			// General slide actions
		SlideShop,				// Shop slide actions
		SlideHangar,			// Hangar slide actions
		End
	};
}

USTRUCT(Blueprintable, Category = Analytics)
struct FAnalyticAction
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
	int32 Category;

	UPROPERTY(EditAnywhere)
	int32 Action;

	UPROPERTY(EditAnywhere)
	int32 Duration;

	void Start()
	{
		Duration = FDateTime::UtcNow().ToUnixTimestamp();
	}

	void Stop()
	{
		Duration = FDateTime::UtcNow().ToUnixTimestamp() - Duration;
	}

	FAnalyticAction() : Category(INDEX_NONE), Action(INDEX_NONE), Duration(INDEX_NONE) {}
	FAnalyticAction(const int32& InCategory, const int32& InAction) : Category(InCategory), Action(InAction), Duration(INDEX_NONE) {}
};

class FLokaAnalytics
{
public:

	FLokaAnalytics();
	static FLokaAnalytics* Get();

	TSharedRef<FAnalyticAction> MakeAction(const FAnalyticAction& InAction);
	TSharedRef<FAnalyticAction> MakeActionAndStart(class FLokaAnalyticsHelper* InOwner, const FAnalyticAction& InAction);
	void ClearActions();
	TArray<TSharedRef<FAnalyticAction>> GetActions() const;

protected:

	TArray<TSharedPtr<FAnalyticAction>> Actions;
};

class FLokaAnalyticsHelper
{
public:

	void StartAnalyticAction(TSharedRef<FAnalyticAction> InAction);
	bool StopAnalyticAction(TSharedRef<FAnalyticAction> InAction);
	bool StopAnalyticAction(const int32& InAction);

protected:

	TMap<int32, TSharedRef<FAnalyticAction>> AnalyticsMap;
};