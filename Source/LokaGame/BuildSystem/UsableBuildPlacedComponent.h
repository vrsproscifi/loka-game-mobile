// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BuildSystem/BuildPlacedComponent.h"
#include "Interfaces/UsableActorInterface.h"
#include "ServiceController/BuildingController/Models/WorldBuildingItemView.h"
#include "UsableBuildPlacedComponent.generated.h"

class SVerticalMenuBuilder;

UENUM(BlueprintType)
namespace EExecuteActionType
{
	enum Type
	{
		None,
		Harvest,
		End
	};
}

UCLASS(abstract, meta = (DeprecatedNode, DeprecationMessage = "This class are deprecated and remove after LOKA 0.52/0.51 version."))
class LOKAGAME_API AUsableBuildPlacedComponent 
	: public ABuildPlacedComponent
	, public IUsableActorInterface
{
	GENERATED_BODY()
	
public:
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Building)
	TMap<int32, FText> GetAvaliableActionsMap() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Building)
	void OnActionsMenuClicked(const int32 InNumber);

	UFUNCTION(BlueprintCallable, Category = Building)
	void ToggleActionsMenu(const bool InToggle);

	UFUNCTION(BlueprintCallable, Category = Building)
	void ExecuteAction(const TEnumAsByte<EExecuteActionType::Type>& InAction);

	// IUsableActorInterface Begin
	virtual void OnFocusStart_Implementation(AActor* InInstigator) override {}
	virtual void OnFocusLost_Implementation(AActor* InInstigator) override {}
	virtual void OnInteractStart_Implementation(AActor* InInstigator) override {}
	virtual void OnInteractLost_Implementation(AActor* InInstigator) override {}
	// IUsableActorInterface End

	virtual void OnComponentPlaced_Implementation(const bool IsInAttack = false) override;

protected:

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Building)
	void OnActionExecuted(EExecuteActionType::Type InAction);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Building)
	void OnActionExecuteFailed(EExecuteActionType::Type InAction, const int32 ResponseCode);

	UPROPERTY(BlueprintReadOnly, Category = Building)
	TMap<FName, int32> OnActionExecutedData;

	UPROPERTY(BlueprintReadOnly, Category = Building)
	TMap<int32, int32> MapToMap;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Building)
	TMap<int32, FText> ActionsList;

	static TSharedPtr<SVerticalMenuBuilder> Slate_VerticalMenuBuilder;

	void CreateMenu();
	void OnMenuClicked(uint8);
	void OnHarvestExecuted(const FWorldBuildingStorageView& InData);
	void OnHarvestExecuteFailed(const int32 InCode);

	
};
