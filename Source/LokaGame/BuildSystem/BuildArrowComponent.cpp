// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "BuildArrowComponent.h"

UBuildArrowComponent::UBuildArrowComponent()
	: Super()
{
	SetCollisionProfileName(TEXT("NoCollision"));
	ArrowColor = FColor::Orange;

	bIsFaceArrow = false;
}

bool UBuildArrowComponent::IsAllowedComponnent(const int32& InFlags) const
{
	return FFlagsHelper::HasAnyFlags(AllowedComponnents, InFlags);
}

void UBuildArrowComponent::CopyExtraDataFrom(const UBuildArrowComponent* InOther)
{
	if(const auto other = GetValidObject(InOther))
	{
		AllowedComponnents = other->AllowedComponnents;
		bIsFaceArrow = other->bIsFaceArrow;
	}
}