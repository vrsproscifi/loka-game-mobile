// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PlayerPreSetId.h"
#include "Interfaces/UsableActorInterface.h"
#include "BuildSystem/BuildPlacedComponent.h"
#include "Models/LobbyClientPreSetData.h"
#include "ProfilePlacedComponent.generated.h"


// Key: Profile, Value: Guid

class SInventoryContainer;
class AWorldPreviewItem;
class ABasePlayerState;
class SRadialMenu;
class UProfileComponent;
class UInventoryComponent;
/**
 * 
 */
UCLASS()
class LOKAGAME_API AProfilePlacedComponent 
	: public ABuildPlacedComponent
	, public IUsableActorInterface
{
	GENERATED_BODY()
	
public:

	static FString ParameterName_Profile;

	AProfilePlacedComponent();

	// IUsableActorInterface Begin
	virtual void OnFocusStart_Implementation(AActor* InInstigator) override {}
	virtual void OnFocusLost_Implementation(AActor* InInstigator) override;
	virtual void OnInteractStart_Implementation(AActor* InInstigator) override;
	virtual void OnInteractLost_Implementation(AActor* InInstigator) override;
	// IUsableActorInterface End
	
	UFUNCTION(BlueprintCallable, Category = Controll)
	void InitializeProfile(const TEnumAsByte<EPlayerPreSetId::Type> InProfile);

	UFUNCTION(BlueprintCallable, Category = System)
	void SaveProfile();

	// Controlls
	void OnAddPitch(float InValue);
	void OnAddYaw(float InValue);

	template<bool Value>
	void OnScrollArmLen_Helper() { OnScrollArmLen(Value); }
	void OnScrollArmLen(const bool InIsDown);

	UFUNCTION(BlueprintNativeEvent, Category = Controll)
	void OnSettingsEntry();

	UFUNCTION(BlueprintNativeEvent, Category = Controll)
	void OnSettingsLeave();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Controll)
	FORCEINLINE bool IsInSettingsMode() const { return bInSettingsMode; }

	virtual void Destroyed() override;

protected:

	UProfileComponent* GetOwnerProfileComponent() const;
	UInventoryComponent* GetOwnerInventoryComponent() const;
	virtual void OnDynamicPropertyDataUpdate_Implementation() override;

	TSharedPtr<SRadialMenu> Slate_RadialMenu;
	TSharedPtr<SInventoryContainer> Slate_InventoryContainer;

	FTimerHandle timer_DestroyMenu, timer_ReUpdate;

	bool TryCreateMenu();
	bool TryCreateInventory();
	void OnDestroyMenu();
	void OnDestroyInventory();

	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(VisibleAnywhere, Category = Visual)
	USkeletalMeshComponent* CharacterMesh;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Visual)
	USpringArmComponent* SpringArmComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Visual)
	UCameraComponent* CameraComponent;

	UFUNCTION(Reliable, Server, WithValidation)
	void ServerChangeProfile(const int32 InProfile);

	UFUNCTION()
	void OnRep_CurrentProfile();

	UPROPERTY(BlueprintReadWrite, Category = Profile, ReplicatedUsing=OnRep_CurrentProfile)
	TEnumAsByte<EPlayerPreSetId::Type> CurrentProfile;

	UPROPERTY(BlueprintReadWrite, Category = Profile, ReplicatedUsing = OnRep_CurrentProfile)
	uint8 NeedRefreshCounter;

	UPROPERTY(BlueprintReadWrite, Category = Profile)
	TEnumAsByte<EPlayerPreSetId::Type> LastProfile;

	UPROPERTY()
	AWorldPreviewItem* PrimaryWeapon;

	UPROPERTY()
	AWorldPreviewItem* SecondaryWeapon;

	UPROPERTY()
	bool bInSettingsMode;

	UPROPERTY()
	ABasePlayerState* LastUser;

	UPROPERTY()
	FClientPreSetData LastData;
};
