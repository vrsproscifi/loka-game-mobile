// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BuildSystem/WarehousePlacedComponent.h"
#include "MinerPlacedComponent.generated.h"

/**
 * 
 */
UCLASS()
class LOKAGAME_API AMinerPlacedComponent 
	: public AWarehousePlacedComponent
{
	GENERATED_BODY()
	
public:

	static FString ParameterName_Performance;

	AMinerPlacedComponent();

	// IUsableActorInterface Begin
	virtual void OnFocusStart_Implementation(AActor* InInstigator) override {}
	virtual void OnFocusLost_Implementation(AActor* InInstigator) override {}
	virtual void OnInteractStart_Implementation(AActor* InInstigator) override;
	virtual void OnInteractLost_Implementation(AActor* InInstigator) override;
	// IUsableActorInterface End

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Storage)
	FORCEINLINE FIntPoint GetPerformance() const { return Performance; }

	UFUNCTION(Client, Reliable)
	void ClientHarvestSuccess(const uint64& harvested);

	UFUNCTION(Client, Reliable)
	void ClientHarvestNotEnouthSpace();

protected:
	
	virtual void OnDynamicPropertyDataUpdate_Implementation() override;
	virtual void OnRep_Storage_Implementation() override;

	UPROPERTY(ReplicatedUsing = OnRep_Storage)
	FIntPoint Performance;
};
