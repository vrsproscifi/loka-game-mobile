// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "MinerPlacedComponent.h"
#include "ConstructionGameState.h"
#include "ConstructionComponent.h"
#include "SNotifyList.h"
#include "RichTextHelpers.h"

FString AMinerPlacedComponent::ParameterName_Performance = TEXT("Performance");

AMinerPlacedComponent::AMinerPlacedComponent()
	: Super()
{
	Performance = FIntPoint::ZeroValue;
}

void AMinerPlacedComponent::OnInteractStart_Implementation(AActor* InInstigator)
{
	AConstructionGameState* MyGameState = GetWorldGameState<AConstructionGameState>(GetWorld());
	auto MyConstructionComp = MyGameState ? MyGameState->GetConstructionComponent() : nullptr;
	if (MyConstructionComp && MyConstructionComp->IsValidLowLevel())
	{
		MyConstructionComp->SendRequestHarvest(this);
	}
}

void AMinerPlacedComponent::OnInteractLost_Implementation(AActor* InInstigator)
{

	// Or harvest code here
}

void AMinerPlacedComponent::ClientHarvestSuccess_Implementation(const uint64& harvested)
{
	FNotifyInfo NotifyInfo;
	
	auto money = FRichHelpers::FResourceHelper().SetValue(harvested).SetType(EResourceTextBoxType::Money);
	NotifyInfo.Title = NSLOCTEXT("AMinerPlacedComponent", "Notify.Harvest.Title", "Harvest");
	NotifyInfo.Content = FText::Format(NSLOCTEXT("AMinerPlacedComponent", "Notify.Harvest.Desc", "You have successfully harvested {0}. Come back again, so as not to lose your earnings!"), money.ToText());
	SNotifyList::Get()->AddNotify(NotifyInfo);
}

void AMinerPlacedComponent::ClientHarvestNotEnouthSpace_Implementation()
{
	FNotifyInfo NotifyInfo(NSLOCTEXT("AMinerPlacedComponent", "Notify.Harvest.NotEnouthSpace.Desc", "Failed processed of harvest, not enough free space on warehouses."), NSLOCTEXT("AMinerPlacedComponent", "Notify.NotEnouthSpace.Harvest.Title", "Harvest"));
	SNotifyList::Get()->AddNotify(NotifyInfo);
}

void AMinerPlacedComponent::OnDynamicPropertyDataUpdate_Implementation()
{
	Super::OnDynamicPropertyDataUpdate_Implementation();

	Performance = GetDynamicPropertyData(ParameterName_Performance).ToIntPoint();
	OnRep_Storage();
}

void AMinerPlacedComponent::OnRep_Storage_Implementation()
{
	Super::OnRep_Storage_Implementation();
}

void AMinerPlacedComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AMinerPlacedComponent, Performance);
}
