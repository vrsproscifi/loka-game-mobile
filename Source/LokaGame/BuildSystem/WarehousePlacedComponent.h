// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BuildSystem/BuildPlacedComponent.h"
#include "Interfaces/UsableActorInterface.h"
#include "WarehousePlacedComponent.generated.h"


/**
 * 
 */
UCLASS()
class LOKAGAME_API AWarehousePlacedComponent 
	: public ABuildPlacedComponent
	, public IUsableActorInterface
{
	GENERATED_BODY()
	
public:

	static FString ParameterName_Storage;

	AWarehousePlacedComponent();

	// IUsableActorInterface Begin
	virtual void OnFocusStart_Implementation(AActor* InInstigator) override {}
	virtual void OnFocusLost_Implementation(AActor* InInstigator) override {}
	virtual void OnInteractStart_Implementation(AActor* InInstigator) override;
	virtual void OnInteractLost_Implementation(AActor* InInstigator) override;
	// IUsableActorInterface End

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Storage)
	FORCEINLINE FIntPoint GetStorage() const { return Storage; }

protected:
	
	virtual void OnDynamicPropertyDataUpdate_Implementation() override;
	
	UFUNCTION(BlueprintNativeEvent)
	void OnRep_Storage();

	UPROPERTY(ReplicatedUsing = OnRep_Storage)
	FIntPoint Storage;
};
