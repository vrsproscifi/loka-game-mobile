// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "BuildBoxComponent.h"


UBuildBoxComponent::UBuildBoxComponent()
	: Super()
{
	SetCollisionProfileName(TEXT("PlayerBuildingBox"));

	bIsFoundationChecker = false;
	bIsOverlapChecker = false;
	bIsAlwaysFindTerrain = true;
}

bool UBuildBoxComponent::IsAllowedComponnent(const int32& InFlags) const
{
	return FFlagsHelper::HasAnyFlags(AllowedComponnents, InFlags);
}

void UBuildBoxComponent::CopyExtraDataFrom(const UBuildBoxComponent* InOther)
{
	if (InOther)
	{
		AllowedComponnents = InOther->AllowedComponnents;
		bIsFoundationChecker = InOther->bIsFoundationChecker;
		bIsOverlapChecker = InOther->bIsOverlapChecker;
		bIsAlwaysFindTerrain = InOther->bIsAlwaysFindTerrain;
	}
}