// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SVerticalMenuBuilder.h"
#include "UTPlayerController.h"
#include "UTGameViewportClient.h"
#include "UsableBuildPlacedComponent.h"


TSharedPtr<SVerticalMenuBuilder> AUsableBuildPlacedComponent::Slate_VerticalMenuBuilder;

void AUsableBuildPlacedComponent::OnComponentPlaced_Implementation(const bool IsInAttack)
{
	Super::OnComponentPlaced_Implementation(IsInAttack);

	if (IsInAttack == false)
	{
		//buildingController.OnHarvest.OnSuccess.AddUObject(this, &AUsableBuildPlacedComponent::OnHarvestExecuted);
		//buildingController.OnHarvest.OnFailed.AddUObject(this, &AUsableBuildPlacedComponent::OnHarvestExecuteFailed, 500);
		//buildingController.OnHarvest.OnNotEnouthSpace.Handler.AddUObject(this, &AUsableBuildPlacedComponent::OnHarvestExecuteFailed, 400);
	}
}

void AUsableBuildPlacedComponent::ToggleActionsMenu(const bool InToggle)
{
	if (InToggle)
	{
		CreateMenu();

		AUsableBuildPlacedComponent::Slate_VerticalMenuBuilder->ToggleWidget(true);
	}
	else
	{
		AUsableBuildPlacedComponent::Slate_VerticalMenuBuilder->ToggleWidget(false);
	}
}

void AUsableBuildPlacedComponent::CreateMenu()
{
	if (AUsableBuildPlacedComponent::Slate_VerticalMenuBuilder.IsValid() == false)
	{
		SAssignNew(AUsableBuildPlacedComponent::Slate_VerticalMenuBuilder, SVerticalMenuBuilder);

		if (GEngine && GEngine->GameViewport)
		{
			if (auto MyViewport = Cast<UUTGameViewportClient>(GEngine->GameViewport))
			{
				MyViewport->AddUsableViewportWidgetContent(AUsableBuildPlacedComponent::Slate_VerticalMenuBuilder.ToSharedRef(), 1 + UUTGameViewportClient::ZOrder_CheckClass);
			}
		}
	}	

	MapToMap.Empty();

	TArray<FText> ActionsText;
	auto ActionsMap = GetAvaliableActionsMap();

	for (auto action : ActionsMap)
	{
		MapToMap.Add(ActionsText.Add(action.Value), action.Key);
	}

	AUsableBuildPlacedComponent::Slate_VerticalMenuBuilder->SetButtons(ActionsText);
	AUsableBuildPlacedComponent::Slate_VerticalMenuBuilder->OnClickAnyButton.BindUObject(this, &AUsableBuildPlacedComponent::OnMenuClicked);
}

TMap<int32, FText> AUsableBuildPlacedComponent::GetAvaliableActionsMap_Implementation() const
{
	return ActionsList;
}

void AUsableBuildPlacedComponent::OnActionsMenuClicked_Implementation(const int32 InNumber)
{

}

void AUsableBuildPlacedComponent::OnMenuClicked(uint8 InNumber)
{
	if (AUsableBuildPlacedComponent::Slate_VerticalMenuBuilder.IsValid() == true && MapToMap.Contains(InNumber))
	{
		OnActionsMenuClicked(MapToMap[InNumber]);
	}
}

void AUsableBuildPlacedComponent::ExecuteAction(const TEnumAsByte<EExecuteActionType::Type>& InAction)
{
	//switch (InAction)
	//{
	//	case EExecuteActionType::Harvest: buildingController.OnHarvest.Request(GUID.ToString()); break;
	//}	
}

void AUsableBuildPlacedComponent::OnActionExecuted_Implementation(EExecuteActionType::Type InAction)
{

}

void AUsableBuildPlacedComponent::OnActionExecuteFailed_Implementation(EExecuteActionType::Type InAction, const int32 ResponseCode)
{

}

void AUsableBuildPlacedComponent::OnHarvestExecuted(const FWorldBuildingStorageView& InData)
{
	OnActionExecutedData.Empty();
	OnActionExecutedData.Add(TEXT("Storage"), InData.Storage);

	OnActionExecuted(EExecuteActionType::Harvest);
}

void AUsableBuildPlacedComponent::OnHarvestExecuteFailed(const int32 InCode)
{
	OnActionExecuteFailed(EExecuteActionType::Harvest, InCode);
}