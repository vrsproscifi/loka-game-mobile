// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/SphereComponent.h"
#include "BuildSystemData.h"
#include "BuildSphereComponent.generated.h"

class UBuildBoxComponent;
class ABuildPlacedComponent;

/**
 * BuildSphere is snap point as socket
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class LOKAGAME_API UBuildSphereComponent : public USphereComponent
{
	GENERATED_BODY()
	
public:

	UBuildSphereComponent();

	UPROPERTY(EditDefaultsOnly, Category = Type, Meta = (Bitmask, BitmaskEnum = "EBuildComponentType"))
	int32 AllowedComponents;

	UPROPERTY(EditDefaultsOnly, Category = Type, Meta = (Bitmask, BitmaskEnum = "EBuildSphereFlag"))
	int32 AllowedSnaps;
	
	UPROPERTY(EditDefaultsOnly, Category = Type)
	TEnumAsByte<EBuildSphereFlag::Type> SnapType;
	
	UPROPERTY()
	ABuildPlacedComponent* SnappedObject;

	UFUNCTION(BlueprintCallable, Category = Construction)
	void CopyExtraDataFrom(const UBuildSphereComponent* InOther);
};
