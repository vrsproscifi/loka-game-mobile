// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BuildSystemData.generated.h"

UENUM(BlueprintType, Meta = (Bitflags))
namespace EBuildComponentType
{
	enum Type
	{
		None,
		Foundation,
		Wall,
		Floor,
		Pillar,
		Fence,
		Stair,
		Door,
		Window,
		Road,
		Defend,
		Decorative,
		CustomType_1,
		CustomType_2,
		CustomType_3,
		CustomType_4,
		CustomType_5,
		CustomType_6,
		CustomType_7,
		CustomType_8,
	};
}
ENUM_CLASS_FLAGS(EBuildComponentType::Type)

// * Used in sphere snap point for perfect snapping.
UENUM(BlueprintType, Meta = (Bitflags))
namespace EBuildSphereFlag
{
	enum Type
	{
		None,
		Option_A,
		Option_B,
		Option_C,
		Option_D,
		Option_E,
		Option_F,
		Option_G,
		Option_H,
		Option_I
	};
}
ENUM_CLASS_FLAGS(EBuildSphereFlag::Type)