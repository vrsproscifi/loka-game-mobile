// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"

class UPlayerInventoryItem;

DECLARE_DELEGATE_OneParam(FOnInventoryItemDrop, UPlayerInventoryItem*);

class LOKAGAME_API SInventoryItemDropTarget : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SInventoryItemDropTarget)
		: _FilterCategory(INDEX_NONE)
		, _FilterSlot(INDEX_NONE)
	{}
	SLATE_DEFAULT_SLOT(FArguments, Content)
	SLATE_ATTRIBUTE(int32, FilterCategory)
	SLATE_ATTRIBUTE(int32, FilterSlot)
	SLATE_EVENT(FOnGetContent, OnGetContent)
	SLATE_EVENT(FOnInventoryItemDrop, OnInventoryItemDrop)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	virtual bool IsValidItem(UPlayerInventoryItem* InItem) const;

	virtual void SetContent(TSharedRef<SWidget> InContentWidget);

protected:

	enum EIndicatorState
	{
		Inactive,
		Allow,
		Disallow,
		End
	};

	EIndicatorState State;
	TSharedPtr<SBorder> Widget_Container;

	TAttribute<const FSlateBrush*> IndicatorBrush[EIndicatorState::End];
	TAttribute<int32> Attr_Category, Attr_Slot;
	FOnGetContent OnGetContent;
	FOnInventoryItemDrop OnInventoryItemDrop;

	virtual void OnDragEnter(const FGeometry& MyGeometry, const FDragDropEvent& DragDropEvent) override;
	virtual void OnDragLeave(const FDragDropEvent& DragDropEvent) override;

	virtual FReply OnDrop(const FGeometry& MyGeometry, const FDragDropEvent& DragDropEvent) override;
};
