// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SInventoryItemDropTarget.h"
#include "SlateOptMacros.h"
#include "LokaDragDrop.h"
#include "PlayerInventoryItem.h"
#include "Item/ItemBaseEntity.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SInventoryItemDropTarget::Construct(const FArguments& InArgs)
{
	OnGetContent = InArgs._OnGetContent;
	OnInventoryItemDrop = InArgs._OnInventoryItemDrop;
	Attr_Category = InArgs._FilterCategory;
	Attr_Slot = InArgs._FilterSlot;

	ChildSlot
	[
		SAssignNew(Widget_Container, SBorder) // Indicator
		.BorderImage_Lambda([&]()
		{
			return IndicatorBrush[State].Get();
		})
	];

	IndicatorBrush[EIndicatorState::Inactive] = new FSlateColorBrush(FColor::Transparent);
	IndicatorBrush[EIndicatorState::Allow] = new FSlateColorBrush(FColor::Green.WithAlpha(128));
	IndicatorBrush[EIndicatorState::Disallow] = new FSlateColorBrush(FColor::Red.WithAlpha(128));

	State = EIndicatorState::Inactive;

	SetContent(InArgs._Content.Widget);
}

END_SLATE_FUNCTION_BUILD_OPTIMIZATION

bool SInventoryItemDropTarget::IsValidItem(UPlayerInventoryItem* InItem) const
{
	if (InItem && InItem->IsValidLowLevel() && InItem->GetEntity())
	{
		return (Attr_Category.Get() == INDEX_NONE || Attr_Category.Get() == InItem->GetEntity()->GetDescription().CategoryType) 
			&& (Attr_Slot.Get() == INDEX_NONE || Attr_Slot.Get() == InItem->GetEntity()->GetDescription().SlotType);
	}

	return false;
}

void SInventoryItemDropTarget::SetContent(TSharedRef<SWidget> InContentWidget)
{
	Widget_Container->SetContent(InContentWidget);
}

void SInventoryItemDropTarget::OnDragEnter(const FGeometry& MyGeometry, const FDragDropEvent& DragDropEvent)
{
	auto TargetOp = DragDropEvent.GetOperationAs<FLokaDragDropOp>().ToSharedRef();
	auto TargetItem = TargetOp->GetObjectPtr().IsValid() ? Cast<UPlayerInventoryItem>(TargetOp->GetObjectPtr().Get()) : nullptr;

	State = IsValidItem(TargetItem) ? EIndicatorState::Allow : EIndicatorState::Disallow;
}

void SInventoryItemDropTarget::OnDragLeave(const FDragDropEvent& DragDropEvent)
{
	State = EIndicatorState::Inactive;
}

FReply SInventoryItemDropTarget::OnDrop(const FGeometry& MyGeometry, const FDragDropEvent& DragDropEvent)
{
	State = EIndicatorState::Inactive;

	auto TargetOp = DragDropEvent.GetOperationAs<FLokaDragDropOp>().ToSharedRef();
	auto TargetItem = TargetOp->GetObjectPtr().IsValid() ? Cast<UPlayerInventoryItem>(TargetOp->GetObjectPtr().Get()) : nullptr;

	if (IsValidItem(TargetItem))
	{
		TSharedRef<SWidget> ContentWidget = TargetOp->GetSourceAs<SWidget>();
		if (OnGetContent.IsBound())
		{
			ContentWidget = OnGetContent.Execute();
		}

		OnInventoryItemDrop.ExecuteIfBound(TargetItem);
		Widget_Container->SetContent(ContentWidget);
		return FReply::Handled().EndDragDrop();
	}

	return FReply::Unhandled();
}
