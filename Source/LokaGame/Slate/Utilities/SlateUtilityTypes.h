// VRSPRO

#pragma once

#include "WebImageCache.h"

template<class TSlateWidget>
class TStaticSlateWidget
{
public:

	TStaticSlateWidget() {}

	static TSharedRef<TSlateWidget> Get()
	{
		if (!ThisInstance.IsValid() || ThisInstance.ToSharedRef() == SNullWidget::NullWidget)
		{
			SAssignNew(ThisInstance, TSlateWidget);
		}

		return ThisInstance.ToSharedRef();
	}

	static void Reset()
	{
		ThisInstance.Reset();
	}

protected:

	static TSharedPtr<TSlateWidget> ThisInstance;
};

template<class TSlateWidget>
TSharedPtr<TSlateWidget> TStaticSlateWidget<TSlateWidget>::ThisInstance = StaticCastSharedRef<TSlateWidget>(SNullWidget::NullWidget);

template<class TSlateWidget>
class TWidgetSupportQueue
{
public:

	static bool AddQueue(const FName& InIndex, const FOnClickedOutside& InEvent)
	{
		if (!Queue.Contains(InIndex))
		{
			Queue.Add(InIndex, InEvent);

			if (Queue.Num() == 1)
			{
				for (auto &i : Queue)
				{
					i.Value.ExecuteIfBound();
					TSlateWidget::Get()->QueueId = i.Key;
					break;
				}
			}

			return true;
		}

		return false;
	}

	static bool RemoveQueue(const FName& InIndex)
	{
		if (Queue.Contains(InIndex))
		{
			Queue.Remove(InIndex);

			if (TSlateWidget::Get()->QueueId == InIndex)
			{
				TSlateWidget::Get()->ToggleWidget(false);
			}

			if (Queue.Num())
			{
				for (auto &i : Queue)
				{
					i.Value.ExecuteIfBound();
					TSlateWidget::Get()->QueueId = i.Key;
					break;
				}
			}

			return true;
		}

		return false;
	}

	static void ResetQueue()
	{
		Queue.Empty();
	}

	FName GetQueueId() const { return QueueId; }

protected:

	static TMap<FName, FOnClickedOutside> Queue;
	FName QueueId;
};

template<class TSlateWidget>
TMap<FName, FOnClickedOutside> TWidgetSupportQueue<TSlateWidget>::Queue = TMap<FName, FOnClickedOutside>();

#define QueueBegin(TWidget, Index, ...) TWidget::AddQueue(Index, FOnClickedOutside::CreateLambda([&, ##__VA_ARGS__ ]() {
#define QueueEnd }));

struct LOKAGAME_API FPlayerAvatarHelper
{
public:
	FPlayerAvatarHelper() {}

	static void Init();
	static FString GetPlayerWebImageUrl(const FString& InPlayerId);
	static FString GetPlayerWebImageUrl(const FGuid& InPlayerId);
	static TSharedRef<const FWebImage> GetPlayerImage(const FString& InPlayerId);

private:

	static FWebImageCache* PlayerWebImageCache;
	static bool IsInit;
};
