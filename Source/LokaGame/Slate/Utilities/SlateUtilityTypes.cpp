// VRSPRO

#include "LokaGame.h"
#include "SlateUtilityTypes.h"

bool FPlayerAvatarHelper::IsInit = false;
FWebImageCache* FPlayerAvatarHelper::PlayerWebImageCache = nullptr;

void FPlayerAvatarHelper::Init()
{
	if (IsInit == false)
	{
		PlayerWebImageCache = new FWebImageCache();
		PlayerWebImageCache->SetDefaultStandInBrush(FLokaStyle::Get().GetBrush(TEXT("Default_NoAvatar")));
		IsInit = true;
	}
}

FString FPlayerAvatarHelper::GetPlayerWebImageUrl(const FString& InPlayerId)
{
	return FString::Printf(TEXT("https://lokagame.com/public/Account/%s_Avatar.png"), *InPlayerId);
}

FString FPlayerAvatarHelper::GetPlayerWebImageUrl(const FGuid& InPlayerId)
{
	return GetPlayerWebImageUrl(InPlayerId.ToString());
}

TSharedRef<const FWebImage> FPlayerAvatarHelper::GetPlayerImage(const FString& InPlayerId)
{
	Init();
	return PlayerWebImageCache->Download(GetPlayerWebImageUrl(InPlayerId));
}