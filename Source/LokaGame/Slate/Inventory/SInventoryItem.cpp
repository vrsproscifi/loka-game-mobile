// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SInventoryItem.h"
#include "SlateOptMacros.h"
#include "SScaleBox.h"

#include "PlayerInventoryItem.h"
#include "Item/ItemBaseEntity.h"
#include "LokaDragDrop.h"
#include "SLokaToolTip.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SInventoryItem::Construct(const FArguments& InArgs, UPlayerInventoryItem* InInstance)
{
	InstancePtr = InInstance;

	ChildSlot
	[
		SNew(SBox).WidthOverride(96).HeightOverride(96)
		[
			SNew(SScaleBox)
			.Stretch(EStretch::ScaleToFit)
			[
				SNew(SImage)
				.Image(this, &SInventoryItem::GetBrush)
			]
		]
	];

	this->SetToolTip(SNew(SLokaToolTip).Text(this, &SInventoryItem::GetName));
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

UItemBaseEntity* SInventoryItem::GetEntity() const
{
	if (InstancePtr.IsValid())
	{
		return InstancePtr.Get()->GetEntity();
	}

	return nullptr;
}

const FSlateBrush* SInventoryItem::GetBrush() const
{
	static FSlateBrush* EmptyBrush = new FSlateNoResource();

	if (auto MyEntity = GetEntity())
	{
		return MyEntity->GetImage();
	}

	return EmptyBrush;
}

FText SInventoryItem::GetName() const
{
	if (auto MyEntity = GetEntity())
	{
		return MyEntity->GetItemName();
	}

	return FText::GetEmpty();
}

FText SInventoryItem::GetDescription() const
{
	if (auto MyEntity = GetEntity())
	{
		return MyEntity->GetItemDescription();
	}

	return FText::GetEmpty();
}

FReply SInventoryItem::OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
	if (MouseEvent.GetEffectingButton() == EKeys::LeftMouseButton)
	{
		return FReply::Handled().DetectDrag(AsShared(), EKeys::LeftMouseButton);
	}

	return FReply::Unhandled();
}

FReply SInventoryItem::OnDragDetected(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
	if (MouseEvent.IsMouseButtonDown(EKeys::LeftMouseButton))
	{
		TSharedRef<FLokaDragDropOp> Operation = MakeShareable(new FLokaDragDropOp(AsShared(), AsShared(), InstancePtr.Get()));

		Operation->SetDecoratorVisibility(true);
		Operation->Construct();

		return FReply::Handled().BeginDragDrop(Operation);
	}

	return FReply::Unhandled();
}
