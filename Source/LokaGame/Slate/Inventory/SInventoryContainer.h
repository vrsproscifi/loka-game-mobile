// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Utilities/SUsableCompoundWidget.h"

class SInventoryItemDropTarget;
class UInventoryComponent;
class UProfileComponent;
class UPlayerInventoryItem;
class SAnimatedBackground;
class SWindowInventory;
class SWindowsContainer;

DECLARE_DELEGATE_TwoParams(FOnDropItemIntoSlot, UPlayerInventoryItem*, const int32&);
/**
 * 
 */
class LOKAGAME_API SInventoryContainer : public SUsableCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SInventoryContainer)
	{}
	SLATE_ARGUMENT(TWeakObjectPtr<UProfileComponent>, ProfileComponent)
	SLATE_ARGUMENT(TWeakObjectPtr<UInventoryComponent>, InventoryComponent)
	SLATE_ATTRIBUTE(int32, ProfileIndex)
	SLATE_EVENT(FOnDropItemIntoSlot, OnDropItemIntoSlot)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	// SUsableCompoundWidget Begin
	virtual void ToggleWidget(const bool) override;
	virtual bool IsInInteractiveMode() const override;
	// SUsableCompoundWidget End

	void OnFillList(const TArray<UPlayerInventoryItem*>& InList);
	void Refresh();

protected:

	UPlayerInventoryItem* GetTargetItem(const int32& InTargetSlot) const;

	FOnDropItemIntoSlot OnDropItemIntoSlot;

	TSharedPtr<SAnimatedBackground> Widget_Animation;
	TSharedPtr<SWindowsContainer> Widget_WindowsContainer;
	TSharedPtr<SWindowInventory> Widget_WindowInventory;

	TSharedPtr<SInventoryItemDropTarget> Widget_PrimaryWeapon;
	TSharedPtr<SInventoryItemDropTarget> Widget_SecondaryWeapon;

	TWeakObjectPtr<UProfileComponent> ProfileCompPtr;
	TWeakObjectPtr<UInventoryComponent> InventoryCompPtr;
	TAttribute<int32> ProfileIndex;
};
