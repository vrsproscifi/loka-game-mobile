// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SInventoryContainer.h"
#include "SlateOptMacros.h"
#include "Windows/SWindowsContainer.h"
#include "SWindowInventory.h"
#include "SAnimatedBackground.h"
#include "Utilities/SInventoryItemDropTarget.h"
#include "Item/ItemSetSlotId.h"
#include "SInventoryItem.h"
#include "ProfileComponent.h"
#include "InventoryComponent.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SInventoryContainer::Construct(const FArguments& InArgs)
{
	ProfileCompPtr = InArgs._ProfileComponent;
	InventoryCompPtr = InArgs._InventoryComponent;
	ProfileIndex = InArgs._ProfileIndex;
	OnDropItemIntoSlot = InArgs._OnDropItemIntoSlot;

	ChildSlot
	[
		SAssignNew(Widget_Animation, SAnimatedBackground)
		.InitAsHide(true)
		.IsEnabledBlur(false)
		.Duration(1.0f)
		.Ease(ECurveEaseFunction::QuadOut)
		.ShowAnimation(EAnimBackAnimation::ZoomOut)
		.HideAnimation(EAnimBackAnimation::ZoomIn)
		[
			SAssignNew(Widget_WindowsContainer, SWindowsContainer)
			.IsNeedQuickPanel(false)
		]
	];

	SAssignNew(Widget_WindowInventory, SWindowInventory, Widget_WindowsContainer.ToSharedRef());

	Widget_WindowsContainer->AddSlot()
	[
		Widget_WindowInventory.ToSharedRef()
	];

	Widget_WindowsContainer->AddSlot()
	[
		SNew(SWindowBase, Widget_WindowsContainer.ToSharedRef())
		.CanClosable(false)
		.CanInvisable(false)
		.InitAsInvisible(false)
		.InitAsClosed(false)
		.TitleText(NSLOCTEXT("SInventoryContainer", "SInventoryContainer.PrimaryWeapon", "Primary Weapon"))
		.SaveTag(TEXT("PrimaryWeapon"))
		.SizeLimits(FBox2D(FVector2D(128, 148), FVector2D(128, 148)))
		.Content()
		[
			SAssignNew(Widget_PrimaryWeapon, SInventoryItemDropTarget).FilterCategory(ECategoryTypeId::Weapon).FilterSlot(EItemSetSlotId::PrimaryWeapon)
			[
				SNew(SInventoryItem, GetTargetItem(EItemSetSlotId::PrimaryWeapon))
			]
			.OnInventoryItemDrop_Lambda([&](UPlayerInventoryItem* InItem) { OnDropItemIntoSlot.ExecuteIfBound(InItem, EItemSetSlotId::PrimaryWeapon); })
		]
	];

	Widget_WindowsContainer->AddSlot()
	[
		SNew(SWindowBase, Widget_WindowsContainer.ToSharedRef())
		.CanClosable(false)
		.CanInvisable(false)
		.InitAsInvisible(false)
		.InitAsClosed(false)
		.TitleText(NSLOCTEXT("SInventoryContainer", "SInventoryContainer.SecondaryWeapon", "Secondary Weapon"))
		.SaveTag(TEXT("SecondaryWeapon"))
		.SizeLimits(FBox2D(FVector2D(128, 148), FVector2D(128, 148)))
		.Content()
		[
			SAssignNew(Widget_SecondaryWeapon, SInventoryItemDropTarget).FilterCategory(ECategoryTypeId::Weapon).FilterSlot(EItemSetSlotId::SecondaryWeapon)
			[
				SNew(SInventoryItem, GetTargetItem(EItemSetSlotId::SecondaryWeapon))
			]
			.OnInventoryItemDrop_Lambda([&](UPlayerInventoryItem* InItem) { OnDropItemIntoSlot.ExecuteIfBound(InItem, EItemSetSlotId::SecondaryWeapon); })
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SInventoryContainer::ToggleWidget(const bool InToggle)
{
	Widget_Animation->ToggleWidget(InToggle);
	SetVisibility(InToggle ? EVisibility::SelfHitTestInvisible : EVisibility::HitTestInvisible);
	OnToggleInteractive.ExecuteIfBound(InToggle);
}

bool SInventoryContainer::IsInInteractiveMode() const
{
	return GetVisibility() == EVisibility::SelfHitTestInvisible;
}

void SInventoryContainer::OnFillList(const TArray<UPlayerInventoryItem*>& InList)
{
	Widget_WindowInventory->OnFillList(InList);
}

void SInventoryContainer::Refresh()
{
	Widget_PrimaryWeapon->SetContent(SNew(SInventoryItem, GetTargetItem(EItemSetSlotId::PrimaryWeapon)));
	Widget_SecondaryWeapon->SetContent(SNew(SInventoryItem, GetTargetItem(EItemSetSlotId::SecondaryWeapon)));
}

UPlayerInventoryItem* SInventoryContainer::GetTargetItem(const int32& InTargetSlot) const
{
	if (InventoryCompPtr.IsValid() && ProfileCompPtr.IsValid())
	{
		if (auto ProfilePtr = ProfileCompPtr->GetProfileDataPtr(static_cast<EPlayerPreSetId::Type>(ProfileIndex.Get())))
		{
			for (auto i : ProfilePtr->Items)
			{
				if (i.SlotId == InTargetSlot)
				{
					return InventoryCompPtr->FindItem(i.ItemId);
				}
			}
		}
	}

	return nullptr;
}
