// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SWindowInventory.h"
#include "SlateOptMacros.h"
#include "SScaleBox.h"
#include "PlayerInventoryItem.h"
#include "Item/ItemBaseEntity.h"
#include "SInventoryItem.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SWindowInventory::Construct(const FArguments& InArgs, TSharedRef<SWindowsContainer> InOwner)
{
	MAKE_UTF8_SYMBOL(sHelp, 0xf059);

	auto SuperArgs = SWindowBase::FArguments()
	.StartPosition(FVector2D(860, 400))
	.StartSize(FVector2D(600, 400))
	.SizeLimits(FBox2D(FVector2D(400, 200), FVector2D(1900, 1000)))
	.TitleText(NSLOCTEXT("SWindowInventory", "SWindowInventory.Title", "Inventory"))
	.QuickButton(FText::FromString(sHelp))
	.QuickButtonOrder(0)
	.CanInvisable(false)
	.CanClosable(false)
	.Content()
	[
		SAssignNew(Widget_Container, STileView<TWeakObjectPtr<UPlayerInventoryItem>>)
		.SelectionMode(ESelectionMode::None)
		.ListItemsSource(&InventoryList)
		.OnGenerateTile_Lambda([&](TWeakObjectPtr<UPlayerInventoryItem> InItem, const TSharedRef<STableViewBase>& InOwnerTable)
		{
			return SNew(STableRow<TWeakObjectPtr<UPlayerInventoryItem>>, InOwnerTable)
				[
					SNew(SInventoryItem, InItem.Get())
				];
		})
	];

	SWindowBase::Construct(SuperArgs, InOwner);

	InternalToggleWidget(true);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SWindowInventory::OnFillList(const TArray<UPlayerInventoryItem*>& InList)
{
	InventoryList.Empty();

	for (auto& Item : InList)
	{
		if (Item && Item->IsValidLowLevel() && Item->GetEntity() && Item->GetEntity()->GetCategory() != ECategoryTypeId::Building) // Discard buildings and invalid items
		{
			InventoryList.Add(Item);
		}
	}

	Widget_Container->RequestListRefresh();
}