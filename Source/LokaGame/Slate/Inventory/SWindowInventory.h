// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Windows/SWindowBase.h"
#include "PlayerInventoryItem.h"

/**
 * 
 */
class LOKAGAME_API SWindowInventory : public SWindowBase
{
public:
	SLATE_BEGIN_ARGS(SWindowInventory)
	{
		_Visibility = EVisibility::Collapsed;
	}
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, TSharedRef<SWindowsContainer> InOwner);

	void OnFillList(const TArray<UPlayerInventoryItem*>& InList);

protected:

	TArray<TWeakObjectPtr<UPlayerInventoryItem>> InventoryList;

	TSharedPtr<STileView<TWeakObjectPtr<UPlayerInventoryItem>>> Widget_Container;

};
