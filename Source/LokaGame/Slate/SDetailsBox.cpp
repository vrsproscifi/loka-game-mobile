// VRSPRO

#include "LokaGame.h"
#include "SDetailsBox.h"
#include "SlateOptMacros.h"

#include "Styles/DetailsBoxRowWidgetStyle.h"

class LOKAGAME_API SDetailsBoxRow : public SHorizontalBox
{
public:
	SLATE_BEGIN_ARGS(SDetailsBoxRow)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FDetailsBoxRowStyle>("SDetailsBoxRowStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FDetailsBoxRowStyle, Style)
	SLATE_ARGUMENT(FDetailsBoxParameter, Parameter)
	SLATE_END_ARGS()

	BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
	void Construct(const FArguments& InArgs)
	{
		Param = InArgs._Parameter;
		Style = InArgs._Style;

		const float BarValue = Param.Value / Param.MaxValue;
		float BarUpgradeValue;

		if (Param.UpgradeValue > 0)
		{
			BarUpgradeValue = Param.UpgradeValue / (Param.MaxValue - Param.Value);
		}
		else
		{
			BarUpgradeValue = Param.UpgradeValue / Param.Value;
			BarUpgradeValue *= -1.0f;
		}

		AddSlot()
		[
			SNew(STextBlock)
			.Font(Style->FontName)
			.ColorAndOpacity(Style->ColorFontName)
			.Text(InArgs._Parameter.Name)
			.AutoWrapText(true)
		];

		TSharedPtr<SWidget> WidgetProgress;

		if (FMath::IsNearlyEqual(Param.UpgradeValue, 0.0f) == false)
		{
			if (Param.UpgradeValue > 0)
			{
				WidgetProgress = SNew(SOverlay)
				+ SOverlay::Slot()
				[
					SNew(SProgressBar)
					.Style(&Style->ProgressBar)
					.BarFillType(EProgressBarFillType::LeftToRight)
					.FillColorAndOpacity(FLinearColor::White)
					.Percent(FMath::Clamp(BarValue, 0.0f, 1.0f))
				]
				+ SOverlay::Slot()
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot().FillWidth(FMath::Clamp(BarValue, 0.0f, 1.0f))
					+ SHorizontalBox::Slot().FillWidth(FMath::Clamp(1.0f - BarValue, 0.0f, 1.0f))
					[
						SNew(SProgressBar)
						.Style(&Style->ProgressBar)
						.BarFillType(EProgressBarFillType::LeftToRight)
						.FillColorAndOpacity(Param.IsReverseColor ? Style->ColorDowngrade : Style->ColorUpgrade)
						.Percent(FMath::Clamp(BarUpgradeValue, 0.0f, 1.0f))
					]
				];
			}
			else
			{
				WidgetProgress = SNew(SOverlay)
				+ SOverlay::Slot()
				[
					SNew(SProgressBar)
					.Style(&Style->ProgressBar)
					.BarFillType(EProgressBarFillType::LeftToRight)
					.FillColorAndOpacity(FLinearColor::White)
					.Percent(FMath::Clamp(BarValue, 0.0f, 1.0f))
				]
				+ SOverlay::Slot()
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot().FillWidth(FMath::Clamp(BarValue, 0.0f, 1.0f))
					[
						SNew(SProgressBar)
						.Style(&Style->ProgressBar)
						.BarFillType(EProgressBarFillType::RightToLeft)
						.FillColorAndOpacity(Param.IsReverseColor ? Style->ColorUpgrade : Style->ColorDowngrade)
						.Percent(FMath::Clamp(BarUpgradeValue, 0.0f, 1.0f))
					]
					+ SHorizontalBox::Slot().FillWidth(FMath::Clamp(1.0f - BarValue, 0.0f, 1.0f))
				];
			}
		}
		else
		{
			WidgetProgress = SNew(SProgressBar)
				.Style(&Style->ProgressBar)
				.BarFillType(EProgressBarFillType::LeftToRight)
				.FillColorAndOpacity(FLinearColor::White)
				.Percent(FMath::Clamp(BarValue, 0.0f, 1.0f));
		}

		AddSlot()
		[
			SNew(SOverlay)
			.ToolTipText(Param.ToolTipText)
			+ SOverlay::Slot()
			[
				WidgetProgress.ToSharedRef()
			]
			+ SOverlay::Slot()
			.HAlign(EHorizontalAlignment::HAlign_Right)
			.VAlign(EVerticalAlignment::VAlign_Center)
			[
				SNew(STextBlock)
				.Margin(Style->MarginFontValue)
				.Font(Style->FontValue)
				.ColorAndOpacity(Style->ColorFontValue)
				.Text(GetTextValues(Param.Value, Param.UpgradeValue))
			]
		];
	}
	END_SLATE_FUNCTION_BUILD_OPTIMIZATION

	FDetailsBoxParameter Param;

	void RebuildWidget()
	{
		this->ClearChildren();
		this->Construct(FArguments().Parameter(Param));
	}

protected:

	FText GetTextValues(const float& Current, const float& Next = 0.0f)
	{
		return FText::Format(FText::FromString("{0} {1}{2}"),
			FText::AsNumber(Current, &FNumberFormattingOptions::DefaultNoGrouping()),
			(FMath::IsNearlyEqual(Next, 0.0f)) ? FText::GetEmpty() : ((Next > 0.0f) ? FText::FromString("+") : FText::GetEmpty()),
			(FMath::IsNearlyEqual(Next, 0.0f)) ? FText::GetEmpty() : FText::AsNumber(Next, &FNumberFormattingOptions::DefaultNoGrouping()));
	}

	

	const FDetailsBoxRowStyle *Style;	
};

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SDetailsBox::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	ChildSlot
	[
		SNew(SBorder)
		.BorderImage(&Style->Backround)
		.Padding(0)
		[
			SNew(SBorder)
			.BorderImage(&Style->Border)
			.Padding(0)
			[
				SAssignNew(ContainerParameters, SVerticalBox)
			]
		]		
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SDetailsBox::AddParameter(const FName &Name, const FDetailsBoxParameter &Param)
{
	Parameters.Add(Name, SNew(SDetailsBoxRow).Parameter(Param));
	ContainerParameters->AddSlot().Padding(Style->PaddingRow).AttachWidget(Parameters[Name].ToSharedRef());
}

void SDetailsBox::AddParameterUpgradeValue(const FName &Name, const float &_UpgradeValue)
{
	if (FMath::IsNearlyEqual(_UpgradeValue, 0.0f) == false)
	{
		if (auto item = Parameters.Find(Name))
		{
			item->Get()->Param.UpgradeValue = _UpgradeValue;
			item->Get()->RebuildWidget();
		}
	}
}

bool SDetailsBox::UpdateParameter(const FName &Name, const FDetailsBoxParameter &Param)
{
	if (Parameters.Find(Name))
	{
		return true;
	}

	return false;
}

bool SDetailsBox::RemoveParameter(const FName &Name)
{
	if (Parameters.Find(Name))
	{
		ContainerParameters->RemoveSlot(Parameters[Name].ToSharedRef());
		Parameters.Remove(Name);
		return true;
	}

	return false;
}

bool SDetailsBox::RemoveParameters()
{
	if (Parameters.Num())
	{
		ContainerParameters->ClearChildren();
		Parameters.Empty();

		return true;
	}

	return false;
}

FDetailsBoxParameter* SDetailsBox::GetParameter(const FName &Name)
{
	if (auto param = Parameters.Find(Name))
	{
		return &param->Get()->Param;
	}

	return nullptr;
}

void SDetailsBox::RebuildParameters()
{
	for (auto i : Parameters)
	{
		i.Value->RebuildWidget();
	}
}