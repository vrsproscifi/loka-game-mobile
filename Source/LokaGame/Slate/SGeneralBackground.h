// VRSPRO

#pragma once

#include "SlateBasics.h"
#include "Utilities/SUsableCompoundWidget.h"
#include "Styles/LobbyGeneralWidgetStyle.h"
#include "LokaSlateStyle.h"

namespace EButtonType
{
	enum Type
	{
		None,
		Helper,
		Settings,
		Close
	};
};


namespace EGBAnimation
{
	enum Type
	{
		None,
		LeftFade,
		RightFade,
		UpFade,
		DownFade,
		ZoomIn,
		ZoomOut
	};
};

DECLARE_DELEGATE_OneParam(FOnGeneralButtonClick, const EButtonType::Type)

class LOKAGAME_API SGeneralBackground : public SUsableCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SGeneralBackground)
		: _BackgroundStyle(&FLokaStyle::Get().GetWidgetStyle<FLobbyGeneralStyle>("SLobbyGeneralStyle"))
		, _HAlign(HAlign_Fill)
		, _VAlign(VAlign_Fill)
		, _Padding(FMargin(4.0f))
		, _IsFirstButtonSettings(false)
		, _Header()
		, _HeadHAlign(HAlign_Fill)
		, _HeadVAlign(VAlign_Fill)
		, _HeadPadding(FMargin(0.0f))
		, _Content()
		, _Head()
		, _ShowAnimation(EGBAnimation::Type::LeftFade)
		, _HideAnimation(EGBAnimation::Type::RightFade)
	{
		_Visibility = EVisibility::Hidden;
	}
	SLATE_STYLE_ARGUMENT(FLobbyGeneralStyle, BackgroundStyle)
	SLATE_ARGUMENT(EHorizontalAlignment, HAlign)
	SLATE_ARGUMENT(EVerticalAlignment, VAlign)
	SLATE_ARGUMENT(bool, IsFirstButtonSettings)
	SLATE_ARGUMENT(EHorizontalAlignment, HeadHAlign)
	SLATE_ARGUMENT(EVerticalAlignment, HeadVAlign)
	SLATE_EVENT(FOnGeneralButtonClick, OnGeneralButtonClick)
	SLATE_ARGUMENT(FText, Header)
	SLATE_ATTRIBUTE(FMargin, Padding)
	SLATE_ATTRIBUTE(FMargin, HeadPadding)
	SLATE_DEFAULT_SLOT(FArguments, Content)
	SLATE_NAMED_SLOT(FArguments, Head)
	SLATE_ATTRIBUTE(EGBAnimation::Type, ShowAnimation)
	SLATE_ATTRIBUTE(EGBAnimation::Type, HideAnimation)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

	virtual void ToggleWidget(const bool) override;
	virtual bool IsInInteractiveMode() const override;
	void SetOnGeneralButtonClick(FOnGeneralButtonClick _OnGeneralButtonClick);
	void SetShowAnimation(const TAttribute<EGBAnimation::Type> InAnimation);
	void SetHideAnimation(const TAttribute<EGBAnimation::Type> InAnimation);

private:

	FOnGeneralButtonClick OnGeneralButtonClick;

	const FLobbyGeneralStyle *BackgroundStyle;

	FReply OnButtonClickSender(const EButtonType::Type _Type);

	EGBAnimation::Type CurrentAnim;

protected:

	bool IsToggled;
	FCurveSequence AnimSequence;
	FCurveHandle TransHandle;
	FCurveHandle ColorHandle;

	TAttribute<EGBAnimation::Type> ShowAnimation;
	TAttribute<EGBAnimation::Type> HideAnimation;

	FSlateColor WidgetColor() const;
	FLinearColor WidgetLineColor() const;
	TOptional<FSlateRenderTransform> WidgetTrans() const;

	const struct FBackgroundBlurStyle* BlurStyle;

	float WidgetBlurStrength() const;
};
