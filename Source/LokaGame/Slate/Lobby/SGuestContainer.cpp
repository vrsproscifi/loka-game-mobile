// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SGuestContainer.h"
#include "SlateOptMacros.h"
#include "ConstructionGameState.h"
#include "ConstructionPlayerState.h"
#include "ConstructionComponent.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SGuestContainer::Construct(const FArguments& InArgs)
{
	SBaseContainer::Construct(SBaseContainer::FArguments().PlayerState(InArgs._PlayerState));

	Widget_Container->AddSlot().HAlign(HAlign_Center).VAlign(VAlign_Top).Padding(0, 10)
	[
		SNew(SBox).MinDesiredWidth(240).MinDesiredHeight(40)
		[
			SNew(SCheckBox) // Into Fight
			.Type(ESlateCheckBoxType::ToggleButton)
			.Style(&Style->IntoFightButton)
			[
				SNew(SBox)
				.VAlign(VAlign_Center)
				.HAlign(HAlign_Center)
				[
					SNew(STextBlock)
					.TextStyle(&FLokaStyle::Get().GetWidgetStyle<FTextBlockStyle>(TEXT("Default_HeaderTextBlock")))
					.Text(NSLOCTEXT("SGuestContainer", "SGuestContainer.ReturnToHome", "Return To Home"))
				]
			]
			.IsChecked(ECheckBoxState::Unchecked)
			.OnCheckStateChanged_Lambda([&](ECheckBoxState) -> void {
				if (PlayerState.IsValid())
				{
					auto MyGameState = GetWorldGameState<AConstructionGameState>(PlayerState->GetWorld());
					auto MyBuildComp = MyGameState ? MyGameState->GetConstructionComponent() : nullptr;
					if (MyBuildComp)
					{
						MyBuildComp->SendGetBuildingList(FGuid());
					}
				}
			})
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION
