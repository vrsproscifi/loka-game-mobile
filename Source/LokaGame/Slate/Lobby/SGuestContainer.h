// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SBaseContainer.h"

/**
 * 
 */
class LOKAGAME_API SGuestContainer : public SBaseContainer
{
public:
	SLATE_BEGIN_ARGS(SGuestContainer)
	{}
	SLATE_ARGUMENT(TWeakObjectPtr<AConstructionPlayerState>, PlayerState)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);
};
