// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SGameModeSelector.h"
#include "SlateOptMacros.h"
#include "GameModeTypeId.h"
#include "SScaleBox.h"
#include "SBackgroundBlur.h"
#include "SAnimatedBackground.h"
#include "Styles/LobbyContainerWidgetStyle.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SGameModeSelector::Construct(const FArguments& InArgs)
{
	TMap<EBlueprintGameMap, FSlateBrush>();


	OnSelectedGameMode = InArgs._OnSelectedGameMode;

	TArray<TSharedPtr<EGameMode>> AllowedModes;

	AllowedModes.Add(MakeShareable(new EGameMode(EGameMode::DuelMatch)));
	AllowedModes.Add(MakeShareable(new EGameMode(EGameMode::TeamDeadMatch)));
	AllowedModes.Add(MakeShareable(new EGameMode(EGameMode::ResearchMatch)));
	AllowedModes.Add(MakeShareable(new EGameMode(EGameMode::LostDeadMatch)));
	AllowedModes.Add(MakeShareable(new EGameMode(EGameMode::Construction)));		
	AllowedModes.Add(MakeShareable(new EGameMode(EGameMode::None)));

	ChildSlot
	[
		SAssignNew(Widget_Background[0], SAnimatedBackground)
		.Visibility(EVisibility::Visible)
		.WithColor(true)
		.Padding(FMargin(200, 100))
		.InitAsHide(true)
		.IsEnabledBlur(true)
		.IsRelative(true)
		.Duration(1.0f)
		.Ease(ECurveEaseFunction::QuadIn)
		.ShowAnimation(EAnimBackAnimation::Color)
		.HideAnimation(EAnimBackAnimation::Color)
		[
			SAssignNew(Widget_Background[1], SAnimatedBackground)
			.Visibility(EVisibility::Visible)
			.WithColor(true)
			.InitAsHide(true)
			.IsEnabledBlur(false)
			.IsRelative(true)
			.Duration(0.5f)
			.Ease(ECurveEaseFunction::QuadOut)
			.ShowAnimation(EAnimBackAnimation::UpFade)
			.HideAnimation(EAnimBackAnimation::ZoomIn)
			[
				SAssignNew(Widget_TileView, STileView<TSharedPtr<EGameMode>>)
				.Visibility(EVisibility::SelfHitTestInvisible)
				.ItemWidth(400)
				.ItemHeight(400)
				.SelectionMode(ESelectionMode::Single)
				.ItemAlignment(EListItemAlignment::CenterAligned)
				.ListItemsSource(new TArray<TSharedPtr<EGameMode>>(AllowedModes))
				.OnSelectionChanged(this, &SGameModeSelector::OnSelected)
				.OnGenerateTile_Lambda([&](TSharedPtr<EGameMode> InItem, const TSharedRef<STableViewBase>& InOwnerTable)
				{
					bool localIsEnabled = true;
					const auto LStyle = &FLokaStyle::Get().GetWidgetStyle<FLobbyContainerStyle>(TEXT("SLobbyContainerStyle"));
					FSlateBrush* FoundBrush = nullptr;
					auto GameModesMap = LStyle->GameModes;
					auto DisabledModes = LStyle->DisabledGameModes;
					for (auto Pair : GameModesMap)
					{
						EGameMode TestValue(Pair.Key);
						if (TestValue == *InItem.Get())
						{
							localIsEnabled = !DisabledModes.Contains(Pair.Key);
							FoundBrush = new FSlateBrush(Pair.Value);
						}
					}					

					return SNew(STableRow<TSharedPtr<EGameMode>>, InOwnerTable)
								.IsEnabled(localIsEnabled)
								.Style(&LStyle->GameModeTile)
								.Visibility(EVisibility::Visible)
								.Padding(10)
								[
									SNew(SOverlay)
									+ SOverlay::Slot()
									[								
										SNew(SScaleBox)
										.Stretch(EStretch::ScaleToFit)
										[
											SNew(SImage)
											.Image(FoundBrush)
										]
									]
									+ SOverlay::Slot().VAlign(VAlign_Center)
									[
										SNew(SBorder)
										.BorderImage(new FSlateColorBrush(FColor::Black.WithAlpha(128)))
										.Padding(4)
										[
											SNew(SVerticalBox)
											+ SVerticalBox::Slot().AutoHeight()
											[
												SNew(STextBlock)
												.AutoWrapText(true)
												.Justification(ETextJustify::Center)
												.Text(InItem->GetDisplayName())
												.TextStyle(&FLokaStyle::Get().GetWidgetStyle<FTextBlockStyle>(TEXT("Default_HeaderTextBlock")))
											]
											+ SVerticalBox::Slot().Padding(FMargin(4, 10, 4, 6))
											[
												SNew(STextBlock)
												.AutoWrapText(true)
												.Text(InItem->GetDescription())
												.TextStyle(&FLokaStyle::Get().GetWidgetStyle<FTextBlockStyle>(TEXT("Default_TextBlock")))
											]
										]
									]
								];
				})
			]
		]
	];

	Widget_Background[0]->SetOnMouseButtonDown(FPointerEventHandler::CreateLambda([&](const FGeometry& InGeometry, const FPointerEvent& InPointerEvent)
	{
		ToggleWidget(false);
		return FReply::Handled();
	}));
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SGameModeSelector::ToggleWidget(const bool InToggle)
{
	if (InToggle)
	{
		Widget_TileView->ClearSelection();
	}

	Widget_Background[0]->ToggleWidget(InToggle);
	Widget_Background[1]->ToggleWidget(InToggle);
}

void SGameModeSelector::OnSelected(TSharedPtr<EGameMode> InItem, ESelectInfo::Type InType)
{
	if (InType != ESelectInfo::Direct && InItem.IsValid())
	{
		ToggleWidget(false);
		OnSelectedGameMode.ExecuteIfBound(*InItem.Get());
	}
}
