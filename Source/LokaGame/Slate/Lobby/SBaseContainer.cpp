// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SBaseContainer.h"
#include "SlateOptMacros.h"

#include "Windows/SWindowsContainer.h"
#include "Windows/SWindowQuit.h"
#include "Components/SResourceTextBox.h"
#include "Friends/SFriendsForm.h"
#include "Layout/SScaleBox.h"

#include "ConstructionPlayerState.h"
#include "IdentityComponent.h"
#include "ProgressComponent.h"
#include "SquadComponent.h"
#include "PlayerFractionEntity.h"
#include "UTGameUserSettings.h"

#include "SSettingsManager.h"
#include "SGameModeSelector.h"
#include "SAnimatedBackground.h"
#include "Windows/SWindowHelp.h"
#include "NodeComponent/NodeSessionMatch.h"
#include "SLobbyChat.h"
#include "SNotifyList.h"
#include "Windows/SWindowDuels.h"
#include "SLokaToolTip.h"
#include "SRadialMenu.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SBaseContainer::Construct(const FArguments& InArgs)
{
	EmptyBrush = new FSlateNoResource();

	Style = InArgs._Style;
	PlayerState = InArgs._PlayerState;

	MAKE_UTF8_SYMBOL(sOptions, 0xf078);
	MAKE_UTF8_SYMBOL(sPayment, 0xf067);

#pragma region WidgetConstruction

	ChildSlot
	[
		SAssignNew(Widget_Container, SOverlay)
		+ SOverlay::Slot()
		[
			SAssignNew(Widget_Background, SAnimatedBackground)
			.Visibility(EVisibility::Visible)
			.IsEnabledBlur(true)
			.InitAsHide(true)
			.Duration(.2f)
			.Ease(ECurveEaseFunction::QuadIn)
			.ShowAnimation(EAnimBackAnimation::Color)
			.HideAnimation(EAnimBackAnimation::Color)
			.WithColor(true)
		]
		+ SOverlay::Slot().HAlign(HAlign_Fill).VAlign(VAlign_Top).Padding(0, 10)
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot().AutoWidth()
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().AutoWidth().Padding(2, 2)
				[
					SNew(SResourceTextBox)
					.Type(EResourceTextBoxType::Level)
					.Value(this, &SBaseContainer::GetPlayerLevel)
					.CustomSize(FVector(32, 32, 18))
				]
				+ SHorizontalBox::Slot().AutoWidth().Padding(2, 0).VAlign(VAlign_Center)
				[
					SNew(SBox).MinDesiredWidth(160)
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot().AutoHeight().Padding(0, 2)
						[
							SNew(STextBlock)
							.TextStyle(&FLokaStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_TextBlock"))
							.Text(this, &SBaseContainer::GetPlayerName)
							.Justification(ETextJustify::Left)
						]
						+ SVerticalBox::Slot().AutoHeight()
						[
							SNew(SBox).HeightOverride(6)
							[
								SNew(SProgressBar)
								.Style(&FLokaStyle::Get().GetWidgetStyle<FProgressBarStyle>("Default_ProgressBar"))
								.BarFillType(EProgressBarFillType::LeftToRight)
								.Percent(this, &SBaseContainer::GetPlayerLevelPercent)
								.BorderPadding(FVector2D::ZeroVector)
							]
						]
					]
				]
			]
			+ SHorizontalBox::Slot().FillWidth(1) // Free space, overlay next layer Matchmaking
			+ SHorizontalBox::Slot().AutoWidth() // Player: Using servies, premium, booster (money, exp, rep)
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot()
				[
					SNew(SResourceTextBox)
					.Type(EResourceTextBoxType::Premium)
					.Value(this, &SBaseContainer::GetBoosterValue, static_cast<int32>(EResourceTextBoxType::Premium))
					.Visibility(this, &SBaseContainer::GetBoosterVisibility, static_cast<int32>(EResourceTextBoxType::Premium))
				]
				+ SHorizontalBox::Slot()
				[
					SNew(SResourceTextBox)
					.Type(EResourceTextBoxType::BoostMoney)
					.Value(this, &SBaseContainer::GetBoosterValue, static_cast<int32>(EResourceTextBoxType::BoostMoney))
					.Visibility(this, &SBaseContainer::GetBoosterVisibility, static_cast<int32>(EResourceTextBoxType::BoostMoney))
				]
				+ SHorizontalBox::Slot()
				[
					SNew(SResourceTextBox)
					.Type(EResourceTextBoxType::BoostExperience)
					.Value(this, &SBaseContainer::GetBoosterValue, static_cast<int32>(EResourceTextBoxType::BoostExperience))
					.Visibility(this, &SBaseContainer::GetBoosterVisibility, static_cast<int32>(EResourceTextBoxType::BoostExperience))
				]
				+ SHorizontalBox::Slot()
				[
					SNew(SResourceTextBox)
					.Type(EResourceTextBoxType::BoostReputation)
					.Value(this, &SBaseContainer::GetBoosterValue, static_cast<int32>(EResourceTextBoxType::BoostReputation))
					.Visibility(this, &SBaseContainer::GetBoosterVisibility, static_cast<int32>(EResourceTextBoxType::BoostReputation))
				]
			]
			+ SHorizontalBox::Slot().AutoWidth() // Player: Fraction name, progress
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().AutoWidth().VAlign(VAlign_Center)
				[
					SNew(SBox).MinDesiredWidth(160)
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot().AutoHeight().Padding(0, 2)
						[
							SNew(STextBlock)
							.TextStyle(&FLokaStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_TextBlock"))
							.Text(this, &SBaseContainer::GetPlayerFractionRank)
							.Justification(ETextJustify::Right)
						]
						+ SVerticalBox::Slot().AutoHeight()
						[
							SNew(SBox).HeightOverride(6)
							[
								SNew(SProgressBar)
								.Style(&FLokaStyle::Get().GetWidgetStyle<FProgressBarStyle>("Default_ProgressBar"))
								.BarFillType(EProgressBarFillType::LeftToRight)
								.Percent(this, &SBaseContainer::GetPlayerFractionRankPercent)
								.BorderPadding(FVector2D::ZeroVector)
							]
						]
					]
				]
				+ SHorizontalBox::Slot().Padding(4)
				[
					SNew(SScaleBox)
					.Stretch(EStretch::ScaleToFit)
					[
						SNew(SImage)
						.Image(this, &SBaseContainer::GetPlayerFractionBrush)
					]
				]
			]
		]
		+ SOverlay::Slot().HAlign(HAlign_Right).VAlign(VAlign_Top).Padding(10, 80)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight().Padding(4, 2).VAlign(VAlign_Center).HAlign(HAlign_Left)
			[
				SNew(SHorizontalBox)
				.ToolTip(SNew(SLokaToolTip).Text(NSLOCTEXT("SLobbyContainer", "SLobbyContainer.Donate.OpenLink", "Click here to payment this currency, on clicked will be opened web page on default web browser.")))
				+ SHorizontalBox::Slot().AutoWidth()
				[
					SNew(SBox).MinDesiredWidth(180)
					[
						SNew(SResourceTextBox)
						.Type(EResourceTextBoxType::Money)
						.Value(this, &SBaseContainer::GetPlayerCurrency, EGameCurrency::Money)
					]
				]
				+ SHorizontalBox::Slot().AutoWidth()
				[
					SNew(SButton)
					.OnClicked(this, &SBaseContainer::OnClickedCurrency, EGameCurrency::Money)
					.TextStyle(&Style->FightOptionsText)
					.ButtonStyle(&Style->PaymentButton)
					.Text(FText::FromString(sPayment))
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
				]
			]
			+ SVerticalBox::Slot().AutoHeight().Padding(4, 2).VAlign(VAlign_Center).HAlign(HAlign_Left)
			[
				SNew(SHorizontalBox)
				.ToolTip(SNew(SLokaToolTip).Text(NSLOCTEXT("SLobbyContainer", "SLobbyContainer.Donate.OpenLink", "Click here to payment this currency, on clicked will be opened web page on default web browser.")))
				+ SHorizontalBox::Slot().AutoWidth()
				[
					SNew(SBox).MinDesiredWidth(180)
					[
						SNew(SResourceTextBox)
						.Type(EResourceTextBoxType::Donate)
						.Value(this, &SBaseContainer::GetPlayerCurrency, EGameCurrency::Donate)
					]
				]
				+ SHorizontalBox::Slot().AutoWidth()
				[
					SNew(SButton)
					.OnClicked(this, &SBaseContainer::OnClickedCurrency, EGameCurrency::Donate)
					.TextStyle(&Style->FightOptionsText)
					.ButtonStyle(&Style->PaymentButton)
					.Text(FText::FromString(sPayment))
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
				]
			]
			+ SVerticalBox::Slot().AutoHeight().Padding(4, 2).VAlign(VAlign_Center).HAlign(HAlign_Left)
			[
				SNew(SHorizontalBox)
				.ToolTip(SNew(SLokaToolTip).Text(NSLOCTEXT("SLobbyContainer", "SLobbyContainer.Donate.OpenLink", "Click here to payment this currency, on clicked will be opened web page on default web browser.")))
				+ SHorizontalBox::Slot().AutoWidth()
				[
					SNew(SBox).MinDesiredWidth(180)
					[
						SNew(SResourceTextBox)
						.Type(EResourceTextBoxType::Coin)
						.Value(this, &SBaseContainer::GetPlayerCurrency, EGameCurrency::Coin)
					]
				]
				+ SHorizontalBox::Slot().AutoWidth()
				[
					SNew(SButton)
					.OnClicked(this, &SBaseContainer::OnClickedCurrency, EGameCurrency::Coin)
					.TextStyle(&Style->FightOptionsText)
					.ButtonStyle(&Style->PaymentButton)
					.Text(FText::FromString(sPayment))
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
				]
			]
		]
	];

	Widget_Container->AddSlot()
	[
		SAssignNew(WindowsContainer, SWindowsContainer)
		.OnRequestToggle(this, &SBaseContainer::ToggleWidgetFromWindow)
	];

	Widget_Background->SetOnMouseButtonDown(FPointerEventHandler::CreateLambda([&](const FGeometry&, const FPointerEvent&) -> FReply
	{
		ToggleWidget(false);
		return FReply::Handled();
	}));

#pragma endregion 

	WindowsContainer->AddSlot()
		[
			SNew(SWindowHelp, WindowsContainer.ToSharedRef())
		];

	WindowsContainer->AddSlot()
		[
			SNew(SFriendsForm, WindowsContainer.ToSharedRef())
			.PlayerState(PlayerState)
		];

	WindowsContainer->AddSlot()
		[
			SNew(SSettingsManager, WindowsContainer.ToSharedRef())
		];

	WindowsContainer->AddSlot()
		[
			SNew(SWindowQuit, WindowsContainer.ToSharedRef())
		];

	WindowsContainer->AddSlot()
		[
			SNotifyList::Get()
		];
#if WITH_EDITOR
	WindowsContainer->AddSlot()
		[
			SNew(SWindowDuels, WindowsContainer.ToSharedRef())
		];
#endif
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SBaseContainer::ToggleWidget(const bool InToggle)
{
	if (InToggle || (InToggle == false && WindowsContainer->RequestToggle(false, false)))
	{
		SetVisibility(InToggle ? EVisibility::SelfHitTestInvisible : EVisibility::HitTestInvisible);
		Widget_Background->ToggleWidget(InToggle);
		OnToggleInteractive.ExecuteIfBound(InToggle);
	}
}

void SBaseContainer::ToggleWidgetFromWindow(const bool InToggle)
{
	if (InToggle || (InToggle == false && WindowsContainer->IsAnyClosableOpened() == false))
	{
		SetVisibility(InToggle ? EVisibility::SelfHitTestInvisible : EVisibility::HitTestInvisible);
		Widget_Background->ToggleWidget(InToggle);
		OnToggleInteractive.ExecuteIfBound(InToggle);
	}
}

int64 SBaseContainer::GetPlayerCurrency(const EGameCurrency::Type InType) const
{
	return GetPlayerInfo().TakeCash(InType);
}

int64 SBaseContainer::GetPlayerLevel() const
{
	return GetPlayerInfo().Experience.Level;
}

int64 SBaseContainer::GetBoosterValue(const int32 InFlag) const
{
	if (const auto localFoundService = GetPlayerInfo().UsingService.FindByPredicate([f = InFlag](const FPlayerUsingServiceView& InData) { return SResourceTextBox::ServiceModelToType(InData.ServiceTypeId) == f; }))
	{
		return (*localFoundService).ExpirationDate;
	}

	return 0;
}

FText SBaseContainer::GetPlayerName() const
{
	return FText::FromString(GetPlayerInfo().Login);
}

FText SBaseContainer::GetPlayerFractionRank() const
{
	if (const auto localCurrentFraction = GetPlayerFraction())
	{
		return localCurrentFraction->GetFractionRank();
	}

	return FText::GetEmpty();
}

TOptional<float> SBaseContainer::GetPlayerLevelPercent() const
{
	return TOptional<float>(float(GetPlayerInfo().Experience.Experience) / float(GetPlayerInfo().Experience.NextExperience));
}

TOptional<float> SBaseContainer::GetPlayerFractionRankPercent() const
{
	if (const auto localCurrentFraction = GetPlayerFraction())
	{
		return float(localCurrentFraction->GetProgress().Reputation) / float(localCurrentFraction->GetProgress().NextReputation);
	}

	return .0f;
}

const FSlateBrush* SBaseContainer::GetPlayerFractionBrush() const
{
	if (const auto localCurrentFraction = GetPlayerFraction())
	{
		return localCurrentFraction->GetImage();
	}

	return EmptyBrush;
}

EVisibility SBaseContainer::GetBoosterVisibility(const int32 InFlag) const
{
	return GetBoosterValue(InFlag) > 0 ? EVisibility::Visible : EVisibility::Collapsed;
}

FPlayerEntityInfo SBaseContainer::GetPlayerInfo() const
{
	if (PlayerState.IsValid() && PlayerState.Get()->GetIdentityComponent())
	{
		return PlayerState.Get()->GetIdentityComponent()->GetPlayerInfo();
	}

	return FPlayerEntityInfo();
}

const UPlayerFractionEntity* SBaseContainer::GetPlayerFraction() const
{
	if (PlayerState.IsValid() && PlayerState.Get()->GetProgressComponent() && PlayerState.Get()->GetProgressComponent()->GetCurrentFraction())
	{
		return PlayerState.Get()->GetProgressComponent()->GetCurrentFraction();
	}

	return nullptr;
}

FReply SBaseContainer::OnClickedCurrency(const EGameCurrency::Type InType)
{
	if (InType == EGameCurrency::Coin)
	{
		FPlatformProcess::LaunchURL(TEXT("http://lokagame.com/Payment/Payment"), nullptr, nullptr);
	}
	else if (InType == EGameCurrency::Donate)
	{
		FPlatformProcess::LaunchURL(TEXT("http://lokagame.com/Market"), nullptr, nullptr);
	}

	return FReply::Handled();
}
