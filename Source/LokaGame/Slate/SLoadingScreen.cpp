#include "LokaGame.h"
#include "SLoadingScreen.h"

#include "Decorators/LokaTextDecorator.h"
#include "Decorators/LokaResourceDecorator.h"
#include "Decorators/LokaKeyDecorator.h"


BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SLoadingScreen::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	TSharedPtr<SCanvas> _Zalipon;

	ChildSlot.VAlign(EVerticalAlignment::VAlign_Fill).HAlign(EHorizontalAlignment::HAlign_Fill)
	[
		SNew(SOverlay)
		+ SOverlay::Slot()
		.VAlign(VAlign_Center)
		.HAlign(HAlign_Center)
		[
			SAssignNew(WidgetImage, SImage)
		]
		+ SOverlay::Slot()
		.VAlign(VAlign_Top)
		.HAlign(HAlign_Fill)
		[
			SNew(SBorder)
			.BorderBackgroundColor(FLinearColor::Black)
			.BorderImage(new FSlateBrush())
			.Padding(0)
			[
				SNew(SBox)
				.HeightOverride(FOptionalSize(140))
				.VAlign(VAlign_Center)
				.HAlign(HAlign_Center)
				[
					SAssignNew(WidgetLoadingTitle, STextBlock)
					.AutoWrapText(false)
					.Justification(ETextJustify::Center)
					.Margin(FMargin(0, 10))
					.TextStyle(&Style->Fonts.Title)
				]
			]
		]
		+ SOverlay::Slot()
		.VAlign(VAlign_Bottom)
		.HAlign(HAlign_Fill)
		[
			SNew(SBorder)
			.BorderBackgroundColor(FLinearColor::Black)
			.BorderImage(new FSlateBrush())
			.Padding(0)
			[
				SNew(SBox)
				.HeightOverride(FOptionalSize(140))
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot()
					.FillWidth(1)
					[
						SAssignNew(WidgetLoadingTip, SRichTextBlock)
						.AutoWrapText(true)
						.TextStyle(&Style->Fonts.Message)
						.Margin(FMargin(40, 10, 100, 10))
						.DecoratorStyleSet(&FLokaStyle::Get())
						+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(TMap<FName, FSlateFontInfo>(), Style->Fonts.Message.Font, Style->Fonts.Message.ColorAndOpacity.GetSpecifiedColor()))
						+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
						+ SRichTextBlock::Decorator(FLokaKeyDecorator::Create())
					]
					+ SHorizontalBox::Slot()
					.AutoWidth()
					.VAlign(VAlign_Center)
					.HAlign(HAlign_Center)
					[
						SNew(SBox)
						.WidthOverride(FOptionalSize(120))
						.HeightOverride(FOptionalSize(120))
						[
							SAssignNew(_Zalipon, SCanvas)
							+ SCanvas::Slot()
							.Size(FVector2D(120, 120))
							[
								SNew(SBorder)
								.BorderImage(&Style->Animated.Background)
							]
							+ SCanvas::Slot()
							.Size(FVector2D(120, 120))
							[
								SAssignNew(SUKA, SBorder)
								.BorderImage(&Style->Animated.Rotator)
								.BorderBackgroundColor(FLinearColor(0.69, 0.839, 1.0, 1.0))
								.HAlign(EHorizontalAlignment::HAlign_Center)
								.VAlign(EVerticalAlignment::VAlign_Center)
								.RenderTransformPivot(FVector2D(0.5f, 0.5f))
							]
							+ SCanvas::Slot()
							.Size(FVector2D(100, 100))
							.Position(FVector2D(60, 60))
							.HAlign(EHorizontalAlignment::HAlign_Center)
							.VAlign(EVerticalAlignment::VAlign_Center)
							[
								SAssignNew(SpritesContainer, SCanvas)
								.RenderTransformPivot(FVector2D(0.5f, 0.5f))
							]
						]
					]
				]
			]
		]
	];
	
	ZaliponRotateHandle[0] = ZaliponRotateAnim[0].AddCurve(0.0f, 10.0f, ECurveEaseFunction::Linear);
	ZaliponRotateHandle[1] = ZaliponRotateAnim[1].AddCurve(0.0f, 8.0f, ECurveEaseFunction::Linear);
	

	int32 _count = 0;
	for (auto &_sprite : Style->Animated.Sprites)
	{
		SpritesContainer->AddSlot().Size(FVector2D(100, 100)).AttachWidget(
			SNew(SBorder)
			.BorderImage(&_sprite)
			.BorderBackgroundColor(this, &SLoadingScreen::ZaliponColor, _count)
		);

		ZaliponHandle[_count] = ZliponAnim.AddCurveRelative(0.0f, 0.6f, ECurveEaseFunction::QuadOut);

		++_count;
	}

	ZliponAnim.Play(this->AsShared());
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

#define XEPH9(val) (val*2.0f > 1.0f) ? (1.0f-val) : (val)

FSlateColor SLoadingScreen::ZaliponColor(int32 _index) const
{
	// 0.111, 0.323, 0.515, 1.0
	// 0.69, 0.839, 1.0, 1.0
	// FMath::Sin(2 * PI * ZaliponHandle[_index].GetLerpLooping())
	return FMath::Lerp(FLinearColor(0.111, 0.323, 0.515, 0.6), FLinearColor(0.69, 0.839, 1.0, 1.0), XEPH9(ZaliponHandle[_index].GetLerp())*1.5f);
}

void SLoadingScreen::Tick(const FGeometry & AllottedGeometry, const double InCurrentTime, const float InDeltaTime)
{
	SWidget::Tick(AllottedGeometry, InCurrentTime, InDeltaTime);

	SUKA->SetRenderTransform(FSlateRenderTransform(FQuat2D(FMath::DegreesToRadians(ZaliponRotateHandle[0].GetLerp()*360.0f))));
	SUKA->SetBorderBackgroundColor(FMath::Lerp(FLinearColor(0.111, 0.323, 0.515, 1.0), FLinearColor(0.69, 0.839, 1.0, 1.0), XEPH9(ZaliponRotateHandle[0].GetLerp())));

	SpritesContainer->SetRenderTransform(FSlateRenderTransform(FQuat2D(FMath::DegreesToRadians(ZaliponRotateHandle[1].GetLerp()*360.0f))));

	if (!ZaliponRotateAnim[0].IsPlaying())
		ZaliponRotateAnim[0].Play(this->AsShared());

	if (!ZaliponRotateAnim[1].IsPlaying())
		ZaliponRotateAnim[1].PlayReverse(this->AsShared());

	if (!ZliponAnim.IsPlaying())
	{
		ZliponAnim.Play(this->AsShared());
	}
}

void SLoadingScreen::SetLoadingImage(const FSlateBrush *InImage)
{
	WidgetImage->SetImage(InImage);
}

void SLoadingScreen::SetLoadingTitle(const FText &InText)
{
	WidgetLoadingTitle->SetText(InText.IsEmpty() ? NSLOCTEXT("Instance", "Instance.Loading", "Loading") : InText);
}

void SLoadingScreen::SetLoadingTip(const FText &InText)
{
	WidgetLoadingTip->SetText(InText);
}

void SLoadingScreen::SetRandomImage()
{
	if (Style->Images.Num())
	{
		SetLoadingImage(&Style->Images[FMath::RandRange(0, Style->Images.Num() - 1)]);
	}
}

void SLoadingScreen::SetRandomTip()
{
	if (Style->Tips.Num())
	{
		SetLoadingTip(Style->Tips[FMath::RandRange(0, Style->Tips.Num() - 1)]);
	}
}

void SLoadingScreen::SetRandomImageAndTip()
{
	SetRandomImage();
	SetRandomTip();
}