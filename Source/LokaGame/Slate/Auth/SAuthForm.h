// VRSPRO

#pragma once


#include "Utilities/SUsableCompoundWidget.h"
#include "Styles/AuthFormWidgetStyle.h"
#include "Components/IdentityComponent/Authorization.h"

/**
 * 
 */
class LOKAGAME_API SAuthForm : public SUsableCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SAuthForm)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FAuthFormStyle>("SAuthFormStyle"))
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_EVENT(FOnAuthorizationRequest, OnAuthentication)
	SLATE_EVENT(FOnClicked, OnClickedSettings)
	SLATE_EVENT(FOnClickedOutside, OnQuit)
	SLATE_STYLE_ARGUMENT(FAuthFormStyle, Style)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	virtual void ToggleWidget(const bool) override;
	virtual bool IsInInteractiveMode() const override;

	void OnErrorMessage(const FText&);

protected:
	
	const FAuthFormStyle* Style;

	TSharedPtr<class SAnimatedBackground> AnimatedBackground[2], AnimatedError;

	TSharedPtr<STextBlock> Widget_ErrorMessage;
	TSharedPtr<SEditableTextBox> Widget_InputEmail, Widget_InputPass;
	TSharedPtr<SCheckBox> Widget_IsRememberMe;

	FOnAuthorizationRequest OnAuthentication;
	FOnClickedOutside OnQuit;

	EActiveTimerReturnType OnErrorMessageHide(double InCurrentTime, float InDeltaTime);

	void SaveAccount();
	void LoadAccount();

	FReply OnClickedSign(const bool);
};
