// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SWindowBuyBuilding.h"
#include "SlateOptMacros.h"

#include "Build/SBuildSystem_Item.h"
#include "Build/ItemBuildEntity.h"
#include "SResourceTextBox.h"
#include "RichTextHelpers.h"

#include "Decorators/LokaTextDecorator.h"
#include "IdentityController/Models/PlayerEntityInfo.h"
#include "PlayerInventoryItem.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SWindowBuyBuilding::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	Attr_Cash = InArgs._Cash;

	bIsSellMode = false;

	ChildSlot
	[
		SNew(SVerticalBox)
		+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(10, 6)) // Desc text
		[
			SNew(SRichTextBlock)
			.AutoWrapText(true)
			.Text(this, &SWindowBuyBuilding::GetCurrentDescription)
			.TextStyle(&Style->DescText)
			.DecoratorStyleSet(&FLokaStyle::Get())
			.Justification(ETextJustify::Center)
			+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(Style->DescText))
		]
		+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(10, 6))
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot().AutoWidth() // Item preview
			[
				SAssignNew(Widget_ItemContainer, SBorder)
				.Padding(4)
				.BorderImage(new FSlateNoResource())
			]
			+ SHorizontalBox::Slot().AutoWidth().VAlign(VAlign_Center)
			[
				SNew(SBox).MinDesiredWidth(600)
				[
					SNew(SVerticalBox)
					+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(0, 2))
					[
						SNew(SHorizontalBox)
						+ SHorizontalBox::Slot().FillWidth(1).HAlign(HAlign_Left).VAlign(VAlign_Center).Padding(FMargin(10, 0))
						[
							SNew(STextBlock).Text(NSLOCTEXT("SWindowBuyBuilding", "SWindowBuyBuilding.Desc.Amount", "Amount:"))
							                .TextStyle(&Style->DescValue)
						]
						+ SHorizontalBox::Slot().FillWidth(2).VAlign(VAlign_Center)
						[
							SAssignNew(Widget_SliderAmount, SSliderNumber<uint8>)
							.ButtonsDelta(1)
							.SliderMinimum(1)
							.SliderMaximum(this, &SWindowBuyBuilding::GetSliderMaximum)
							.InitValue(1)
						]
					]
					+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(0, 2))
					[
						SNew(SHorizontalBox)
						+ SHorizontalBox::Slot().FillWidth(1).HAlign(HAlign_Left).VAlign(VAlign_Center).Padding(FMargin(10, 0))
						[
							SNew(STextBlock).Text(NSLOCTEXT("SWindowBuyBuilding", "SWindowBuyBuilding.Desc.Price", "Price:"))
							                .TextStyle(&Style->DescValue)
						]
						+ SHorizontalBox::Slot().FillWidth(2).VAlign(VAlign_Center)
						[
							SNew(SResourceTextBox)
							.CustomSize(FVector(16, 16, 12))
							.TypeAndValue_Lambda([&]() {
								if(auto item = GetCurrentItemEntity())
								{
									return FResourceTextBoxValue(item->GetCostAsResource().Type, bIsSellMode 
									? (item->GetCostAsResource().Value * GetCurrentItemAmount()) / 2
									: item->GetCostAsResource().Value * GetCurrentItemAmount());
								}
								return FResourceTextBoxValue();
							})
						]
					]
					+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(0, 2))
					[
						SNew(SHorizontalBox)
						+ SHorizontalBox::Slot().FillWidth(1).HAlign(HAlign_Left).VAlign(VAlign_Center).Padding(FMargin(10, 0))
						[
							SNew(STextBlock).Text(NSLOCTEXT("SWindowBuyBuilding", "SWindowBuyBuilding.Desc.Currency", "Your Currency:"))
							                .TextStyle(&Style->DescValue)
						]
						+ SHorizontalBox::Slot().FillWidth(2).VAlign(VAlign_Center)
						[
							SNew(SResourceTextBox)
							.CustomSize(FVector(16, 16, 12))
							.TypeAndValue_Lambda([&]() {
								auto EntityItem = GetCurrentItemEntity();

								if (Attr_Cash.IsSet() && EntityItem)
								{
									//====================================
									auto cash = Attr_Cash.Get().FindByPredicate([&, c = EntityItem->GetCost().Currency](const FTypeCash& t)
									{
										return t.Currency == c;
									});

									//====================================
									if (cash)
									{
										return FResourceTextBoxValue(static_cast<EResourceTextBoxType::Type>(EntityItem->GetCost().Currency.GetValue()), cash->Amount);
									}
								}

								return FResourceTextBoxValue();
							})
						]
					]
				]
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SWindowBuyBuilding::SetCurrentItem(const UPlayerInventoryItem* InItem)
{
	SourcePtr = InItem;

	if (SourcePtr.IsValid())
	{
		Widget_ItemContainer->SetContent(SNew(SBuildSystem_Item, SourcePtr.Get()));
		CurrentFormatedName = FRichHelpers::TextHelper_NotifyValue.SetValue(SourcePtr->GetEntity<UItemBuildEntity>()->GetDescription().Name).ToText();

		if (Widget_SliderAmount->GetValue() <= 0)
		{
			Widget_SliderAmount->SetValue(1);
		}
	}
}

const UPlayerInventoryItem* SWindowBuyBuilding::GetCurrentItem() const
{
	return SourcePtr.Get();
}

const UItemBaseEntity* SWindowBuyBuilding::GetCurrentItemEntity() const
{
	if (SourcePtr.IsValid())
	{
		if(auto item = GetValidObject(SourcePtr.Get()))
		{
			return item->GetEntity();
		}
	}
	return nullptr;
}

void SWindowBuyBuilding::ToggleSellMode(const bool InToggle)
{
	bIsSellMode = InToggle;
}

int32 SWindowBuyBuilding::GetCurrentItemAmount() const
{
	return Widget_SliderAmount->GetValue();
}

void SWindowBuyBuilding::SetCurrentItemAmount(const int32& InValue) const
{
	Widget_SliderAmount->SetValue(InValue);
}

FText SWindowBuyBuilding::GetCurrentDescription() const
{
	if (bIsSellMode)
	{
		return FText::Format(NSLOCTEXT("SWindowBuyBuilding", "SWindowBuyBuilding.Desc.Sell", "You really want to sell {0}?"), CurrentFormatedName);
	}

	return FText::Format(NSLOCTEXT("SWindowBuyBuilding", "SWindowBuyBuilding.Desc", "You really want to buy {0}?"), CurrentFormatedName);
}

uint8 SWindowBuyBuilding::GetSliderMaximum() const
{
	if (bIsSellMode && GetCurrentItem())
	{
		return GetCurrentItem()->GetCount();
	}

	return 100;
}
