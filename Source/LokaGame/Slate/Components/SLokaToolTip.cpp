// VRSPRO

#include "LokaGame.h"
#include "SLokaToolTip.h"
#include "SlateOptMacros.h"

#include "SAnimatedBackground.h"
#include "Decorators/LokaTextDecorator.h"
#include "Decorators/LokaResourceDecorator.h"
#include "Decorators/LokaKeyDecorator.h"
#include "Decorators/LokaSpaceDecorator.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SLokaToolTip::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	Attr_Font = InArgs._Font.IsSet() ? InArgs._Font : TAttribute<FSlateFontInfo>(Style->Font);
	Attr_FontColor = InArgs._FontColor.IsSet() ? InArgs._FontColor : TAttribute<FSlateColor>(Style->FontColor);
	Attr_TextMargin = InArgs._TextMargin.IsSet() ? InArgs._TextMargin : TAttribute<FMargin>(Style->TextMargin);
	Attr_BackgroundImage = InArgs._BackgroundImage.IsSet() ? InArgs._BackgroundImage : TAttribute<const FSlateBrush*>(&Style->Background);
	Attr_BorderImage = InArgs._BorderImage.IsSet() ? InArgs._BorderImage : TAttribute<const FSlateBrush*>(&Style->Border);

	Attr_ContentText = InArgs._Text;
	Attr_IsInteractive = InArgs._IsInteractive;

	SetContentWidget(InArgs._Content.Widget);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SLokaToolTip::SetContentWidget(const TSharedRef<SWidget>& InContentWidget)
{
	if (InContentWidget != SNullWidget::NullWidget)
	{
		Weak_ContentWidget = InContentWidget;
	}

	TSharedPtr< SWidget > PinnedWidgetContent = Weak_ContentWidget.Pin();
	if (PinnedWidgetContent.IsValid())
	{
		ToolTipContent = PinnedWidgetContent;
	}
	else
	{
		auto TextStyle = FTextBlockStyle().SetColorAndOpacity(Attr_FontColor.Get()).SetFont(Attr_Font.Get());

		ToolTipContent =
			SNew(SRichTextBlock)
			.Text(Attr_ContentText)
			.AutoWrapText(true)
			.TextStyle(new FTextBlockStyle(TextStyle))
			.WrapTextAt_Static(&SToolTip::GetToolTipWrapWidth)
			.DecoratorStyleSet(&FLokaStyle::Get())
			+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(TextStyle))
			+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
			+ SRichTextBlock::Decorator(FLokaKeyDecorator::Create())
			+ SRichTextBlock::Decorator(FLokaSpaceDecorator::Create())
			+ SRichTextBlock::Decorator(FHyperlinkDecorator::Create(TEXT(""), FSlateHyperlinkRun::FOnClick::CreateLambda([&](const FSlateHyperlinkRun::FMetadata& InData) {
				if (InData.Contains("url")) FPlatformProcess::LaunchURL(*InData.FindChecked("url"), nullptr, nullptr);
			})));				
	}

	ChildSlot
	[
		SAssignNew(FxWidget, SAnimatedBackground)
		.ShowAnimation(EAnimBackAnimation::ZoomOut)
		.HideAnimation(EAnimBackAnimation::ZoomIn)
		.InitAsHide(true)
		.Duration(.3f)
		[
			SNew(SBorder)
			.BorderImage(Attr_BackgroundImage)
			.Padding(0)
			[
				SNew(SBorder)
				.BorderImage(Attr_BorderImage)
				.Padding(Attr_TextMargin)
				[
					ToolTipContent.ToSharedRef()
				]
			]
		]
	];
}

bool SLokaToolTip::IsEmpty() const
{
	return !Weak_ContentWidget.IsValid() && Attr_ContentText.Get().IsEmpty();
}

bool SLokaToolTip::IsInteractive() const
{
	return Attr_IsInteractive.Get();
}

void SLokaToolTip::OnOpening()
{
	if (IsEnabled())
	{
		FxWidget->ToggleWidget(true);
	}
}

void SLokaToolTip::OnClosed()
{
	FxWidget->ToggleWidget(false);
}