// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/MessageBoxWidgetStyle.h"
/**
 * Simple component for message box
 */
class LOKAGAME_API SInputWindow : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SInputWindow)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FMessageBoxStyle>("SMessageBoxStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FMessageBoxStyle, Style)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void SetContent(const FText&);
	FText GetInputText() const;
	void SetInputText(const FText&);
	void SetInputError(const FText&);

protected:

	const FMessageBoxStyle* Style;

	TSharedPtr<STextBlock> Widget_TextBlock;
	TSharedPtr<SEditableTextBox> Widget_EditableTextBox;
};
