// VRSPRO

#include "LokaGame.h"
#include "SWindowBackground.h"
#include "SBackgroundBlur.h"
#include "SlateOptMacros.h"

#include "ToggableWidgetHelper.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SWindowBackground::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	bIsExpandable = InArgs._IsExpandable;

	TSharedRef<SWidget> HeaderWidget = (InArgs._HeaderContent.Widget == SNullWidget::NullWidget) ? SNullWidget::NullWidget :
		SNew(SBorder)
		.BorderImage(InArgs._IsEnabledHeaderBackground ? &Style->Header : new FSlateNoResource())
		.HAlign(HAlign_Fill)
		.VAlign(VAlign_Fill)
		.Padding(InArgs._HeaderPadding)
		[
			InArgs._HeaderContent.Widget
		];

	if (InArgs._Header.IsEmpty() == false)
	{
		if (InArgs._IsEnabledHeaderBackground)
		{
			HeaderWidget =
				SNew(SBorder)
				.BorderImage(&Style->Header)
				.HAlign(HAlign_Center)
				.VAlign(VAlign_Center)
				.Padding(InArgs._HeaderPadding)
				[
					SNew(STextBlock)
					.TextStyle(&Style->HeaderText)
					.Text(InArgs._Header)
				];
		}
		else
		{
			HeaderWidget =
				SNew(SBox)
				.HAlign(HAlign_Center)
				.VAlign(VAlign_Center)
				.Padding(InArgs._HeaderPadding)
				[
					SNew(STextBlock)
					.TextStyle(&Style->HeaderText)
					.Text(InArgs._Header)
				];
		}
	}

	BackgroundBrush = new FSlateBrush(Style->Background);
	const auto SourceColor = BackgroundBrush->TintColor.GetSpecifiedColor();
	BackgroundBrush->TintColor = FSlateColor(FLinearColor(SourceColor.R, SourceColor.G, SourceColor.B, (InArgs._Opacity > .0f) ? InArgs._Opacity : SourceColor.A));

	const FBackgroundBlurStyle* BackgroundBlurStyle = InArgs._BlurStyle;

	ChildSlot
	[
		SAssignNew(Widget_BackgroundBlur, SBackgroundBlur)
		.Padding(0)
		.HAlign(HAlign_Fill)
		.VAlign(VAlign_Fill)
		.bApplyAlphaToBlur(BackgroundBlurStyle->bApplyAlphaToBlur)
		.BlurStrength(InArgs._IsEnabledBlur ? BackgroundBlurStyle->BlurStrength : .0f)
		.BlurRadius(InArgs._IsEnabledBlur ? BackgroundBlurStyle->BlurRadius : 0)
		.LowQualityFallbackBrush(&BackgroundBlurStyle->LowQualityFallbackBrush)
		[
			SNew(SBorder) // Background
			.BorderImage(BackgroundBrush)
			.Padding(0)
			[
				SNew(SBorder) // Border
				.BorderImage(&Style->Border)
				.Padding(0)
				[
					SNew(SBorder) // Shadow
					.BorderImage(&Style->Shadow)				
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot().AutoHeight()
						[
							HeaderWidget
						]
						+ SVerticalBox::Slot().FillHeight(1).Padding(InArgs._Padding)
						[
							SNew(SBorder)
							.BorderBackgroundColor(FLinearColor::Transparent)
							.Padding(0)
							.Visibility(this, &SWindowBackground::OnGetContentVisibility)
							.DesiredSizeScale(this, &SWindowBackground::GetSectionScale)
							[
								InArgs._Content.Widget
							]
						]
					]
				]
			]
		]
	];

	ExpandableAnimation = FCurveSequence(0.0f, 1.5f, ECurveEaseFunction::QuadInOut);

	if (bIsExpandable == false)
	{
		SetExpanded(true, false);
	}
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

// SBackgroundBlur Begin
void SWindowBackground::SetApplyAlphaToBlur(bool bInApplyAlphaToBlur)
{
	Widget_BackgroundBlur->SetApplyAlphaToBlur(bInApplyAlphaToBlur);
}

void SWindowBackground::SetBlurRadius(const TAttribute<TOptional<int32>>& InBlurRadius)
{
	Widget_BackgroundBlur->SetBlurRadius(InBlurRadius);
}

void SWindowBackground::SetBlurStrength(const TAttribute<float>& InStrength)
{
	Widget_BackgroundBlur->SetBlurStrength(InStrength);
}

void SWindowBackground::SetLowQualityBackgroundBrush(const FSlateBrush* InBrush)
{
	Widget_BackgroundBlur->SetLowQualityBackgroundBrush(InBrush);
}
// SBackgroundBlur End

void SWindowBackground::OnMouseEnter(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
	SCompoundWidget::OnMouseEnter(MyGeometry, MouseEvent);

	if (bIsExpandable)
	{
		SetExpanded(true, true);
	}
}

void SWindowBackground::OnMouseLeave(const FPointerEvent& MouseEvent)
{
	SCompoundWidget::OnMouseLeave(MouseEvent);

	if (bIsExpandable)
	{
		SetExpanded(false, true);
	}
}

void SWindowBackground::SetExpanded(const bool InExpanded, const bool WithAnimation)
{
	bIsExpanded = InExpanded;

	if (WithAnimation)
	{
		FToggableWidgetHelper::ToggleWidget(AsShared(), ExpandableAnimation, bIsExpanded);
	}
	else
	{
		if (bIsExpanded)
		{
			ExpandableAnimation.JumpToEnd();
		}
		else
		{
			ExpandableAnimation.JumpToStart();
		}
	}
}

FVector2D SWindowBackground::GetSectionScale() const
{
	return FVector2D(1.0f, ExpandableAnimation.GetLerp());
}

EVisibility SWindowBackground::OnGetContentVisibility() const
{
	return FMath::IsNearlyEqual(GetSectionScale().Y, .0f) ? EVisibility::Collapsed : EVisibility::Visible;
}