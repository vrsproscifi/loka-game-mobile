// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/ItemPropertyWidgetStyle.h"

class UItemBaseEntity;
struct FDisplayParam;

class LOKAGAME_API SItemProperty : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SItemProperty)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FItemPropertyStyle>("SItemPropertyStyle"))
		, _IsShowedModifers(true)
	{}
	SLATE_STYLE_ARGUMENT(FItemPropertyStyle, Style)
	SLATE_ARGUMENT(bool, IsShowedModifers)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, const UItemBaseEntity* InInstance);

	void BuildWidget();
	void BuildWidget(const UItemBaseEntity* InInstance);

protected:

	const FItemPropertyStyle* Style;

	TSharedPtr<SVerticalBox> Container;
	TArray<TSharedPtr<FDisplayParam>> DisplayParams, DisplayImprovements;
	bool IsShowedModifers;

	const UItemBaseEntity* Instance;
};
