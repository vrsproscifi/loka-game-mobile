#include "LokaGame.h"
#include "SResourceTextBoxValue.h"
#include "SResourceTextBoxType.h"

FResourceTextBoxValue FResourceTextBoxValue::Money(const int64& InValue)
{
	return FResourceTextBoxValue(EResourceTextBoxType::Money, InValue);
}

FResourceTextBoxValue FResourceTextBoxValue::Donate(const int64& InValue)
{
	return FResourceTextBoxValue(EResourceTextBoxType::Donate, InValue);
}
