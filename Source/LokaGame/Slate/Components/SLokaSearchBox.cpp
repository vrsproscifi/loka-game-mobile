// VRSPRO

#include "LokaGame.h"
#include "SLokaSearchBox.h"
#include "SlateOptMacros.h"

#include "STextBlockButton.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SLokaSearchBox::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	OnSearch = InArgs._OnSearch;

	SEditableTextBox::Construct(SEditableTextBox::FArguments()
		.Style(&FLokaStyle::Get().GetWidgetStyle<FEditableTextBoxStyle>("Default_EditableTextBox"))
		.Text(InArgs._InitialText)
		.HintText(InArgs._HintText)
		.SelectAllTextWhenFocused(InArgs._SelectAllTextWhenFocused)
		.RevertTextOnEscape(false)
		.ClearKeyboardFocusOnCommit(false)
		.MinDesiredWidth(InArgs._MinDesiredWidth)
		.OnTextChanged(this, &SLokaSearchBox::HandleTextChanged)
		.OnTextCommitted(this, &SLokaSearchBox::HandleTextCommitted)
	);

	int32 SlotIndex = Box->NumSlots();

	FString sSearch, sCancel;

	ASSIGN_UTF8_SYMBOL(sSearch, 0xf002);
	ASSIGN_UTF8_SYMBOL(sCancel, 0xf00d);

	Box->InsertSlot(SlotIndex++)
		.AutoWidth()
		.HAlign(HAlign_Center)
		.VAlign(VAlign_Center)
		.Padding(FMargin(6, 0))
		[
			SNew(STextBlockButton)
			.Text(FText::FromString(sSearch))
			.Style(&Style->Glass)
			.Visibility_Lambda([&]() {return IsShowingCancel ? EVisibility::Collapsed : EVisibility::Visible; })
			.OnClicked(this, &SLokaSearchBox::OnClickedTextButton, false)
		];

	Box->InsertSlot(SlotIndex++)
		.AutoWidth()
		.HAlign(HAlign_Center)
		.VAlign(VAlign_Center)
		.Padding(FMargin(6, 0))
		[
			SNew(STextBlockButton)
			.Text(FText::FromString(sCancel))
			.Style(&Style->Cancel)
			.Visibility_Lambda([&]() {return IsShowingCancel ? EVisibility::Visible : EVisibility::Collapsed; })
			.OnClicked(this, &SLokaSearchBox::OnClickedTextButton, true)
		];

	IsShowingCancel = false;
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FReply SLokaSearchBox::OnClickedTextButton(const bool IsCancel)
{
	if (IsCancel)
	{
		EditableText->SetText(FText::GetEmpty());
		HandleTextCommitted(FText::GetEmpty(), ETextCommit::OnCleared);
	}
	else
	{
		if (EditableText->GetText().IsEmpty())
		{
			FSlateApplication::Get().SetUserFocus(0, EditableText, EFocusCause::Mouse);
		}
		else
		{
			HandleTextCommitted(EditableText->GetText(), ETextCommit::OnEnter);
		}
	}

	return FReply::Handled();
}

void SLokaSearchBox::HandleTextChanged(const FText& Text)
{
	if (Text.IsEmpty())
	{
		OnClickedTextButton(true);
		IsShowingCancel = false;
	}
	else
	{
		IsShowingCancel = true;
	}
}

void SLokaSearchBox::HandleTextCommitted(const FText& Text, ETextCommit::Type CommitType)
{
	OnSearch.ExecuteIfBound(Text, CommitType);
}