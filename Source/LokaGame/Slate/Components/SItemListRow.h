// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Slate/Styles/ItemListRowWidgetStyle.h"
/**
 * 
 */
class LOKAGAME_API SItemListRow : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SItemListRow)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FItemListRowStyle>("SItemListRowStyle"))
		, _Name(NSLOCTEXT("SItemListRow", "Default.Name", "Name"))
		, _SubName(NSLOCTEXT("SItemListRow", "Default.SubName", "SubName"))
		, _ButtonName(NSLOCTEXT("SItemListRow", "Default.ButtonName", "ButtonName"))
		, _AmountText(NSLOCTEXT("SItemListRow", "Default.AmountText", "10/64"))
	{}
	SLATE_ARGUMENT(FText, Name)
	SLATE_ARGUMENT(FText, SubName)
	SLATE_ARGUMENT(FText, ButtonName)
	SLATE_ARGUMENT(FText, AmountText)
	SLATE_EVENT(FOnClicked, OnClicked)
	SLATE_STYLE_ARGUMENT(FItemListRowStyle, Style)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	enum ETarget
	{
		Name,
		SubName,
		Button,
		Amount,
		End
	};

	void SetText(const ETarget _target, const FText &_text);

protected:

	const FItemListRowStyle *Style;

	TSharedPtr<class STextBlock> Text[ETarget::End];
};
