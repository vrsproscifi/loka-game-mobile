// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SlateBasics.h"
#include "Utilities/SUsableCompoundWidget.h"
#include "Styles/VerticalMenuBuilderWidgetStyle.h"
#include "Styles/BackgroundBlurWidgetStyle.h"

DECLARE_DELEGATE_OneParam(FOnClickAnyButton, uint8)

//struct FVerticalMenuButton
//{
//	TSharedPtr<SButton> Button;
//	TSharedPtr<STextBlock> ButtonText;
//
//	FVerticalMenuButton() : Button(), ButtonText() {}
//};

class LOKAGAME_API SVerticalMenuBuilder : public SUsableCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SVerticalMenuBuilder)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FVerticalMenuBuilderStyle>(TEXT("SVerticalMenuBuilderStyle")))
		, _BlurStyle(&FLokaStyle::Get().GetWidgetStyle<FBackgroundBlurStyle>("Back_BackgroundBlur"))
		, _ButtonsStyle(nullptr)
		, _ButtonsTextStyle(nullptr)
		, _IsDoubleClickProtection(true)
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_ARGUMENT(TArray<FText>, ButtonsText)
	SLATE_ARGUMENT(bool, IsDoubleClickProtection)
	SLATE_STYLE_ARGUMENT(FVerticalMenuBuilderStyle, Style)
	SLATE_STYLE_ARGUMENT(FButtonStyle, ButtonsStyle)
	SLATE_STYLE_ARGUMENT(FTextBlockStyle, ButtonsTextStyle)
	SLATE_STYLE_ARGUMENT(FBackgroundBlurStyle, BlurStyle)
	SLATE_EVENT(FOnClickAnyButton, OnClickAnyButton)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);
	virtual void ToggleWidget(const bool) override;
	bool IsVisible() const;
	void SetButtons(const TArray<FText>&);

	FOnClickAnyButton OnClickAnyButton;

	virtual bool IsInInteractiveMode() const override { return IsVisible(); }

private:

	TSharedPtr<SVerticalBox> ContainerButtons;

	void EnableButtons();

protected:

	bool bIsDoubleClickProtection;

	const FVerticalMenuBuilderStyle *Style;
	const FBackgroundBlurStyle *BlurStyle;
	const FButtonStyle *ButtonsStyle;
	const FTextBlockStyle *ButtonsTextStyle;

	//FVerticalMenuButton MakeMenuButton(const FText *ButtonText);

	FReply OnClickMenuButtonSender(const SIZE_T Index);

	TOptional<FSlateRenderTransform> WidgetScale() const;
	FSlateColor WidgetContainerColor() const;
	FSlateColor WidgetBorderColor() const;
	FSlateColor WidgetBordersColor() const;
	FSlateColor WidgetButtonsColor() const;
	FSlateColor WidgetButtonsTextColor() const;
	float WidgetBlurStrength() const;
	TOptional<int32> WidgetBlurRadius() const;

	FCurveSequence ShowAnim;
	FCurveHandle ShowBorderColorHandle;
	FCurveHandle ShowContainerColorHandle;
	FCurveHandle ShowBordersColorHandle;
	FCurveHandle ShowButtonsColorHandle;
	FCurveHandle ShowTransHandle;

	TArray<TSharedPtr<SButton>> Buttons;
};
