// VRSPRO

#include "LokaGame.h"
#include "SWindowEverydayBonus.h"
#include "SlateOptMacros.h"

#include "SResourceTextBox.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SWindowEverydayBonus::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	const int32 _DayNumber = InArgs._DayNumber;
	const int32 _StartNumber = (_DayNumber < 3) ? 1 : _DayNumber - 2;

	auto Container = SNew(SHorizontalBox);

	for (int32 i = _StartNumber; i < _StartNumber + 5; ++i)
	{
		Container->AddSlot().Padding(4)
			[
				SNew(SBox).WidthOverride(260).HeightOverride(180)
				[
					SNew(SBorder).Padding(0)
					.BorderImage((_DayNumber == i) ? &Style->Background[EWindowEverydayBonusBorder::Active] : ((_DayNumber > i) ? &Style->Background[EWindowEverydayBonusBorder::Complete] : &Style->Background[EWindowEverydayBonusBorder::Next]))
					[
						SNew(SBorder).Padding(0)
						.BorderImage((_DayNumber == i) ? &Style->Border[EWindowEverydayBonusBorder::Active] : ((_DayNumber > i) ? &Style->Border[EWindowEverydayBonusBorder::Complete] : &Style->Border[EWindowEverydayBonusBorder::Next]))
						[
							SNew(SBorder).Padding(0)
							.BorderImage((_DayNumber == i) ? &Style->Shadow[EWindowEverydayBonusBorder::Active] : ((_DayNumber > i) ? &Style->Shadow[EWindowEverydayBonusBorder::Complete] : &Style->Shadow[EWindowEverydayBonusBorder::Next]))
							[
								SNew(SOverlay)
								+ SOverlay::Slot().HAlign(HAlign_Center).VAlign(VAlign_Center)
								[
									SNew(SResourceTextBox)
									.TypeAndValue(FResourceTextBoxValue::Money(100*i))
								]
								+ SOverlay::Slot().HAlign(HAlign_Center).VAlign(VAlign_Bottom).Padding(2, 6)
								[	
									SNew(STextBlock)
									.TextStyle(&Style->DayText)
									.Text(FText::Format(NSLOCTEXT("Notify", "Notify.WeekBonus.DescDay", "Day {0}"), FText::AsNumber(i)))
								]
							]
						]
					]
				]
			];
	}
	

	ChildSlot
	[
		SNew(SVerticalBox)
		+ SVerticalBox::Slot().HAlign(HAlign_Center).Padding(20, 4).AutoHeight()
		[
			SNew(STextBlock)
			.Text(NSLOCTEXT("Notify", "Notify.WeekBonus.DescAdvance", "Coming every day, you'll receive the following bonuses."))
			.TextStyle(&Style->DescText)
		]
		+ SVerticalBox::Slot().AutoHeight()
		[
			Container
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION
