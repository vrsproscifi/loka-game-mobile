// VRSPRO

#pragma once

#include "Widgets/Text/STextBlock.h"
#include "Slate/Styles/TextBlockButtonWidgetStyle.h"

/**
 * 
 */
class LOKAGAME_API STextBlockButton : public STextBlock
{	
public:
	SLATE_BEGIN_ARGS(STextBlockButton)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FTextBlockButtonStyle>("STextBlockButtonStyle"))
		, _ClickMethod(EButtonClickMethod::DownAndUp)
	{}
	SLATE_STYLE_ARGUMENT(FTextBlockButtonStyle, Style)
	SLATE_ARGUMENT(STextBlock::FArguments, Origin)
	SLATE_EVENT(FOnClicked, OnClicked)
	SLATE_ARGUMENT(EButtonClickMethod::Type, ClickMethod)
	SLATE_ARGUMENT(FText, Text)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

	void SetStyle(const FTextBlockButtonStyle *_Style);

protected:

	virtual FReply OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;
	virtual FReply OnMouseButtonUp(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;

	virtual void OnMouseEnter(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;
	virtual void OnMouseLeave(const FPointerEvent& MouseEvent) override;

	const FTextBlockButtonStyle *Style;

	FOnClicked OnClickedEvent;

	EButtonClickMethod::Type ClickMethod;

	bool bIsPressed;

	void Press();
	void Release();
};
