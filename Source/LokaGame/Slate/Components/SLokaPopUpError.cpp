// VRSPRO

#include "LokaGame.h"
#include "SLokaPopUpError.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SLokaPopUpError::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;


	TAttribute<FSlateFontInfo> Font = InArgs._Font.IsSet() ? InArgs._Font : Style->Font;
	TAttribute<FSlateColor> FontColorAndOpacity = InArgs._FontColorAndOpacity.IsSet() ? InArgs._FontColorAndOpacity : Style->FontColor;
	TAttribute<const FSlateBrush*> BackgroundImage = InArgs._BackgroundImage.IsSet() ? InArgs._BackgroundImage : &Style->Background;
	TAttribute<FSlateColor> BackgroundColorAndOpacity = InArgs._BackgroundColorAndOpacity.IsSet() ? InArgs._BackgroundColorAndOpacity : (InArgs._BackgroundImage.IsSet() ? FColor::White : FColor::Red);

	SComboButton::Construct(SComboButton::FArguments()
		.ComboButtonStyle(FCoreStyle::Get(), "MessageLogListingComboButton")
		.HasDownArrow(false)
		.ContentPadding(0)
		.ButtonContent()
		[
			SAssignNew(HasErrorSymbol, SBorder)
			.Visibility(EVisibility::Hidden)
			.BorderImage(BackgroundImage)
			.BorderBackgroundColor(BackgroundColorAndOpacity)
			.HAlign(EHorizontalAlignment::HAlign_Center)
			.VAlign(EVerticalAlignment::VAlign_Center)
			.Padding(FMargin(10, 2))
			[
				SNew(STextBlock)
				.Font(Font)
				.ColorAndOpacity(FontColorAndOpacity)
				.Text(NSLOCTEXT("SLokaPopUpError", "Error.Symbol", "!"))
			]
		]
		.MenuPlacement(MenuPlacement_BelowAnchor)
		.MenuContent()
		[
			SAssignNew(ErrorTextContainer, SBorder)
			.BorderImage(BackgroundImage)
			.BorderBackgroundColor(BackgroundColorAndOpacity)
			.HAlign(EHorizontalAlignment::HAlign_Center)
			.VAlign(EVerticalAlignment::VAlign_Center)
			.Padding(FMargin(10, 2))
			[
				SAssignNew(ErrorText, STextBlock)
				.Font(Font)
				.ColorAndOpacity(FontColorAndOpacity)
			]
		]
	);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SLokaPopUpError::SetError(const FText & InErrorText)
{
	const bool bHasError = !InErrorText.IsEmpty();

	ErrorText->SetText(InErrorText);
	HasErrorSymbol->SetVisibility(bHasError ? EVisibility::Visible : EVisibility::Hidden);

	this->SetIsOpen(bHasError, false);
}

void SLokaPopUpError::SetError(const FString & InErrorText)
{
	SetError(FText::FromString(InErrorText));
}

bool SLokaPopUpError::HasError() const
{
	return !ErrorText->GetText().IsEmpty();
}
TSharedRef<SWidget> SLokaPopUpError::AsWidget()
{
	return SharedThis(this);
}

