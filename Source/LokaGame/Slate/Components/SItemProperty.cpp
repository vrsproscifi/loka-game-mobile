// VRSPRO

#include "LokaGame.h"
#include "SItemProperty.h"
#include "SlateOptMacros.h"

#include "Item/ItemBaseEntity.h"

#define LOCTEXT_NAMESPACE "SItemProperty"

class LOKAGAME_API SItemProperty_Row : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SItemProperty_Row)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FItemPropertyStyle>("SItemPropertyStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FItemPropertyStyle, Style)
	SLATE_ATTRIBUTE(float, Value)
	SLATE_ARGUMENT(EValueDisplayAs::Type, DisplayAs)
	SLATE_ARGUMENT(FText, Text)
	SLATE_END_ARGS()

	BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
	void Construct(const FArguments& InArgs)
	{
		Style = InArgs._Style;
		Value = InArgs._Value;
		DisplayAs = InArgs._DisplayAs;

		ChildSlot
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot().AutoWidth().Padding(FMargin(10, 0))
			[
				SNew(STextBlock).Text(InArgs._Text).TextStyle(&Style->RowTextName)
			]
			+ SHorizontalBox::Slot().FillWidth(1).HAlign(HAlign_Right).Padding(FMargin(40, 0, 10, 0))
			[
				SNew(STextBlock).Text(this, &SItemProperty_Row::GetFormatedValue).Justification(ETextJustify::Right).TextStyle(&Style->RowTextValue)
			]
		];
	}
	END_SLATE_FUNCTION_BUILD_OPTIMIZATION

protected:

	const FItemPropertyStyle* Style;

	TAttribute<float> Value;
	EValueDisplayAs::Type DisplayAs;

	FText GetFormatedValue() const
	{
		FText ReaturnText = FText::GetEmpty();

		switch (DisplayAs)
		{
		case EValueDisplayAs::Float:
			ReaturnText = FText::AsNumber(Value.Get());
			break;
		case EValueDisplayAs::Meters:
			ReaturnText = FText::Format(LOCTEXT("Property.Dist", "{0} m"), FText::AsNumber(Value.Get()));
			break;
		case EValueDisplayAs::Kilograms:
			ReaturnText = FText::Format(LOCTEXT("Property.Mass", "{0} kg"), FText::AsNumber(Value.Get()));
			break;
		case EValueDisplayAs::FireRate:
			ReaturnText = FText::Format(LOCTEXT("Property.Fire", "{0} s/min"), FText::AsNumber(FMath::CeilToInt(Value.Get())));
			break;
		case EValueDisplayAs::Degres:
			ReaturnText = FText::Format(LOCTEXT("Property.Deg", "{0} deg"), FText::AsNumber(Value.Get()));
			break;
		case EValueDisplayAs::Seconds:
			ReaturnText = FText::Format(LOCTEXT("Property.Duration", "{0} sec"), FText::AsNumber(Value.Get()));
			break;
		case EValueDisplayAs::Percent:
			ReaturnText = FText::AsPercent(Value.Get() / 100.0f);
			break;
		case EValueDisplayAs::End:
			break;
		default:
			break;
		}	

		return ReaturnText;
	}
};


BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SItemProperty::Construct(const FArguments& InArgs, const UItemBaseEntity* InInstance)
{
	Style = InArgs._Style;
	Instance = InInstance;
	IsShowedModifers = InArgs._IsShowedModifers;

	ChildSlot
	[
		SAssignNew(Container, SVerticalBox)
	];

	BuildWidget();
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SItemProperty::BuildWidget()
{
	if (Instance)
	{
		Container->ClearChildren();
		DisplayParams.Empty();
		DisplayImprovements.Empty();

		{
			auto Values = Instance->GetDisplayParams();

			for (auto &Value : Values)
			{
				DisplayParams.Add(MakeShareable(new FDisplayParam(Value)));
			}

			auto Params = SNew(SListView<TSharedPtr<FDisplayParam>>)
				.SelectionMode(ESelectionMode::None)
				.ListItemsSource(&DisplayParams)
				.OnGenerateRow_Lambda([&](TSharedPtr<FDisplayParam> Item, const TSharedRef<STableViewBase>& OwnerTable) 
			{
				return SNew(STableRow<TSharedPtr<FDisplayParam>>, OwnerTable).Padding(FMargin(0, 2)).Style(&Style->Row)
				[
					SNew(SItemProperty_Row).Text(Item->Name).Value(Item->Value).DisplayAs(Item->DisplayAs)
				];
			});

			Container->AddSlot().AutoHeight()
				[
					Params
				];
		}

		if (IsShowedModifers)
		{
			auto Values = Instance->GetDisplayModifers();

			Container->AddSlot().AutoHeight().Padding(FMargin(10, 6)).VAlign(VAlign_Center)
				[
					SNew(STextBlock).Text(FText::Format(NSLOCTEXT("SItemProperty", "Improvements", "Improvements {0}/5"), FText::AsNumber(Values.Num()))).Justification(ETextJustify::Center)
				];

			if (Values.Num())
			{
				for (auto &Value : Values)
				{
					DisplayImprovements.Add(MakeShareable(new FDisplayParam(Value)));
				}

				auto Params = SNew(SListView<TSharedPtr<FDisplayParam>>)
					.SelectionMode(ESelectionMode::None)
					.ListItemsSource(&DisplayImprovements)
					.OnGenerateRow_Lambda([&](TSharedPtr<FDisplayParam> Item, const TSharedRef<STableViewBase>& OwnerTable)
				{
					return SNew(STableRow<TSharedPtr<FDisplayParam>>, OwnerTable).Padding(FMargin(0, 2)).Style(&Style->Row)
						[
							SNew(SItemProperty_Row).Text(Item->Name).Value(Item->Value).DisplayAs(Item->DisplayAs)
						];
				});

				Container->AddSlot().AutoHeight()
					[
						Params
					];
			}
		}
	}
}

void SItemProperty::BuildWidget(const UItemBaseEntity* InInstance)
{
	Instance = InInstance;

	BuildWidget();
}

#undef LOCTEXT_NAMESPACE