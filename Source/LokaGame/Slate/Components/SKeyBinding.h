// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/KeyBindingWidgetStyle.h"

/**
 * 
 */
class LOKAGAME_API SKeyBinding : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SKeyBinding)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FKeyBindingStyle>("SKeyBindingStyle"))
		, _WithBackground(true)
		, _Key(EKeys::Invalid)
		, _IsAlternative(false)
	{}
	SLATE_STYLE_ARGUMENT(FKeyBindingStyle, Style)
	SLATE_ARGUMENT(bool, WithBackground)
	SLATE_ATTRIBUTE(bool, IsAlternative)
	SLATE_ATTRIBUTE(FKey, Key)
	SLATE_ATTRIBUTE(FString, Binding)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

protected:

	const FKeyBindingStyle* Style;

	TAttribute<FKey> Key;
	TAttribute<FString> Binding;
	TAttribute<bool> IsAlternative;

	FText GetKeyText() const;
};
