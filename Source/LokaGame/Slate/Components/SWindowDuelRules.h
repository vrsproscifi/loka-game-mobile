//// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/ResourceTextBoxWidgetStyle.h" 
#include "Styles/WindowDuelRulesWidgetStyle.h"
#include "Weapon/WeaponContainer.h"
#include "GameModeTypeId.h"
#include "SSliderNumber.h"
#include "Shared/SessionMatchOptions.h"
#include "SSpinNumber.h"

class LOKAGAME_API SWindowDuelRules : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SWindowDuelRules)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FWindowDuelRulesStyle>("SWindowDuelRulesStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FWindowDuelRulesStyle, Style)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

	FSessionMatchOptions GetOptions() const;

protected:

	const FResourceTextBoxStyle* ResourceStyle;
	const FWindowDuelRulesStyle* Style;

	EGameCurrency::Type CurrentCurrencyType;
	EGameMap CurrentSelectedMap;
	EWeaponType CurrentWeaponType;
	TSharedPtr<SSpinNumber<int32>> Widget_SpinMoney, Widget_SpinLifes;

	FText GetCurrencyToolTip(const int32) const;
};
