// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"

/**
 * 
 */
class LOKAGAME_API SCharacteristicParameter : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SCharacteristicParameter)
	{}
	SLATE_ATTRIBUTE(FText, Name)
	SLATE_ATTRIBUTE(float, Value)
	SLATE_ATTRIBUTE(const FSlateBrush*, Icon)
	SLATE_STYLE_ARGUMENT(FTextBlockStyle, TextStyleName)
	SLATE_STYLE_ARGUMENT(FTextBlockStyle, TextStyleValue)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

protected:

	TAttribute<float> ValueTypeAttribute;

	FText GetValueText() const;
};
