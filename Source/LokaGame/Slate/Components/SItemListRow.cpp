// VRSPRO

#include "LokaGame.h"
#include "SItemListRow.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SItemListRow::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	ChildSlot.HAlign(Style->RootSlot.HAlign).VAlign(Style->RootSlot.VAlign).Padding(Style->RootSlot.Margin)
	[
		SNew(SBorder)
		.Padding(0)
		.BorderImage(&Style->Background)
		[
			SNew(SBorder)
			.Padding(0)
			.BorderImage(&Style->Border)
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot()
				.VAlign(Style->ImageSlot.VAlign)
				.HAlign(Style->ImageSlot.HAlign)
				.Padding(Style->ImageSlot.Margin)
				[
					SNew(SImage)
					.Image(&Style->DefaultImage)
				]
				+ SHorizontalBox::Slot()
				.VAlign(EVerticalAlignment::VAlign_Center)
				[
					SNew(SVerticalBox)
					+ SVerticalBox::Slot()
					[
						SAssignNew(Text[ETarget::Name], STextBlock)
						.TextStyle(&Style->TextName)
						.Text(InArgs._Name)
					]
					+ SVerticalBox::Slot()
					[
						SAssignNew(Text[ETarget::SubName], STextBlock)
						.TextStyle(&Style->TextSubName)
						.Text(InArgs._SubName)
					]
				]
				+ SHorizontalBox::Slot()
				.VAlign(EVerticalAlignment::VAlign_Center)
				.HAlign(EHorizontalAlignment::HAlign_Center)
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot()
					.VAlign(EVerticalAlignment::VAlign_Center)
					.HAlign(EHorizontalAlignment::HAlign_Center)
					[
						SNew(SImage)
						.Image(&Style->DefaultIcon)
					]
					+ SHorizontalBox::Slot()
					.VAlign(EVerticalAlignment::VAlign_Center)
					.AutoWidth()
					[
						SAssignNew(Text[ETarget::Amount], STextBlock)
						.TextStyle(&Style->TextAmount)
						.Text(InArgs._AmountText)
					]
				]
				+ SHorizontalBox::Slot()
				.VAlign(EVerticalAlignment::VAlign_Center)
				.HAlign(EHorizontalAlignment::HAlign_Center)
				[
					SNew(SButton)
					.ButtonStyle(&Style->Button)
					.OnClicked(InArgs._OnClicked)
					[
						SAssignNew(Text[ETarget::Button], STextBlock)
						.TextStyle(&Style->TextButton)
						.Text(InArgs._ButtonName)
					]
				]
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SItemListRow::SetText(const ETarget _target, const FText & _text)
{
	if (_target < ETarget::End)
	{
		Text[_target]->SetText(_text);
	}
}
