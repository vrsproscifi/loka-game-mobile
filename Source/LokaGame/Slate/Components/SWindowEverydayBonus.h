// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/WindowEverydayBonusWidgetStyle.h"


class LOKAGAME_API SWindowEverydayBonus : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SWindowEverydayBonus)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FWindowEverydayBonusStyle>("SWindowEverydayBonusStyle"))
		, _DayNumber(1)
	{}
	SLATE_STYLE_ARGUMENT(FWindowEverydayBonusStyle, Style)
	SLATE_ARGUMENT(int32, DayNumber)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

protected:

	const FWindowEverydayBonusStyle* Style;
};
