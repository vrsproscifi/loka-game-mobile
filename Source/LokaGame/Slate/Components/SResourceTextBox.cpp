// VRSPRO

#include "LokaGame.h"
#include "SResourceTextBox.h"
#include "SlateOptMacros.h"

#include "SLokaToolTip.h"
#include "Decorators/LokaTextDecorator.h"

#include "SResourceTextBoxType.h"
#include "RichTextHelpers.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SResourceTextBox::Construct(const FArguments& InArgs)
{
	Style = new FResourceTextBoxStyle(*InArgs._Style);
	OnClicked = InArgs._OnClicked;

	SetCustomSize(InArgs._CustomSize);

	ChildSlot
	[
		SNew(SHorizontalBox)
		+ SHorizontalBox::Slot()
		.AutoWidth()
		.HAlign(InArgs._ImageHAlign)
		.VAlign(InArgs._ImageVAlign)
		[
			SAssignNew(Image, SImage)
			.Image(this, &SResourceTextBox::GetCurrentImage)
			.OnMouseButtonDown(this, &SResourceTextBox::OnClickedOnImage)
		]
		+ SHorizontalBox::Slot()
		.FillWidth(1)
		.VAlign(InArgs._TextVAlign)
		.HAlign(InArgs._TextHAlign)
		[
			SAssignNew(TextBlock, STextBlock)
			.Font(this, &SResourceTextBox::GetCurrentFont)
			.ColorAndOpacity(this, &SResourceTextBox::GetCurrentFontColor)
			.Text(this, &SResourceTextBox::GetCurrentValue)
			.Margin(FMargin(10, 0, 0, 0))
			.Visibility(this, &SResourceTextBox::GetCurrentVisibilityText)
		]
	];

	SetTypeAndValue(InArgs._TypeAndValue);
	SetType(InArgs._Type);
	SetValue(InArgs._Value);

	auto ToolTipStyle = &FLokaStyle::Get().GetWidgetStyle<FLokaToolTipStyle>("SLokaToolTipStyle");

	SetToolTip(SNew(SLokaToolTip).Content()
	[
		SNew(SRichTextBlock)
		.Text(this, &SResourceTextBox::GetCurrentToolTip)
		.AutoWrapText(true)
		.TextStyle(&FTextBlockStyle().SetFont(ToolTipStyle->Font).SetColorAndOpacity(ToolTipStyle->FontColor))
		.DecoratorStyleSet(&FLokaStyle::Get())
		.AutoWrapText(true)
		.WrapTextAt(320)
		+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(TMap<FName, FSlateFontInfo>(), ToolTipStyle->Font, ToolTipStyle->FontColor.GetSpecifiedColor()))
	]);		
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FReply SResourceTextBox::OnClickedOnImage(const FGeometry& InGeometry, const FPointerEvent& InEvent)
{
	if (OnClicked.IsBound())
	{
		return OnClicked.Execute();
	}

	return FReply::Handled();
}

const int64 SResourceTextBox::GetValue() const
{
	if (Attr_TypeAndValue.IsSet())
	{
		return Attr_TypeAndValue.Get().Value;
	}

	return Attr_ValueAmount.Get();
}

const EResourceTextBoxType::Type SResourceTextBox::GetValueType() const
{
	if (Attr_TypeAndValue.IsSet())
	{
		return Attr_TypeAndValue.Get().Type;
	}

	return Attr_ValueType.Get();
}

void SResourceTextBox::SetTypeAndValue(const TAttribute<FResourceTextBoxValue>& InTypeAndValueAttr)
{
	Attr_TypeAndValue = InTypeAndValueAttr;
}

void SResourceTextBox::SetType(const TAttribute<EResourceTextBoxType::Type>& InTypeAttr)
{
	Attr_ValueType = InTypeAttr;
}

void SResourceTextBox::SetValue(const TAttribute<int64>& InValueAttr)
{
	Attr_ValueAmount = InValueAttr;
}

void SResourceTextBox::SetValue(const FText &InCustomText)
{
	TextBlock->SetText(InCustomText);
}

void SResourceTextBox::SetCustomSize(const FVector& Size)
{
	if (Size == FVector::ZeroVector)
	{
		delete Style;
		Style = new FResourceTextBoxStyle(FLokaStyle::Get().GetWidgetStyle<FResourceTextBoxStyle>("SResourceTextBoxStyle"));
	}
	else
	{
		for (auto &s : Style->DefaultStyles)
		{
			s.Value.Image.ImageSize = FVector2D(Size.X, Size.Y);
			s.Value.TextBlock.Font.Size = Size.Z;
		}
	}
}

FText SResourceTextBox::GetCurrentValue() const
{
	if (GetValueType() == EResourceTextBoxType::Time)
	{
		return GetCurrentValueAsTime();
	}

	return FText::AsNumber(GetValue());
}

FText SResourceTextBox::GetCurrentValueAsTime() const
{
	auto Time = FDateTime::FromUnixTimestamp(GetValue());

	if (Time.GetHour())
	{
		return FText::FromString(Time.ToString(TEXT("%H:%M:%S")));
	}
	else if (Time.GetMinute())
	{
		return FText::FromString(Time.ToString(TEXT("%M:%S")));
	}
	else
	{
		return FText::FromString(Time.ToString(TEXT("%S")));
	}
}

const FSlateBrush* SResourceTextBox::GetCurrentImage() const
{
	return &GetResourceStyle()->Image;
}

FSlateFontInfo SResourceTextBox::GetCurrentFont() const
{
	return GetResourceStyle()->TextBlock.Font;
}

FSlateColor SResourceTextBox::GetCurrentFontColor() const
{
	return GetResourceStyle()->TextBlock.ColorAndOpacity;
}

FText SResourceTextBox::GetCurrentToolTip() const
{
	const auto CurrentType = GetValueType();

	if (CurrentType >= EResourceTextBoxType::Premium && CurrentType <= EResourceTextBoxType::BoostReputation)
	{
		FText AppendText;
		FRichHelpers::FTextHelper OptText;
		OptText.SetColor(FColorList::BrightGold);
		const auto CurrentTime = FDateTime::UtcNow().ToUnixTimestamp();

		switch (CurrentType)
		{
			case EResourceTextBoxType::Premium: AppendText = NSLOCTEXT("SResourceTextBox", "SResourceTextBox.Premium", "A premium account provides you with increase to the awards for the battle in the amount of 50% is obtained."); break;
			case EResourceTextBoxType::BoostMoney: AppendText = NSLOCTEXT("SResourceTextBox", "SResourceTextBox.BoostMoney", "Booster money gives you 20% of the received money for the fight."); break;
			case EResourceTextBoxType::BoostExperience: AppendText = NSLOCTEXT("SResourceTextBox", "SResourceTextBox.BoostExperience", "Booster experience gives you 20% of the experience per battle."); break;
			case EResourceTextBoxType::BoostReputation: AppendText = NSLOCTEXT("SResourceTextBox", "SResourceTextBox.BoostReputation", "Booster reputation gives you 20% obtained reputation in battle."); break;
		}

		if (GetValue() > CurrentTime)
		{
			FText AppendTextOpt = NSLOCTEXT("SResourceTextBox", "SResourceTextBox.TimeLeft", "[Time left ");
			auto TimeString = AppendTextOpt.ToString();
			TimeString += FTimespan::FromSeconds(CurrentTime - GetValue()).ToString(TEXT("%d:%h:%m]"));

			return FText::Format(FText::FromString("{0}\n\n{1}"), AppendText, OptText.SetValue(TimeString).ToText());
		}
		else
		{
			return FText::Format(FText::FromString("{0}\n\n{1}"), AppendText, OptText.SetValue(NSLOCTEXT("SResourceTextBox", "SResourceTextBox.ClickToBuy", "[Click to buy]")).ToText());
		}		
	}

	switch (CurrentType)
	{
		case EResourceTextBoxType::Money: return NSLOCTEXT("SResourceTextBox", "SResourceTextBox.Money", "Crypto-dollars, in-game currency.");
		case EResourceTextBoxType::Donate: return NSLOCTEXT("SResourceTextBox", "SResourceTextBox.Donate", "Energetic-crystal, rare currency, is issued for the victory in PvP battles.");
		case EResourceTextBoxType::Coin: return NSLOCTEXT("SResourceTextBox", "SResourceTextBox.Coin", "Satoshi (part of Bitcoin), real crypto-currency.");
		case EResourceTextBoxType::Time: return NSLOCTEXT("SResourceTextBox", "SResourceTextBox.Time", "Time.");
		case EResourceTextBoxType::RealMoney: return NSLOCTEXT("SResourceTextBox", "SResourceTextBox.RealMoney", "Real currency.");
		case EResourceTextBoxType::Level: return NSLOCTEXT("SResourceTextBox", "SResourceTextBox.Level", "Account level.");
	}

	return FText::GetEmpty();
}

EVisibility SResourceTextBox::GetCurrentVisibilityText() const
{
	const auto CurrentType = GetValueType();
	return ((CurrentType >= EResourceTextBoxType::Premium && CurrentType <= EResourceTextBoxType::BoostReputation) || GetCurrentFont().Size == 0) ? EVisibility::Collapsed : EVisibility::Visible;
}

EResourceTextBoxType::Type SResourceTextBox::ServiceModelToType(const ServiceTypeId::Type& InModel)
{
	if(InModel == ServiceTypeId::PremiumAccount) return EResourceTextBoxType::Premium;
	if(InModel == ServiceTypeId::BoosterOfExperience) return EResourceTextBoxType::BoostExperience;
	if(InModel == ServiceTypeId::BoosterOfReputation) return EResourceTextBoxType::BoostReputation;
	if(InModel == ServiceTypeId::BoosterOfMoney) return EResourceTextBoxType::BoostMoney;

	return EResourceTextBoxType::Custom;
}

const FRTBStyles* SResourceTextBox::GetResourceStyle() const
{
	const auto localType = GetValueType();
	if (Style->DefaultStyles.Contains(localType))
	{
		return &Style->DefaultStyles.FindChecked(localType);
	}

	return nullptr;
}
