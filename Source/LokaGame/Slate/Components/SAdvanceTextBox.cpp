﻿// VRSPRO

#include "LokaGame.h"
#include "SAdvanceTextBox.h"
#include "SlateOptMacros.h"

#include "STextBlockButton.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SAdvanceTextBox::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	OnAcceptText = InArgs._OnAcceptText;

	SEditableTextBox::Construct(SEditableTextBox::FArguments()
		.Style(&Style->TextBoxStyle)
		.Text(InArgs._InitialText)
		.HintText(InArgs._HintText)
		.SelectAllTextWhenFocused(InArgs._SelectAllTextWhenFocused)
		.RevertTextOnEscape(true)
		.ClearKeyboardFocusOnCommit(false)
		.MinDesiredWidth(InArgs._MinDesiredWidth)
		.IsReadOnly(true)
		.ErrorReporting(InArgs._ErrorReporting)
	);

	MAKE_UTF8_SYMBOL(strStartEdit, 0xf040);
	MAKE_UTF8_SYMBOL(strAcceptEdit, 0xf00c);
	MAKE_UTF8_SYMBOL(strCancelEdit, 0xf00d);

	Box->AddSlot()
	.AutoWidth()
	.HAlign(EHorizontalAlignment::HAlign_Center)
	.VAlign(EVerticalAlignment::VAlign_Center)
	[
		SAssignNew(ButtonStartEdit, STextBlockButton)
		.Style(&Style->ButtonEdit)
		.OnClicked(this, &SAdvanceTextBox::StartEdit)
		.Text(FText::FromString(strStartEdit))
	];

	Box->AddSlot()
	.AutoWidth()
	.HAlign(EHorizontalAlignment::HAlign_Center)
	.VAlign(EVerticalAlignment::VAlign_Center)
	[
		SAssignNew(ButtonAcceptEdit, STextBlockButton)
		.Style(&Style->ButtonAccept)
		.OnClicked(this, &SAdvanceTextBox::AcceptEdit)
		.Visibility(EVisibility::Collapsed)
		.Text(FText::FromString(strAcceptEdit))
	];

	Box->AddSlot()
	.AutoWidth()
	.HAlign(EHorizontalAlignment::HAlign_Center)
	.VAlign(EVerticalAlignment::VAlign_Center)
	[
		SAssignNew(ButtonCancelEdit, STextBlockButton)
		.Style(&Style->ButtonCancel)
		.OnClicked(this, &SAdvanceTextBox::CancelEdit)
		.Visibility(EVisibility::Collapsed)
		.Text(FText::FromString(strCancelEdit))
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FReply SAdvanceTextBox::StartEdit()
{
	ToggleEditMode(true);
	return FReply::Handled();
}

FReply SAdvanceTextBox::AcceptEdit()
{
	OnAcceptText.ExecuteIfBound(SEditableTextBox::GetText());
	ToggleEditMode(false);
	return FReply::Handled();
}

FReply SAdvanceTextBox::CancelEdit()
{
	ToggleEditMode(false);
	SEditableTextBox::SetText(SourceText);
	return FReply::Handled();
}

void SAdvanceTextBox::ToggleEditMode(const bool isEdit)
{
	if (isEdit)
	{
		SEditableTextBox::SetIsReadOnly(false);
		SourceText = SEditableTextBox::GetText();	
		FSlateApplication::Get().SetKeyboardFocus(this->AsShared(), EFocusCause::Mouse);
	}
	else
	{
		SEditableTextBox::SetIsReadOnly(true);
	}

	ButtonStartEdit->SetVisibility(isEdit ? EVisibility::Collapsed : EVisibility::Visible);
	ButtonAcceptEdit->SetVisibility(isEdit ? EVisibility::Visible : EVisibility::Collapsed);
	ButtonCancelEdit->SetVisibility(isEdit ? EVisibility::Visible : EVisibility::Collapsed);
}

void SAdvanceTextBox::SetAllowEdit(const bool IsAllowEdit)
{
	ToggleEditMode(false);
	ButtonStartEdit->SetVisibility(IsAllowEdit ? EVisibility::Visible : EVisibility::Collapsed);
}
