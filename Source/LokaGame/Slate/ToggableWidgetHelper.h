// VRSPRO

#pragma once

struct FToggableWidgetHelper
{
	static void ToggleWidget(TSharedRef<SWidget> SourceWidget, FCurveSequence& SourceCurveSequence, const bool Toggle);
};