// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "LokaGame.h"
#include "Slate/LokaStyle.h"
#include "Slate/LokaSlateStyle.h"
#include "SlateMaterialBrush.h"
#include "Styles/ContextMenuWidgetStyle.h"
#include "SlateGameResources.h"

TSharedPtr< FSlateStyleSet > FLokaStyle::StyleInstance = nullptr;

void FLokaStyle::Initialize()
{
	if (!StyleInstance.IsValid() || StyleInstance.Get() == nullptr)
	{
		StyleInstance = Create();
		FSlateStyleRegistry::RegisterSlateStyle(*StyleInstance);
	}
}

void FLokaStyle::Shutdown()
{
	FSlateStyleRegistry::UnRegisterSlateStyle(*StyleInstance);
	ensure(StyleInstance.IsUnique());
	StyleInstance.Reset();
}

FName FLokaStyle::GetStyleSetName()
{
	static FName StyleSetName(TEXT("LokaStyle"));
	return StyleSetName;
}

#define IMAGE_BRUSH( RelativePath, ... ) FSlateImageBrush( FPaths::ProjectContentDir() / "Slate"/ RelativePath + TEXT(".png"), __VA_ARGS__ )
#define BOX_BRUSH( RelativePath, ... ) FSlateBoxBrush( FPaths::ProjectContentDir() / "Slate"/ RelativePath + TEXT(".png"), __VA_ARGS__ )
#define BORDER_BRUSH( RelativePath, ... ) FSlateBorderBrush( FPaths::ProjectContentDir() / "Slate"/ RelativePath + TEXT(".png"), __VA_ARGS__ )
#define BORDER_BRUSH_ENGINE( RelativePath, ... ) FSlateBorderBrush(FPaths::EngineContentDir() / TEXT("Slate") / RelativePath + TEXT(".png"), __VA_ARGS__)
#define TTF_FONT( RelativePath, ... ) FSlateFontInfo( FPaths::ProjectContentDir() / "Slate"/ RelativePath + TEXT(".ttf"), __VA_ARGS__ )
#define OTF_FONT( RelativePath, ... ) FSlateFontInfo( FPaths::ProjectContentDir() / "Slate"/ RelativePath + TEXT(".otf"), __VA_ARGS__ )

TSharedRef<FSlateStyleSet> FLokaStyle::Create()
{
	//Get Styles From Assets
	TSharedRef<FSlateStyleSet> Style = FLokaSlateStyle::New(FLokaStyle::GetStyleSetName(), "/Game/1LOKAgame/UserInterface/Styles", "/Game/1LOKAgame/UserInterface/Styles");

	// SHyperlink defaults...
	{
		FButtonStyle HyperlinkButton = FButtonStyle()
			.SetNormal(BORDER_BRUSH_ENGINE("Old/HyperlinkDotted", FMargin(0, 0, 0, 3 / 16.0f)))
			.SetPressed(FSlateNoResource())
			.SetHovered(BORDER_BRUSH_ENGINE("Old/HyperlinkUnderline", FMargin(0, 0, 0, 3 / 16.0f)));

		FHyperlinkStyle Hyperlink = FHyperlinkStyle()
			.SetUnderlineStyle(HyperlinkButton)
			//.SetTextStyle(NormalText)
			.SetPadding(FMargin(0.0f));
		Style->Set("Hyperlink", Hyperlink);
	}

	FVector2D IsDonate_24x24(24, 24);
	FLinearColor IsDonate_MoneyColor(1.0f, 0.936f, 0.0f);
	FLinearColor IsDonate_DonateColor(0.207f, 1.0f, 0.18f);	

	Style->Set("Editor.IsDonate",
			   FCheckBoxStyle()
			   .SetCheckBoxType(ESlateCheckBoxType::CheckBox)
			   .SetUncheckedImage(IMAGE_BRUSH("Icons" / "base_icons_money", IsDonate_24x24, IsDonate_MoneyColor))
			   .SetUncheckedHoveredImage(IMAGE_BRUSH("Icons" / "base_icons_money", IsDonate_24x24, FMath::Lerp(IsDonate_MoneyColor, FLinearColor::White, .5f)))
			   .SetUncheckedPressedImage(IMAGE_BRUSH("Icons" / "base_icons_money", IsDonate_24x24, FLinearColor::White))

			   .SetCheckedImage(IMAGE_BRUSH("Icons" / "base_icons_donate", IsDonate_24x24, IsDonate_DonateColor))
			   .SetCheckedHoveredImage(IMAGE_BRUSH("Icons" / "base_icons_donate", IsDonate_24x24, FMath::Lerp(IsDonate_DonateColor, FLinearColor::White, .5f)))
			   .SetCheckedPressedImage(IMAGE_BRUSH("Icons" / "base_icons_donate", IsDonate_24x24, FLinearColor::White))
	);

	ReGenerateContextMenuStyle(Style);
	return Style;
}

void FLokaStyle::ReGenerateContextMenuStyle(TSharedRef<class FSlateStyleSet> InStyle)
{
	if (InStyle->HasWidgetStyle<FContextMenuStyle>("SContextMenuStyle"))
	{
		const FContextMenuStyle *ContextMenu = &InStyle->GetWidgetStyle<FContextMenuStyle>("SContextMenuStyle");

		// MenuBar
		{
			InStyle->Set("Menu.Background", new FSlateBrush(ContextMenu->Background));
			InStyle->Set("Menu.Icon", new FSlateBrush(ContextMenu->Icon));
			InStyle->Set("Menu.Expand", new FSlateBrush(ContextMenu->Expand));
			InStyle->Set("Menu.SubMenuIndicator", new FSlateBrush(ContextMenu->SubMenuIndicator));
			InStyle->Set("Menu.SToolBarComboButtonBlock.Padding", ContextMenu->SToolBarComboButtonBlockPadding);
			InStyle->Set("Menu.SToolBarButtonBlock.Padding", ContextMenu->SToolBarButtonBlockPadding);
			InStyle->Set("Menu.SToolBarCheckComboButtonBlock.Padding", ContextMenu->SToolBarCheckComboButtonBlockPadding);
			InStyle->Set("Menu.SToolBarButtonBlock.CheckBox.Padding", ContextMenu->SToolBarButtonBlockCheckBoxPadding);
			InStyle->Set("Menu.SToolBarComboButtonBlock.ComboButton.Color", ContextMenu->SToolBarComboButtonBlockComboButtonColor);

			InStyle->Set("Menu.Block.IndentedPadding", ContextMenu->BlockIndentedPadding);
			InStyle->Set("Menu.Block.Padding", ContextMenu->BlockPadding);

			InStyle->Set("Menu.Separator", new FSlateBrush(ContextMenu->Separator));
			InStyle->Set("Menu.Separator.Padding", ContextMenu->SeparatorPadding);

			InStyle->Set("Menu.Label", ContextMenu->Label);
			InStyle->Set("Menu.EditableText", ContextMenu->EditableText);
			InStyle->Set("Menu.Keybinding", ContextMenu->Keybinding);

			InStyle->Set("Menu.Heading", ContextMenu->Heading);

			InStyle->Set("Menu.CheckBox", ContextMenu->CheckBox);

			InStyle->Set("Menu.Check", ContextMenu->Check);

			InStyle->Set("Menu.RadioButton", ContextMenu->RadioButton);

			InStyle->Set("Menu.ToggleButton", ContextMenu->ToggleButton);

			InStyle->Set("Menu.Button", ContextMenu->Button);

			InStyle->Set("Menu.Button.Checked", new FSlateBrush(ContextMenu->ButtonChecked));
			InStyle->Set("Menu.Button.Checked_Hovered", new FSlateBrush(ContextMenu->ButtonChecked_Hovered));
			InStyle->Set("Menu.Button.Checked_Pressed", new FSlateBrush(ContextMenu->ButtonChecked_Pressed));

			InStyle->Set("Menu.Button.SubMenuOpen", new FSlateBrush(ContextMenu->ButtonSubMenuOpen));

			InStyle->Set("MultiBox.DeleteButton", FButtonStyle()
					   .SetNormal(FSlateNoResource())
					   .SetPressed(FSlateNoResource())
					   .SetHovered(FSlateNoResource())
					   );
		}
	}
}

#undef IMAGE_BRUSH
#undef BOX_BRUSH
#undef BORDER_BRUSH
#undef TTF_FONT
#undef OTF_FONT
#undef BORDER_BRUSH_ENGINE

void FLokaStyle::ReloadTextures()
{
	FSlateApplication::Get().GetRenderer()->ReloadTextureResources();
}

const ISlateStyle& FLokaStyle::Get()
{
	Initialize();
	return *StyleInstance;
}

//TSharedRef<FLokaSlateStyle> FLokaStyle::GetAlt()
//{
//	Initialize();
//	return StaticCastSharedRef<FLokaSlateStyle>(StyleInstance.ToSharedRef());
//}
