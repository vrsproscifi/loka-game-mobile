// VRSPRO

#pragma once

#include "ITextDecorator.h"

class FLokaTextDecorator : public ITextDecorator
{
public:

	static TSharedRef<FLokaTextDecorator> Create(const TMap<FName, FSlateFontInfo>& InFonts, const FSlateFontInfo& InDefaultFont, const FLinearColor& InDefaultColor);
	static TSharedRef<FLokaTextDecorator> Create(const FTextBlockStyle& InTextBlockStyle, const TMap<FName, FSlateFontInfo>& InFonts = TMap<FName, FSlateFontInfo>());

	virtual ~FLokaTextDecorator()
	{
	}

	virtual bool Supports(const FTextRunParseResults& RunParseResult, const FString& Text) const override;
	virtual TSharedRef<ISlateRun> Create(const TSharedRef<FTextLayout>& TextLayout, const FTextRunParseResults& RunParseResult, const FString& OriginalText, const TSharedRef<FString>& InOutModelText, const ISlateStyle* Style) override;

private:

	FLokaTextDecorator(const TMap<FName, FSlateFontInfo>& InFonts, const FSlateFontInfo& InDefaultFont, const FLinearColor& InDefaultColor);

	TMap<FName, FSlateFontInfo> Fonts;
	FSlateFontInfo DefaultFont;
	FLinearColor DefaultColor;

protected:

	FTextBlockStyle CreateTextBlockStyle(const FRunInfo& InRunInfo) const;
};