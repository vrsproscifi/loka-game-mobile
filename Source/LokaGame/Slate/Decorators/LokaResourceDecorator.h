// VRSPRO

#pragma once

#include "ITextDecorator.h"
#include "SlateWidgetRun.h"

class FLokaResourceDecorator : public ITextDecorator
{
public:

	static TSharedRef<FLokaResourceDecorator> Create();

	virtual ~FLokaResourceDecorator()
	{
	}

	virtual bool Supports(const FTextRunParseResults& RunParseResult, const FString& Text) const override;
	virtual TSharedRef<ISlateRun> Create(const TSharedRef<FTextLayout>& TextLayout, const FTextRunParseResults& RunParseResult, const FString& OriginalText, const TSharedRef<FString>& InOutModelText, const ISlateStyle* Style) override;

private:

	FLokaResourceDecorator();
};