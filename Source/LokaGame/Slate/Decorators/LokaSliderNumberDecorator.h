// VRSPRO

#pragma once

#include "ITextDecorator.h"
#include "SlateWidgetRun.h"

class FLokaSliderNumberDecorator : public ITextDecorator
{
public:

	static TSharedRef<FLokaSliderNumberDecorator> Create(FOnInt32ValueChanged&);

	virtual ~FLokaSliderNumberDecorator()
	{
	}

	virtual bool Supports(const FTextRunParseResults& RunParseResult, const FString& Text) const override;
	virtual TSharedRef<ISlateRun> Create(const TSharedRef<FTextLayout>& TextLayout, const FTextRunParseResults& RunParseResult, const FString& OriginalText, const TSharedRef<FString>& InOutModelText, const ISlateStyle* Style) override;

private:

	FLokaSliderNumberDecorator();

protected:

	FOnInt32ValueChanged OnValueChange;
};