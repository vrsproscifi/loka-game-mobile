
#include "LokaGame.h"
#include "LokaResourceDecorator.h"
#include "SResourceTextBox.h"
#include "SResourceTextBoxType.h"

FLokaResourceDecorator::FLokaResourceDecorator()
{
}

TSharedRef<FLokaResourceDecorator> FLokaResourceDecorator::Create()
{
	return MakeShareable(new FLokaResourceDecorator());
}

bool FLokaResourceDecorator::Supports(const FTextRunParseResults& RunParseResult, const FString& Text) const
{
	return (RunParseResult.Name == TEXT("resource"));
}

TSharedRef<ISlateRun> FLokaResourceDecorator::Create(const TSharedRef<FTextLayout>& TextLayout, const FTextRunParseResults& RunParseResult, const FString& OriginalText, const TSharedRef<FString>& InOutModelText, const ISlateStyle* Style)
{
	FTextRange ModelRange;
	ModelRange.BeginIndex = InOutModelText->Len();
	*InOutModelText += TEXT('\x200B'); // Zero-Width Breaking Space
	ModelRange.EndIndex = InOutModelText->Len();

	FTextRunInfo RunInfo(RunParseResult.Name, FText::FromString(OriginalText.Mid(RunParseResult.ContentRange.BeginIndex, RunParseResult.ContentRange.EndIndex - RunParseResult.ContentRange.BeginIndex)));
	for (const TPair<FString, FTextRange>& Pair : RunParseResult.MetaData)
	{
		RunInfo.MetaData.Add(Pair.Key, OriginalText.Mid(Pair.Value.BeginIndex, Pair.Value.EndIndex - Pair.Value.BeginIndex));
	}

	//==============================================================================================================================]

	EResourceTextBoxType::Type ResourceTextBoxType = EResourceTextBoxType::Money;
	auto TargetBoxType = RunInfo.MetaData.Find("type");
	if (TargetBoxType)
	{
		if (TargetBoxType->Equals("money", ESearchCase::IgnoreCase))
		{
			ResourceTextBoxType = EResourceTextBoxType::Money;
		}
		else if (TargetBoxType->Equals("donate", ESearchCase::IgnoreCase))
		{
			ResourceTextBoxType = EResourceTextBoxType::Donate;
		}
		else if (TargetBoxType->Equals("coin", ESearchCase::IgnoreCase))
		{
			ResourceTextBoxType = EResourceTextBoxType::Coin;
		}
		else if (TargetBoxType->Equals("level", ESearchCase::IgnoreCase))
		{
			ResourceTextBoxType = EResourceTextBoxType::Level;
		}
		else if (TargetBoxType->Equals("time", ESearchCase::IgnoreCase))
		{
			ResourceTextBoxType = EResourceTextBoxType::Time;
		}
	}

	int32 ResourceTextBoxValue = 0;
	if (RunInfo.Content.IsNumeric())
	{
		ResourceTextBoxValue = FCString::Atoi(*RunInfo.Content.ToString());
	}

	FVector ResourceTextBoxSize(16, 16, 12);
	auto TargetBoxSize = RunInfo.MetaData.Find("size");
	if (TargetBoxSize && !ResourceTextBoxSize.InitFromString(*TargetBoxSize))
	{
		FString strX, strY, strZ;

		TargetBoxSize->Split(",", &strX, &strY);
		TargetBoxSize->Split(",", &strY, &strZ);

		if (!strX.IsEmpty() && !strY.IsEmpty() && !strZ.IsEmpty())
		{
			ResourceTextBoxSize = FVector(FCString::Atof(*strX), FCString::Atof(*strY), FCString::Atof(*strZ));
		}
	}

	TSharedRef<SWidget> ResultWidget = SNew(SResourceTextBox)
		.TypeAndValue(FResourceTextBoxValue(ResourceTextBoxType, ResourceTextBoxValue))
		.CustomSize(ResourceTextBoxSize);

	//TSharedRef< FSlateFontMeasure > FontMeasure = FSlateApplication::Get().GetRenderer()->GetFontMeasureService();
	//int16 Baseline = FontMeasure->GetBaseline(Style->Content.Text.Font);

	return FSlateWidgetRun::Create(TextLayout, RunInfo, InOutModelText, FSlateWidgetRun::FWidgetRunInfo(ResultWidget, 0), ModelRange);
}