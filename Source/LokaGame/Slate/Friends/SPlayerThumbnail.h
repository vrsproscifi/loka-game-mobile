// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "Styles/PlayerThumbnailWidgetStyle.h"
#include "Shared/PlayerOnlineStatus.h"

/**
 * 
 */
class LOKAGAME_API SPlayerThumbnail : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SPlayerThumbnail)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FPlayerThumbnailStyle>("SPlayerThumbnailStyle"))
		, _IsLocal(false)
		, _IsSquad(false)
	{}
	SLATE_STYLE_ARGUMENT(FPlayerThumbnailStyle, Style)
	SLATE_ATTRIBUTE(EPlayerFriendStatus, Status)
	SLATE_ATTRIBUTE(FText, Name)
	SLATE_ATTRIBUTE(bool, IsLocal)
	SLATE_ATTRIBUTE(bool, IsSquad)
	SLATE_ATTRIBUTE(FGuid, PlayerId)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	FSlateFontInfo GetNameFont() const;
	FSlateFontInfo GetStatusFont() const;
	FSlateColor GetNameColorAndOpacity() const;
	FSlateColor GetStatusColorAndOpacity() const;

	FVector2D GetNameShadowOffset() const;
	FVector2D GetStatusShadowOffset() const;

	FLinearColor GetNameShadowColor() const;
	FLinearColor GetStatusShadowColor() const;

	const FSlateBrush* GetStatusImage() const;
	FText GetNameText() const;
	FText GetStatusText() const;

protected:

	const FPlayerThumbnailStyle*		Style;

	TAttribute<EPlayerFriendStatus>		Status;
	TAttribute<FText>					Name;
	TAttribute<bool>					IsLocal;
	TAttribute<bool>					IsSquad;
	TAttribute<FGuid>					PlayerId;
};
