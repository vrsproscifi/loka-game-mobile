// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/FriendsFormWidgetStyle.h"
#include "FriendComponent/PlayerBasicInformation.h"

namespace EFriendsTreeItemFlags
{
	enum Type
	{
		None,
		Friend = 1,
		HeaderSmall = 2,
		HeaderMedium = 4,
		HeaderBig = 8,
		Expanded = 16,
		AllFlags = Friend | HeaderSmall | HeaderMedium | HeaderBig | Expanded
	};
}

struct FFriendsTreeItem
{
	FName Index;
	FText Name;
	TSharedPtr<FPlayerBasicInformation> Source;
	int32 Flags;
	TArray<TSharedPtr<FFriendsTreeItem>> List;

	FFriendsTreeItem() : Name(), Flags(0), List(), Source() {}
	FFriendsTreeItem(const FText& n) : Name(n), Flags(0), List(), Source() {}
	FFriendsTreeItem(const FText& n, const TArray<TSharedPtr<FFriendsTreeItem>>& l) : Name(n), Flags(0), List(l), Source() {}
	FFriendsTreeItem(const FText& n, const TArray<TSharedPtr<FFriendsTreeItem>>& l, const int32& f) : Name(n), Flags(f), List(l), Source() {}
};


class LOKAGAME_API SFriendsForm_Item : public STableRow<TSharedPtr<FFriendsTreeItem>>
{
	typedef STableRow<TSharedPtr<FFriendsTreeItem>> SSuper;

public:
	SLATE_BEGIN_ARGS(SFriendsForm_Item)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FFriendsFormStyle>("SFriendsFormStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FFriendsFormStyle, Style)
	SLATE_EVENT(FOnClickedOutside, OnClickedFriend)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, const TSharedRef<STableViewBase>& OwnerTable, TSharedPtr<FFriendsTreeItem> InInstance);

	static const FSlateBrush* GetStatusImage(const FFriendsFormStyle* InStyle, const EPlayerFriendStatus InFlag);
	static FText GetStatusToolTip(const EPlayerFriendStatus InFlag);

protected:

	const FFriendsFormStyle* Style;
	FOnClickedOutside OnClickedFriend;
	TSharedPtr<FFriendsTreeItem> Instance;
};
