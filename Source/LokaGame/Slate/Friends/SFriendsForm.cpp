﻿//// VRSPRO
//
#include "LokaGame.h"
#include "SFriendsForm.h"
#include "SlateOptMacros.h"

#include "STextBlockButton.h"
#include "SLokaSearchBox.h"

#include "IdentityController/Models/PlayerEntityInfo.h"
#include "Styles/LobbyChatWidgetStyle.h"

#include "SFriends_Commands.h"
#include "SquadComponent/SqaudInformation.h"
#include "BasePlayerState.h"
#include "Components/FriendComponent.h"
#include "Components/SquadComponent.h"
#include "Containers/STabbedContainer.h"
#include "ConstructionPlayerState.h"
#include "IdentityComponent.h"
#include "SScaleBox.h"
#include "Utilities/SlateUtilityTypes.h"
#include "SPlayerThumbnail.h"
#include "GameSingleton.h"

#define TO_FFriendsTreeItemArray(In, Out)\
	for (auto &f : In)\
	{\
		Out.Add(MakeShareable(new FFriendsTreeItem(FText::FromString(f.Name))));\
		Out.Last()->Source = MakeShareable(new FPlayerBasicInformation(f));\
	}

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SFriendsForm::Construct(const FArguments& InArgs, const TSharedRef<SWindowsContainer>& InOwner)
{
	Style = InArgs._Style;
	PlayerState = InArgs._PlayerState;

	auto StyleTabs = &FLokaStyle::Get().GetWidgetStyle<FLobbyChatStyle>("SLobbyChatStyle");

	TSharedRef<SScrollBar> ScrollBar_Friends = SNew(SScrollBar).Style(&FLokaStyle::Get().GetWidgetStyle<FScrollBarStyle>("Default_ScrollBar")).Thickness(FVector2D(4, 4))
		, ScrollBar_Squad = SNew(SScrollBar).Style(&FLokaStyle::Get().GetWidgetStyle<FScrollBarStyle>("Default_ScrollBar")).Thickness(FVector2D(4, 4))
		, ScrollBar_Top = SNew(SScrollBar).Style(&FLokaStyle::Get().GetWidgetStyle<FScrollBarStyle>("Default_ScrollBar")).Thickness(FVector2D(4, 4));

	SAssignNew(ContainerListFriend, STreeView<TSharedPtr<FFriendsTreeItem>>)
		.ExternalScrollbar(ScrollBar_Friends)
		.SelectionMode(ESelectionMode::None)
		.TreeItemsSource(&FriendsList)
		.OnGetChildren_Lambda([&](TSharedPtr<FFriendsTreeItem> Item, TArray<TSharedPtr<FFriendsTreeItem>>& OutChildren) {
			OutChildren = Item->List;
		})
		.OnGenerateRow_Lambda([&](TSharedPtr<FFriendsTreeItem> Item, const TSharedRef<STableViewBase>& OwnerTable) {
			return SNew(SFriendsForm_Item, OwnerTable, Item).OnClickedFriend_Lambda([&, i = Item.ToSharedRef()]() {
			if(i->Source.IsValid())
				OpenContextMenu(i->Source.ToSharedRef());
		});
	});
	
	MAKE_UTF8_SYMBOL(sContacts, 0xf0c0);

	TSharedRef<SWidget> TabbedContainer = SNew(STabbedContainer)
	+ STabbedContainer::Slot().Name(NSLOCTEXT("SFriendsForm", "Friends.Header.Top", "Top")).IsAutoSize(true)
	[
		SNew(SHorizontalBox)
		+ SHorizontalBox::Slot().FillWidth(1)
		[
			SAssignNew(TopContainer, SListView<TSharedPtr<FPlayerRatingTopContainer>>)
			.ListItemsSource(&TopPlayersArray)
			.SelectionMode(ESelectionMode::None)
			.ExternalScrollbar(ScrollBar_Top)
			.OnGenerateRow_Lambda([&, s = Style](TSharedPtr<FPlayerRatingTopContainer> Item, const TSharedRef<STableViewBase>& OwnerTable) {

				auto pWidget = SNew(SPlayerThumbnail)
					.Name(FText::FromString(Item->Name))
					.IsLocal(Item->Id == UGameSingleton::Get()->GetSavedPlayerEntityInfo().PlayerId)
					.PlayerId(Item->Id)
					.Status(Item->IsOnline ? EPlayerFriendStatus::Online : EPlayerFriendStatus::Offline);

				return SNew(STableRow<TSharedPtr<FPlayerRatingTopContainer>>, OwnerTable).Padding(FMargin(2, 4))
				[
					SNew(SButton)
					.ButtonStyle(&s->List.Button)
					.ContentPadding(0)
					.OnClicked_Lambda([&, i = Item.ToSharedRef()]() {
						TSharedPtr<FPlayerBasicInformation> TmpPlayer = MakeShareable(new FPlayerBasicInformation());
						TmpPlayer->Name = i->Name;
						TmpPlayer->PlayerId = i->Id;
						TmpPlayer->Status = i->IsOnline ? EPlayerFriendStatus::Online : EPlayerFriendStatus::Offline;
						TmpPlayer->State = ActionPlayerRequestState::End;
						OpenContextMenu(TmpPlayer.ToSharedRef());
						return FReply::Handled();
					})
					[
						SNew(SHorizontalBox)
						+ SHorizontalBox::Slot().AutoWidth()
						[
							pWidget
						]
						+ SHorizontalBox::Slot().Padding(4, 2).HAlign(HAlign_Right)
						[
							SNew(STextBlock)
							.Text(FText::AsNumber(Item->Score))
							.Font_Lambda([w = pWidget]() { return w->GetNameFont(); })							
							.ColorAndOpacity_Lambda([w = pWidget]()
							{
								return w->GetNameColorAndOpacity();
							})
						]
					]
				];
			})
		]
		+ SHorizontalBox::Slot().AutoWidth()
		[
			ScrollBar_Top
		]
	]
	+ STabbedContainer::Slot().Name(NSLOCTEXT("SFriendsForm", "Friends.Header.Contacts", "Contacts"))
	[
		SNew(SVerticalBox)
		+ SVerticalBox::Slot() //Search or add
		.AutoHeight()
		[
			SNew(SLokaSearchBox)
			.OnSearch(this, &SFriendsForm::OnSearch)
		]
		+ SVerticalBox::Slot() //List
		.FillHeight(1)
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot().FillWidth(1)
			[
				ContainerListFriend.ToSharedRef()
			]
			+ SHorizontalBox::Slot().AutoWidth()
			[
				ScrollBar_Friends
			]
		]
	]
	+ STabbedContainer::Slot().Name(NSLOCTEXT("SFriendsForm", "Friends.Header.Squad", "Squad")).IsAutoSize(true)
	[
		SNew(SHorizontalBox)
		+ SHorizontalBox::Slot().FillWidth(1)
		[
			SAssignNew(SquadContainer, SListView<TSharedPtr<FQueueMemberContainer>>)
			.ExternalScrollbar(ScrollBar_Squad)
			.ListItemsSource(&SquadMembers)
			.SelectionMode(ESelectionMode::None)
			.OnGenerateRow_Lambda([&](TSharedPtr<FQueueMemberContainer> Source, const TSharedRef<STableViewBase>& OwnerTableView) {
				return SNew(STableRow<TSharedPtr<FQueueMemberContainer>>, OwnerTableView)
				[
					SNew(SFriendsForm_Squad, Source.ToSharedRef()).OnRemoveMember_Lambda([&, i = Source.ToSharedRef()]() {
						if (PlayerState.IsValid() && PlayerState->GetSquadComponent())
						{
							PlayerState->GetSquadComponent()->SendRequestRemove(i->PlayerId);
						}
					})
				];
			})
		]
		+ SHorizontalBox::Slot().AutoWidth()
		[
			ScrollBar_Squad
		]
	];

	auto SuperArgs = SWindowBase::FArguments()
	.SizeLimits(FBox2D(FVector2D(320, 380), FVector2D(320, 680)))
	.StartPosition(FVector2D(20.0f, 460))
	.TitleText(NSLOCTEXT("Contacts", "Contacts.Title", "Contacts"))
	.QuickButton(FText::FromString(sContacts))
	.QuickButtonOrder(1)
	.Content()
	[
		TabbedContainer
	];

	SWindowBase::Construct(SuperArgs, InOwner);

	if (PlayerState.IsValid() && PlayerState->GetFriendComponent() && PlayerState->GetSquadComponent())
	{
		PlayerState->GetFriendComponent()->OnListUpdate.AddSP(this, &SFriendsForm::OnFillingList, true);
		PlayerState->GetFriendComponent()->OnSearchListUpdate.AddSP(this, &SFriendsForm::OnFillingList, false);

		PlayerState->GetSquadComponent()->OnSquadUpdate.AddSP(this, &SFriendsForm::OnSquapInformationUpdate);

		PlayerState->GetIdentityComponent()->OnTopPlayersEvent.AddLambda([&](const FPlayerRatingTopView& InData)
		{
			TopPlayersArray.Empty();

			for (auto p : InData.PlayerRating)
			{
				TopPlayersArray.Add(MakeShareable(new FPlayerRatingTopContainer(p)));
			}

			TopContainer->RequestListRefresh();
		});

		// Небольшой костыль для костыля
		RegisterActiveTimer(5, FWidgetActiveTimerDelegate::CreateLambda([&](double InCurrentTime, float InDeltaTime)
		{
			if (PlayerState.IsValid() && PlayerState->GetSquadComponent())
			{
				OnSquapInformationUpdate(PlayerState->GetSquadComponent()->GetSqaudInformation());
			}

			return EActiveTimerReturnType::Continue;
		}));
	}

	OnContextMenuClosed();

	bIsFirstUpdate = true;
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

SFriendsForm::~SFriendsForm()
{
	SFriendsCommands::GetAlt().OnAnyAction.RemoveAll(this);
}

void SFriendsForm::OnContextMenuClosed()
{
	bIsMenuOpen = false;
}

void SFriendsForm::OnContextMenuClosed(TSharedRef<IMenu> InMenu)
{
	OnContextMenuClosed();
}

void SFriendsForm::OnModeChange(ECheckBoxState State, const bool IsSquad)
{
	CurrentMode = IsSquad;
}

ECheckBoxState SFriendsForm::GetCurrentMode(const bool IsSquad) const
{
	return (CurrentMode == IsSquad) ? ECheckBoxState::Checked : ECheckBoxState::Unchecked;
}

void SFriendsForm::OnSearch(const FText& Name, ETextCommit::Type Commit)
{
	bIsMenuOpen = false;

	if (Commit == ETextCommit::OnEnter)
	{
		if (PlayerState.IsValid() && PlayerState->GetFriendComponent())
		{
			PlayerState->GetFriendComponent()->SendRequestSearch(Name.ToString());
		}

		bIsSearchState = true;
	}
	else if (Commit == ETextCommit::OnCleared)
	{
		if (PlayerState.IsValid() && PlayerState->GetFriendComponent())
		{
			PlayerState->GetFriendComponent()->SendRequestSearch(FString());
		}

		bIsSearchState = false;
	}
}

void SFriendsForm::OpenContextMenu(TSharedRef<FPlayerBasicInformation> Target)
{
	bIsMenuOpen = true;

	SFriendsCommands::GetAlt().SetCurrentTargetId(Target->PlayerId);
	FMenuBuilder MenuBuilder(true, SFriendsCommands::Get().GetCommandList());
	{
		MenuBuilder.BeginSection("FriendMenu.Header", FText::FromString(Target->Name));
		{
			if (Target->State != ActionPlayerRequestState::Blocked)
			{
				//MenuBuilder.AddMenuEntry(SFriendsCommands::Get().PrivateMessage);
				MenuBuilder.AddMenuEntry(SFriendsCommands::Get().GetCommand(SFriendsCommands::ConstructionInvite));
				MenuBuilder.AddMenuEntry(SFriendsCommands::Get().GetCommand(SFriendsCommands::ConstructionRequest));

				MenuBuilder.AddMenuEntry(SFriendsCommands::Get().GetCommand(SFriendsCommands::SquadInvite));
				MenuBuilder.AddMenuEntry(SFriendsCommands::Get().GetCommand(SFriendsCommands::SquadRequest));

				MenuBuilder.AddMenuEntry(SFriendsCommands::Get().GetCommand(SFriendsCommands::DuelInvite));
				MenuBuilder.AddMenuEntry(SFriendsCommands::Get().GetCommand(SFriendsCommands::GuestView));
				//MenuBuilder.AddMenuEntry(SFriendsCommands::Get().GetCommand(SFriendsCommands::Attack));
			}
			else
			{
				MenuBuilder.AddMenuSeparator();
			}
		}
		MenuBuilder.EndSection();

		if (Target->State != ActionPlayerRequestState::End)
		{
			if (Target->State == ActionPlayerRequestState::Waiting || Target->State == ActionPlayerRequestState::ConfirmedWaiting)
			{
				MenuBuilder.BeginSection("FriendMenu.Middle");
				{
					if (Target->State == ActionPlayerRequestState::ConfirmedWaiting)
					{
						MenuBuilder.AddMenuEntry(SFriendsCommands::Get().GetCommand(SFriendsCommands::AcceptInvite));
					}
					MenuBuilder.AddMenuEntry(SFriendsCommands::Get().GetCommand(SFriendsCommands::DeclineInvite));
				}
				MenuBuilder.EndSection();
			}
			else
			{
				if (Target->State != ActionPlayerRequestState::Blocked)
				{
					MenuBuilder.BeginSection("FriendMenu.Middle");
					{
						MenuBuilder.AddMenuEntry(SFriendsCommands::Get().GetCommand(SFriendsCommands::AddBlockList));
					}
					MenuBuilder.EndSection();
				}

				MenuBuilder.BeginSection("FriendMenu.Footer");
				{
					MenuBuilder.AddMenuEntry(SFriendsCommands::Get().GetCommand(SFriendsCommands::DeleteFriend));
				}
				MenuBuilder.EndSection();
			}
		}
		else
		{
			MenuBuilder.BeginSection("FriendMenu.Middle");
			{
				MenuBuilder.AddMenuEntry(SFriendsCommands::Get().GetCommand(SFriendsCommands::AddFriend));
			}
			MenuBuilder.EndSection();
		}
	}

	SFriendsCommands::GetAlt().OnAnyAction.AddSP(this, &SFriendsForm::OnContextMenuClosed);
	FLokaStyle::ReGenerateContextMenuStyle();
	MenuBuilder.SetStyle(&FLokaStyle::Get(), "Menu");

	FSlateApplication::Get().PushMenu(AsShared(), FWidgetPath(), MenuBuilder.MakeWidget(), FSlateApplication::Get().GetCursorPos(), FPopupTransitionEffect(FPopupTransitionEffect::ContextMenu))->GetOnMenuDismissed().AddSP(this, &SFriendsForm::OnContextMenuClosed);
}

//=====================================================================================================================================================] Public Functions

void SFriendsForm::OnFillingList(const TArray<FPlayerBasicInformation>& friends, const bool IsFriend)
{
	if ((bIsSearchState && IsFriend == false) || (bIsSearchState == false && IsFriend == true) && !bIsMenuOpen)
	{
		if (LastData != friends)
		{
			LastData = friends;
			AppendRows(friends);
		}
	}
}


void SFriendsForm::AppendRows(const TArray<FPlayerBasicInformation>& Rows)
{
	TArray<FName> ExpandedMap;

	for (auto &i : FriendsList)
	{
		if (FFlagsHelper::HasAnyFlags(i->Flags, EFriendsTreeItemFlags::Expanded))
		{
			ExpandedMap.Add(i->Index);
		}

		if (i->List.Num())
		{
			for (auto &s : i->List)
			{
				if (FFlagsHelper::HasAnyFlags(s->Flags, EFriendsTreeItemFlags::Expanded))
				{
					ExpandedMap.Add(s->Index);
				}
			}
		}
	}


	TArray<TSharedPtr<FFriendsTreeItem>> ListOnline, ListOffline, ListQuery, ListQueryWaiting, ListQueryConfirmedWaiting, ListBlocked, ListFriends, ListOther, ListMostOnline;

	auto Friends_Confirmed = Rows.FilterByPredicate([](const FPlayerBasicInformation& A) {return A.State == ActionPlayerRequestState::Confirmed && !FFlagsHelper::HasAnyFlags(A.Status, PlayerFriendStatus::Global); });
	auto Friends_Waiting = Rows.FilterByPredicate([](const FPlayerBasicInformation& A) {return A.State == ActionPlayerRequestState::Waiting && !FFlagsHelper::HasAnyFlags(A.Status, PlayerFriendStatus::Global); });
	auto Friends_ConfirmedWaiting = Rows.FilterByPredicate([](const FPlayerBasicInformation& A) {return A.State == ActionPlayerRequestState::ConfirmedWaiting && !FFlagsHelper::HasAnyFlags(A.Status, PlayerFriendStatus::Global); });
	auto Friends_Blocked = Rows.FilterByPredicate([](const FPlayerBasicInformation& A) {return A.State == ActionPlayerRequestState::Blocked && !FFlagsHelper::HasAnyFlags(A.Status, PlayerFriendStatus::Global); });
	auto Friends_Search = Rows.FilterByPredicate([](const FPlayerBasicInformation& A) {return A.State == ActionPlayerRequestState::End && !FFlagsHelper::HasAnyFlags(A.Status, PlayerFriendStatus::Global); });
	auto Friends_MostOnline = Rows.FilterByPredicate([](const FPlayerBasicInformation& A) { return FFlagsHelper::HasAnyFlags(A.Status, PlayerFriendStatus::Global); });

	auto OnlineFriends = Friends_Confirmed.FilterByPredicate([](const FPlayerBasicInformation& A) {return A.Status >= PlayerFriendStatus::Online; });
	auto OfflineFriends = Friends_Confirmed.FilterByPredicate([](const FPlayerBasicInformation& A) {return A.Status == PlayerFriendStatus::Offline; });

	auto Lambda_NameGreater = [](const FPlayerBasicInformation& A, const FPlayerBasicInformation& B) {return A.Name <= B.Name; };

	OnlineFriends.Sort(Lambda_NameGreater);
	OfflineFriends.Sort(Lambda_NameGreater);
	Friends_Waiting.Sort(Lambda_NameGreater);
	Friends_ConfirmedWaiting.Sort(Lambda_NameGreater);
	Friends_Blocked.Sort(Lambda_NameGreater);
	Friends_Search.Sort(Lambda_NameGreater);
	Friends_MostOnline.Sort(Lambda_NameGreater);

	TO_FFriendsTreeItemArray(OnlineFriends, ListOnline);
	TO_FFriendsTreeItemArray(OfflineFriends, ListOffline);
	TO_FFriendsTreeItemArray(Friends_Blocked, ListBlocked);
	TO_FFriendsTreeItemArray(Friends_Waiting, ListQueryWaiting);
	TO_FFriendsTreeItemArray(Friends_ConfirmedWaiting, ListQueryConfirmedWaiting);
	TO_FFriendsTreeItemArray(Friends_Search, ListOther);
	TO_FFriendsTreeItemArray(Friends_MostOnline, ListMostOnline);

	FriendsList.Empty();

	if (ListOnline.Num())
	{
		EFriendsTreeItemFlags::Type Flags = ExpandedMap.Contains("FriendsOnline") ? EFriendsTreeItemFlags::Expanded : EFriendsTreeItemFlags::None;
		ListFriends.Add(MakeShareable(new FFriendsTreeItem(NSLOCTEXT("SFriendsForm", "Tree.Friends.Online", "Online"), ListOnline, EFriendsTreeItemFlags::HeaderMedium | Flags)));
		ListFriends.Last()->Index = "FriendsOnline";
	}
	if (ListOffline.Num()) 
	{
		EFriendsTreeItemFlags::Type Flags = ExpandedMap.Contains("FriendsOffline") ? EFriendsTreeItemFlags::Expanded : EFriendsTreeItemFlags::None;
		ListFriends.Add(MakeShareable(new FFriendsTreeItem(NSLOCTEXT("SFriendsForm", "Tree.Friends.Offline", "Offline"), ListOffline, EFriendsTreeItemFlags::HeaderMedium | Flags)));
		ListFriends.Last()->Index = "FriendsOffline";
	}

	if (ListQueryConfirmedWaiting.Num())
	{
		EFriendsTreeItemFlags::Type Flags = ExpandedMap.Contains("QuerysIn") ? EFriendsTreeItemFlags::Expanded : EFriendsTreeItemFlags::None;
		ListQuery.Add(MakeShareable(new FFriendsTreeItem(NSLOCTEXT("SFriendsForm", "Tree.Querys.Inbox", "Inbox"), ListQueryConfirmedWaiting, EFriendsTreeItemFlags::HeaderMedium | Flags)));
		ListQuery.Last()->Index = "QuerysIn";
	}
	if (ListQueryWaiting.Num())
	{
		EFriendsTreeItemFlags::Type Flags = ExpandedMap.Contains("QuerysOut") ? EFriendsTreeItemFlags::Expanded : EFriendsTreeItemFlags::None;
		ListQuery.Add(MakeShareable(new FFriendsTreeItem(NSLOCTEXT("SFriendsForm", "Tree.Querys.Outbox", "Outbox"), ListQueryWaiting, EFriendsTreeItemFlags::HeaderMedium | Flags)));
		ListQuery.Last()->Index = "QuerysOut";
	}

	if (ListFriends.Num())
	{
		EFriendsTreeItemFlags::Type Flags = ExpandedMap.Contains("Friends") ? EFriendsTreeItemFlags::Expanded : EFriendsTreeItemFlags::None;
		FriendsList.Add(MakeShareable(new FFriendsTreeItem(NSLOCTEXT("SFriendsForm", "Tree.Friends", "Friends"), ListFriends, EFriendsTreeItemFlags::HeaderBig | Flags)));
		FriendsList.Last()->Index = "Friends";
	}
	if (ListQuery.Num())
	{
		EFriendsTreeItemFlags::Type Flags = ExpandedMap.Contains("Querys") ? EFriendsTreeItemFlags::Expanded : EFriendsTreeItemFlags::None;
		FriendsList.Add(MakeShareable(new FFriendsTreeItem(NSLOCTEXT("SFriendsForm", "Tree.Querys", "Querys"), ListQuery, EFriendsTreeItemFlags::HeaderBig | Flags)));
		FriendsList.Last()->Index = "Querys";
	}
	if (ListBlocked.Num())
	{
		EFriendsTreeItemFlags::Type Flags = ExpandedMap.Contains("BlackList") ? EFriendsTreeItemFlags::Expanded : EFriendsTreeItemFlags::None;
		FriendsList.Add(MakeShareable(new FFriendsTreeItem(NSLOCTEXT("SFriendsForm", "Tree.BlackList", "Black List"), ListBlocked, EFriendsTreeItemFlags::HeaderBig | Flags)));
		FriendsList.Last()->Index = "BlackList";
	}
	if (ListOther.Num())
	{
		FriendsList.Add(MakeShareable(new FFriendsTreeItem(NSLOCTEXT("SFriendsForm", "Tree.ListOther", "Other Players"), ListOther, EFriendsTreeItemFlags::HeaderBig | EFriendsTreeItemFlags::Expanded)));
	}

	if (ListMostOnline.Num())
	{
		EFriendsTreeItemFlags::Type Flags = (ExpandedMap.Contains("ListMostOnline") || bIsFirstUpdate) ? EFriendsTreeItemFlags::Expanded : EFriendsTreeItemFlags::None;
		FriendsList.Add(MakeShareable(new FFriendsTreeItem(NSLOCTEXT("SFriendsForm", "Tree.ListMostOnline", "Players Online"), ListMostOnline, EFriendsTreeItemFlags::HeaderBig | Flags)));
		FriendsList.Last()->Index = "ListMostOnline";
	}

	ContainerListFriend->RequestTreeRefresh();

	for (auto &i : FriendsList)
	{		
		if (FFlagsHelper::HasAnyFlags(i->Flags, EFriendsTreeItemFlags::Expanded))
		{
			ContainerListFriend->SetItemExpansion(i, true);
		}

		if (i->List.Num())
		{
			for (auto &s : i->List)
			{
				if (FFlagsHelper::HasAnyFlags(s->Flags, EFriendsTreeItemFlags::Expanded))
				{
					ContainerListFriend->SetItemExpansion(s, true);
				}
			}
		}
	}

	bIsFirstUpdate = false;
}

void SFriendsForm::AppendRowsSquad(const TArray<TSharedPtr<FQueueMemberContainer>>& SquadRows)
{
	SquadMembers += SquadRows;
	SquadContainer->RequestListRefresh();
}

void SFriendsForm::OnSquapInformationUpdate(const FSqaudInformation& information)
{
	SquadMembers.Empty();

	AGameState* GameState = nullptr;
	if (PlayerState.IsValid() && PlayerState->GetWorld() && PlayerState->GetWorld()->GetGameState<AGameState>())
	{
		GameState = PlayerState->GetWorld()->GetGameState<AGameState>();
	}

	for (auto member : information.Members)
	{
		if (GameState && GameState->IsValidLowLevel())
		{
			for (auto pState : GameState->PlayerArray)
			{
				if (auto cState = Cast<AConstructionPlayerState>(pState))
				{
					if (cState->GetIdentityComponent() && cState->GetIdentityComponent()->GetPlayerInfo().PlayerId == member.PlayerId)
					{
						member.PlayerState = pState;
					}
				}
			}
		}

		SquadMembers.Add(MakeShareable(new FQueueMemberContainer(member)));
	}

	SquadContainer->RequestListRefresh();
}
