//// VRSPRO

#pragma once

#include "Windows/SWindowBase.h"
#include "Styles/FriendsFormWidgetStyle.h"


#include "SFriendsForm_Item.h"
#include "SFriendsForm_Squad.h"

struct FPlayerRatingTopContainer;
class ABasePlayerState;
struct FSqaudInformation;
struct FPlayerBasicInformation;

class LOKAGAME_API SFriendsForm : public SWindowBase
{
public:
	SLATE_BEGIN_ARGS(SFriendsForm)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FFriendsFormStyle>("SFriendsFormStyle"))
	{
		_Visibility = EVisibility::Collapsed;
	}
	SLATE_STYLE_ARGUMENT(FFriendsFormStyle, Style)
	SLATE_ARGUMENT(TWeakObjectPtr<ABasePlayerState>, PlayerState)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs, const TSharedRef<SWindowsContainer>& InOwner);
	~SFriendsForm();

	void OnFillingList(const TArray<FPlayerBasicInformation>& friends, const bool IsFriend);
	void AppendRows(const TArray<FPlayerBasicInformation>&);

	void AppendRowsSquad(const TArray<TSharedPtr<FQueueMemberContainer>>&);

	void OnSquapInformationUpdate(const FSqaudInformation& information);
	void OnContextMenuClosed();
	void OnContextMenuClosed(TSharedRef<IMenu> InMenu);
	

protected:

	TArray<FPlayerBasicInformation> LastData;

	TWeakObjectPtr<ABasePlayerState> PlayerState;

	TSharedPtr<SListView<TSharedPtr<FPlayerRatingTopContainer>>> TopContainer;
	TSharedPtr<SListView<TSharedPtr<FQueueMemberContainer>>> SquadContainer;
	TArray<TSharedPtr<FQueueMemberContainer>> SquadMembers;
	TArray<TSharedPtr<FPlayerRatingTopContainer>> TopPlayersArray;

	TArray<TSharedPtr<FFriendsTreeItem>>
		FriendsList, 
		SearchList;

	TSharedPtr<STreeView<TSharedPtr<FFriendsTreeItem>>>
		ContainerListFriend,
		ContainerListSearch;

	void OpenContextMenu(TSharedRef<FPlayerBasicInformation>);
	void OnSearch(const FText&, ETextCommit::Type);

	void OnModeChange(ECheckBoxState, const bool);
	ECheckBoxState GetCurrentMode(const bool) const;

	bool CurrentMode;
	bool bIsSearchState;
	bool bIsMenuOpen;
	bool bIsFirstUpdate;

	const FFriendsFormStyle *Style;	
};
