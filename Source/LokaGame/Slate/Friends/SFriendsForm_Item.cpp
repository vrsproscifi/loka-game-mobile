// VRSPRO

#include "LokaGame.h"
#include "SFriendsForm_Item.h"
#include "SlateOptMacros.h"

#include "STextBlockButton.h"
#include "SLokaToolTip.h"

#include "SScaleBox.h"
#include "Utilities/SlateUtilityTypes.h"
#include "SPlayerThumbnail.h"
#include "GameSingleton.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SFriendsForm_Item::Construct(const FArguments& InArgs, const TSharedRef<STableViewBase>& OwnerTable, TSharedPtr<FFriendsTreeItem> InInstance)
{
	Instance = InInstance;
	Style = InArgs._Style;
	OnClickedFriend = InArgs._OnClickedFriend;
		
	if (Instance->Source.IsValid()) // Construct as friend
	{
		SSuper::Construct(SSuper::FArguments().Padding(FMargin(2, 4))
		[
			SNew(SButton)
			.ButtonStyle(&Style->List.Button)
			.ContentPadding(0)
			.OnClicked_Lambda([&]() {
				OnClickedFriend.ExecuteIfBound();
				return FReply::Handled();
			})
			[
				SNew(SPlayerThumbnail)
				.Name(FText::FromString(Instance->Source->Name))
				.IsLocal(Instance->Source->PlayerId == UGameSingleton::Get()->GetSavedPlayerEntityInfo().PlayerId)
				.PlayerId(Instance->Source->PlayerId)
				.Status(Instance->Source->Status)				
			]
		]
		, OwnerTable);
	}
	else // Construct as list
	{
		SSuper::Construct(SSuper::FArguments().Padding(FMargin(20, 6))
		[
			SNew(STextBlockButton)
			.Style(FFlagsHelper::HasAnyFlags(Instance->Flags, EFriendsTreeItemFlags::HeaderBig) ? &Style->List.BigHeader : (FFlagsHelper::HasAnyFlags(Instance->Flags, EFriendsTreeItemFlags::HeaderMedium) ? &Style->List.MiddleHeader : &Style->List.SmallHrader))
			.Text(Instance->Name)
			.OnClicked_Lambda([&]() {
				if (IsItemExpanded())
				{
					FFlagsHelper::ClearFlags(Instance->Flags, EFriendsTreeItemFlags::Expanded);
				}
				else
				{
					FFlagsHelper::SetFlags(Instance->Flags, EFriendsTreeItemFlags::Expanded);
				}						
				ToggleExpansion();
				return FReply::Handled();
			})
		]
		, OwnerTable);
	}
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

const FSlateBrush* SFriendsForm_Item::GetStatusImage(const FFriendsFormStyle* InStyle, const EPlayerFriendStatus InFlag)
{
	if (FFlagsHelper::HasAnyFlags(InFlag, EPlayerFriendStatus::PlayGame))
	{
		return &InStyle->Status.Playing;
	}
	else if (FFlagsHelper::HasAnyFlags(InFlag, EPlayerFriendStatus::Online | EPlayerFriendStatus::Global))
	{
		return &InStyle->Status.Online;
	}
	else if (FFlagsHelper::HasAnyFlags(InFlag, EPlayerFriendStatus::SearchGame))
	{
		return &InStyle->Status.Waiting;
	}
	else if (FFlagsHelper::HasAnyFlags(InFlag, EPlayerFriendStatus::Building))
	{
		return &InStyle->Status.Building;
	}
	else
	{
		return &InStyle->Status.Offline;
	}
}

FText SFriendsForm_Item::GetStatusToolTip(const EPlayerFriendStatus InFlag)
{
	if (FFlagsHelper::HasAnyFlags(InFlag, PlayerFriendStatus::PlayGame))
	{
		return NSLOCTEXT("SFriendsFormItem", "Playing", "Playing");
	}
	else if (FFlagsHelper::HasAnyFlags(InFlag, PlayerFriendStatus::Online | PlayerFriendStatus::Global))
	{
		return NSLOCTEXT("SFriendsFormItem", "Online", "Online");
	}
	else if (FFlagsHelper::HasAnyFlags(InFlag, PlayerFriendStatus::SearchGame))
	{
		return NSLOCTEXT("SFriendsFormItem", "Waiting", "Waiting game");
	}
	else if (FFlagsHelper::HasAnyFlags(InFlag, PlayerFriendStatus::Building))
	{
		return NSLOCTEXT("SFriendsFormItem", "Building", "Construction");
	}
	else
	{
		return NSLOCTEXT("SFriendsFormItem", "Offline", "Offline");
	}
}
