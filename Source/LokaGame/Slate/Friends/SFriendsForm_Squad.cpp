//// VRSPRO
//
#include "LokaGame.h"
#include "SFriendsForm.h"
#include "SlateOptMacros.h"
#include "STextBlockButton.h"
#include "IdentityController/Models/PlayerEntityInfo.h"

#include "SLokaToolTip.h"
#include "GameSingleton.h"
#include "ConstructionPlayerState.h"
#include "SScaleBox.h"
#include "Utilities/SlateUtilityTypes.h"
#include "SPlayerThumbnail.h"

FName FSquadColumn::Status = FName("Status");
FName FSquadColumn::Name = FName("Name");
FName FSquadColumn::Action = FName("Action");

void SFriendsForm_Squad::Construct(const FArguments& InArgs, TSharedRef<FQueueMemberContainer> InInstance)
{
	Style = InArgs._Style;
	Instance = InInstance;
	OnRemoveMember = InArgs._OnRemoveMember;

	auto lWidget = [&]() -> TSharedRef<SWidget>
	{
		MAKE_UTF8_SYMBOL(sClose, 0xf00d);

		PlayerFriendStatus::Type Value = Instance->Status.GetValue();

		if (auto pState = Cast<AConstructionPlayerState>(Instance->PlayerState))
		{
			MAKE_UTF8_SYMBOL(sSelect, 0xf12d);
			MAKE_UTF8_SYMBOL(sBuild, 0xf1b2);

#define BTN_MACRO(sym, flag, style)\
			SNew(STextBlockButton).Text(FText::FromString(sym)).Style(style)\
			.IsEnabled_Lambda([&]()\
			{\
				return UGameSingleton::Get()->GetSavedPlayerEntityInfo().IsSquadLeader;\
			})\
				.OnClicked_Lambda([&, ts = pState]()\
			{\
				auto MyFlags = ts->AllowFlags;\
				FFlagsHelper::ToggleFlags<int32, int32>(MyFlags, flag);\
				ts->ChangeAllowFlags(MyFlags, UGameSingleton::Get()->GetSavedPlayerEntityInfo().PlayerId);\
				return FReply::Handled();\
			})

			return SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().AutoWidth()
				[
					SNew(SWidgetSwitcher)
					.WidgetIndex_Lambda([&, ts = pState]() { return FFlagsHelper::HasAnyFlags(ts->AllowFlags, EAllowConstructionMode::Selecting); })
					+ SWidgetSwitcher::Slot()
					[
						BTN_MACRO(sSelect, EAllowConstructionMode::Selecting, &Style->Squad.ModeDisallow)
					]
					+ SWidgetSwitcher::Slot()
					[
						BTN_MACRO(sSelect, EAllowConstructionMode::Selecting, &Style->Squad.ModeAllow)
					]
				]
				+ SHorizontalBox::Slot().AutoWidth()
				[
					SNew(SWidgetSwitcher)
					.WidgetIndex_Lambda([&, ts = pState]() { return FFlagsHelper::HasAnyFlags(ts->AllowFlags, EAllowConstructionMode::Building); })
					+ SWidgetSwitcher::Slot()
					[
						BTN_MACRO(sBuild, EAllowConstructionMode::Building, &Style->Squad.ModeDisallow)
					]
					+ SWidgetSwitcher::Slot()
					[
						BTN_MACRO(sBuild, EAllowConstructionMode::Building, &Style->Squad.ModeAllow)
					]
				]
				+ SHorizontalBox::Slot().AutoWidth()
				[
					SNew(STextBlockButton).Text(FText::FromString(sClose)).Style(&Style->Squad.Remove).IsEnabled_Lambda([&]()
					{
						return (Instance->PlayerId == UGameSingleton::Get()->GetSavedPlayerEntityInfo().PlayerId || UGameSingleton::Get()->GetSavedPlayerEntityInfo().IsSquadLeader);
					})
					.OnClicked_Lambda([&]()
					{
						OnRemoveMember.ExecuteIfBound(); return FReply::Handled();
					})
				];
		}
		else
		{
			return SNew(STextBlockButton).Text(FText::FromString(sClose)).Style(&Style->Squad.Remove).IsEnabled_Lambda([&, &v = Value]()
			{
				return (Instance->PlayerId == UGameSingleton::Get()->GetSavedPlayerEntityInfo().PlayerId || UGameSingleton::Get()->GetSavedPlayerEntityInfo().IsSquadLeader);
			}).OnClicked_Lambda([&]() { OnRemoveMember.ExecuteIfBound(); return FReply::Handled(); });
		}
	};

	ChildSlot
	[
		SNew(SHorizontalBox)
		+ SHorizontalBox::Slot().AutoWidth()
		[
			SNew(SPlayerThumbnail)
			.Name(FText::FromString(Instance->Name))
			.IsLocal(Instance->PlayerId == UGameSingleton::Get()->GetSavedPlayerEntityInfo().PlayerId)
			.PlayerId(Instance->PlayerId)
			.Status(Instance->Status)
			.IsSquad(true)
		]
		+ SHorizontalBox::Slot().FillWidth(1)
		+ SHorizontalBox::Slot().AutoWidth()
		[
			lWidget()
		]
	];
}
