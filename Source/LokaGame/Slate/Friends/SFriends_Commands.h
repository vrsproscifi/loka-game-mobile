//// VRSPRO

#pragma once

#include "Widgets/SCompoundWidget.h"
//#include "LobbyController/Operation/OnAttackTo.h"


DECLARE_MULTICAST_DELEGATE(FOnFriendsCommandsAnyAction);

class ABasePlayerState;
class ALobbyGameMode;
class UFriendComponent;
class USquadComponent;

class SFriendsCommands : public TCommands<SFriendsCommands>
{
public:

	SFriendsCommands() : TCommands<SFriendsCommands>
	(
		"SFriendsCommands", // Context name for fast lookup
		NSLOCTEXT("SFriendsCommands", "SFriendsCommands", "SFriendsCommands"), // Localized context name for displaying
		NAME_None, FCoreStyle::Get().GetStyleSetName()
	)
	{}

	enum ECommand
	{
		PrivateMessage,
		AddBlockList,
		AddFriend,
		AcceptInvite,
		DeclineInvite,
		DeleteFriend,
		SquadInvite,
		SquadRequest,
		DuelInvite,
		ConstructionInvite,
		ConstructionRequest,
		Attack,
		GuestView,
		End
	};	

	virtual void RegisterCommands() override;

	FORCENOINLINE static SFriendsCommands& GetAlt()
	{
		if (Instance.IsValid() == false)
		{
			SFriendsCommands::Register();
		}

		return *(Instance.Pin());
	}

	FORCEINLINE TSharedRef<FUICommandList> GetCommandList() const { return UICommandList.ToSharedRef(); }
	FORCEINLINE TSharedRef<FUICommandInfo> GetCommand(const ECommand& InCommandTarget) const { return Commands[InCommandTarget].ToSharedRef(); }
	FORCEINLINE void SetCurrentTargetId(const FGuid& InTargetId) { CurrentTargetId = InTargetId; }

	void SetPlayerState(TWeakObjectPtr<ABasePlayerState> InPlayerState);

	FOnFriendsCommandsAnyAction OnAnyAction;

protected:

	TSharedPtr<FUICommandInfo> Commands[ECommand::End];
	TSharedPtr<FUICommandList> UICommandList;
	FGuid CurrentTargetId;

	TWeakObjectPtr<ABasePlayerState> PlayerState;

	USquadComponent* GetSquadComponent() const;
	UFriendComponent* GetFriendComponent() const;
};
