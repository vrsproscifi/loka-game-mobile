// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/DetailsBoxWidgetStyle.h"

struct FDetailsBoxParameter
{
	FText Name;
	FText ToolTipText;
	float Value;
	float MaxValue;
	float UpgradeValue;
	bool IsReverseColor;

	FDetailsBoxParameter() : Name(), Value(0), MaxValue(400), UpgradeValue(0), IsReverseColor(false), ToolTipText() {}
	FDetailsBoxParameter(const FText &_Name, const float &_Value) : Name(_Name), Value(_Value), MaxValue(400), UpgradeValue(0), IsReverseColor(false), ToolTipText() {}
	FDetailsBoxParameter(const FText &_Name, const float &_Value, const float &_MaxValue, const bool &IsReverse = false, const FText& Tip = FText::GetEmpty()) : Name(_Name), Value(_Value), MaxValue(_MaxValue), UpgradeValue(0), IsReverseColor(IsReverse), ToolTipText(Tip) {}
};

class LOKAGAME_API SDetailsBox : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SDetailsBox)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FDetailsBoxStyle>("SDetailsBoxStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FDetailsBoxStyle, Style)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

	void AddParameter(const FName &Name, const FDetailsBoxParameter &Param);
	void AddParameterUpgradeValue(const FName &Name, const float &_UpgradeValue);
	bool UpdateParameter(const FName &Name, const FDetailsBoxParameter &Param);
	bool RemoveParameter(const FName &Name);
	bool RemoveParameters();
	FDetailsBoxParameter* GetParameter(const FName &Name);
	void RebuildParameters();

protected:

	TMap<FName, TSharedPtr<class SDetailsBoxRow>> Parameters;

	TSharedPtr<class SVerticalBox> ContainerParameters;

	const FDetailsBoxStyle *Style;
};
