// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "Styles/TabbedContainerWidgetStyle.h"

/**
 * 
 */
class LOKAGAME_API STabbedContainer : public SCompoundWidget
{
public:

	class FSlot
	{
		friend class STabbedContainer;

	public:

		HACK_SLATE_SLOT_ARGS(FSlot)
			: _Name(FText::FromString("Unknown"))
			, _Padding(FMargin(4))
			, _IsAutoSize(false)
		{}
		SLATE_ATTRIBUTE(FText, Name)
		SLATE_ATTRIBUTE(bool, IsAutoSize)
		SLATE_ARGUMENT(FMargin, Padding)
		SLATE_DEFAULT_SLOT(FArguments, Content)
		SLATE_END_ARGS()

		FSlot(const FArguments& InArgs)
			: Name(InArgs._Name)
			, Content(InArgs._Content)
			, Padding(InArgs._Padding)
			, IsAutoSize(InArgs._IsAutoSize)
		{}

	protected:

		TAttribute<bool> IsAutoSize;
		TAttribute<FText> Name;
		TAlwaysValidWidget Content;
		FMargin Padding;
	};

	SLATE_BEGIN_ARGS(STabbedContainer)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FTabbedContainerStyle>(TEXT("STabbedContainerStyle")))
		, _Orientation(Orient_Horizontal)
	{}
	SLATE_STYLE_ARGUMENT(FTabbedContainerStyle, Style)
	SLATE_SUPPORTS_SLOT_WITH_ARGS(FSlot)
	SLATE_ARGUMENT(EOrientation, Orientation)
	SLATE_END_ARGS()

	static FSlot::FArguments Slot()
	{
		return FSlot::FArguments();
	}

	void Construct(const FArguments& InArgs);

	void AddSlot(const FSlot::FArguments& InSlot);
	bool AddSlotUnique(const FSlot::FArguments& InSlot);
	void ClearChildren();
	bool SetActiveSlot(const int32 InSlotIndex);
	bool SetActiveSlot(const TSharedRef<SWidget> InWidgetRef);

	ECheckBoxState GetControllState(const int32 InIndex) const;
	int32 GetActiveSlot() const;

protected:

	void ClearChildrenInternal();
	void RegenerateWidgets();

	void OnControllStateChanged(ECheckBoxState InState, const int32 InIndex);

	EOrientation Orientation;
	int32 ActiveSlot;

	const FTabbedContainerStyle* Style;

	TSharedPtr<SHorizontalBox> Widget_ControllsHContainer;
	TSharedPtr<SVerticalBox> Widget_ControllsVContainer;
	TSharedPtr<SWidgetSwitcher> Widget_ContentContainer;

	TIndirectArray<FSlot> Slots;
};
