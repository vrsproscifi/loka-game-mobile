// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SBuildSystem_Inventory.h"
#include "Components/SResourceTextBox.h"
#include "Components/SWindowBackground.h"
#include "SlateOptMacros.h"

#include "Build/ItemBuildEntity.h"
#include "PlayerInventoryItem.h"

class LOKAGAME_API SBuildSystem_Inventory_Item : public SButton
{
public:
	SLATE_BEGIN_ARGS(SBuildSystem_Inventory_Item)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FBuildSystemStyle>("SBuildSystemStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FBuildSystemStyle, Style)
	SLATE_EVENT(FOnBuildAction, OnClickedBuyItem)
	SLATE_ATTRIBUTE(bool, IsSellingMode)
	SLATE_END_ARGS()

	BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
	void Construct(const FArguments& InArgs, UPlayerInventoryItem* InItem)
	{
		Style = InArgs._Style;
		IsSellingMode = InArgs._IsSellingMode;

		SButton::Construct(
			SButton::FArguments()
			.ContentPadding(4)
			.ButtonStyle(&Style->Inventory.ItemBuyButton)
			.OnClicked_Lambda([&, i = InItem, e = InArgs._OnClickedBuyItem]() { e.ExecuteIfBound(i); return FReply::Handled(); })
			[
				SNew(SBox).WidthOverride(400)
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot().AutoWidth()
					[
						SAssignNew(Widget_Item, SBuildSystem_Item, InItem).Visibility(EVisibility::HitTestInvisible)
					]
					+ SHorizontalBox::Slot()
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Fill).Padding(FMargin(4, 0))
						[
							SNew(STextBlock).Text_Lambda([i = InItem->GetEntity()]() { return i->GetDescription().Name; })
							.TextStyle(&Style->Inventory.ItemName)
							.AutoWrapText(true).Visibility(EVisibility::HitTestInvisible)
						]
						+ SVerticalBox::Slot().HAlign(HAlign_Fill).Padding(FMargin(4, 2))
						[
							SNew(STextBlock).Text_Lambda([i = InItem->GetEntity()]() { return i->GetDescription().Description; })
							.TextStyle(&Style->Inventory.ItemDesc)
							.AutoWrapText(true).Visibility(EVisibility::HitTestInvisible)
						]
						+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(8, 2))
						[
							SNew(SHorizontalBox)
							+ SHorizontalBox::Slot().HAlign(HAlign_Left)
							[
								SNew(SButton)
								.ButtonStyle(&Style->Inventory.ItemSubButton)
								.OnClicked_Lambda([&, i = InItem, e = InArgs._OnClickedBuyItem]() { e.ExecuteIfBound(i); return FReply::Handled(); })
								[
									SNew(STextBlock).Text_Lambda([&]() { return IsSellingMode.Get() ? NSLOCTEXT("All", "All.Sell", "Sell") : NSLOCTEXT("All", "All.Buy", "Buy"); })
									.TextStyle(&Style->Inventory.ItemName)
									.AutoWrapText(true).Visibility(EVisibility::HitTestInvisible)
								]
							]
							+ SHorizontalBox::Slot().HAlign(HAlign_Right).AutoWidth()
							[
								SNew(SResourceTextBox)
								.CustomSize(FVector(16, 16, 10)).Visibility(EVisibility::HitTestInvisible)
								.TypeAndValue_Lambda([&, i = InItem->GetEntity()]{ return IsSellingMode.Get() ? FResourceTextBoxValue(i->GetCostAsResource().Type, i->GetCostAsResource().Value / 2) : i->GetCostAsResource(); })
							]
						]
					]
				]
			]
		);
	}
	END_SLATE_FUNCTION_BUILD_OPTIMIZATION

protected:

	const FBuildSystemStyle* Style;
	TSharedPtr<SBuildSystem_Item> Widget_Item;
	TAttribute<bool> IsSellingMode;

	virtual FReply OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override
	{
		if (MouseEvent.GetEffectingButton() == EKeys::LeftMouseButton)
		{
			FReply Reply = SButton::OnMouseButtonDown(MyGeometry, MouseEvent);
			return Reply.DetectDrag(AsShared(), EKeys::LeftMouseButton);
		}

		return SButton::OnMouseButtonDown(MyGeometry, MouseEvent);
	}

	virtual FReply OnDragDetected(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override
	{
		if (Widget_Item.IsValid())
		{
			Release();
			return Widget_Item->OnDragDetected(MyGeometry, MouseEvent);
		}

		return FReply::Unhandled();
	}
};


BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SBuildSystem_Inventory::Construct(const FArguments& InArgs, TSharedRef<SWindowsContainer> InOwnerRef)
{
	Style = InArgs._Style;
	OnClickedBuyItem = InArgs._OnClickedBuyItem;
	OnClickedSellItem = InArgs._OnClickedSellItem;

	MAKE_UTF8_SYMBOL(sBuild, 0xf1b2);
	
	auto SuperArgs = SWindowBase::FArguments()
		.StartPosition(FVector2D(130, 220))
		.QuickButton(FText::FromString(sBuild))
		.QuickButtonOrder(5)
		.CanInvisable(false)
		.SizeLimits(FBox2D(FVector2D(800, 480), FVector2D(1600, 900)))
		.TitleText(NSLOCTEXT("BuildSystem", "BuildSystem.Inventory.Title", "Construction Mode Inventory"))
		.Content()
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot().AutoWidth().Padding(6, 0)
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot().AutoHeight()
				[
					SAssignNew(Widget_CatContainer, SVerticalBox)
					+ SVerticalBox::Slot().AutoHeight()
					[
						SNew(SCheckBox)
						.Type(ESlateCheckBoxType::ToggleButton)
						.Style(&Style->Inventory.SortButton)
						.OnCheckStateChanged(this, &SBuildSystem_Inventory::OnSortChanged, FName(NAME_None))
						.IsChecked(this, &SBuildSystem_Inventory::GetIsChecked, FName(NAME_None))
						[
							SNew(SBox)
							.Padding(0)
							.HAlign(HAlign_Left)
							.VAlign(VAlign_Center)
							[
								SNew(STextBlock)
								.Font(this, &SBuildSystem_Inventory::GetSortFont, FName(NAME_None))
								.ColorAndOpacity(this, &SBuildSystem_Inventory::GetSortFontColor, FName(NAME_None))
								.Margin(4)
								.Text(NSLOCTEXT("All", "All", "All"))
							]
						]
					]
				]
				+ SVerticalBox::Slot()
				+ SVerticalBox::Slot().AutoHeight()
				[
					SNew(SCheckBox)
					.Type(ESlateCheckBoxType::ToggleButton)
					.Style(&Style->Inventory.SortButton)
					.OnCheckStateChanged(this, &SBuildSystem_Inventory::OnSortChanged, FName(NAME_Name))
					.IsChecked(this, &SBuildSystem_Inventory::GetIsChecked, FName(NAME_Name))
					[
						SNew(SBox)
						.Padding(0)
						.HAlign(HAlign_Left)
						.VAlign(VAlign_Center)
						[
							SNew(STextBlock)
							.Font(this, &SBuildSystem_Inventory::GetSortFont, FName(NAME_Name))
							.ColorAndOpacity(this, &SBuildSystem_Inventory::GetSortFontColor, FName(NAME_Name))
							.Margin(4)
							.Text(NSLOCTEXT("SBuildSystem_Inventory", "SBuildSystem_Inventory.Inventory", "Inventory"))
						]
					]
				]
			]
			+ SHorizontalBox::Slot()
			[
				SNew(SScrollBox)
				.AllowOverscroll(EAllowOverscroll::Yes)
				.ConsumeMouseWheel(EConsumeMouseWheel::WhenScrollingPossible)
				.ScrollBarAlwaysVisible(true)
				.ScrollBarStyle(&FLokaStyle::Get().GetWidgetStyle<FScrollBarStyle>("Default_ScrollBar"))
				.ScrollBarThickness(FVector2D(4, 4))
				+ SScrollBox::Slot()
				[
					SAssignNew(Widget_WrapBox, SWrapBox)
					.UseAllottedWidth(true)
					.InnerSlotPadding(FVector2D(2, 2))
				]
			]
		];

	SWindowBase::Construct(SuperArgs, InOwnerRef);

	AddedCategorys.Add(NAME_None);

	CurrentCategory = NAME_None;
	CurrentSubCategory = NAME_None;
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SBuildSystem_Inventory::OnFillList(const TArray<UPlayerInventoryItem*>& InList)
{
	Widget_WrapBox->ClearChildren();

	for (auto Item : InList)
	{
		auto TargetItem = Item ? Item->GetEntity<UItemBuildEntity>() : nullptr;
		if (TargetItem && TargetItem->IsAllowToBuy)
		{
			const FName ItemCategory = TargetItem->GetCategoryName().IsEmpty() ? FName(NAME_None) : FName(*TargetItem->GetCategoryName().BuildSourceString());
			const FName ItemSubCategory = TargetItem->GetSubCategoryName().IsEmpty() ? FName(NAME_None) : FName(*TargetItem->GetSubCategoryName().BuildSourceString());

			Widget_WrapBox->AddSlot()
				[
					SNew(SBuildSystem_Inventory_Item, Item)
					.IsSellingMode_Lambda([&]() { return (CurrentCategory == NAME_Name); })
					.OnClickedBuyItem_Lambda([&](UPlayerInventoryItem* InItem)
					{
						if (CurrentCategory == NAME_Name) OnClickedSellItem.ExecuteIfBound(InItem);
						else OnClickedBuyItem.ExecuteIfBound(InItem);
					})
					.Visibility_Lambda([&, i = Item, c = ItemCategory, sc = ItemSubCategory]() {
						return (CurrentCategory == NAME_Name && i->GetCount() > 0) || ((CurrentCategory == c || CurrentCategory == NAME_None) && (CurrentSubCategory == sc || CurrentSubCategory == NAME_None)) ? EVisibility::Visible : EVisibility::Collapsed;
					})
				];
			
			if (AddedCategorys.Contains(ItemCategory) == false)
			{
				AddedCategorys.Add(ItemCategory);
				
				TSharedRef<SVerticalBox> VBox = SNew(SVerticalBox);

				Widget_CatContainer->AddSlot().AutoHeight()
				[
					SNew(SVerticalBox)
					+ SVerticalBox::Slot().AutoHeight()
					[
						SNew(SCheckBox)
						.Type(ESlateCheckBoxType::ToggleButton)
						.Style(&Style->Inventory.SortButton)
						.OnCheckStateChanged(this, &SBuildSystem_Inventory::OnSortChanged, ItemCategory)
						.IsChecked(this, &SBuildSystem_Inventory::GetIsChecked, ItemCategory)
						[
							SNew(SBox)
							.Padding(0)
							.HAlign(HAlign_Left)
							.VAlign(VAlign_Center)
							[
								SNew(STextBlock)
								.Font(this, &SBuildSystem_Inventory::GetSortFont, ItemCategory)
								.ColorAndOpacity(this, &SBuildSystem_Inventory::GetSortFontColor, ItemCategory)
								.Margin(4)
								.Text(TargetItem->GetCategoryName())
							]
						]
					]
					+ SVerticalBox::Slot().AutoHeight().Padding(10, 0, 0, 0)
					[
						VBox
					]
				];

				SubCatContainersMap.Add(ItemCategory, VBox);
			}			

			if (AddedSubCategorysMap.FindOrAdd(ItemCategory).Contains(ItemSubCategory) == false && SubCatContainersMap.Contains(ItemCategory))
			{
				AddedSubCategorysMap.FindOrAdd(ItemCategory).AddUnique(ItemSubCategory);
				AddedSubCategorysMap.FindOrAdd(ItemCategory).AddUnique(FName(NAME_None));

				SubCatContainersMap.FindChecked(ItemCategory)->AddSlot().AutoHeight()
					[
						SNew(SCheckBox)
						.Type(ESlateCheckBoxType::ToggleButton)
						.Style(&Style->Inventory.SubSortButton)
						.OnCheckStateChanged(this, &SBuildSystem_Inventory::OnSubSortChanged, ItemSubCategory)
						.IsChecked(this, &SBuildSystem_Inventory::GetIsCheckedSub, ItemSubCategory)
						.Visibility_Lambda([&, c = ItemCategory, sc = ItemSubCategory]() {
							return CurrentCategory == c && AddedSubCategorysMap.Contains(CurrentCategory) && AddedSubCategorysMap.FindChecked(CurrentCategory).Contains(sc) ? EVisibility::Visible : EVisibility::Collapsed;
						})
						[
							SNew(SBox)
							.Padding(0)
							.HAlign(HAlign_Left)
							.VAlign(VAlign_Center)
							[
								SNew(STextBlock)
								.Font(this, &SBuildSystem_Inventory::GetSubSortFont, ItemSubCategory)
								.ColorAndOpacity(this, &SBuildSystem_Inventory::GetSubSortFontColor, ItemSubCategory)
								.Margin(FMargin(2, 4))
								.Text(TargetItem->GetSubCategoryName())
							]
						]
					];
			}
		}
	}
}

void SBuildSystem_Inventory::OnSortChanged(ECheckBoxState InState, const FName InCategory)
{	
	CurrentCategory = InCategory;
	CurrentSubCategory = FName(NAME_None);
}

ECheckBoxState SBuildSystem_Inventory::GetIsChecked(const FName InCategory) const
{
	return (CurrentCategory == InCategory) ? ECheckBoxState::Checked : ECheckBoxState::Unchecked;
}

void SBuildSystem_Inventory::OnSubSortChanged(ECheckBoxState InState, const FName InCategory)
{
	CurrentSubCategory = InCategory;
}

ECheckBoxState SBuildSystem_Inventory::GetIsCheckedSub(const FName InCategory) const
{
	return (CurrentSubCategory == InCategory) ? ECheckBoxState::Checked : ECheckBoxState::Unchecked;
}

FSlateFontInfo SBuildSystem_Inventory::GetSortFont(const FName InCategory) const
{
	return CurrentCategory == InCategory ? Style->Inventory.SortButtonTextSelected.Font : Style->Inventory.SortButtonText.Font;
}

FSlateFontInfo SBuildSystem_Inventory::GetSubSortFont(const FName InCategory) const
{
	return CurrentSubCategory == InCategory ? Style->Inventory.SubSortButtonTextSelected.Font : Style->Inventory.SubSortButtonText.Font;
}

FSlateColor SBuildSystem_Inventory::GetSortFontColor(const FName InCategory) const
{
	return CurrentCategory == InCategory ? Style->Inventory.SortButtonTextSelected.ColorAndOpacity : Style->Inventory.SortButtonText.ColorAndOpacity;
}

FSlateColor SBuildSystem_Inventory::GetSubSortFontColor(const FName InCategory) const
{
	return CurrentSubCategory == InCategory ? Style->Inventory.SubSortButtonTextSelected.ColorAndOpacity : Style->Inventory.SubSortButtonText.ColorAndOpacity;
}
