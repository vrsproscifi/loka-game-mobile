// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "Windows/SWindowBase.h"
#include "Styles/BuildSystemWidgetStyle.h"

#include "Types/TypeCash.h"


class SBuildSystem_ToolBar_Item;
class SNotifyList;
class UItemBuildEntity;
class UPlayerInventoryItem;

DECLARE_DELEGATE_TwoParams(FOnDropBuildInSlot, const int32&, UPlayerInventoryItem*);

class LOKAGAME_API SBuildSystem_ToolBar : public SWindowBase
{
public:
	SLATE_BEGIN_ARGS(SBuildSystem_ToolBar)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FBuildSystemStyle>("SBuildSystemStyle"))
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FBuildSystemStyle, Style)
	SLATE_ATTRIBUTE(int32, SelectedSlot)
	SLATE_ATTRIBUTE(int32, CurrentMode)
	SLATE_EVENT(FOnDropBuildInSlot, OnDropBuildInSlot)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, TSharedRef<SWindowsContainer> InOwnerRef);

	void SetSlotItem(const int32& InSlotTarget, UPlayerInventoryItem* InItem);
	UPlayerInventoryItem* GetSlotItem(const int32& InSlotTarget) const;

	enum ETipBindType
	{
		ActionPlace,
		ActionCancel,
		ActionRotate,
		ActionInventory,
		ActionMode,
		End
	};

	virtual bool CanClosable() const override { return false; }
	virtual bool HasQuickButton() const override { return false; }

protected:

	const FBuildSystemStyle* Style;

	TSharedPtr<SBuildSystem_ToolBar_Item> Widget_Slot[10];
	TAttribute<int32> SelectedSlot, CurrentMode;
	FOnDropBuildInSlot OnDropBuildInSlot;

	bool IsSelectedSlot(const int32 InSlotTarget) const;

	FText GetCurrentActionTip(const ETipBindType InAction) const;

	int32 HintIteration;
	EActiveTimerReturnType OnHintIteration(double InCurrentTime, float InDeltaTime);
};
