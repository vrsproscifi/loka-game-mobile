// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SBuildSystem_ToolBar.h"
#include "SBuildSystem_Item.h"
#include "SlateOptMacros.h"

#include "SKeyBinding.h"
#include "SResourceTextBox.h"
#include "LokaDragDrop.h"
#include "SBackgroundBlur.h"

#include "Build/ItemBuildEntity.h"
#include "BuildSystem/BuildHelperComponent.h"
#include "Styles/BackgroundBlurWidgetStyle.h"
#include "IdentityController/Models/PlayerEntityInfo.h"
#include "PlayerInventoryItem.h"


class LOKAGAME_API SBuildSystem_ToolBar_Item : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SBuildSystem_ToolBar_Item)
		: _IsModeIcon(false)
	{}
	SLATE_ATTRIBUTE(bool, IsSelected)
	SLATE_EVENT(FOnBuildAction, OnDropBuild)
	SLATE_ARGUMENT(bool, IsModeIcon)
	SLATE_DEFAULT_SLOT(FArguments, Content)
	SLATE_END_ARGS()

	BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
	void Construct(const FArguments& InArgs, const int32& InNumber)
	{
		Attr_IsSelected = InArgs._IsSelected;
		OnDropBuild = InArgs._OnDropBuild;
		EmptyBrush = FSlateNoResource();
		BackgoundNormal = FSlateColorBrush(FColor::Black.WithAlpha(128));
		BackgroundSelected = FSlateColorBrush(FColorList::Orange.WithAlpha(128));
		IsModeIcon = InArgs._IsModeIcon;

		const auto BlurStyle = &FLokaStyle::Get().GetWidgetStyle<FBackgroundBlurStyle>("Default_BackgroundBlur");

		if (IsModeIcon)
		{
			ChildSlot
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Center).VAlign(VAlign_Center).Padding(4)
				[
					SNew(SBackgroundBlur)
					.Padding(0)
					.HAlign(HAlign_Fill)
					.VAlign(VAlign_Fill)
					.bApplyAlphaToBlur(BlurStyle->bApplyAlphaToBlur)
					.BlurStrength(BlurStyle->BlurStrength)
					.BlurRadius(BlurStyle->BlurRadius)
					.LowQualityFallbackBrush(&BlurStyle->LowQualityFallbackBrush)
					[
						SNew(SBorder)
						.BorderImage(this, &SBuildSystem_ToolBar_Item::GetItemBackgorund)
						.Padding(4)
						[
							SAssignNew(Widget_Container, SBox).WidthOverride(96).HeightOverride(96).HAlign(HAlign_Center).VAlign(VAlign_Center)
							[
								InArgs._Content.Widget
							]
						]
					]
				]
				+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Center).VAlign(VAlign_Center).Padding(4)
				[
					SNew(SKeyBinding).Binding(TEXT("BuildMode"))
				]
			];
		}
		else
		{
			ChildSlot
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Center).VAlign(VAlign_Center).Padding(4)
				[
					SNew(SBackgroundBlur)
					.Padding(0)
					.HAlign(HAlign_Fill)
					.VAlign(VAlign_Fill)
					.bApplyAlphaToBlur(BlurStyle->bApplyAlphaToBlur)
					.BlurStrength(BlurStyle->BlurStrength)
					.BlurRadius(BlurStyle->BlurRadius)
					.LowQualityFallbackBrush(&BlurStyle->LowQualityFallbackBrush)
					[
						SNew(SBorder)
						.BorderImage(this, &SBuildSystem_ToolBar_Item::GetItemBackgorund)
						.Padding(4)
						[
							SAssignNew(Widget_Container, SBox).WidthOverride(96).HeightOverride(96)
						]
					]
				]
				+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Center).VAlign(VAlign_Center).Padding(4)
				[
					SNew(SKeyBinding).Binding(FString::Printf(TEXT("BuildSelection_%d"), InNumber))
				]
			];
		}
	}
	END_SLATE_FUNCTION_BUILD_OPTIMIZATION

	void SetItem(UPlayerInventoryItem* InItem)
	{
		if (IsModeIcon == false && ItemBuildEntity != InItem)
		{
			ItemBuildEntity = InItem;

			if (ItemBuildEntity.IsValid())
			{
				Widget_Container->SetContent(SNew(SBuildSystem_Item, ItemBuildEntity.Get()).IsToolBarItem(true).OnDragStart(this, &SBuildSystem_ToolBar_Item::OnDragOut));
			}
			else
			{
				Widget_Container->SetContent(SNullWidget::NullWidget);
			}
		}
	}

	UPlayerInventoryItem* GetItem() const
	{
		return ItemBuildEntity.Get();
	}

protected:

	bool IsModeIcon;
	FSlateBrush EmptyBrush, BackgoundNormal, BackgroundSelected;
	TWeakObjectPtr<UPlayerInventoryItem> ItemBuildEntity;
	TAttribute<bool> Attr_IsSelected;
	TSharedPtr<SBox> Widget_Container;
	FOnBuildAction OnDropBuild;

	const FSlateBrush* GetItemImage() const
	{
		if (ItemBuildEntity.IsValid())
		{
			return ItemBuildEntity->GetEntity()->GetImage();
		}

		return &EmptyBrush;
	}

	const FSlateBrush* GetItemBackgorund() const
	{
		if (Attr_IsSelected.Get() == true)
		{
			return &BackgroundSelected;
		}

		return &BackgoundNormal;
	}

	virtual FReply OnDrop(const FGeometry& MyGeometry, const FDragDropEvent& DragDropEvent) override
	{
		if (DragDropEvent.GetOperation().IsValid() && DragDropEvent.GetOperation()->IsOfType<FLokaDragDropOp>() && IsModeIcon == false)
		{
			auto MyOperation = DragDropEvent.GetOperationAs<FLokaDragDropOp>().ToSharedRef();
			auto TargetPointer = MyOperation->GetObjectPtr().IsValid() ? Cast<UPlayerInventoryItem>(MyOperation->GetObjectPtr().Get()) : nullptr;
			if (TargetPointer)
			{
				OnDropBuild.ExecuteIfBound(TargetPointer);
				SetItem(TargetPointer);
				return FReply::Handled().EndDragDrop();
			}
		}

		return FReply::Unhandled();
	}

	void OnDragOut()
	{
		SetItem(nullptr);
		OnDropBuild.ExecuteIfBound(nullptr);
	}
};


BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SBuildSystem_ToolBar::Construct(const FArguments& InArgs, TSharedRef<SWindowsContainer> InOwnerRef)
{
	Style = InArgs._Style;
	SelectedSlot = InArgs._SelectedSlot;
	CurrentMode = InArgs._CurrentMode;
	OnDropBuildInSlot = InArgs._OnDropBuildInSlot;

	TSharedPtr<SHorizontalBox> Widget_Box;

	MAKE_UTF8_SYMBOL(sNone, 0xf05e);
	MAKE_UTF8_SYMBOL(sSelecting, 0xf12d);

	auto SuperArgs = SWindowBase::FArguments()
		.InitAsInvisible(true)
		.StartPosition(FVector2D(332.5f, 880))
		.SizeLimits(FBox2D(FVector2D(1255, 200), FVector2D(1900, 200)))
		.TitleText(NSLOCTEXT("BuildSystem", "BuildSystem.Tool.Title", "Construction Mode Tool Bar"))
		.Content()
		[
			SAssignNew(Widget_Box, SHorizontalBox)
			+ SHorizontalBox::Slot()
			[
				SNew(SBuildSystem_ToolBar_Item, 0).IsModeIcon(true).IsSelected_Lambda([&]() { return (CurrentMode.Get() == EBuildHelperMode::None); })
				[
					SNew(STextBlock)
					.TextStyle(&Style->FontAwesome)
					.Text(FText::FromString(sNone))
				]
			]
			+ SHorizontalBox::Slot()
			[
				SNew(SBuildSystem_ToolBar_Item, 0).IsModeIcon(true).IsSelected_Lambda([&]() { return (CurrentMode.Get() == EBuildHelperMode::Selecting); })
				[
					SNew(STextBlock)
					.TextStyle(&Style->FontAwesome)
					.Text(FText::FromString(sSelecting))
				]
			]
		];

	SWindowBase::Construct(SuperArgs, InOwnerRef);

	for (int32 i = 1; i < 10; ++i)
	{
		Widget_Box->AddSlot()
			[
				SAssignNew(Widget_Slot[i], SBuildSystem_ToolBar_Item, i).IsSelected(this, &SBuildSystem_ToolBar::IsSelectedSlot, i)
				.OnDropBuild_Lambda([&, slot = i](UPlayerInventoryItem* InItem) {
					OnDropBuildInSlot.ExecuteIfBound(slot, InItem);
				})
			];
	}		
		
	//RegisterActiveTimer(1.0f, FWidgetActiveTimerDelegate::CreateSP(this, &SBuildSystem_ToolBar::OnHintIteration));
	//HintIteration = 0;	
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

EActiveTimerReturnType SBuildSystem_ToolBar::OnHintIteration(double InCurrentTime, float InDeltaTime)
{
	//++HintIteration;
	//FText HintText = FText::GetEmpty();
	//FText HintTitle = FText::GetEmpty();

	//switch (HintIteration)
	//{
	//	case 10: 
	//		HintTitle = NSLOCTEXT("BuildSystem", "BuildSystem.Hint.Title.1", "Helllo");
	//		HintText = FText::Format(NSLOCTEXT("BuildSystem", "BuildSystem.Hint.1", "Greetings \"{0}\" Now you are on your own territory, she needs to be protected from bloodthirsty raiders and develop."), FText::FromString(FPlayerEntityInfo::Instance.Login)); 
	//		break;
	//	case 40: 
	//		HintTitle = NSLOCTEXT("BuildSystem", "BuildSystem.Hint.Title.2", "Warning");
	//		HintText = NSLOCTEXT("BuildSystem", "BuildSystem.Hint.2", "Look, your construction is not in the best condition, take in the inventory missing items to install the defense construction."); 
	//		break;
	//	case 80: 
	//		HintTitle = NSLOCTEXT("BuildSystem", "BuildSystem.Hint.Title.3", "Buildings cost");
	//		HintText = NSLOCTEXT("BuildSystem", "BuildSystem.Hint.3", "You probably already noticed that the necessary materials are quite expensive, fortunately they are replenished. You can increase your finances in several ways, for example, devastate other territories, go to battle tasks or place production buildings of crypto-dollar.");
	//		break;
	//	case 120: 
	//		HintTitle = NSLOCTEXT("BuildSystem", "BuildSystem.Hint.Title.4", "Hint");
	//		HintText = NSLOCTEXT("BuildSystem", "BuildSystem.Hint.4", "For a more successful attack, it is better to use a rocket launcher or a grenade launcher."); 
	//		break;
	//	case 130: 
	//		HintTitle = NSLOCTEXT("BuildSystem", "BuildSystem.Hint.Title.4", "Hint");
	//		HintText = NSLOCTEXT("BuildSystem", "BuildSystem.Hint.5", "You can buy new weapons and ammunition in the Control Center(Shop)."); 
	//		break;
	//	case 140: 
	//		HintTitle = NSLOCTEXT("BuildSystem", "BuildSystem.Hint.Title.4", "Hint");
	//		HintText = NSLOCTEXT("BuildSystem", "BuildSystem.Hint.6", "For a more successful attack on enemy territory, you can make squads."); 
	//		break;
	//	case 150: return EActiveTimerReturnType::Stop;
	//}

	//if (HintText.IsEmpty() == false)
	//{
	//	FNotifyInfo NotifyInfo(HintText, HintTitle);
	//	NotifyInfo.ShowTime = 40.0f;

	//	Widget_NotifyList->AddNotify(NotifyInfo);
	//}

	return EActiveTimerReturnType::Stop;
}

void SBuildSystem_ToolBar::SetSlotItem(const int32& InSlotTarget, UPlayerInventoryItem* InItem)
{
	if (InSlotTarget >= 0 && InSlotTarget < ARRAY_COUNT(Widget_Slot) && Widget_Slot[InSlotTarget].IsValid())
	{
		Widget_Slot[InSlotTarget]->SetItem(InItem);
	}
}

UPlayerInventoryItem* SBuildSystem_ToolBar::GetSlotItem(const int32& InSlotTarget) const
{
	if (InSlotTarget >= 0 && InSlotTarget < ARRAY_COUNT(Widget_Slot) && Widget_Slot[InSlotTarget].IsValid())
	{
		return Widget_Slot[InSlotTarget]->GetItem();
	}

	return nullptr;
}

bool SBuildSystem_ToolBar::IsSelectedSlot(const int32 InSlotTarget) const
{
	return (CurrentMode.Get() == EBuildHelperMode::Building && SelectedSlot.Get() == InSlotTarget);
}

FText SBuildSystem_ToolBar::GetCurrentActionTip(const ETipBindType InAction) const
{
	switch (InAction)
	{
		case ETipBindType::ActionPlace: {
			switch (CurrentMode.Get())
			{
				case EBuildHelperMode::None: return FText::GetEmpty();
				case EBuildHelperMode::Building: return NSLOCTEXT("SBuildSystem", "SBuildSystem.ToolBar.ActionPlace.Building", "Place object: ");
				case EBuildHelperMode::Selecting: return NSLOCTEXT("SBuildSystem", "SBuildSystem.ToolBar.ActionPlace.Selecting", "Remove selected object: ");
			}
		}
		case ETipBindType::ActionCancel: {
			switch (CurrentMode.Get())
			{
				case EBuildHelperMode::None: return FText::GetEmpty();
				case EBuildHelperMode::Building: return NSLOCTEXT("SBuildSystem", "SBuildSystem.ToolBar.ActionCancel.Building", "Toggle around rotation: ");
				case EBuildHelperMode::Selecting: return NSLOCTEXT("SBuildSystem", "SBuildSystem.ToolBar.ActionCancel.Selecting", "Take selected object: ");
			}
		}
		case ETipBindType::ActionRotate: {
			switch (CurrentMode.Get())
			{
				case EBuildHelperMode::None: return FText::GetEmpty();
				case EBuildHelperMode::Building: return NSLOCTEXT("SBuildSystem", "SBuildSystem.ToolBar.ActionRotate.Building", "Rotate object around snap: ");
				case EBuildHelperMode::Selecting: return FText::GetEmpty();
			}
		}
		case ETipBindType::ActionInventory: return NSLOCTEXT("SBuildSystem", "SBuildSystem.ToolBar.ActionInventory", "Open/Close inventory: ");
		case ETipBindType::ActionMode: {
			switch (CurrentMode.Get())
			{
				case EBuildHelperMode::None: return NSLOCTEXT("SBuildSystem", "SBuildSystem.ToolBar.ActionMode.None", "Switch to building mode: ");
				case EBuildHelperMode::Building: return NSLOCTEXT("SBuildSystem", "SBuildSystem.ToolBar.ActionMode.Building", "Switch to selecting  mode: ");
				case EBuildHelperMode::Selecting: return NSLOCTEXT("SBuildSystem", "SBuildSystem.ToolBar.ActionMode.Selecting", "Switch to none mode: ");
			}
		}
	}

	return FText::GetEmpty();
}
