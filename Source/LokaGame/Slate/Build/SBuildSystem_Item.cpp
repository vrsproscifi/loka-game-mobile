// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SBuildSystem_Item.h"
#include "SlateOptMacros.h"

#include "SLokaToolTip.h"
#include "LokaDragDrop.h"
#include "Build/ItemBuildEntity.h"
#include "PlayerInventoryItem.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SBuildSystem_Item::Construct(const FArguments& InArgs, UPlayerInventoryItem* InSource)
{
	checkf(InSource != nullptr, TEXT("SBuildSystem_Item::Construct UItemBuildEntity is null"));

	Style = InArgs._Style;
	OnDragStart = InArgs._OnDragStart;
	Source = InSource;

	ChildSlot
	[
		SNew(SBox).WidthOverride(128).HeightOverride(128).Visibility(EVisibility::HitTestInvisible)
		[
			SNew(SOverlay)
			+ SOverlay::Slot()
			[
				SNew(SImage).Image(this, &SBuildSystem_Item::GetItemImage)
			]
			+ SOverlay::Slot().HAlign(HAlign_Right).VAlign(VAlign_Bottom)
			[
				SNew(STextBlock)
				.AutoWrapText(true)
				.Justification(ETextJustify::Right)
				.TextStyle(&Style->CountFont)
				.Text_Lambda([&, IsTool = InArgs._IsToolBarItem]() {
					if (Source.IsValid() && Source->GetEntity<UItemBuildEntity>())
					{
						return IsTool ?
							FText::FromString(Source->GetEntity<UItemBuildEntity>()->GetDescription().Name.ToString() + "\r\nx" + FString::FromInt(Source->GetCount())) :
							FText::FromString("x" + FString::FromInt(Source->GetCount()));
					}

					return FText::GetEmpty();
				})
			]
		]
	];

	//SetToolTip(SNew(SLokaToolTip).Text_Lambda([&]() {
	//	return Source.IsValid() ? Source->GetDescription().Name : FText::GetEmpty();
	//}));

	EmptyResource = new FSlateNoResource();
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

const FSlateBrush* SBuildSystem_Item::GetItemImage() const
{
	return (Source.IsValid() && Source->GetEntity()) ? Source->GetEntity()->GetImage() : EmptyResource;
}

FReply SBuildSystem_Item::OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
	if (MouseEvent.GetEffectingButton() == EKeys::LeftMouseButton)
	{
		return FReply::Handled().DetectDrag(AsShared(), EKeys::LeftMouseButton);
	}

	return SCompoundWidget::OnMouseButtonDown(MyGeometry, MouseEvent);
}

FReply SBuildSystem_Item::OnDragDetected(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
	if (MouseEvent.IsMouseButtonDown(EKeys::LeftMouseButton))
	{
		TSharedRef<FLokaDragDropOp> Operation = MakeShareable(new FLokaDragDropOp(AsShared(), AsShared(), Source.Get()));

		Operation->SetDecoratorVisibility(true);
		Operation->Construct();

		OnDragStart.ExecuteIfBound();
		return FReply::Handled().BeginDragDrop(Operation);
	}
	else
	{
		return FReply::Unhandled();
	}
}