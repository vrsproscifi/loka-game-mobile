// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "Windows/SWindowBase.h"
#include "SBuildSystem_Item.h"
#include "Styles/BuildSystemWidgetStyle.h"


class UItemBuildEntity;
class UPlayerInventoryItem;
/**
 * 
 */
class LOKAGAME_API SBuildSystem_Inventory : public SWindowBase
{
public:
	SLATE_BEGIN_ARGS(SBuildSystem_Inventory)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FBuildSystemStyle>("SBuildSystemStyle"))
	{
		_Visibility = EVisibility::Hidden;
	}
	SLATE_STYLE_ARGUMENT(FBuildSystemStyle, Style)
	SLATE_EVENT(FOnBuildAction, OnClickedBuyItem)
	SLATE_EVENT(FOnBuildAction, OnClickedSellItem)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, TSharedRef<SWindowsContainer> InOwnerRef);
	void OnFillList(const TArray<UPlayerInventoryItem*>& InList);

protected:

	void OnSortChanged(ECheckBoxState InState, const FName InCategory);
	void OnSubSortChanged(ECheckBoxState InState, const FName InCategory);
	ECheckBoxState GetIsChecked(const FName InCategory) const;
	ECheckBoxState GetIsCheckedSub(const FName InCategory) const;

	FSlateFontInfo GetSortFont(const FName InCategory) const;
	FSlateFontInfo GetSubSortFont(const FName InCategory) const;

	FSlateColor GetSortFontColor(const FName InCategory) const;
	FSlateColor GetSubSortFontColor(const FName InCategory) const;

	const FBuildSystemStyle* Style;

	FOnBuildAction OnClickedBuyItem;
	FOnBuildAction OnClickedSellItem;

	TSharedPtr<SWrapBox> Widget_WrapBox;
	TSharedPtr<SHorizontalBox> Widget_HorizontalBox[2];
	TSharedPtr<SVerticalBox> Widget_VerticalBox[2];
	TSharedPtr<SVerticalBox> Widget_CatContainer;
	TMap<FName, TSharedPtr<SVerticalBox>> SubCatContainersMap;

	FName CurrentCategory, CurrentSubCategory;
	TArray<FName> AddedCategorys, AddedSubCategorys;
	TMap<FName, TArray<FName>> AddedSubCategorysMap;
};
