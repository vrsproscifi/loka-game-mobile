// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/BuildSystemWidgetStyle.h"

class UItemBuildEntity;
class UPlayerInventoryItem;

DECLARE_DELEGATE_OneParam(FOnBuildAction, UPlayerInventoryItem*);

class LOKAGAME_API SBuildSystem_Item : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SBuildSystem_Item)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FBuildSystemStyle>("SBuildSystemStyle"))
		, _IsToolBarItem(false)
	{}
	SLATE_STYLE_ARGUMENT(FBuildSystemStyle, Style)
	SLATE_EVENT(FOnClickedOutside, OnDragStart)
	SLATE_ARGUMENT(bool, IsToolBarItem)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, UPlayerInventoryItem* InSource);
	virtual FReply OnDragDetected(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;

protected:

	const FBuildSystemStyle* Style;

	TWeakObjectPtr<UPlayerInventoryItem> Source;
	FOnClickedOutside OnDragStart;
	const FSlateBrush* EmptyResource;

	const FSlateBrush* GetItemImage() const;
	virtual FReply OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;	
};
