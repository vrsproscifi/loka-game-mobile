// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/WeaponModularManagerWidgetStyle.h"

class UItemModuleEntity;

class LOKAGAME_API SWeaponModularModule : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SWeaponModularModule)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FWeaponModularManagerStyle>(TEXT("SWeaponModularManagerStyle")))
	{}
	SLATE_STYLE_ARGUMENT(FWeaponModularManagerStyle, Style)
	SLATE_ATTRIBUTE(const UItemModuleEntity*, Instance)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

protected:

	const FWeaponModularManagerStyle* Style;
	TAttribute<const UItemModuleEntity*> Instance;

	FText GetFractionName() const;
	FText GetFractionRank() const;
};
