// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "SWeaponModularModule.h"
#include "Module/ItemModuleEntity.h"

class UItemWeaponEntity;
class AWeaponModular;

DECLARE_DELEGATE_RetVal_OneParam(FVector2D, FOnGetSocketScreen, const FName&);
DECLARE_DELEGATE_OneParam(FOnModuleAction, UItemModuleEntity*);

class LOKAGAME_API SWeaponModularManager : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SWeaponModularManager)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FWeaponModularManagerStyle>(TEXT("SWeaponModularManagerStyle")))
	{}
	SLATE_STYLE_ARGUMENT(FWeaponModularManagerStyle, Style)
	SLATE_EVENT(FOnGetSocketScreen, OnGetSocketScreen)
	SLATE_EVENT(FOnModuleAction, OnSelectedModule)	
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void SetInstance(const UItemWeaponEntity*);

	FOnGetSocketScreen OnGetSocketScreen;
	FOnModuleAction OnSelectedModule;

	FLinearColor GetContentOpacity() const;
	AWeaponModular* ModularActor;

protected:

	const FWeaponModularManagerStyle* Style;

	const UItemWeaponEntity* Instance;	

	TSharedPtr<SCanvas> Layers;	
	TMap<FName, FVector2D> LayersPosition;
	TMap<FName, FVector2D> TargetPosition;
	TMap<FName, SCanvas::FSlot*> CachedSlots;
	TMap<FName, TSharedPtr<SListView<UItemModuleEntity*>>> Lists;

	FCurveSequence AnimInstance;
	FCurveHandle AnimHandle;

	void GenerateSocketModules(const FName&);

	FVector2D GetCurrentPositionFor(const FName) const;
	TSharedRef<ITableRow> OnGenerateModuleRow(UItemModuleEntity*, const TSharedRef<STableViewBase>&);
	void OnSelectionModuleRow(UItemModuleEntity*, ESelectInfo::Type);

	virtual int32 OnPaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyClippingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const override;
	virtual void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;
};
