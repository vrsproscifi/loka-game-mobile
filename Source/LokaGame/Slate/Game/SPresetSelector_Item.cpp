// VRSPRO

#include "LokaGame.h"
#include "SPresetSelector_Item.h"
#include "SlateOptMacros.h"
#include "Layout/SScaleBox.h"
#include "Models/LobbyClientPreSetData.h"
#include "InventoryComponent.h"
#include "PlayerInventoryItem.h"
#include "Item/ItemBaseEntity.h"
#include "Character/CharacterBaseEntity.h"
#include "CharacterAbility.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SPresetSelector_Item::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	ProfileData = InArgs._ProfileData;
	InventoryComponent = InArgs._InventoryComponent;
	IsSelected = InArgs._IsSelected;
	EmptyBrush = FSlateNoResource();

	ChildSlot
	[
		SNew(SBorder)
		.BorderImage(this, &SPresetSelector_Item::GetBackgroundImage)
		.Padding(0)
		[
			SNew(SBorder)
			.BorderImage(this, &SPresetSelector_Item::GetBorderImage)
			.Padding(FMargin(8))
			[
				SNew(SOverlay)
				+ SOverlay::Slot()
				[
					SNew(SVerticalBox)
					+ SVerticalBox::Slot().AutoHeight()
					[
						SNew(SHorizontalBox)
						+ SHorizontalBox::Slot()
						[
							SNew(STextBlock)
							.Text(this, &SPresetSelector_Item::GetTextName)
							.Font(Style->Item.TextName)
							.ColorAndOpacity_Lambda([&]() { return IsSelected.Get() ? Style->Item.TextNameSelected : Style->Item.TextNameUnselected; })
							.Justification(ETextJustify::Center)
						]
						+ SHorizontalBox::Slot().AutoWidth()
						[
							SNew(STextBlock)
							.Text(this, &SPresetSelector_Item::GetTextCharacter)
							.Font(Style->Item.TextName)
							.ColorAndOpacity_Lambda([&]() { return IsSelected.Get() ? Style->Item.TextNameSelected : Style->Item.TextNameUnselected; })
							.Justification(ETextJustify::Center)
						]
					]
					+ SVerticalBox::Slot().FillHeight(1)
					[
						SNew(SWidgetSwitcher)
						.WidgetIndex(this, &SPresetSelector_Item::GetWidgetIndexIsEnabled)
						+ SWidgetSwitcher::Slot()
						[
							SNew(SScaleBox)
							.HAlign(HAlign_Center)
							.VAlign(VAlign_Center)
							.Stretch(EStretch::ScaleToFit)
							[
								SNew(SImage).Image(&Style->Item.LockedImage)
							]
						]
						+ SWidgetSwitcher::Slot()
						[
							SNew(SHorizontalBox)
							+ SHorizontalBox::Slot().FillWidth(1)
							[
								SNew(SScaleBox)
								.HAlign(HAlign_Center)
								.VAlign(VAlign_Center)
								.Stretch(EStretch::ScaleToFit)
								[
									SNew(SImage).Image(this, &SPresetSelector_Item::GetBrushCharacter)
								]
							]
							+ SHorizontalBox::Slot().FillWidth(1)
							[
								SNew(SScaleBox)
								.HAlign(HAlign_Center)
								.VAlign(VAlign_Center)
								.Stretch(EStretch::ScaleToFit)
								[
									SNew(SImage).Image(this, &SPresetSelector_Item::GetBrushPrimaryWeapon)
								]
							]
						]
					]
				]
				+ SOverlay::Slot().HAlign(HAlign_Right).VAlign(VAlign_Bottom)
				[
					SNew(SImage).Image(this, &SPresetSelector_Item::GetBrushCharacterAbility)
				]
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

const FSlateBrush* SPresetSelector_Item::GetBackgroundImage() const
{
	return IsSelected.Get() ? &Style->Item.BackgroundSelected : &Style->Item.Background;
}

const FSlateBrush* SPresetSelector_Item::GetBorderImage() const
{
	return IsSelected.Get() ? &Style->Item.BorderSelected : &Style->Item.Border;
}

const FSlateBrush* SPresetSelector_Item::GetBrushPrimaryWeapon() const
{
	if (ProfileData.IsSet() && ProfileData.Get() && InventoryComponent.IsSet() && InventoryComponent.Get() && ProfileData.Get()->Items.Num())
	{
		auto localCount = 0;
		for (auto localItem : ProfileData.Get()->Items)
		{
			if (localItem.AsSlotId() == EItemSetSlotId::PrimaryWeapon)
			{
				break;
			}

			++localCount;
		}

		if (ProfileData.Get()->Items.IsValidIndex(localCount))
		{
			if (auto localFoundItem = InventoryComponent.Get()->FindItem(ProfileData.Get()->Items[localCount].ItemId))
			{
				if (auto localFoundInstance = localFoundItem->GetEntity())
				{
					return localFoundInstance->GetImage();
				}
			}
		}
	}

	return &EmptyBrush;
}

const FSlateBrush* SPresetSelector_Item::GetBrushCharacter() const
{
	if (ProfileData.IsSet() && ProfileData.Get() && InventoryComponent.IsSet() && InventoryComponent.Get())
	{
		if (auto localFoundItem = InventoryComponent.Get()->FindItem(ProfileData.Get()->CharacterId))
		{
			if (auto localFoundInstance = localFoundItem->GetEntity())
			{
				return localFoundInstance->GetImage();
			}
		}
	}

	return &EmptyBrush;
}

const FSlateBrush* SPresetSelector_Item::GetBrushCharacterAbility() const
{
	if (ProfileData.IsSet() && ProfileData.Get() && InventoryComponent.IsSet() && InventoryComponent.Get())
	{
		if (auto localFoundItem = InventoryComponent.Get()->FindItem(ProfileData.Get()->CharacterId))
		{
			auto localFoundInstance = localFoundItem->GetEntity<UCharacterBaseEntity>();
			if (localFoundInstance && localFoundInstance->CharacterAbility)
			{
				return localFoundInstance->CharacterAbility.GetDefaultObject()->GetIcon();
			}
		}
	}

	return &EmptyBrush;
}

int32 SPresetSelector_Item::GetWidgetIndexIsEnabled() const
{
	if (ProfileData.IsSet() && ProfileData.Get())
	{
		return ProfileData.Get()->IsEnabled;
	}

	return 0;
}

FText SPresetSelector_Item::GetTextName() const
{
	if (ProfileData.IsSet() && ProfileData.Get())
	{
		return FText::FromString(ProfileData.Get()->Name);
	}

	return FText::GetEmpty();
}

FText SPresetSelector_Item::GetTextCharacter() const
{
	if (ProfileData.IsSet() && ProfileData.Get() && InventoryComponent.IsSet() && InventoryComponent.Get())
	{
		if (auto localFoundItem = InventoryComponent.Get()->FindItem(ProfileData.Get()->CharacterId))
		{
			if (auto localFoundInstance = localFoundItem->GetEntity())
			{
				return localFoundInstance->GetItemName();
			}
		}
	}

	return FText::GetEmpty();
}
