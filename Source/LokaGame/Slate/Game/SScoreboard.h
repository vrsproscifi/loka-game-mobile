// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SlateBasics.h"
#include "Styles/ScoreboardWidgetStyle.h"

#include "Slate/LokaStyle.h"

#define LOCTEXT_NAMESPACE "SScoreboard"

#define SCOREBOARD_LASTROW_INDEX 255

class AUTPlayerState;
class AUTGameState;


class LOKAGAME_API SScoreboard : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SScoreboard)
		: _ScoreboardStyle(&FLokaStyle::Get().GetWidgetStyle<FScoreboardStyle>("SScoreboardStyle"))
		, _ColorDuration(0.4f)
		, _TransDuration(0.6f)
		, _MaxRowsPerTeam(12)
		, _LeftHeadText(FText(LOCTEXT("NonSetLeftHeadText", "YOU TEAM")))
		, _RightHeadText(FText(LOCTEXT("NonSetRightHeadText", "ENEMY TEAM")))
	{}
	SLATE_ARGUMENT(float, ColorDuration)
	SLATE_ARGUMENT(float, TransDuration)
	SLATE_ARGUMENT(uint8, MaxRowsPerTeam)
	SLATE_ARGUMENT(FText, LeftHeadText)
	SLATE_ARGUMENT(FText, RightHeadText)
	SLATE_STYLE_ARGUMENT(FScoreboardStyle, ScoreboardStyle)
	SLATE_ATTRIBUTE(AUTGameState*, GameState)
	SLATE_ATTRIBUTE(AUTPlayerState*, LocalPlayerState)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

	void AddRow(AUTPlayerState*);
	bool RemoveRow(AUTPlayerState*);

	void ToggleTable(const bool);
	EActiveTimerReturnType Update(double InCurrentTime, float InDeltaTime);

	void AppendKillRow(const AUTPlayerState* Killer, const AUTPlayerState* Target, const FSlateBrush *WeaponIcon);

	TSharedPtr<class SGameTimer> GameTimer;

protected:

	const struct FScoreboardStyle *ScoreboardStyle;

	FCurveSequence ShowAnim;
	FCurveHandle ShowColorHandle;
	FCurveHandle ShowTransHandle;

	uint8 MaxRowsPerTeam;

	FLinearColor WidgetColor() const;
	FSlateColor WidgetBackgroundColor() const;
	TOptional<FSlateRenderTransform> WidgetLeftOffset() const;
	TOptional<FSlateRenderTransform> WidgetRightOffset() const;
	FText GetTotalScore(const bool) const;

	void onRemoveKillRow(const TSharedRef<SWidget> &_Widget);

	TAttribute<AUTGameState*> GameState;
	TAttribute<AUTPlayerState*> LocalPlayerState;
	int32 CachedNumPlayers;

	TSharedPtr<SVerticalBox> LeftTeamList;
	TSharedPtr<SVerticalBox> RightTeamList;

	TSharedPtr<SBorder> LeftBorder;
	TSharedPtr<SBorder> RightBorder;

	TSharedPtr<STextBlock> LeftTotalScore;
	TSharedPtr<STextBlock> RightTotalScore;

	TArray<TSharedPtr<class SScoreboardRow>> LeftTeamRows;
	TArray<TSharedPtr<class SScoreboardRow>> RightTeamRows;

	SVerticalBox::FSlot* LeftLocalRow, *RightLocalRow;

	TSharedPtr<SScrollBox> KillsBox;
};


#undef LOCTEXT_NAMESPACE