// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SGameTimer.h"
#include "PvE/PvESurviveGameState.h"
#include "UTPlayerState.h"
#include "UTTeamInfo.h"
#include "ToggableWidgetHelper.h"

#include "SAnimatedBackground.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SGameTimer::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	GameState = InArgs._GameState;
	LocalPlayerState = InArgs._LocalPlayerState;

	ASSIGN_UTF8_SYMBOL(sLife, 0xf183);

	ChildSlot
	[
		SAssignNew(Widget_FxBackground, SAnimatedBackground)
		.ShowAnimation(EAnimBackAnimation::UpFade)
		.HideAnimation(EAnimBackAnimation::UpFade)
		.InitAsHide(true)
		.Duration(1.0f)
		[
			SNew(SOverlay)
			+ SOverlay::Slot()
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot()
				.VAlign(VAlign_Center)
				.Padding(FMargin(0, 32))
				[
					SNew(SHorizontalBox)
					.Visibility(this, &SGameTimer::GetVisibility_Scores)
					+ SHorizontalBox::Slot().FillWidth(1)
					+ SHorizontalBox::Slot().FillWidth(.8f)
					[
						SNew(SOverlay)
						+ SOverlay::Slot()
						[
							SNew(SProgressBar)
							.BarFillType(EProgressBarFillType::RightToLeft)
							.Style(&Style->ScoreProgress)
							.FillColorAndOpacity(FColor::Green)
							.Percent(this, &SGameTimer::GetScorePercent, false)
						]
						+ SOverlay::Slot()
						[
							SNew(SBorder)
							.Padding(0)
							.BorderImage(&Style->ScoreBorder)
							.HAlign(HAlign_Left)
							.VAlign(VAlign_Center)
							[
								SAssignNew(LeftScore, STextBlock)
								.TextStyle(&Style->ScoreFont)
								.Margin(FMargin(40, 0))
							]
						]
						+ SOverlay::Slot()
						[
							SNew(SBorder)
							.Padding(0)
							.BorderImage(&Style->ScoreEdge)
						]
					]
					+ SHorizontalBox::Slot().AutoWidth()[SNew(SBox).WidthOverride(100)]
					+ SHorizontalBox::Slot().FillWidth(.8f)
					[
						SNew(SOverlay)
						+ SOverlay::Slot()
						[
							SNew(SProgressBar)
							.BarFillType(EProgressBarFillType::LeftToRight)
							.Style(&Style->ScoreProgress)
							.FillColorAndOpacity(FColor::Red)
							.Percent(this, &SGameTimer::GetScorePercent, true)
						]
						+ SOverlay::Slot()
						[
							SNew(SBorder)
							.Padding(0)
							.BorderImage(&Style->ScoreBorder)
							.HAlign(HAlign_Right)
							.VAlign(VAlign_Center)
							[
								SAssignNew(RightScore, STextBlock)
								.TextStyle(&Style->ScoreFont)
								.Margin(FMargin(40, 0))
							]
						]
						+ SOverlay::Slot()
						[
							SNew(SBorder)
							.Padding(0)
							.BorderImage(&Style->ScoreEdge)
						]
					]
					+ SHorizontalBox::Slot().FillWidth(1)
				]
				+ SVerticalBox::Slot()
				.HAlign(HAlign_Center)
				.VAlign(VAlign_Center)
				.Padding(FMargin(0, 0))
				[
					SNew(SBox).WidthOverride(132).RenderTransform(FSlateRenderTransform(FVector2D(0.0f, -(50.0f + 30.0f))))
					[
						SNew(SOverlay)
						+ SOverlay::Slot()
						[
							SNew(SProgressBar)
							.BarFillType(EProgressBarFillType::FillFromCenter)
							.Style(&Style->TimeProgress)
							.FillColorAndOpacity(FColor::White)
							.Percent(this, &SGameTimer::GetTimePercent)
						]
						+ SOverlay::Slot()
						[
							SNew(SBorder)
							.Padding(0)
							.BorderImage(&Style->TimeBorder)
							.HAlign(HAlign_Center)
							.VAlign(VAlign_Center)
							[
								SNew(STextBlock)
								.TextStyle(&Style->TimeFont)
								.Margin(FMargin(20, 0))
								.Text(this, &SGameTimer::GetTimeText)
							]
						]
					]
				]
			]
			+ SOverlay::Slot().VAlign(VAlign_Bottom).HAlign(HAlign_Center).Padding(FMargin(0, 46))
			[
				SNew(SHorizontalBox)
				.Visibility(this, &SGameTimer::GetVisibility_Lifes)
				+ SHorizontalBox::Slot().AutoWidth()
				[
					SNew(STextBlock)
					.Text(FText::FromString(sLife))
					.TextStyle(&Style->SymbolFont)
					.Justification(ETextJustify::Center)
					.Margin(FMargin(4, 0))
					.ColorAndOpacity(this, &SGameTimer::GetLifeColor, 1)
				]
				+ SHorizontalBox::Slot().AutoWidth()
				[
					SNew(STextBlock)
					.Text(FText::FromString(sLife))
					.TextStyle(&Style->SymbolFont)
					.Justification(ETextJustify::Center)
					.Margin(FMargin(4, 0))
					.ColorAndOpacity(this, &SGameTimer::GetLifeColor, 2)
				]
				+ SHorizontalBox::Slot().AutoWidth()
				[
					SNew(STextBlock)
					.Text(FText::FromString(sLife))
					.TextStyle(&Style->SymbolFont)
					.Justification(ETextJustify::Center)
					.Margin(FMargin(4, 0))
					.ColorAndOpacity(this, &SGameTimer::GetLifeColor, 3)
				]
			]
			+ SOverlay::Slot().VAlign(VAlign_Bottom).HAlign(HAlign_Center).Padding(FMargin(0, 46))
			[
				SNew(SHorizontalBox)
				.Visibility(this, &SGameTimer::GetVisibility_NumberLifes)
				+ SHorizontalBox::Slot().AutoWidth()
				[
					SNew(STextBlock)
					.Text(FText::FromString(sLife))
					.TextStyle(&Style->SymbolFont)
					.Justification(ETextJustify::Center)
					.Margin(FMargin(4, 0))
					.ColorAndOpacity(this, &SGameTimer::GetNumberLifesColor)
				]
				+ SHorizontalBox::Slot().AutoWidth()
				[
					SNew(STextBlock)
					.Text(this, &SGameTimer::GetNumberLifesText)
					.TextStyle(&Style->LastHeroText)
					.Justification(ETextJustify::Center)
					.Margin(FMargin(4, 0))
					.ColorAndOpacity(FColor::White)
				]
			]
			+ SOverlay::Slot().VAlign(VAlign_Center).HAlign(HAlign_Center)
			[
				SNew(SHorizontalBox)
				.Visibility(this, &SGameTimer::GetVisibility_LasHero)
				+ SHorizontalBox::Slot().AutoWidth().HAlign(HAlign_Center)
				[
					SNew(SBox).WidthOverride(220)
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Center)
						[
							SNew(STextBlock).Text(NSLOCTEXT("SGameTimer", "SGameTimer.YourPlace", "Your Place")).TextStyle(&Style->LastHeroText)
						]
						+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Center)
						[
							SNew(STextBlock).Text_Lambda([&]() { return FText::AsNumber(CachedPlace); }).TextStyle(&Style->LastHeroNumber)
						]
					]
				]
				+ SHorizontalBox::Slot().AutoWidth()[SNew(SBox).WidthOverride(240)]
				+ SHorizontalBox::Slot().AutoWidth().HAlign(HAlign_Center)
				[
					SNew(SBox).WidthOverride(220)
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Center)
						[
							SNew(STextBlock).Text(NSLOCTEXT("SGameTimer", "SGameTimer.AlivePlayers", "Alive")).TextStyle(&Style->LastHeroText)
						]
						+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Center)
						[
							SNew(STextBlock).Text_Lambda([&]() { return FText::AsNumber(CachedAlive); }).TextStyle(&Style->LastHeroNumber)
						]
					]
				]
			]
			+ SOverlay::Slot().VAlign(VAlign_Center).HAlign(HAlign_Center)
			[
				SNew(SHorizontalBox)
				.Visibility(this, &SGameTimer::GetVisibility_Waves)
				+ SHorizontalBox::Slot().AutoWidth()[SNew(SBox).WidthOverride(220)]
				+ SHorizontalBox::Slot().AutoWidth().HAlign(HAlign_Center)
				[
					SNew(SBox).WidthOverride(220)
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Center)
						[
							SNew(STextBlock).Text(NSLOCTEXT("SGameTimer", "SGameTimer.YourScore", "Your Score")).TextStyle(&Style->LastHeroText)
						]
						+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Center)
						[
							SNew(STextBlock).Text_Lambda([&]() { return FText::AsNumber(LocalPlayerState.Get() ? FMath::CeilToInt(LocalPlayerState.Get()->GetScore()) : 0); }).TextStyle(&Style->LastHeroNumber)
						]
					]
				]
				+ SHorizontalBox::Slot().AutoWidth()[SNew(SBox).WidthOverride(240)]
				+ SHorizontalBox::Slot().AutoWidth().HAlign(HAlign_Center)
				[
					SNew(SBox).WidthOverride(220)
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Center)
						[
							SNew(STextBlock).Text(NSLOCTEXT("SGameTimer", "SGameTimer.Wave", "Wave")).TextStyle(&Style->LastHeroText)
						]
						+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Center)
						[
							SNew(STextBlock).Text_Lambda([&]() { 
								if (auto MyGameState = Cast<APvESurviveGameState>(GameState.Get()))
								{
									return FText::Format(FText::FromString("{0}/{1}"), FText::AsNumber(MyGameState->CurrentRound), FText::AsNumber(MyGameState->NumberOfRounds));
								}
								return FText::GetEmpty();
							}).TextStyle(&Style->LastHeroNumber)
						]
					]
				]
				+ SHorizontalBox::Slot().AutoWidth().HAlign(HAlign_Center)
				[
					SNew(SBox).WidthOverride(220)
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Center)
						[
							SNew(STextBlock).Text(NSLOCTEXT("SGameTimer", "SGameTimer.Enemies", "Enemies")).TextStyle(&Style->LastHeroText)
						]
						+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Center)
						[
							SNew(STextBlock).Text_Lambda([&]() {
									return FText::AsNumber(GameState.Get() ? GameState.Get()->GetTeamScore(1) : 0);
							}).TextStyle(&Style->LastHeroNumber)
						]
					]
				]
			]
		]
	];

	RegisterActiveTimer(1.0f, FWidgetActiveTimerDelegate::CreateSP(this, &SGameTimer::UpdateLastHeroData));	
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

EActiveTimerReturnType SGameTimer::UpdateLastHeroData(double InCurrentTime, float InDeltaTime)
{
	if (GameState.Get() && LocalPlayerState.Get())
	{
		if (FFlagsHelper::HasAnyFlags(GameState.Get()->GetGameModeType().Value, EGameMode::LostDeadMatch.ToFlag()) == false)
		{
			return EActiveTimerReturnType::Stop;
		}

		CachedAlive = 0;
		CachedPlace = 0;

		TMultiMap<int32, AUTPlayerState*> SortedMap;
		for (int32 i = 0; i < GameState.Get()->PlayerArray.Num(); ++i)
		{
			int32 Score = 0;
			AUTPlayerState* CurPlayerState = Cast<AUTPlayerState>(GameState.Get()->PlayerArray[i]);
			if (CurPlayerState && CurPlayerState->bIsDemoRecording == false)
			{
				SortedMap.Add(FMath::TruncToInt(CurPlayerState->Kills), CurPlayerState);
			}
		}

		SortedMap.KeySort(TGreater<int32>());

		SIZE_T _count = 1;
		for (auto &t : SortedMap)
		{
			if (auto p = Cast<AUTPlayerState>(t.Value))
			{
				if (p->Deaths < GameState.Get()->GoalScore)
				{
					++CachedAlive;
				}

				if (p == LocalPlayerState.Get())
				{
					CachedPlace = _count;
				}

				++_count;
			}
		}
	}

	return EActiveTimerReturnType::Continue;
}

void SGameTimer::ToggleWidget(const bool _Toggle)
{
	Widget_FxBackground->ToggleWidget(_Toggle);
}

FText SGameTimer::GetTimeText() const
{
	if (GameState.Get())
	{
		if(GameState.Get()->GetRemainingTime() % 2 == 0)
			return FText::FromString(FTimespan::FromSeconds(GameState.Get()->GetRemainingTime()).ToString(TEXT("%m:%s")));
		else
			return FText::FromString(FTimespan::FromSeconds(GameState.Get()->GetRemainingTime()).ToString(TEXT("%m %s")));
	}

	return FText::FromString("00:00");
}

TOptional<float> SGameTimer::GetScorePercent(const bool IsRed) const
{
	if (GameState.Get() && LocalPlayerState.Get() && GameState.Get()->Teams.Num() > 1)
	{
		if (!IsRed)
		{			
			return float(GameState.Get()->Teams[LocalPlayerState.Get()->GetTeamNum()]->Score) / float(GameState.Get()->GoalScore);
		}

		return float(GameState.Get()->Teams[!LocalPlayerState.Get()->GetTeamNum()]->Score) / float(GameState.Get()->GoalScore);
	}

	return .0f;
}

TOptional<float> SGameTimer::GetTimePercent() const
{
	if (GameState.Get())
	{
		return float(GameState.Get()->GetRemainingTime()) / float(GameState.Get()->TimeLimit);
	}

	return .0f;
}

FSlateColor SGameTimer::GetLifeColor(const int32 LifeNum) const
{
	if (GameState.Get() && LocalPlayerState.Get())
	{
		if (GameState.Get()->GetGameModeType() == EGameMode::LostDeadMatch)
		{
			return LocalPlayerState.Get()->Deaths < LifeNum ? FColor::Green.ReinterpretAsLinear() : FColor::Red.ReinterpretAsLinear();
		}
	}

	return FColor::Transparent.ReinterpretAsLinear();
}

EVisibility SGameTimer::GetVisibility_Scores() const
{
	return GameState.Get() ? (FFlagsHelper::HasAnyFlags(GameState.Get()->GetGameModeType().Value, EGameMode::LostDeadMatch.ToFlag() | EGameMode::DuelMatch.ToFlag() | EGameMode::PVE_Waves.ToFlag()) ? EVisibility::Hidden : EVisibility::SelfHitTestInvisible) : EVisibility::SelfHitTestInvisible;
}

EVisibility SGameTimer::GetVisibility_Lifes() const
{
	return GameState.Get() ? (GameState.Get()->GetGameModeType() == EGameMode::LostDeadMatch ? EVisibility::SelfHitTestInvisible : EVisibility::Hidden) : EVisibility::Hidden;
}

EVisibility SGameTimer::GetVisibility_NumberLifes() const
{
	return GameState.Get() ? (FFlagsHelper::HasAnyFlags(GameState.Get()->GetGameModeType().Value, EGameMode::DuelMatch.ToFlag() | EGameMode::PVE_Waves.ToFlag()) ? EVisibility::SelfHitTestInvisible : EVisibility::Hidden) : EVisibility::Hidden;
}

EVisibility SGameTimer::GetVisibility_LasHero() const
{
	return GameState.Get() ? (GameState.Get()->GetGameModeType() == EGameMode::LostDeadMatch ? EVisibility::SelfHitTestInvisible : EVisibility::Hidden) : EVisibility::Hidden;
}

EVisibility SGameTimer::GetVisibility_Waves() const
{
	return GameState.Get() ? (GameState.Get()->GetGameModeType() == EGameMode::PVE_Waves ? EVisibility::SelfHitTestInvisible : EVisibility::Hidden) : EVisibility::Hidden;
}

FSlateColor SGameTimer::GetNumberLifesColor() const
{
	if (GameState.Get() && LocalPlayerState.Get())
	{
		if (GameState.Get()->GetGameModeType() == EGameMode::PVE_Waves)
		{
			return FMath::Lerp(FColor::Green.ReinterpretAsLinear(), FColor::Red.ReinterpretAsLinear(), float(LocalPlayerState.Get()->Deaths) / float(GameState.Get()->GetTeamScore(0) - 1));
		}

		return FMath::Lerp(FColor::Green.ReinterpretAsLinear(), FColor::Red.ReinterpretAsLinear(), float(LocalPlayerState.Get()->Deaths) / float(GameState.Get()->GoalScore - 1));
	}

	return FColor::White.ReinterpretAsLinear();
}

FText SGameTimer::GetNumberLifesText() const
{
	if (GameState.Get() && LocalPlayerState.Get())
	{
		if (GameState.Get()->GetGameModeType() == EGameMode::PVE_Waves)
		{
			return FText::Format(FText::FromString("x{0}"), FText::AsNumber(GameState.Get()->GetTeamScore(0)));
		}

		return FText::Format(FText::FromString("x{0}"), FText::AsNumber(GameState.Get()->GoalScore - LocalPlayerState.Get()->Deaths));
	}

	return FText::Format(FText::FromString("x{0}"), FText::AsNumber(0));
}