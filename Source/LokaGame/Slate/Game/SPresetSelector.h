// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "SPresetSelector_Item.h"

class AUTGameState;
class UInventoryComponent;
class UProfileComponent;

class LOKAGAME_API SPresetSelector : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SPresetSelector)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FPresetSelectorStyle>("SPresetSelectorStyle"))
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FPresetSelectorStyle, Style)
	SLATE_ATTRIBUTE(const UProfileComponent*, ProfileComponent)
	SLATE_ATTRIBUTE(const UInventoryComponent*, InventoryComponent)
	SLATE_ATTRIBUTE(int32, Selected)
	SLATE_ATTRIBUTE(const AUTGameState*, GameState)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);
	void ApplyPresets(const TArray<FPresetDisplayData>&);
	void ClearPresets();

	void ToggleWidget(const bool);

protected:

	const FPresetSelectorStyle* Style;

	int32 CachedSelected;
	TAttribute<int32> Selected;
	TAttribute<const AUTGameState*> GameState;
	TAttribute<const UProfileComponent*> ProfileComponent;
	TAttribute<const UInventoryComponent*> InventoryComponent;

	TSharedPtr<SHorizontalBox> BoxContainer;
	TSharedPtr<class SAnimatedBackground> AnimatedBackground[4];

	bool GetIsSelected(const int32) const;
	virtual void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;
};
