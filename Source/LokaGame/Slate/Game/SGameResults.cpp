// VRSPRO

#include "LokaGame.h"
#include "SGameResults.h"

#include "UTGameState.h"
#include "UTPlayerState.h"

#include "Layout/SScaleBox.h"
#include "Decorators/LokaTextDecorator.h"
#include "Decorators/LokaKeyDecorator.h"
#include "SWindowBackground.h"

#include "Styles/LoadingScreenWidgetStyle.h"

#define LOCTEXT_NAMESPACE "SGameResults"

struct FTargetColumnName
{
	const static FName Name;
	const static FName Kills;
	const static FName Deaths;
	const static FName Score;
	const static FName Money;
	const static FName Exp;
	const static FName Reputation;
};

const FName FTargetColumnName::Name = FName("Name");
const FName FTargetColumnName::Kills = FName("Kills");
const FName FTargetColumnName::Deaths = FName("Deaths");
const FName FTargetColumnName::Score = FName("Score");
const FName FTargetColumnName::Money = FName("Money");
const FName FTargetColumnName::Exp = FName("Exp");
const FName FTargetColumnName::Reputation = FName("Reputation");

class LOKAGAME_API SGRAchievement : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SGRAchievement)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FGameResultsStyle>("SGameResultsStyle"))
		, _Text()
		, _Index(0)
		, _Amount(0)
		, _Custom(new FSlateNoResource())
		, _IsCustom(false)
	{}
	SLATE_STYLE_ARGUMENT(FGameResultsStyle, Style)
	SLATE_ARGUMENT(int32, Index)
	SLATE_ARGUMENT(FText, Text)
	SLATE_ARGUMENT(int32, Amount)
	SLATE_ARGUMENT(const FSlateBrush*, Custom)
	SLATE_ARGUMENT(bool, IsCustom)
	SLATE_END_ARGS()

	BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
	void Construct(const FArguments& InArgs)
	{
		Style = InArgs._Style;
		CurrentAmount = 0;
		TargetAmount = InArgs._Amount;
		Name = InArgs._Text;

		const FSlateBrush* Image = InArgs._Custom;

		if (!InArgs._IsCustom)
		{
			if (!Style->AchievementsArray.IsValidIndex(InArgs._Index))
			{
				return;
			}

			Image = &Style->AchievementsArray[InArgs._Index].Icon;
		}

		if (TargetAmount != 0)
		{
			RegisterActiveTimer(0.01f, FWidgetActiveTimerDelegate::CreateSP(this, &SGRAchievement::ValueAnimate));
		}		

		ChildSlot
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Center)
			[
				SNew(SScaleBox)
				.Stretch(EStretch::ScaleToFit)
				[
					SNew(SImage)
					.Image(Image)
				]
			]
			+ SVerticalBox::Slot()
			.AutoHeight()
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Center)
			[
				SAssignNew(ValueContainer, STextBlock)
				.TextStyle(&Style->AchievementsArray[InArgs._Index].TextStyle)
				.Text(Name)
			]
		];
	}
	END_SLATE_FUNCTION_BUILD_OPTIMIZATION

protected:

	FText Name;
	const FGameResultsStyle *Style;
	float CurrentAmount, TargetAmount;
	TSharedPtr<STextBlock> ValueContainer;

	EActiveTimerReturnType ValueAnimate(double InCurrentTime, float InDeltaTime)
	{
		CurrentAmount = FMath::FInterpConstantTo(CurrentAmount, TargetAmount, InDeltaTime, TargetAmount > 0 ? TargetAmount * 0.5f : TargetAmount * -0.5f);

		if (FMath::IsNearlyEqual(CurrentAmount, TargetAmount) == false)
		{
			if (TargetAmount < 0)
			{
				ValueContainer->SetText(FText::AsNumber(FPlatformMath::FloorToInt(CurrentAmount), &FNumberFormattingOptions::DefaultNoGrouping()));
			}
			else
			{
				ValueContainer->SetText(FText::Format(FText::FromString("{1} +{0}"), FText::AsNumber(FPlatformMath::CeilToInt(CurrentAmount), &FNumberFormattingOptions::DefaultNoGrouping()), Name));
			}

			return EActiveTimerReturnType::Continue;
		}

		return EActiveTimerReturnType::Stop;
	}
};

class LOKAGAME_API SGRPlayerRow : public SMultiColumnTableRow<AUTPlayerState*>
{
public:
	SLATE_BEGIN_ARGS(SGRPlayerRow)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FGameResultsStyle>("SGameResultsStyle"))
		, _StyleTeam(EStyleTeam::Neutral)
	{}
		SLATE_STYLE_ARGUMENT(FGameResultsStyle, Style)
		SLATE_ARGUMENT(TWeakObjectPtr<AUTPlayerState>, Source)
		SLATE_ARGUMENT(EStyleTeam, StyleTeam)
		SLATE_ARGUMENT(bool, IsLocal)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, const TSharedRef<STableViewBase>& InOwnerTableView)
	{
		Source = InArgs._Source;
		Style = InArgs._Style;
		StyleTeam = InArgs._StyleTeam;
		IsLocal = InArgs._IsLocal;

		FSuperRowType::Construct(FSuperRowType::FArguments()
			.ShowSelection(true)
			.Padding(1)
			.Style(&Style->TableRow)
			, InOwnerTableView);
	}

	virtual TSharedRef<SWidget> GenerateWidgetForColumn(const FName& ColumnName) override
	{
		return SNew(STextBlock).TextStyle(IsLocal ? &Style->TeamTextStyleLocal : &Style->TeamTextStyle[StyleTeam]).Text(this, &SGRPlayerRow::GetValueFrom, ColumnName).Justification(ETextJustify::Left).Margin(FMargin(4, 1));
	}

protected:
	const FGameResultsStyle *Style;
	TWeakObjectPtr<AUTPlayerState> Source;
	EStyleTeam StyleTeam;
	bool IsLocal;

	struct FCachedInformation
	{
		FString Name;
		int32 Kills;
		int32 Deaths;
		int32 Score;
		int32 Money;
		int32 PremiumMoney;
		int32 Experience;
		int32 PremiumExperience;
		int32 Reputation;
		int32 PremiumReputation;

		void From(TWeakObjectPtr<AUTPlayerState> &InFrom)
		{
			if (InFrom.IsValid())
			{
				Name = InFrom->GetPlayerName();
				Kills = InFrom->GetKills();
				Deaths = InFrom->GetDeaths();
				Score = FMath::CeilToInt(InFrom->GetScore());

				Money = InFrom->TakeCash(EGameCurrency::Money)->Amount;
				PremiumMoney = InFrom->TakePremiumCash(EGameCurrency::Money)->Amount;

				Experience = InFrom->Experience;
				PremiumExperience = InFrom->PremiumExperience;
				Reputation = InFrom->Reputation;
				PremiumReputation = InFrom->PremiumReputation;
			}
		}
	} CachedInformation;

	FText GetValueFrom(const FName ColumnName) const
	{
		if (FTargetColumnName::Name == ColumnName)
		{
			return FText::FromString(CachedInformation.Name);
		}
		else if (FTargetColumnName::Kills == ColumnName)
		{
			return FText::AsNumber(CachedInformation.Kills);
		}
		else if (FTargetColumnName::Deaths == ColumnName)
		{
			return FText::AsNumber(CachedInformation.Deaths);
		}
		else if (FTargetColumnName::Score == ColumnName)
		{
			return FText::AsNumber(CachedInformation.Score);
		}
		else if (FTargetColumnName::Money == ColumnName)
		{
			if (CachedInformation.PremiumMoney > 0)
			{
				return FText::Format(FText::FromString("{0} + {1}"), FText::AsNumber(CachedInformation.Money), FText::AsNumber(CachedInformation.PremiumMoney));
			}

			return FText::AsNumber(CachedInformation.Money);
		}
		else if (FTargetColumnName::Exp == ColumnName)
		{
			if (CachedInformation.PremiumExperience > 0)
			{
				return FText::Format(FText::FromString("{0} + {1}"), FText::AsNumber(CachedInformation.Experience), FText::AsNumber(CachedInformation.PremiumExperience));
			}

			return FText::AsNumber(CachedInformation.Experience);
		}
		else if (FTargetColumnName::Reputation == ColumnName)
		{
			if (CachedInformation.PremiumReputation > 0)
			{
				return FText::Format(FText::FromString("{0} + {1}"), FText::AsNumber(CachedInformation.Reputation), FText::AsNumber(CachedInformation.PremiumReputation));
			}

			return FText::AsNumber(CachedInformation.Reputation);
		}

		return FText::GetEmpty();
	}	

	virtual void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override
	{
		FSuperRowType::Tick(AllottedGeometry, InCurrentTime, InDeltaTime);
		CachedInformation.From(Source);
	}
};

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SGameResults::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	GameState = InArgs._GameState;
	PlayerState = InArgs._PlayerState;

	TipsText = FLokaStyle::Get().GetWidgetStyle<FLoadingScreenStyle>("SLoadingScreenStyle").Tips;
	CurrentTip = 0;

	FSlateColor TeamStatusColor;
	FText TeamStatusText;
	TSharedPtr<SImage> StatusImage;

	if (InArgs._MatchWinner == PlayerState->GetTeamNum())
	{
		TeamStatusColor = Style->TeamStatus.Color[EStyleTeam::Friendly];
		StatusImage = SNew(SImage).Image(&Style->TeamStatus.Image[EStyleTeam::Friendly]);
		TeamStatusText = LOCTEXT("Win", "THE VICTORY");
		FSlateApplication::Get().PlaySound(Style->TeamStatus.Sound[EStyleTeam::Friendly]);
	}
	else if (InArgs._MatchWinner != PlayerState->GetTeamNum() && InArgs._MatchWinner != INDEX_NONE)
	{
		TeamStatusColor = Style->TeamStatus.Color[EStyleTeam::Enemy];
		StatusImage = SNew(SImage).Image(&Style->TeamStatus.Image[EStyleTeam::Enemy]);
		TeamStatusText = LOCTEXT("Lose", "THE DEFEAT");
		FSlateApplication::Get().PlaySound(Style->TeamStatus.Sound[EStyleTeam::Enemy]);
	}
	else
	{
		TeamStatusColor = Style->TeamStatus.Color[EStyleTeam::Neutral];
		StatusImage = SNew(SImage).Image(&Style->TeamStatus.Image[EStyleTeam::Neutral]);
		TeamStatusText = LOCTEXT("Draw", "DRAW");
		FSlateApplication::Get().PlaySound(Style->TeamStatus.Sound[EStyleTeam::Neutral]);
	}		

	auto TeamListFriendly = new TArray<AUTPlayerState*>();
	auto TeamListEnemy = new TArray<AUTPlayerState*>();
	auto TeamListNeutral = new TArray<AUTPlayerState*>();
	const auto ModeFlag = GameState->GetGameModeType().Value;
	const bool IsUseNeutral = FFlagsHelper::HasAnyFlags(ModeFlag, EGameMode::LostDeadMatch.ToFlag() | EGameMode::DuelMatch.ToFlag() | EGameMode::PVE_Waves.ToFlag());

	if (IsUseNeutral)
	{
		for (auto ps : GameState->PlayerArray)
		{
			auto OtherPlayerState = Cast<AUTPlayerState>(ps);
			if (OtherPlayerState && OtherPlayerState->bIsDemoRecording == false)
			{
				TeamListNeutral->Add(OtherPlayerState);
			}
		}
	}
	else
	{
		for (auto ps : GameState->PlayerArray)
		{
			auto OtherPlayerState = Cast<AUTPlayerState>(ps);
			if (OtherPlayerState && OtherPlayerState->bIsDemoRecording == false)
			{
				if (OtherPlayerState->GetTeamNum() == PlayerState->GetTeamNum())
				{
					TeamListFriendly->Add(OtherPlayerState);
				}
				else
				{
					TeamListEnemy->Add(OtherPlayerState);
				}				
			}
		}
	}

	TeamListFriendly->Sort([](const AUTPlayerState& A, const AUTPlayerState& B) { return A.Score > B.Score; });
	TeamListEnemy->Sort([](const AUTPlayerState& A, const AUTPlayerState& B) { return A.Score > B.Score; });
	TeamListNeutral->Sort([](const AUTPlayerState& A, const AUTPlayerState& B) { return A.Score > B.Score; });

	auto WidgetTeamListFriendly = SNew(SListView<AUTPlayerState*>)
		.ListItemsSource(TeamListFriendly)
		.HeaderRow
		(
			SNew(SHeaderRow).Style(&Style->TableHeader)
			+ SHeaderRow::Column(FTargetColumnName::Name).FillWidth(Style->ColumnsSize.Name)[SNew(STextBlock).TextStyle(&Style->TeamStyle.TextStyle).Justification(ETextJustify::Left).Text(LOCTEXT("Name", "NAME"))]
			+ SHeaderRow::Column(FTargetColumnName::Kills).FillWidth(Style->ColumnsSize.Kills)[SNew(STextBlock).TextStyle(&Style->TeamStyle.TextStyle).Justification(ETextJustify::Left).Text(LOCTEXT("Kills", "KILLS"))]
			+ SHeaderRow::Column(FTargetColumnName::Deaths).FillWidth(Style->ColumnsSize.Deaths)[SNew(STextBlock).TextStyle(&Style->TeamStyle.TextStyle).Justification(ETextJustify::Left).Text(LOCTEXT("Deaths", "DEATHS"))]
			+ SHeaderRow::Column(FTargetColumnName::Score).FillWidth(Style->ColumnsSize.Score)[SNew(STextBlock).TextStyle(&Style->TeamStyle.TextStyle).Justification(ETextJustify::Left).Text(LOCTEXT("Score", "SCORE"))]
			+ SHeaderRow::Column(FTargetColumnName::Money).FillWidth(Style->ColumnsSize.PlusMoney)[SNew(STextBlock).TextStyle(&Style->TeamStyle.TextStyle).Justification(ETextJustify::Left).Text(LOCTEXT("Money", "MONEY"))]
			+ SHeaderRow::Column(FTargetColumnName::Exp).FillWidth(Style->ColumnsSize.PlusExp)[SNew(STextBlock).TextStyle(&Style->TeamStyle.TextStyle).Justification(ETextJustify::Left).Text(LOCTEXT("Exp", "EXPERIENCE"))]
			+ SHeaderRow::Column(FTargetColumnName::Reputation).FillWidth(Style->ColumnsSize.PlusExp)[SNew(STextBlock).TextStyle(&Style->TeamStyle.TextStyle).Justification(ETextJustify::Left).Text(LOCTEXT("Reputation", "REPUTATION"))]
		)
		.OnGenerateRow_Lambda([&](AUTPlayerState* InItem, const TSharedRef<STableViewBase>& OwnerTable)
		{
			return SNew(SGRPlayerRow, OwnerTable).Source(InItem).StyleTeam(EStyleTeam::Friendly).IsLocal(PlayerState == InItem);
		});

	auto WidgetTeamListEnemy = SNew(SListView<AUTPlayerState*>)
		.ListItemsSource(TeamListEnemy)
		.HeaderRow
		(
			SNew(SHeaderRow).Style(&Style->TableHeader)
			+ SHeaderRow::Column(FTargetColumnName::Name).FillWidth(Style->ColumnsSize.Name)[SNew(STextBlock).TextStyle(&Style->TeamStyle.TextStyle).Justification(ETextJustify::Left).Text(LOCTEXT("Name", "NAME"))]
			+ SHeaderRow::Column(FTargetColumnName::Kills).FillWidth(Style->ColumnsSize.Kills)[SNew(STextBlock).TextStyle(&Style->TeamStyle.TextStyle).Justification(ETextJustify::Left).Text(LOCTEXT("Kills", "KILLS"))]
			+ SHeaderRow::Column(FTargetColumnName::Deaths).FillWidth(Style->ColumnsSize.Deaths)[SNew(STextBlock).TextStyle(&Style->TeamStyle.TextStyle).Justification(ETextJustify::Left).Text(LOCTEXT("Deaths", "DEATHS"))]
			+ SHeaderRow::Column(FTargetColumnName::Score).FillWidth(Style->ColumnsSize.Score)[SNew(STextBlock).TextStyle(&Style->TeamStyle.TextStyle).Justification(ETextJustify::Left).Text(LOCTEXT("Score", "SCORE"))]
			+ SHeaderRow::Column(FTargetColumnName::Money).FillWidth(Style->ColumnsSize.PlusMoney)[SNew(STextBlock).TextStyle(&Style->TeamStyle.TextStyle).Justification(ETextJustify::Left).Text(LOCTEXT("Money", "MONEY"))]
			+ SHeaderRow::Column(FTargetColumnName::Exp).FillWidth(Style->ColumnsSize.PlusExp)[SNew(STextBlock).TextStyle(&Style->TeamStyle.TextStyle).Justification(ETextJustify::Left).Text(LOCTEXT("Exp", "EXPERIENCE"))]
			+ SHeaderRow::Column(FTargetColumnName::Reputation).FillWidth(Style->ColumnsSize.PlusExp)[SNew(STextBlock).TextStyle(&Style->TeamStyle.TextStyle).Justification(ETextJustify::Left).Text(LOCTEXT("Reputation", "REPUTATION"))]
		)
		.OnGenerateRow_Lambda([&](AUTPlayerState* InItem, const TSharedRef<STableViewBase>& OwnerTable)
		{
			return SNew(SGRPlayerRow, OwnerTable).Source(InItem).StyleTeam(EStyleTeam::Enemy).IsLocal(PlayerState == InItem);
		});

	auto WidgetTeamListNeutral = SNew(SListView<AUTPlayerState*>)
		.ListItemsSource(TeamListNeutral)
		.HeaderRow
		(
			SNew(SHeaderRow).Style(&Style->TableHeader)
			+ SHeaderRow::Column(FTargetColumnName::Name).FillWidth(Style->ColumnsSize.Name)[SNew(STextBlock).TextStyle(&Style->TeamStyle.TextStyle).Justification(ETextJustify::Left).Text(LOCTEXT("Name", "NAME"))]
			+ SHeaderRow::Column(FTargetColumnName::Kills).FillWidth(Style->ColumnsSize.Kills)[SNew(STextBlock).TextStyle(&Style->TeamStyle.TextStyle).Justification(ETextJustify::Left).Text(LOCTEXT("Kills", "KILLS"))]
			+ SHeaderRow::Column(FTargetColumnName::Deaths).FillWidth(Style->ColumnsSize.Deaths)[SNew(STextBlock).TextStyle(&Style->TeamStyle.TextStyle).Justification(ETextJustify::Left).Text(LOCTEXT("Deaths", "DEATHS"))]
			+ SHeaderRow::Column(FTargetColumnName::Score).FillWidth(Style->ColumnsSize.Score)[SNew(STextBlock).TextStyle(&Style->TeamStyle.TextStyle).Justification(ETextJustify::Left).Text(LOCTEXT("Score", "SCORE"))]
			+ SHeaderRow::Column(FTargetColumnName::Money).FillWidth(Style->ColumnsSize.PlusMoney)[SNew(STextBlock).TextStyle(&Style->TeamStyle.TextStyle).Justification(ETextJustify::Left).Text(LOCTEXT("Money", "MONEY"))]
			+ SHeaderRow::Column(FTargetColumnName::Exp).FillWidth(Style->ColumnsSize.PlusExp)[SNew(STextBlock).TextStyle(&Style->TeamStyle.TextStyle).Justification(ETextJustify::Left).Text(LOCTEXT("Exp", "EXPERIENCE"))]
			+ SHeaderRow::Column(FTargetColumnName::Reputation).FillWidth(Style->ColumnsSize.PlusExp)[SNew(STextBlock).TextStyle(&Style->TeamStyle.TextStyle).Justification(ETextJustify::Left).Text(LOCTEXT("Reputation", "REPUTATION"))]
		)
		.OnGenerateRow_Lambda([&](AUTPlayerState* InItem, const TSharedRef<STableViewBase>& OwnerTable) {
			return SNew(SGRPlayerRow, OwnerTable).Source(InItem).StyleTeam(EStyleTeam::Neutral).IsLocal(PlayerState == InItem);
		});

	MAKE_UTF8_SYMBOL(sListLeft, 0xf053);
	MAKE_UTF8_SYMBOL(sListRight, 0xf054);

	ChildSlot
	[
		SNew(SVerticalBox)
		//=============================================================================[ LOCAL PLAYER TEAM STATUS
		+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(0, 0, Style->TeamStatus.HeightAndRightOffset.Y, 0))
		[
			SNew(SBox)
			.HeightOverride(Style->TeamStatus.HeightAndRightOffset.X)
			[
				SNew(SHorizontalBox)					
				+ SHorizontalBox::Slot().FillWidth(1)
				[
					SNew(SBorder)
					.BorderImage(&Style->TeamStatus.Background)
					.BorderBackgroundColor(TeamStatusColor)
					[
						SNew(SHorizontalBox)
						+ SHorizontalBox::Slot()
						+ SHorizontalBox::Slot().HAlign(HAlign_Right).VAlign(VAlign_Center).AutoWidth()
						[
							StatusImage.ToSharedRef()
						]
						+ SHorizontalBox::Slot().HAlign(HAlign_Center).VAlign(VAlign_Center).AutoWidth()
						[
							SNew(STextBlock)
							.TextStyle(&Style->TeamStatus.TextStyle)
							.Text(TeamStatusText)
						]
						+ SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Center).AutoWidth()
						[
							SNew(SBorder)
							.Padding(0)
							.BorderBackgroundColor(FLinearColor::Transparent)
							.RenderTransformPivot(FVector2D(0.5f, 0.5f))
							.RenderTransform(FSlateRenderTransform(FScale2D(-1.0f, 1.0f)))
							[
								StatusImage.ToSharedRef()
							]
						]
						+ SHorizontalBox::Slot()
					]
				]
				+ SHorizontalBox::Slot().AutoWidth()
				[
					SNew(SBox)
					.WidthOverride(Style->TeamStatus.Background2Width)
					[
						SNew(SBorder)
						.BorderImage(&Style->TeamStatus.Background2)
						.BorderBackgroundColor(TeamStatusColor)
					]
				]
			]
		]
		+ SVerticalBox::Slot().FillHeight(0.1f)
		//=============================================================================[ Achievements
		+ SVerticalBox::Slot().AutoHeight().Padding(Style->Achievements.Padding)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight()
			[
				SNew(SBorder)
				.BorderImage(&Style->Achievements.Line)
				.Padding(Style->Achievements.LinePadding)
				[
					SNew(STextBlock)
					.Text(LOCTEXT("Achievements", "Achievements"))
					.TextStyle(&Style->Achievements.TextStyle)
				]
			]
			+ SVerticalBox::Slot().AutoHeight()
			[
				SNew(SBox)
				.HeightOverride(Style->Achievements.BoxHeight)
				[
					SNew(SBorder)
					.BorderImage(&Style->Achievements.Background)
					.Padding(FMargin(0, 10))
					[
						SAssignNew(AchievementsBox, SScrollBox)
						.Orientation(EOrientation::Orient_Horizontal)
						.ScrollBarVisibility(EVisibility::Collapsed)
					]
				]
			]
		]
		+ SVerticalBox::Slot().FillHeight(0.1f)
		//=============================================================================[ Players Lists
		+ SVerticalBox::Slot().FillHeight(1)
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot().Padding(Style->TeamStyle.Padding)
				[
					SNew(SBorder)
					.BorderImage(&Style->TeamStyle.Background)
					.BorderBackgroundColor(Style->TeamColor[EStyleTeam::Friendly])
					.Padding(0)
					.Visibility(IsUseNeutral ? EVisibility::Collapsed : EVisibility::Visible)
					[
						SNew(SBorder)
						.BorderImage(&Style->TeamStyle.UpLine)
						.BorderBackgroundColor(Style->TeamColor[EStyleTeam::Friendly])
						.VAlign(EVerticalAlignment::VAlign_Fill)
						.Padding(FMargin(0, Style->TeamStyleTextPadding.Top, 0, Style->TeamStyleTextPadding.Bottom))
						[
							WidgetTeamListFriendly
						]
					]
				]
				+ SVerticalBox::Slot().Padding(Style->TeamStyle.Padding)
				[
					SNew(SBorder)
					.BorderImage(&Style->TeamStyle.Background)
					.BorderBackgroundColor(Style->TeamColor[EStyleTeam::Enemy])
					.RenderTransformPivot(FVector2D(0.5f, 0.5f))
					.RenderTransform(FSlateRenderTransform(FScale2D(-1.0f, 1.0f)))
					.Padding(0)
					.Visibility(IsUseNeutral ? EVisibility::Collapsed : EVisibility::Visible)
					[
						SNew(SBorder)
						.RenderTransformPivot(FVector2D(0.5f, 0.5f))
						.RenderTransform(FSlateRenderTransform(FScale2D(-1.0f, 1.0f)))
						.BorderImage(&Style->TeamStyle.UpLine)
						.BorderBackgroundColor(Style->TeamColor[EStyleTeam::Enemy])
						.VAlign(EVerticalAlignment::VAlign_Fill)
						.Padding(FMargin(0, Style->TeamStyleTextPadding.Top, 0, Style->TeamStyleTextPadding.Bottom))
						[
							WidgetTeamListEnemy
						]
					]
				]
				+ SVerticalBox::Slot().Padding(Style->TeamStyle.Padding)
				[
					SNew(SBorder)
					.BorderImage(&Style->TeamStyle.Background)
					.BorderBackgroundColor(Style->TeamColor[EStyleTeam::Neutral])
					.Padding(0)
					.Visibility(IsUseNeutral ? EVisibility::Visible : EVisibility::Collapsed)
					[
						SNew(SBorder)
						.BorderImage(&Style->TeamStyle.UpLine)
						.BorderBackgroundColor(Style->TeamColor[EStyleTeam::Neutral])
						.VAlign(EVerticalAlignment::VAlign_Fill)
						.Padding(FMargin(0, Style->TeamStyleTextPadding.Top, 0, Style->TeamStyleTextPadding.Bottom))
						[
							WidgetTeamListNeutral
						]
					]
				]
			]
			+ SHorizontalBox::Slot().FillWidth(.4f).Padding(Style->TeamStyle.Padding)
			[
				SNew(SWindowBackground)
				.Header(NSLOCTEXT("WindowsHeader", "SGameResults.Tips", "TIPS"))
				.Padding(4)
				[
					SNew(SVerticalBox)
					+ SVerticalBox::Slot()
					[
						SAssignNew(Widget_TipsText, SRichTextBlock)
						.AutoWrapText(true)
						.TextStyle(&Style->Tips.TextStyle)
						.DecoratorStyleSet(&FLokaStyle::Get())
						+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(TMap<FName, FSlateFontInfo>(), Style->Tips.TextStyle.Font, Style->Tips.TextStyle.ColorAndOpacity.GetSpecifiedColor()))
						+ SRichTextBlock::Decorator(FLokaKeyDecorator::Create())
					]
					+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Right).Padding(4)
					[
						SNew(SHorizontalBox)
						+ SHorizontalBox::Slot()
						[
							SNew(SButton)
							.Text(FText::FromString(sListLeft))
							.TextStyle(&Style->Tips.Awesome)
							.ButtonStyle(&Style->Tips.LeftButton)
							.HAlign(HAlign_Center)
							.VAlign(VAlign_Center)
							.ContentPadding(FMargin(4, 2))
							.OnClicked_Lambda([&]() {
								--CurrentTip;
								if (CurrentTip < 0) CurrentTip = TipsText.Num() - 1;
								Widget_TipsText->SetText(TipsText[CurrentTip]);
								return FReply::Handled();
							})
						]
						+ SHorizontalBox::Slot()
						[
							SNew(SButton)
							.Text(FText::FromString(sListRight))
							.TextStyle(&Style->Tips.Awesome)
							.ButtonStyle(&Style->Tips.RightButton)
							.HAlign(HAlign_Center)
							.VAlign(VAlign_Center)
							.ContentPadding(FMargin(4, 2))
							.OnClicked_Lambda([&]() {
								++CurrentTip;
								if (CurrentTip >= TipsText.Num()) CurrentTip = 0;
								Widget_TipsText->SetText(TipsText[CurrentTip]);
								return FReply::Handled();
							})
						]
					]
				]
			]
		]
	];

	SGeneralBackground::Construct(SGeneralBackground::FArguments(InArgs._BackgroundArguments)
		.Padding(Style->ContentPadding)
		.Header(NSLOCTEXT("WindowsHeader", "SGameResults", "GAME RESULTS"))
		.Content()
		[
			ChildSlot.GetWidget()
		]
	);

	CurrentTip = FMath::RandRange(0, TipsText.Num() - 1);
	Widget_TipsText->SetText(TipsText[CurrentTip]);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SGameResults::AddAchievement(const int32 &_index, const FText &_text)
{
	if (Style->AchievementsArray.IsValidIndex(_index))
	{
		AchievementsBox->AddSlot()
			.Padding(FMargin(10, 0))
			.AttachWidget(SNew(SGRAchievement).Index(_index).Text(_text));
	}
}

void SGameResults::AddAchievement(const FString &_tag, const FText &_text)
{
	int32 _index = 0;

	for (auto it : Style->AchievementsArray)
	{
		if (it.Tag.Equals(_tag))
		{
			AddAchievement(_index, _text);
			break;
		}

		++_index;
	}
}

void SGameResults::AddAchievement(const int32 &_index, const int32 &_amount)
{
	if (Style->AchievementsArray.IsValidIndex(_index))
	{
		AchievementsBox->AddSlot()
			.Padding(FMargin(10, 0))
			.AttachWidget(SNew(SBox).WidthOverride(Style->Achievements.BoxHeight)[SNew(SGRAchievement).Index(_index).Amount(_amount)]);
	}
}

void SGameResults::AddAchievement(const FString &_tag, const int32 &_amount)
{
	int32 _index = 0;

	for (auto it : Style->AchievementsArray)
	{
		if (it.Tag.Equals(_tag))
		{
			AddAchievement(_index, _amount);
			break;
		}

		++_index;
	}
}

void SGameResults::AddAchievementCustom(const FSlateBrush *Icon, const FText &Text)
{
	AchievementsBox->AddSlot()
		.Padding(FMargin(10, 0))
		.AttachWidget(SNew(SBox).WidthOverride(Style->Achievements.BoxHeight)[SNew(SGRAchievement).IsCustom(true).Custom(Icon).Text(Text)]);
}

#undef LOCTEXT_NAMESPACE