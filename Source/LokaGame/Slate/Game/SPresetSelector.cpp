// VRSPRO

#include "LokaGame.h"
#include "SPresetSelector.h"
#include "SlateOptMacros.h"

#include "SAnimatedBackground.h"
#include "UTGameState.h"
#include "UTDmgType_AttachParticles.h"
#include "ProfileComponent.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SPresetSelector::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	Selected = InArgs._Selected;
	GameState = InArgs._GameState;
	ProfileComponent = InArgs._ProfileComponent;
	InventoryComponent = InArgs._InventoryComponent;

	ChildSlot.HAlign(HAlign_Fill).VAlign(VAlign_Fill)
	[
		SNew(SVerticalBox)
		+ SVerticalBox::Slot().AutoHeight().VAlign(VAlign_Top)
		[
			SAssignNew(AnimatedBackground[0], SAnimatedBackground)
			.ShowAnimation(EAnimBackAnimation::UpFade)
			.HideAnimation(EAnimBackAnimation::UpFade)
			.Duration(1.5f)
			.InitAsHide(true)
			.IsRelative(true)
			.WithColor(false)
			.Ease(ECurveEaseFunction::QuadIn)
			[
				SNew(SBorder)
				.BorderImage(&Style->Background)
				.Padding(0)
				[
					SNew(SBox).HeightOverride(193).VAlign(VAlign_Center).Padding(FMargin(280, 0))
					[
						SAssignNew(AnimatedBackground[2], SAnimatedBackground)
						.ShowAnimation(EAnimBackAnimation::Color)
						.HideAnimation(EAnimBackAnimation::Color)
						.Duration(2.0f)
						.InitAsHide(true)
						.IsRelative(true)
						.Ease(ECurveEaseFunction::QuadOut)
						[
							SNew(STextBlock)
							.Justification(ETextJustify::Center)
							.TextStyle(&Style->TipModeText)
							.AutoWrapText(true)
							.Text_Lambda([&]() {
								if (GameState.IsSet() && GameState.Get())
								{
									return FText::Format
									(
										FText::FromString("{0} - {1}"),
										GameState.Get()->GetGameModeType().GetDisplayName(), 
										GameState.Get()->GetGameModeType().GetDescription()
									);
								}

								return FText::GetEmpty();
							})
						]
					]
				]
			]
		]
		+ SVerticalBox::Slot()
		+ SVerticalBox::Slot().AutoHeight().VAlign(VAlign_Bottom)
		[				
			SAssignNew(AnimatedBackground[1], SAnimatedBackground)
			.ShowAnimation(EAnimBackAnimation::DownFade)
			.HideAnimation(EAnimBackAnimation::DownFade)
			.Duration(1.5f)
			.InitAsHide(true)
			.IsRelative(true)
			.WithColor(false)
			.Ease(ECurveEaseFunction::QuadIn)
			[
				SNew(SBorder)
				.BorderImage(&Style->Background)
				[
					SAssignNew(AnimatedBackground[3], SAnimatedBackground)
					.ShowAnimation(EAnimBackAnimation::Color)
					.HideAnimation(EAnimBackAnimation::Color)
					.Duration(2.0f)
					.InitAsHide(true)
					.IsRelative(true)
					.Ease(ECurveEaseFunction::QuadOut)
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(10, 20)).HAlign(HAlign_Center).VAlign(VAlign_Center)
						[
							SNew(STextBlock).Text(NSLOCTEXT("SPresetSelector", "SPresetSelector.Tip", "Use mouse scroll or numeric keyboard buttons for select profile."))
							.TextStyle(&Style->TipText)
						]
						+ SVerticalBox::Slot().AutoHeight()
						[
							SNew(SBox).HeightOverride(128)
							[
								SAssignNew(BoxContainer, SHorizontalBox)
							]
						]
					]
				]
			]
		]
	];	

	for (SSIZE_T i = 0; i < EPlayerPreSetId::End; ++i)
	{
		BoxContainer->AddSlot().Padding(FMargin(4, 6))
			[
				SNew(SPresetSelector_Item)
				.IsSelected(this, &SPresetSelector::GetIsSelected, static_cast<int32>(i))
				.ProfileData_Lambda([&, n = i]()
				{
					const FClientPreSetData* ProfileData = nullptr;

					if (ProfileComponent.Get())
					{
						return ProfileComponent.Get()->GetProfileDataPtr(static_cast<EPlayerPreSetId::Type>(n));
					}

					return ProfileData;
				})
				.InventoryComponent(InventoryComponent)
			];
	}

	CachedSelected = -1;
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SPresetSelector::ApplyPresets(const TArray<FPresetDisplayData>& Data)
{
//	for (SSIZE_T i = 0; i < Data.Num(); ++i)
//	{
//		BoxContainer->AddSlot().Padding(FMargin(4, 6))
//			[
//				SNew(SPresetSelector_Item, Data[i])
//				.IsSelected(this, &SPresetSelector::GetIsSelected, static_cast<int32>(i))
//			];
//	}
}

void SPresetSelector::ClearPresets()
{
	BoxContainer->ClearChildren();
}

void SPresetSelector::ToggleWidget(const bool Toggle)
{
	AnimatedBackground[0]->ToggleWidget(Toggle);
	AnimatedBackground[1]->ToggleWidget(Toggle);

	AnimatedBackground[2]->ToggleWidget(Toggle);
	AnimatedBackground[3]->ToggleWidget(Toggle);
}

bool SPresetSelector::GetIsSelected(const int32 PresetId) const
{
	return (Selected.Get() == PresetId);
}

void SPresetSelector::Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime)
{
	if (CachedSelected != Selected.Get())
	{
		CachedSelected = Selected.Get();
		FSlateApplication::Get().PlaySound(Style->SwitchSound);
	}

	SCompoundWidget::Tick(AllottedGeometry, InCurrentTime, InDeltaTime);
}