// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SGameHUD_Live.h"
#include "ToggableWidgetHelper.h"
// Experimental
#include "UTCharacter.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SGameHUD_Live::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	Character = InArgs._Character;

	//.Offset(FMargin(10, -10, 600, 200))
	ChildSlot.HAlign(HAlign_Left).VAlign(VAlign_Bottom).Padding(FMargin(10, 0, 0, 10))
	[
		SNew(SBox)
		.WidthOverride(600)
		.HeightOverride(200)
		.RenderTransform(Style->LiveBarTransform.ToSlateRenderTransform())
		.RenderTransformPivot(Style->LiveBarTransformPivot)
		[
			SNew(SBorder)
			.BorderBackgroundColor(FLinearColor::Transparent)
			.ColorAndOpacity(this, &SGameHUD_Live::WidgetColor)
			.Padding(0)
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot()
				.FillHeight(1)
				[
					SNew(SBorder)
					.Padding(0)
					.BorderImage(&Style->HealthBackground)
					[
						SNew(SHorizontalBox)
						+ SHorizontalBox::Slot()
						.AutoWidth()
						[
							SNew(SSpacer)
							.Size(Style->HealthSpacer)
						]
						+ SHorizontalBox::Slot()
						.AutoWidth()
						.HAlign(EHorizontalAlignment::HAlign_Center)
						.VAlign(EVerticalAlignment::VAlign_Center)
						[
							SNew(SImage)
							.Image(&Style->IconHealth)
						]
						+ SHorizontalBox::Slot()
						.FillWidth(1)
						[
							SNew(SOverlay)
							+ SOverlay::Slot()
							.HAlign(EHorizontalAlignment::HAlign_Left)
							.VAlign(EVerticalAlignment::VAlign_Top)
							.Padding(FMargin(10, 20, 0, 0))
							[
								SAssignNew(HealthText, STextBlock)
								.TextStyle(&Style->PercentText)
								.Text(this, &SGameHUD_Live::GetBarValueAsText, true)
							]
							+ SOverlay::Slot()
							.HAlign(EHorizontalAlignment::HAlign_Fill)
							.VAlign(EVerticalAlignment::VAlign_Bottom)
							.Padding(FMargin(0, 0, 0, 16))
							[
								SNew(SBox)
								.HeightOverride(24)
								[
									SAssignNew(HealthProgress, SProgressBar)
									.RenderTransform(FVector2D(-15, 0))
									.BackgroundImage(&Style->ProgressBackground)
									.FillImage(&Style->ProgressForeground)
									.BorderPadding(FVector2D(0, 0))
									.FillColorAndOpacity(this, &SGameHUD_Live::HealthColor)
									.Percent(this, &SGameHUD_Live::GetBarPercent, true)
								]
							]
						]
					]
				]
				+ SVerticalBox::Slot()
				.FillHeight(1)
				[
					SNew(SBorder)
					.Padding(0)
					.BorderImage(&Style->ArmorBackground)
					.RenderTransform(FVector2D(Style->SecondBlockTranslation.X, Style->SecondBlockTranslation.Y))
					[
						SNew(SHorizontalBox)
						+ SHorizontalBox::Slot()
						.AutoWidth()
						[
							SNew(SSpacer)
							.Size(Style->ArmorSpacer)
						]
						+ SHorizontalBox::Slot()
						.AutoWidth()
						.HAlign(EHorizontalAlignment::HAlign_Center)
						.VAlign(EVerticalAlignment::VAlign_Center)
						[
							SNew(SImage)
							.RenderTransform(FVector2D(0, 6))
							.Image(&Style->IconArmour)
						]
						+ SHorizontalBox::Slot()
						.FillWidth(1)
						[
							SNew(SOverlay)
							+ SOverlay::Slot()
							.HAlign(EHorizontalAlignment::HAlign_Left)
							.VAlign(EVerticalAlignment::VAlign_Top)
							.Padding(FMargin(10, 20, 0, 0))
							[
								SAssignNew(ArmorText, STextBlock)
								.TextStyle(&Style->PercentText)
								.Text(this, &SGameHUD_Live::GetBarValueAsText, false)
							]
							+ SOverlay::Slot()
							.HAlign(EHorizontalAlignment::HAlign_Fill)
							.VAlign(EVerticalAlignment::VAlign_Bottom)
							.Padding(FMargin(0, 0, 0, 16))
							[
								SNew(SBox)
								.HeightOverride(24)
								[
									SAssignNew(ArmorProgress, SProgressBar)
									.RenderTransform(FVector2D(-15, 0))
									.BackgroundImage(&Style->ProgressBackground)
									.FillImage(&Style->ProgressForeground)
									.BorderPadding(FVector2D(0, 0))
									.FillColorAndOpacity(this, &SGameHUD_Live::ArmorColor)
									.Percent(this, &SGameHUD_Live::GetBarPercent, false)
								]
							]
						]
					]
				]
			]
		]
	];

	ShowColorHandle = ShowAnim.AddCurve(0.0f, 1.5f, ECurveEaseFunction::QuadOut);

	LowHealthHandle = LowHealthAnim.AddCurve(0.0f, 1.2f, ECurveEaseFunction::QuadOut);
	LowArmorHandle = LowArmorAnim.AddCurve(0.0f, 1.2f, ECurveEaseFunction::QuadOut);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SGameHUD_Live::SetCharacter(const TAttribute<AUTCharacter*>& InCharacter)
{
	Character = InCharacter;
}

TOptional<float> SGameHUD_Live::GetBarPercent(const bool IsHealth) const
{
	if (Character.Get())
	{
		if (IsHealth)
		{
			return FMath::Clamp(Character.Get()->Health.Amount / Character.Get()->Health.MaxAmount, .0f, 1.0f);
		}
		else
		{
			return FMath::Clamp(Character.Get()->Armour.Amount / Character.Get()->Armour.MaxAmount, .0f, 1.0f);
		}
	}

	return .0f;
}

FText SGameHUD_Live::GetBarValueAsText(const bool IsHealth) const
{
	if (Character.Get())
	{
		if (IsHealth)
		{
			return FText::AsNumber(FMath::CeilToInt(Character.Get()->Health.Amount), &FNumberFormattingOptions::DefaultNoGrouping());
		}
		else
		{
			return FText::AsNumber(FMath::CeilToInt(Character.Get()->Armour.Amount), &FNumberFormattingOptions::DefaultNoGrouping());
		}
	}

	return FText::AsNumber(0);
}

void SGameHUD_Live::SetLiveBarFleshing(const bool IsHealth)
{
	if (IsHealth)
	{
		LowHealthAnim.PlayReverse(HealthProgress->AsShared());
	}
	else
	{
		LowArmorAnim.PlayReverse(ArmorProgress->AsShared());
	}
}

void SGameHUD_Live::ToggleWidget(const bool Toggle)
{
	FToggableWidgetHelper::ToggleWidget(AsShared(), ShowAnim, Toggle);
}
