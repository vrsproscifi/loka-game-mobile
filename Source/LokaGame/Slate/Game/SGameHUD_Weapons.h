// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Styles/GameHUDWidgetStyle.h"
#include "Weapon/WeaponContainer.h"

class AUTCharacter;

class LOKAGAME_API SGameHUD_Weapons : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SGameHUD_Weapons)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FGameHUDStyle>(TEXT("SGameHUDStyle")))
	{
		_Visibility = EVisibility::HitTestInvisible;
	}
	SLATE_ARGUMENT(TWeakObjectPtr<AUTCharacter>, Character)
	SLATE_STYLE_ARGUMENT(FGameHUDStyle, Style)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void SetCharacter(const TWeakObjectPtr<AUTCharacter>&);

	void ToggleWidget(const bool Toggle);

protected:

	const FGameHUDStyle *Style;
	const FSlateBrush* NoResource;

	TWeakObjectPtr<AUTCharacter> Character;

	FCurveSequence ShowAnim;
	FCurveHandle ShowColorHandle;

	TSharedPtr<STextBlock> AmmoClip[EShooterWeaponSlot::Grenade];
	TSharedPtr<STextBlock> AmmoTotal[EShooterWeaponSlot::End];
	TSharedPtr<SImage> WeaponIcon[EShooterWeaponSlot::End];
	TSharedPtr<SBorder> Glow[EShooterWeaponSlot::End];

	FLinearColor WidgetColor() const
	{
		return FLinearColor(1, 1, 1, FMath::Lerp(0.0f, 1.0f, ShowColorHandle.GetLerp()));
	}

	EVisibility IsWeaponSelected(const EShooterWeaponSlot) const;
	const FSlateBrush* GetWeaponIcon(const EShooterWeaponSlot) const;
	FText GetWeaponClipAmmo(const EShooterWeaponSlot) const;
	FText GetWeaponTotalAmmo(const EShooterWeaponSlot) const;
};