// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SScoreboard.h"

#include "Anchors.h"
#include "SConstraintCanvas.h"
#include "ToggableWidgetHelper.h"
// Style
#include "Styles/ScoreboardWidgetStyle.h"

#include "SGameTimer.h"

#include "UTPlayerState.h"
#include "UTGameState.h"
#include "UTTeamInfo.h"
#include "NodeComponent/NodeSessionMatch.h"

namespace EScoreboardRow
{
	enum Type
	{
		Name,
		Kills,
		Deaths,
		Score,
		End
	};
}

#define LOCTEXT_NAMESPACE "SScoreboard"

DECLARE_DELEGATE_OneParam(FOnRemoveKillRow, const TSharedRef<SWidget> &)

class LOKAGAME_API SScoreboardKillRow : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SScoreboardKillRow)
	{}
	SLATE_ARGUMENT(FSlateFontInfo, Font)
	SLATE_ARGUMENT(FSlateColor, KillerColor)
	SLATE_ARGUMENT(FSlateColor, PlayerColor)
	SLATE_ARGUMENT(const FSlateBrush*, WeaponIcon)
	SLATE_ARGUMENT(float, ShowTime)
	SLATE_ARGUMENT(FText, KillerName)
	SLATE_ARGUMENT(FText, PlayerName)
	SLATE_EVENT(FOnRemoveKillRow, OnRemoveKillRow)
	SLATE_END_ARGS()

private:
	FCurveSequence ShowAnim;
	FCurveHandle ShowColorHandle;

	FOnRemoveKillRow OnRemoveKillRow;

public:

	BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
	void Construct(const FArguments& InArgs)
	{
		OnRemoveKillRow = InArgs._OnRemoveKillRow;

		ChildSlot
		[
			SNew(SBorder)
			.BorderBackgroundColor(FLinearColor::Transparent)
			.ColorAndOpacity(this, &SScoreboardKillRow::WidgetColor)
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot()
				.AutoWidth()
				.VAlign(EVerticalAlignment::VAlign_Center)
				[
					SNew(STextBlock)
					.Font(InArgs._Font)
					.ColorAndOpacity(InArgs._KillerColor)
					.Text(InArgs._KillerName)
				]
				+ SHorizontalBox::Slot()
				.AutoWidth()
				[
					SNew(SSpacer)
					.Size(FVector2D(10, 0))
				]
				+ SHorizontalBox::Slot()
				.AutoWidth()
				[
					SNew(SBox)
					.WidthOverride(80)
					.HeightOverride(80)
					.HAlign(EHorizontalAlignment::HAlign_Fill)
					.VAlign(EVerticalAlignment::VAlign_Fill)
					[
						SNew(SImage)
						.Image(InArgs._WeaponIcon)
					]
				]
				+ SHorizontalBox::Slot()
				.AutoWidth()
				[
					SNew(SSpacer)
					.Size(FVector2D(10, 0))
				]
				+ SHorizontalBox::Slot()
				.AutoWidth()
				.VAlign(EVerticalAlignment::VAlign_Center)
				[
					SNew(STextBlock)
					.Font(InArgs._Font)
					.ColorAndOpacity(InArgs._PlayerColor)
					.Text(InArgs._PlayerName)
				]
				+ SHorizontalBox::Slot()
				.AutoWidth()
				[
					SNew(SSpacer).Size(FVector2D(10, 0))
				]
			]
		];

		ShowColorHandle = ShowAnim.AddCurve(0.0f, 1.0f);
		ShowAnim.AddCurveRelative(0.0f, InArgs._ShowTime/2);
		ShowAnim.Play(this->AsShared());
	}
	END_SLATE_FUNCTION_BUILD_OPTIMIZATION

private:

	FLinearColor WidgetColor() const
	{		
		return FLinearColor(1, 1, 1, ShowColorHandle.GetLerp());
	}

	virtual void Tick(const FGeometry & AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override
	{
		if (!ShowAnim.IsPlaying())
		{
			if (ShowAnim.IsAtEnd()) { ShowAnim.PlayReverse(this->AsShared()); }
			else { OnRemoveKillRow.Execute(this->AsShared()); }
		}
	}
};

class LOKAGAME_API SScoreboardRow : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SScoreboardRow)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FScoreboardStyle>("SScoreboardStyle"))
		, _IsRed(false)
	{}
	SLATE_ARGUMENT(bool, IsRed)
	SLATE_STYLE_ARGUMENT(FScoreboardStyle, Style)
	SLATE_ATTRIBUTE(AUTPlayerState*, PlayerState)
	SLATE_END_ARGS()
		
	BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
	void Construct(const FArguments& InArgs)
	{
		Style = InArgs._Style;
		PlayerState = InArgs._PlayerState;

		if (!InArgs._IsRed)
		{
			ChildSlot
			.Padding(FMargin(0, 4))
			[
				SNew(SHorizontalBox) + SHorizontalBox::Slot().FillWidth(Style->ColumnsSize.Name)[SNew(STextBlock).Font(Style->TableRowsFont).ColorAndOpacity(this, &SScoreboardRow::GetRowColor).Margin(FMargin(10, 0, 0, 0)).Text(this, &SScoreboardRow::GetColumnText, EScoreboardRow::Name)]
				+ SHorizontalBox::Slot().FillWidth(Style->ColumnsSize.Kills)[SNew(STextBlock).Font(Style->TableRowsFont).ColorAndOpacity(this, &SScoreboardRow::GetRowColor).Text(this, &SScoreboardRow::GetColumnText, EScoreboardRow::Kills)]
				+ SHorizontalBox::Slot().FillWidth(Style->ColumnsSize.Deaths)[SNew(STextBlock).Font(Style->TableRowsFont).ColorAndOpacity(this, &SScoreboardRow::GetRowColor).Text(this, &SScoreboardRow::GetColumnText, EScoreboardRow::Deaths)]
				+ SHorizontalBox::Slot().FillWidth(Style->ColumnsSize.Score)[SNew(STextBlock).Font(Style->TableRowsFont).ColorAndOpacity(this, &SScoreboardRow::GetRowColor).Text(this, &SScoreboardRow::GetColumnText, EScoreboardRow::Score)]
			];

			SetColor(Style->LeftRowColor);
		}
		else
		{
			ChildSlot
			.Padding(FMargin(0, 4))
			[
				SNew(SHorizontalBox) + SHorizontalBox::Slot().FillWidth(Style->ColumnsSize.Score)[SNew(STextBlock).Font(Style->TableRowsFont).ColorAndOpacity(this, &SScoreboardRow::GetRowColor).Justification(ETextJustify::Right).Text(this, &SScoreboardRow::GetColumnText, EScoreboardRow::Score)]
				+ SHorizontalBox::Slot().FillWidth(Style->ColumnsSize.Deaths)[SNew(STextBlock).Font(Style->TableRowsFont).ColorAndOpacity(this, &SScoreboardRow::GetRowColor).Justification(ETextJustify::Right).Text(this, &SScoreboardRow::GetColumnText, EScoreboardRow::Deaths)]
				+ SHorizontalBox::Slot().FillWidth(Style->ColumnsSize.Kills)[SNew(STextBlock).Font(Style->TableRowsFont).ColorAndOpacity(this, &SScoreboardRow::GetRowColor).Justification(ETextJustify::Right).Text(this, &SScoreboardRow::GetColumnText, EScoreboardRow::Kills)]
				+ SHorizontalBox::Slot().FillWidth(Style->ColumnsSize.Name)[SNew(STextBlock).Font(Style->TableRowsFont).ColorAndOpacity(this, &SScoreboardRow::GetRowColor).Justification(ETextJustify::Right).Margin(FMargin(0, 0, 10, 0)).Text(this, &SScoreboardRow::GetColumnText, EScoreboardRow::Name)]
			];

			SetColor(Style->RightRowColor);
		}
	}
	END_SLATE_FUNCTION_BUILD_OPTIMIZATION
		
	void SetColor(const FSlateColor &_Color)
	{
		Color = _Color;
	}

	FORCEINLINE int32 GetScore() const
	{
		if (PlayerState.Get())
		{
			return FMath::CeilToInt(PlayerState.Get()->Score);
		}

		return 0;
	}

protected:

	const FScoreboardStyle *Style;

	TAttribute<AUTPlayerState*> PlayerState;
	FSlateColor Color;

	FText GetColumnText(const EScoreboardRow::Type Column) const
	{
		if (PlayerState.Get())
		{
			switch (Column)
			{
				case EScoreboardRow::Name: return FText::FromString(PlayerState.Get()->GetPlayerName());
				case EScoreboardRow::Kills: return FText::AsNumber(PlayerState.Get()->Kills);
				case EScoreboardRow::Deaths: return FText::AsNumber(PlayerState.Get()->Deaths);
				case EScoreboardRow::Score: return FText::AsNumber(FMath::CeilToInt(PlayerState.Get()->Score));
			}
		}

		return FText::GetEmpty();
	}

	FSlateColor GetRowColor() const
	{
		return Color;
	}
};

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SScoreboard::Construct(const FArguments& InArgs) //Icon size 136x80 (16:9)
{	
	ScoreboardStyle = InArgs._ScoreboardStyle;
	MaxRowsPerTeam = InArgs._MaxRowsPerTeam;
	GameState = InArgs._GameState;
	LocalPlayerState = InArgs._LocalPlayerState;
	CachedNumPlayers = 0;

	SScoreboard::SetVisibility(EVisibility::HitTestInvisible);

	ChildSlot
	[
		SNew(SOverlay)
		+ SOverlay::Slot()
		.HAlign(HAlign_Fill)
		.VAlign(VAlign_Top)
		[
			SAssignNew(GameTimer, SGameTimer)
			.GameState(GameState)
			.LocalPlayerState(LocalPlayerState)
		]
		+ SOverlay::Slot()
		.HAlign(HAlign_Left)
		.VAlign(VAlign_Center)
		[
			SNew(SBox).WidthOverride(ScoreboardStyle->TableSize.X).HeightOverride(ScoreboardStyle->TableSize.Y)
			[
				SAssignNew(LeftBorder, SBorder)
				.HAlign(EHorizontalAlignment::HAlign_Fill)
				.VAlign(EVerticalAlignment::VAlign_Fill)			
				.BorderImage(&ScoreboardStyle->LeftBackground)
				.RenderTransform(this, &SScoreboard::WidgetLeftOffset)
				.ColorAndOpacity(this, &SScoreboard::WidgetColor)
				.BorderBackgroundColor(this, &SScoreboard::WidgetBackgroundColor)
				.Padding(0)
				[
					SNew(SOverlay)
					+ SOverlay::Slot()
					[
						SNew(SBorder)
						.HAlign(EHorizontalAlignment::HAlign_Fill)
						.VAlign(EVerticalAlignment::VAlign_Fill)					
						.BorderImage(&ScoreboardStyle->LeftBackgroundEdge)
					]
					+ SOverlay::Slot()
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot()
						.AutoHeight()
						[
							SNew(SBox)
							.HeightOverride(FOptionalSize(120.0f))
							.Padding(FMargin(0,20))
							[
								SNew(SHorizontalBox)
								+ SHorizontalBox::Slot()
								.FillWidth(1)
								.VAlign(EVerticalAlignment::VAlign_Center)	
								.HAlign(EHorizontalAlignment::HAlign_Fill)
								[
									SNew(STextBlock)
									.Text(InArgs._LeftHeadText)
									.Font(ScoreboardStyle->HeadFont)
									.Margin(FMargin(20,0,0,0))
								]
								+ SHorizontalBox::Slot()
								.AutoWidth()
								[
									SNew(SBox)
									.WidthOverride(FOptionalSize(136.0f))
									[
										SNew(SImage)
										.Image(&ScoreboardStyle->LeftIcon)
									]
								]
								+ SHorizontalBox::Slot()
								.FillWidth(0.2)
							]
						]
						+ SVerticalBox::Slot()
						.AutoHeight()
						.Padding(FMargin(0, 0, ScoreboardStyle->TableLinePadding.X, 0))
						[
							SNew(SBorder)
							.BorderImage(&ScoreboardStyle->UpLine)
							.Padding(0)
							[
								SNew(SHorizontalBox)
								+ SHorizontalBox::Slot().FillWidth(ScoreboardStyle->ColumnsSize.Name).VAlign(VAlign_Center)[SNew(STextBlock).Text(FText(LOCTEXT("playerName", "NAME"))).Font(ScoreboardStyle->TableHeadFont).ColorAndOpacity(ScoreboardStyle->LeftHeadColor).Margin(FMargin(10, 0, 0, 0))]
								+ SHorizontalBox::Slot().FillWidth(ScoreboardStyle->ColumnsSize.Kills).HAlign(HAlign_Left).VAlign(VAlign_Center)[SNew(SImage).Image(&ScoreboardStyle->IconLeftKills)]
								+ SHorizontalBox::Slot().FillWidth(ScoreboardStyle->ColumnsSize.Deaths).HAlign(HAlign_Left).VAlign(VAlign_Center)[SNew(SImage).Image(&ScoreboardStyle->IconLeftDeaths)]
								+ SHorizontalBox::Slot().FillWidth(ScoreboardStyle->ColumnsSize.Score).VAlign(VAlign_Center)[SNew(STextBlock).Text(FText(LOCTEXT("playerScore", "SCORE"))).Font(ScoreboardStyle->TableHeadFont).ColorAndOpacity(ScoreboardStyle->LeftHeadColor)]
							]
						]
						+ SVerticalBox::Slot()
						.AutoHeight()
						.Padding(FMargin(0, 0, ScoreboardStyle->TableLinePadding.X, 0))
						[
							SAssignNew(LeftTeamList, SVerticalBox)
						]
						+ SVerticalBox::Slot()
						.FillHeight(1)
						+ SVerticalBox::Slot()
						.AutoHeight()
						.Padding(FMargin(0, 0, ScoreboardStyle->TableLinePadding.X, 0))
						.Expose(LeftLocalRow)
						+ SVerticalBox::Slot()
						.AutoHeight()
						.Padding(FMargin(0, 0, ScoreboardStyle->TableLinePadding.X, ScoreboardStyle->TableLinePadding.Y))
						[
							SNew(SBorder)
							.BorderImage(&ScoreboardStyle->DownLine)
							.Padding(FMargin(10,20,10,10))
							[
								SNew(SHorizontalBox)
								+ SHorizontalBox::Slot()
								.AutoWidth()
								.HAlign(EHorizontalAlignment::HAlign_Left)
								[
									SNew(STextBlock).Text(FText(LOCTEXT("totalScore", "TOTAL SCORE"))).Font(ScoreboardStyle->TableTotalScoreFont)
								]
								+ SHorizontalBox::Slot()
								.FillWidth(1)
								.HAlign(EHorizontalAlignment::HAlign_Right)
								[
									SAssignNew(LeftTotalScore, STextBlock).Text(this, &SScoreboard::GetTotalScore, false).Font(ScoreboardStyle->TableTotalScoreFont)
								]
							]
						]
					]
				]
			]
		]
		+ SOverlay::Slot()
		.HAlign(HAlign_Right)
		.VAlign(VAlign_Center)
		[
			SNew(SBox).WidthOverride(ScoreboardStyle->TableSize.X).HeightOverride(ScoreboardStyle->TableSize.Y)
			[
				SAssignNew(RightBorder, SBorder)
				.HAlign(EHorizontalAlignment::HAlign_Fill)
				.VAlign(EVerticalAlignment::VAlign_Fill)
				.BorderImage(&ScoreboardStyle->RightBackground)
				.RenderTransform(this, &SScoreboard::WidgetRightOffset)
				.ColorAndOpacity(this, &SScoreboard::WidgetColor)
				.BorderBackgroundColor(this, &SScoreboard::WidgetBackgroundColor)
				.Padding(0)
				[
					SNew(SOverlay)
					+ SOverlay::Slot()
					[
						SNew(SBorder)
						.HAlign(EHorizontalAlignment::HAlign_Fill)
						.VAlign(EVerticalAlignment::VAlign_Fill)
						.BorderImage(&ScoreboardStyle->RightBackgroundEdge)
					]
					+ SOverlay::Slot()
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot()
						.AutoHeight()
						[
							SNew(SBox)
							.HeightOverride(FOptionalSize(120.0f))
							.Padding(FMargin(0, 20))
							[
								SNew(SHorizontalBox)
								+ SHorizontalBox::Slot()
								.FillWidth(0.2)
								+ SHorizontalBox::Slot()
								.AutoWidth()
								[
									SNew(SBox)
									.WidthOverride(FOptionalSize(136.0f))
									[
										SNew(SImage)
										.Image(&ScoreboardStyle->RightIcon)
									]
								]
								+ SHorizontalBox::Slot()
								.FillWidth(1)
								.VAlign(EVerticalAlignment::VAlign_Center)
								.HAlign(EHorizontalAlignment::HAlign_Fill)
								[
									SNew(STextBlock)
									.Text(InArgs._RightHeadText)
									.Font(ScoreboardStyle->HeadFont)
									.Margin(FMargin(0, 0, 20, 0))
									.Justification(ETextJustify::Right)
								]							
							]
						]
						+ SVerticalBox::Slot()
						.AutoHeight()
						.Padding(FMargin(ScoreboardStyle->TableLinePadding.X, 0, 0, 0))
						[
							SNew(SBorder)
							.BorderImage(&ScoreboardStyle->UpLine)
							.Padding(0)
							[
								SNew(SHorizontalBox)
								+ SHorizontalBox::Slot().FillWidth(ScoreboardStyle->ColumnsSize.Score).VAlign(VAlign_Center)[SNew(STextBlock).Text(FText(LOCTEXT("playerScore", "SCORE"))).Font(ScoreboardStyle->TableHeadFont).Justification(ETextJustify::Right).ColorAndOpacity(ScoreboardStyle->RightHeadColor)]
								+ SHorizontalBox::Slot().FillWidth(ScoreboardStyle->ColumnsSize.Deaths).HAlign(HAlign_Right).VAlign(VAlign_Center)[SNew(SImage).Image(&ScoreboardStyle->IconRightDeaths)]
								+ SHorizontalBox::Slot().FillWidth(ScoreboardStyle->ColumnsSize.Kills).HAlign(HAlign_Right).VAlign(VAlign_Center)[SNew(SImage).Image(&ScoreboardStyle->IconRightKills)]
								+ SHorizontalBox::Slot().FillWidth(ScoreboardStyle->ColumnsSize.Name).VAlign(VAlign_Center)[SNew(STextBlock).Text(FText(LOCTEXT("playerName", "NAME"))).Font(ScoreboardStyle->TableHeadFont).Justification(ETextJustify::Right).ColorAndOpacity(ScoreboardStyle->RightHeadColor).Margin(FMargin(0, 0, 10, 0))]
							]
						]
						+ SVerticalBox::Slot()
						.AutoHeight()
						.Padding(FMargin(ScoreboardStyle->TableLinePadding.X, 0, 0, 0))
						[
							SAssignNew(RightTeamList, SVerticalBox)
						]
						+ SVerticalBox::Slot()
						.FillHeight(1)
						+ SVerticalBox::Slot()
						.AutoHeight()
						.Padding(FMargin(0, 0, ScoreboardStyle->TableLinePadding.X, 0))
						.Expose(RightLocalRow)
						+ SVerticalBox::Slot()
						.AutoHeight()
						.Padding(FMargin(ScoreboardStyle->TableLinePadding.X, 0, 0, ScoreboardStyle->TableLinePadding.Y))
						[
							SNew(SBorder)
							.BorderImage(&ScoreboardStyle->DownLine)
							.Padding(FMargin(10, 20, 10, 10))
							[
								SNew(SHorizontalBox)							
								+ SHorizontalBox::Slot()
								.FillWidth(1)
								.HAlign(EHorizontalAlignment::HAlign_Left)
								[
									SAssignNew(RightTotalScore, STextBlock).Text(this, &SScoreboard::GetTotalScore, true).Font(ScoreboardStyle->TableTotalScoreFont)
								]
								+ SHorizontalBox::Slot()
								.AutoWidth()
								.HAlign(EHorizontalAlignment::HAlign_Right)
								[
									SNew(STextBlock).Text(FText(LOCTEXT("totalScore", "TOTAL SCORE"))).Font(ScoreboardStyle->TableTotalScoreFont)
								]
							]
						]
					]
				]
			]
		]
		//====================================================================[ RIGHT TOP (Kill Rows)
		+ SOverlay::Slot()
		.HAlign(HAlign_Right)
		.VAlign(VAlign_Top)
		[
			SAssignNew(KillsBox, SScrollBox)
			.ScrollBarVisibility(EVisibility::Collapsed)
		]
		+ SOverlay::Slot()
		.HAlign(HAlign_Center)
		.VAlign(VAlign_Top)
		[
			SNew(SBox)
			.MinDesiredWidth(400)
			.Padding(FMargin(0, 100, 0, 0))
			.Visibility_Lambda([&]() { return ShowAnim.IsAtStart() == false ? EVisibility::Visible : EVisibility::Hidden; })
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot()
				.AutoHeight()
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot()
					[
						SNew(STextBlock)
						.Text(NSLOCTEXT("SHUD", "GameMode", "Game mode:"))
						.Font(ScoreboardStyle->KillRowFont)
					]
					+ SHorizontalBox::Slot()
					[
						SNew(STextBlock)
						.Font(ScoreboardStyle->KillRowFont)
						.Text_Lambda([&]() { return (GameState.IsSet() && GameState.Get()) ? GameState.Get()->GetMatchInformation().Options.GetGameMode().GetDisplayName() : FText::GetEmpty(); })
					]
				]
				+ SVerticalBox::Slot()
				.AutoHeight()
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot()
					[
						SNew(STextBlock)
						.Text(NSLOCTEXT("SHUD", "GameMap", "Game map:"))
						.Font(ScoreboardStyle->KillRowFont)
					]
					+ SHorizontalBox::Slot()
					[
						SNew(STextBlock)
						.Font(ScoreboardStyle->KillRowFont)
						.Text_Lambda([&]() { return (GameState.IsSet() && GameState.Get()) ? GameState.Get()->GetMatchInformation().Options.GetGameMap().GetDisplayName() : FText::GetEmpty(); })
					]
				]
				+ SVerticalBox::Slot()
				.AutoHeight()
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot()
					[
						SNew(STextBlock)
						.Text(NSLOCTEXT("SHUD", "GameDiff", "Game difficulty:"))
						.Font(ScoreboardStyle->KillRowFont)
					]
					+ SHorizontalBox::Slot()
					[
						SNew(STextBlock)
						.Font(ScoreboardStyle->KillRowFont)
						.Text_Lambda([&]() { return (GameState.IsSet() && GameState.Get()) ? FText::AsNumber(GameState.Get()->GetMatchInformation().Options.GetDifficulty()) : FText::GetEmpty(); })
					]
				]
			]
		]
	];
	
	GameTimer->LeftScore->SetText(TAttribute<FText>::Create(TAttribute<FText>::FGetter::CreateSP(this, &SScoreboard::GetTotalScore, false)));
	GameTimer->RightScore->SetText(TAttribute<FText>::Create(TAttribute<FText>::FGetter::CreateSP(this, &SScoreboard::GetTotalScore, true)));

	ShowTransHandle = ShowAnim.AddCurve(0.0f, InArgs._TransDuration, ECurveEaseFunction::QuadIn);
	ShowColorHandle = ShowAnim.AddCurve(InArgs._TransDuration / 3, InArgs._ColorDuration, ECurveEaseFunction::QuadOut);
	ShowAnim.AddCurveRelative(0.0f, 0.1f); //Rezerv

	RegisterActiveTimer(1.0f, FWidgetActiveTimerDelegate::CreateSP(this, &SScoreboard::Update));
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SScoreboard::ToggleTable(const bool _Toggle)
{
	FToggableWidgetHelper::ToggleWidget(AsShared(), ShowAnim, _Toggle);
}

EActiveTimerReturnType SScoreboard::Update(double InCurrentTime, float InDeltaTime)
{
	if (GameState.Get())
	{
		// Update row count
		//if (GameState.Get()->PlayerArray.Num() != CachedNumPlayers && LocalPlayerState.Get())
		{
			CachedNumPlayers = GameState.Get()->PlayerArray.Num();

			LeftTeamList->ClearChildren();
			RightTeamList->ClearChildren();

			TArray<AUTPlayerState*> TeamList;
			for (auto &i : GameState.Get()->PlayerArray)
			{
				if (auto p = Cast<AUTPlayerState>(i))
				{
					if (p->bIsDemoRecording == false) TeamList.Add(p);
				}
			}
			TeamList.Sort([](const AUTPlayerState& A, const AUTPlayerState& B) { return A.Score > B.Score; });

			for (auto p : TeamList)
			{
				auto ShooterPlayerState = Cast<AUTPlayerState>(p);
				if (ShooterPlayerState)
				{
					const bool IsFriendly = (ShooterPlayerState == LocalPlayerState.Get() || GameState.Get()->OnSameTeam(LocalPlayerState.Get(), ShooterPlayerState));// FFlagsHelper::HasAnyFlags(GameState.Get()->CurrentGameMode, EGameModeTypeId::LostDeadMatch | EGameModeTypeId::DuelMatch) ? (ShooterPlayerState == LocalPlayerState.Get() ? true : false) : ShooterPlayerState->GetTeamNum() == LocalPlayerState.Get()->GetTeamNum();
					auto RowWidget = SNew(SScoreboardRow).IsRed(!IsFriendly).PlayerState(ShooterPlayerState);
					auto Container = ((IsFriendly) ? LeftTeamList : RightTeamList);

					if (Container->GetChildren()->Num() < MaxRowsPerTeam)
					{
						Container->AddSlot()[RowWidget];

						if (ShooterPlayerState == LocalPlayerState.Get())
						{
							RowWidget->SetColor(ScoreboardStyle->LocalPlayerColor);
						}
					}	
					else if (ShooterPlayerState == LocalPlayerState.Get())
					{
						LeftLocalRow->AttachWidget(RowWidget);
						RowWidget->SetColor(ScoreboardStyle->LocalPlayerColor);
					}
				}
			}


			// Offline
			//for (auto p : GameState.Get()->PlayerArrayOffline)
			//{
			//	auto ShooterPlayerState = Cast<AShooterPlayerState>(p);
			//	if (ShooterPlayerState)
			//	{
			//		const bool IsFriendly = ShooterPlayerState->GetTeamNum() == LocalPlayerState.Get()->GetTeamNum();
			//		auto RowWidget = SNew(SScoreboardRow).IsRed(!IsFriendly).PlayerState(ShooterPlayerState);
			//		auto Container = ((IsFriendly) ? LeftTeamList : RightTeamList);

			//		if (Container->GetChildren()->Num() < MaxRowsPerTeam)
			//		{
			//			Container->AddSlot()[RowWidget];
			//			RowWidget->SetColor(FLinearColor::Gray);
			//		}
			//	}
			//}
		}

		auto TargetLambda = [](const SBoxPanel::FSlot& A, const SBoxPanel::FSlot& B)
		{
			auto GreenRowA = StaticCastSharedRef<SScoreboardRow>(A.GetWidget());
			auto GreenRowB = StaticCastSharedRef<SScoreboardRow>(B.GetWidget());

			if (GreenRowA != SNullWidget::NullWidget && GreenRowB != SNullWidget::NullWidget)
			{
				return GreenRowA->GetScore() > GreenRowB->GetScore();
			}

			return false;
		};

		//Sort by score		
		TPanelChildren<SBoxPanel::FSlot>* ChildsGreen = static_cast<TPanelChildren<SBoxPanel::FSlot>*>(LeftTeamList->GetChildren());
		ChildsGreen->Sort(TargetLambda);

		TPanelChildren<SBoxPanel::FSlot>* ChildsRed = static_cast<TPanelChildren<SBoxPanel::FSlot>*>(RightTeamList->GetChildren());
		ChildsRed->Sort(TargetLambda);
	}

	return EActiveTimerReturnType::Continue;
}

FLinearColor SScoreboard::WidgetColor() const
{
	return FLinearColor(1, 1, 1, FMath::Lerp(0.0f, 1.0f, ShowColorHandle.GetLerp()));
}

FSlateColor SScoreboard::WidgetBackgroundColor() const
{
	return FLinearColor(1, 1, 1, FMath::Lerp(0.0f, 1.0f, ShowColorHandle.GetLerp()));
}

TOptional<FSlateRenderTransform> SScoreboard::WidgetLeftOffset() const
{
	return FSlateRenderTransform(FVector2D(FMath::Lerp(-ScoreboardStyle->TableSize.X, 0.0f, ShowTransHandle.GetLerp()), 0));
}

TOptional<FSlateRenderTransform> SScoreboard::WidgetRightOffset() const
{
	return FSlateRenderTransform(FVector2D(FMath::Lerp(ScoreboardStyle->TableSize.X, 0.0f, ShowTransHandle.GetLerp()), 0));
}

FText SScoreboard::GetTotalScore(const bool IsRight) const
{
	if (GameState.Get() && LocalPlayerState.Get() && GameState.Get()->bTeamGame && GameState.Get()->Teams.Num() >= 2)
	{		
		if (FMath::IsNearlyEqual(GameState.Get()->GoalScore, 100.0f)) // Capture point
		{
			return FText::AsPercent(float(GameState.Get()->Teams[IsRight ? !LocalPlayerState.Get()->GetTeamNum() : LocalPlayerState.Get()->GetTeamNum()]->Score) / float(GameState.Get()->GoalScore));
		}

		return FText::AsNumber(GameState.Get()->Teams[IsRight ? !LocalPlayerState.Get()->GetTeamNum() : LocalPlayerState.Get()->GetTeamNum()]->Score);
	}

	return FText::GetEmpty();
}

void SScoreboard::onRemoveKillRow(const TSharedRef<SWidget> &_Widget)
{
	KillsBox->RemoveSlot(_Widget);
}

void SScoreboard::AppendKillRow(const AUTPlayerState* Killer, const AUTPlayerState* Target, const FSlateBrush *WeaponIcon)
{
	if (GameState.Get() && LocalPlayerState.Get() && Killer && Target && WeaponIcon)
	{
		KillsBox->AddSlot().AttachWidget
			(
				SNew(SScoreboardKillRow)
				.Font(ScoreboardStyle->KillRowFont)
				.PlayerColor(Target == LocalPlayerState.Get() ? ScoreboardStyle->LocalPlayerColor : GameState.Get()->OnSameTeam(Target, LocalPlayerState.Get()) ? ScoreboardStyle->LeftHeadColor : ScoreboardStyle->RightHeadColor)
				.PlayerName(FText::FromString(Target->GetPlayerName()))
				.KillerColor(Killer == LocalPlayerState.Get() ? ScoreboardStyle->LocalPlayerColor : GameState.Get()->OnSameTeam(Killer, LocalPlayerState.Get()) ? ScoreboardStyle->LeftHeadColor : ScoreboardStyle->RightHeadColor)
				.KillerName((Killer == Target) ? FText::GetEmpty() : FText::FromString(Killer->GetPlayerName()))
				.WeaponIcon(WeaponIcon)
				.ShowTime(ScoreboardStyle->DefaultKillRowShowTime)
				.OnRemoveKillRow(this, &SScoreboard::onRemoveKillRow)
			);

		KillsBox->ScrollToEnd();
	}
}

#undef LOCTEXT_NAMESPACE