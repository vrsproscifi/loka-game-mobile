// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SGameHUD.h"
#include "ToggableWidgetHelper.h"
// UMG CanvasPanel
#include "Anchors.h"
#include "SConstraintCanvas.h"
// Experimental
#include "UTCharacter.h"
#include "CharacterAbility.h"
#include "UTGameState.h"
#include "Components/WidgetComponent.h"
#include "WidgetLayoutLibrary.h"

class LOKAGAME_API SSimpleChatRow : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SSimpleChatRow)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FGameHUDStyle>(TEXT("SGameHUDStyle")))
		, _IsSimple(false)
		, _SendTo(ESendMessageTo::All)
		, _PlayerColor(FLinearColor::White)
		, _IsAction(false)
	{}
	SLATE_STYLE_ARGUMENT(FGameHUDStyle, Style)
	SLATE_ARGUMENT(bool, IsSimple)
	SLATE_ARGUMENT(bool, IsAction)
	SLATE_ARGUMENT(ESendMessageTo, SendTo)
	SLATE_ARGUMENT(FSlateColor, PlayerColor)
	SLATE_ARGUMENT(int32, ActionValue)
	SLATE_ARGUMENT(FText, Player)
	SLATE_ARGUMENT(FText, Message)
	SLATE_END_ARGS()

	FCurveSequence HideAnim;
	FCurveHandle HideColorHandle;

	BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
	void Construct(const FArguments& InArgs)
	{
		const FGameHUDStyle *Style = InArgs._Style;

		if (InArgs._IsAction)
		{
			CurrentValue = 0;
			TargetValue = InArgs._ActionValue;

			HideAnim.AddCurve(0.0f, 5.0f);
			HideColorHandle = HideAnim.AddCurveRelative(0.0f, 3.0f);

			ChildSlot
			[
				SNew(SBorder)
				.BorderBackgroundColor(FLinearColor::Transparent)
				.Padding(0)
				.ColorAndOpacity(this, &SSimpleChatRow::WidgetColor)
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot().FillWidth(1)
					+ SHorizontalBox::Slot().AutoWidth()
					[
						SNew(STextBlock)
						.Font(Style->ChatText)
						.Text(InArgs._Message)
						.ColorAndOpacity(InArgs._PlayerColor)
						.Justification(ETextJustify::Center)
					]
					+ SHorizontalBox::Slot().AutoWidth()
					[
						SAssignNew(ValueContainer, STextBlock)
						.Font(Style->ChatText)
						.Text(FText::AsNumber(0))
						.ColorAndOpacity(FMath::Lerp(InArgs._PlayerColor.GetSpecifiedColor(), FLinearColor(0, 0, 0, 1), 0.3f))
						.Justification(ETextJustify::Right)
					]					
				]
			];

			RegisterActiveTimer(0.01f, FWidgetActiveTimerDelegate::CreateSP(this, &SSimpleChatRow::ValueAnimate));
		}
		else
		{
			HideAnim.AddCurve(0.0f, 15.0f);
			HideColorHandle = HideAnim.AddCurveRelative(0.0f, 3.0f);

			ChildSlot
			[
				SNew(SBorder)
				.BorderBackgroundColor(FLinearColor::Transparent)
				.Padding(0)
				.ColorAndOpacity(this, &SSimpleChatRow::WidgetColor)
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot()
					.AutoWidth()
					[
						SNew(STextBlock)
						.Font(Style->ChatPlayer)
						.Text(FText::Format(FText::FromString("{0}: "), InArgs._Player))
						.ColorAndOpacity(InArgs._PlayerColor)
					]
					+ SHorizontalBox::Slot()
					.FillWidth(1)
					[
						SNew(STextBlock)
						.Font(Style->ChatText)
						.Text(InArgs._Message)
						.AutoWrapText(true)
						.ColorAndOpacity((InArgs._SendTo == ESendMessageTo::All) ? Style->ColorMessageToAll : FMath::Lerp(InArgs._PlayerColor.GetSpecifiedColor(), FLinearColor(1, 1, 1, 1), 0.3f))
					]
				]
			];
		}

		if (InArgs._IsSimple && !InArgs._IsAction)
			HideAnim.Play(this->AsShared());

	}
	END_SLATE_FUNCTION_BUILD_OPTIMIZATION

	FLinearColor WidgetColor() const
	{
		return FLinearColor(1, 1, 1, FMath::Lerp(1.0f, 0.0f, HideColorHandle.GetLerp()));
	}

protected:

	TSharedPtr<STextBlock> ValueContainer;

	float CurrentValue;
	float TargetValue;

	EActiveTimerReturnType ValueAnimate(double InCurrentTime, float InDeltaTime)
	{
		//CurrentValue = 2.0f / InDeltaTime * TargetValue * 2.0f;

		CurrentValue = FMath::FInterpConstantTo(CurrentValue, TargetValue, InDeltaTime, TargetValue > 0 ? TargetValue * 0.5f : TargetValue * -0.5f);

		//2000\10*kolichestvo*skorost
		//CurrentValue = FMath::Lerp(CurrentValue, TargetValue, FMath::Clamp(InDeltaTime * (2.0f / InDeltaTime * TargetValue * 2.0f), .0f, 1.f));

		if (FMath::IsNearlyEqual(CurrentValue, TargetValue) == false)
		{
			if (TargetValue < 0)
			{
				ValueContainer->SetText(FText::Format(FText::FromString(" {0}"), FText::AsNumber(FPlatformMath::CeilToInt(CurrentValue), &FNumberFormattingOptions::DefaultNoGrouping())));
			}
			else
			{
				ValueContainer->SetText(FText::Format(FText::FromString(" +{0}"), FText::AsNumber(FPlatformMath::CeilToInt(CurrentValue), &FNumberFormattingOptions::DefaultNoGrouping())));
			}

			return EActiveTimerReturnType::Continue;
		}

		HideAnim.Play(this->AsShared());
		return EActiveTimerReturnType::Stop;
	}
};

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SGameHUD::Construct(const FArguments& InArgs)
{
	Controller = InArgs._Controller;
	Style = InArgs._Style;
	SendMessageEvent = InArgs._OnMessageSend;

	TSharedRef<SWidget> LiveBarWidget = SNullWidget::NullWidget;
	TSharedRef<SWidget> WeaponBarWidget = SNullWidget::NullWidget;

	SAssignNew(LiveBar, SGameHUD_Live);
	SAssignNew(WeaponBar, SGameHUD_Weapons);
	SAssignNew(Widget_Ability, SGameHUD_Ability);

	if (InArgs._IsUnion)
	{
		LiveBarWidget = LiveBar.ToSharedRef();
		WeaponBarWidget = WeaponBar.ToSharedRef();
	}

	auto ScrollyBox = new FScrollBoxStyle();
	ScrollyBox->SetTopShadowBrush(FSlateNoResource());
	ScrollyBox->SetBottomShadowBrush(FSlateNoResource());
	ScrollyBox->SetLeftShadowBrush(FSlateNoResource());
	ScrollyBox->SetRightShadowBrush(FSlateNoResource());

	ChildSlot
	[
		SNew(SConstraintCanvas)
		//===============================================[ LEFT (Health & Armor)
		+ SConstraintCanvas::Slot()
		.Anchors(FAnchors(0, 0, 1, 1))
		.Offset(FMargin(0))
		[
			LiveBarWidget
		]
		//===============================================[ RIGHT (Weapons)
		+ SConstraintCanvas::Slot()
		.Anchors(FAnchors(0, 0, 1, 1))
		.Offset(FMargin(0))
		[
			WeaponBarWidget
		]
		//===============================================[ CENTER (Ability)
		+ SConstraintCanvas::Slot()
		.Anchors(FAnchors(0, 0, 1, 1))
		.Offset(FMargin(0))
		[
			Widget_Ability.ToSharedRef()
		]
		//===============================================[ LEFT-CENTER CENTER-BOTTOM (Actions)
		+ SConstraintCanvas::Slot()
		.Anchors(FAnchors(0.5, 1))
		.Alignment(FVector2D(1, 1))
		.Offset(FMargin(-300, -300, 400, 300))
		[
			SAssignNew(ActionsContainer, SScrollBox)
			.ScrollBarAlwaysVisible(false)
			.ScrollBarVisibility(EVisibility::Collapsed)
			.Style(ScrollyBox)
		]
		//===============================================[ CENTER BOTTOM (Chat)
		+ SConstraintCanvas::Slot()
		.Anchors(FAnchors(0.5, 1))
		.Alignment(FVector2D(0.5, 1))
		.Offset(FMargin(0, -100, 600, 300))
		[
			SAssignNew(ChatContainer, SVerticalBox).Visibility(EVisibility::Hidden)
			+ SVerticalBox::Slot()
			.FillHeight(1)
			[
				SNew(SBorder)
				.BorderImage(&Style->ChatBackground)
				.Padding(8)
				[
					SAssignNew(MessagesContainer, SScrollBox)
					.ScrollBarStyle(&Style->ChatContainerScroll)
					.Style(&Style->ChatContainer)
				]
			]
			+ SVerticalBox::Slot()
			.AutoHeight()
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot()
				.FillWidth(1)
				[
					SNew(SOverlay)
					+ SOverlay::Slot()
					.HAlign(EHorizontalAlignment::HAlign_Fill)
					.VAlign(EVerticalAlignment::VAlign_Fill)
					[
						SAssignNew(ChatInput, SEditableTextBox)
						.Style(&Style->ChatInput)
						.Cursor(EMouseCursor::TextEditBeam)
						.Visibility(EVisibility::Visible)
						.ClearKeyboardFocusOnCommit(false)
						.OnTextCommitted(this, &SGameHUD::OnSendMessage)
					]
					+ SOverlay::Slot()
					.HAlign(EHorizontalAlignment::HAlign_Left)
					.VAlign(EVerticalAlignment::VAlign_Fill)
					[
						SNew(SBox)
						.WidthOverride(100)
						.Padding(6)
						[
							SAssignNew(ChatTypeToggle, SCheckBox)
							.Style(&Style->ChatToggleType)
							.HAlign(EHorizontalAlignment::HAlign_Center)
							.Padding(FMargin(0, 10))
							.OnCheckStateChanged(this, &SGameHUD::OnSendMessageType)
							[
								SAssignNew(ChatTypeText, STextBlock)
								.TextStyle(&Style->ChatToggleStyle)
								.Text(NSLOCTEXT("SHUDChat", "SendMessageToAll", "TO ALL"))
							]
						]
					]
				]
				+ SHorizontalBox::Slot()
				.AutoWidth()
				[
					SNew(SBox)
					.WidthOverride(60)
					.HeightOverride(40)
					[
						SNew(SButton)
						.ButtonStyle(&Style->ChatButton)
						.HAlign(EHorizontalAlignment::HAlign_Center)
						.VAlign(EVerticalAlignment::VAlign_Center)
						.OnClicked(this, &SGameHUD::OnSendMessageClicked)
						[
							SNew(SImage)
							.Image(&Style->ChatIcon)
						]
					]
				]
			]
		]
		//===============================================[ LEFT CENTER (Chat (Simple View Only))
		+ SConstraintCanvas::Slot()
		.Anchors(FAnchors(0, 0.5f))
		.Alignment(FVector2D(0, 0.5f))
		.Offset(FMargin(10, 10, 700, 100))
		[
			SAssignNew(SimpleContainer, SScrollBox)
			.ScrollBarVisibility(EVisibility::Collapsed)
			.Style(&Style->ChatContainer)
			.Visibility(EVisibility::HitTestInvisible)
		]
		//===============================================[ CENTER BOTTOM (Respawn time)
		+ SConstraintCanvas::Slot()
		.Anchors(FAnchors(0.5, 1))
		.Alignment(FVector2D(0.5, 1))
		.Offset(FMargin(0.0f, -400.0f, 600.0f, 100.0f))
		[
			SAssignNew(RespawnContainer, SBorder)
			.Padding(0)
			.BorderBackgroundColor(FLinearColor::Transparent)
			.ColorAndOpacity(this, &SGameHUD::RespawnColor)
			.Visibility(EVisibility::HitTestInvisible)
			.VAlign(VAlign_Bottom)
			.HAlign(HAlign_Center)
			[
				SNew(SBorder)
				.BorderImage(&Style->RespawnTextBackground)
				.Padding(FMargin(40, 6))
				[
					SAssignNew(BottomSwitcher, SWidgetSwitcher)
					.Visibility(EVisibility::HitTestInvisible)
					+ SWidgetSwitcher::Slot()
					.HAlign(HAlign_Center)
					[
						SNew(SHorizontalBox)
						+ SHorizontalBox::Slot()
						.HAlign(EHorizontalAlignment::HAlign_Right)
						.AutoWidth()
						[
							SAssignNew(RespawnText, STextBlock)
							.Font(Style->RespawnTextFont)
							.ColorAndOpacity(Style->RespawnInColor)
							.Text(NSLOCTEXT("SHUD", "RespawnIn", "Respawn in: "))
							.Visibility(EVisibility::HitTestInvisible)
						]
						+ SHorizontalBox::Slot()
						.HAlign(EHorizontalAlignment::HAlign_Left)
						.AutoWidth()
						[
							SAssignNew(RespawnCounter, STextBlock)
							.Font(Style->RespawnTextFont)
							.ColorAndOpacity(this, &SGameHUD::RespawnCountColor)
							.Visibility(EVisibility::HitTestInvisible)
						]
					]
					+ SWidgetSwitcher::Slot()
					.HAlign(HAlign_Center)
					[
						SAssignNew(BottomText, STextBlock)
						.Font(Style->RespawnTextFont)
						.ColorAndOpacity(Style->RespawnInColor)
						.Justification(ETextJustify::Center)
						.Visibility(EVisibility::HitTestInvisible)
					]
				]
			]
		]
		//===============================================[ CENTER TOP (Medals)
		+ SConstraintCanvas::Slot()
		.Anchors(FAnchors(0.5, 0.3))
		.Alignment(FVector2D(0.5, 0.5))
		.Offset(FMargin(0.0f, 0.0f, 400.0f, 120.0f))
		[
			SAssignNew(Widget_Medals, SGameHUD_Medals)
		]
	];

	ShowColorHandle = ShowAnim.AddCurve(0.0f, 1.5f, ECurveEaseFunction::QuadOut);

	RespawnCountHandle = RespawnCountAnim.AddCurve(0.0f, 0.8f, ECurveEaseFunction::QuadOut);
	RespawnVisibleHandle = RespawnVisibleAnim.AddCurve(0.0f, 1.6f, ECurveEaseFunction::QuadInOut);

	eSendMessageTo = ESendMessageTo::All;

	SGameHUD::SetVisibility(EVisibility::SelfHitTestInvisible);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SGameHUD::ToggleChat(const bool _Toggle)
{
	SGameHUD::ChatContainer->SetVisibility(_Toggle ? EVisibility::Visible : EVisibility::Hidden);
	OnToggleInteractive.ExecuteIfBound(_Toggle);

	if (_Toggle)
	{
		FSlateApplication::Get().SetKeyboardFocus(ChatInput, EFocusCause::SetDirectly);
	}
	else
	{
		FSlateApplication::Get().SetKeyboardFocus(ChatInput, EFocusCause::Cleared);
	}
}

void SGameHUD::SetIsAutoHideChat(const bool _IsHide)
{
	SGameHUD::bAutoHideChat = _IsHide;
}

void SGameHUD::SetIsAutoClearInput(const bool _IsClear)
{
	SGameHUD::bAutoClearInput = _IsClear;
}

void SGameHUD::SetMessageSendTo(const ESendMessageTo _SendTo)
{
	eSendMessageTo = _SendTo;
	ChatTypeText->SetText((eSendMessageTo == ESendMessageTo::All) ? NSLOCTEXT("SHUDChat", "SendMessageToAll", "TO ALL") : NSLOCTEXT("SHUDChat", "SendMessageToTeam", "TO TEAM"));
	ChatTypeToggle->SetIsChecked((eSendMessageTo == ESendMessageTo::All) ? ECheckBoxState::Unchecked : ECheckBoxState::Checked);
}

void SGameHUD::AppendMessage(const FText &_Player, const FSlateColor &_PlayerColor, const FText &_Message, const ESendMessageTo _SendTo)
{
	SGameHUD::MessagesContainer->AddSlot().AttachWidget(SNew(SSimpleChatRow).Player(_Player).PlayerColor(_PlayerColor).Message(_Message).SendTo(_SendTo));
	SGameHUD::SimpleContainer->AddSlot().AttachWidget(SNew(SSimpleChatRow).Player(_Player).PlayerColor(_PlayerColor).Message(_Message).SendTo(_SendTo).IsSimple(true));

	SGameHUD::MessagesContainer->ScrollToEnd();
	SGameHUD::SimpleContainer->ScrollToEnd();
}

void SGameHUD::ScrollChat(const float _Offset)
{
	const float _off = SGameHUD::MessagesContainer->GetScrollOffset();
	SGameHUD::MessagesContainer->SetScrollOffset(_off + _Offset);
}

FReply SGameHUD::OnSendMessageClicked()
{	
	if (SGameHUD::bAutoHideChat)
	{
		SGameHUD::ToggleChat(false);
	}

	if (SendMessageEvent.IsBound())
	{
		SendMessageEvent.Execute(ChatInput->GetText(), eSendMessageTo);
	}

	if (bAutoClearInput)
	{
		ChatInput->SetText(FText::GetEmpty());
	}

	return FReply::Handled();
}

void SGameHUD::OnSendMessage(const FText &_Message, ETextCommit::Type _Type)
{
	if (_Type == ETextCommit::Type::OnEnter)
	{
		if (SGameHUD::bAutoHideChat)
		{
			SGameHUD::ToggleChat(false);
		}

		if (SendMessageEvent.IsBound())
		{
			SendMessageEvent.Execute(_Message, eSendMessageTo);
		}

		if (bAutoClearInput)
		{
			ChatInput->SetText(FText::GetEmpty());
		}
	}	
}

void SGameHUD::OnSendMessageType(ECheckBoxState _State)
{
	if (_State == ECheckBoxState::Checked)
	{
		SGameHUD::SetMessageSendTo(ESendMessageTo::Team);
	}
	else
	{
		SGameHUD::SetMessageSendTo(ESendMessageTo::All);
	}	
}

FLinearColor SGameHUD::WidgetColor() const
{
	return FLinearColor(1, 1, 1, FMath::Lerp(0.0f, 1.0f, ShowColorHandle.GetLerp()));
}

void SGameHUD::ToggleWidget(const bool Toggle)
{
	LiveBar->ToggleWidget(Toggle);
	WeaponBar->ToggleWidget(Toggle);
	Widget_Ability->ToggleWidget(Toggle);

	FToggableWidgetHelper::ToggleWidget(AsShared(), ShowAnim, Toggle);
}

FSlateColor SGameHUD::RespawnCountColor() const
{
	return FSlateColor(FLinearColor(Style->RespawnCountColor.GetSpecifiedColor().CopyWithNewOpacity(RespawnCountHandle.GetLerp())));
}

FLinearColor SGameHUD::RespawnColor() const
{
	return FLinearColor(1, 1, 1, RespawnVisibleHandle.GetLerp());
}

void SGameHUD::SetBottomTextTime(const int32 &_Time)
{
	BottomTextTime = _Time;

	if (!BottomTextTime_Handle.IsValid())
	{
		BottomSwitcher->SetActiveWidgetIndex(0);
		RespawnVisibleAnim.Play(RespawnContainer->AsShared());

		BottomTextTime_Handle = RegisterActiveTimer(1.0f, FWidgetActiveTimerDelegate::CreateSP(this, &SGameHUD::OnBottomTextTime));
	}
}

EActiveTimerReturnType SGameHUD::OnBottomTextTime(double InCurrentTime, float InDeltaTime)
{
	EActiveTimerReturnType ReturnResult;

	if (BottomTextTime <= 1)
	{
		RespawnVisibleAnim.PlayReverse(RespawnContainer->AsShared());
		BottomTextTime_Handle.Reset();
		ReturnResult = EActiveTimerReturnType::Stop;
	}
	else 
	{		
		ReturnResult = EActiveTimerReturnType::Continue;
	}

	RespawnCounter->SetText(FText::AsNumber(BottomTextTime));

	--BottomTextTime;

	RespawnCountAnim.Play(RespawnCounter->AsShared());

	return ReturnResult;
}

void SGameHUD::ToggleBottomText(const bool Visible)
{
	if (Visible)
	{
		BottomSwitcher->SetActiveWidgetIndex(1);
		RespawnVisibleAnim.Play(this->AsShared());
	}
	else
	{
		if (!RespawnVisibleAnim.IsAtStart())
		{
			RespawnVisibleAnim.PlayReverse(this->AsShared());
		}
	}
}

void SGameHUD::SetBottomText(const FText &_Text, const bool IsTime)
{
	if (IsTime)
	{
		RespawnText->SetText(_Text);
	}
	else
	{
		BottomText->SetText(_Text);
	}
}

void SGameHUD::SetOnMessageSend(const FOnMessageSend &_Event)
{
	SendMessageEvent.Unbind();
	SendMessageEvent = _Event;
}

void SGameHUD::SetLiveBarFleshing(const bool _IsHealth)
{
	LiveBar->SetLiveBarFleshing(_IsHealth);
}

void SGameHUD::AppendAction(const EActionMessage MessageID, const int32& Score)
{
	if (Score != 0)
	{
		FText Message;
		FSlateColor Color(FColor::White.ReinterpretAsLinear());

		if (MessageID == EActionMessage::Kill)
		{
			if (Score > 0)
			{
				Message = NSLOCTEXT("SHUD", "Action.Kill", "Kill");
				Color = FColor::Yellow.ReinterpretAsLinear();
			}
			else
			{
				Message = NSLOCTEXT("SHUD", "Action.KillFriendly", "Kill Friendly");
				Color = FColor::Red.ReinterpretAsLinear();
			}
		}
		else if (MessageID == EActionMessage::Assist)
		{
			if (Score > 0)
			{
				Message = NSLOCTEXT("SHUD", "Action.Assist", "Assist");
				Color = FColor::Cyan.ReinterpretAsLinear();
			}
			else
			{
				Message = NSLOCTEXT("SHUD", "Action.AssistFriendly", "Assist Friendly");
				Color = FColor::Red.ReinterpretAsLinear();
			}
		}
		else if (MessageID == EActionMessage::InHead)
		{
			Message = NSLOCTEXT("SHUD", "Action.InHead", "To the head");
			Color = FColor::Green.ReinterpretAsLinear();
		}
		else if (MessageID == EActionMessage::Capture)
		{
			Message = NSLOCTEXT("SHUD", "Action.Capture", "Capture");
			Color = FColor::Green.ReinterpretAsLinear();
		}
		else if (MessageID == EActionMessage::TurretKill)
		{
			Message = NSLOCTEXT("SHUD", "Action.TurretKill", "Turret destruction");

			if (Score > 0)
			{
				Color = FColor::Green.ReinterpretAsLinear();
			}
			else
			{
				Color = FColor::Red.ReinterpretAsLinear();
			}
		}
		else if (MessageID == EActionMessage::AbilityKill)
		{
			Message = NSLOCTEXT("SHUD", "Action.AbilityKill", "Ability destruction");
			
			if (Score > 0)
			{
				Color = FColor::Green.ReinterpretAsLinear();
			}
			else
			{
				Color = FColor::Red.ReinterpretAsLinear();
			}
		}
		else if (MessageID == EActionMessage::AbilityAction)
		{
			Message = NSLOCTEXT("SHUD", "Action.AbilityAction", "Ability influence");

			if (Score > 0)
			{
				Color = FColor::Green.ReinterpretAsLinear();
			}
			else
			{
				Color = FColor::Red.ReinterpretAsLinear();
			}
		}	
		else if (MessageID == EActionMessage::BuildingKill)
		{
			Message = NSLOCTEXT("SHUD", "Action.BuildingKill", "Building destruction");

			if (Score > 0)
			{
				Color = FColor::Green.ReinterpretAsLinear();
			}
			else
			{
				Color = FColor::Red.ReinterpretAsLinear();
			}
		}
		else
		{
			Message = NSLOCTEXT("SHUD", "Action.Death", "Death");
		}

		ActionsContainer->AddSlot()
			[
				SNew(SSimpleChatRow)
				.IsSimple(true)
				.IsAction(true)
				.Message(Message)
				.ActionValue(Score)
				.PlayerColor(Color)
			];

		ActionsContainer->ScrollToEnd();
	}
}

void SGameHUD::SetCharacter(const TAttribute<AUTCharacter*>& InCharacter)
{
	Character = InCharacter;

	LiveBar->SetCharacter(Character);
	WeaponBar->SetCharacter(Character.Get());

	if (Character.Get() && Character.Get()->IsValidLowLevel())
	{
		Widget_Ability->Init(Character.Get()->CharacterAbility);
	}
}

int32 SGameHUD::OnPaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyClippingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const
{
	//if (Controller && Controller->GetPawn())
	//{
	//	if (auto MyPawn = Cast<AUTCharacter>(Controller->GetPawn()))
	//	{
	//		//FVector ScreenLocation3D;
	//		//FVector2D ScreenLocation, ScreenSize;
	//		//UWidgetLayoutLibrary::ProjectWorldLocationToWidgetPositionWithDistance(Controller, MyPawn->VectorField->GetComponentLocation(), ScreenLocation3D);

	//		//ScreenLocation = FVector2D(ScreenLocation3D.X, ScreenLocation3D.Y);

	//		//ScreenSize = UWidgetLayoutLibrary::GetViewportSize(Controller->GetWorld()) / UWidgetLayoutLibrary::GetViewportScale(Controller->GetWorld());
	//		//ScreenLocation -= ScreenSize / 2;
	//		//ScreenLocation.Y *= -1.0f;

	//		//auto Trans = FSlateRenderTransform(ScreenLocation.ClampAxes(-100.0f, 100.0f));

	//		//LiveBar->SetRenderTransform(Trans);
	//		//WeaponBar->SetRenderTransform(Trans);
	//	}
	//}

	return SCompoundWidget::OnPaint(Args, AllottedGeometry, MyClippingRect, OutDrawElements, LayerId, InWidgetStyle, bParentEnabled);
}

bool SGameHUD::IsInInteractiveMode() const
{
	return ChatContainer->GetVisibility() == EVisibility::Visible;
}