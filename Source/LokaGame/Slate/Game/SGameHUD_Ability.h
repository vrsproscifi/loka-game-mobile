// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/GameHUDWidgetStyle.h"

class ACharacterAbility;

class LOKAGAME_API SGameHUD_Ability 
	: public SCompoundWidget
	, public FGCObject
{
public:
	SLATE_BEGIN_ARGS(SGameHUD_Ability)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FGameHUDStyle>(TEXT("SGameHUDStyle")))
	{
		_Visibility = EVisibility::HitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FGameHUDStyle, Style)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);
	void Init(const TWeakObjectPtr<ACharacterAbility>& InAbility);

	void ToggleWidget(const bool Toggle);

protected:

	const FGameHUDStyle* Style;
	UMaterialInstanceDynamic* MID;
	FSlateBrush MIDBrush;
	float IsReverse;
	float TimeAfterShowing;
	bool bHiddenTip;
	TWeakObjectPtr<ACharacterAbility> Ability;
	TSharedPtr<class SAnimatedBackground> AnimatedBackground[3];

	EVisibility GetAbilityVisibility() const;
	FText GetAbilityName() const;
	FText GetAbilityDescription() const;
	FKey GetAbilityKey() const;
	const FSlateBrush* GetAbilityBrush() const;

	virtual void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;
	virtual void AddReferencedObjects(FReferenceCollector& Collector) override;
};
