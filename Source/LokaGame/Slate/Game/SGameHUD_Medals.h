// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/GameHUDWidgetStyle.h"

class LOKAGAME_API SGameHUD_Medals : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SGameHUD_Medals)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FGameHUDStyle>(TEXT("SGameHUDStyle")))
	{
		_Visibility = EVisibility::HitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FGameHUDStyle, Style)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);
	void AddMedal(const FSlateBrush*, const FText&, const FSlateSound* = nullptr);

protected:

	const FGameHUDStyle* Style;

	TArray<TSharedPtr<class SGameHUD_Medal>> QueueMedals;
	void OnRemoveMedal();
};
