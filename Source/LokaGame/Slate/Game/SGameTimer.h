// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SlateBasics.h"
#include "Styles/GameTimerWidgetStyle.h"

class AUTGameState;
class AUTPlayerState;

class LOKAGAME_API SGameTimer : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SGameTimer)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FGameTimerStyle>("SGameTimerStyle"))
		, _AnimColorDuration(1.5f)
		, _AnimTransDuration(1.5f)
	{}
	SLATE_STYLE_ARGUMENT(FGameTimerStyle, Style)
	SLATE_ARGUMENT(float, AnimColorDuration)
	SLATE_ARGUMENT(float, AnimTransDuration)
	SLATE_ATTRIBUTE(AUTGameState*, GameState)
	SLATE_ATTRIBUTE(AUTPlayerState*, LocalPlayerState)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);
	void ToggleWidget(const bool _Toggle);

	TSharedPtr<STextBlock> LeftScore;
	TSharedPtr<STextBlock> RightScore;

protected:
	const FGameTimerStyle *Style;
	TSharedPtr<STextBlock> TimeText;

	TAttribute<AUTGameState*> GameState;
	TAttribute<AUTPlayerState*> LocalPlayerState;

	TSharedPtr<class SAnimatedBackground> Widget_FxBackground;

	FText GetTimeText() const;
	TOptional<float> GetScorePercent(const bool) const;
	TOptional<float> GetTimePercent() const;
	FSlateColor GetLifeColor(const int32) const;

	EActiveTimerReturnType UpdateLastHeroData(double InCurrentTime, float InDeltaTime);

	int32 CachedPlace, CachedAlive;

	FString sLife;

	//===================================================[ Visibility by timer module (P.S. Need refactoring as UTHUD_Widgets for Slate)
	EVisibility GetVisibility_Scores() const;
	EVisibility GetVisibility_Lifes() const;
	EVisibility GetVisibility_NumberLifes() const;
	EVisibility GetVisibility_LasHero() const;
	EVisibility GetVisibility_Waves() const;

	//===================================================[ Parameters

	FSlateColor GetNumberLifesColor() const;
	FText GetNumberLifesText() const;
};
