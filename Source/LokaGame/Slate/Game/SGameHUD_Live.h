// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Styles/GameHUDWidgetStyle.h"
#include "Weapon/WeaponContainer.h"

class AUTCharacter;

class LOKAGAME_API SGameHUD_Live : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SGameHUD_Live)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FGameHUDStyle>(TEXT("SGameHUDStyle")))
	{
		_Visibility = EVisibility::HitTestInvisible;
	}
	SLATE_ATTRIBUTE(AUTCharacter*, Character)
	SLATE_STYLE_ARGUMENT(FGameHUDStyle, Style)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void SetCharacter(const TAttribute<AUTCharacter*>&);

	void SetLiveBarFleshing(const bool IsHealth);
	void ToggleWidget(const bool);

protected:

	const FGameHUDStyle *Style;

	TAttribute<AUTCharacter*> Character;

	FCurveSequence ShowAnim;
	FCurveHandle ShowColorHandle;

	FCurveSequence LowHealthAnim;
	FCurveHandle LowHealthHandle;

	FCurveSequence LowArmorAnim;
	FCurveHandle LowArmorHandle;

	TSharedPtr<SProgressBar> HealthProgress, ArmorProgress;
	TSharedPtr<STextBlock> HealthText, ArmorText;

	TOptional<float> GetBarPercent(const bool) const;
	FText GetBarValueAsText(const bool) const;

	FLinearColor WidgetColor() const
	{
		return FLinearColor(1, 1, 1, FMath::Lerp(0.0f, 1.0f, ShowColorHandle.GetLerp()));
	}

	FSlateColor HealthColor() const
	{
		return FMath::Lerp(Style->ColorHealth.GetSpecifiedColor(), FLinearColor::Red, LowHealthHandle.GetLerp());
	}

	FSlateColor ArmorColor() const
	{
		return FMath::Lerp(Style->ColorArmor.GetSpecifiedColor(), FLinearColor::Red, LowArmorHandle.GetLerp());
	}
};
