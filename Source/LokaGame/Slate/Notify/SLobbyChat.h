// VRSPRO

#pragma once


#include "Windows/SWindowBase.h"
#include "Styles/LobbyChatWidgetStyle.h"
#include "ChatComponent/ChatChanelModel.h"

class STabbedContainer;
struct FChatMessageReciveModel;
struct FChatChanelModel;
class UChatComponent;

class LOKAGAME_API SLobbyChat : public SWindowBase
{
public:
	SLATE_BEGIN_ARGS(SLobbyChat)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FLobbyChatStyle>("SLobbyChatStyle"))
	{
		_Visibility = EVisibility::Collapsed;
	}
	SLATE_STYLE_ARGUMENT(FLobbyChatStyle, Style)
	SLATE_ARGUMENT(UChatComponent*, ChatComponent)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs, TSharedRef<SWindowsContainer> InOwner);
	
	void AppendMessage(const FChatMessageReciveModel& Message);

protected:

	const FLobbyChatStyle *Style;

	TWeakObjectPtr<UChatComponent> ChatComponent;

	TSharedPtr<STabbedContainer> MessagesSwitcher;
	TSharedPtr<SScrollBox> MessagesContainer[EChatChanelGroup::End];
	TSharedPtr<SScrollBar> MessagesContainerScroll[EChatChanelGroup::End];

	TSharedPtr<SEditableTextBox> MessageInput;
	void OnMessageCommitted(const FText&, ETextCommit::Type);
	
	void OnUpdateChannels(const TArray<FChatChanelModel>& InChannels);
	void OnUpdateMessages(const TArray<FChatMessageReciveModel>& InMessages);
	
	FGuid CurrentChannel;
	TMap<FLokaGuid, FChatChanelModel> ChanneslMap;

	FLokaGuid CurrentPlayerId;
	void OpenContextMenu(const FString& Name);

	
};
