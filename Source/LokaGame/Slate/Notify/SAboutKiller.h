// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/AboutKillerWidgetStyle.h"
/**
 * 
 */
class LOKAGAME_API SAboutKiller : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SAboutKiller)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FAboutKillerStyle>("SAboutKillerStyle"))
	{
		_Visibility = EVisibility::HitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FAboutKillerStyle, Style)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

	void ShowAboutKiller(const FText&, const FSlateBrush*, const int32& = 6);

protected:

	TSharedPtr<SImage> AboutImage;
	TSharedPtr<STextBlock> AboutText;

	TSharedPtr<class SAnimatedBackground> AnimatedBackground[2];

	const FAboutKillerStyle* Style;

	EActiveTimerReturnType OnReverse(const double InCurrentTime, const float InDeltaTime);
};
