// VRSPRO

#include "LokaGame.h"
#include "SMessageBox.h"
#include "SBackgroundBlur.h"
#include "Styles/BackgroundBlurWidgetStyle.h"
#include "SlateOptMacros.h"

#include "ToggableWidgetHelper.h"

#include "Components/STextBlockButton.h"
#include "Components/SAnimatedBackground.h"

#include "Decorators/LokaTextDecorator.h"
#include "Decorators/LokaResourceDecorator.h"
#include "Decorators/LokaKeyDecorator.h"
#include "Decorators/LokaSliderNumberDecorator.h"

// For configuration support;
#include "Widget.h"
#include "Decorators/LokaSpaceDecorator.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SMessageBox::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	HeadTextAlignment = ETextJustify::Center;
	IsEnabledClose = false;
	IsAutoUnbind_OnMessageBoxButton = true;
	IsAutoUnbind_OnMessageBoxHidden = true;
	IsAutoUnbind_OnInt32ValueChanged = true;

	IsOnceClickButton[EMessageBoxButton::Left] = false;
	IsOnceClickButton[EMessageBoxButton::Middle] = false;
	IsOnceClickButton[EMessageBoxButton::Right] = false;

	BackgroundBlurStyle = &FLokaStyle::Get().GetWidgetStyle<FBackgroundBlurStyle>("Back_BackgroundBlur");

	MAKE_UTF8_SYMBOL(sClose, 0xf00d);

	ChildSlot
	[
		SNew(SBackgroundBlur)
		.Padding(0)
		.HAlign(HAlign_Fill)
		.VAlign(VAlign_Fill)
		.bApplyAlphaToBlur(BackgroundBlurStyle->bApplyAlphaToBlur)
		.BlurRadius(this, &SMessageBox::GetBlurRadius)
		.LowQualityFallbackBrush(&BackgroundBlurStyle->LowQualityFallbackBrush)
		.BlurStrength(this, &SMessageBox::GetBlurStrength)
		.Visibility(EVisibility::SelfHitTestInvisible)
		[
			SNew(SBorder)			
			.BorderImage(new FSlateColorBrush(FColor::White))
			.BorderBackgroundColor(this, &SMessageBox::GetBackColor)
			.Visibility(this, &SMessageBox::GetBackVisibility)
			.OnMouseButtonDown_Lambda([&](const FGeometry&, const FPointerEvent&) {
				if (IsEnabledClose && Widget_AnimClose.IsValid() && Widget_AnimClose->IsAnimAtEnd()) ToggleWidget(false);
				return FReply::Handled();
			})
			[
				SNew(SOverlay)
				+ SOverlay::Slot()
				.HAlign(HAlign_Center)
				.VAlign(VAlign_Center)
				[
					SAssignNew(Widget_AnimBox, SAnimatedBackground)
					.ShowAnimation(EAnimBackAnimation::DownFade)
					.HideAnimation(EAnimBackAnimation::UpFade)
					.Duration(Style->Box.Duration)
					.InitAsHide(true)
					[
						SNew(SBox)
						.MinDesiredWidth(this, &SMessageBox::GetBoxSizeParameter, ESizeParameter::MinDesiredWidth) //Style->Box.MinSize.X)
						.MaxDesiredWidth(this, &SMessageBox::GetBoxSizeParameter, ESizeParameter::MaxDesiredWidth) //(Style->Box.MaxSize.X)
						.MinDesiredHeight(this, &SMessageBox::GetBoxSizeParameter, ESizeParameter::MinDesiredHeight) //(Style->Box.MinSize.Y)
						.MaxDesiredHeight(this, &SMessageBox::GetBoxSizeParameter, ESizeParameter::MaxDesiredHeight) //(Style->Box.MaxSize.Y)
						[
							SNew(SBorder)
							.BorderImage(&Style->Box.Background)
							.Padding(0)
							.Visibility(EVisibility::Visible)
							.OnMouseButtonDown_Lambda([&](const FGeometry&, const FPointerEvent&) {
								return FReply::Handled();
							})
							[							
								SNew(SVerticalBox)
								+ SVerticalBox::Slot().AutoHeight() //Window header and close button
								[
									SNew(SBox).MinDesiredHeight(40)
									[
										SNew(SBorder)
										.BorderImage(&Style->Head.Background)
										.Padding(FMargin(20, 2))
										.VAlign(VAlign_Center)
										[
											SAssignNew(Widget_HeadText, SRichTextBlock)
											.TextStyle(&Style->Head.Text)
											.Justification(this, &SMessageBox::GetHeadTextAlignment)
											.DecoratorStyleSet(&FLokaStyle::Get())
											+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(Style->Head.Text))
											+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
											+ SRichTextBlock::Decorator(FLokaKeyDecorator::Create())
										]
									]
								]
								+ SVerticalBox::Slot().AutoHeight().Padding(2) //Split line
								[
									SNew(SSeparator)
									.SeparatorImage(&Style->Separator.SeparatorImage)
									.Orientation(Style->Separator.Orientation)
									.ColorAndOpacity(Style->Separator.ColorAndOpacity)
									.Thickness(Style->Separator.Thickness)
								]
								+ SVerticalBox::Slot().FillHeight(1).Padding(2).Expose(ContentSlot) //Content
								+ SVerticalBox::Slot().AutoHeight().Padding(8) //Split line
								[
									SNew(SSeparator)
									.SeparatorImage(&Style->Separator.SeparatorImage)
									.Orientation(Style->Separator.Orientation)
									.ColorAndOpacity(Style->Separator.ColorAndOpacity)
									.Thickness(Style->Separator.Thickness)
								]
								+ SVerticalBox::Slot().AutoHeight().Padding(2) //Buttons
								[
									SNew(SHorizontalBox)
									+ SHorizontalBox::Slot().HAlign(HAlign_Center)
									[
										SAssignNew(Widget_Button[EMessageBoxButton::Left], SButton)
										.OnClicked(this, &SMessageBox::OnClickedButtonEvent, EMessageBoxButton::Left)
										.ButtonStyle(&Style->Buttons.Button)
										.HAlign(HAlign_Center)
										.VAlign(VAlign_Center)
										.ContentPadding(FMargin(10, 6))
										[
											SAssignNew(Widget_ButtonText[EMessageBoxButton::Left], STextBlock)
											.TextStyle(&Style->Buttons.Text)
											.Justification(ETextJustify::Center)
										]
									]
									+ SHorizontalBox::Slot().HAlign(HAlign_Center)
									[
										SAssignNew(Widget_Button[EMessageBoxButton::Middle], SButton)
										.OnClicked(this, &SMessageBox::OnClickedButtonEvent, EMessageBoxButton::Middle)
										.ButtonStyle(&Style->Buttons.Button)
										.HAlign(HAlign_Center)
										.VAlign(VAlign_Center)
										.ContentPadding(FMargin(10, 6))
										[
											SAssignNew(Widget_ButtonText[EMessageBoxButton::Middle], STextBlock)
											.TextStyle(&Style->Buttons.Text)
											.Justification(ETextJustify::Center)
										]
									]
									+ SHorizontalBox::Slot().HAlign(HAlign_Center)
									[
										SAssignNew(Widget_Button[EMessageBoxButton::Right], SButton)
										.OnClicked(this, &SMessageBox::OnClickedButtonEvent, EMessageBoxButton::Right)
										.ButtonStyle(&Style->Buttons.Button)
										.HAlign(HAlign_Center)
										.VAlign(VAlign_Center)
										.ContentPadding(FMargin(10, 6))
										[
											SAssignNew(Widget_ButtonText[EMessageBoxButton::Right], STextBlock)
											.TextStyle(&Style->Buttons.Text)
											.Justification(ETextJustify::Center)
										]
									]
								]
							]
						]
					]
				]
				+ SOverlay::Slot().HAlign(HAlign_Right).VAlign(VAlign_Bottom).Padding(50, 50)
				[
					SAssignNew(Widget_AnimClose, SAnimatedBackground)
					.InitAsHide(true)
					.IsEnabledBlur(false)
					.Duration(0.65f)
					.IsRelative(true)
					.WithColor(true)
					.ShowAnimation(EAnimBackAnimation::RightFade)
					.HideAnimation(EAnimBackAnimation::RightFade)
					[
						SNew(STextBlock)
						.TextStyle(&Style->Back.CloseFont)
						.Text(NSLOCTEXT("All", "All.Close", "Close"))
					]
				]
			]
		]
	];

	Anim_Back = FCurveSequence();
	Handle_Back = Anim_Back.AddCurve(.0f, Style->Back.Duration, ECurveEaseFunction::QuadOut);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FReply SMessageBox::OnClickedButtonEvent(const EMessageBoxButton Button) 
{
	if (Button < EMessageBoxButton::End && IsOnceClickButton[Button])
	{
		Widget_Button[Button]->SetEnabled(false);
	}

	if (OnMessageBoxButton.IsBound()) OnMessageBoxButton.Broadcast(Button);
	if (OnMessageBoxDynamicButton.IsBound()) OnMessageBoxDynamicButton.ExecuteIfBound(Button);
	return FReply::Handled();
}

EVisibility SMessageBox::GetBackVisibility() const
{
	return (Handle_Back.GetLerp() > .0f) ? EVisibility::Visible : EVisibility::SelfHitTestInvisible;
}

FSlateColor SMessageBox::GetBackColor() const
{
	return FSlateColor(FLinearColor(Style->Back.Color.R, Style->Back.Color.G, Style->Back.Color.B, FMath::Lerp(.0f, Style->Back.Color.A, Handle_Back.GetLerp())));
}

float SMessageBox::GetBlurStrength() const
{
	return FMath::Lerp(.0f, BackgroundBlurStyle ? BackgroundBlurStyle->BlurStrength : 4.0f, Handle_Back.GetLerp());
}

TOptional<int32> SMessageBox::GetBlurRadius() const
{
	return FMath::Lerp(0, BackgroundBlurStyle ? BackgroundBlurStyle->BlurRadius : 8, Handle_Back.GetLerp());
}

ETextJustify::Type SMessageBox::GetHeadTextAlignment() const
{
	return HeadTextAlignment;
}

void SMessageBox::SetSound(const FSlateSound& InSound, const bool IsShowSound)
{
	if (IsShowSound)
	{
		ShowSound = InSound;
	}
	else
	{
		HideSound = InSound;
	}
}

void SMessageBox::ToggleWidget(const bool toggle)
{
	Widget_AnimBox->ToggleWidget(toggle);
	FToggableWidgetHelper::ToggleWidget(AsShared(), Anim_Back, toggle);	

	FSlateApplication::Get().PlaySound(toggle ? ShowSound : HideSound);

	if (toggle == false)
	{
		if (IsAutoUnbind_OnMessageBoxButton)
		{
			OnMessageBoxButton.Clear();
			OnMessageBoxDynamicButton.Clear();
		}
		if (IsAutoUnbind_OnInt32ValueChanged) OnInt32ValueChanged.Unbind();

		RegisterActiveTimer((Style->Back.Duration > Style->Box.Duration) ? Style->Back.Duration : Style->Box.Duration, FWidgetActiveTimerDelegate::CreateLambda([&](double InCurrentTime, float InDeltaTime) {
			if (OnMessageBoxHidden.IsBound()) OnMessageBoxHidden.Broadcast();
			if (OnMessageBoxDynamicHidden.IsBound()) OnMessageBoxDynamicHidden.ExecuteIfBound();
			OnToggleInteractive.ExecuteIfBound(false);

			if (QueueId != NAME_None)
			{
				auto CurrentName = QueueId;
				QueueId = NAME_None;
				SMessageBox::RemoveQueueMessage(CurrentName);
			}			

			if (IsAutoUnbind_OnMessageBoxHidden)
			{
				OnMessageBoxHidden.Clear();
				OnMessageBoxDynamicHidden.Clear();
			}

			ShowSound = Style->ShowSound;
			HideSound = Style->HideSound;

			return EActiveTimerReturnType::Stop;
		}));

		Widget_AnimClose->ToggleWidget(false);
	}

	if (toggle)
	{
		ResetOnceClickButtons();
		OnToggleInteractive.ExecuteIfBound(true);
	}
}

void SMessageBox::SetButtonText(const EMessageBoxButton Target, const FText& Text)
{
	if (Target < EMessageBoxButton::End)
	{
		Widget_Button[Target]->SetVisibility(Text.IsEmptyOrWhitespace() ? EVisibility::Collapsed : EVisibility::Visible);
		Widget_ButtonText[Target]->SetText(Text);
	}
}

void SMessageBox::SetButtonsText(const FText& InTextLeft, const FText& InTextMiddle, const FText& InTextRight)
{
	SetButtonText(EMessageBoxButton::Left, InTextLeft);
	SetButtonText(EMessageBoxButton::Middle, InTextMiddle);
	SetButtonText(EMessageBoxButton::Right, InTextRight);
}

void SMessageBox::SetHeaderText(const FText& Text)
{
	Widget_HeadText->SetText(Text);
}

void SMessageBox::SetContent(const FText& Text, const ETextJustify::Type TextJustify)
{
	BoxSize = FBox2D(Style->Box.MinSize, Style->Box.MaxSize);
	ResetOnceClickButtons();

	TMap<FName, FSlateFontInfo> Fonts;

	auto Widget = SNew(SRichTextBlock)
		.Text(Text)
		.AutoWrapText(true)
		.TextStyle(&Style->Content.Text)
		.Justification(TextJustify)
		.DecoratorStyleSet(&FLokaStyle::Get())
		+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(Fonts, Style->Content.Text.Font, Style->Content.Text.ColorAndOpacity.GetSpecifiedColor()))
		+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
		+ SRichTextBlock::Decorator(FLokaKeyDecorator::Create())
		+ SRichTextBlock::Decorator(FLokaSpaceDecorator::Create())
		+ SRichTextBlock::Decorator(FLokaSliderNumberDecorator::Create(OnInt32ValueChanged))
		+ SRichTextBlock::Decorator(FHyperlinkDecorator::Create(TEXT(""), FSlateHyperlinkRun::FOnClick::CreateLambda([&](const FSlateHyperlinkRun::FMetadata& InData) {
			if (InData.Contains("url")) FPlatformProcess::LaunchURL(*InData.FindChecked("url"), nullptr, nullptr);
		})));

	if (Text.ToString().Len() >= 800)
	{
		ContentSlot->AttachWidget(
			SNew(SScrollBox)
			.Orientation(EOrientation::Orient_Vertical)
			.ScrollBarStyle(&FLokaStyle::Get().GetWidgetStyle<FScrollBarStyle>("Default_ScrollBar")).ScrollBarThickness(FVector2D(4, 4))
			+ SScrollBox::Slot()
			[
				Widget
			]
		);
	}
	else
	{
		ContentSlot->AttachWidget(Widget);
	}

	ContentSlot->Padding(FMargin(20, 10));
}

void SMessageBox::SetContent(TSharedPtr<SWidget> Widget)
{
	ResetOnceClickButtons();
	BoxSize = FBox2D(Style->Box.MinSize, Style->Box.MaxSize);
	ContentSlot->AttachWidget(Widget.ToSharedRef());
}

void SMessageBox::SetEnableClose(const bool IsEnable)
{
	if (IsEnabledClose != IsEnable && IsEnable == false)
	{
		Widget_AnimClose->ToggleWidget(false);
	}
	IsEnabledClose = IsEnable;
}

void SMessageBox::SetFromCfg(const UMessageBoxCfg* Cfg)
{
	if (Cfg)
	{
		SetFromCfg(Cfg->Message);
	}
}

void SMessageBox::SetFromCfg(const FMessageBoxMsg& InCfg)
{
	SetHeaderText(InCfg.Title);

	if (InCfg.Widget)
	{
		SetContent(InCfg.Widget->TakeWidget());
	}
	else
	{
		SetContent(InCfg.Message);
	}

	if (InCfg.SizeLimit.bIsValid)
	{
		SetAllowedSize(InCfg.SizeLimit);
	}

	SetButtonText(EMessageBoxButton::Left, InCfg.ButtonLeft);
	SetButtonText(EMessageBoxButton::Middle, InCfg.ButtonMiddle);
	SetButtonText(EMessageBoxButton::Right, InCfg.ButtonRight);
	SetEnableClose(InCfg.IsAllowClose);

	OnMessageBoxDynamicButton = InCfg.OnMessageBoxButton;
	OnMessageBoxDynamicHidden = InCfg.OnMessageBoxHidden;
}

bool SMessageBox::AddQueueMessage(const FName& Index, const FOnClickedOutside& Event)
{
	return AddQueue(Index, Event);
}

bool SMessageBox::RemoveQueueMessage(const FName& Index)
{
	return RemoveQueue(Index);
}

void SMessageBox::ResetQueueMessages()
{
	ResetQueue();
}

void SMessageBox::SetAllowedSize(const FBox2D& InSize)
{
	BoxSize = InSize;
}

FOptionalSize SMessageBox::GetBoxSizeParameter(const ESizeParameter InParameter) const
{
	switch (InParameter)
	{
		case ESizeParameter::MinDesiredWidth: return BoxSize.Min.X;
		case ESizeParameter::MaxDesiredWidth: return BoxSize.Max.X;
		case ESizeParameter::MinDesiredHeight: return BoxSize.Min.Y;
		case ESizeParameter::MaxDesiredHeight: return BoxSize.Max.Y;
	}

	return .0f;
}

FReply SMessageBox::OnMouseMove(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
	if (IsEnabledClose && Anim_Back.IsAtEnd())
	{
		Widget_AnimClose->ToggleWidget(!Widget_AnimBox->IsHovered());
	}

	return SUsableCompoundWidget::OnMouseMove(MyGeometry, MouseEvent);
}

void SMessageBox::SetIsOnceClickButton(const EMessageBoxButton InButton, const bool InToggle)
{
	if (InButton < EMessageBoxButton::End)
	{
		if (IsOnceClickButton[InButton] != InToggle && InToggle == false)
		{
			Widget_Button[InButton]->SetEnabled(true);
		}

		IsOnceClickButton[InButton] = InToggle;
	}
}

void SMessageBox::SetIsOnceClickButtons(const bool InToggle)
{
	SetIsOnceClickButton(EMessageBoxButton::Left, InToggle);
	SetIsOnceClickButton(EMessageBoxButton::Middle, InToggle);
	SetIsOnceClickButton(EMessageBoxButton::Right, InToggle);
}

void SMessageBox::ResetOnceClickButton(const EMessageBoxButton InButton)
{
	SetIsOnceClickButton(InButton, false);
}

void SMessageBox::ResetOnceClickButtons()
{
	ResetOnceClickButton(EMessageBoxButton::Left);
	ResetOnceClickButton(EMessageBoxButton::Middle);
	ResetOnceClickButton(EMessageBoxButton::Right);
}