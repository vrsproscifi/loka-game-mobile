// VRSPRO

#pragma once

#include "MessageBoxCfg.h"
#include "Widgets/SCompoundWidget.h"
#include "Styles/MessageBoxWidgetStyle.h"
#include "Utilities/SUsableCompoundWidget.h"
#include "Utilities/SlateUtilityTypes.h"

struct FBackgroundBlurStyle;
typedef MessageBoxButton::Type EMessageBoxButton;

DECLARE_MULTICAST_DELEGATE(FOnMessageBoxNativeHidden);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnMessageBoxNativeButton, const EMessageBoxButton&);

#define QueueMessageBegin(Index, ...) QueueBegin(SMessageBox, Index, ##__VA_ARGS__)
#define QueueMessageEnd QueueEnd

class LOKAGAME_API SMessageBox 
	: public SUsableCompoundWidget
	, public TStaticSlateWidget<SMessageBox>
	, public TWidgetSupportQueue<SMessageBox>
{
public:
	SLATE_BEGIN_ARGS(SMessageBox)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FMessageBoxStyle>("SMessageBoxStyle"))
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FMessageBoxStyle, Style)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	virtual void ToggleWidget(const bool) override;
	virtual bool IsInInteractiveMode() const override { return Anim_Back.IsAtStart() == false; }

	void SetButtonText(const EMessageBoxButton, const FText& = FText::GetEmpty());
	void SetButtonsText(const FText& = FText::GetEmpty(), const FText& = FText::GetEmpty(), const FText& = FText::GetEmpty());
	void SetHeaderText(const FText&);

	void SetContent(const FText&, const ETextJustify::Type = ETextJustify::Center);
	void SetContent(TSharedPtr<SWidget>);

	// Use after SetContent if need, SetContent reset this to default
	void SetAllowedSize(const FBox2D&);

	void SetEnableClose(const bool);
	void SetSound(const FSlateSound& InSound, const bool IsShowSound = true);

	void SetIsOnceClickButton(const EMessageBoxButton, const bool);
	void SetIsOnceClickButtons(const bool);
	void ResetOnceClickButton(const EMessageBoxButton);
	void ResetOnceClickButtons();

	void SetFromCfg(const UMessageBoxCfg*);
	void SetFromCfg(const FMessageBoxMsg&);

	FOnMessageBoxButton OnMessageBoxDynamicButton;
	FOnMessageBoxHidden OnMessageBoxDynamicHidden;

	FOnMessageBoxNativeHidden OnMessageBoxHidden;
	FOnMessageBoxNativeButton OnMessageBoxButton;

	FOnInt32ValueChanged OnInt32ValueChanged;

	static bool AddQueueMessage(const FName&, const FOnClickedOutside&);
	static bool RemoveQueueMessage(const FName&);
	static void ResetQueueMessages();

protected:

	TSharedPtr<SRichTextBlock>  Widget_HeadText;

	TSharedPtr<class SAnimatedBackground> Widget_AnimBox, Widget_AnimClose;

	TSharedPtr<SButton> Widget_Button[MessageBoxButton::Type::End];
	TSharedPtr<STextBlock> Widget_ButtonText[MessageBoxButton::Type::End];

	SVerticalBox::FSlot* ContentSlot;

	const FMessageBoxStyle* Style;
	const FBackgroundBlurStyle* BackgroundBlurStyle;

	FCurveSequence Anim_Back;
	FCurveHandle Handle_Back;

	FReply OnClickedButtonEvent(const EMessageBoxButton);
	virtual FReply OnMouseMove(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;

	EVisibility GetBackVisibility() const;
	FSlateColor GetBackColor() const;
	ETextJustify::Type GetHeadTextAlignment() const;
	float GetBlurStrength() const;
	TOptional<int32> GetBlurRadius() const;

	// Storage variables
	ETextJustify::Type HeadTextAlignment;

	bool IsEnabledClose
		, IsAutoUnbind_OnMessageBoxButton
		, IsAutoUnbind_OnMessageBoxHidden
		, IsAutoUnbind_OnInt32ValueChanged;

	enum ESizeParameter
	{
		MinDesiredWidth,
		MaxDesiredWidth,
		MinDesiredHeight,
		MaxDesiredHeight
	};

	FOptionalSize GetBoxSizeParameter(const ESizeParameter) const;

	FBox2D BoxSize;
	FSlateSound ShowSound, HideSound;

	bool IsOnceClickButton[MessageBoxButton::Type::End];
};
