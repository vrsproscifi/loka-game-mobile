// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SMesBoxContentEarlyNotify.h"
#include "SlateOptMacros.h"

#include "GameVersion.h"

#include "Decorators/LokaTextDecorator.h"
#include "Decorators/LokaResourceDecorator.h"
#include "Decorators/LokaKeyDecorator.h"
#include "Decorators/LokaSpaceDecorator.h"
#include "Styles/MessageBoxWidgetStyle.h"

#include "Localization/BaseStringsTable.h"

typedef TSharedPtr<FLokaTestingGraph> LTGraph;

class SLTGraph : public SMultiColumnTableRow<LTGraph>
{
	typedef SMultiColumnTableRow<LTGraph> SSuper;

public:
	SLATE_BEGIN_ARGS(SLTGraph)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FContentEarlyNotifyStyle>("SContentEarlyNotifyStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FContentEarlyNotifyStyle, Style)
	SLATE_END_ARGS()

	BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
	void Construct(const FArguments& InArgs, const TSharedRef<STableViewBase>& OwnerTable, LTGraph Item)
	{
		Style = InArgs._Style;
		Instance = Item;

		SSuper::Construct(SSuper::FArguments().Padding(FMargin(2, 6)).Style(&Style->TableRow), OwnerTable);
	}
	END_SLATE_FUNCTION_BUILD_OPTIMIZATION

	virtual TSharedRef<SWidget> GenerateWidgetForColumn(const FName& InColumnName) override
	{
		if (InColumnName == TEXT("Day"))
		{
			return SNew(STextBlock)
			.Text(FLokaBaseStringsTable::GetDayOfWeek(Instance->OpenTime.GetDayOfWeek()))
			.TextStyle(&Style->TableRowText);
		}
		else if (InColumnName == TEXT("Time"))
		{
			return SNew(STextBlock)
			.Text(FText::Format(FText::FromString("{0}-{1}"), FText::AsTime(Instance->OpenTime, EDateTimeStyle::Short), FText::AsTime(Instance->CloseTime, EDateTimeStyle::Short)))
			.TextStyle(&Style->TableRowText);
		}
		else if (InColumnName == TEXT("Target"))
		{
			return SNew(STextBlock)
			.AutoWrapText(true)
			.Text(Instance->Target)
			.TextStyle(&Style->TableRowText);
		}
		//
		return SNullWidget::NullWidget;
	}

protected:

	const FContentEarlyNotifyStyle* Style;
	LTGraph Instance;
};

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SMesBoxContentEarlyNotify::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	auto MessageBoxStyle = &FLokaStyle::Get().GetWidgetStyle<FMessageBoxStyle>("SMessageBoxStyle");

	auto aGraph = new TArray<LTGraph>();
	
	for (auto s : Style->TestTimes)
	{
		aGraph->Add(MakeShareable(new FLokaTestingGraph(s)));
	}

	ChildSlot
	[
		SNew(SVerticalBox)
		+ SVerticalBox::Slot().AutoHeight() // Current Version
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot().HAlign(HAlign_Left)
			[
				SNew(STextBlock).Text(NSLOCTEXT("EarlyNotify", "EarlyNotify.CurrentVer", "Current version of game"))
				.TextStyle(&Style->VersionDescText)
			]
			+ SHorizontalBox::Slot().HAlign(HAlign_Right)
			[
				SNew(STextBlock).Text(FText::FromString(GAME_VERSION_STRING))
				.TextStyle(&Style->VersionNumText)
			]
		]
		+ SVerticalBox::Slot().AutoHeight().Padding(0, 2, 0, 10) //Split line
		[
			SNew(SSeparator)
			.SeparatorImage(&MessageBoxStyle->Separator.SeparatorImage)
			.Orientation(MessageBoxStyle->Separator.Orientation)
			.ColorAndOpacity(MessageBoxStyle->Separator.ColorAndOpacity)
			.Thickness(MessageBoxStyle->Separator.Thickness)
		]
		+ SVerticalBox::Slot().AutoHeight() // Graph description & test days
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot().AutoHeight().Padding(0, 2)
				[
					SNew(SRichTextBlock)
					.Text(Style->TinyDesc)
					.AutoWrapText(true)
					.TextStyle(&FLokaStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_TextBlock"))
					.Justification(ETextJustify::Center)
					.DecoratorStyleSet(&FLokaStyle::Get())
					+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(FLokaStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_TextBlock")))
					+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
					+ SRichTextBlock::Decorator(FLokaKeyDecorator::Create())
					+ SRichTextBlock::Decorator(FLokaSpaceDecorator::Create())
					+ SRichTextBlock::Decorator(FHyperlinkDecorator::Create(TEXT(""), FSlateHyperlinkRun::FOnClick::CreateLambda([&](const FSlateHyperlinkRun::FMetadata& InData) {
						if (InData.Contains("url")) FPlatformProcess::LaunchURL(*InData.FindChecked("url"), nullptr, nullptr);
					})))
				]
				+ SVerticalBox::Slot()
				[
					SNew(SListView<LTGraph>)
					.HeaderRow
					(
						SNew(SHeaderRow).Style(&Style->TableHeaderRow)
						+ SHeaderRow::Column(TEXT("Day")).HAlignHeader(HAlign_Center).VAlignHeader(VAlign_Center)
						[SNew(STextBlock).TextStyle(&Style->TableHeaderRowText).Text(NSLOCTEXT("EarlyNotify", "EarlyNotify.TableHeader.Day", "Day"))]
						+ SHeaderRow::Column(TEXT("Time")).HAlignHeader(HAlign_Center).VAlignHeader(VAlign_Center)
						[SNew(STextBlock).TextStyle(&Style->TableHeaderRowText).Text(NSLOCTEXT("EarlyNotify", "EarlyNotify.TableHeader.Time", "Time"))]
						+ SHeaderRow::Column(TEXT("Target")).HAlignHeader(HAlign_Center).VAlignHeader(VAlign_Center)
						[SNew(STextBlock).TextStyle(&Style->TableHeaderRowText).Text(NSLOCTEXT("EarlyNotify", "EarlyNotify.TableHeader.Target", "Target"))]					
					)
					.SelectionMode(ESelectionMode::None)
					.ListItemsSource(aGraph)
					.OnGenerateRow_Lambda([&](LTGraph InItem, const TSharedRef<STableViewBase>& InOwnerTable)
					{
						return SNew(SLTGraph, InOwnerTable, InItem);
					})
				]
			]
			+ SHorizontalBox::Slot().Padding(10, 0)
			[
				SNew(SRichTextBlock)
				.Text(Style->ShortDesc)
				.AutoWrapText(true)
				.TextStyle(&FLokaStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_TextBlock"))
				.Justification(ETextJustify::Left)
				.DecoratorStyleSet(&FLokaStyle::Get())
				+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(FLokaStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_TextBlock")))
				+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
				+ SRichTextBlock::Decorator(FLokaKeyDecorator::Create())
				+ SRichTextBlock::Decorator(FLokaSpaceDecorator::Create())
				+ SRichTextBlock::Decorator(FHyperlinkDecorator::Create(TEXT(""), FSlateHyperlinkRun::FOnClick::CreateLambda([&](const FSlateHyperlinkRun::FMetadata& InData) {
					if (InData.Contains("url")) FPlatformProcess::LaunchURL(*InData.FindChecked("url"), nullptr, nullptr);
				})))
			]
		]
		+ SVerticalBox::Slot().FillHeight(1) // Early Access description
		[
			SNew(SRichTextBlock)
			.Text(Style->LongDesc)
			.AutoWrapText(true)
			.TextStyle(&FLokaStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_TextBlock"))
			.Justification(ETextJustify::Left)
			.DecoratorStyleSet(&FLokaStyle::Get())
			+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(FLokaStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_TextBlock")))
			+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
			+ SRichTextBlock::Decorator(FLokaKeyDecorator::Create())
			+ SRichTextBlock::Decorator(FLokaSpaceDecorator::Create())
			+ SRichTextBlock::Decorator(FHyperlinkDecorator::Create(TEXT(""), FSlateHyperlinkRun::FOnClick::CreateLambda([&](const FSlateHyperlinkRun::FMetadata& InData) {
				if (InData.Contains("url")) FPlatformProcess::LaunchURL(*InData.FindChecked("url"), nullptr, nullptr);
			})))
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION
