// VRSPRO

#include "LokaGame.h"
#include "SAboutKiller.h"
#include "SlateOptMacros.h"

#include "Layout/SScaleBox.h"
#include "SAnimatedBackground.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SAboutKiller::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	ChildSlot
	.HAlign(HAlign_Left)
	.VAlign(VAlign_Bottom)
	.Padding(FMargin(0, 256))
	[
		SAssignNew(AnimatedBackground[0], SAnimatedBackground)
		.ShowAnimation(EAnimBackAnimation::LeftFade)
		.HideAnimation(EAnimBackAnimation::LeftFade)
		.Duration(1.5f)
		.InitAsHide(true)
		.IsRelative(true)
		.WithColor(false)
		.Ease(ECurveEaseFunction::QuadOut)		
		[
			SNew(SBorder)
			.Padding(0)
			.BorderImage(&Style->Background)
			.RenderTransformPivot(FVector2D(.5f, .5f))
			.RenderTransform(FSlateRenderTransform(FVector2D(-80, 0)))
			[
				SNew(SBorder)
				.Padding(FMargin(90, 4, 40, 0))
				.BorderImage(&Style->Border)
				[
					SAssignNew(AnimatedBackground[1], SAnimatedBackground)
					.ShowAnimation(EAnimBackAnimation::Color)
					.HideAnimation(EAnimBackAnimation::Color)
					.Duration(1.0f)
					.InitAsHide(true)
					.Ease(ECurveEaseFunction::Linear)
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot()
						.AutoHeight()
						.Padding(6)
						[
							SAssignNew(AboutText, STextBlock)
							.TextStyle(&Style->Text)
							.Justification(ETextJustify::Center)
						]
						+ SVerticalBox::Slot()
						.AutoHeight()
						.Padding(FMargin(20, 4))
						[
							SNew(SBox).HeightOverride(200)
							[
								SNew(SScaleBox)
								.HAlign(HAlign_Center)
								.VAlign(VAlign_Center)
								.Stretch(EStretch::ScaleToFit)
								[
									SAssignNew(AboutImage, SImage)
								]
							]
						]
					]
				]
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SAboutKiller::ShowAboutKiller(const FText& Name, const FSlateBrush* Brush, const int32& ShowTime)
{
	AboutText->SetText(Name);
	AboutImage->SetImage(Brush);

	AnimatedBackground[0]->ToggleWidget(true);
	AnimatedBackground[1]->ToggleWidget(true);

	RegisterActiveTimer(ShowTime, FWidgetActiveTimerDelegate::CreateSP(this, &SAboutKiller::OnReverse));
}

EActiveTimerReturnType SAboutKiller::OnReverse(const double InCurrentTime, const float InDeltaTime)
{
	AnimatedBackground[0]->ToggleWidget(false);
	AnimatedBackground[1]->ToggleWidget(false);

	return EActiveTimerReturnType::Stop;
}