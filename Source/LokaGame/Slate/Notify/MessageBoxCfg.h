// VRSPRO

#pragma once

#include "Object.h"
#include "MessageBoxCfg.generated.h"


class UWidget;

UENUM(Blueprintable, Category = MessageBox)
namespace MessageBoxButton
{
	enum Type
	{
		Left,
		Middle,
		Right,
		End UMETA(Hidden)
	};
}

DECLARE_DYNAMIC_DELEGATE(FOnMessageBoxHidden);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnMessageBoxButton, TEnumAsByte<MessageBoxButton::Type>, InButton);

USTRUCT(BlueprintType, Category = MessageBox)
struct FMessageBoxMsg
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UWidget* Widget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Title;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Message;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText ButtonLeft;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText ButtonMiddle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText ButtonRight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool IsAllowClose;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FBox2D SizeLimit;

	UPROPERTY()
	FOnMessageBoxHidden OnMessageBoxHidden;

	UPROPERTY()
	FOnMessageBoxButton OnMessageBoxButton;
};

UCLASS(hidecategories = Object, MinimalAPI, Category = MessageBox)
class UMessageBoxCfg : public UObject
{
	GENERATED_BODY()
	
public:

	UPROPERTY(Category = Configuration, EditAnywhere, BlueprintReadWrite, meta = (ShowOnlyInnerProperties))
	FMessageBoxMsg Message;
	
};
