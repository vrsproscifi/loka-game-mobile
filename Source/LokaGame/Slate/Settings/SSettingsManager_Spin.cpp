// VRSPRO

#include "LokaGame.h"
#include "SSettingsManager_Spin.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SSettingsManager_Spin::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	TArray<FText> Values = InArgs._Values;

	if (InArgs._IsGraphics)
	{
		Values.Empty();

		Values.Add(NSLOCTEXT("Settings", "Settings.Low", "Low"));
		Values.Add(NSLOCTEXT("Settings", "Settings.Medium", "Medium"));
		Values.Add(NSLOCTEXT("Settings", "Settings.High", "High"));
		Values.Add(NSLOCTEXT("Settings", "Settings.VeryHigh", "Very High"));
		Values.Add(NSLOCTEXT("Settings", "Settings.Ultra", "Ultra"));
	}

	ChildSlot
	[
		SNew(SVerticalBox)
		+ SVerticalBox::Slot().AutoHeight()
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
			[
				SAssignNew(Widget_TextBlock, STextBlock).Text(InArgs._Name)
				.TextStyle(&Style->Names)
			]
			+ SHorizontalBox::Slot()
			[
				SNew(SBox).HeightOverride(Style->HeightOverride)
				[
					SAssignNew(Widget_SliderSpin, SSliderSpin).Values(Values)
					.OnListValue(InArgs._OnValueChange)
					.Style(&Style->Spin)
				]
			]
		]
		+ SVerticalBox::Slot().AutoHeight()
		[
			SNew(SSpacer).Size(FVector2D(0, Style->BottomRowPadding))
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SSettingsManager_Spin::SetName(const FText& Name)
{
	Widget_TextBlock->SetText(Name);
}

void SSettingsManager_Spin::SetValues(TArray<FText> Values)
{
	Widget_SliderSpin->SetValues(Values);
}

void SSettingsManager_Spin::AddValue(const FText& Value)
{
	Widget_SliderSpin->AddValue(Value);
}

void SSettingsManager_Spin::SetActiveValue(const FText& Value)
{
	Widget_SliderSpin->SetActiveValue(Value);
}

void SSettingsManager_Spin::SetActiveValueIndex(const int32& Index)
{
	Widget_SliderSpin->SetActiveValueIndex(Index);
}

FText SSettingsManager_Spin::GetActiveValue() const
{
	return Widget_SliderSpin->GetActiveValue();
}

int32 SSettingsManager_Spin::GetActiveValueIndex() const
{
	return Widget_SliderSpin->GetActiveValueIndex();
}