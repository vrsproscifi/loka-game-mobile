// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Components/SSliderPercent.h"
#include "Styles/SettingsManagerWidgetStyle.h"
/**
 * 
 */
class LOKAGAME_API SSettingsManager_Percent : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SSettingsManager_Percent)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FSettingsManagerStyle>("SSettingsManagerStyle"))
		, _ButtonsDelta(.1f)
	{}
	SLATE_STYLE_ARGUMENT(FSettingsManagerStyle, Style)
	SLATE_ARGUMENT(FText, Name)
	SLATE_EVENT(FOnFloatValueChanged, OnValueChange)
	SLATE_ATTRIBUTE(float, ButtonsDelta)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void SetName(const FText&);
	FORCEINLINE FText GetName() const { return Widget_TextBlock->GetText(); }

	void SetValue(const float);
	float GetValue() const;

protected:

	const FSettingsManagerStyle* Style;

	FOnFloatValueChanged OnValueChange;

	TSharedPtr<SSliderPercent> Widget_SliderPercent;
	TSharedPtr<STextBlock> Widget_TextBlock;
};
