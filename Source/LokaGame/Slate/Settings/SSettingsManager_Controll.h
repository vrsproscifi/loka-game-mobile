// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/SettingsManagerWidgetStyle.h"
DECLARE_DELEGATE_TwoParams(FOnCallChangeKey, const FString&, const bool);

class LOKAGAME_API SSettingsManager_Controll : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SSettingsManager_Controll)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FSettingsManagerStyle>("SSettingsManagerStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FSettingsManagerStyle, Style)
	SLATE_EVENT(FOnCallChangeKey, OnCallChangeKey)
	SLATE_EVENT(FOnCallChangeKey, OnRemoveKey)
	SLATE_ARGUMENT(FText, Name)
	SLATE_ARGUMENT(FString, Key)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void SetName(const FText&);
	FORCEINLINE FText GetName() const { return Widget_TextBlock->GetText(); }
	void SetKeyName(const FText&, const bool = false);

protected:

	const FSettingsManagerStyle* Style;

	FString Key;
	FOnCallChangeKey OnCallChangeKey;
	FOnCallChangeKey OnRemoveKey;

	EVisibility GetVisibilityRemove(const bool) const;

	FReply OnClickedButtonEvent(const bool);
	FReply OnClickedButtonEventDelete(const bool);

	TSharedPtr<STextBlock> 
		  Widget_TextBlock
		, Widget_ButtonOne
		, Widget_ButtonTwo;

	TSharedPtr<SButton>	Widget_ButtonKey[2];
};
