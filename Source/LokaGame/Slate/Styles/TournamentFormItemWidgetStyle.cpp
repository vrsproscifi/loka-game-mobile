// VRSPRO

#include "LokaGame.h"
#include "TournamentFormItemWidgetStyle.h"


FTournamentFormItemStyle::FTournamentFormItemStyle()
{
}

FTournamentFormItemStyle::~FTournamentFormItemStyle()
{
}

const FName FTournamentFormItemStyle::TypeName(TEXT("FTournamentFormItemStyle"));

const FTournamentFormItemStyle& FTournamentFormItemStyle::GetDefault()
{
	static FTournamentFormItemStyle Default;
	return Default;
}

void FTournamentFormItemStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

