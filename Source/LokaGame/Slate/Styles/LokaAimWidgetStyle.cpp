// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "LokaAimWidgetStyle.h"


FLokaAimStyle::FLokaAimStyle()
: Size(FVector2D(100.0f, 100.0f))
, Offsets(FVector2D(0.0f, 100.0f))
, BaseAngle(0.0f)
, IsValid(false)
{
}

FLokaAimStyle::~FLokaAimStyle()
{
}

const FName FLokaAimStyle::TypeName(TEXT("LokaAimStyle"));

const FLokaAimStyle& FLokaAimStyle::GetDefault()
{
	static FLokaAimStyle Default;
	return Default;
}

void FLokaAimStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

