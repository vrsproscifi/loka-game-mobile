// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "WidgetHighlightingWidgetStyle.h"


FWidgetHighlightingStyle::FWidgetHighlightingStyle()
{
}

FWidgetHighlightingStyle::~FWidgetHighlightingStyle()
{
}

const FName FWidgetHighlightingStyle::TypeName(TEXT("FWidgetHighlightingStyle"));

const FWidgetHighlightingStyle& FWidgetHighlightingStyle::GetDefault()
{
	static FWidgetHighlightingStyle Default;
	return Default;
}

void FWidgetHighlightingStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

