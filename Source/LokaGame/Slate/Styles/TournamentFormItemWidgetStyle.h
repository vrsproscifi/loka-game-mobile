// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "TournamentFormItemWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct LOKAGAME_API FTournamentFormItemStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FTournamentFormItemStyle();
	virtual ~FTournamentFormItemStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FTournamentFormItemStyle& GetDefault();

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateBrush Background;

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateBrush Border;

	UPROPERTY(Category = Visual, EditAnywhere)
		FTextBlockStyle TextName;

	UPROPERTY(Category = Visual, EditAnywhere)
		FTextBlockStyle TextInfo;

	UPROPERTY(Category = Visual, EditAnywhere)
		FTextBlockStyle TextButton;

	UPROPERTY(Category = Visual, EditAnywhere)
		FButtonStyle Button;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FVector2D Size;

	UPROPERTY(Category = Configuration, EditAnywhere)
		float Height[3];		

	UPROPERTY(Category = Configuration, EditAnywhere)
		FMargin InfoBoxPadding;

	UPROPERTY(Category = Default, EditAnywhere)
		FSlateBrush DefaultImage;
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UTournamentFormItemWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()
		
public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FTournamentFormItemStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
