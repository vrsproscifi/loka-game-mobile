// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "LokaPopUpErrorWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct LOKAGAME_API FLokaPopUpErrorStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FLokaPopUpErrorStyle();
	virtual ~FLokaPopUpErrorStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FLokaPopUpErrorStyle& GetDefault();

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush Background;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateFontInfo Font;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateColor FontColor;
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class ULokaPopUpErrorWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FLokaPopUpErrorStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
