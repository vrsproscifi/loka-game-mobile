// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "RadialMenuWidgetStyle.h"


FRadialMenuStyle::FRadialMenuStyle()
{
}

FRadialMenuStyle::~FRadialMenuStyle()
{
}

const FName FRadialMenuStyle::TypeName(TEXT("FRadialMenuStyle"));

const FRadialMenuStyle& FRadialMenuStyle::GetDefault()
{
	static FRadialMenuStyle Default;
	return Default;
}

void FRadialMenuStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

