// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"
#include "TextBlockButtonWidgetStyle.h"
#include "AdvanceTextBoxWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct LOKAGAME_API FAdvanceTextBoxStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FAdvanceTextBoxStyle();
	virtual ~FAdvanceTextBoxStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FAdvanceTextBoxStyle& GetDefault();

	UPROPERTY(Category = Appearance, EditAnywhere)
		FEditableTextBoxStyle TextBoxStyle;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockButtonStyle ButtonEdit;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockButtonStyle ButtonAccept;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockButtonStyle ButtonCancel;
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UAdvanceTextBoxWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FAdvanceTextBoxStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
