// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "WindowDuelRulesWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct LOKAGAME_API FWindowDuelRulesStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle Desc;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle Row;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle RowPopUp;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FComboBoxStyle ComboBox;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSpinBoxStyle SpinBox;

	FWindowDuelRulesStyle();
	virtual ~FWindowDuelRulesStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FWindowDuelRulesStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UWindowDuelRulesWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FWindowDuelRulesStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
