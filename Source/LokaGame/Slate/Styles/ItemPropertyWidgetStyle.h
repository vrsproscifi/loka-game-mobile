// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "ItemPropertyWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct LOKAGAME_API FItemPropertyStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTableRowStyle Row;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle RowTextName;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle RowTextValue;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle RowTextImproValue;

	FItemPropertyStyle();
	virtual ~FItemPropertyStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FItemPropertyStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UItemPropertyWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FItemPropertyStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
