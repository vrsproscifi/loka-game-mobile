// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SlateWidgetStyleContainerBase.h"
#include "VerticalMenuBuilderWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct LOKAGAME_API FVerticalMenuBuilderStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FVerticalMenuBuilderStyle();
	virtual ~FVerticalMenuBuilderStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FVerticalMenuBuilderStyle& GetDefault();

	UPROPERTY(Category = Background, EditAnywhere)
		FSlateBrush Background;

	UPROPERTY(Category = Background, EditAnywhere)
		FSlateBrush Border;

	UPROPERTY(Category = Background, EditAnywhere)
		FSlateBrush SubBorder;

	UPROPERTY(Category = Default, EditAnywhere)
		FButtonStyle ButtonsStyle;

	UPROPERTY(Category = Default, EditAnywhere)
		FTextBlockStyle ButtonsTextStyle;
	//* X - Horizontal, Y - Vertical
	UPROPERTY(Category = Configuration, EditAnywhere)
		FVector2D ContainerButtonsPadding;
	//* X - Horizontal, Y - Vertical
	UPROPERTY(Category = Configuration, EditAnywhere)
		FVector2D ButtonsTextPadding;
	//* X - Start, Y - Duration
	UPROPERTY(Category = Configuration, EditAnywhere) 
		FVector2D AnimationTransform;
	//* X - Start, Y - Duration
	UPROPERTY(Category = Configuration, EditAnywhere)
		FVector2D AnimationContainerColor;
	//* X - Start, Y - Duration
	UPROPERTY(Category = Configuration, EditAnywhere)
		FVector2D AnimationBorderColor;
	//* X - Start, Y - Duration
	UPROPERTY(Category = Configuration, EditAnywhere)
		FVector2D AnimationSubBorders;
	//* X - Start, Y - Duration
	UPROPERTY(Category = Configuration, EditAnywhere)
		FVector2D AnimationButtons;
	//* X - Horizontal, Y - Vertical, Use 0,0 to disable.
	UPROPERTY(Category = Configuration, EditAnywhere)
		FVector2D ButtonsSize;
	//* X - Horizontal, Y - Vertical
	UPROPERTY(Category = Configuration, EditAnywhere)
		FVector2D ButtonsPadding;

	UPROPERTY(Category = Sound, EditAnywhere)
		FSlateSound SoundOpen;

	UPROPERTY(Category = Sound, EditAnywhere)
		FSlateSound SoundClose;
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UVerticalMenuBuilderWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FVerticalMenuBuilderStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
