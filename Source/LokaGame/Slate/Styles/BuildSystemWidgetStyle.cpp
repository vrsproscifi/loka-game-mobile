// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "BuildSystemWidgetStyle.h"


FBuildSystemStyle::FBuildSystemStyle()
{
}

FBuildSystemStyle::~FBuildSystemStyle()
{
}

const FName FBuildSystemStyle::TypeName(TEXT("FBuildSystemStyle"));

const FBuildSystemStyle& FBuildSystemStyle::GetDefault()
{
	static FBuildSystemStyle Default;
	return Default;
}

void FBuildSystemStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

