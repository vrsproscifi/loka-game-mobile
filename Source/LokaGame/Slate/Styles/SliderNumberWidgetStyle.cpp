// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SliderNumberWidgetStyle.h"


FSliderNumberStyle::FSliderNumberStyle()
{
}

FSliderNumberStyle::~FSliderNumberStyle()
{
}

const FName FSliderNumberStyle::TypeName(TEXT("FSliderNumberStyle"));

const FSliderNumberStyle& FSliderNumberStyle::GetDefault()
{
	static FSliderNumberStyle Default;
	return Default;
}

void FSliderNumberStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

