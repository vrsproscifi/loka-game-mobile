// VRSPRO

#include "LokaGame.h"
#include "DetailsBoxRowWidgetStyle.h"


FDetailsBoxRowStyle::FDetailsBoxRowStyle()
{
}

FDetailsBoxRowStyle::~FDetailsBoxRowStyle()
{
}

const FName FDetailsBoxRowStyle::TypeName(TEXT("FDetailsBoxRowStyle"));

const FDetailsBoxRowStyle& FDetailsBoxRowStyle::GetDefault()
{
	static FDetailsBoxRowStyle Default;
	return Default;
}

void FDetailsBoxRowStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

