// VRSPRO

#include "LokaGame.h"
#include "LokaFontsListWidgetStyle.h"


FLokaFontsListStyle::FLokaFontsListStyle()
{
}

FLokaFontsListStyle::~FLokaFontsListStyle()
{
}

const FName FLokaFontsListStyle::TypeName(TEXT("FLokaFontsListStyle"));

const FLokaFontsListStyle& FLokaFontsListStyle::GetDefault()
{
	static FLokaFontsListStyle Default;
	return Default;
}

void FLokaFontsListStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

