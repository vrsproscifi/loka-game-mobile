// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "DetailsBoxWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct LOKAGAME_API FDetailsBoxStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FDetailsBoxStyle();
	virtual ~FDetailsBoxStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FDetailsBoxStyle& GetDefault();

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateBrush Backround;

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateBrush Border;

	UPROPERTY(Category = Visual, EditAnywhere)
		FMargin PaddingRow;
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UDetailsBoxWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FDetailsBoxStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
