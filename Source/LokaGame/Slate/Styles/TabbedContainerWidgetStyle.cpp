// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "TabbedContainerWidgetStyle.h"


FTabbedContainerStyle::FTabbedContainerStyle()
{
}

FTabbedContainerStyle::~FTabbedContainerStyle()
{
}

const FName FTabbedContainerStyle::TypeName(TEXT("FTabbedContainerStyle"));

const FTabbedContainerStyle& FTabbedContainerStyle::GetDefault()
{
	static FTabbedContainerStyle Default;
	return Default;
}

void FTabbedContainerStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

