// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "LokaToolTipWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct LOKAGAME_API FLokaToolTipStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush Background;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush Border;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateFontInfo Font;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateColor FontColor;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FMargin TextMargin;

	FLokaToolTipStyle();
	virtual ~FLokaToolTipStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FLokaToolTipStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class ULokaToolTipWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FLokaToolTipStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
