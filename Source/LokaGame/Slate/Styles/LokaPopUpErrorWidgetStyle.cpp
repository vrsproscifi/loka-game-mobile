// VRSPRO

#include "LokaGame.h"
#include "LokaPopUpErrorWidgetStyle.h"


FLokaPopUpErrorStyle::FLokaPopUpErrorStyle()
{
}

FLokaPopUpErrorStyle::~FLokaPopUpErrorStyle()
{
}

const FName FLokaPopUpErrorStyle::TypeName(TEXT("FLokaPopUpErrorStyle"));

const FLokaPopUpErrorStyle& FLokaPopUpErrorStyle::GetDefault()
{
	static FLokaPopUpErrorStyle Default;
	return Default;
}

void FLokaPopUpErrorStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

