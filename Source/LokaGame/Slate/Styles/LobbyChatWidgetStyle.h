// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"
#include "TextBlockButtonWidgetStyle.h"
#include "LobbyChatWidgetStyle.generated.h"

USTRUCT()
struct LOKAGAME_API FLobbyChatStyle_Row
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle TextMessage;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle TextTime;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FHyperlinkStyle Hyperlink;
};

USTRUCT()
struct LOKAGAME_API FLobbyChatStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FLobbyChatStyle();
	virtual ~FLobbyChatStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FLobbyChatStyle& GetDefault();

	UPROPERTY(Category = Appearance, EditAnywhere)
		FCheckBoxStyle ChannelButtons;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FButtonStyle SendButton;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle SendButtonText;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FEditableTextBoxStyle ChatRow;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FEditableTextBoxStyle ChatInput;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FScrollBarStyle ScrollBar;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle ChannelTexts;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockButtonStyle ChannelCloseButtons;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FLobbyChatStyle_Row Row;
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class ULobbyChatWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FLobbyChatStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
