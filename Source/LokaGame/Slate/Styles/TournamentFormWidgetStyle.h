// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "TournamentFormWidgetStyle.generated.h"

/**
 * 
 */

USTRUCT()
struct LOKAGAME_API FTournamentFormStyle_Item
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Visual, EditAnywhere)
	FSlateBrush Background;

	UPROPERTY(Category = Visual, EditAnywhere)
	FSlateBrush Image;

	UPROPERTY(Category = Visual, EditAnywhere)
	FTextBlockStyle Header;

	UPROPERTY(Category = Visual, EditAnywhere)
	FTextBlockStyle Desc;

	UPROPERTY(Category = Visual, EditAnywhere)
	FTextBlockStyle ButtonText;

	UPROPERTY(Category = Visual, EditAnywhere)
	FButtonStyle Button;
};



USTRUCT()
struct LOKAGAME_API FTournamentFormStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FTournamentFormStyle();
	virtual ~FTournamentFormStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FTournamentFormStyle& GetDefault();

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateBrush TopBackground;

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateBrush SideBackground;

	UPROPERTY(Category = Visual, EditAnywhere)
		FScrollBarStyle ScrollBar;

	UPROPERTY(Category = Visual, EditAnywhere)
		FSearchBoxStyle SearchBox;

	UPROPERTY(Category = Visual, EditAnywhere)
		FTournamentFormStyle_Item Item;
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UTournamentFormWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FTournamentFormStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
