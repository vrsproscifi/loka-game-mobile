// VRSPRO

#include "LokaGame.h"
#include "TournamentFormWidgetStyle.h"


FTournamentFormStyle::FTournamentFormStyle()
{
}

FTournamentFormStyle::~FTournamentFormStyle()
{
}

const FName FTournamentFormStyle::TypeName(TEXT("FTournamentFormStyle"));

const FTournamentFormStyle& FTournamentFormStyle::GetDefault()
{
	static FTournamentFormStyle Default;
	return Default;
}

void FTournamentFormStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

