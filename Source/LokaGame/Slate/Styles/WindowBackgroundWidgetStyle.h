// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "WindowBackgroundWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct LOKAGAME_API FWindowBackgroundStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush Background;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush Border;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush Shadow;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush Header;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle HeaderText;

	FWindowBackgroundStyle();
	virtual ~FWindowBackgroundStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FWindowBackgroundStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UWindowBackgroundWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FWindowBackgroundStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
