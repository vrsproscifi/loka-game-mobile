// VRSPRO

#include "LokaGame.h"
#include "WindowEverydayBonusWidgetStyle.h"


FWindowEverydayBonusStyle::FWindowEverydayBonusStyle()
{
}

FWindowEverydayBonusStyle::~FWindowEverydayBonusStyle()
{
}

const FName FWindowEverydayBonusStyle::TypeName(TEXT("FWindowEverydayBonusStyle"));

const FWindowEverydayBonusStyle& FWindowEverydayBonusStyle::GetDefault()
{
	static FWindowEverydayBonusStyle Default;
	return Default;
}

void FWindowEverydayBonusStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

