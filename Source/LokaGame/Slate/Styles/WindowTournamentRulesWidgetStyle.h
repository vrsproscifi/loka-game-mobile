// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "WindowTournamentRulesWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct LOKAGAME_API FWindowTournamentRulesStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle TextOne;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle TextTwo;

	FWindowTournamentRulesStyle();
	virtual ~FWindowTournamentRulesStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FWindowTournamentRulesStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UWindowTournamentRulesWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FWindowTournamentRulesStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
