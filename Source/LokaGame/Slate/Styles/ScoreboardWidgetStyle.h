// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SlateBasics.h"

#include "SlateWidgetStyleContainerBase.h"
#include "ScoreboardWidgetStyle.generated.h"


USTRUCT(BlueprintType)
struct LOKAGAME_API FScoreboardColumnsSize
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ScoreboardConfig)
		float Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ScoreboardConfig)
		float Kills;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ScoreboardConfig)
		float Deaths;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ScoreboardConfig)
		float Score;

public:

	FScoreboardColumnsSize() : Name(1.0f), Kills(0.6f), Deaths(0.6f), Score(0.8f) {}
};

USTRUCT()
struct LOKAGAME_API FScoreboardStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FScoreboardStyle();
	virtual ~FScoreboardStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FScoreboardStyle& GetDefault();

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush IconLeftKills;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush IconLeftDeaths;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush IconRightKills;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush IconRightDeaths;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush LeftBackground;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush RightBackground;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush LeftBackgroundEdge;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush RightBackgroundEdge;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush LeftIcon;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush RightIcon;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush UpLine;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush DownLine;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateFontInfo HeadFont;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateFontInfo TableHeadFont;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateFontInfo TableTotalScoreFont;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateFontInfo TableRowsFont;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateColor LeftHeadColor;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateColor LeftRowColor;	

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateColor RightHeadColor;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateColor RightRowColor;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateColor LocalPlayerColor;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateFontInfo KillRowFont;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FVector2D TableSize;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FVector2D TableLinePadding;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FScoreboardColumnsSize ColumnsSize;
	
	UPROPERTY(Category = Appearance, EditAnywhere)
		float DefaultKillRowShowTime;
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UScoreboardWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
		FScoreboardStyle ScoreboardWidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >(&ScoreboardWidgetStyle);
	}
};
