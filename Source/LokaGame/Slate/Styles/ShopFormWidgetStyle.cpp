// VRSPRO

#include "LokaGame.h"
#include "ShopFormWidgetStyle.h"


FShopFormStyle::FShopFormStyle()
	: MarginItemsContainer(40.0f)
	, ControlSize(0.25f)
	, ItemsSize(1.0f)
{
}

FShopFormStyle::~FShopFormStyle()
{
}

const FName FShopFormStyle::TypeName(TEXT("ShopFormStyle"));

const FShopFormStyle& FShopFormStyle::GetDefault()
{
	static FShopFormStyle Default;
	return Default;
}

void FShopFormStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

