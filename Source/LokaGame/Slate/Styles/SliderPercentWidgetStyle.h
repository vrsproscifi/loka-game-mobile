// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "SliderPercentWidgetStyle.generated.h"


USTRUCT()
struct LOKAGAME_API FSliderPercentStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush Background;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FButtonStyle ButtonLeft;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FButtonStyle ButtonRight;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle TextButtons;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FMargin ButtonsTextPadding;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle TextCenter;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FMargin CenterTextPadding;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FProgressBarStyle ProgressBar;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSliderStyle Slider;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FMargin ProgressBarPadding;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FMargin SliderPadding;

	FSliderPercentStyle();
	virtual ~FSliderPercentStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FSliderPercentStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class USliderPercentWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FSliderPercentStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
