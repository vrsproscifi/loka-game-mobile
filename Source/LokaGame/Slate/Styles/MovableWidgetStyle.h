// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "MovableWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct LOKAGAME_API FMovableStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FMovableStyle();
	virtual ~FMovableStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FMovableStyle& GetDefault();

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush HeaderBackground;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush Background;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush Border;
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UMovableWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FMovableStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
