// VRSPRO

#include "LokaGame.h"
#include "DetailsBoxWidgetStyle.h"


FDetailsBoxStyle::FDetailsBoxStyle()
{
}

FDetailsBoxStyle::~FDetailsBoxStyle()
{
}

const FName FDetailsBoxStyle::TypeName(TEXT("FDetailsBoxStyle"));

const FDetailsBoxStyle& FDetailsBoxStyle::GetDefault()
{
	static FDetailsBoxStyle Default;
	return Default;
}

void FDetailsBoxStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

