// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "AboutKillerWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct LOKAGAME_API FAboutKillerStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FAboutKillerStyle();
	virtual ~FAboutKillerStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FAboutKillerStyle& GetDefault();

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush Background;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush Border;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle Text;
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UAboutKillerWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FAboutKillerStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
