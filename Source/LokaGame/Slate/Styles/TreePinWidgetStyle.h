// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "TreePinWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct LOKAGAME_API FTreePinStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FTreePinStyle();
	virtual ~FTreePinStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FTreePinStyle& GetDefault();

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateBrush Disabled;

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateBrush Normal;
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UTreePinWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FTreePinStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
