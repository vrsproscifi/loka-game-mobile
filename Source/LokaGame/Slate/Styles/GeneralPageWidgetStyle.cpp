// VRSPRO

#include "LokaGame.h"
#include "GeneralPageWidgetStyle.h"


FGeneralPageStyle::FGeneralPageStyle()
{
}

FGeneralPageStyle::~FGeneralPageStyle()
{
}

const FName FGeneralPageStyle::TypeName(TEXT("FGeneralPageStyle"));

const FGeneralPageStyle& FGeneralPageStyle::GetDefault()
{
	static FGeneralPageStyle Default;
	return Default;
}

void FGeneralPageStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

