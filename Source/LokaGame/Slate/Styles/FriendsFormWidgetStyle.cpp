// VRSPRO

#include "LokaGame.h"
#include "FriendsFormWidgetStyle.h"


FFriendsFormStyle::FFriendsFormStyle()
{
}

FFriendsFormStyle::~FFriendsFormStyle()
{
}

const FName FFriendsFormStyle::TypeName(TEXT("FFriendsFormStyle"));

const FFriendsFormStyle& FFriendsFormStyle::GetDefault()
{
	static FFriendsFormStyle Default;
	return Default;
}

void FFriendsFormStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

