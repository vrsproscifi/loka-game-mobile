// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "UpgradeWindowWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct LOKAGAME_API FUpgradeWindowStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush Background;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush Border;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush Header;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle HeaderText;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle ParameterName;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle ParameterValue;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTableRowStyle ParameterRow;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FButtonStyle Buttons;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle ButtonsText;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle DescText;

	FUpgradeWindowStyle();
	virtual ~FUpgradeWindowStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FUpgradeWindowStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UUpgradeWindowWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FUpgradeWindowStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
