// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "TutorialSelectorWidgetStyle.h"


FTutorialSelectorStyle::FTutorialSelectorStyle()
{
}

FTutorialSelectorStyle::~FTutorialSelectorStyle()
{
}

const FName FTutorialSelectorStyle::TypeName(TEXT("FTutorialSelectorStyle"));

const FTutorialSelectorStyle& FTutorialSelectorStyle::GetDefault()
{
	static FTutorialSelectorStyle Default;
	return Default;
}

void FTutorialSelectorStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

