// VRSPRO

#include "LokaGame.h"
#include "SliderPercentWidgetStyle.h"


FSliderPercentStyle::FSliderPercentStyle()
{
}

FSliderPercentStyle::~FSliderPercentStyle()
{
}

const FName FSliderPercentStyle::TypeName(TEXT("FSliderPercentStyle"));

const FSliderPercentStyle& FSliderPercentStyle::GetDefault()
{
	static FSliderPercentStyle Default;
	return Default;
}

void FSliderPercentStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

