// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "GameTimerWidgetStyle.h"


FGameTimerStyle::FGameTimerStyle()
{
}

FGameTimerStyle::~FGameTimerStyle()
{
}

const FName FGameTimerStyle::TypeName(TEXT("GameTimerStyle"));

const FGameTimerStyle& FGameTimerStyle::GetDefault()
{
	static FGameTimerStyle Default;
	return Default;
}

void FGameTimerStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{

}

