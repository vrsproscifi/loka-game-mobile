// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "PlayerThumbnailWidgetStyle.generated.h"

UENUM()
namespace EStylePlayerState
{
	enum Type
	{
		Offline,
		Online,
		Waiting,
		Playing,
		Building,
		Local,
		End
	};
}

USTRUCT()
struct LOKAGAME_API FPlayerThumbnailStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Name, EditAnywhere)			FTextBlockStyle			Name[EStylePlayerState::End];
	UPROPERTY(Category = Status, EditAnywhere)			FTextBlockStyle			Status[EStylePlayerState::End];
	UPROPERTY(Category = Background, EditAnywhere)		FSlateBrush				Background[EStylePlayerState::End];

	FPlayerThumbnailStyle();
	virtual ~FPlayerThumbnailStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FPlayerThumbnailStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UPlayerThumbnailWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FPlayerThumbnailStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
