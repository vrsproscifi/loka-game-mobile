// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "HitIndicatorWidgetStyle.generated.h"

UENUM(BlueprintType)
namespace EHitIndicator
{
	enum Type
	{
		FrontTop,
		FrontLeft,
		FrontRight,
		Left,
		Right,
		BackLeft,
		BackRight,
		Back,
		End
	};
}

USTRUCT(BlueprintType)
struct LOKAGAME_API FHIFrame
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere, BlueprintReadWrite)
	FSlateBrush Image;

	UPROPERTY(Category = Appearance, EditAnywhere, BlueprintReadWrite)
	FVector2D Offset;
};

USTRUCT()
struct LOKAGAME_API FHitIndicatorStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FHitIndicatorStyle();
	virtual ~FHitIndicatorStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FHitIndicatorStyle& GetDefault();

	UPROPERTY(Category = Appearance, EditAnywhere)
	FHIFrame Images[EHitIndicator::End];

	UPROPERTY(Category = Configuration, EditAnywhere)
	float ShowTime;
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UHitIndicatorWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FHitIndicatorStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
