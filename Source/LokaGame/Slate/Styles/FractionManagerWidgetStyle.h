// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "FractionManagerWidgetStyle.generated.h"

USTRUCT(Blueprintable)
struct LOKAGAME_API FFractionManagerStyle_Selector
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FButtonStyle Button;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FTextBlockStyle Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FTextBlockStyle Desc;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FTextBlockStyle ButtonText;
};

USTRUCT(Blueprintable)
struct LOKAGAME_API FFractionManagerStyle_Button
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FCheckBoxStyle Button;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSlateBrush Background;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSlateBrush Border;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle TextName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FButtonStyle ButtonInformation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle TextInformation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FButtonStyle ButtonJoin;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle TextJoin;
};

USTRUCT(Blueprintable)
struct LOKAGAME_API FFractionManagerStyle_Item
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FButtonStyle Button;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSlateBrush NameBorder;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle NameText;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSlateFontInfo BuyText;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSlateBrush BuyBorder;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSlateColor BuyTextColor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSlateColor BuyedTextColor;
};

USTRUCT(Blueprintable)
struct LOKAGAME_API FFractionManagerStyle_Container
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FCheckBoxStyle Buttons;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FComboButtonStyle CharSelector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FScrollBarStyle ScrollBar;
};

USTRUCT(Blueprintable)
struct LOKAGAME_API FFractionManagerStyle_Column
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FProgressBarStyle ProgressBar;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle LockedText;
};

USTRUCT(BlueprintType)
struct LOKAGAME_API FFractionManagerStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FFractionManagerStyle_Column Column;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FFractionManagerStyle_Container Container;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FFractionManagerStyle_Item Item;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FFractionManagerStyle_Button Button;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FFractionManagerStyle_Selector Selector;

	FFractionManagerStyle();
	virtual ~FFractionManagerStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FFractionManagerStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UFractionManagerWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FFractionManagerStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
