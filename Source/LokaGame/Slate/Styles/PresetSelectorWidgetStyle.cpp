// VRSPRO

#include "LokaGame.h"
#include "PresetSelectorWidgetStyle.h"


FPresetSelectorStyle::FPresetSelectorStyle()
{
}

FPresetSelectorStyle::~FPresetSelectorStyle()
{
}

const FName FPresetSelectorStyle::TypeName(TEXT("FPresetSelectorStyle"));

const FPresetSelectorStyle& FPresetSelectorStyle::GetDefault()
{
	static FPresetSelectorStyle Default;
	return Default;
}

void FPresetSelectorStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

