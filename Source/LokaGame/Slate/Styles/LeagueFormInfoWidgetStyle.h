// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"
#include "TextBlockButtonWidgetStyle.h"
#include "LeagueFormInfoWidgetStyle.generated.h"

USTRUCT()
struct LOKAGAME_API FLFIInfoBlockBG
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush IconAndBaseInfo;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush MoneyAndButtons;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush Events;
};

USTRUCT()
struct LOKAGAME_API FLFIInfoBlock
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(Category = Appearance, EditAnywhere)
		float IconAndBaseInfo;

	UPROPERTY(Category = Appearance, EditAnywhere)
		float MoneyAndButtons;

	UPROPERTY(Category = Appearance, EditAnywhere)
		float Events;
};

USTRUCT()
struct LOKAGAME_API FLeagueFormInfoStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FLeagueFormInfoStyle();
	virtual ~FLeagueFormInfoStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FLeagueFormInfoStyle& GetDefault();

	UPROPERTY(Category = Appearance, EditAnywhere)
		FHeaderRowStyle HeaderRow;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTableRowStyle TableRow;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle TableHeadText;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle TableRowText;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle InfoHeadText;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle InfoRowText;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FCheckBoxStyle CheckBoxes;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FLFIInfoBlockBG InfoBackgrounds;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockButtonStyle ToDonate;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockButtonStyle ToDonateAccept;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockButtonStyle ToDonateCancel;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FEditableTextBoxStyle AbbrAndName;

	UPROPERTY(Category = Configuration, EditAnywhere)
		float InfoBlockWidth;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FLFIInfoBlock InfoBlockHeights;

	UPROPERTY(Category = Default, EditAnywhere)
		FSlateBrush DefaultImage;
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class ULeagueFormInfoWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FLeagueFormInfoStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
