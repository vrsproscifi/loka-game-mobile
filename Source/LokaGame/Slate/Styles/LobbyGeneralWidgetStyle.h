// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SlateWidgetStyleContainerBase.h"
#include "LobbyGeneralWidgetStyle.generated.h"

USTRUCT()
struct LOKAGAME_API FLobbyGeneralStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FLobbyGeneralStyle();
	virtual ~FLobbyGeneralStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FLobbyGeneralStyle& GetDefault();

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush Background;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush Border;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle HeaderText;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FButtonStyle HelperButton;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FButtonStyle SettingButton;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FButtonStyle CloseButton;

	UPROPERTY(Category = Configuration, EditAnywhere)
		float HeaderSize;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FVector2D ButtonsOffset;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FMargin Padding;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FMargin BorderPadding;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FMargin HeaderTextMargin;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FMargin ButtonsPadding;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FMargin ButtonPadding;

};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class ULobbyGeneralWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
		FLobbyGeneralStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
