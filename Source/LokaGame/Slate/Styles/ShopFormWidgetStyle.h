// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"
#include "StyleHelper.h"
#include "ShopFormWidgetStyle.generated.h"

USTRUCT()
struct LOKAGAME_API FShopFormStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FShopFormStyle();
	virtual ~FShopFormStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FShopFormStyle& GetDefault();

	UPROPERTY(Category = Configuration, EditAnywhere)
		FMargin MarginItemsContainer;

	UPROPERTY(Category = Configuration, EditAnywhere)
		float ControlSize;

	UPROPERTY(Category = Configuration, EditAnywhere)
		float ItemsSize;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FSeparatorAdvanceStyle Separator;
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UShopFormWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FShopFormStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
