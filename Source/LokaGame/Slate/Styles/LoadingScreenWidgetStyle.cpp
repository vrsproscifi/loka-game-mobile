// VRSPRO

#include "LokaGame.h"
#include "LoadingScreenWidgetStyle.h"


FLoadingScreenStyle::FLoadingScreenStyle()
{
}

FLoadingScreenStyle::~FLoadingScreenStyle()
{
}

const FName FLoadingScreenStyle::TypeName(TEXT("LoadingScreenStyle"));

const FLoadingScreenStyle& FLoadingScreenStyle::GetDefault()
{
	static FLoadingScreenStyle Default;
	return Default;
}

void FLoadingScreenStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

