// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "GeneralPageWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct LOKAGAME_API FGeneralPageStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FHeaderRowStyle HeaderRow;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle HeaderText;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle YourPlaceText;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle YourRowText;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle OtherRowText;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTableRowStyle TableRow;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle TableRowText;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush Image;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FButtonStyle Button;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle TimeStart;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle TimeEnd;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle TotalPrizeCost;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FButtonStyle Hyperlink;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle OtherRowTextOnline;

	FGeneralPageStyle();
	virtual ~FGeneralPageStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FGeneralPageStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UGeneralPageWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FGeneralPageStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
