// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "KeyBindingWidgetStyle.h"


FKeyBindingStyle::FKeyBindingStyle()
{
}

FKeyBindingStyle::~FKeyBindingStyle()
{
}

const FName FKeyBindingStyle::TypeName(TEXT("FKeyBindingStyle"));

const FKeyBindingStyle& FKeyBindingStyle::GetDefault()
{
	static FKeyBindingStyle Default;
	return Default;
}

void FKeyBindingStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

