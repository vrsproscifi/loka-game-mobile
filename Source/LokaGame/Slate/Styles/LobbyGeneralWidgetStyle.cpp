// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "LobbyGeneralWidgetStyle.h"


FLobbyGeneralStyle::FLobbyGeneralStyle()
{
}

FLobbyGeneralStyle::~FLobbyGeneralStyle()
{
}

const FName FLobbyGeneralStyle::TypeName(TEXT("LobbyGeneralStyle"));

const FLobbyGeneralStyle& FLobbyGeneralStyle::GetDefault()
{
	static FLobbyGeneralStyle Default;
	return Default;
}

void FLobbyGeneralStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	OutBrushes.Add(&Background);
	OutBrushes.Add(&Border);
	HeaderText.GetResources(OutBrushes);
	HelperButton.GetResources(OutBrushes);
	SettingButton.GetResources(OutBrushes);
	CloseButton.GetResources(OutBrushes);
}

