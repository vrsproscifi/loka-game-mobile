// VRSPRO

#include "LokaGame.h"
#include "LeagueFormInfoWidgetStyle.h"


FLeagueFormInfoStyle::FLeagueFormInfoStyle()
{
}

FLeagueFormInfoStyle::~FLeagueFormInfoStyle()
{
}

const FName FLeagueFormInfoStyle::TypeName(TEXT("FLeagueFormInfoStyle"));

const FLeagueFormInfoStyle& FLeagueFormInfoStyle::GetDefault()
{
	static FLeagueFormInfoStyle Default;
	return Default;
}

void FLeagueFormInfoStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

