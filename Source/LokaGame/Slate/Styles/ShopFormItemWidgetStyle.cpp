// VRSPRO

#include "LokaGame.h"
#include "ShopFormItemWidgetStyle.h"


FShopFormItemStyle::FShopFormItemStyle()
	: Size(FVector2D(480.0f, 420.0f))
{
}

FShopFormItemStyle::~FShopFormItemStyle()
{
}

const FName FShopFormItemStyle::TypeName(TEXT("ShopFormItemStyle"));

const FShopFormItemStyle& FShopFormItemStyle::GetDefault()
{
	static FShopFormItemStyle Default;
	return Default;
}

void FShopFormItemStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

