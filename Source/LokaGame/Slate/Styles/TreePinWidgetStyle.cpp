// VRSPRO

#include "LokaGame.h"
#include "TreePinWidgetStyle.h"


FTreePinStyle::FTreePinStyle()
{
}

FTreePinStyle::~FTreePinStyle()
{
}

const FName FTreePinStyle::TypeName(TEXT("FTreePinStyle"));

const FTreePinStyle& FTreePinStyle::GetDefault()
{
	static FTreePinStyle Default;
	return Default;
}

void FTreePinStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

