// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "VotingWidgetStyle.h"


FVotingStyle::FVotingStyle()
{
}

FVotingStyle::~FVotingStyle()
{
}

const FName FVotingStyle::TypeName(TEXT("FVotingStyle"));

const FVotingStyle& FVotingStyle::GetDefault()
{
	static FVotingStyle Default;
	return Default;
}

void FVotingStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

