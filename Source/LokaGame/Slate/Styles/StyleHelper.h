// VRSPRO

#pragma once

#include "StyleHelper.generated.h"

UENUM(BlueprintType)
namespace EButtonState
{
	enum Type
	{
		Normal,
		Active,
		End UMETA(Hidden)
	};
}

USTRUCT(BlueprintType)
struct FSlotHelper
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Configuration, EditAnywhere)
		TEnumAsByte<EHorizontalAlignment> HAlign;

	UPROPERTY(Category = Configuration, EditAnywhere)
		TEnumAsByte<EVerticalAlignment> VAlign;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FMargin Margin;
};

USTRUCT(BlueprintType)
struct FSeparatorAdvanceStyle
{
	GENERATED_USTRUCT_BODY()	

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateBrush Image;

	UPROPERTY(Category = Configuration, EditAnywhere)
		TEnumAsByte<EOrientation> Orientation;

	UPROPERTY(Category = Configuration, EditAnywhere)
		float Thickness;
};