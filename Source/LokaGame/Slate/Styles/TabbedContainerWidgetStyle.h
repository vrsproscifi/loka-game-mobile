// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "TabbedContainerWidgetStyle.generated.h"

USTRUCT()
struct LOKAGAME_API FTabbedContainerStyle_Controlls
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)	FCheckBoxStyle First;
	UPROPERTY(Category = Appearance, EditAnywhere)	FCheckBoxStyle Middle;
	UPROPERTY(Category = Appearance, EditAnywhere)	FCheckBoxStyle Last;

	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle Text;
	UPROPERTY(Category = Appearance, EditAnywhere)	FMargin TextMargin;
};

USTRUCT()
struct LOKAGAME_API FTabbedContainerStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTabbedContainerStyle_Controlls HorizontalControlls;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTabbedContainerStyle_Controlls VerticalControlls;

	FTabbedContainerStyle();
	virtual ~FTabbedContainerStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FTabbedContainerStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UTabbedContainerWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FTabbedContainerStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
