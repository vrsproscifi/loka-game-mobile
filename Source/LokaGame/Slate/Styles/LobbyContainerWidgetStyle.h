// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"
#include "GameModeTypeId.h"
#include "LobbyContainerWidgetStyle.generated.h"


USTRUCT()
struct LOKAGAME_API FLobbyContainerStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FCheckBoxStyle IntoFightButton;

	UPROPERTY(Category = Appearance, EditAnywhere)
	TMap<EBlueprintGameMode, FSlateBrush> GameModes;

	UPROPERTY(Category = Appearance, EditAnywhere)
	TArray<EBlueprintGameMode> DisabledGameModes;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTableRowStyle GameModeTile;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle FightOptionsText;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FButtonStyle FightOptionsButton;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FMargin FightOptionsPadding;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FButtonStyle PaymentButton;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle CooperativeText;

	FLobbyContainerStyle();
	virtual ~FLobbyContainerStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FLobbyContainerStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class ULobbyContainerWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FLobbyContainerStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
