// VRSPRO

#include "LokaGame.h"
#include "NotifyListWidgetStyle.h"


FNotifyListStyle::FNotifyListStyle()
{
}

FNotifyListStyle::~FNotifyListStyle()
{
}

const FName FNotifyListStyle::TypeName(TEXT("FNotifyListStyle"));

const FNotifyListStyle& FNotifyListStyle::GetDefault()
{
	static FNotifyListStyle Default;
	return Default;
}

void FNotifyListStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

