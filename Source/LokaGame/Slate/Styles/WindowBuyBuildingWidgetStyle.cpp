// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "WindowBuyBuildingWidgetStyle.h"


FWindowBuyBuildingStyle::FWindowBuyBuildingStyle()
{
}

FWindowBuyBuildingStyle::~FWindowBuyBuildingStyle()
{
}

const FName FWindowBuyBuildingStyle::TypeName(TEXT("FWindowBuyBuildingStyle"));

const FWindowBuyBuildingStyle& FWindowBuyBuildingStyle::GetDefault()
{
	static FWindowBuyBuildingStyle Default;
	return Default;
}

void FWindowBuyBuildingStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

