// VRSPRO

#include "LokaGame.h"
#include "WelcomeWindowsWidgetStyle.h"


FWelcomeWindowsStyle::FWelcomeWindowsStyle()
{
}

FWelcomeWindowsStyle::~FWelcomeWindowsStyle()
{
}

const FName FWelcomeWindowsStyle::TypeName(TEXT("FWelcomeWindowsStyle"));

const FWelcomeWindowsStyle& FWelcomeWindowsStyle::GetDefault()
{
	static FWelcomeWindowsStyle Default;
	return Default;
}

void FWelcomeWindowsStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

