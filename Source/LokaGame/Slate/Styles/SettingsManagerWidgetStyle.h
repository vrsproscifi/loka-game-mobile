// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"
#include "SliderSpinWidgetStyle.h"
#include "SliderPercentWidgetStyle.h"
#include "SettingsManagerWidgetStyle.generated.h"

USTRUCT()
struct LOKAGAME_API FSettingsManagerStyle_Dialog
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush Background;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush Head;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FButtonStyle Button;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle ButtonText;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle DescText;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle HeadText;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle KeyText;
};


USTRUCT()
struct LOKAGAME_API FSettingsManagerStyle_Container
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush Background;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FCheckBoxStyle Button;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle ButtonText;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FButtonStyle ActionButton;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle ActionButtonText;
};

USTRUCT()
struct LOKAGAME_API FSettingsManagerStyle_Controll
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FButtonStyle ButtonKey;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FButtonStyle ButtonRemove;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle ButtonTextKey;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle ButtonTextRemove;
};

USTRUCT()
struct LOKAGAME_API FSettingsManagerStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSettingsManagerStyle_Container Container;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSettingsManagerStyle_Dialog Dialog;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSettingsManagerStyle_Controll Ctrl;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle Names;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSliderSpinStyle Spin;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSliderPercentStyle Percent;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FButtonStyle Controll;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle ControllText;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FCheckBoxStyle Check;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FButtonStyle CloseButton;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle CloseButtonText;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle HeaderText;

	UPROPERTY(Category = Appearance, EditAnywhere)
	float HeightOverride;

	UPROPERTY(Category = Appearance, EditAnywhere)
	float BottomRowPadding;

	FSettingsManagerStyle();
	virtual ~FSettingsManagerStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FSettingsManagerStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class USettingsManagerWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FSettingsManagerStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
