// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "RadialMenuWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct LOKAGAME_API FRadialMenuStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)		FSlateBrush Background;
	UPROPERTY(Category = Appearance, EditAnywhere)		FSlateBrush OuterShadow;
	UPROPERTY(Category = Appearance, EditAnywhere)		FSlateBrush InnerShadow;
	UPROPERTY(Category = Appearance, EditAnywhere)		FSlateBrush Selected;
	UPROPERTY(Category = Configuration, EditAnywhere)	float		SelectedShowHideTime;

	FRadialMenuStyle();
	virtual ~FRadialMenuStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FRadialMenuStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class URadialMenuWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FRadialMenuStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
