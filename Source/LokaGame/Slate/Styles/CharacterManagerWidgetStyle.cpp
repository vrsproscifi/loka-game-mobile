// VRSPRO

#include "LokaGame.h"
#include "CharacterManagerWidgetStyle.h"


FCharacterManagerStyle::FCharacterManagerStyle()
{
}

FCharacterManagerStyle::~FCharacterManagerStyle()
{
}

const FName FCharacterManagerStyle::TypeName(TEXT("FCharacterManagerStyle"));

const FCharacterManagerStyle& FCharacterManagerStyle::GetDefault()
{
	static FCharacterManagerStyle Default;
	return Default;
}

void FCharacterManagerStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

