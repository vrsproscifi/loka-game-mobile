// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "WindowsWidgetStyle.generated.h"


USTRUCT()
struct LOKAGAME_API FWindowsStyle_Header
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush Background;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle Text;
};

USTRUCT()
struct LOKAGAME_API FWindowsStyle_QuickButton
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FCheckBoxStyle Button;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle Awesome;
};

USTRUCT()
struct LOKAGAME_API FWindowsStyle_Controlls
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FButtonStyle ButtonClose;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FButtonStyle ButtonMode;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle Awesome;
};

USTRUCT()
struct LOKAGAME_API FWindowsStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FWindowsStyle_Controlls Controlls;

	UPROPERTY(Category = Default, EditAnywhere)
	FWindowsStyle_QuickButton DefaultQuickButton;

	UPROPERTY(Category = Default, EditAnywhere)
	FWindowsStyle_Header Header;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush Background;

	FWindowsStyle();
	virtual ~FWindowsStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FWindowsStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UWindowsWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FWindowsStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
