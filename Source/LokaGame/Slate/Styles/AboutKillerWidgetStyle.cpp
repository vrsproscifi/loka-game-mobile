// VRSPRO

#include "LokaGame.h"
#include "AboutKillerWidgetStyle.h"


FAboutKillerStyle::FAboutKillerStyle()
{
}

FAboutKillerStyle::~FAboutKillerStyle()
{
}

const FName FAboutKillerStyle::TypeName(TEXT("FAboutKillerStyle"));

const FAboutKillerStyle& FAboutKillerStyle::GetDefault()
{
	static FAboutKillerStyle Default;
	return Default;
}

void FAboutKillerStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

