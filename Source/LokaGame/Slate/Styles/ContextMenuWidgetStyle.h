// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "ContextMenuWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct LOKAGAME_API FContextMenuStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush Background;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush Icon;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush Expand;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush SubMenuIndicator;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FMargin SToolBarComboButtonBlockPadding;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FMargin SToolBarButtonBlockPadding;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FMargin SToolBarCheckComboButtonBlockPadding;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FMargin SToolBarButtonBlockCheckBoxPadding;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FLinearColor SToolBarComboButtonBlockComboButtonColor;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FMargin BlockIndentedPadding;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FMargin BlockPadding;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush Separator;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FMargin SeparatorPadding;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle Label;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FEditableTextBoxStyle EditableText;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle Keybinding;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle Heading;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FCheckBoxStyle CheckBox;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FCheckBoxStyle Check;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FCheckBoxStyle RadioButton;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FCheckBoxStyle ToggleButton;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FButtonStyle Button;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush ButtonChecked;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush ButtonChecked_Hovered;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush ButtonChecked_Pressed;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush ButtonSubMenuOpen;

	FContextMenuStyle();
	virtual ~FContextMenuStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FContextMenuStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UContextMenuWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FContextMenuStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
