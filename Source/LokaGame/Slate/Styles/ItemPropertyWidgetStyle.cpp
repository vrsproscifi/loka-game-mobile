// VRSPRO

#include "LokaGame.h"
#include "ItemPropertyWidgetStyle.h"


FItemPropertyStyle::FItemPropertyStyle()
{
}

FItemPropertyStyle::~FItemPropertyStyle()
{
}

const FName FItemPropertyStyle::TypeName(TEXT("FItemPropertyStyle"));

const FItemPropertyStyle& FItemPropertyStyle::GetDefault()
{
	static FItemPropertyStyle Default;
	return Default;
}

void FItemPropertyStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

