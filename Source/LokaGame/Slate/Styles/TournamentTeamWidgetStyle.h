// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"
#include "TextBlockButtonWidgetStyle.h"
#include "TournamentTeamWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct LOKAGAME_API FTournamentTeamStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FTournamentTeamStyle();
	virtual ~FTournamentTeamStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FTournamentTeamStyle& GetDefault();

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle HeaderText;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle SubHeaderTexts;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockButtonStyle TextBlockButton;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FButtonStyle Buttons;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle ButtonsTexts;

	UPROPERTY(Category = Configuration, EditAnywhere)
		float BalanceDPI;
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UTournamentTeamWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FTournamentTeamStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
