#pragma once

#include "SlateBasics.h"
#include "SlateExtras.h"

//class FLokaSlateStyle;

class LOKAGAME_API FLokaStyle
{
public:

	static void Initialize();

	static void Shutdown();

	static void ReloadTextures();

	static const ISlateStyle& Get();
	//static TSharedRef<FLokaSlateStyle> GetAlt();

	static FName GetStyleSetName();

	static void ReGenerateContextMenuStyle()
	{
#if WITH_EDITOR
		ReGenerateContextMenuStyle(StyleInstance.ToSharedRef());
#endif
	}

	static void ReGenerateContextMenuStyle(TSharedRef<class FSlateStyleSet> InStyle);

private:

	static TSharedRef< class FSlateStyleSet > Create();

private:

	static TSharedPtr< class FSlateStyleSet > StyleInstance;
};

