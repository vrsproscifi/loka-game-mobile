// VRSPRO

#include "LokaGame.h"
#include "SGeneralBackground.h"
#include "SBackgroundBlur.h"
#include "Styles/BackgroundBlurWidgetStyle.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SGeneralBackground::Construct(const FArguments& InArgs)
{
	BackgroundStyle = InArgs._BackgroundStyle;

	ShowAnimation = InArgs._ShowAnimation;
	HideAnimation = InArgs._HideAnimation;

	OnGeneralButtonClick = InArgs._OnGeneralButtonClick;

	BlurStyle = &FLokaStyle::Get().GetWidgetStyle<FBackgroundBlurStyle>("Default_BackgroundBlur");

	ChildSlot
	[
		SNew(SBackgroundBlur)
		.bApplyAlphaToBlur(BlurStyle->bApplyAlphaToBlur)
		.BlurStrength(this, &SGeneralBackground::WidgetBlurStrength)
		.BlurRadius(BlurStyle->BlurRadius)
		.LowQualityFallbackBrush(&BlurStyle->LowQualityFallbackBrush)
		[
			SNew(SBox)
			.Padding(BackgroundStyle->Padding)
			[
				SNew(SBorder)
				.BorderImage(&BackgroundStyle->Background)
				.Padding(BackgroundStyle->BorderPadding)
				.RenderTransformPivot(FVector2D(0.5f, 0.5f))
				.RenderTransform(this, &SGeneralBackground::WidgetTrans)
				.BorderBackgroundColor(this, &SGeneralBackground::WidgetColor)
				.ColorAndOpacity(this, &SGeneralBackground::WidgetLineColor)
				[
					SNew(SBorder)
					.BorderImage(&BackgroundStyle->Border)
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot()
						.AutoHeight()
						[
							SNew(SBox)
							.HeightOverride(BackgroundStyle->HeaderSize)
							[
								SNew(SHorizontalBox)
								+ SHorizontalBox::Slot()
								.FillWidth(1)
								.VAlign(EVerticalAlignment::VAlign_Center)
								[
									SNew(STextBlock)
									.Text(InArgs._Header)
									.TextStyle(&BackgroundStyle->HeaderText)
									.Margin(BackgroundStyle->HeaderTextMargin)
								]
								+ SHorizontalBox::Slot()
								.AutoWidth()
								.HAlign(EHorizontalAlignment::HAlign_Fill)
								.VAlign(EVerticalAlignment::VAlign_Fill)
								.Padding(BackgroundStyle->ButtonsPadding)
								[
									SNew(SHorizontalBox)
									+ SHorizontalBox::Slot()
									.AutoWidth()
									.HAlign(EHorizontalAlignment::HAlign_Center)
									.VAlign(EVerticalAlignment::VAlign_Center)
									.Padding(BackgroundStyle->ButtonPadding)
									[
										SNew(SButton)
										.ButtonStyle(InArgs._IsFirstButtonSettings ? &BackgroundStyle->SettingButton : &BackgroundStyle->HelperButton)
										.OnClicked(this, &SGeneralBackground::OnButtonClickSender, InArgs._IsFirstButtonSettings ? EButtonType::Type::Settings : EButtonType::Type::Helper)
									]
									+ SHorizontalBox::Slot()
									.AutoWidth()
									[
										SNew(SSpacer).Size(FVector2D(BackgroundStyle->ButtonsOffset.X, 0))
									]
									+ SHorizontalBox::Slot()
									.AutoWidth()
									.HAlign(EHorizontalAlignment::HAlign_Center)
									.VAlign(EVerticalAlignment::VAlign_Center)
									.Padding(BackgroundStyle->ButtonPadding)
									[
										SNew(SButton)
										.ButtonStyle(&BackgroundStyle->CloseButton)
										.OnClicked(this, &SGeneralBackground::OnButtonClickSender, EButtonType::Type::Close)
									]
								]
								+ SHorizontalBox::Slot()
								.AutoWidth()
								[
									SNew(SSpacer).Size(FVector2D(BackgroundStyle->ButtonsOffset.Y, 0))
								]
							]
						]
						+ SVerticalBox::Slot()
						.FillHeight(1)
						.HAlign(InArgs._HAlign)
						.VAlign(InArgs._VAlign)
						.Padding(InArgs._Padding)
						[
							InArgs._Content.Widget
						]				
					]
				]
			]
		]
	];

	IsToggled = false;
	CurrentAnim = EGBAnimation::Type::None;

	ColorHandle = AnimSequence.AddCurve(0.2f, 1.3f, ECurveEaseFunction::QuadInOut);
	TransHandle = AnimSequence.AddCurve(0.0f, 1.5f, ECurveEaseFunction::QuadInOut);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SGeneralBackground::ToggleWidget(const bool _Toggle)
{
	if (AnimSequence.IsPlaying())
	{
		CurrentAnim = _Toggle ? ShowAnimation.Get() : HideAnimation.Get();
		AnimSequence.Reverse();
	}
	else
	{
		if (_Toggle)
		{
			if (!!ShowAnimation.Get())
			{
				CurrentAnim = ShowAnimation.Get();
				AnimSequence.Play(this->AsShared());
			}

			SGeneralBackground::SetVisibility(EVisibility::SelfHitTestInvisible);
		}
		else
		{
			if (!!HideAnimation.Get())
			{
				CurrentAnim = HideAnimation.Get();
				AnimSequence.PlayReverse(this->AsShared());
			}
			else
			{
				SGeneralBackground::SetVisibility(EVisibility::Hidden);
			}
		}
	}

	IsToggled = _Toggle;
	OnToggleInteractive.ExecuteIfBound(IsToggled);
}

bool SGeneralBackground::IsInInteractiveMode() const
{
	return IsToggled;
}

void SGeneralBackground::SetOnGeneralButtonClick(FOnGeneralButtonClick _OnGeneralButtonClick)
{
	OnGeneralButtonClick = _OnGeneralButtonClick;
}

void SGeneralBackground::SetShowAnimation(const TAttribute<EGBAnimation::Type> InAnimation)
{
	ShowAnimation = InAnimation;
}

void SGeneralBackground::SetHideAnimation(const TAttribute<EGBAnimation::Type> InAnimation)
{
	HideAnimation = InAnimation;
}

FReply SGeneralBackground::OnButtonClickSender(const EButtonType::Type _Type)
{
	OnGeneralButtonClick.ExecuteIfBound(_Type);
	return FReply::Unhandled();
}

FSlateColor SGeneralBackground::WidgetColor() const
{
	return FSlateColor(FLinearColor(1.0f, 1.0f, 1.0f, ColorHandle.GetLerp()));
}

FLinearColor SGeneralBackground::WidgetLineColor() const
{
	return FLinearColor(1.0f, 1.0f, 1.0f, ColorHandle.GetLerp());
}

TOptional<FSlateRenderTransform> SGeneralBackground::WidgetTrans() const
{
	switch (CurrentAnim)
	{		
		case EGBAnimation::LeftFade: return FSlateRenderTransform(FVector2D(FMath::Lerp(-1920.0f, 0.0f, TransHandle.GetLerp()), 0.0f));
		case EGBAnimation::RightFade: return FSlateRenderTransform(FVector2D(FMath::Lerp(1920.0f, 0.0f, TransHandle.GetLerp()), 0.0f));
		case EGBAnimation::UpFade: return FSlateRenderTransform(FVector2D(0.0f, FMath::Lerp(-1080.0f, 0.0f, TransHandle.GetLerp())));
		case EGBAnimation::DownFade: return FSlateRenderTransform(FVector2D(0.0f, FMath::Lerp(1080.0f, 0.0f, TransHandle.GetLerp())));
		case EGBAnimation::Type::ZoomIn: return FSlateRenderTransform(FScale2D(TransHandle.GetLerp()));
		case EGBAnimation::ZoomOut: return FSlateRenderTransform(FScale2D(FMath::Lerp(2.0f, 1.0f, TransHandle.GetLerp())));
		default: break;
	}
	//
	return FSlateRenderTransform();
}

float SGeneralBackground::WidgetBlurStrength() const
{
	return FMath::Lerp(.0f, BlurStyle ? BlurStyle->BlurStrength : 10.0f, ColorHandle.GetLerp());
}