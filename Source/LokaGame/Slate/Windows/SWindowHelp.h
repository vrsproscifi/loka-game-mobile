// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SWindowBase.h"

class STabbedContainer;
/**
 * 
 */
class LOKAGAME_API SWindowHelp : public SWindowBase
{
public:
	SLATE_BEGIN_ARGS(SWindowHelp)
	{
		_Visibility = EVisibility::Collapsed;
	}
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, TSharedRef<SWindowsContainer> InOwner);

protected:

	TSharedPtr<STabbedContainer> Widget_Container;

	void Load();
};
