// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SWindowHelp.h"
#include "SlateOptMacros.h"
#include "WindowHelpContent.h"
#include "GameSingleton.h"
#include "Containers/STabbedContainer.h"
#include "Decorators/LokaTextDecorator.h"
#include "Decorators/LokaResourceDecorator.h"
#include "Decorators/LokaKeyDecorator.h"
#include "Decorators/LokaSpaceDecorator.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SWindowHelp::Construct(const FArguments& InArgs, TSharedRef<SWindowsContainer> InOwner)
{
	MAKE_UTF8_SYMBOL(sHelp, 0xf059);

	auto SuperArgs = SWindowBase::FArguments()
	.StartPosition(FVector2D(860, 400))
	.StartSize(FVector2D(600, 400))
	.SizeLimits(FBox2D(FVector2D(400, 200), FVector2D(1900, 1000)))
	.TitleText(NSLOCTEXT("SWindowHelp", "SWindowHelp.Title", "Help"))
	.QuickButton(FText::FromString(sHelp))
	.QuickButtonOrder(0)
	.CanInvisable(false)
	.Content()
	[
		SAssignNew(Widget_Container, STabbedContainer)
	];

	SWindowBase::Construct(SuperArgs, InOwner);

	Load();
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SWindowHelp::Load()
{
	Widget_Container->ClearChildren();

	TArray<FAssetData> AssetData;
	GetAllAssetData(UWindowHelpContent::StaticClass(), AssetData);

	for (auto Asset : AssetData)
	{
		if (auto TargetAsset = Cast<UWindowHelpContent>(Asset.GetAsset()))
		{
			for (auto ContentPair : TargetAsset->Content)
			{
				Widget_Container->AddSlotUnique(STabbedContainer::Slot().Name(TargetAsset->Category[ContentPair.Key])[
					SNew(SScrollBox).ScrollBarStyle(&FLokaStyle::Get().GetWidgetStyle<FScrollBarStyle>(TEXT("Default_ScrollBar")))
					+ SScrollBox::Slot()
					[
						SNew(SRichTextBlock)
						.Text(ContentPair.Value)
						.AutoWrapText(true)
						.TextStyle(&FLokaStyle::Get().GetWidgetStyle<FTextBlockStyle>(TEXT("Default_TextBlock")))
						.Justification(ETextJustify::Left)
						.DecoratorStyleSet(&FLokaStyle::Get())
						+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(FLokaStyle::Get().GetWidgetStyle<FTextBlockStyle>(TEXT("Default_TextBlock"))))
						+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
						+ SRichTextBlock::Decorator(FLokaKeyDecorator::Create())
						+ SRichTextBlock::Decorator(FLokaSpaceDecorator::Create())
						+ SRichTextBlock::Decorator(FHyperlinkDecorator::Create(TEXT(""), FSlateHyperlinkRun::FOnClick::CreateLambda([&](const FSlateHyperlinkRun::FMetadata& InData) {
							if (InData.Contains("url")) FPlatformProcess::LaunchURL(*InData.FindChecked("url"), nullptr, nullptr);
						})))
					]
				]);
			}
		}
	}
}