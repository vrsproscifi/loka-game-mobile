// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SWindowQuit.h"
#include "SlateOptMacros.h"
#include "SMessageBox.h"
#include "SWindowsContainer.h"
#include "BasePlayerController.h"
#include "StringTableRegistry.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SWindowQuit::Construct(const FArguments& InArgs, TSharedRef<SWindowsContainer> InOwner)
{
	MAKE_UTF8_SYMBOL(sQuit, 0xf011);

	SWindowBase::Construct(
			SWindowBase::FArguments()
			.SizeLimits(FBox2D(FVector2D(0, 0), FVector2D(0, 0)))
			.StartPosition(FVector2D(0, 0))
			.StartSize(FVector2D(0, 0))
			.QuickButton(FText::FromString(sQuit))
			.QuickButtonOrder(128)
			.CanClosable(false)
			.CanInvisable(false)
			.IsAlwaysOnTop(true)
			.InitAsClosed(true)
			.TitleText(NSLOCTEXT("SLobbyContainer", "Lobby.Exit", "Exit"))
		,
			InOwner
		);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SWindowQuit::ToggleWidget(const bool InToggle, const bool IsAlternative)
{
	if (InToggle)
	{
		QueueMessageBegin("Quit")

			UWorld* TargetWorld = nullptr;
			ABasePlayerController* LocalController = nullptr;

			for (auto &WorldContext : GEngine->GetWorldContexts())
			{
				if (WorldContext.WorldType == EWorldType::Game || WorldContext.WorldType == EWorldType::GamePreview || WorldContext.WorldType == EWorldType::PIE)
				{
					TargetWorld = WorldContext.World();
					break;
				}
			}

			if (TargetWorld && TargetWorld->GetFirstPlayerController())
			{
				if (auto MyCtrl = Cast<ABasePlayerController>(TargetWorld->GetFirstPlayerController()))
				{
					LocalController = MyCtrl;
				}
			}

			SMessageBox::Get()->SetHeaderText(NSLOCTEXT("SLobbyContainer", "Lobby.Exit", "Exit"));

			if (LocalController && LocalController->GetNetMode() == NM_Standalone)
			{
				SMessageBox::Get()->SetContent(NSLOCTEXT("SLobbyContainer", "Lobby.Exit.Content", "You seriously want to exit game?"));
			}
			else
			{
				SMessageBox::Get()->SetContent(NSLOCTEXT("SLobbyContainer", "Lobby.Exit.Content.Coop", "You seriously want to exit out of cooperative construction?"));
			}
			
			SMessageBox::Get()->SetButtonsText(LOCTABLE("BaseStrings", "All.Yes"), LOCTABLE("BaseStrings", "All.No"));
			SMessageBox::Get()->SetEnableClose(true);
			SMessageBox::Get()->OnMessageBoxButton.AddLambda([&, c = LocalController](const EMessageBoxButton& Button) {
				if (Button == EMessageBoxButton::Left)
				{					
					if (c)
					{
						if (c->GetNetMode() == NM_Standalone)
						{
							c->ConsoleCommand("quit");
						}
						else
						{
							c->ServerRequestLeave(true);
						}							
					}
				}

				SMessageBox::Get()->ToggleWidget(false);
			});
			SMessageBox::Get()->ToggleWidget(true);
		QueueMessageEnd
	}
}