// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SWindowBase.h"

class LOKAGAME_API SWindowQuickPanel : public SWindowBase
{
	friend class SWindowsContainer;

public:

	SLATE_BEGIN_ARGS(SWindowQuickPanel)
	{
		_Visibility = EVisibility::Visible;
	}
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs, TSharedRef<SWindowsContainer> InOwner);

	virtual bool HasQuickButton() const override { return false; }

	void Refresh();

protected:

	TSharedPtr<SWrapBox> Widget_ButtonsContainer;
	
};
