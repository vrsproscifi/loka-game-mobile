// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SWindowQuickPanel.h"
#include "SlateOptMacros.h"

#include "SWindowsContainer.h"


BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SWindowQuickPanel::Construct(const FArguments& InArgs, TSharedRef<SWindowsContainer> InOwner)
{
	SWindowBase::Construct(
			SWindowBase::FArguments()
			.SizeLimits(FBox2D(FVector2D(56, 56), FVector2D(500, 500)))
			.StartPosition(FVector2D(0, 500))
			.StartSize(FVector2D(56, 344))
			.CanClosable(false)
			.CanInvisable(true)
			.IsAlwaysOnTop(true)
			.InitAsInvisible(true)
			//.TitleText(NSLOCTEXT("Slate", "Slate.QuickPanel.Title", "Quick Panel"))
			.Content()
			[
				SAssignNew(Widget_ButtonsContainer, SWrapBox)
				.Clipping(EWidgetClipping::ClipToBoundsAlways)
				.UseAllottedWidth(true)
				.InnerSlotPadding(FVector2D(2, 2))
			]
		,
			InOwner
		);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SWindowQuickPanel::Refresh()
{
	Widget_ButtonsContainer->ClearChildren();

	TArray<TSharedRef<SWindowBase>> WidgetsArray;

	const int32 NumSlots = OwnerRef->GetChildren()->Num();
	for (int32 SlotIndex = 0; SlotIndex < NumSlots; ++SlotIndex)
	{		
		WidgetsArray.Add(StaticCastSharedRef<SWindowBase>(OwnerRef->GetChildren()->GetChildAt(SlotIndex)));
	}

	WidgetsArray.Sort([](const TSharedRef<SWindowBase>& InA, const TSharedRef<SWindowBase>& InB) -> bool { return InA->QOrder < InB->QOrder; });

	for (auto MyChild : WidgetsArray)
	{
		if (MyChild != SNullWidget::NullWidget && MyChild->HasQuickButton())
		{
			Widget_ButtonsContainer->AddSlot()[MyChild->GetToggableButton()];
		}
	}
}