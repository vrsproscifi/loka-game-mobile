// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SWindowGuestMode.h"
#include "SlateOptMacros.h"
#include "Decorators/LokaTextDecorator.h"
#include "Styles/TutorialSelectorWidgetStyle.h"
#include "ConstructionComponent.h"
#include "RichTextHelpers.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SWindowGuestMode::Construct(const FArguments& InArgs, TSharedRef<SWindowsContainer> InOwner)
{
	Style = InArgs._Style;
	Data = InArgs._Data;
	OnNextLocation = InArgs._OnNextLocation;

	const auto SubStyle = &FLokaStyle::Get().GetWidgetStyle<FTutorialSelectorStyle>("STutorialSelectorStyle");

	auto SuperArgs = SWindowBase::FArguments()
		.SizeLimits(FBox2D(FVector2D(420, 100), FVector2D(680, 180)))
		.StartPosition(FVector2D(20.0f, 960))
		.TitleText(NSLOCTEXT("SWindowGuestMode", "SWindowGuestMode.Title", "Guest Mode"))
		.CanClosable(false)
		.CanInvisable(true)
		.Content()
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().FillHeight(1)
			[
				SNew(SRichTextBlock)
				.AutoWrapText(true)
				.TextStyle(&FLokaStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_TextBlock"))
				.Text(this, &SWindowGuestMode::GetDescription)
				.Justification(ETextJustify::Center)
				.DecoratorStyleSet(&FLokaStyle::Get())
				+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(FLokaStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_TextBlock")))
			]
			+ SVerticalBox::Slot().AutoHeight()
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot()
				[
					SNew(SButton) // Prev
					.TextStyle(&SubStyle->Text)
					.ButtonStyle(&SubStyle->Button)
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					.ContentPadding(FMargin(4, 20))
					.Text(this, &SWindowGuestMode::GetPrevName)
					.OnClicked(this, &SWindowGuestMode::OnClickedHandler, false)
				]
				+ SHorizontalBox::Slot()
				[
					SNew(SButton) // Next
					.TextStyle(&SubStyle->Text)
					.ButtonStyle(&SubStyle->Button)
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					.ContentPadding(FMargin(4, 20))
					.Text(this, &SWindowGuestMode::GetNextName)
					.OnClicked(this, &SWindowGuestMode::OnClickedHandler, true)
				]
			]
		];

	SWindowBase::Construct(SuperArgs, InOwner);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FText SWindowGuestMode::GetDescription() const
{
	if (Data.IsSet() && Data.Get())
	{
		return FText::Format(NSLOCTEXT("SWindowGuestMode", "SWindowGuestMode.Desc", "You are into guest mode on \"{0}\" player location.\nYou can choice next player location below."), 
			FRichHelpers::TextHelper_NotifyValue.SetValue(Data.Get()->CurrentName).ToText());
	}

	return FText::GetEmpty();
}

FText SWindowGuestMode::GetPrevName() const
{
	if (Data.IsSet() && Data.Get())
	{
		return FText::FromString(Data.Get()->PrevName);
	}

	return FText::GetEmpty();
}

FText SWindowGuestMode::GetNextName() const
{
	if (Data.IsSet() && Data.Get())
	{
		return FText::FromString(Data.Get()->NextName);
	}

	return FText::GetEmpty();
}

FReply SWindowGuestMode::OnClickedHandler(const bool IsNext)
{
	if (Data.IsSet() && Data.Get())
	{
		OnNextLocation.ExecuteIfBound(IsNext ? Data.Get()->NextId : Data.Get()->PrevId);
	}

	return FReply::Handled();
}
