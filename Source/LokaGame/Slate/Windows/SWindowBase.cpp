// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SWindowBase.h"
#include "SlateOptMacros.h"

#include "SBackgroundBlur.h"
#include "SWindowsContainer.h"
#include "Styles/BackgroundBlurWidgetStyle.h"
#include "SLokaToolTip.h"

#pragma region WindowHelpers

FWindowBaseRect::FWindowBaseRect()
	: FSlateRect()
{
	
}

FWindowBaseRect::FWindowBaseRect(const FVector2D& InPosition, const FVector2D& InSize)
	: FSlateRect(FSlateRect::FromPointAndExtent(InPosition, InSize))
{
	
}

FWindowBaseRect& FWindowBaseRect::ClampSizeToLimits(const FVector2D& InMin, const FVector2D& InMax)
{
	auto localSize = GetSize();
	localSize.X = FMath::Clamp(localSize.X, InMin.X, InMax.X);
	localSize.Y = FMath::Clamp(localSize.Y, InMin.Y, InMax.Y);
	
	Right = Left + localSize.X;
	Bottom = Top + localSize.Y;

	return *this;
}

FWindowBaseRect& FWindowBaseRect::ClampSizeToLimits(const FBox2D& InLimits)
{
	return ClampSizeToLimits(InLimits.Min, InLimits.Max);
}

FWindowBaseRect& FWindowBaseRect::ExtendBy(const FMargin& InValue)
{
	*this = FSlateRect::ExtendBy(InValue);
	return *this;
}

FWindowBaseRect& FWindowBaseRect::ExtendByWithLimits(const FMargin& InValue, const FBox2D& InLimits)
{
	FWindowBaseRect OldRect = *this;
	ExtendBy(InValue);

	if (Left != OldRect.Left || Top != OldRect.Top)
	{
		const float TopSize = Bottom - Top;
		const float LeftSize = Right - Left;

		if (TopSize > InLimits.Max.Y || TopSize < InLimits.Min.Y)
		{
			Bottom = OldRect.Bottom;
			Top = OldRect.Top;
		}

		if (LeftSize > InLimits.Max.X || LeftSize < InLimits.Min.X)
		{
			Right = OldRect.Right;
			Left = OldRect.Left;
		}
	}
	else
	{
		ClampSizeToLimits(InLimits);
	}

	return *this;
}

FWindowBaseRect& FWindowBaseRect::operator=(const FSlateRect& InValue)
{
	Left = InValue.Left;
	Right = InValue.Right;
	Top = InValue.Top;
	Bottom = InValue.Bottom;
	return *this;
}

#pragma endregion 

SWindowBase::SWindowBase() 
	: OwnerRef(StaticCastSharedRef<SWindowsContainer>(SNullWidget::NullWidget))
	, ZOrder(0)
	, LastVersion(1)
{
	
}

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SWindowBase::Construct(const FArguments& InArgs, TSharedRef<SWindowsContainer> InOwner)
{
	EmptyBrush = new FSlateNoResource();

	if (InArgs._SaveTag != NAME_None)
	{
		SaveTag = InArgs._SaveTag;
	}
	else
	{
		SaveTag = GetType();
	}

	Style = InArgs._Style;
	OwnerRef = InOwner;

	CurrentMoveType = MT_None;
	CurrentSizeType = ST_None;
	ActionOffset = FVector2D::ZeroVector;
	QOrder = InArgs._QuickButtonOrder;

	Attr_CanClosable	= InArgs._CanClosable;
	Attr_CanInvisable	= InArgs._CanInvisable;
	Attr_IsAlwaysOnTop	= InArgs._IsAlwaysOnTop;

	SetFlashing(false);
	SetSizeLimits(InArgs._SizeLimits, true);
	InternalSetSize(InArgs._StartSize, true);
	InternalSetPosition(InArgs._StartPosition, true);
	SetQuickButton(InArgs._QuickButton);

	auto BackgroundBlurStyle = &FLokaStyle::Get().GetWidgetStyle<FBackgroundBlurStyle>("Default_BackgroundBlur");

	ChildSlot
	[
		SNew(SBox).Tag(*(SaveTag.ToString() + ".Window"))
		.MinDesiredWidth_Lambda([&]() { return Attr_SizeLimits.Get().Min.X; })
		.MinDesiredHeight_Lambda([&]() { return Attr_SizeLimits.Get().Min.Y; })
		.MaxDesiredWidth_Lambda([&]() { return Attr_SizeLimits.Get().Max.X; })
		.MaxDesiredHeight_Lambda([&]() { return Attr_SizeLimits.Get().Max.Y; })
		[
			SNew(SBackgroundBlur)
			.bApplyAlphaToBlur(BackgroundBlurStyle->bApplyAlphaToBlur)
			.BlurRadius_Lambda([&, BS = BackgroundBlurStyle]() -> TOptional<int32> { return bIsInvisible ? 0 : BS->BlurRadius; })
			.LowQualityFallbackBrush(&BackgroundBlurStyle->LowQualityFallbackBrush)
			.BlurStrength_Lambda([&, BS = BackgroundBlurStyle]() -> float { return bIsInvisible ? 0 : BS->BlurStrength; })
			.Padding(2)
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot().AutoHeight()
				[
					SNew(SBox).HeightOverride(40)
					[
						SNew(SBorder)
						.VAlign(VAlign_Center)
						.HAlign(HAlign_Fill)
						.BorderImage_Lambda([&]() -> const FSlateBrush*{ return bIsInvisible ? EmptyBrush : &Style->Header.Background; })
						.Padding(0)
						[
							SNew(SHorizontalBox)
							+ SHorizontalBox::Slot()
							.VAlign(VAlign_Center)
							.HAlign(HAlign_Left)
							[
								SAssignNew(Widget_TitleContainer, SBox)								
							]
							+ SHorizontalBox::Slot().AutoWidth()
							.VAlign(VAlign_Center)
							.HAlign(HAlign_Right)
							.Padding(FMargin(0, 0, 10, 0))
							[
								SAssignNew(Widget_ControllsContainer, SHorizontalBox)
							]
						]
					]
				]
				+ SVerticalBox::Slot()
				[
					SAssignNew(Widget_ContentContainer, SBorder)
					.BorderImage_Lambda([&]() -> const FSlateBrush*{ return bIsInvisible ? EmptyBrush : &Style->Background; })
				]
			]
		]
	];

	if (InArgs._Title.Widget != SNullWidget::NullWidget)
	{
		SetTitle(InArgs._Title.Widget);
	}
	else
	{
		SetTitle(InArgs._TitleText);
	}

	SetContent(InArgs._Content.Widget);

	OnWindowControllsGeneration();

	InternalToggleMode(InArgs._InitAsInvisible);
	InternalToggleWidget(!InArgs._InitAsClosed);

	LoadState();
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FVector2D SWindowBase::GetPosition() const
{
	return CurrentPosition;
}

FVector2D SWindowBase::GetSize() const
{
	return CurrentSize;
}

void SWindowBase::SetTitle(const TAttribute<FText>& InTitle)
{
	Widget_TitleContainer->SetContent(
		SNew(STextBlock)
		.TextStyle(&Style->Header.Text)
		.Margin(FMargin(10, 2, 0, 2))
		.Justification(ETextJustify::Left)
		.Text(InTitle)
	);
}

void SWindowBase::SetTitle(const TSharedRef<SWidget> InTitleWidget)
{
	Widget_TitleContainer->SetContent(InTitleWidget);
}

void SWindowBase::SetContent(const TSharedRef<SWidget> InContentWidget)
{
	Widget_ContentContainer->SetContent(InContentWidget);
}

void SWindowBase::SetPosition(const FVector2D& InPosition, const bool InIsForce)
{
	InternalSetPosition(InPosition, InIsForce);
	SaveState();
}

void SWindowBase::SetSize(const FVector2D& InSize, const bool InIsForce)
{
	InternalSetSize(InSize, InIsForce);
	SaveState();
}

void SWindowBase::SetSizeLimits(const TAttribute<FBox2D>& InSizeLimits, const bool InIsForce)
{
	Attr_SizeLimits = InSizeLimits;

	TargetSize.X = FMath::Clamp(TargetSize.X, Attr_SizeLimits.Get().Min.X, Attr_SizeLimits.Get().Max.X);
	TargetSize.Y = FMath::Clamp(TargetSize.Y, Attr_SizeLimits.Get().Min.Y, Attr_SizeLimits.Get().Max.Y);

	if (InIsForce)
	{
		CurrentSize = TargetSize;
	}
}

void SWindowBase::SetQuickButton(const TAttribute<FText>& InQuickButton)
{
	Attr_QuickButton = InQuickButton;
}

void SWindowBase::SetFlashing(const bool InToggle, const float InTime)
{
	bIsFlashing = InToggle;
	if (bIsFlashing && InTime > .1f)
	{
		RegisterActiveTimer(InTime, FWidgetActiveTimerDelegate::CreateLambda([&](double InCurrentTime, float InDeltaTime) -> EActiveTimerReturnType {
			SetFlashing(false);
			return EActiveTimerReturnType::Stop;
		}));
	}
}

void SWindowBase::ToggleWidget(const bool InToggle)
{
	ToggleWidget(InToggle, false);
}

void SWindowBase::ToggleWidget(const bool InToggle, const bool IsAlternative)
{
	SetFlashing(false);

	if (CanClosable())
	{
		SetVisibility(InToggle ? EVisibility::Visible : EVisibility::Collapsed);

		if (OwnerRef != SNullWidget::NullWidget)
		{
			if (InToggle)
			{
				ZOrder = ZOrder + OwnerRef->GetChildren()->Num();
				OwnerRef->RequestZSort();
			}

			if (IsAlternative == false)
			{
				OwnerRef->RequestToggle(InToggle);
			}
		}

		SaveState();
	}
}

bool SWindowBase::IsInInteractiveMode() const
{
	return GetVisibility() == EVisibility::Visible;
}

void SWindowBase::ToggleMode(const bool InToggle)
{
	if (CanInvisable())
	{
		bIsInvisible = InToggle;
		Widget_TitleContainer->SetVisibility(InToggle ? EVisibility::Hidden : EVisibility::SelfHitTestInvisible);
		SaveState();
	}
}

bool SWindowBase::CanClosable() const
{
	return Attr_CanClosable.Get(true);
}

bool SWindowBase::CanInvisable() const
{
	return Attr_CanInvisable.Get(true);
}

bool SWindowBase::HasQuickButton() const
{
	return Attr_QuickButton.IsSet() && Attr_QuickButton.Get().IsEmpty() == false;
}

bool SWindowBase::IsAlwaysOnTop() const
{
	return Attr_IsAlwaysOnTop.Get(false);
}

uint64 SWindowBase::GetZOrder() const
{
	return ZOrder;
}

TSharedRef<SWidget> SWindowBase::GetToggableButton()
{
	return SNew(SCheckBox).Tag(*(SaveTag.ToString() + ".QuickButton"))
		.ToolTip(SNew(SLokaToolTip)[Widget_TitleContainer->GetChildren()->GetChildAt(0)])
		.Style(&Style->DefaultQuickButton.Button)
		.IsChecked_Lambda([&]() { return IsFlashing() ? ECheckBoxState::Undetermined : IsInInteractiveMode() ? ECheckBoxState::Checked : ECheckBoxState::Unchecked; })
		.OnCheckStateChanged_Lambda([&](ECheckBoxState InState) { ToggleWidget(!IsInInteractiveMode(), true); })
		.Type(ESlateCheckBoxType::ToggleButton)
		[
			SNew(SBox)
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Center)
			.WidthOverride(46)
			.HeightOverride(46)
			[
				SNew(STextBlock)
				.TextStyle(&Style->DefaultQuickButton.Awesome)
				.Text(Attr_QuickButton.IsSet() ? Attr_QuickButton : FText::GetEmpty())
				.Margin(4)
			]
		];
}

void SWindowBase::OnWindowControllsGeneration()
{
	Widget_ControllsContainer->ClearChildren();

	if (CanInvisable())
	{
		MAKE_UTF8_SYMBOL(sMode, 0xf192);

		Widget_ControllsContainer->AddSlot()
			[
				SNew(SButton)
				.ButtonStyle(&Style->Controlls.ButtonMode)
				.TextStyle(&Style->Controlls.Awesome)
				.Cursor(EMouseCursor::Default)
				.Text(FText::FromString(sMode))
				.OnClicked_Lambda([&]() {
					ToggleMode(!bIsInvisible);
					return FReply::Handled();
				})
			];
	}

	if (CanClosable())
	{
		MAKE_UTF8_SYMBOL(sClose, 0xf00d);

		Widget_ControllsContainer->AddSlot()
			[
				SNew(SButton)
				.ButtonStyle(&Style->Controlls.ButtonClose)
				.TextStyle(&Style->Controlls.Awesome)
				.Cursor(EMouseCursor::Default)
				.Text(FText::FromString(sClose))
				.OnClicked_Lambda([&]()
				{
					ToggleWidget(false);
					return FReply::Handled();
				})
			];
	}
}

void SWindowBase::Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime)
{
	SCompoundWidget::Tick(AllottedGeometry, InCurrentTime, InDeltaTime);

	if (OwnerRef != SNullWidget::NullWidget && OwnerRef->GetCachedGeometry().GetLayoutBoundingRect().IsValid())
	{
		TargetPosition.X = FMath::Clamp(TargetPosition.X, FVector2D::ZeroVector.X, OwnerRef->GetCachedGeometry().GetLocalSize().X - TargetSize.X);
		TargetPosition.Y = FMath::Clamp(TargetPosition.Y, FVector2D::ZeroVector.Y, OwnerRef->GetCachedGeometry().GetLocalSize().Y - TargetSize.Y);

		TargetSize.X = FMath::Clamp(TargetSize.X, Attr_SizeLimits.Get().Min.X, Attr_SizeLimits.Get().Max.X);
		TargetSize.Y = FMath::Clamp(TargetSize.Y, Attr_SizeLimits.Get().Min.Y, Attr_SizeLimits.Get().Max.Y);		
	}

	CurrentPosition = FMath::Vector2DInterpTo(CurrentPosition, TargetPosition, InDeltaTime, 20.0f);
	CurrentSize = FMath::Vector2DInterpTo(CurrentSize, TargetSize, InDeltaTime, 20.0f);
}

FCursorReply SWindowBase::OnCursorQuery(const FGeometry& MyGeometry, const FPointerEvent& CursorEvent) const
{
	auto DetectedZone = (CurrentSizeType != ST_None) ? CurrentSizeType : DetectZone(MyGeometry.AbsoluteToLocal(CursorEvent.GetScreenSpacePosition()));
	if (DetectedZone != ST_None)
	{
		EMouseCursor::Type localCursor = EMouseCursor::Default;
		switch (DetectedZone) 
		{ 
			case ST_RightDown:
			case ST_LeftUp:		localCursor = EMouseCursor::ResizeSouthEast; break;
			case ST_LeftDown:
			case ST_RightUp:	localCursor = EMouseCursor::ResizeSouthWest; break;
			case ST_Left:
			case ST_Right:		localCursor = EMouseCursor::ResizeLeftRight; break;
			case ST_Up:
			case ST_Down:		localCursor = EMouseCursor::ResizeUpDown; break;
			case ST_Title:		localCursor = EMouseCursor::CardinalCross; break;
		}

		return FCursorReply::Cursor(localCursor);
	}

	return FCursorReply::Unhandled();
}

FReply SWindowBase::OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
	if (OwnerRef != SNullWidget::NullWidget)
	{
		auto DetectedZone = DetectZone(MyGeometry.AbsoluteToLocal(MouseEvent.GetScreenSpacePosition()));

		if (DetectedZone != ST_None)
		{
			ZOrder = ZOrder + OwnerRef->GetChildren()->Num();
			OwnerRef->RequestZSort();
		}

		if (DetectedZone != ST_None && DetectedZone != ST_Title)
		{
			CurrentMoveType = MT_Size;
			CurrentSizeType = DetectedZone;
			return FReply::Handled().CaptureMouse(this->AsShared());
		}
		else if (DetectedZone == ST_Title)
		{
			ActionOffset = TargetPosition - OwnerRef->GetCachedGeometry().AbsoluteToLocal(MouseEvent.GetScreenSpacePosition());
			CurrentMoveType = MT_Move;
			CurrentSizeType = DetectedZone;
			return FReply::Handled().CaptureMouse(this->AsShared());
		}
	}

	return SCompoundWidget::OnMouseButtonDown(MyGeometry, MouseEvent);
}

FReply SWindowBase::OnMouseButtonUp(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
	if (CurrentMoveType != MT_None)
	{
		CurrentMoveType = MT_None;
		CurrentSizeType = ST_None;
		SaveState();
		return FReply::Handled().ReleaseMouseCapture();
	}

	return SCompoundWidget::OnMouseButtonUp(MyGeometry, MouseEvent);
}

FReply SWindowBase::OnMouseMove(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
	if (CurrentMoveType != MT_None && OwnerRef != SNullWidget::NullWidget)
	{
		if (CurrentMoveType == MT_Move)
		{
			TargetPosition = OwnerRef->GetCachedGeometry().AbsoluteToLocal(MouseEvent.GetScreenSpacePosition()) + ActionOffset;
		}
		else if (CurrentMoveType == MT_Size)
		{
			FVector2D MousePosition = OwnerRef->GetCachedGeometry().AbsoluteToLocal(MouseEvent.GetScreenSpacePosition());
			FWindowBaseRect CurrentRect(TargetPosition, TargetSize);

			const FVector2D LocalSizeExpanderLeftTop = CurrentRect.GetTopLeft() - MousePosition;
			const FVector2D LocalSizeExpanderRightDown = MousePosition - CurrentRect.GetBottomRight();

			switch (CurrentSizeType) 
			{ 
				case ST_None: break;
				case ST_LeftUp:		
					CurrentRect.ExtendByWithLimits(FMargin(LocalSizeExpanderLeftTop.X, LocalSizeExpanderLeftTop.Y, 0, 0), Attr_SizeLimits.Get());
				break;
				case ST_Up: 
					CurrentRect.ExtendByWithLimits(FMargin(0, LocalSizeExpanderLeftTop.Y, 0, 0), Attr_SizeLimits.Get());
				break;
				case ST_RightUp:
					CurrentRect.ExtendByWithLimits(FMargin(0, LocalSizeExpanderLeftTop.Y, LocalSizeExpanderRightDown.X, 0), Attr_SizeLimits.Get());
				break;
				case ST_Right:
					CurrentRect.ExtendByWithLimits(FMargin(0, 0, LocalSizeExpanderRightDown.X, 0), Attr_SizeLimits.Get());
				break;
				case ST_RightDown: 
					CurrentRect.ExtendByWithLimits(FMargin(0, 0, LocalSizeExpanderRightDown.X, LocalSizeExpanderRightDown.Y), Attr_SizeLimits.Get());
				break;
				case ST_Down: 
					CurrentRect.ExtendByWithLimits(FMargin(0, 0, 0, LocalSizeExpanderRightDown.Y), Attr_SizeLimits.Get());
				break;
				case ST_LeftDown: 
					CurrentRect.ExtendByWithLimits(FMargin(0, LocalSizeExpanderLeftTop.Y, 0, LocalSizeExpanderRightDown.Y), Attr_SizeLimits.Get());
				break;
				case ST_Left: 
					CurrentRect.ExtendByWithLimits(FMargin(LocalSizeExpanderLeftTop.X, 0, 0, 0), Attr_SizeLimits.Get());
				break;
				case ST_End: break;
				default: ;
			}

			TargetSize = CurrentRect.GetSize();
			TargetPosition = CurrentRect.GetTopLeft();
		}
	}

	return SCompoundWidget::OnMouseMove(MyGeometry, MouseEvent);
}


SWindowBase::ESizeType SWindowBase::DetectZone(const FVector2D& InPosition) const
{
	ESizeType OutMouseZone = ST_None;
	const FSlateRect HitResultBorderSize = FSlateRect(10, 10, 10, 10);
	const FVector2D NodeSize = CurrentSize;

	// Test for hit in location of 'grab' zone
	if (InPosition.Y > (NodeSize.Y - HitResultBorderSize.Bottom))
	{
		OutMouseZone = ST_Down;
	}
	else if (InPosition.Y <= (HitResultBorderSize.Top))
	{
		OutMouseZone = ST_Up;
	}
	else if (InPosition.Y <= 40)
	{
		OutMouseZone = ST_Title;
	}

	if (InPosition.X > (NodeSize.X - HitResultBorderSize.Right))
	{
		if (OutMouseZone == ST_Down)
		{
			OutMouseZone = ST_RightDown;
		}
		else if (OutMouseZone == ST_Up)
		{
			OutMouseZone = ST_RightUp;
		}
		else
		{
			OutMouseZone = ST_Right;
		}
	}
	else if (InPosition.X <= HitResultBorderSize.Left)
	{
		if (OutMouseZone == ST_Up)
		{
			OutMouseZone = ST_LeftUp;
		}
		else if (OutMouseZone == ST_Down)
		{
			OutMouseZone = ST_LeftDown;
		}
		else
		{
			OutMouseZone = ST_Left;
		}
	}

	// Test for hit on rest of frame
	if (OutMouseZone == ST_None)
	{
		if (InPosition.Y > HitResultBorderSize.Top)
		{
			OutMouseZone = ST_None;
		}
		else if (InPosition.X > HitResultBorderSize.Left)
		{
			OutMouseZone = ST_None;
		}
	}

	return OutMouseZone;
}

void SWindowBase::LoadState()
{
	bool localIsVisible, localIsInvisible;
	int64 localZOrder;

	FString TargetSourceDirectory = FPaths::ProjectConfigDir();
	FString TargetUsedDirectory = FPaths::GeneratedConfigDir();

	auto SaveFileName = TargetUsedDirectory + "sWindows.ini";
	auto SaveFile = FConfigFile();	

#if WITH_EDITOR
	SaveFileName = TargetSourceDirectory + "sWindows.ini";
	SaveFile.Read(SaveFileName);
#else
	if (!IFileManager::Get().FileExists(*SaveFileName))
	{
		IFileManager::Get().Copy(*SaveFileName, *(TargetSourceDirectory + "sWindows.ini"));
		SaveFile.Read(TargetSourceDirectory + "sWindows.ini");
	}
	else
	{
		auto SourceSaveFile = FConfigFile();
		int64 localVersion;
		SourceSaveFile.Read(TargetSourceDirectory + "sWindows.ini");
		SourceSaveFile.GetInt64(*SaveTag.ToString(), TEXT("Version"), localVersion);

		SaveFile.Read(SaveFileName);
		SaveFile.GetInt64(*SaveTag.ToString(), TEXT("Version"), LastVersion);

		if (localVersion < LastVersion)
		{
			SaveFile = SourceSaveFile;
		}
	}
#endif

	if (SaveFile.GetBool(*SaveTag.ToString(), TEXT("IsVisible"), localIsVisible))
	{
		SaveFile.GetFloat(*SaveTag.ToString(), TEXT("PosX"), TargetPosition.X);
		SaveFile.GetFloat(*SaveTag.ToString(), TEXT("PosY"), TargetPosition.Y);

		SaveFile.GetFloat(*SaveTag.ToString(), TEXT("SizeX"), TargetSize.X);
		SaveFile.GetFloat(*SaveTag.ToString(), TEXT("SizeY"), TargetSize.Y);

		SaveFile.GetBool(*SaveTag.ToString(), TEXT("IsVisible"), localIsVisible);
		SaveFile.GetBool(*SaveTag.ToString(), TEXT("IsInvisible"), localIsInvisible);

		SaveFile.GetInt64(*SaveTag.ToString(), TEXT("ZOrder"), localZOrder);
		SaveFile.GetInt64(*SaveTag.ToString(), TEXT("Version"), LastVersion);

		ZOrder = localZOrder;

		InternalToggleMode(localIsInvisible);
		InternalToggleWidget(localIsVisible);
	}
}

void SWindowBase::SaveState()
{
#if WITH_EDITOR
	const auto SaveFileName = FPaths::ProjectConfigDir() + "sWindows.ini";
#else
	const auto SaveFileName = FPaths::GeneratedConfigDir() + "sWindows.ini";
#endif
	auto SaveFile = FConfigFile();

	SaveFile.SetString(*SaveTag.ToString(), TEXT("PosX"), *FString::SanitizeFloat(TargetPosition.X));
	SaveFile.SetString(*SaveTag.ToString(), TEXT("PosY"), *FString::SanitizeFloat(TargetPosition.Y));

	SaveFile.SetString(*SaveTag.ToString(), TEXT("SizeX"), *FString::SanitizeFloat(TargetSize.X));
	SaveFile.SetString(*SaveTag.ToString(), TEXT("SizeY"), *FString::SanitizeFloat(TargetSize.Y));

	SaveFile.SetString(*SaveTag.ToString(), TEXT("IsVisible"), IsInInteractiveMode() ? TEXT("True") : TEXT("False"));
	SaveFile.SetString(*SaveTag.ToString(), TEXT("IsInvisible"), bIsInvisible ? TEXT("True") : TEXT("False"));

	SaveFile.SetInt64(*SaveTag.ToString(), TEXT("ZOrder"), ZOrder);

#if WITH_EDITOR
	SaveFile.SetInt64(*SaveTag.ToString(), TEXT("Version"), LastVersion + 1);
#else
	SaveFile.SetInt64(*SaveTag.ToString(), TEXT("Version"), LastVersion);
#endif

	if (IFileManager::Get().FileExists(*SaveFileName))
	{
		SaveFile.UpdateSections(*SaveFileName, *SaveTag.ToString());
	}
	else
	{
		SaveFile.Write(SaveFileName);
	}
}

void SWindowBase::InternalToggleWidget(const bool InToggle)
{
	if (OwnerRef != SNullWidget::NullWidget)
	{
		OwnerRef->RequestZSort();
	}

	SetVisibility(InToggle ? EVisibility::Visible : EVisibility::Collapsed);
}

void SWindowBase::InternalToggleMode(const bool InToggle)
{
	bIsInvisible = InToggle;
	Widget_TitleContainer->SetVisibility(InToggle ? EVisibility::Hidden : EVisibility::SelfHitTestInvisible);
}

void SWindowBase::InternalSetPosition(const FVector2D& InPosition, const bool InIsForce)
{
	TargetPosition = InPosition;

	if (InIsForce)
	{
		CurrentPosition = TargetPosition;
	}
}

void SWindowBase::InternalSetSize(const FVector2D& InSize, const bool InIsForce)
{
	TargetSize.X = FMath::Clamp(InSize.X, Attr_SizeLimits.Get().Min.X, Attr_SizeLimits.Get().Max.X);
	TargetSize.Y = FMath::Clamp(InSize.Y, Attr_SizeLimits.Get().Min.Y, Attr_SizeLimits.Get().Max.Y);

	if (InIsForce)
	{
		CurrentSize = TargetSize;
	}
}
