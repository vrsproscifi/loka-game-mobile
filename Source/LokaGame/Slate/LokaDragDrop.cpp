
#include "LokaGame.h"
#include "LokaDragDrop.h"
#include "Runtime/Engine/Classes/Engine/UserInterfaceSettings.h"

FLokaDragDropOp::FLokaDragDropOp()
	: FGameDragDropOperation()
	, SourceRef(SNullWidget::NullWidget)
	, DecoratorRef(SNullWidget::NullWidget)
	, DPIScale(1.0f)
{
	if (GEngine && GEngine->GameViewport)
	{
		FVector2D ViewportSize;
		GEngine->GameViewport->GetViewportSize(ViewportSize);
		DPIScale = GetDefault<UUserInterfaceSettings>()->GetDPIScaleBasedOnSize(FIntPoint(ViewportSize.X, ViewportSize.Y));
	}
}

FLokaDragDropOp::FLokaDragDropOp(TSharedRef<SWidget> Decorator)
	: FLokaDragDropOp()
{
	DecoratorRef = Decorator;
}

FLokaDragDropOp::FLokaDragDropOp(TSharedRef<SWidget> Source, TSharedRef<SWidget> Decorator)
	: FLokaDragDropOp(Decorator)
{
	SourceRef = Source;
}

FLokaDragDropOp::FLokaDragDropOp(TSharedRef<SWidget> Source, TSharedRef<SWidget> Decorator, UObject* Object)
	: FLokaDragDropOp(Source, Decorator)
{
	ObjectPtr = Object;
}

void FLokaDragDropOp::OnDragged(const FDragDropEvent& DragDropEvent)
{
	DecoratorPosition = DragDropEvent.GetScreenSpacePosition();
	FGameDragDropOperation::OnDragged(DragDropEvent);
}

TSharedPtr<SWidget> FLokaDragDropOp::GetDefaultDecorator() const
{
	return SNew(SDPIScaler).DPIScale(DPIScale)[DecoratorRef];
}

TWeakObjectPtr<UObject> FLokaDragDropOp::GetObjectPtr() const
{
	return ObjectPtr;
}