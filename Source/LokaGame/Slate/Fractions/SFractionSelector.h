// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"

class UFractionEntity;

DECLARE_DELEGATE_OneParam(FOnFractionSelected, const UFractionEntity*);

class LOKAGAME_API SFractionSelector : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SFractionSelector)
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_EVENT(FOnFractionSelected, OnFractionSelected)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);
	void AddFraction(const UFractionEntity*);


protected:

	FOnFractionSelected OnFractionSelected;
	TSharedPtr<SHorizontalBox> Box;
};
