// VRSPRO

#include "LokaGame.h"
#include "SFractionSelector.h"
#include "SlateOptMacros.h"

#include "Fraction/FractionEntity.h"
#include "SWindowBackground.h"
#include "SAnimatedBackground.h"

#include "Styles/FractionManagerWidgetStyle.h"
#include "Layout/SScaleBox.h"

class LOKAGAME_API SFractionSelector_Item : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SFractionSelector_Item)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FFractionManagerStyle>(TEXT("SFractionManagerStyle")))
	{}
	SLATE_STYLE_ARGUMENT(FFractionManagerStyle, Style)
	SLATE_EVENT(FOnClicked, OnClicked)
	SLATE_END_ARGS()

	BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
	void Construct(const FArguments& InArgs, const UFractionEntity* InInstance)
	{
		Style = InArgs._Style;
		Instance = InInstance;

		ChildSlot
		[
			SNew(SButton)
			.OnClicked(InArgs._OnClicked)
			.ButtonStyle(&Style->Selector.Button)
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot().AutoHeight()
				[
					SNew(SBox).WidthOverride(128).HeightOverride(128)
					[
						SNew(SScaleBox)
						.Stretch(EStretch::ScaleToFit)
						[
							SNew(SImage).Image(Instance->GetIcon())
						]
					]
				]
				+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Center).Padding(FMargin(4, 2))
				[
					SNew(STextBlock).Text(Instance->GetDescription().Name).TextStyle(&Style->Selector.Name)
				]
				+ SVerticalBox::Slot().FillHeight(1).HAlign(HAlign_Center).Padding(FMargin(4, 10))
				[
					SNew(STextBlock).Text(Instance->GetDescription().Description).TextStyle(&Style->Selector.Desc).AutoWrapText(true)
				]
				+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(10, 2)).HAlign(HAlign_Center).VAlign(VAlign_Center)
				[
					SNew(STextBlock)					
					.Margin(FMargin(20, 8))
					.Text(NSLOCTEXT("SFractionSelector", "SFractionSelector.Join", "Join"))					
					.TextStyle(&Style->Selector.ButtonText)
					
				]
			]
		];
	}
	END_SLATE_FUNCTION_BUILD_OPTIMIZATION

protected:

	const FFractionManagerStyle* Style;
	const UFractionEntity* Instance;
};


BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SFractionSelector::Construct(const FArguments& InArgs)
{
	OnFractionSelected = InArgs._OnFractionSelected;

	ChildSlot
	[
		SNew(SBox)
		.WidthOverride(920)
		.MinDesiredHeight(640)
		.MaxDesiredHeight(920)
		[
			SAssignNew(Box, SHorizontalBox)
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SFractionSelector::AddFraction(const UFractionEntity* Fraction)
{
	Box->AddSlot().FillWidth(1)
		[
			SNew(SFractionSelector_Item, Fraction).OnClicked_Lambda([&, f = Fraction]() { 
				OnFractionSelected.ExecuteIfBound(f);
				return FReply::Handled(); 
			})
		];
}