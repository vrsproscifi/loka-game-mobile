@echo off
echo Killing crash reporter
Taskkill /F /IM CrashReportProcess.exe
Taskkill /F /IM CrashReportReceiver.exe
echo Check svn updates

echo Starting build server only
cd C:\WorkSpace\Loka\trunk
START /WAIT /REALTIME BuildServer_x64.bat
echo Commit server to repository
cd C:\WorkSpace\BuildedGameServer
svn add C:\WorkSpace\BuildedGameServer --force
svn commit -m=commit_by_script_UT_ALPHA_VERSIONS
echo Running crash reporter
cd C:\WorkSpace\UnrealEngine\Engine\Binaries\DotNET
START CrashReportProcess.exe
START CrashReportReceiver.exe
echo Script complete %DATE% - %TIME%, check logs on errors before real update.
echo Please update server manual
pause /NOBREAK

Test script for update and build.
By Serinc