@echo off
echo Killing crash reporter
Taskkill /F /IM CrashReportProcess.exe
Taskkill /F /IM CrashReportReceiver.exe
Taskkill /F /IM Launcher.exe
Taskkill /F /IM LauncherPTS.exe

echo Check svn updates
cd /D D:\WorkSpace\Loka\trunk\Content
svn update

echo Remove build client directory
cd /D C:\0Build\Game
rmdir /s /q WindowsNoEditor

echo Remove build server directory, without root directory
cd /D C:\0Build\Server\WindowsServer
rmdir /s /q Engine
rmdir /s /q LokaGame

echo Starting build clients only
cd /D D:\WorkSpace\Loka\trunk

echo Build x64
START /WAIT /REALTIME BuildClient_x64.bat  
if not exist "C:\0Build\Game\WindowsNoEditor\LokaGame\Binaries\Win64\LokaGame-Win64-Shipping.exe" goto ProcessDoneWithError
::echo Build x32
::START /WAIT /REALTIME BuildClient_x32_nocook.bat 

echo Starting torrent building
cd /D C:\0Build\Game\WindowsNoEditor
del /F /Q "LokaGame.exe"
cd /D D:\WorkSpace\Loka\trunk\Binaries
START /WAIT /REALTIME run_build.bat

echo Starting build server only
cd /D D:\WorkSpace\Loka\trunk
START /WAIT /REALTIME BuildServer_x64.bat
if not exist "C:\0Build\Server\WindowsServer" goto ProcessDoneWithError

echo Commit server to repository
cd /D C:\0Build\Server\WindowsServer
svn add C:\0Build\Server\WindowsServer\Engine --force
svn add C:\0Build\Server\WindowsServer\LokaGame --force
svn commit -m=commit_by_script
goto ProcessDone
:ProcessDoneWithError
color 0C
echo Found errors, please fix and try again!
:ProcessDone
echo Running crash reporter
START C:\WorkSpace\UnrealEngine\Engine\Binaries\DotNET\CrashReport\CrashReportProcess.exe
START C:\WorkSpace\UnrealEngine\Engine\Binaries\DotNET\CrashReport\CrashReportReceiver.exe
echo Script complete %DATE% - %TIME%, check logs on errors before real update.
echo Please update server manual
pause /NOBREAK

Test script for update and build.
By Serinc