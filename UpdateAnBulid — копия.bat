@echo off
echo Killing crash reporter
Taskkill /F /IM CrashReportProcess.exe
Taskkill /F /IM CrashReportReceiver.exe
echo Check svn updates
cd D:\WorkSpace\Loka\trunk\Content
svn update
echo Remove build client directory
cd D:\WorkSpace\Steam\tools\ContentBuilder\content\windows_content
rmdir /s /q WindowsNoEditor
echo Remove build server directory, without root directory
cd D:\WorkSpace\BuildedGameServer\WindowsServer
rmdir /s /q Engine
rmdir /s /q LokaGame
echo Starting build clients only
cd D:\WorkSpace\Loka\trunk
echo Build x64
START /WAIT /REALTIME BuildClient_x64.bat  
echo Build x32
START /WAIT /REALTIME BuildClient_x32_nocook.bat 
echo Starting uploading to Steam
cd D:\WorkSpace\Steam\tools\ContentBuilder\content\windows_content\WindowsNoEditor
del /F /Q "LokaGame.exe"
cd D:\WorkSpace\Steam\tools\ContentBuilder
START /WAIT /REALTIME run_build.bat
echo Starting build server only
cd D:\WorkSpace\Loka\trunk
START /WAIT /REALTIME BuildServer_x64.bat
echo Commit server to repository
cd D:\WorkSpace\BuildedGameServer\WindowsServer
svn add D:\WorkSpace\BuildedGameServer\WindowsServer --force
svn commit -m=commit_by_script
echo Running crash reporter
START C:\WorkSpace\UnrealEngine\Engine\Binaries\DotNET\CrashReportProcess.exe
START C:\WorkSpace\UnrealEngine\Engine\Binaries\DotNET\CrashReportReceiver.exe
echo Script complete %DATE% - %TIME%, check logs on errors before real update.
echo Please update server manual
pause /NOBREAK

Test script for update and build.
By Serinc