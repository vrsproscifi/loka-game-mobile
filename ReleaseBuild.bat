@echo off
echo Killing crash reporter
Taskkill /F /IM Launcher.exe

echo Check svn updates
cd /D D:\WorkSpace\Loka\trunk\Content
svn update

echo Remove build client directory
cd /D C:\ReleaseBuild\Game
rmdir /s /q WindowsNoEditor

echo Remove build server directory, without root directory
cd /D C:\ReleaseBuild\Server\WindowsServer
rmdir /s /q Engine
rmdir /s /q LokaGame

echo Starting build clients only
cd /D D:\WorkSpace\Loka\trunk

echo Build x64
START /WAIT /REALTIME ReleaseBuildClient_x64.bat  
if not exist "C:\ReleaseBuild\Game\WindowsNoEditor\LokaGame\Binaries\Win64\LokaGame-Win64-Shipping.exe" goto ProcessDoneWithError
::echo Build x32
::START /WAIT /REALTIME ReleaseBuildClient_x32_nocook.bat 
::if not exist "C:\ReleaseBuild\Game\WindowsNoEditor\LokaGame\Binaries\Win32\LokaGame-Win32-Shipping.exe" goto ProcessDoneWithError

echo Copy configs
xcopy "D:\WorkSpace\Loka\trunk\Config" "C:\ReleaseBuild\Game\WindowsNoEditor\LokaGame\Config" /i/f

echo Starting torrent building
cd /D C:\ReleaseBuild\Game\WindowsNoEditor
del /F /Q "LokaGame.exe"
cd /D D:\WorkSpace\Loka\trunk\Binaries
START /WAIT /REALTIME ReleaseSeeding.bat

echo Starting build server only
cd /D D:\WorkSpace\Loka\trunk
START /WAIT /REALTIME ReleaseBuildServer_x64.bat
if not exist "C:\ReleaseBuild\Server\WindowsServer" goto ProcessDoneWithError

echo Commit server to repository
cd /D C:\ReleaseBuild\Server\WindowsServer
svn add C:\ReleaseBuild\Server\WindowsServer\Engine --force
svn add C:\ReleaseBuild\Server\WindowsServer\LokaGame --force
svn commit -m=commit_by_script
goto ProcessDone
:ProcessDoneWithError
color 0C
echo Found errors, please fix and try again!
:ProcessDone

echo Script complete %DATE% - %TIME%, check logs on errors before real update.
echo Please update server manual
pause /NOBREAK

Test script for update and build.
By Serinc